/**
//-----------------------------------------------------------------------------
// This is an unpublished work, the copyright in which vests in 
// DreamChip Technologies GmbH. The information contained herein is the property 
// of DreamChip Technologies GmbH and is supplied without liability for errors or 
// omissions. No part may be reproduced or used except as authorized by 
// contract or other written permission.
// Copyright(c) DreamChip Technologies GmbH, 2023. All rights reserved.
//-----------------------------------------------------------------------------
//
//       N-1         _ __             _
//        __        |  ||  /    1 \    |
//  X  = \    x  cos|  -- ( n + -  ) k |   k = 0,...,N-1
//   k   /__   n    |_  N  \    2 /   _|
//       n=0
//
//-----------------------------------------------------------------------------
// Module   : rpp_hist20
//
// Purpose  : Register C header file
//
// Creator  : DCT
//
// Comments : automatically generated by sig (4.0.5)
//            !!!!! DO NOT EDIT THIS FILE !!!!!
//-----------------------------------------------------------------------------
* @file rpp_hist20_regs_mask.h
*
* <pre>
*
* Description:
*   This header file exports the module register structure and masks. 
*   It should not be included directly by your driver/application, it will be  
*   exported by the top level header file. 
*
* </pre>
*/
/*****************************************************************************/

#ifndef __RPP_HIST20_REGS_MASK_H__
#define __RPP_HIST20_REGS_MASK_H__


// - MASK AND SHIFT MACROS ----------------------------------------------------------

//! Register Group: isp_histogram_module_registers

//! Register: rpp_hist_version: Histogram Version (RPP_HIST_BASE + 0x00000000)
//! Slice: hist_version:
//! Histogram version 
//0x3; internal bits = 12 
//0x4; internal bits = 20 
//0x5; internal bits = 24
#define RPP_HIST_HIST_VERSION
#define RPP_HIST_HIST_VERSION_MASK 0x000000FFU
#define RPP_HIST_HIST_VERSION_SHIFT 0U

//! Register: rpp_hist_ctrl: Histogram control (RPP_HIST_BASE + 0x00000004)
//! Slice: hist_update_enable:
//! 0: automatic register update at end of measuement or frame denied 
// 1: automatic register update at end of measuement or frame enabled.
#define RPP_HIST_HIST_UPDATE_ENABLE
#define RPP_HIST_HIST_UPDATE_ENABLE_MASK 0x00000001U
#define RPP_HIST_HIST_UPDATE_ENABLE_SHIFT 0U

//! Register: rpp_hist_mode: Histogram Mode (RPP_HIST_BASE + 0x00000008)
//! Slice: hist_mode:
//! histogram mode (RGB/Bayer)
// 7, 6: reserved 
// 5: bayer Gb histogram 
// 4: bayer B histogram 
// 3: bayer Gr histogram 
// 2: bayer R histogram 
// 1: Y/R/G/B histogram controlled via coefficients coeff_r/g/b 
// 0: disable, no measurements 
// With histogram mode 1 all three subchannels are used. Modes 2...5 use only th subchannel 0 which transfers the bayer pattern data. Check with top level specification to discover the channel type.
#define RPP_HIST_HIST_MODE
#define RPP_HIST_HIST_MODE_MASK 0x00000007U
#define RPP_HIST_HIST_MODE_SHIFT 0U

//! Register: rpp_hist_channel_sel: Histogram Channel Select (RPP_HIST_BASE + 0x0000000c)
//! Slice: channel_select:
//! select 1 out of max. 8 input channels 
// 7: channel 7 
// 6: channel 6 
// 5: channel 5 
// 4: channel 4 
// 3: channel 3 
// 2: channel 2 
// 1: channel 1 
// 0: channel 0 
// The channels might be RGB or Bayer channels. Each channel provides 3 subchannels for transfer the RGB component data. However if the channel operates in bayer mode only subchannel 0 is used transferring the interleaved bayer pattern data. Check with top level specification to discover the channel type.
#define RPP_HIST_CHANNEL_SELECT
#define RPP_HIST_CHANNEL_SELECT_MASK 0x00000007U
#define RPP_HIST_CHANNEL_SELECT_SHIFT 0U

//! Register: rpp_hist_last_meas_line: Last Measurement Line for Histogram (RPP_HIST_BASE + 0x00000010)
//! Slice: last_meas_line:
//! last measurement line in video 
// Should be equal to hist_v_offset + 5*hist_v_size -1. 
// Used for fast channel switching.
#define RPP_HIST_LAST_MEAS_LINE
#define RPP_HIST_LAST_MEAS_LINE_MASK 0x00001FFFU
#define RPP_HIST_LAST_MEAS_LINE_SHIFT 0U

//! Register: rpp_hist_subsampling: Subsampling properties (RPP_HIST_BASE + 0x00000014)
//! Slice: v_stepsize:
//! histogram veritcal predivider, process every (stepsize)th line, all other lines are skipped 
// RGB mode: 
// 0: not allowed 
// 1: process every input line 
// 2: process every second line 
// 3: process every third input line 
// 4: process every fourth input line 
// ... 
// 7FH: process every 127th line 
// Bayer mode: 
// 0: not allowed 
// 1: process every second input line 
// 2: process every fourth line 
// 3: process every sixth input line 
// 4: process every eighth input line 
// ... 
// 7FH: process every 254th line 
// In bayer mode vertical subsampling will start at the 1st input line which contain the bayer component selected in RPP_MODE.
#define RPP_HIST_V_STEPSIZE
#define RPP_HIST_V_STEPSIZE_MASK 0x7F000000U
#define RPP_HIST_V_STEPSIZE_SHIFT 24U
//! Slice: h_step_inc:
//! horizontal subsampling step counter increment. 
// In RGB mode the subsampling counter cnt is incremented by h_step_inc with every input pixel (cnt %= cnt + h_step_inc). A valid subsampling position is reached when cnt would result in a value %= 2^16. In this case the new counter value is cnt = cnt + h_step_inc - 2^16. For example if every incoming pixel shall be selected configure h_step_inc = 2^16. 
// In Bayer mode the behaviour is similar but for the fact that cnt is only incremented for pixels which belong to the bayer component selected in RPP_HIST_MODE. h_step_inc must be <= 2^16.
#define RPP_HIST_H_STEP_INC
#define RPP_HIST_H_STEP_INC_MASK 0x0001FFFFU
#define RPP_HIST_H_STEP_INC_SHIFT 0U

//! Register: rpp_hist_coeff_r: Color conversion coefficient for red (RPP_HIST_BASE + 0x00000018)
//! Slice: coeff_r:
//! coefficient for red for weighted component sum: out_sample = coeff_r * red + coeff_g*green + coeff_b * blue.
#define RPP_HIST_COEFF_R
#define RPP_HIST_COEFF_R_MASK 0x000000FFU
#define RPP_HIST_COEFF_R_SHIFT 0U

//! Register: rpp_hist_coeff_g: Color conversion coefficient for green (RPP_HIST_BASE + 0x0000001c)
//! Slice: coeff_g:
//! coefficient for green for weighted component sum: out_sample = coeff_r * red + coeff_g*green + coeff_b * blue.
#define RPP_HIST_COEFF_G
#define RPP_HIST_COEFF_G_MASK 0x000000FFU
#define RPP_HIST_COEFF_G_SHIFT 0U

//! Register: rpp_hist_coeff_b: Color conversion coefficient for blue (RPP_HIST_BASE + 0x00000020)
//! Slice: coeff_b:
//! coefficient for blue for weighted component sum: out_sample = coeff_r * red + coeff_g*green + coeff_b * blue.
#define RPP_HIST_COEFF_B
#define RPP_HIST_COEFF_B_MASK 0x000000FFU
#define RPP_HIST_COEFF_B_SHIFT 0U

//! Register: rpp_hist_h_offs: Histogram window horizontal offset for first window of 25 sub-windows (RPP_HIST_BASE + 0x00000024)
//! Slice: hist_h_offset:
//! Horizontal offset of first window in pixels.
#define RPP_HIST_HIST_H_OFFSET
#define RPP_HIST_HIST_H_OFFSET_MASK 0x00001FFFU
#define RPP_HIST_HIST_H_OFFSET_SHIFT 0U

//! Register: rpp_hist_v_offs: Histogram window vertical offset for first window of 25 sub-windows (RPP_HIST_BASE + 0x00000028)
//! Slice: hist_v_offset:
//! Vertical offset of first window in pixels.
#define RPP_HIST_HIST_V_OFFSET
#define RPP_HIST_HIST_V_OFFSET_MASK 0x00001FFFU
#define RPP_HIST_HIST_V_OFFSET_SHIFT 0U

//! Register: rpp_hist_h_size: Horizontal (sub-)window size (RPP_HIST_BASE + 0x0000002c)
//! Slice: hist_h_size:
//! Horizontal size in pixels of one sub-window.
#define RPP_HIST_HIST_H_SIZE
#define RPP_HIST_HIST_H_SIZE_MASK 0x000007FFU
#define RPP_HIST_HIST_H_SIZE_SHIFT 0U

//! Register: rpp_hist_v_size: Vertical (sub-)window size (RPP_HIST_BASE + 0x00000030)
//! Slice: hist_v_size:
//! Vertical size in lines of one sub-window.
#define RPP_HIST_HIST_V_SIZE
#define RPP_HIST_HIST_V_SIZE_MASK 0x000007FFU
#define RPP_HIST_HIST_V_SIZE_SHIFT 0U

//! Register: rpp_hist_sample_range: Weighting factor for sub-windows (RPP_HIST_BASE + 0x00000034)
//! Slice: sample_shift:
//! sample (left) shift will be executed after offset subtraction and prior to histogram evaluation
#define RPP_HIST_SAMPLE_SHIFT
#define RPP_HIST_SAMPLE_SHIFT_MASK 0x1F000000U
#define RPP_HIST_SAMPLE_SHIFT_SHIFT 24U
//! Slice: sample_offset:
//! sample offset will be subtracted from input sample prior to shift and histogram evaluation
#define RPP_HIST_SAMPLE_OFFSET
#define RPP_HIST_SAMPLE_OFFSET_MASK 0x000FFFFFU
#define RPP_HIST_SAMPLE_OFFSET_SHIFT 0U

//! Register: rpp_hist_weight_00to30: Weighting factor for sub-windows (RPP_HIST_BASE + 0x00000038)
//! Slice: hist_weight_30:
//! weighting factor for sub-window 30
#define RPP_HIST_HIST_WEIGHT_30
#define RPP_HIST_HIST_WEIGHT_30_MASK 0x1F000000U
#define RPP_HIST_HIST_WEIGHT_30_SHIFT 24U
//! Slice: hist_weight_20:
//! weighting factor for sub-window 20
#define RPP_HIST_HIST_WEIGHT_20
#define RPP_HIST_HIST_WEIGHT_20_MASK 0x001F0000U
#define RPP_HIST_HIST_WEIGHT_20_SHIFT 16U
//! Slice: hist_weight_10:
//! weighting factor for sub-window 10
#define RPP_HIST_HIST_WEIGHT_10
#define RPP_HIST_HIST_WEIGHT_10_MASK 0x00001F00U
#define RPP_HIST_HIST_WEIGHT_10_SHIFT 8U
//! Slice: hist_weight_00:
//! weighting factor for sub-window 00
#define RPP_HIST_HIST_WEIGHT_00
#define RPP_HIST_HIST_WEIGHT_00_MASK 0x0000001FU
#define RPP_HIST_HIST_WEIGHT_00_SHIFT 0U

//! Register: rpp_hist_weight_40to21: Weighting factor for sub-windows (RPP_HIST_BASE + 0x0000003c)
//! Slice: hist_weight_21:
//! weighting factor for sub-window 21
#define RPP_HIST_HIST_WEIGHT_21
#define RPP_HIST_HIST_WEIGHT_21_MASK 0x1F000000U
#define RPP_HIST_HIST_WEIGHT_21_SHIFT 24U
//! Slice: hist_weight_11:
//! weighting factor for sub-window 11
#define RPP_HIST_HIST_WEIGHT_11
#define RPP_HIST_HIST_WEIGHT_11_MASK 0x001F0000U
#define RPP_HIST_HIST_WEIGHT_11_SHIFT 16U
//! Slice: hist_weight_01:
//! weighting factor for sub-window 01
#define RPP_HIST_HIST_WEIGHT_01
#define RPP_HIST_HIST_WEIGHT_01_MASK 0x00001F00U
#define RPP_HIST_HIST_WEIGHT_01_SHIFT 8U
//! Slice: hist_weight_40:
//! weighting factor for sub-window 40
#define RPP_HIST_HIST_WEIGHT_40
#define RPP_HIST_HIST_WEIGHT_40_MASK 0x0000001FU
#define RPP_HIST_HIST_WEIGHT_40_SHIFT 0U

//! Register: rpp_hist_weight_31to12: Weighting factor for sub-windows (RPP_HIST_BASE + 0x00000040)
//! Slice: hist_weight_12:
//! weighting factor for sub-window 12
#define RPP_HIST_HIST_WEIGHT_12
#define RPP_HIST_HIST_WEIGHT_12_MASK 0x1F000000U
#define RPP_HIST_HIST_WEIGHT_12_SHIFT 24U
//! Slice: hist_weight_02:
//! weighting factor for sub-window 02
#define RPP_HIST_HIST_WEIGHT_02
#define RPP_HIST_HIST_WEIGHT_02_MASK 0x001F0000U
#define RPP_HIST_HIST_WEIGHT_02_SHIFT 16U
//! Slice: hist_weight_41:
//! weighting factor for sub-window 41
#define RPP_HIST_HIST_WEIGHT_41
#define RPP_HIST_HIST_WEIGHT_41_MASK 0x00001F00U
#define RPP_HIST_HIST_WEIGHT_41_SHIFT 8U
//! Slice: hist_weight_31:
//! weighting factor for sub-window 31
#define RPP_HIST_HIST_WEIGHT_31
#define RPP_HIST_HIST_WEIGHT_31_MASK 0x0000001FU
#define RPP_HIST_HIST_WEIGHT_31_SHIFT 0U

//! Register: rpp_hist_weight_22to03: Weighting factor for sub-windows (RPP_HIST_BASE + 0x00000044)
//! Slice: hist_weight_03:
//! weighting factor for sub-window 03
#define RPP_HIST_HIST_WEIGHT_03
#define RPP_HIST_HIST_WEIGHT_03_MASK 0x1F000000U
#define RPP_HIST_HIST_WEIGHT_03_SHIFT 24U
//! Slice: hist_weight_42:
//! weighting factor for sub-window 42
#define RPP_HIST_HIST_WEIGHT_42
#define RPP_HIST_HIST_WEIGHT_42_MASK 0x001F0000U
#define RPP_HIST_HIST_WEIGHT_42_SHIFT 16U
//! Slice: hist_weight_32:
//! weighting factor for sub-window 32
#define RPP_HIST_HIST_WEIGHT_32
#define RPP_HIST_HIST_WEIGHT_32_MASK 0x00001F00U
#define RPP_HIST_HIST_WEIGHT_32_SHIFT 8U
//! Slice: hist_weight_22:
//! weighting factor for sub-window 22
#define RPP_HIST_HIST_WEIGHT_22
#define RPP_HIST_HIST_WEIGHT_22_MASK 0x0000001FU
#define RPP_HIST_HIST_WEIGHT_22_SHIFT 0U

//! Register: rpp_hist_weight_13to43: Weighting factor for sub-windows (RPP_HIST_BASE + 0x00000048)
//! Slice: hist_weight_43:
//! weighting factor for sub-window 43
#define RPP_HIST_HIST_WEIGHT_43
#define RPP_HIST_HIST_WEIGHT_43_MASK 0x1F000000U
#define RPP_HIST_HIST_WEIGHT_43_SHIFT 24U
//! Slice: hist_weight_33:
//! weighting factor for sub-window 33
#define RPP_HIST_HIST_WEIGHT_33
#define RPP_HIST_HIST_WEIGHT_33_MASK 0x001F0000U
#define RPP_HIST_HIST_WEIGHT_33_SHIFT 16U
//! Slice: hist_weight_23:
//! weighting factor for sub-window 23
#define RPP_HIST_HIST_WEIGHT_23
#define RPP_HIST_HIST_WEIGHT_23_MASK 0x00001F00U
#define RPP_HIST_HIST_WEIGHT_23_SHIFT 8U
//! Slice: hist_weight_13:
//! weighting factor for sub-window 13
#define RPP_HIST_HIST_WEIGHT_13
#define RPP_HIST_HIST_WEIGHT_13_MASK 0x0000001FU
#define RPP_HIST_HIST_WEIGHT_13_SHIFT 0U

//! Register: rpp_hist_weight_04to34: Weighting factor for sub-windows (RPP_HIST_BASE + 0x0000004c)
//! Slice: hist_weight_34:
//! weighting factor for sub-window 34
#define RPP_HIST_HIST_WEIGHT_34
#define RPP_HIST_HIST_WEIGHT_34_MASK 0x1F000000U
#define RPP_HIST_HIST_WEIGHT_34_SHIFT 24U
//! Slice: hist_weight_24:
//! weighting factor for sub-window 24
#define RPP_HIST_HIST_WEIGHT_24
#define RPP_HIST_HIST_WEIGHT_24_MASK 0x001F0000U
#define RPP_HIST_HIST_WEIGHT_24_SHIFT 16U
//! Slice: hist_weight_14:
//! weighting factor for sub-window 14
#define RPP_HIST_HIST_WEIGHT_14
#define RPP_HIST_HIST_WEIGHT_14_MASK 0x00001F00U
#define RPP_HIST_HIST_WEIGHT_14_SHIFT 8U
//! Slice: hist_weight_04:
//! weighting factor for sub-window 04
#define RPP_HIST_HIST_WEIGHT_04
#define RPP_HIST_HIST_WEIGHT_04_MASK 0x0000001FU
#define RPP_HIST_HIST_WEIGHT_04_SHIFT 0U

//! Register: rpp_hist_weight_44: Weighting factor for sub-windows (RPP_HIST_BASE + 0x00000050)
//! Slice: hist_weight_44:
//! weighting factor for sub-window 44
#define RPP_HIST_HIST_WEIGHT_44
#define RPP_HIST_HIST_WEIGHT_44_MASK 0x0000001FU
#define RPP_HIST_HIST_WEIGHT_44_SHIFT 0U

//! Register: rpp_hist_forced_upd_start_line: Forced update start line limit (RPP_HIST_BASE + 0x00000054)
//! Slice: forced_upd_start_line:
//! start line for histogram calculation in case of forced update. histogram is started as soon as current line < forced_upd_start_line. Used start line will be given in RPP_HIST_VSTART_STATUS.
#define RPP_HIST_FORCED_UPD_START_LINE
#define RPP_HIST_FORCED_UPD_START_LINE_MASK 0x00001FFFU
#define RPP_HIST_FORCED_UPD_START_LINE_SHIFT 0U

//! Register: rpp_hist_forced_update: Histogram forced update (RPP_HIST_BASE + 0x00000058)
//! Slice: forced_upd:
//! 0: no effect 
// 1: forcing register update.
#define RPP_HIST_FORCED_UPD
#define RPP_HIST_FORCED_UPD_MASK 0x00000001U
#define RPP_HIST_FORCED_UPD_SHIFT 0U

//! Register: rpp_hist_vstart_status: Forced update start line status (RPP_HIST_BASE + 0x0000005c)
//! Slice: hist_vstart_status:
//! start line for histogram. Important in case of backward switching because 1st histogram after switch might not cover the complete image.
#define RPP_HIST_HIST_VSTART_STATUS
#define RPP_HIST_HIST_VSTART_STATUS_MASK 0x00001FFFU
#define RPP_HIST_HIST_VSTART_STATUS_SHIFT 0U

//! Register array: rpp_hist_bin: histogram measurement result bin (RPP_HIST_BASE + 0x00000060 + n*0x4 (n=0..31))
//! Slice: hist_bin:
// measured bin count as 16-bit unsigned integer value plus 4 bit fractional part
#define RPP_HIST_HIST_BIN
#define RPP_HIST_HIST_BIN_MASK 0x000FFFFFU
#define RPP_HIST_HIST_BIN_SHIFT 0U

#endif /* __RPP_HIST20_REGS_MASK_H__ */
/*************************** EOF **************************************/
