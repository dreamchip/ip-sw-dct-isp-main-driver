/*
//-----------------------------------------------------------------------------
// This is an unpublished work, the copyright in which vests in 
// DreamChip Technologies GmbH. The information contained herein is the property 
// of DreamChip Technologies GmbH and is supplied without liability for errors or 
// omissions. No part may be reproduced or used except as authorized by 
// contract or other written permission.
// Copyright(c) DreamChip Technologies GmbH, 2022. All rights reserved.
//-----------------------------------------------------------------------------
//
//       N-1         _ __             _
//        __        |  ||  /    1 \    |
//  X  = \    x  cos|  -- ( n + -  ) k |   k = 0,...,N-1
//   k   /__   n    |_  N  \    2 /   _|
//       n=0
//
//-----------------------------------------------------------------------------
// Module   : rpp_acq
//
// Purpose  : Register address map definition
//
// Creator  : DCT
//
// Comments : automatically generated by sig (4.0.5)
//            !!!!! DO NOT EDIT THIS FILE !!!!!
//-----------------------------------------------------------------------------
 * file rpp_acq_regs_addr_map.h
 *
 * Programming Language: C
 * Runtime Environment : ARM, WIN32, Linux, Solaris
 *
 * Description:
 *  generate full address map based on external definition of *_BASE macro
 */
/*****************************************************************************/

#ifndef __RPP_ACQ_REGS_ADDR_MAP_H__
#define __RPP_ACQ_REGS_ADDR_MAP_H__


#ifndef RPP_ACQ_BASE
#error You must #include the global memory map header file before rpp_acq_regs_addr_map.h and it must define RPP_ACQ_BASE
#endif /* RPP_ACQ_BASE */



// register group: rpp_input_acquisition_and_formatting_registers
#define RPP_RPP_ACQ_VERSION_REG (RPP_ACQ_BASE + 0x00000000)
#define RPP_RPP_ACQ_CTRL_REG (RPP_ACQ_BASE + 0x00000004)
#define RPP_RPP_ACQ_PROP_REG (RPP_ACQ_BASE + 0x00000008)
#define RPP_RPP_ACQ_H_OFFS_REG (RPP_ACQ_BASE + 0x0000000C)
#define RPP_RPP_ACQ_V_OFFS_REG (RPP_ACQ_BASE + 0x00000010)
#define RPP_RPP_ACQ_H_SIZE_REG (RPP_ACQ_BASE + 0x00000014)
#define RPP_RPP_ACQ_V_SIZE_REG (RPP_ACQ_BASE + 0x00000018)
#define RPP_RPP_ACQ_OUT_H_OFFS_REG (RPP_ACQ_BASE + 0x0000001C)
#define RPP_RPP_ACQ_OUT_V_OFFS_REG (RPP_ACQ_BASE + 0x00000020)
#define RPP_RPP_ACQ_OUT_H_SIZE_REG (RPP_ACQ_BASE + 0x00000024)
#define RPP_RPP_ACQ_OUT_V_SIZE_REG (RPP_ACQ_BASE + 0x00000028)
#define RPP_RPP_ACQ_FLAGS_SHD_REG (RPP_ACQ_BASE + 0x0000002C)
#define RPP_RPP_ACQ_OUT_H_OFFS_SHD_REG (RPP_ACQ_BASE + 0x00000030)
#define RPP_RPP_ACQ_OUT_V_OFFS_SHD_REG (RPP_ACQ_BASE + 0x00000034)
#define RPP_RPP_ACQ_OUT_H_SIZE_SHD_REG (RPP_ACQ_BASE + 0x00000038)
#define RPP_RPP_ACQ_OUT_V_SIZE_SHD_REG (RPP_ACQ_BASE + 0x0000003C)

// last index
#define RPP_ACQ_REGIDX_LAST      (RPP_ACQ_BASE + 0x00000040)
#define RPP_ACQ_REGIDX_LAST_OFFS 0x00000040

#endif /* __RPP_ACQ_REGS_ADDR_MAP_H__ */ 
/*************************** EOF **************************************/
