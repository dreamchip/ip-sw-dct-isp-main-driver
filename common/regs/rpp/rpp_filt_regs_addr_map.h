/*
//-----------------------------------------------------------------------------
// This is an unpublished work, the copyright in which vests in 
// DreamChip Technologies GmbH. The information contained herein is the property 
// of DreamChip Technologies GmbH and is supplied without liability for errors or 
// omissions. No part may be reproduced or used except as authorized by 
// contract or other written permission.
// Copyright(c) DreamChip Technologies GmbH, 2023. All rights reserved.
//-----------------------------------------------------------------------------
//
//       N-1         _ __             _
//        __        |  ||  /    1 \    |
//  X  = \    x  cos|  -- ( n + -  ) k |   k = 0,...,N-1
//   k   /__   n    |_  N  \    2 /   _|
//       n=0
//
//-----------------------------------------------------------------------------
// Module   : rpp_filt
//
// Purpose  : Register address map definition
//
// Creator  : DCT
//
// Comments : automatically generated by sig (4.0.5)
//            !!!!! DO NOT EDIT THIS FILE !!!!!
//-----------------------------------------------------------------------------
 * file rpp_filt_regs_addr_map.h
 *
 * Programming Language: C
 * Runtime Environment : ARM, WIN32, Linux, Solaris
 *
 * Description:
 *  generate full address map based on external definition of *_BASE macro
 */
/*****************************************************************************/

#ifndef __RPP_FILT_REGS_ADDR_MAP_H__
#define __RPP_FILT_REGS_ADDR_MAP_H__


#ifndef RPP_FILT_BASE
#error You must #include the global memory map header file before rpp_filt_regs_addr_map.h and it must define RPP_FILT_BASE
#endif /* RPP_FILT_BASE */



// register group: isp_filter_module_registers
#define RPP_RPP_FILT_VERSION_REG (RPP_FILT_BASE + 0x00000000)
#define RPP_RPP_DEMOSAIC_REG (RPP_FILT_BASE + 0x00000004)
#define RPP_RPP_FILT_MODE_REG (RPP_FILT_BASE + 0x00000008)
#define RPP_RPP_FILT_THRESH_BL0_REG (RPP_FILT_BASE + 0x0000000C)
#define RPP_RPP_FILT_THRESH_BL1_REG (RPP_FILT_BASE + 0x00000010)
#define RPP_RPP_FILT_THRESH_SH0_REG (RPP_FILT_BASE + 0x00000014)
#define RPP_RPP_FILT_THRESH_SH1_REG (RPP_FILT_BASE + 0x00000018)
#define RPP_RPP_FILT_LUM_WEIGHT_REG (RPP_FILT_BASE + 0x0000001C)
#define RPP_RPP_FILT_FAC_SH1_REG (RPP_FILT_BASE + 0x00000020)
#define RPP_RPP_FILT_FAC_SH0_REG (RPP_FILT_BASE + 0x00000024)
#define RPP_RPP_FILT_FAC_MID_REG (RPP_FILT_BASE + 0x00000028)
#define RPP_RPP_FILT_FAC_BL0_REG (RPP_FILT_BASE + 0x0000002C)
#define RPP_RPP_FILT_FAC_BL1_REG (RPP_FILT_BASE + 0x00000030)
#define RPP_RPP_CAC_VERSION_REG (RPP_FILT_BASE + 0x00000080)
#define RPP_RPP_CAC_CTRL_REG (RPP_FILT_BASE + 0x00000084)
#define RPP_RPP_CAC_COUNT_START_REG (RPP_FILT_BASE + 0x00000088)
#define RPP_RPP_CAC_A_REG (RPP_FILT_BASE + 0x0000008C)
#define RPP_RPP_CAC_B_REG (RPP_FILT_BASE + 0x00000090)
#define RPP_RPP_CAC_C_REG (RPP_FILT_BASE + 0x00000094)
#define RPP_RPP_CAC_X_NORM_REG (RPP_FILT_BASE + 0x00000098)
#define RPP_RPP_CAC_Y_NORM_REG (RPP_FILT_BASE + 0x0000009C)

// last index
#define RPP_FILT_REGIDX_LAST      (RPP_FILT_BASE + 0x000000A0)
#define RPP_FILT_REGIDX_LAST_OFFS 0x000000A0

#endif /* __RPP_FILT_REGS_ADDR_MAP_H__ */ 
/*************************** EOF **************************************/
