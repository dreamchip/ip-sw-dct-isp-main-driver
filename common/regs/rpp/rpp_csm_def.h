/******************************************************************************
 *
 * Copyright 2019 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_csm_def.h
 *
 * @brief   Default colorspace conversion matrix definitions.
 *
 *****************************************************************************/
#ifndef __RPP_CSM_DEF_H__
#define __RPP_CSM_DEF_H__

#ifdef __cplusplus
extern "C" {
#endif

#define SET_CSM_OFF(vec, i, val)    (vec)[i] = (val)
#define SET_CSM_COEFF(vec, i, val)  (vec)[i] = (val)


/**************************************************************************//**
 * @def SET_OFF_BYPASS(f)
 * This Macro defines the offset vector for a bypass.
 *****************************************************************************/
#define SET_OFF_BYPASS(f)           \
    SET_CSM_OFF(f, 0, 0);           \
    SET_CSM_OFF(f, 1, 0);           \
    SET_CSM_OFF(f, 2, 0)

/**************************************************************************//**
 * @def SET_OFF_RGB_2_YUV(f, colorbits)
 * This Macro defines the offset vector for any RGB to YUV conversion.
 *****************************************************************************/
#define SET_OFF_RGB_2_YUV(f, colorbits)         \
    SET_CSM_OFF(f, 0, 0);                       \
    SET_CSM_OFF(f, 1, (1 << (colorbits - 1)));  \
    SET_CSM_OFF(f, 2, (1 << (colorbits - 1)))

/**************************************************************************//**
 * @def SET_OFF_YUV_2_RGB(f, colorbits)
 * This Macro defines the offset vector for any YUV to RGB conversion.
 *
 * This offset vector need to be applied before matrix multiplication,
 * which requires that the CCOR unit is correspondingly hardware-configured.
 *****************************************************************************/
#define SET_OFF_YUV_2_RGB(f, colorbits)         \
    SET_CSM_OFF(f, 0, 0);                       \
    SET_CSM_OFF(f, 1, -(1 << (colorbits - 1))); \
    SET_CSM_OFF(f, 2, -(1 << (colorbits - 1)))

/**************************************************************************//**
 * @def SET_CSM_BYPASS(f)
 * This Macro defines conversion coefficients and offset for a bypass.
 *****************************************************************************/
#define SET_CSM_BYPASS(f)           \
    SET_CSM_COEFF(f, 0, 4096 );     \
    SET_CSM_COEFF(f, 1,    0 );     \
    SET_CSM_COEFF(f, 2,    0 );     \
    SET_CSM_COEFF(f, 3,    0 );     \
    SET_CSM_COEFF(f, 4, 4096 );     \
    SET_CSM_COEFF(f, 5,    0 );     \
    SET_CSM_COEFF(f, 6,    0 );     \
    SET_CSM_COEFF(f, 7,    0 );     \
    SET_CSM_COEFF(f, 8, 4096 )

/**************************************************************************//**
 * @def SET_CSM_BT601_RGB_2_YUV(f)
 * This Macro defines conversion coefficients for a BT.601 RGB
 * to YUV conversion. 
 *****************************************************************************/
#define SET_CSM_BT601_RGB_2_YUV(f)      \
    SET_CSM_COEFF(f, 0,  1225 );        \
    SET_CSM_COEFF(f, 1,  2404 );        \
    SET_CSM_COEFF(f, 2,   467 );        \
    SET_CSM_COEFF(f, 3,  -691 );        \
    SET_CSM_COEFF(f, 4, -1357 );        \
    SET_CSM_COEFF(f, 5,  2048 );        \
    SET_CSM_COEFF(f, 6,  2048 );        \
    SET_CSM_COEFF(f, 7, -1715 );        \
    SET_CSM_COEFF(f, 8,  -333 )

/**************************************************************************//**
 * @def SET_CSM_BT601_RGB_2_YVU(f)
 * This Macro defines conversion coefficients for a BT.601 RGB
 * to YVU conversion. This is the same as RGB to YUV, with matrix
 * lines 2/3 exchanged.
 *****************************************************************************/
#define SET_CSM_BT601_RGB_2_YVU(f)      \
    SET_CSM_COEFF(f, 0,  1225 );        \
    SET_CSM_COEFF(f, 1,  2404 );        \
    SET_CSM_COEFF(f, 2,   467 );        \
    SET_CSM_COEFF(f, 6,  -691 );        \
    SET_CSM_COEFF(f, 7, -1357 );        \
    SET_CSM_COEFF(f, 8,  2048 );        \
    SET_CSM_COEFF(f, 3,  2048 );        \
    SET_CSM_COEFF(f, 4, -1715 );        \
    SET_CSM_COEFF(f, 5,  -333 )

/**************************************************************************//**
 * @def SET_CSM_BT709_RGB_2_YUV(f)
 * This Macro defines conversion coefficients for a BT.709 RGB
 * to YUV conversion. 
 *****************************************************************************/
#define SET_CSM_BT709_RGB_2_YUV(f)      \
    SET_CSM_COEFF(f, 0,   871 );        \
    SET_CSM_COEFF(f, 1,  2929 );        \
    SET_CSM_COEFF(f, 2,   296 );        \
    SET_CSM_COEFF(f, 3,  -469 );        \
    SET_CSM_COEFF(f, 4, -1579 );        \
    SET_CSM_COEFF(f, 5,  2048 );        \
    SET_CSM_COEFF(f, 6,  2048 );        \
    SET_CSM_COEFF(f, 7, -1860 );        \
    SET_CSM_COEFF(f, 8,  -188 )

/**************************************************************************//**
 * @def SET_CSM_BT709_RGB_2_YVU(f)
 * This Macro defines conversion coefficients for a BT.709 RGB
 * to YUV conversion. This is the same as RGB to YUV, with matrix
 * lines 2/3 exchanged.
 *****************************************************************************/
#define SET_CSM_BT709_RGB_2_YVU(f)      \
    SET_CSM_COEFF(f, 0,   871 );        \
    SET_CSM_COEFF(f, 1,  2929 );        \
    SET_CSM_COEFF(f, 2,   296 );        \
    SET_CSM_COEFF(f, 6,  -469 );        \
    SET_CSM_COEFF(f, 7, -1579 );        \
    SET_CSM_COEFF(f, 8,  2048 );        \
    SET_CSM_COEFF(f, 3,  2048 );        \
    SET_CSM_COEFF(f, 4, -1860 );        \
    SET_CSM_COEFF(f, 5,  -188 )

/**************************************************************************//**
 * @def SET_CSM_YUV_2_BT601_RGB(f)
 * This Macro defines conversion coefficients for a YUV to BT.601 RGB
 * conversion. 
 *****************************************************************************/
#define SET_CSM_YUV_2_BT601_RGB(f)      \
    SET_CSM_COEFF(f, 0,  4096 );        \
    SET_CSM_COEFF(f, 1,     0 );        \
    SET_CSM_COEFF(f, 2,  5743 );        \
    SET_CSM_COEFF(f, 3,  4096 );        \
    SET_CSM_COEFF(f, 4, -1410 );        \
    SET_CSM_COEFF(f, 5, -2925 );        \
    SET_CSM_COEFF(f, 6,  4096 );        \
    SET_CSM_COEFF(f, 7,  7258 );        \
    SET_CSM_COEFF(f, 8,     0 )

/**************************************************************************//**
 * @def SET_CSM_YVU_2_BT601_RGB(f)
 * This Macro defines conversion coefficients for a YUV to BT.601 RGB
 * conversion. This is the same as RGB to YUV, with matrix
 * columns 2/3 exchanged.
 *****************************************************************************/
#define SET_CSM_YVU_2_BT601_RGB(f)      \
    SET_CSM_COEFF(f, 0,  4096 );        \
    SET_CSM_COEFF(f, 2,     0 );        \
    SET_CSM_COEFF(f, 1,  5743 );        \
    SET_CSM_COEFF(f, 3,  4096 );        \
    SET_CSM_COEFF(f, 5, -1410 );        \
    SET_CSM_COEFF(f, 4, -2925 );        \
    SET_CSM_COEFF(f, 6,  4096 );        \
    SET_CSM_COEFF(f, 8,  7258 );        \
    SET_CSM_COEFF(f, 7,     0 )


/**************************************************************************//**
 * @def SET_CSM_YUV_2_BT709_RGB(f)
 * This Macro defines conversion coefficients for a YUV to BT.709 RGB
 * conversion. 
 *****************************************************************************/
#define SET_CSM_YUV_2_BT709_RGB(f)      \
    SET_CSM_COEFF(f, 0,  4096 );        \
    SET_CSM_COEFF(f, 1,     0 );        \
    SET_CSM_COEFF(f, 2,  6450 );        \
    SET_CSM_COEFF(f, 3,  4096 );        \
    SET_CSM_COEFF(f, 4,  -767 );        \
    SET_CSM_COEFF(f, 5, -1917 );        \
    SET_CSM_COEFF(f, 6,  4096 );        \
    SET_CSM_COEFF(f, 7,  7601 );        \
    SET_CSM_COEFF(f, 8,     0 )


/**************************************************************************//**
 * @def SET_CSM_YVU_2_BT709_RGB(f)
 * This Macro defines conversion coefficients for a YVU to BT.709 RGB
 * conversion. This is the same as RGB to YUV, with matrix
 * columns 2/3 exchanged.
 *****************************************************************************/
#define SET_CSM_YVU_2_BT709_RGB(f)      \
    SET_CSM_COEFF(f, 0,  4096 );        \
    SET_CSM_COEFF(f, 2,     0 );        \
    SET_CSM_COEFF(f, 1,  6450 );        \
    SET_CSM_COEFF(f, 3,  4096 );        \
    SET_CSM_COEFF(f, 5,  -767 );        \
    SET_CSM_COEFF(f, 4, -1917 );        \
    SET_CSM_COEFF(f, 6,  4096 );        \
    SET_CSM_COEFF(f, 8,  7601 );        \
    SET_CSM_COEFF(f, 7,     0 )

/**************************************************************************//**
 * @def SET_CSM_RGB_2_XYZ(f)
 * This Macro defines conversion coefficients for a RGB to CIE XYZ
 * conversion. 
 *****************************************************************************/
#define SET_CSM_RGB_2_XYZ(f)            \
    SET_CSM_COEFF(f, 0,  1689 );        \
    SET_CSM_COEFF(f, 1,  1464 );        \
    SET_CSM_COEFF(f, 2,   739 );        \
    SET_CSM_COEFF(f, 3,   870 );        \
    SET_CSM_COEFF(f, 4,  2929 );        \
    SET_CSM_COEFF(f, 5,   295 );        \
    SET_CSM_COEFF(f, 6,   244 );        \
    SET_CSM_COEFF(f, 7,   488 );        \
    SET_CSM_COEFF(f, 8,  3893 )

/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_CSM_DEF_H__ */

