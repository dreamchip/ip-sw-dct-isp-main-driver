/**
//-----------------------------------------------------------------------------
// This is an unpublished work, the copyright in which vests in 
// DreamChip Technologies GmbH. The information contained herein is the property 
// of DreamChip Technologies GmbH and is supplied without liability for errors or 
// omissions. No part may be reproduced or used except as authorized by 
// contract or other written permission.
// Copyright(c) DreamChip Technologies GmbH, 2022. All rights reserved.
//-----------------------------------------------------------------------------
//
//       N-1         _ __             _
//        __        |  ||  /    1 \    |
//  X  = \    x  cos|  -- ( n + -  ) k |   k = 0,...,N-1
//   k   /__   n    |_  N  \    2 /   _|
//       n=0
//
//-----------------------------------------------------------------------------
// Module   : rpp_dpcc
//
// Purpose  : Register C header file
//
// Creator  : DCT
//
// Comments : automatically generated by sig (4.0.5)
//            !!!!! DO NOT EDIT THIS FILE !!!!!
//-----------------------------------------------------------------------------
* @file rpp_dpcc_regs_mask.h
*
* <pre>
*
* Description:
*   This header file exports the module register structure and masks. 
*   It should not be included directly by your driver/application, it will be  
*   exported by the top level header file. 
*
* </pre>
*/
/*****************************************************************************/

#ifndef __RPP_DPCC_REGS_MASK_H__
#define __RPP_DPCC_REGS_MASK_H__


// - MASK AND SHIFT MACROS ----------------------------------------------------------

//! Register Group: isp_dpcc_module_registers

//! Register: rpp_dpcc_version: DPCC Version (RPP_DPCC_BASE + 0x00000000)
//! Slice: dpcc_version:
//! version number for DPCC module 
// 0x01: initial version, 12 bit 
// 0x02: register naming partly reworked, 12 bit 
// 0x03: 24 bit version 
// 0x04: 12 bit, adapt reset values for RO_LIMITS and RND_OFFS 
// 0x05: 24 bit version 
// 0x06: 12 bit, 64MP images 
// 0x07: 24 bit, 64MP images
#define RPP_DPCC_DPCC_VERSION
#define RPP_DPCC_DPCC_VERSION_MASK 0x000000FFU
#define RPP_DPCC_DPCC_VERSION_SHIFT 0U

//! Register: rpp_dpcc_mode: Mode control for DPCC detection unit (RPP_DPCC_BASE + 0x00000004)
//! Slice: stage1_enable:
//! 1: enable  stage1 *Default*
// 0: bypass  stage1
#define RPP_DPCC_STAGE1_ENABLE
#define RPP_DPCC_STAGE1_ENABLE_MASK 0x00000004U
#define RPP_DPCC_STAGE1_ENABLE_SHIFT 2U
//! Slice: grayscale_mode:
//! 1: enable gray scale data input from black and white sensors (without color filter array) 
// 0: BAYER DATA INPUT  *Default*
#define RPP_DPCC_GRAYSCALE_MODE
#define RPP_DPCC_GRAYSCALE_MODE_MASK 0x00000002U
#define RPP_DPCC_GRAYSCALE_MODE_SHIFT 1U
//! Slice: dpcc_enable:
//! 1: enable DPCC 
// 0: bypass DPCC  *Default*
#define RPP_DPCC_DPCC_ENABLE
#define RPP_DPCC_DPCC_ENABLE_MASK 0x00000001U
#define RPP_DPCC_DPCC_ENABLE_SHIFT 0U

//! Register: rpp_dpcc_output_mode: Interpolation mode for correction unit (RPP_DPCC_BASE + 0x00000008)
//! Slice: stage1_rb_3x3:
//! 1: stage1 red/blue 9 pixel (3x3) output median  
// 0: stage1 red/blue 4 or 5 pixel output median  *Default*
#define RPP_DPCC_STAGE1_RB_3X3
#define RPP_DPCC_STAGE1_RB_3X3_MASK 0x00000008U
#define RPP_DPCC_STAGE1_RB_3X3_SHIFT 3U
//! Slice: stage1_g_3x3:
//! 1: stage1 green 9 pixel (3x3) output median  
// 0: stage1 green 4 or 5 pixel output median  *Default*
#define RPP_DPCC_STAGE1_G_3X3
#define RPP_DPCC_STAGE1_G_3X3_MASK 0x00000004U
#define RPP_DPCC_STAGE1_G_3X3_SHIFT 2U
//! Slice: stage1_incl_rb_center:
//! 1: stage1 include center pixel for red/blue output median 2x2+1 
//0: stage1 do not include center pixel for red/blue output median 2x2 *Default*
#define RPP_DPCC_STAGE1_INCL_RB_CENTER
#define RPP_DPCC_STAGE1_INCL_RB_CENTER_MASK 0x00000002U
#define RPP_DPCC_STAGE1_INCL_RB_CENTER_SHIFT 1U
//! Slice: stage1_incl_green_center:
//! 1: stage1 include center pixel for green output median 2x2+1 
// 0: stage1 do not include center pixel for green output median 2x2 *Default*
#define RPP_DPCC_STAGE1_INCL_GREEN_CENTER
#define RPP_DPCC_STAGE1_INCL_GREEN_CENTER_MASK 0x00000001U
#define RPP_DPCC_STAGE1_INCL_GREEN_CENTER_SHIFT 0U

//! Register: rpp_dpcc_set_use: DPCC methods set usage for detection (RPP_DPCC_BASE + 0x0000000c)
//! Slice: stage1_use_fix_set:
//! 1: stage1 use hard coded methods set *Default* 
// 0: stage1 do not use hard coded methods set
#define RPP_DPCC_STAGE1_USE_FIX_SET
#define RPP_DPCC_STAGE1_USE_FIX_SET_MASK 0x00000008U
#define RPP_DPCC_STAGE1_USE_FIX_SET_SHIFT 3U
//! Slice: stage1_use_set_3:
//! 1: stage1 use methods set 3  
// 0: stage1 do not use methods set 3 *Default*
#define RPP_DPCC_STAGE1_USE_SET_3
#define RPP_DPCC_STAGE1_USE_SET_3_MASK 0x00000004U
#define RPP_DPCC_STAGE1_USE_SET_3_SHIFT 2U
//! Slice: stage1_use_set_2:
//! 1: stage1 use methods set 2  
// 0: stage1 do not use methods set 2 *Default*
#define RPP_DPCC_STAGE1_USE_SET_2
#define RPP_DPCC_STAGE1_USE_SET_2_MASK 0x00000002U
#define RPP_DPCC_STAGE1_USE_SET_2_SHIFT 1U
//! Slice: stage1_use_set_1:
//! 1: stage1 use methods set 1  *Default* 
// 0: stage1 do not use methods set 1
#define RPP_DPCC_STAGE1_USE_SET_1
#define RPP_DPCC_STAGE1_USE_SET_1_MASK 0x00000001U
#define RPP_DPCC_STAGE1_USE_SET_1_SHIFT 0U

//! Register: rpp_dpcc_methods_set_1: Methods enable bits for SET_1 (RPP_DPCC_BASE + 0x00000010)
//! Slice: rg_red_blue1_enable:
//! 1: enable Rank Gradient check for red_blue  *Default* 
// 0: bypass Rank Gradient check for red_blue
#define RPP_DPCC_RG_RED_BLUE1_ENABLE
#define RPP_DPCC_RG_RED_BLUE1_ENABLE_MASK 0x00001000U
#define RPP_DPCC_RG_RED_BLUE1_ENABLE_SHIFT 12U
//! Slice: rnd_red_blue1_enable:
//! 1: enable Rank Neighbor Difference check for red_blue  *Default* 
// 0: bypass Rank Neighbor Difference check for red_blue
#define RPP_DPCC_RND_RED_BLUE1_ENABLE
#define RPP_DPCC_RND_RED_BLUE1_ENABLE_MASK 0x00000800U
#define RPP_DPCC_RND_RED_BLUE1_ENABLE_SHIFT 11U
//! Slice: ro_red_blue1_enable:
//! 1: enable Rank Order check for red_blue  *Default* 
// 0: bypass Rank Order check for red_blue
#define RPP_DPCC_RO_RED_BLUE1_ENABLE
#define RPP_DPCC_RO_RED_BLUE1_ENABLE_MASK 0x00000400U
#define RPP_DPCC_RO_RED_BLUE1_ENABLE_SHIFT 10U
//! Slice: lc_red_blue1_enable:
//! 1: enable Line check for red_blue *Default*  
// 0: bypass Line check for red_blue
#define RPP_DPCC_LC_RED_BLUE1_ENABLE
#define RPP_DPCC_LC_RED_BLUE1_ENABLE_MASK 0x00000200U
#define RPP_DPCC_LC_RED_BLUE1_ENABLE_SHIFT 9U
//! Slice: pg_red_blue1_enable:
//! 1: enable Peak Gradient check for red_blue  *Default* 
// 0: bypass Peak Gradient check for red_blue
#define RPP_DPCC_PG_RED_BLUE1_ENABLE
#define RPP_DPCC_PG_RED_BLUE1_ENABLE_MASK 0x00000100U
#define RPP_DPCC_PG_RED_BLUE1_ENABLE_SHIFT 8U
//! Slice: rg_green1_enable:
//! 1: enable Rank Gradient check for green  *Default* 
// 0: bypass Rank Gradient check for green
#define RPP_DPCC_RG_GREEN1_ENABLE
#define RPP_DPCC_RG_GREEN1_ENABLE_MASK 0x00000010U
#define RPP_DPCC_RG_GREEN1_ENABLE_SHIFT 4U
//! Slice: rnd_green1_enable:
//! 1: enable Rank Neighbor Difference check for green  *Default* 
// 0: bypass Rank Neighbor Difference check for green
#define RPP_DPCC_RND_GREEN1_ENABLE
#define RPP_DPCC_RND_GREEN1_ENABLE_MASK 0x00000008U
#define RPP_DPCC_RND_GREEN1_ENABLE_SHIFT 3U
//! Slice: ro_green1_enable:
//! 1: enable Rank Order check for green  *Default* 
// 0: bypass Rank Order check for green
#define RPP_DPCC_RO_GREEN1_ENABLE
#define RPP_DPCC_RO_GREEN1_ENABLE_MASK 0x00000004U
#define RPP_DPCC_RO_GREEN1_ENABLE_SHIFT 2U
//! Slice: lc_green1_enable:
//! 1: enable Line check for green *Default*  
// 0: bypass Line check for green
#define RPP_DPCC_LC_GREEN1_ENABLE
#define RPP_DPCC_LC_GREEN1_ENABLE_MASK 0x00000002U
#define RPP_DPCC_LC_GREEN1_ENABLE_SHIFT 1U
//! Slice: pg_green1_enable:
//! 1: enable Peak Gradient check for green  *Default* 
// 0: bypass Peak Gradient check for green
#define RPP_DPCC_PG_GREEN1_ENABLE
#define RPP_DPCC_PG_GREEN1_ENABLE_MASK 0x00000001U
#define RPP_DPCC_PG_GREEN1_ENABLE_SHIFT 0U

//! Register: rpp_dpcc_methods_set_2: Methods enable bits for SET_2 (RPP_DPCC_BASE + 0x00000014)
//! Slice: rg_red_blue2_enable:
//! 1: enable Rank Gradient check for red_blue  *Default* 
// 0: bypass Rank Gradient check for red_blue
#define RPP_DPCC_RG_RED_BLUE2_ENABLE
#define RPP_DPCC_RG_RED_BLUE2_ENABLE_MASK 0x00001000U
#define RPP_DPCC_RG_RED_BLUE2_ENABLE_SHIFT 12U
//! Slice: rnd_red_blue2_enable:
//! 1: enable Rank Neighbor Difference check for red_blue  *Default* 
// 0: bypass Rank Neighbor Difference check for red_blue
#define RPP_DPCC_RND_RED_BLUE2_ENABLE
#define RPP_DPCC_RND_RED_BLUE2_ENABLE_MASK 0x00000800U
#define RPP_DPCC_RND_RED_BLUE2_ENABLE_SHIFT 11U
//! Slice: ro_red_blue2_enable:
//! 1: enable Rank Order check for red_blue  *Default* 
// 0: bypass Rank Order check for red_blue
#define RPP_DPCC_RO_RED_BLUE2_ENABLE
#define RPP_DPCC_RO_RED_BLUE2_ENABLE_MASK 0x00000400U
#define RPP_DPCC_RO_RED_BLUE2_ENABLE_SHIFT 10U
//! Slice: lc_red_blue2_enable:
//! 1: enable Line check for red_blue 
// 0: bypass Line check for red_blue  *Default*
#define RPP_DPCC_LC_RED_BLUE2_ENABLE
#define RPP_DPCC_LC_RED_BLUE2_ENABLE_MASK 0x00000200U
#define RPP_DPCC_LC_RED_BLUE2_ENABLE_SHIFT 9U
//! Slice: pg_red_blue2_enable:
//! 1: enable Peak Gradient check for red_blue  *Default* 
// 0: bypass Peak Gradient check for red_blue
#define RPP_DPCC_PG_RED_BLUE2_ENABLE
#define RPP_DPCC_PG_RED_BLUE2_ENABLE_MASK 0x00000100U
#define RPP_DPCC_PG_RED_BLUE2_ENABLE_SHIFT 8U
//! Slice: rg_green2_enable:
//! 1: enable Rank Gradient check for green  *Default* 
// 0: bypass Rank Gradient check for green
#define RPP_DPCC_RG_GREEN2_ENABLE
#define RPP_DPCC_RG_GREEN2_ENABLE_MASK 0x00000010U
#define RPP_DPCC_RG_GREEN2_ENABLE_SHIFT 4U
//! Slice: rnd_green2_enable:
//! 1: enable Rank Neighbor Difference check for green  *Default* 
// 0: bypass Rank Neighbor Difference check for green
#define RPP_DPCC_RND_GREEN2_ENABLE
#define RPP_DPCC_RND_GREEN2_ENABLE_MASK 0x00000008U
#define RPP_DPCC_RND_GREEN2_ENABLE_SHIFT 3U
//! Slice: ro_green2_enable:
//! 1: enable Rank Order check for green  *Default* 
// 0: bypass Rank Order check for green
#define RPP_DPCC_RO_GREEN2_ENABLE
#define RPP_DPCC_RO_GREEN2_ENABLE_MASK 0x00000004U
#define RPP_DPCC_RO_GREEN2_ENABLE_SHIFT 2U
//! Slice: lc_green2_enable:
//! 1: enable Line check for green 
// 0: bypass Line check for green  *Default*
#define RPP_DPCC_LC_GREEN2_ENABLE
#define RPP_DPCC_LC_GREEN2_ENABLE_MASK 0x00000002U
#define RPP_DPCC_LC_GREEN2_ENABLE_SHIFT 1U
//! Slice: pg_green2_enable:
//! 1: enable Peak Gradient check for green  *Default* 
// 0: bypass Peak Gradient check for green
#define RPP_DPCC_PG_GREEN2_ENABLE
#define RPP_DPCC_PG_GREEN2_ENABLE_MASK 0x00000001U
#define RPP_DPCC_PG_GREEN2_ENABLE_SHIFT 0U

//! Register: rpp_dpcc_methods_set_3: Methods enable bits for SET_3 (RPP_DPCC_BASE + 0x00000018)
//! Slice: rg_red_blue3_enable:
//! 1: enable Rank Gradient check for red_blue 
// 0: bypass Rank Gradient check for red_blue  *Default*
#define RPP_DPCC_RG_RED_BLUE3_ENABLE
#define RPP_DPCC_RG_RED_BLUE3_ENABLE_MASK 0x00001000U
#define RPP_DPCC_RG_RED_BLUE3_ENABLE_SHIFT 12U
//! Slice: rnd_red_blue3_enable:
//! 1: enable Rank Neighbor Difference check for red_blue 
// 0: bypass Rank Neighbor Difference check for red_blue  *Default*
#define RPP_DPCC_RND_RED_BLUE3_ENABLE
#define RPP_DPCC_RND_RED_BLUE3_ENABLE_MASK 0x00000800U
#define RPP_DPCC_RND_RED_BLUE3_ENABLE_SHIFT 11U
//! Slice: ro_red_blue3_enable:
//! 1: enable Rank Order check for red_blue  *Default* 
// 0: bypass Rank Order check for red_blue
#define RPP_DPCC_RO_RED_BLUE3_ENABLE
#define RPP_DPCC_RO_RED_BLUE3_ENABLE_MASK 0x00000400U
#define RPP_DPCC_RO_RED_BLUE3_ENABLE_SHIFT 10U
//! Slice: lc_red_blue3_enable:
//! 1: enable Line check for red_blue  *Default* 
// 0: bypass Line check for red_blue
#define RPP_DPCC_LC_RED_BLUE3_ENABLE
#define RPP_DPCC_LC_RED_BLUE3_ENABLE_MASK 0x00000200U
#define RPP_DPCC_LC_RED_BLUE3_ENABLE_SHIFT 9U
//! Slice: pg_red_blue3_enable:
//! 1: enable Peak Gradient check for red_blue  *Default* 
// 0: bypass Peak Gradient check for red_blue
#define RPP_DPCC_PG_RED_BLUE3_ENABLE
#define RPP_DPCC_PG_RED_BLUE3_ENABLE_MASK 0x00000100U
#define RPP_DPCC_PG_RED_BLUE3_ENABLE_SHIFT 8U
//! Slice: rg_green3_enable:
//! 1: enable Rank Gradient check for green 
// 0: bypass Rank Gradient check for green  *Default*
#define RPP_DPCC_RG_GREEN3_ENABLE
#define RPP_DPCC_RG_GREEN3_ENABLE_MASK 0x00000010U
#define RPP_DPCC_RG_GREEN3_ENABLE_SHIFT 4U
//! Slice: rnd_green3_enable:
//! 1: enable Rank Neighbor Difference check for green 
// 0: bypass Rank Neighbor Difference check for green  *Default*
#define RPP_DPCC_RND_GREEN3_ENABLE
#define RPP_DPCC_RND_GREEN3_ENABLE_MASK 0x00000008U
#define RPP_DPCC_RND_GREEN3_ENABLE_SHIFT 3U
//! Slice: ro_green3_enable:
//! 1: enable Rank Order check for green  *Default* 
// 0: bypass Rank Order check for green
#define RPP_DPCC_RO_GREEN3_ENABLE
#define RPP_DPCC_RO_GREEN3_ENABLE_MASK 0x00000004U
#define RPP_DPCC_RO_GREEN3_ENABLE_SHIFT 2U
//! Slice: lc_green3_enable:
//! 1: enable Line check for green  *Default* 
// 0: bypass Line check for green
#define RPP_DPCC_LC_GREEN3_ENABLE
#define RPP_DPCC_LC_GREEN3_ENABLE_MASK 0x00000002U
#define RPP_DPCC_LC_GREEN3_ENABLE_SHIFT 1U
//! Slice: pg_green3_enable:
//! 1: enable Peak Gradient check for green  *Default* 
// 0: bypass Peak Gradient check for green
#define RPP_DPCC_PG_GREEN3_ENABLE
#define RPP_DPCC_PG_GREEN3_ENABLE_MASK 0x00000001U
#define RPP_DPCC_PG_GREEN3_ENABLE_SHIFT 0U

//! Register: rpp_dpcc_line_thresh_1: Line threshold SET_1 (RPP_DPCC_BASE + 0x0000001c)
//! Slice: line_thr_1_rb:
//! line threshold for set 1 red/blue
#define RPP_DPCC_LINE_THR_1_RB
#define RPP_DPCC_LINE_THR_1_RB_MASK 0x0000FF00U
#define RPP_DPCC_LINE_THR_1_RB_SHIFT 8U
//! Slice: line_thr_1_g:
//! line threshold for set 1 green
#define RPP_DPCC_LINE_THR_1_G
#define RPP_DPCC_LINE_THR_1_G_MASK 0x000000FFU
#define RPP_DPCC_LINE_THR_1_G_SHIFT 0U

//! Register: rpp_dpcc_line_mad_fac_1: Mean Absolute Difference (MAD) factor for Line check set 1 (RPP_DPCC_BASE + 0x00000020)
//! Slice: line_mad_fac_1_rb:
//! line MAD factor for set 1 red/blue
#define RPP_DPCC_LINE_MAD_FAC_1_RB
#define RPP_DPCC_LINE_MAD_FAC_1_RB_MASK 0x00003F00U
#define RPP_DPCC_LINE_MAD_FAC_1_RB_SHIFT 8U
//! Slice: line_mad_fac_1_g:
//! line MAD factor for set 1 green
#define RPP_DPCC_LINE_MAD_FAC_1_G
#define RPP_DPCC_LINE_MAD_FAC_1_G_MASK 0x0000003FU
#define RPP_DPCC_LINE_MAD_FAC_1_G_SHIFT 0U

//! Register: rpp_dpcc_pg_fac_1: Peak gradient factor for set 1 (RPP_DPCC_BASE + 0x00000024)
//! Slice: pg_fac_1_rb:
//! Peak gradient factor for set 1 red/blue
#define RPP_DPCC_PG_FAC_1_RB
#define RPP_DPCC_PG_FAC_1_RB_MASK 0x00003F00U
#define RPP_DPCC_PG_FAC_1_RB_SHIFT 8U
//! Slice: pg_fac_1_g:
//! Peak gradient factor for set 1 green
#define RPP_DPCC_PG_FAC_1_G
#define RPP_DPCC_PG_FAC_1_G_MASK 0x0000003FU
#define RPP_DPCC_PG_FAC_1_G_SHIFT 0U

//! Register: rpp_dpcc_rnd_thresh_1: Rank Neighbor Difference threshold for set 1 (RPP_DPCC_BASE + 0x00000028)
//! Slice: rnd_thr_1_rb:
//! Rank Neighbor Difference threshold for set 1 red/blue
#define RPP_DPCC_RND_THR_1_RB
#define RPP_DPCC_RND_THR_1_RB_MASK 0x0000FF00U
#define RPP_DPCC_RND_THR_1_RB_SHIFT 8U
//! Slice: rnd_thr_1_g:
//! Rank Neighbor Difference threshold for set 1 green
#define RPP_DPCC_RND_THR_1_G
#define RPP_DPCC_RND_THR_1_G_MASK 0x000000FFU
#define RPP_DPCC_RND_THR_1_G_SHIFT 0U

//! Register: rpp_dpcc_rg_fac_1: Rank gradient factor for set 1 (RPP_DPCC_BASE + 0x0000002c)
//! Slice: rg_fac_1_rb:
//! Rank gradient factor for set 1 red/blue
#define RPP_DPCC_RG_FAC_1_RB
#define RPP_DPCC_RG_FAC_1_RB_MASK 0x00003F00U
#define RPP_DPCC_RG_FAC_1_RB_SHIFT 8U
//! Slice: rg_fac_1_g:
//! Rank gradient factor for set 1 green
#define RPP_DPCC_RG_FAC_1_G
#define RPP_DPCC_RG_FAC_1_G_MASK 0x0000003FU
#define RPP_DPCC_RG_FAC_1_G_SHIFT 0U

//! Register: rpp_dpcc_line_thresh_2: Line threshold set 2 (RPP_DPCC_BASE + 0x00000030)
//! Slice: line_thr_2_rb:
//! line threshold for set 2 red/blue
#define RPP_DPCC_LINE_THR_2_RB
#define RPP_DPCC_LINE_THR_2_RB_MASK 0x0000FF00U
#define RPP_DPCC_LINE_THR_2_RB_SHIFT 8U
//! Slice: line_thr_2_g:
//! line threshold for set 2 green
#define RPP_DPCC_LINE_THR_2_G
#define RPP_DPCC_LINE_THR_2_G_MASK 0x000000FFU
#define RPP_DPCC_LINE_THR_2_G_SHIFT 0U

//! Register: rpp_dpcc_line_mad_fac_2: Mean Absolute Difference (MAD) factor for Line check set 2 (RPP_DPCC_BASE + 0x00000034)
//! Slice: line_mad_fac_2_rb:
//! line MAD factor for set 2 red/blue
#define RPP_DPCC_LINE_MAD_FAC_2_RB
#define RPP_DPCC_LINE_MAD_FAC_2_RB_MASK 0x00003F00U
#define RPP_DPCC_LINE_MAD_FAC_2_RB_SHIFT 8U
//! Slice: line_mad_fac_2_g:
//! line MAD factor for set 2 green
#define RPP_DPCC_LINE_MAD_FAC_2_G
#define RPP_DPCC_LINE_MAD_FAC_2_G_MASK 0x0000003FU
#define RPP_DPCC_LINE_MAD_FAC_2_G_SHIFT 0U

//! Register: rpp_dpcc_pg_fac_2: Peak gradient factor for set 2 (RPP_DPCC_BASE + 0x00000038)
//! Slice: pg_fac_2_rb:
//! Peak gradient factor for set 2 red/blue
#define RPP_DPCC_PG_FAC_2_RB
#define RPP_DPCC_PG_FAC_2_RB_MASK 0x00003F00U
#define RPP_DPCC_PG_FAC_2_RB_SHIFT 8U
//! Slice: pg_fac_2_g:
//! Peak gradient factor for set 2 green
#define RPP_DPCC_PG_FAC_2_G
#define RPP_DPCC_PG_FAC_2_G_MASK 0x0000003FU
#define RPP_DPCC_PG_FAC_2_G_SHIFT 0U

//! Register: rpp_dpcc_rnd_thresh_2: Rank Neighbor Difference threshold for set 2 (RPP_DPCC_BASE + 0x0000003c)
//! Slice: rnd_thr_2_rb:
//! Rank Neighbor Difference threshold for set 2 red/blue
#define RPP_DPCC_RND_THR_2_RB
#define RPP_DPCC_RND_THR_2_RB_MASK 0x0000FF00U
#define RPP_DPCC_RND_THR_2_RB_SHIFT 8U
//! Slice: rnd_thr_2_g:
//! Rank Neighbor Difference threshold for set 2 green
#define RPP_DPCC_RND_THR_2_G
#define RPP_DPCC_RND_THR_2_G_MASK 0x000000FFU
#define RPP_DPCC_RND_THR_2_G_SHIFT 0U

//! Register: rpp_dpcc_rg_fac_2: Rank gradient factor for set 2 (RPP_DPCC_BASE + 0x00000040)
//! Slice: rg_fac_2_rb:
//! Rank gradient factor for set 2 red/blue
#define RPP_DPCC_RG_FAC_2_RB
#define RPP_DPCC_RG_FAC_2_RB_MASK 0x00003F00U
#define RPP_DPCC_RG_FAC_2_RB_SHIFT 8U
//! Slice: rg_fac_2_g:
//! Rank gradient factor for set 2 green
#define RPP_DPCC_RG_FAC_2_G
#define RPP_DPCC_RG_FAC_2_G_MASK 0x0000003FU
#define RPP_DPCC_RG_FAC_2_G_SHIFT 0U

//! Register: rpp_dpcc_line_thresh_3: Line threshold set 3 (RPP_DPCC_BASE + 0x00000044)
//! Slice: line_thr_3_rb:
//! line threshold for set 3 red/blue
#define RPP_DPCC_LINE_THR_3_RB
#define RPP_DPCC_LINE_THR_3_RB_MASK 0x0000FF00U
#define RPP_DPCC_LINE_THR_3_RB_SHIFT 8U
//! Slice: line_thr_3_g:
//! line threshold for set 3 green
#define RPP_DPCC_LINE_THR_3_G
#define RPP_DPCC_LINE_THR_3_G_MASK 0x000000FFU
#define RPP_DPCC_LINE_THR_3_G_SHIFT 0U

//! Register: rpp_dpcc_line_mad_fac_3: Mean Absolute Difference (MAD) factor for Line check set 3 (RPP_DPCC_BASE + 0x00000048)
//! Slice: line_mad_fac_3_rb:
//! line MAD factor for set 3 red/blue
#define RPP_DPCC_LINE_MAD_FAC_3_RB
#define RPP_DPCC_LINE_MAD_FAC_3_RB_MASK 0x00003F00U
#define RPP_DPCC_LINE_MAD_FAC_3_RB_SHIFT 8U
//! Slice: line_mad_fac_3_g:
//! line MAD factor for set 3 green
#define RPP_DPCC_LINE_MAD_FAC_3_G
#define RPP_DPCC_LINE_MAD_FAC_3_G_MASK 0x0000003FU
#define RPP_DPCC_LINE_MAD_FAC_3_G_SHIFT 0U

//! Register: rpp_dpcc_pg_fac_3: Peak gradient factor for set 3 (RPP_DPCC_BASE + 0x0000004c)
//! Slice: pg_fac_3_rb:
//! Peak gradient factor for set 3 red/blue
#define RPP_DPCC_PG_FAC_3_RB
#define RPP_DPCC_PG_FAC_3_RB_MASK 0x00003F00U
#define RPP_DPCC_PG_FAC_3_RB_SHIFT 8U
//! Slice: pg_fac_3_g:
//! Peak gradient factor for set 3 green
#define RPP_DPCC_PG_FAC_3_G
#define RPP_DPCC_PG_FAC_3_G_MASK 0x0000003FU
#define RPP_DPCC_PG_FAC_3_G_SHIFT 0U

//! Register: rpp_dpcc_rnd_thresh_3: Rank Neighbor Difference threshold for set 3 (RPP_DPCC_BASE + 0x00000050)
//! Slice: rnd_thr_3_rb:
//! Rank Neighbor Difference threshold for set 3 red/blue
#define RPP_DPCC_RND_THR_3_RB
#define RPP_DPCC_RND_THR_3_RB_MASK 0x0000FF00U
#define RPP_DPCC_RND_THR_3_RB_SHIFT 8U
//! Slice: rnd_thr_3_g:
//! Rank Neighbor Difference threshold for set 3 green
#define RPP_DPCC_RND_THR_3_G
#define RPP_DPCC_RND_THR_3_G_MASK 0x000000FFU
#define RPP_DPCC_RND_THR_3_G_SHIFT 0U

//! Register: rpp_dpcc_rg_fac_3: Rank gradient factor for set 3 (RPP_DPCC_BASE + 0x00000054)
//! Slice: rg_fac_3_rb:
//! Rank gradient factor for set 3 red/blue
#define RPP_DPCC_RG_FAC_3_RB
#define RPP_DPCC_RG_FAC_3_RB_MASK 0x00003F00U
#define RPP_DPCC_RG_FAC_3_RB_SHIFT 8U
//! Slice: rg_fac_3_g:
//! Rank gradient factor for set 3 green
#define RPP_DPCC_RG_FAC_3_G
#define RPP_DPCC_RG_FAC_3_G_MASK 0x0000003FU
#define RPP_DPCC_RG_FAC_3_G_SHIFT 0U

//! Register: rpp_dpcc_ro_limits: Rank Order Limits (RPP_DPCC_BASE + 0x00000058)
//! Slice: ro_lim_3_rb:
//! Rank order limit for set 3 red/blue
#define RPP_DPCC_RO_LIM_3_RB
#define RPP_DPCC_RO_LIM_3_RB_MASK 0x00000C00U
#define RPP_DPCC_RO_LIM_3_RB_SHIFT 10U
//! Slice: ro_lim_3_g:
//! Rank order limit for set 3 green
#define RPP_DPCC_RO_LIM_3_G
#define RPP_DPCC_RO_LIM_3_G_MASK 0x00000300U
#define RPP_DPCC_RO_LIM_3_G_SHIFT 8U
//! Slice: ro_lim_2_rb:
//! Rank order limit for set 2 red/blue
#define RPP_DPCC_RO_LIM_2_RB
#define RPP_DPCC_RO_LIM_2_RB_MASK 0x000000C0U
#define RPP_DPCC_RO_LIM_2_RB_SHIFT 6U
//! Slice: ro_lim_2_g:
//! Rank order limit for set 2 green
#define RPP_DPCC_RO_LIM_2_G
#define RPP_DPCC_RO_LIM_2_G_MASK 0x00000030U
#define RPP_DPCC_RO_LIM_2_G_SHIFT 4U
//! Slice: ro_lim_1_rb:
//! Rank order limit for set 1 red/blue
#define RPP_DPCC_RO_LIM_1_RB
#define RPP_DPCC_RO_LIM_1_RB_MASK 0x0000000CU
#define RPP_DPCC_RO_LIM_1_RB_SHIFT 2U
//! Slice: ro_lim_1_g:
//! Rank order limit for set 1 green
#define RPP_DPCC_RO_LIM_1_G
#define RPP_DPCC_RO_LIM_1_G_MASK 0x00000003U
#define RPP_DPCC_RO_LIM_1_G_SHIFT 0U

//! Register: rpp_dpcc_rnd_offs: Differential Rank Offsets for Rank Neighbor Difference (RPP_DPCC_BASE + 0x0000005c)
//! Slice: rnd_offs_3_rb:
//! Rank Offset to Neighbor for set 3 red/blue
#define RPP_DPCC_RND_OFFS_3_RB
#define RPP_DPCC_RND_OFFS_3_RB_MASK 0x00000C00U
#define RPP_DPCC_RND_OFFS_3_RB_SHIFT 10U
//! Slice: rnd_offs_3_g:
//! Rank Offset to Neighbor for set 3 green
#define RPP_DPCC_RND_OFFS_3_G
#define RPP_DPCC_RND_OFFS_3_G_MASK 0x00000300U
#define RPP_DPCC_RND_OFFS_3_G_SHIFT 8U
//! Slice: rnd_offs_2_rb:
//! Rank Offset to Neighbor for set 2 red/blue
#define RPP_DPCC_RND_OFFS_2_RB
#define RPP_DPCC_RND_OFFS_2_RB_MASK 0x000000C0U
#define RPP_DPCC_RND_OFFS_2_RB_SHIFT 6U
//! Slice: rnd_offs_2_g:
//! Rank Offset to Neighbor for set 2 green
#define RPP_DPCC_RND_OFFS_2_G
#define RPP_DPCC_RND_OFFS_2_G_MASK 0x00000030U
#define RPP_DPCC_RND_OFFS_2_G_SHIFT 4U
//! Slice: rnd_offs_1_rb:
//! Rank Offset to Neighbor for set 1 red/blue
#define RPP_DPCC_RND_OFFS_1_RB
#define RPP_DPCC_RND_OFFS_1_RB_MASK 0x0000000CU
#define RPP_DPCC_RND_OFFS_1_RB_SHIFT 2U
//! Slice: rnd_offs_1_g:
//! Rank Offset to Neighbor for set 1 green
#define RPP_DPCC_RND_OFFS_1_G
#define RPP_DPCC_RND_OFFS_1_G_MASK 0x00000003U
#define RPP_DPCC_RND_OFFS_1_G_SHIFT 0U

//! Register: rpp_dpcc_bpt_ctrl: bad pixel table settings (RPP_DPCC_BASE + 0x00000060)
//! Slice: bpt_rb_3x3:
//! 1: if BPT active red/blue 9 pixel (3x3) output median  
// 0: if BPT active red/blue 4 or 5 pixel output median  *Default*
#define RPP_DPCC_BPT_RB_3X3
#define RPP_DPCC_BPT_RB_3X3_MASK 0x00000800U
#define RPP_DPCC_BPT_RB_3X3_SHIFT 11U
//! Slice: bpt_g_3x3:
//! 1: if BPT active green 9 pixel (3x3) output median  
// 0: if BPT active green 4 or 5 pixel output median  *Default*
#define RPP_DPCC_BPT_G_3X3
#define RPP_DPCC_BPT_G_3X3_MASK 0x00000400U
#define RPP_DPCC_BPT_G_3X3_SHIFT 10U
//! Slice: bpt_incl_rb_center:
//! 1: if BPT active include center pixel for red/blue output median 2x2+1 
// 0: if BPT active do not include center pixel for red/blue output median 2x2 *Default*
#define RPP_DPCC_BPT_INCL_RB_CENTER
#define RPP_DPCC_BPT_INCL_RB_CENTER_MASK 0x00000200U
#define RPP_DPCC_BPT_INCL_RB_CENTER_SHIFT 9U
//! Slice: bpt_incl_green_center:
//! 1: if BPT active include center pixel for green output median 2x2+1 
// 0: if BPT active do not include center pixel for green output median 2x2 *Default*
#define RPP_DPCC_BPT_INCL_GREEN_CENTER
#define RPP_DPCC_BPT_INCL_GREEN_CENTER_MASK 0x00000100U
#define RPP_DPCC_BPT_INCL_GREEN_CENTER_SHIFT 8U
//! Slice: bpt_use_fix_set:
//! 1: for BPT write use hard coded methods set 
// 0: for BPT write do not use hard coded methods set *Default*
#define RPP_DPCC_BPT_USE_FIX_SET
#define RPP_DPCC_BPT_USE_FIX_SET_MASK 0x00000080U
#define RPP_DPCC_BPT_USE_FIX_SET_SHIFT 7U
//! Slice: bpt_use_set_3:
//! 1: for BPT write use methods set 3  
// 0: for BPT write do not use methods set 3 *Default*
#define RPP_DPCC_BPT_USE_SET_3
#define RPP_DPCC_BPT_USE_SET_3_MASK 0x00000040U
#define RPP_DPCC_BPT_USE_SET_3_SHIFT 6U
//! Slice: bpt_use_set_2:
//! 1: for BPT write use methods set 2  
// 0: for BPT write do not use methods set 2 *Default*
#define RPP_DPCC_BPT_USE_SET_2
#define RPP_DPCC_BPT_USE_SET_2_MASK 0x00000020U
#define RPP_DPCC_BPT_USE_SET_2_SHIFT 5U
//! Slice: bpt_use_set_1:
//! 1: for BPT write use methods set 1  
// 0: for BPT write do not use methods set 1 *Default*
#define RPP_DPCC_BPT_USE_SET_1
#define RPP_DPCC_BPT_USE_SET_1_MASK 0x00000010U
#define RPP_DPCC_BPT_USE_SET_1_SHIFT 4U
//! Slice: bpt_cor_en:
//! table based correction enable 
// 1: table based correction is enabled 
// 0: table based correction is disabled
#define RPP_DPCC_BPT_COR_EN
#define RPP_DPCC_BPT_COR_EN_MASK 0x00000002U
#define RPP_DPCC_BPT_COR_EN_SHIFT 1U
//! Slice: bpt_det_en:
//! Bad pixel detection write enable 
// 1: bad pixel detection write to memory is enabled 
// 0: bad pixel detection write to memory is disabled
#define RPP_DPCC_BPT_DET_EN
#define RPP_DPCC_BPT_DET_EN_MASK 0x00000001U
#define RPP_DPCC_BPT_DET_EN_SHIFT 0U

//! Register: rpp_dpcc_bp_number: Number of entries for bad pixel table (table based correction) (RPP_DPCC_BASE + 0x00000064)
//! Slice: bp_number:
//! Number of current Bad Pixel entries in bad pixel table (BPT)
#define RPP_DPCC_BP_NUMBER
#define RPP_DPCC_BP_NUMBER_MASK 0x00000FFFU
#define RPP_DPCC_BP_NUMBER_SHIFT 0U

//! Register: rpp_dpcc_bp_taddr: TABLE Start Address for table-based correction algorithm (RPP_DPCC_BASE + 0x00000068)
//! Slice: bp_table_addr:
//! Table RAM start address for read or write operations. The address counter is incremented at each read or write access to the data register (auto-increment mechanism).
#define RPP_DPCC_BP_TABLE_ADDR
#define RPP_DPCC_BP_TABLE_ADDR_MASK 0x000007FFU
#define RPP_DPCC_BP_TABLE_ADDR_SHIFT 0U

//! Register: rpp_dpcc_bp_position: TABLE DATA register for read and write access of table RAM (RPP_DPCC_BASE + 0x0000006c)
//! Slice: bp_position:
//! 28:16: Bad Pixel vertical pixel position 
// 12:0 : Bad Pixel horizontal pixel position 
//
#define RPP_DPCC_BP_POSITION
#define RPP_DPCC_BP_POSITION_MASK 0x1FFFFFFFU
#define RPP_DPCC_BP_POSITION_SHIFT 0U

#endif /* __RPP_DPCC_REGS_MASK_H__ */
/*************************** EOF **************************************/
