/******************************************************************************
 *
 * Copyright 2017 - 2021, Dream Chip Technologies GmbH. All rights reserved
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_helper.h
 *
 * @brief   Useful macros and functions to be used by low-level drivers.
 *          Provided to avoid code duplication.
 *
 *****************************************************************************/
#ifndef __RPP_HELPER_H__
#define __RPP_HELPER_H__

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppHdrHelper RPP HDR - Helper functions and macros
 * RPP HDR - Helper functions and macros.
 * @{
 *****************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************//**
 * @brief     Count number of leading zero's
 * @param[in] x
 *
 *****************************************************************************/
static inline int clz(int x)
{
    int r;

    if ( !x )
    {
        return ( sizeof(int) * 8 );
    }

    r = 0;

    if ( !(x & 0xffff0000u) )
    {
        x <<= 16;
        r += 16;
    }

    if ( !(x & 0xff000000u) )
    {
        x <<= 8;
        r += 8;
    }

    if ( !(x & 0xf0000000u) )
    {
        x <<= 4;
        r += 4;
    }

    if ( !(x & 0xc0000000u) )
    {
        x <<= 2;
        r += 2;
    }

    if ( !(x & 0x80000000u) )
    {
        x <<= 1;
        r += 1;
    }

    return ( r );
}

/* if available, but check depends on used compiler
 * in clang: #if __has_builtin(__builtin_clz) ...
static inline int clz(int x)
{
    return __builtin_clz(x);
}
*/

/**************************************************************************//**
 * @brief     Natural logarith to the basis 2
 * @param[in] x
 *
 *****************************************************************************/
static inline unsigned log2i(unsigned x)
{
    return (sizeof(x) * 8 - clz(x) - 1);
}

/**************************************************************************//**
 * @def SIGNED_MIN(bits)
 * @brief returns the minimum signed integer value representable by a bitfield
 * @param bits      the number of bits in the bitfield (including the sign bit)
 *
 * This macro returns the minimum integer value that can be represented by a
 * signed bitfield containing the number of bits passed as argument. The returned
 * value is always a negative number.
 *
 * @note: The supplied number of bits must be between 1 and 31.
 *****************************************************************************/
#define SIGNED_MIN(bits)        (-(1 << ( (bits)-1 ) ))

/**************************************************************************//**
 * @def SIGNED_MAX(bits)
 * @brief returns the maximum signed integer value representable by a bitfield
 * @param bits      the number of bits in the bitfield (including the sign bit)
 *
 * This macro returns the maximum integer value that can be represented in a
 * signed bitfield containing the number of bits passed as argument.
 *
 * @note: The supplied number of bits must be between 1 and 31.
 *****************************************************************************/
#define SIGNED_MAX(bits)        ( ( 1 << ((bits)-1) ) - 1)

/**************************************************************************//**
 * @def UNSIGNED_MAX(bits)
 * @brief returns the maximum unsigned integer value representable by a bitfield
 * @param bits      the number of bits in the bitfield
 *
 * This macro returns the maximum integer value that can be represented in a
 * bitfield containing the number of bits passed as argument.
 *
 * @note: The supplied number of bits must be between 1 and 31.
 * @note: The returned value is equal to the bitmask of the bitfield.
 *****************************************************************************/
#define UNSIGNED_MAX(bits)        ( ( 1 << (bits) ) - 1 )

/**************************************************************************//**
 * @brief function to convert a signed bitfield to a valid signed number
 * @param value     the masked bitfield
 * @param bits      the number of bits in the bitfield (including the sign bit)
 *
 * This function returns the signed integer value that is represented by
 * a bitfield of given size.
 *
 * Since the bitfield is extracted from an unsigned register value using mask
 * and right-shift operations, negative values represented by the bitfield need
 * to get the MSBs added to be recognized as negative numbers by the host CPU.
 * This process is called sign extension.
 *****************************************************************************/
static inline int32_t sign_extend(const uint32_t value, const int bits)
{
    /* Don't make this a macro because it uses a computed value multiple
     * times and for performance reasons, it's better to keep the computed
     * value in a variable. Dumb compilers may otherwise compute the
     * value multiple times. */

    /* Calculate the bitmask of the bitfield without the sign bit.
     * This is exactly equivalent to the signed maximum value supported
     * by the bitfield. */
    const uint32_t mask = SIGNED_MAX(bits);

    /* Now, if the bitfield value is larger than the signed maximum value,
     * it will have the sign bit set and therefore be negative. To be
     * recognized as negative value by the host CPU, all MSBs up to the
     * bitfield's sign bit must be set to "1". This is accomplished by
     * inverting the SIGNED_MAX value and OR'ing that to the value.
     */
    return (value > mask) ? (value | ~mask) : value;
}

/**************************************************************************//**
 * @def REG_GET_SLICE
 * @brief returns the value of slice \a name from register or variable \a reg
 * @note "parameter" \a reg could be a hardware register or a (32bit) variable, but not a pointer! \n
 *       each slice (specified as "parameter" \a name) requires two \#defines: \n
 *        - <tt>\<name\>_MASK  </tt>: defines the mask to use on register side
 *        - <tt>\<name\>_SHIFT </tt>: defines the shift value to use (left on write, right on read)
 *****************************************************************************/
#define REG_GET_SLICE(reg, name)    (((reg) & (name##_MASK)) >> (name##_SHIFT))

/**************************************************************************//**
 * @def REG_SET_SLICE
 * @brief writes the value \a value into slice \a name of register or variable \a reg
 * @note "parameter" \a reg could be a hardware register or a (32bit) variable, but not a pointer! \n
 *       each slice (specified as "parameter" \a name) requires two \#defines: \n
 *        - <tt>\<name\>_MASK  </tt>: defines the mask to use on register side
 *        - <tt>\<name\>_SHIFT </tt>: defines the shift value to use (left on write, right on read)
 *****************************************************************************/
#define REG_SET_SLICE(reg, name, value) \
    { \
        ((reg) = (((reg) & ~(name##_MASK)) | (((value) << (name##_SHIFT)) & (name##_MASK)))); \
    }

/**************************************************************************//**
 * @def REG_RANGE_CHECK
 * @brief returns all bits which are not masked by the slice mask
 * @note "parameter" \a reg could be a hardware register or a (32bit) variable, but not a pointer! \n
 *       each slice (specified as "parameter" \a name) requires two \#defines: \n
 *        - <tt>\<name\>_MASK  </tt>: defines the mask to use on register side
 *        - <tt>\<name\>_SHIFT </tt>: defines the shift value to use (left on write, right on read)
 *****************************************************************************/
#define REG_RANGE_CHECK(reg, name)  ((reg) & ~(name##_MASK>>name##_SHIFT))

/**************************************************************************//**
 * @def NUM_REGS(count, shift)
 * @brief returns the number of packed registers for @c count values using the given @c shift
 * @param count     number of values to pack into the 32bit registers
 * @param shift     number of bits between adjacent values in a register
 * @return the number of registers required to hold @c count values
 *
 * Frequently an array is distributed across several unit registers, where a
 * single register holds more than one array element.
 *
 * This function returns the number of unit registers required to pack an array
 * of @c count values into the unit's registers, assuming that subsequent array
 * elements are @c shift bits away from each other.
 * 
 * Examples:
 * - shift=2, 16 values per register, returns (count + 15) / 16
 * - shift=8, 4 values per register, returns (count + 3) / 4
 * - shift=12, 2 values per register, returns (count + 1) / 2
 * - shift=18, 1 value per register, returns count.
 *****************************************************************************/
#define NUM_REGS(count, shift)  ( ( (count) + 32/(shift) - 1 ) / (32/(shift)) )

/**************************************************************************//**
 * @def PACK_REGS(count, shift, mask, invals, outregs)
 * @brief packs a number of array elements densly into a register set
 * @param count     the number of values to pack
 * @param shift     the distance of array values in a register
 * @param mask      the array element mask
 * @param invals    the input array, must be indexable
 * @param outregs   the output register array of type uint32_t
 *
 * This macro is provided to pack an array of values of given size densely
 * into a set of registers. The @c mask parameter determines how many
 * bits are occupied by a single value in the register, while the
 * @c shift parameter determines the distance of adjacent values in
 * the register. Both values may well be distinct: it is possible that
 * 3bit values are packed on 8bit boundaries.
 *****************************************************************************/
#define PACK_REGS(count,shift,mask,invals,outregs)                      \
{                                                                       \
    /* calculate first invalid max. shift value */                      \
    unsigned jjmax = 33 - shift;                                        \
    unsigned ii, jj, kk = 0;                                            \
    unsigned num_regs = NUM_REGS(count, shift);                         \
    for (ii = 0; ii < num_regs; ++ii) {                                 \
        /* clear register initially. */                                 \
        uint32_t regval = 0;                                            \
        /* loop over all possible slices per per register */            \
        for (jj = 0; (jj < jjmax) && (kk < count); jj += shift, ++kk) { \
            /* shift the current value up by 'jj' bits and              \
             * combine it with the register value already prepared.     \
             */                                                         \
            regval |= ((invals)[kk] << jj);                             \
        }                                                               \
        (outregs)[ii] = regval;                                         \
    }                                                                   \
}

/**************************************************************************//**
 * @def UNPACK_REGS(count, shift, mask, inregs, outvals)
 * @brief packs a number of array elements densly into a register set
 * @param count     the number of values to pack
 * @param shift     the distance of array values in a register
 * @param mask      the array element mask
 * @param inregs    the input register array of type uint32_t
 * @param outvals   the output value array, must be indexable
 *
 * This macro is provided to unpack an array of values of given size
 * from a set of densely packed registers. The @c mask parameter determines
 * how many bits are occupied by a single value in the register, while the
 * @c shift parameter determines the distance of adjacent values in
 * the register. Both values may well be distinct: it is possible that
 * 3bit values are packed on 8bit boundaries.
 *****************************************************************************/
#define UNPACK_REGS(count,shift,mask,inregs,outvals)                    \
{                                                                       \
    /* calculate first invalid max. shift value */                      \
    unsigned jjmax = 33 - shift;                                        \
    unsigned ii, jj, kk = 0;                                            \
    unsigned num_regs = NUM_REGS(count, shift);                         \
    for (ii = 0; ii < num_regs; ++ii) {                                 \
        uint32_t reg = (inregs)[ii];                                    \
        /* loop over all possible values per per register */            \
        for (jj = 0; (jj < jjmax) && (kk < count); jj += shift, ++kk) { \
            /* extract the current value from the register              \
             * by shifting and masking. */                              \
            (outvals)[kk] = (reg >> jj) & mask;                         \
        }                                                               \
    }                                                                   \
}

/**************************************************************************//**
 * @def READL(reg, outval)
 * @brief helper macro to read a single register from the device
 * @param reg       the register offset inside the unit to read from
 * @param outval    uint32_t pointer where to store the value
 *
 * This macro calls the rpp_device function that reads a single register
 * from the unit. It expects that the surrounding context defines a
 * variable @ drv that points to the driver-specific data structure
 * to be used for register access.
 *****************************************************************************/
#define READL(reg, outval)  \
    drv->dev->readl(drv->dev, drv->base, reg, outval)

/**************************************************************************//**
 * @def WRITEL(reg, outval)
 * @brief helper macro to read a single register from the device
 * @param reg       the register offset inside the unit to write to
 * @param outval    uint32_t holding the value to write
 *
 * This macro calls the rpp_device function that writes a single register
 * in the unit. It expects that the surrounding context defines a
 * variable @ drv that points to the driver-specific data structure
 * to be used for register access.
 *****************************************************************************/
#define WRITEL(reg, outval)  \
    drv->dev->writel(drv->dev, drv->base, reg, outval)

/**************************************************************************//**
 * @def READ_REGS(reg, outvals, nregs)
 * @brief helper macro to read multiple registers from the device
 * @param reg       the first register offset inside the unit to read from
 * @param outvals   uint32_t pointer where to store the values
 * @param nregs     number of registers to read
 *
 * This macro calls the rpp_device function that reads multiple adjacent registers
 * from the unit with a default increment of 4.
 *
 * It expects that the surrounding context defines a
 * variable @ drv that points to the driver-specific data structure
 * to be used for register access.
 *****************************************************************************/
#define READ_REGS(reg, outvals, nregs)  \
    drv->dev->read_regs(drv->dev, drv->base, reg, nregs, sizeof(uint32_t), outvals)

/**************************************************************************//**
 * @def WRITE_REGS(reg, vals, nregs)
 * @brief helper macro to write multiple registers to the device
 * @param reg       the first register offset inside the unit to write to
 * @param vals      uint32_t pointer where the values to be written are stored
 * @param nregs     number of registers to write
 *
 * This macro calls the rpp_device function that writes multiple adjacent registers
 * to the unit with a default increment of 4.
 *
 * It expects that the surrounding context defines a
 * variable @ drv that points to the driver-specific data structure
 * to be used for register access.
 *****************************************************************************/
#define WRITE_REGS(reg, vals, nregs)  \
    drv->dev->write_regs(drv->dev, drv->base, reg, nregs, sizeof(uint32_t), vals)

/**************************************************************************//**
 * @def READ_RAM(reg, outvals, ncells)
 * @brief helper macro to read multiple indirect RAM cells from the device
 * @param reg       the unit's indirect RAM data register offset
 * @param outvals   uint32_t pointer where to store the values
 * @param ncells    number of indirect RAM cells to read
 *
 * This macro calls the rpp_device function that reads multiple registers
 * from the unit with a default increment of 0. The @c reg parameter shall
 * point to the unit's indirect RAM data register, the corresponding indirect
 * RAM address register must have been programmed in advance. It is expected
 * that the indirect RAM address register is self-incrementing.
 *
 * It expects that the surrounding context defines a
 * variable @ drv that points to the driver-specific data structure
 * to be used for register access.
 *****************************************************************************/
#define READ_RAM(reg, outvals, ncells) \
    drv->dev->read_regs(drv->dev, drv->base, reg, ncells, 0, outvals)

/**************************************************************************//**
 * @def WRITE_RAM(reg, vals, ncells)
 * @brief helper macro to write to multiple indirect RAM cells in the device
 * @param reg       the unit's indirect RAM data register offset
 * @param vals      uint32_t pointer where the values to be written are stored
 * @param ncells    number of indirect RAM cells to write
 *
 * This macro calls the rpp_device function that writes multiple registers
 * to the unit with a default increment of 0. The @c reg parameter shall
 * point to the unit's indirect RAM data register, the corresponding indirect
 * RAM address register must have been programmed in advance. It is expected
 * that the indirect RAM address register is self-incrementing.
 *
 * It expects that the surrounding context defines a
 * variable @ drv that points to the driver-specific data structure
 * to be used for register access.
 *****************************************************************************/
#define WRITE_RAM(reg, vals, ncells)  \
    drv->dev->write_regs(drv->dev, drv->base, reg, ncells, 0, vals)

/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_HELPER_H__ */

