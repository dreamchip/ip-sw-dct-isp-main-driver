/**
//-----------------------------------------------------------------------------
// This is an unpublished work, the copyright in which vests in 
// DreamChip Technologies GmbH. The information contained herein is the property 
// of DreamChip Technologies GmbH and is supplied without liability for errors or 
// omissions. No part may be reproduced or used except as authorized by 
// contract or other written permission.
// Copyright(c) DreamChip Technologies GmbH, 2023. All rights reserved.
//-----------------------------------------------------------------------------
//
//       N-1         _ __             _
//        __        |  ||  /    1 \    |
//  X  = \    x  cos|  -- ( n + -  ) k |   k = 0,...,N-1
//   k   /__   n    |_  N  \    2 /   _|
//       n=0
//
//-----------------------------------------------------------------------------
// Module   : rpp_gamma_out24
//
// Purpose  : Register C header file
//
// Creator  : DCT
//
// Comments : automatically generated by sig (4.0.5)
//            !!!!! DO NOT EDIT THIS FILE !!!!!
//-----------------------------------------------------------------------------
* @file rpp_gamma_out24_regs_mask.h
*
* <pre>
*
* Description:
*   This header file exports the module register structure and masks. 
*   It should not be included directly by your driver/application, it will be  
*   exported by the top level header file. 
*
* </pre>
*/
/*****************************************************************************/

#ifndef __RPP_GAMMA_OUT24_REGS_MASK_H__
#define __RPP_GAMMA_OUT24_REGS_MASK_H__


// - MASK AND SHIFT MACROS ----------------------------------------------------------

//! Register Group: rpp_gamma_out_registers

//! Register: rpp_gamma_out_version: Gamma Out Version (RPP_GAMMA_OUT_BASE + 0x00000000)
//! Slice: gamma_out_version:
//! version number for gamma out module 
// 0x01: version for 12 bit component width 
// 0x02: version for 24 bit component width
#define RPP_GAMMA_OUT_GAMMA_OUT_VERSION
#define RPP_GAMMA_OUT_GAMMA_OUT_VERSION_MASK 0x000000FFU
#define RPP_GAMMA_OUT_GAMMA_OUT_VERSION_SHIFT 0U

//! Register: rpp_gamma_out_enable: Gamma out module enable (RPP_GAMMA_OUT_BASE + 0x00000004)
//! Slice: gamma_out_en:
//! 0: GAMMA_OUT stage disabled. Output samples of the GAMMA_OUT stage are not compensated. 
// 1: GAMMA_OUT stage enabled. LInearization active.
#define RPP_GAMMA_OUT_GAMMA_OUT_EN
#define RPP_GAMMA_OUT_GAMMA_OUT_EN_MASK 0x00000001U
#define RPP_GAMMA_OUT_GAMMA_OUT_EN_SHIFT 0U

//! Register: rpp_gamma_out_mode: gamma segmentation mode register for output gamma (RPP_GAMMA_OUT_BASE + 0x00000008)
//! Slice: gamma_out_equ_segm:
//! 0: logarithmic like segmentation of gamma curve (default after reset). Segmentation from 0 to (2^24)-1: 
// 2^18, 2^18, 2^18, 2^18, 2^19, 2^19, 2^19, 2^19, 2^20, 2^20, 2^20, 2^21, 2^21, 2^21, 2^21, 2^21 
// 1: equidistant segmentation (all 16 segments are 2^20 )
#define RPP_GAMMA_OUT_GAMMA_OUT_EQU_SEGM
#define RPP_GAMMA_OUT_GAMMA_OUT_EQU_SEGM_MASK 0x00000001U
#define RPP_GAMMA_OUT_GAMMA_OUT_EQU_SEGM_SHIFT 0U

//! Register array: rpp_gamma_out_y: Gamma Out Curve definition y_ (RPP_GAMMA_OUT_BASE + 0x0000000C + n*0x4 (n=0..16))
//! Slice: gamma_out_y:
// Gamma_out curve point definition y-axis (output) for all color components (red,green,blue)
#define RPP_GAMMA_OUT_GAMMA_OUT_Y
#define RPP_GAMMA_OUT_GAMMA_OUT_Y_MASK 0x00FFFFFFU
#define RPP_GAMMA_OUT_GAMMA_OUT_Y_SHIFT 0U

#endif /* __RPP_GAMMA_OUT24_REGS_MASK_H__ */
/*************************** EOF **************************************/
