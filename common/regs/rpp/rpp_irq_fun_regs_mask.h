/**
//-----------------------------------------------------------------------------
// This is an unpublished work, the copyright in which vests in 
// DreamChip Technologies GmbH. The information contained herein is the property 
// of DreamChip Technologies GmbH and is supplied without liability for errors or 
// omissions. No part may be reproduced or used except as authorized by 
// contract or other written permission.
// Copyright(c) DreamChip Technologies GmbH, 2022. All rights reserved.
//-----------------------------------------------------------------------------
//
//       N-1         _ __             _
//        __        |  ||  /    1 \    |
//  X  = \    x  cos|  -- ( n + -  ) k |   k = 0,...,N-1
//   k   /__   n    |_  N  \    2 /   _|
//       n=0
//
//-----------------------------------------------------------------------------
// Module   : rpp_irq_fun
//
// Purpose  : Register C header file
//
// Creator  : DCT
//
// Comments : automatically generated by sig (4.0.2)
//            !!!!! DO NOT EDIT THIS FILE !!!!!
//-----------------------------------------------------------------------------
* @file rpp_irq_fun_regs_mask.h
*
* <pre>
*
* Description:
*   This header file exports the module register structure and masks. 
*   It should not be included directly by your driver/application, it will be  
*   exported by the top level header file. 
*
* </pre>
*/
/*****************************************************************************/

#ifndef __RPP_IRQ_FUN_REGS_MASK_H__
#define __RPP_IRQ_FUN_REGS_MASK_H__


// - MASK AND SHIFT MACROS ----------------------------------------------------------

//! Register Group: rpp_interrupt_control_registers

//! Register array: rpp_ism: Interrupt status mask (RPP_IRQ_BASE + 0x00000000 + n*0x14 (n=0..0))
//! Slice: ism_rpp_main_pre1_rpp_hist256:
// Interrupt mask: 256 bin Histogram write complete
#define RPP_IRQ_ISM_RPP_MAIN_PRE1_RPP_HIST256
#define RPP_IRQ_ISM_RPP_MAIN_PRE1_RPP_HIST256_MASK 0x08000000U
#define RPP_IRQ_ISM_RPP_MAIN_PRE1_RPP_HIST256_SHIFT 27U
//! Slice: ism_rpp_main_pre3_rpp_dpcc:
// Interrupt mask: PRE3 defect pixel output write complete
#define RPP_IRQ_ISM_RPP_MAIN_PRE3_RPP_DPCC
#define RPP_IRQ_ISM_RPP_MAIN_PRE3_RPP_DPCC_MASK 0x04000000U
#define RPP_IRQ_ISM_RPP_MAIN_PRE3_RPP_DPCC_SHIFT 26U
//! Slice: ism_rpp_main_pre2_rpp_dpcc:
// Interrupt mask: PRE2 defect pixel output write complete
#define RPP_IRQ_ISM_RPP_MAIN_PRE2_RPP_DPCC
#define RPP_IRQ_ISM_RPP_MAIN_PRE2_RPP_DPCC_MASK 0x02000000U
#define RPP_IRQ_ISM_RPP_MAIN_PRE2_RPP_DPCC_SHIFT 25U
//! Slice: ism_rpp_main_pre1_rpp_dpcc:
// Interrupt mask: PRE1 defect pixel output write complete
#define RPP_IRQ_ISM_RPP_MAIN_PRE1_RPP_DPCC
#define RPP_IRQ_ISM_RPP_MAIN_PRE1_RPP_DPCC_MASK 0x01000000U
#define RPP_IRQ_ISM_RPP_MAIN_PRE1_RPP_DPCC_SHIFT 24U
//! Slice: ism_rpp_mv_out_frame_out:
// Interrupt mask: MV_OUT write complete
#define RPP_IRQ_ISM_RPP_MV_OUT_FRAME_OUT
#define RPP_IRQ_ISM_RPP_MV_OUT_FRAME_OUT_MASK 0x00800000U
#define RPP_IRQ_ISM_RPP_MV_OUT_FRAME_OUT_SHIFT 23U
//! Slice: ism_rpp_mv_out_off:
// Interrupt mask: MV_OUT switched off
#define RPP_IRQ_ISM_RPP_MV_OUT_OFF
#define RPP_IRQ_ISM_RPP_MV_OUT_OFF_MASK 0x00400000U
#define RPP_IRQ_ISM_RPP_MV_OUT_OFF_SHIFT 22U
//! Slice: ism_rpp_main_post_awb_done:
// Interrupt mask: POST AWB measurement complete.
#define RPP_IRQ_ISM_RPP_MAIN_POST_AWB_DONE
#define RPP_IRQ_ISM_RPP_MAIN_POST_AWB_DONE_MASK 0x00200000U
#define RPP_IRQ_ISM_RPP_MAIN_POST_AWB_DONE_SHIFT 21U
//! Slice: ism_rpp_main_post_rpp_hist:
// Interrupt mask: POST Histogram measurement complete.
#define RPP_IRQ_ISM_RPP_MAIN_POST_RPP_HIST
#define RPP_IRQ_ISM_RPP_MAIN_POST_RPP_HIST_MASK 0x00100000U
#define RPP_IRQ_ISM_RPP_MAIN_POST_RPP_HIST_SHIFT 20U
//! Slice: ism_rpp_main_post_tm:
// Interrupt mask: POST Tone Mapping measurement complete.
#define RPP_IRQ_ISM_RPP_MAIN_POST_TM
#define RPP_IRQ_ISM_RPP_MAIN_POST_TM_MASK 0x00080000U
#define RPP_IRQ_ISM_RPP_MAIN_POST_TM_SHIFT 19U
//! Slice: ism_rpp_main_pre1_exm:
// Interrupt mask: PRE1 Exposure measurement complete.
#define RPP_IRQ_ISM_RPP_MAIN_PRE1_EXM
#define RPP_IRQ_ISM_RPP_MAIN_PRE1_EXM_MASK 0x00040000U
#define RPP_IRQ_ISM_RPP_MAIN_PRE1_EXM_SHIFT 18U
//! Slice: ism_rpp_main_pre1_rpp_hist:
// Interrupt mask: PRE1 Histogram measurement complete.
#define RPP_IRQ_ISM_RPP_MAIN_PRE1_RPP_HIST
#define RPP_IRQ_ISM_RPP_MAIN_PRE1_RPP_HIST_MASK 0x00020000U
#define RPP_IRQ_ISM_RPP_MAIN_PRE1_RPP_HIST_SHIFT 17U
//! Slice: ism_rpp_main_pre1_rpp_acq_frame_in:
// Interrupt mask: PRE1 Frame received at input.
#define RPP_IRQ_ISM_RPP_MAIN_PRE1_RPP_ACQ_FRAME_IN
#define RPP_IRQ_ISM_RPP_MAIN_PRE1_RPP_ACQ_FRAME_IN_MASK 0x00010000U
#define RPP_IRQ_ISM_RPP_MAIN_PRE1_RPP_ACQ_FRAME_IN_SHIFT 16U
//! Slice: ism_rpp_main_pre1_rpp_acq_inform_h_start_edge:
// Interrupt mask: PRE1 hstart edge received.
#define RPP_IRQ_ISM_RPP_MAIN_PRE1_RPP_ACQ_INFORM_H_START_EDGE
#define RPP_IRQ_ISM_RPP_MAIN_PRE1_RPP_ACQ_INFORM_H_START_EDGE_MASK 0x00008000U
#define RPP_IRQ_ISM_RPP_MAIN_PRE1_RPP_ACQ_INFORM_H_START_EDGE_SHIFT 15U
//! Slice: ism_rpp_main_pre1_rpp_acq_inform_v_start_edge:
// Interrupt mask: PRE1 vstart edge received.
#define RPP_IRQ_ISM_RPP_MAIN_PRE1_RPP_ACQ_INFORM_V_START_EDGE
#define RPP_IRQ_ISM_RPP_MAIN_PRE1_RPP_ACQ_INFORM_V_START_EDGE_MASK 0x00004000U
#define RPP_IRQ_ISM_RPP_MAIN_PRE1_RPP_ACQ_INFORM_V_START_EDGE_SHIFT 14U
//! Slice: ism_rpp_main_pre2_exm:
// Interrupt mask: PRE2 Exposure measurement complete.
#define RPP_IRQ_ISM_RPP_MAIN_PRE2_EXM
#define RPP_IRQ_ISM_RPP_MAIN_PRE2_EXM_MASK 0x00002000U
#define RPP_IRQ_ISM_RPP_MAIN_PRE2_EXM_SHIFT 13U
//! Slice: ism_rpp_main_pre2_rpp_hist:
// Interrupt mask: PRE2 Histogram measurement complete.
#define RPP_IRQ_ISM_RPP_MAIN_PRE2_RPP_HIST
#define RPP_IRQ_ISM_RPP_MAIN_PRE2_RPP_HIST_MASK 0x00001000U
#define RPP_IRQ_ISM_RPP_MAIN_PRE2_RPP_HIST_SHIFT 12U
//! Slice: ism_rpp_main_pre2_rpp_acq_frame_in:
// Interrupt mask: PRE2 Frame received at input.
#define RPP_IRQ_ISM_RPP_MAIN_PRE2_RPP_ACQ_FRAME_IN
#define RPP_IRQ_ISM_RPP_MAIN_PRE2_RPP_ACQ_FRAME_IN_MASK 0x00000800U
#define RPP_IRQ_ISM_RPP_MAIN_PRE2_RPP_ACQ_FRAME_IN_SHIFT 11U
//! Slice: ism_rpp_main_pre2_rpp_acq_inform_h_start_edge:
// Interrupt mask: PRE2 hstart edge received.
#define RPP_IRQ_ISM_RPP_MAIN_PRE2_RPP_ACQ_INFORM_H_START_EDGE
#define RPP_IRQ_ISM_RPP_MAIN_PRE2_RPP_ACQ_INFORM_H_START_EDGE_MASK 0x00000400U
#define RPP_IRQ_ISM_RPP_MAIN_PRE2_RPP_ACQ_INFORM_H_START_EDGE_SHIFT 10U
//! Slice: ism_rpp_main_pre2_rpp_acq_inform_v_start_edge:
// Interrupt mask: PRE2 vstart edge received.
#define RPP_IRQ_ISM_RPP_MAIN_PRE2_RPP_ACQ_INFORM_V_START_EDGE
#define RPP_IRQ_ISM_RPP_MAIN_PRE2_RPP_ACQ_INFORM_V_START_EDGE_MASK 0x00000200U
#define RPP_IRQ_ISM_RPP_MAIN_PRE2_RPP_ACQ_INFORM_V_START_EDGE_SHIFT 9U
//! Slice: ism_rpp_main_pre3_exm:
// Interrupt mask: PRE3 Exposure measurement complete.
#define RPP_IRQ_ISM_RPP_MAIN_PRE3_EXM
#define RPP_IRQ_ISM_RPP_MAIN_PRE3_EXM_MASK 0x00000100U
#define RPP_IRQ_ISM_RPP_MAIN_PRE3_EXM_SHIFT 8U
//! Slice: ism_rpp_main_pre3_rpp_hist:
// Interrupt mask: PRE3 Histogram measurement complete.
#define RPP_IRQ_ISM_RPP_MAIN_PRE3_RPP_HIST
#define RPP_IRQ_ISM_RPP_MAIN_PRE3_RPP_HIST_MASK 0x00000080U
#define RPP_IRQ_ISM_RPP_MAIN_PRE3_RPP_HIST_SHIFT 7U
//! Slice: ism_rpp_main_pre3_rpp_acq_frame_in:
// Interrupt mask: PRE3 Frame received at input.
#define RPP_IRQ_ISM_RPP_MAIN_PRE3_RPP_ACQ_FRAME_IN
#define RPP_IRQ_ISM_RPP_MAIN_PRE3_RPP_ACQ_FRAME_IN_MASK 0x00000040U
#define RPP_IRQ_ISM_RPP_MAIN_PRE3_RPP_ACQ_FRAME_IN_SHIFT 6U
//! Slice: ism_rpp_main_pre3_rpp_acq_inform_h_start_edge:
// Interrupt mask: PRE3 hstart edge received.
#define RPP_IRQ_ISM_RPP_MAIN_PRE3_RPP_ACQ_INFORM_H_START_EDGE
#define RPP_IRQ_ISM_RPP_MAIN_PRE3_RPP_ACQ_INFORM_H_START_EDGE_MASK 0x00000020U
#define RPP_IRQ_ISM_RPP_MAIN_PRE3_RPP_ACQ_INFORM_H_START_EDGE_SHIFT 5U
//! Slice: ism_rpp_main_pre3_rpp_acq_inform_v_start_edge:
// Interrupt mask: PRE3 vstart edge received.
#define RPP_IRQ_ISM_RPP_MAIN_PRE3_RPP_ACQ_INFORM_V_START_EDGE
#define RPP_IRQ_ISM_RPP_MAIN_PRE3_RPP_ACQ_INFORM_V_START_EDGE_MASK 0x00000010U
#define RPP_IRQ_ISM_RPP_MAIN_PRE3_RPP_ACQ_INFORM_V_START_EDGE_SHIFT 4U
//! Slice: ism_rpp_out_frame_out:
// Interrupt mask: RPP_OUT Output frame complete.
#define RPP_IRQ_ISM_RPP_OUT_FRAME_OUT
#define RPP_IRQ_ISM_RPP_OUT_FRAME_OUT_MASK 0x00000008U
#define RPP_IRQ_ISM_RPP_OUT_FRAME_OUT_SHIFT 3U
//! Slice: ism_rpp_out_off:
// Interrupt mask: RPP_OUT is off.
#define RPP_IRQ_ISM_RPP_OUT_OFF
#define RPP_IRQ_ISM_RPP_OUT_OFF_MASK 0x00000004U
#define RPP_IRQ_ISM_RPP_OUT_OFF_SHIFT 2U
//! Slice: ism_rpp_rmap_meas:
// Interrupt mask: RMAP measurement complete.
#define RPP_IRQ_ISM_RPP_RMAP_MEAS
#define RPP_IRQ_ISM_RPP_RMAP_MEAS_MASK 0x00000002U
#define RPP_IRQ_ISM_RPP_RMAP_MEAS_SHIFT 1U
//! Slice: ism_rpp_rmap:
// Interrupt mask: RMAP Output frame complete.
#define RPP_IRQ_ISM_RPP_RMAP
#define RPP_IRQ_ISM_RPP_RMAP_MASK 0x00000001U
#define RPP_IRQ_ISM_RPP_RMAP_SHIFT 0U

//! Register array: rpp_ris: Raw interrupt status (RPP_IRQ_BASE + 0x00000004 + n*0x14 (n=0..0))
//! Slice: ris_rpp_main_pre1_rpp_hist256:
// raw interrupt status: 256 bin Histogram write complete
#define RPP_IRQ_RIS_RPP_MAIN_PRE1_RPP_HIST256
#define RPP_IRQ_RIS_RPP_MAIN_PRE1_RPP_HIST256_MASK 0x08000000U
#define RPP_IRQ_RIS_RPP_MAIN_PRE1_RPP_HIST256_SHIFT 27U
//! Slice: ris_rpp_main_pre3_rpp_dpcc:
// raw interrupt status: PRE3 defect pixel output write complete
#define RPP_IRQ_RIS_RPP_MAIN_PRE3_RPP_DPCC
#define RPP_IRQ_RIS_RPP_MAIN_PRE3_RPP_DPCC_MASK 0x04000000U
#define RPP_IRQ_RIS_RPP_MAIN_PRE3_RPP_DPCC_SHIFT 26U
//! Slice: ris_rpp_main_pre2_rpp_dpcc:
// raw interrupt status: PRE2 defect pixel output write complete
#define RPP_IRQ_RIS_RPP_MAIN_PRE2_RPP_DPCC
#define RPP_IRQ_RIS_RPP_MAIN_PRE2_RPP_DPCC_MASK 0x02000000U
#define RPP_IRQ_RIS_RPP_MAIN_PRE2_RPP_DPCC_SHIFT 25U
//! Slice: ris_rpp_main_pre1_rpp_dpcc:
// raw interrupt status: PRE1 defect pixel output write complete
#define RPP_IRQ_RIS_RPP_MAIN_PRE1_RPP_DPCC
#define RPP_IRQ_RIS_RPP_MAIN_PRE1_RPP_DPCC_MASK 0x01000000U
#define RPP_IRQ_RIS_RPP_MAIN_PRE1_RPP_DPCC_SHIFT 24U
//! Slice: ris_rpp_mv_out_frame_out:
// raw interrupt status: MV_OUT write complete
#define RPP_IRQ_RIS_RPP_MV_OUT_FRAME_OUT
#define RPP_IRQ_RIS_RPP_MV_OUT_FRAME_OUT_MASK 0x00800000U
#define RPP_IRQ_RIS_RPP_MV_OUT_FRAME_OUT_SHIFT 23U
//! Slice: ris_rpp_mv_out_off:
// raw interrupt status: MV_OUT switched off
#define RPP_IRQ_RIS_RPP_MV_OUT_OFF
#define RPP_IRQ_RIS_RPP_MV_OUT_OFF_MASK 0x00400000U
#define RPP_IRQ_RIS_RPP_MV_OUT_OFF_SHIFT 22U
//! Slice: ris_rpp_main_post_awb_done:
// raw interrupt status: POST AWB measurement complete.
#define RPP_IRQ_RIS_RPP_MAIN_POST_AWB_DONE
#define RPP_IRQ_RIS_RPP_MAIN_POST_AWB_DONE_MASK 0x00200000U
#define RPP_IRQ_RIS_RPP_MAIN_POST_AWB_DONE_SHIFT 21U
//! Slice: ris_rpp_main_post_rpp_hist:
// raw interrupt status: POST Histogram measurement complete.
#define RPP_IRQ_RIS_RPP_MAIN_POST_RPP_HIST
#define RPP_IRQ_RIS_RPP_MAIN_POST_RPP_HIST_MASK 0x00100000U
#define RPP_IRQ_RIS_RPP_MAIN_POST_RPP_HIST_SHIFT 20U
//! Slice: ris_rpp_main_post_tm:
// raw interrupt status: POST Tone Mapping measurement complete.
#define RPP_IRQ_RIS_RPP_MAIN_POST_TM
#define RPP_IRQ_RIS_RPP_MAIN_POST_TM_MASK 0x00080000U
#define RPP_IRQ_RIS_RPP_MAIN_POST_TM_SHIFT 19U
//! Slice: ris_rpp_main_pre1_exm:
// raw interrupt status: PRE1 Exposure measurement complete.
#define RPP_IRQ_RIS_RPP_MAIN_PRE1_EXM
#define RPP_IRQ_RIS_RPP_MAIN_PRE1_EXM_MASK 0x00040000U
#define RPP_IRQ_RIS_RPP_MAIN_PRE1_EXM_SHIFT 18U
//! Slice: ris_rpp_main_pre1_rpp_hist:
// raw interrupt status: PRE1 Histogram measurement complete.
#define RPP_IRQ_RIS_RPP_MAIN_PRE1_RPP_HIST
#define RPP_IRQ_RIS_RPP_MAIN_PRE1_RPP_HIST_MASK 0x00020000U
#define RPP_IRQ_RIS_RPP_MAIN_PRE1_RPP_HIST_SHIFT 17U
//! Slice: ris_rpp_main_pre1_rpp_acq_frame_in:
// raw interrupt status: PRE1 Frame received at input.
#define RPP_IRQ_RIS_RPP_MAIN_PRE1_RPP_ACQ_FRAME_IN
#define RPP_IRQ_RIS_RPP_MAIN_PRE1_RPP_ACQ_FRAME_IN_MASK 0x00010000U
#define RPP_IRQ_RIS_RPP_MAIN_PRE1_RPP_ACQ_FRAME_IN_SHIFT 16U
//! Slice: ris_rpp_main_pre1_rpp_acq_inform_h_start_edge:
// raw interrupt status: PRE1 hstart edge received.
#define RPP_IRQ_RIS_RPP_MAIN_PRE1_RPP_ACQ_INFORM_H_START_EDGE
#define RPP_IRQ_RIS_RPP_MAIN_PRE1_RPP_ACQ_INFORM_H_START_EDGE_MASK 0x00008000U
#define RPP_IRQ_RIS_RPP_MAIN_PRE1_RPP_ACQ_INFORM_H_START_EDGE_SHIFT 15U
//! Slice: ris_rpp_main_pre1_rpp_acq_inform_v_start_edge:
// raw interrupt status: PRE1 vstart edge received.
#define RPP_IRQ_RIS_RPP_MAIN_PRE1_RPP_ACQ_INFORM_V_START_EDGE
#define RPP_IRQ_RIS_RPP_MAIN_PRE1_RPP_ACQ_INFORM_V_START_EDGE_MASK 0x00004000U
#define RPP_IRQ_RIS_RPP_MAIN_PRE1_RPP_ACQ_INFORM_V_START_EDGE_SHIFT 14U
//! Slice: ris_rpp_main_pre2_exm:
// raw interrupt status: PRE2 Exposure measurement complete.
#define RPP_IRQ_RIS_RPP_MAIN_PRE2_EXM
#define RPP_IRQ_RIS_RPP_MAIN_PRE2_EXM_MASK 0x00002000U
#define RPP_IRQ_RIS_RPP_MAIN_PRE2_EXM_SHIFT 13U
//! Slice: ris_rpp_main_pre2_rpp_hist:
// raw interrupt status: PRE2 Histogram measurement complete.
#define RPP_IRQ_RIS_RPP_MAIN_PRE2_RPP_HIST
#define RPP_IRQ_RIS_RPP_MAIN_PRE2_RPP_HIST_MASK 0x00001000U
#define RPP_IRQ_RIS_RPP_MAIN_PRE2_RPP_HIST_SHIFT 12U
//! Slice: ris_rpp_main_pre2_rpp_acq_frame_in:
// raw interrupt status: PRE2 Frame received at input.
#define RPP_IRQ_RIS_RPP_MAIN_PRE2_RPP_ACQ_FRAME_IN
#define RPP_IRQ_RIS_RPP_MAIN_PRE2_RPP_ACQ_FRAME_IN_MASK 0x00000800U
#define RPP_IRQ_RIS_RPP_MAIN_PRE2_RPP_ACQ_FRAME_IN_SHIFT 11U
//! Slice: ris_rpp_main_pre2_rpp_acq_inform_h_start_edge:
// raw interrupt status: PRE2 hstart edge received.
#define RPP_IRQ_RIS_RPP_MAIN_PRE2_RPP_ACQ_INFORM_H_START_EDGE
#define RPP_IRQ_RIS_RPP_MAIN_PRE2_RPP_ACQ_INFORM_H_START_EDGE_MASK 0x00000400U
#define RPP_IRQ_RIS_RPP_MAIN_PRE2_RPP_ACQ_INFORM_H_START_EDGE_SHIFT 10U
//! Slice: ris_rpp_main_pre2_rpp_acq_inform_v_start_edge:
// raw interrupt status: PRE2 vstart edge received.
#define RPP_IRQ_RIS_RPP_MAIN_PRE2_RPP_ACQ_INFORM_V_START_EDGE
#define RPP_IRQ_RIS_RPP_MAIN_PRE2_RPP_ACQ_INFORM_V_START_EDGE_MASK 0x00000200U
#define RPP_IRQ_RIS_RPP_MAIN_PRE2_RPP_ACQ_INFORM_V_START_EDGE_SHIFT 9U
//! Slice: ris_rpp_main_pre3_exm:
// raw interrupt status: PRE3 Exposure measurement complete.
#define RPP_IRQ_RIS_RPP_MAIN_PRE3_EXM
#define RPP_IRQ_RIS_RPP_MAIN_PRE3_EXM_MASK 0x00000100U
#define RPP_IRQ_RIS_RPP_MAIN_PRE3_EXM_SHIFT 8U
//! Slice: ris_rpp_main_pre3_rpp_hist:
// raw interrupt status: PRE3 Histogram measurement complete.
#define RPP_IRQ_RIS_RPP_MAIN_PRE3_RPP_HIST
#define RPP_IRQ_RIS_RPP_MAIN_PRE3_RPP_HIST_MASK 0x00000080U
#define RPP_IRQ_RIS_RPP_MAIN_PRE3_RPP_HIST_SHIFT 7U
//! Slice: ris_rpp_main_pre3_rpp_acq_frame_in:
// raw interrupt status: PRE3 Frame received at input.
#define RPP_IRQ_RIS_RPP_MAIN_PRE3_RPP_ACQ_FRAME_IN
#define RPP_IRQ_RIS_RPP_MAIN_PRE3_RPP_ACQ_FRAME_IN_MASK 0x00000040U
#define RPP_IRQ_RIS_RPP_MAIN_PRE3_RPP_ACQ_FRAME_IN_SHIFT 6U
//! Slice: ris_rpp_main_pre3_rpp_acq_inform_h_start_edge:
// raw interrupt status: PRE3 hstart edge received.
#define RPP_IRQ_RIS_RPP_MAIN_PRE3_RPP_ACQ_INFORM_H_START_EDGE
#define RPP_IRQ_RIS_RPP_MAIN_PRE3_RPP_ACQ_INFORM_H_START_EDGE_MASK 0x00000020U
#define RPP_IRQ_RIS_RPP_MAIN_PRE3_RPP_ACQ_INFORM_H_START_EDGE_SHIFT 5U
//! Slice: ris_rpp_main_pre3_rpp_acq_inform_v_start_edge:
// raw interrupt status: PRE3 vstart edge received.
#define RPP_IRQ_RIS_RPP_MAIN_PRE3_RPP_ACQ_INFORM_V_START_EDGE
#define RPP_IRQ_RIS_RPP_MAIN_PRE3_RPP_ACQ_INFORM_V_START_EDGE_MASK 0x00000010U
#define RPP_IRQ_RIS_RPP_MAIN_PRE3_RPP_ACQ_INFORM_V_START_EDGE_SHIFT 4U
//! Slice: ris_rpp_out_frame_out:
// raw interrupt status: RPP_OUT Output frame complete.
#define RPP_IRQ_RIS_RPP_OUT_FRAME_OUT
#define RPP_IRQ_RIS_RPP_OUT_FRAME_OUT_MASK 0x00000008U
#define RPP_IRQ_RIS_RPP_OUT_FRAME_OUT_SHIFT 3U
//! Slice: ris_rpp_out_off:
// raw interrupt status: RPP_OUT is off.
#define RPP_IRQ_RIS_RPP_OUT_OFF
#define RPP_IRQ_RIS_RPP_OUT_OFF_MASK 0x00000004U
#define RPP_IRQ_RIS_RPP_OUT_OFF_SHIFT 2U
//! Slice: ris_rpp_rmap_meas:
// raw interrupt status: RMAP measurement complete.
#define RPP_IRQ_RIS_RPP_RMAP_MEAS
#define RPP_IRQ_RIS_RPP_RMAP_MEAS_MASK 0x00000002U
#define RPP_IRQ_RIS_RPP_RMAP_MEAS_SHIFT 1U
//! Slice: ris_rpp_rmap:
// raw interrupt status: RMAP Output frame complete.
#define RPP_IRQ_RIS_RPP_RMAP
#define RPP_IRQ_RIS_RPP_RMAP_MASK 0x00000001U
#define RPP_IRQ_RIS_RPP_RMAP_SHIFT 0U

//! Register array: rpp_mis: Masked interrupt status (RPP_IRQ_BASE + 0x00000008 + n*0x14 (n=0..0))
//! Slice: mis_rpp_main_pre1_rpp_hist256:
// masked interrupt status: 256 bin Histogram write complete
#define RPP_IRQ_MIS_RPP_MAIN_PRE1_RPP_HIST256
#define RPP_IRQ_MIS_RPP_MAIN_PRE1_RPP_HIST256_MASK 0x08000000U
#define RPP_IRQ_MIS_RPP_MAIN_PRE1_RPP_HIST256_SHIFT 27U
//! Slice: mis_rpp_main_pre3_rpp_dpcc:
// masked interrupt status: PRE3 defect pixel output write complete
#define RPP_IRQ_MIS_RPP_MAIN_PRE3_RPP_DPCC
#define RPP_IRQ_MIS_RPP_MAIN_PRE3_RPP_DPCC_MASK 0x04000000U
#define RPP_IRQ_MIS_RPP_MAIN_PRE3_RPP_DPCC_SHIFT 26U
//! Slice: mis_rpp_main_pre2_rpp_dpcc:
// masked interrupt status: PRE2 defect pixel output write complete
#define RPP_IRQ_MIS_RPP_MAIN_PRE2_RPP_DPCC
#define RPP_IRQ_MIS_RPP_MAIN_PRE2_RPP_DPCC_MASK 0x02000000U
#define RPP_IRQ_MIS_RPP_MAIN_PRE2_RPP_DPCC_SHIFT 25U
//! Slice: mis_rpp_main_pre1_rpp_dpcc:
// masked interrupt status: PRE1 defect pixel output write complete
#define RPP_IRQ_MIS_RPP_MAIN_PRE1_RPP_DPCC
#define RPP_IRQ_MIS_RPP_MAIN_PRE1_RPP_DPCC_MASK 0x01000000U
#define RPP_IRQ_MIS_RPP_MAIN_PRE1_RPP_DPCC_SHIFT 24U
//! Slice: mis_rpp_mv_out_frame_out:
// masked interrupt status: MV_OUT write complete
#define RPP_IRQ_MIS_RPP_MV_OUT_FRAME_OUT
#define RPP_IRQ_MIS_RPP_MV_OUT_FRAME_OUT_MASK 0x00800000U
#define RPP_IRQ_MIS_RPP_MV_OUT_FRAME_OUT_SHIFT 23U
//! Slice: mis_rpp_mv_out_off:
// masked interrupt status: MV_OUT switched off
#define RPP_IRQ_MIS_RPP_MV_OUT_OFF
#define RPP_IRQ_MIS_RPP_MV_OUT_OFF_MASK 0x00400000U
#define RPP_IRQ_MIS_RPP_MV_OUT_OFF_SHIFT 22U
//! Slice: mis_rpp_main_post_awb_done:
// masked interrupt status: POST AWB measurement complete.
#define RPP_IRQ_MIS_RPP_MAIN_POST_AWB_DONE
#define RPP_IRQ_MIS_RPP_MAIN_POST_AWB_DONE_MASK 0x00200000U
#define RPP_IRQ_MIS_RPP_MAIN_POST_AWB_DONE_SHIFT 21U
//! Slice: mis_rpp_main_post_rpp_hist:
// masked interrupt status: POST Histogram measurement complete.
#define RPP_IRQ_MIS_RPP_MAIN_POST_RPP_HIST
#define RPP_IRQ_MIS_RPP_MAIN_POST_RPP_HIST_MASK 0x00100000U
#define RPP_IRQ_MIS_RPP_MAIN_POST_RPP_HIST_SHIFT 20U
//! Slice: mis_rpp_main_post_tm:
// masked interrupt status: POST Tone Mapping measurement complete.
#define RPP_IRQ_MIS_RPP_MAIN_POST_TM
#define RPP_IRQ_MIS_RPP_MAIN_POST_TM_MASK 0x00080000U
#define RPP_IRQ_MIS_RPP_MAIN_POST_TM_SHIFT 19U
//! Slice: mis_rpp_main_pre1_exm:
// masked interrupt status: PRE1 Exposure measurement complete.
#define RPP_IRQ_MIS_RPP_MAIN_PRE1_EXM
#define RPP_IRQ_MIS_RPP_MAIN_PRE1_EXM_MASK 0x00040000U
#define RPP_IRQ_MIS_RPP_MAIN_PRE1_EXM_SHIFT 18U
//! Slice: mis_rpp_main_pre1_rpp_hist:
// masked interrupt status: PRE1 Histogram measurement complete.
#define RPP_IRQ_MIS_RPP_MAIN_PRE1_RPP_HIST
#define RPP_IRQ_MIS_RPP_MAIN_PRE1_RPP_HIST_MASK 0x00020000U
#define RPP_IRQ_MIS_RPP_MAIN_PRE1_RPP_HIST_SHIFT 17U
//! Slice: mis_rpp_main_pre1_rpp_acq_frame_in:
// masked interrupt status: PRE1 Frame received at input.
#define RPP_IRQ_MIS_RPP_MAIN_PRE1_RPP_ACQ_FRAME_IN
#define RPP_IRQ_MIS_RPP_MAIN_PRE1_RPP_ACQ_FRAME_IN_MASK 0x00010000U
#define RPP_IRQ_MIS_RPP_MAIN_PRE1_RPP_ACQ_FRAME_IN_SHIFT 16U
//! Slice: mis_rpp_main_pre1_rpp_acq_inform_h_start_edge:
// masked interrupt status: PRE1 hstart edge received.
#define RPP_IRQ_MIS_RPP_MAIN_PRE1_RPP_ACQ_INFORM_H_START_EDGE
#define RPP_IRQ_MIS_RPP_MAIN_PRE1_RPP_ACQ_INFORM_H_START_EDGE_MASK 0x00008000U
#define RPP_IRQ_MIS_RPP_MAIN_PRE1_RPP_ACQ_INFORM_H_START_EDGE_SHIFT 15U
//! Slice: mis_rpp_main_pre1_rpp_acq_inform_v_start_edge:
// masked interrupt status: PRE1 vstart edge received.
#define RPP_IRQ_MIS_RPP_MAIN_PRE1_RPP_ACQ_INFORM_V_START_EDGE
#define RPP_IRQ_MIS_RPP_MAIN_PRE1_RPP_ACQ_INFORM_V_START_EDGE_MASK 0x00004000U
#define RPP_IRQ_MIS_RPP_MAIN_PRE1_RPP_ACQ_INFORM_V_START_EDGE_SHIFT 14U
//! Slice: mis_rpp_main_pre2_exm:
// masked interrupt status: PRE2 Exposure measurement complete.
#define RPP_IRQ_MIS_RPP_MAIN_PRE2_EXM
#define RPP_IRQ_MIS_RPP_MAIN_PRE2_EXM_MASK 0x00002000U
#define RPP_IRQ_MIS_RPP_MAIN_PRE2_EXM_SHIFT 13U
//! Slice: mis_rpp_main_pre2_rpp_hist:
// masked interrupt status: PRE2 Histogram measurement complete.
#define RPP_IRQ_MIS_RPP_MAIN_PRE2_RPP_HIST
#define RPP_IRQ_MIS_RPP_MAIN_PRE2_RPP_HIST_MASK 0x00001000U
#define RPP_IRQ_MIS_RPP_MAIN_PRE2_RPP_HIST_SHIFT 12U
//! Slice: mis_rpp_main_pre2_rpp_acq_frame_in:
// masked interrupt status: PRE2 Frame received at input.
#define RPP_IRQ_MIS_RPP_MAIN_PRE2_RPP_ACQ_FRAME_IN
#define RPP_IRQ_MIS_RPP_MAIN_PRE2_RPP_ACQ_FRAME_IN_MASK 0x00000800U
#define RPP_IRQ_MIS_RPP_MAIN_PRE2_RPP_ACQ_FRAME_IN_SHIFT 11U
//! Slice: mis_rpp_main_pre2_rpp_acq_inform_h_start_edge:
// masked interrupt status: PRE2 hstart edge received.
#define RPP_IRQ_MIS_RPP_MAIN_PRE2_RPP_ACQ_INFORM_H_START_EDGE
#define RPP_IRQ_MIS_RPP_MAIN_PRE2_RPP_ACQ_INFORM_H_START_EDGE_MASK 0x00000400U
#define RPP_IRQ_MIS_RPP_MAIN_PRE2_RPP_ACQ_INFORM_H_START_EDGE_SHIFT 10U
//! Slice: mis_rpp_main_pre2_rpp_acq_inform_v_start_edge:
// masked interrupt status: PRE2 vstart edge received.
#define RPP_IRQ_MIS_RPP_MAIN_PRE2_RPP_ACQ_INFORM_V_START_EDGE
#define RPP_IRQ_MIS_RPP_MAIN_PRE2_RPP_ACQ_INFORM_V_START_EDGE_MASK 0x00000200U
#define RPP_IRQ_MIS_RPP_MAIN_PRE2_RPP_ACQ_INFORM_V_START_EDGE_SHIFT 9U
//! Slice: mis_rpp_main_pre3_exm:
// masked interrupt status: PRE3 Exposure measurement complete.
#define RPP_IRQ_MIS_RPP_MAIN_PRE3_EXM
#define RPP_IRQ_MIS_RPP_MAIN_PRE3_EXM_MASK 0x00000100U
#define RPP_IRQ_MIS_RPP_MAIN_PRE3_EXM_SHIFT 8U
//! Slice: mis_rpp_main_pre3_rpp_hist:
// masked interrupt status: PRE3 Histogram measurement complete.
#define RPP_IRQ_MIS_RPP_MAIN_PRE3_RPP_HIST
#define RPP_IRQ_MIS_RPP_MAIN_PRE3_RPP_HIST_MASK 0x00000080U
#define RPP_IRQ_MIS_RPP_MAIN_PRE3_RPP_HIST_SHIFT 7U
//! Slice: mis_rpp_main_pre3_rpp_acq_frame_in:
// masked interrupt status: PRE3 Frame received at input.
#define RPP_IRQ_MIS_RPP_MAIN_PRE3_RPP_ACQ_FRAME_IN
#define RPP_IRQ_MIS_RPP_MAIN_PRE3_RPP_ACQ_FRAME_IN_MASK 0x00000040U
#define RPP_IRQ_MIS_RPP_MAIN_PRE3_RPP_ACQ_FRAME_IN_SHIFT 6U
//! Slice: mis_rpp_main_pre3_rpp_acq_inform_h_start_edge:
// masked interrupt status: PRE3 hstart edge received.
#define RPP_IRQ_MIS_RPP_MAIN_PRE3_RPP_ACQ_INFORM_H_START_EDGE
#define RPP_IRQ_MIS_RPP_MAIN_PRE3_RPP_ACQ_INFORM_H_START_EDGE_MASK 0x00000020U
#define RPP_IRQ_MIS_RPP_MAIN_PRE3_RPP_ACQ_INFORM_H_START_EDGE_SHIFT 5U
//! Slice: mis_rpp_main_pre3_rpp_acq_inform_v_start_edge:
// masked interrupt status: PRE3 vstart edge received.
#define RPP_IRQ_MIS_RPP_MAIN_PRE3_RPP_ACQ_INFORM_V_START_EDGE
#define RPP_IRQ_MIS_RPP_MAIN_PRE3_RPP_ACQ_INFORM_V_START_EDGE_MASK 0x00000010U
#define RPP_IRQ_MIS_RPP_MAIN_PRE3_RPP_ACQ_INFORM_V_START_EDGE_SHIFT 4U
//! Slice: mis_rpp_out_frame_out:
// masked interrupt status: RPP_OUT Output frame complete.
#define RPP_IRQ_MIS_RPP_OUT_FRAME_OUT
#define RPP_IRQ_MIS_RPP_OUT_FRAME_OUT_MASK 0x00000008U
#define RPP_IRQ_MIS_RPP_OUT_FRAME_OUT_SHIFT 3U
//! Slice: mis_rpp_out_off:
// masked interrupt status: RPP_OUT is off.
#define RPP_IRQ_MIS_RPP_OUT_OFF
#define RPP_IRQ_MIS_RPP_OUT_OFF_MASK 0x00000004U
#define RPP_IRQ_MIS_RPP_OUT_OFF_SHIFT 2U
//! Slice: mis_rpp_rmap_meas:
// masked interrupt status: RMAP measurement complete.
#define RPP_IRQ_MIS_RPP_RMAP_MEAS
#define RPP_IRQ_MIS_RPP_RMAP_MEAS_MASK 0x00000002U
#define RPP_IRQ_MIS_RPP_RMAP_MEAS_SHIFT 1U
//! Slice: mis_rpp_rmap:
// masked interrupt status: RMAP Output frame complete.
#define RPP_IRQ_MIS_RPP_RMAP
#define RPP_IRQ_MIS_RPP_RMAP_MASK 0x00000001U
#define RPP_IRQ_MIS_RPP_RMAP_SHIFT 0U

//! Register array: rpp_isc: Interrupt status clear (RPP_IRQ_BASE + 0x0000000C + n*0x14 (n=0..0))
//! Slice: isc_rpp_main_pre1_rpp_hist256:
// clear interrupt status: 256 bin Histogram write complete
#define RPP_IRQ_ISC_RPP_MAIN_PRE1_RPP_HIST256
#define RPP_IRQ_ISC_RPP_MAIN_PRE1_RPP_HIST256_MASK 0x08000000U
#define RPP_IRQ_ISC_RPP_MAIN_PRE1_RPP_HIST256_SHIFT 27U
//! Slice: isc_rpp_main_pre3_rpp_dpcc:
// clear interrupt status: PRE3 defect pixel output write complete
#define RPP_IRQ_ISC_RPP_MAIN_PRE3_RPP_DPCC
#define RPP_IRQ_ISC_RPP_MAIN_PRE3_RPP_DPCC_MASK 0x04000000U
#define RPP_IRQ_ISC_RPP_MAIN_PRE3_RPP_DPCC_SHIFT 26U
//! Slice: isc_rpp_main_pre2_rpp_dpcc:
// clear interrupt status: PRE2 defect pixel output write complete
#define RPP_IRQ_ISC_RPP_MAIN_PRE2_RPP_DPCC
#define RPP_IRQ_ISC_RPP_MAIN_PRE2_RPP_DPCC_MASK 0x02000000U
#define RPP_IRQ_ISC_RPP_MAIN_PRE2_RPP_DPCC_SHIFT 25U
//! Slice: isc_rpp_main_pre1_rpp_dpcc:
// clear interrupt status: PRE1 defect pixel output write complete
#define RPP_IRQ_ISC_RPP_MAIN_PRE1_RPP_DPCC
#define RPP_IRQ_ISC_RPP_MAIN_PRE1_RPP_DPCC_MASK 0x01000000U
#define RPP_IRQ_ISC_RPP_MAIN_PRE1_RPP_DPCC_SHIFT 24U
//! Slice: isc_rpp_mv_out_frame_out:
// clear interrupt status: MV_OUT write complete
#define RPP_IRQ_ISC_RPP_MV_OUT_FRAME_OUT
#define RPP_IRQ_ISC_RPP_MV_OUT_FRAME_OUT_MASK 0x00800000U
#define RPP_IRQ_ISC_RPP_MV_OUT_FRAME_OUT_SHIFT 23U
//! Slice: isc_rpp_mv_out_off:
// clear interrupt status: MV_OUT switched off
#define RPP_IRQ_ISC_RPP_MV_OUT_OFF
#define RPP_IRQ_ISC_RPP_MV_OUT_OFF_MASK 0x00400000U
#define RPP_IRQ_ISC_RPP_MV_OUT_OFF_SHIFT 22U
//! Slice: isc_rpp_main_post_awb_done:
// clear interrupt status: POST AWB measurement complete.
#define RPP_IRQ_ISC_RPP_MAIN_POST_AWB_DONE
#define RPP_IRQ_ISC_RPP_MAIN_POST_AWB_DONE_MASK 0x00200000U
#define RPP_IRQ_ISC_RPP_MAIN_POST_AWB_DONE_SHIFT 21U
//! Slice: isc_rpp_main_post_rpp_hist:
// clear interrupt status: POST Histogram measurement complete.
#define RPP_IRQ_ISC_RPP_MAIN_POST_RPP_HIST
#define RPP_IRQ_ISC_RPP_MAIN_POST_RPP_HIST_MASK 0x00100000U
#define RPP_IRQ_ISC_RPP_MAIN_POST_RPP_HIST_SHIFT 20U
//! Slice: isc_rpp_main_post_tm:
// clear interrupt status: POST Tone Mapping measurement complete.
#define RPP_IRQ_ISC_RPP_MAIN_POST_TM
#define RPP_IRQ_ISC_RPP_MAIN_POST_TM_MASK 0x00080000U
#define RPP_IRQ_ISC_RPP_MAIN_POST_TM_SHIFT 19U
//! Slice: isc_rpp_main_pre1_exm:
// clear interrupt status: PRE1 Exposure measurement complete.
#define RPP_IRQ_ISC_RPP_MAIN_PRE1_EXM
#define RPP_IRQ_ISC_RPP_MAIN_PRE1_EXM_MASK 0x00040000U
#define RPP_IRQ_ISC_RPP_MAIN_PRE1_EXM_SHIFT 18U
//! Slice: isc_rpp_main_pre1_rpp_hist:
// clear interrupt status: PRE1 Histogram measurement complete.
#define RPP_IRQ_ISC_RPP_MAIN_PRE1_RPP_HIST
#define RPP_IRQ_ISC_RPP_MAIN_PRE1_RPP_HIST_MASK 0x00020000U
#define RPP_IRQ_ISC_RPP_MAIN_PRE1_RPP_HIST_SHIFT 17U
//! Slice: isc_rpp_main_pre1_rpp_acq_frame_in:
// clear interrupt status: PRE1 Frame received at input.
#define RPP_IRQ_ISC_RPP_MAIN_PRE1_RPP_ACQ_FRAME_IN
#define RPP_IRQ_ISC_RPP_MAIN_PRE1_RPP_ACQ_FRAME_IN_MASK 0x00010000U
#define RPP_IRQ_ISC_RPP_MAIN_PRE1_RPP_ACQ_FRAME_IN_SHIFT 16U
//! Slice: isc_rpp_main_pre1_rpp_acq_inform_h_start_edge:
// clear interrupt status: PRE1 hstart edge received.
#define RPP_IRQ_ISC_RPP_MAIN_PRE1_RPP_ACQ_INFORM_H_START_EDGE
#define RPP_IRQ_ISC_RPP_MAIN_PRE1_RPP_ACQ_INFORM_H_START_EDGE_MASK 0x00008000U
#define RPP_IRQ_ISC_RPP_MAIN_PRE1_RPP_ACQ_INFORM_H_START_EDGE_SHIFT 15U
//! Slice: isc_rpp_main_pre1_rpp_acq_inform_v_start_edge:
// clear interrupt status: PRE1 vstart edge received.
#define RPP_IRQ_ISC_RPP_MAIN_PRE1_RPP_ACQ_INFORM_V_START_EDGE
#define RPP_IRQ_ISC_RPP_MAIN_PRE1_RPP_ACQ_INFORM_V_START_EDGE_MASK 0x00004000U
#define RPP_IRQ_ISC_RPP_MAIN_PRE1_RPP_ACQ_INFORM_V_START_EDGE_SHIFT 14U
//! Slice: isc_rpp_main_pre2_exm:
// clear interrupt status: PRE2 Exposure measurement complete.
#define RPP_IRQ_ISC_RPP_MAIN_PRE2_EXM
#define RPP_IRQ_ISC_RPP_MAIN_PRE2_EXM_MASK 0x00002000U
#define RPP_IRQ_ISC_RPP_MAIN_PRE2_EXM_SHIFT 13U
//! Slice: isc_rpp_main_pre2_rpp_hist:
// clear interrupt status: PRE2 Histogram measurement complete.
#define RPP_IRQ_ISC_RPP_MAIN_PRE2_RPP_HIST
#define RPP_IRQ_ISC_RPP_MAIN_PRE2_RPP_HIST_MASK 0x00001000U
#define RPP_IRQ_ISC_RPP_MAIN_PRE2_RPP_HIST_SHIFT 12U
//! Slice: isc_rpp_main_pre2_rpp_acq_frame_in:
// clear interrupt status: PRE2 Frame received at input.
#define RPP_IRQ_ISC_RPP_MAIN_PRE2_RPP_ACQ_FRAME_IN
#define RPP_IRQ_ISC_RPP_MAIN_PRE2_RPP_ACQ_FRAME_IN_MASK 0x00000800U
#define RPP_IRQ_ISC_RPP_MAIN_PRE2_RPP_ACQ_FRAME_IN_SHIFT 11U
//! Slice: isc_rpp_main_pre2_rpp_acq_inform_h_start_edge:
// clear interrupt status: PRE2 hstart edge received.
#define RPP_IRQ_ISC_RPP_MAIN_PRE2_RPP_ACQ_INFORM_H_START_EDGE
#define RPP_IRQ_ISC_RPP_MAIN_PRE2_RPP_ACQ_INFORM_H_START_EDGE_MASK 0x00000400U
#define RPP_IRQ_ISC_RPP_MAIN_PRE2_RPP_ACQ_INFORM_H_START_EDGE_SHIFT 10U
//! Slice: isc_rpp_main_pre2_rpp_acq_inform_v_start_edge:
// clear interrupt status: PRE2 vstart edge received.
#define RPP_IRQ_ISC_RPP_MAIN_PRE2_RPP_ACQ_INFORM_V_START_EDGE
#define RPP_IRQ_ISC_RPP_MAIN_PRE2_RPP_ACQ_INFORM_V_START_EDGE_MASK 0x00000200U
#define RPP_IRQ_ISC_RPP_MAIN_PRE2_RPP_ACQ_INFORM_V_START_EDGE_SHIFT 9U
//! Slice: isc_rpp_main_pre3_exm:
// clear interrupt status: PRE3 Exposure measurement complete.
#define RPP_IRQ_ISC_RPP_MAIN_PRE3_EXM
#define RPP_IRQ_ISC_RPP_MAIN_PRE3_EXM_MASK 0x00000100U
#define RPP_IRQ_ISC_RPP_MAIN_PRE3_EXM_SHIFT 8U
//! Slice: isc_rpp_main_pre3_rpp_hist:
// clear interrupt status: PRE3 Histogram measurement complete.
#define RPP_IRQ_ISC_RPP_MAIN_PRE3_RPP_HIST
#define RPP_IRQ_ISC_RPP_MAIN_PRE3_RPP_HIST_MASK 0x00000080U
#define RPP_IRQ_ISC_RPP_MAIN_PRE3_RPP_HIST_SHIFT 7U
//! Slice: isc_rpp_main_pre3_rpp_acq_frame_in:
// clear interrupt status: PRE3 Frame received at input.
#define RPP_IRQ_ISC_RPP_MAIN_PRE3_RPP_ACQ_FRAME_IN
#define RPP_IRQ_ISC_RPP_MAIN_PRE3_RPP_ACQ_FRAME_IN_MASK 0x00000040U
#define RPP_IRQ_ISC_RPP_MAIN_PRE3_RPP_ACQ_FRAME_IN_SHIFT 6U
//! Slice: isc_rpp_main_pre3_rpp_acq_inform_h_start_edge:
// clear interrupt status: PRE3 hstart edge received.
#define RPP_IRQ_ISC_RPP_MAIN_PRE3_RPP_ACQ_INFORM_H_START_EDGE
#define RPP_IRQ_ISC_RPP_MAIN_PRE3_RPP_ACQ_INFORM_H_START_EDGE_MASK 0x00000020U
#define RPP_IRQ_ISC_RPP_MAIN_PRE3_RPP_ACQ_INFORM_H_START_EDGE_SHIFT 5U
//! Slice: isc_rpp_main_pre3_rpp_acq_inform_v_start_edge:
// clear interrupt status: PRE3 vstart edge received.
#define RPP_IRQ_ISC_RPP_MAIN_PRE3_RPP_ACQ_INFORM_V_START_EDGE
#define RPP_IRQ_ISC_RPP_MAIN_PRE3_RPP_ACQ_INFORM_V_START_EDGE_MASK 0x00000010U
#define RPP_IRQ_ISC_RPP_MAIN_PRE3_RPP_ACQ_INFORM_V_START_EDGE_SHIFT 4U
//! Slice: isc_rpp_out_frame_out:
// clear interrupt status: RPP_OUT Output frame complete.
#define RPP_IRQ_ISC_RPP_OUT_FRAME_OUT
#define RPP_IRQ_ISC_RPP_OUT_FRAME_OUT_MASK 0x00000008U
#define RPP_IRQ_ISC_RPP_OUT_FRAME_OUT_SHIFT 3U
//! Slice: isc_rpp_out_off:
// clear interrupt status: RPP_OUT is off.
#define RPP_IRQ_ISC_RPP_OUT_OFF
#define RPP_IRQ_ISC_RPP_OUT_OFF_MASK 0x00000004U
#define RPP_IRQ_ISC_RPP_OUT_OFF_SHIFT 2U
//! Slice: isc_rpp_rmap_meas:
// clear interrupt status: RMAP measurement complete.
#define RPP_IRQ_ISC_RPP_RMAP_MEAS
#define RPP_IRQ_ISC_RPP_RMAP_MEAS_MASK 0x00000002U
#define RPP_IRQ_ISC_RPP_RMAP_MEAS_SHIFT 1U
//! Slice: isc_rpp_rmap:
// clear interrupt status: RMAP Output frame complete.
#define RPP_IRQ_ISC_RPP_RMAP
#define RPP_IRQ_ISC_RPP_RMAP_MASK 0x00000001U
#define RPP_IRQ_ISC_RPP_RMAP_SHIFT 0U

//! Register array: rpp_iss: Interrupt status set (RPP_IRQ_BASE + 0x00000010 + n*0x14 (n=0..0))
//! Slice: iss_rpp_main_pre1_rpp_hist256:
// set interrupt status: 256 bin Histogram write complete
#define RPP_IRQ_ISS_RPP_MAIN_PRE1_RPP_HIST256
#define RPP_IRQ_ISS_RPP_MAIN_PRE1_RPP_HIST256_MASK 0x08000000U
#define RPP_IRQ_ISS_RPP_MAIN_PRE1_RPP_HIST256_SHIFT 27U
//! Slice: iss_rpp_main_pre3_rpp_dpcc:
// set interrupt status: PRE3 defect pixel output write complete
#define RPP_IRQ_ISS_RPP_MAIN_PRE3_RPP_DPCC
#define RPP_IRQ_ISS_RPP_MAIN_PRE3_RPP_DPCC_MASK 0x04000000U
#define RPP_IRQ_ISS_RPP_MAIN_PRE3_RPP_DPCC_SHIFT 26U
//! Slice: iss_rpp_main_pre2_rpp_dpcc:
// set interrupt status: PRE2 defect pixel output write complete
#define RPP_IRQ_ISS_RPP_MAIN_PRE2_RPP_DPCC
#define RPP_IRQ_ISS_RPP_MAIN_PRE2_RPP_DPCC_MASK 0x02000000U
#define RPP_IRQ_ISS_RPP_MAIN_PRE2_RPP_DPCC_SHIFT 25U
//! Slice: iss_rpp_main_pre1_rpp_dpcc:
// set interrupt status: PRE1 defect pixel output write complete
#define RPP_IRQ_ISS_RPP_MAIN_PRE1_RPP_DPCC
#define RPP_IRQ_ISS_RPP_MAIN_PRE1_RPP_DPCC_MASK 0x01000000U
#define RPP_IRQ_ISS_RPP_MAIN_PRE1_RPP_DPCC_SHIFT 24U
//! Slice: iss_rpp_mv_out_frame_out:
// set interrupt status: MV_OUT write complete
#define RPP_IRQ_ISS_RPP_MV_OUT_FRAME_OUT
#define RPP_IRQ_ISS_RPP_MV_OUT_FRAME_OUT_MASK 0x00800000U
#define RPP_IRQ_ISS_RPP_MV_OUT_FRAME_OUT_SHIFT 23U
//! Slice: iss_rpp_mv_out_off:
// set interrupt status: MV_OUT switched off
#define RPP_IRQ_ISS_RPP_MV_OUT_OFF
#define RPP_IRQ_ISS_RPP_MV_OUT_OFF_MASK 0x00400000U
#define RPP_IRQ_ISS_RPP_MV_OUT_OFF_SHIFT 22U
//! Slice: iss_rpp_main_post_awb_done:
// set interrupt status: POST AWB measurement complete.
#define RPP_IRQ_ISS_RPP_MAIN_POST_AWB_DONE
#define RPP_IRQ_ISS_RPP_MAIN_POST_AWB_DONE_MASK 0x00200000U
#define RPP_IRQ_ISS_RPP_MAIN_POST_AWB_DONE_SHIFT 21U
//! Slice: iss_rpp_main_post_rpp_hist:
// set interrupt status: POST Histogram measurement complete.
#define RPP_IRQ_ISS_RPP_MAIN_POST_RPP_HIST
#define RPP_IRQ_ISS_RPP_MAIN_POST_RPP_HIST_MASK 0x00100000U
#define RPP_IRQ_ISS_RPP_MAIN_POST_RPP_HIST_SHIFT 20U
//! Slice: iss_rpp_main_post_tm:
// set interrupt status: POST Tone Mapping measurement complete.
#define RPP_IRQ_ISS_RPP_MAIN_POST_TM
#define RPP_IRQ_ISS_RPP_MAIN_POST_TM_MASK 0x00080000U
#define RPP_IRQ_ISS_RPP_MAIN_POST_TM_SHIFT 19U
//! Slice: iss_rpp_main_pre1_exm:
// set interrupt status: PRE1 Exposure measurement complete.
#define RPP_IRQ_ISS_RPP_MAIN_PRE1_EXM
#define RPP_IRQ_ISS_RPP_MAIN_PRE1_EXM_MASK 0x00040000U
#define RPP_IRQ_ISS_RPP_MAIN_PRE1_EXM_SHIFT 18U
//! Slice: iss_rpp_main_pre1_rpp_hist:
// set interrupt status: PRE1 Histogram measurement complete.
#define RPP_IRQ_ISS_RPP_MAIN_PRE1_RPP_HIST
#define RPP_IRQ_ISS_RPP_MAIN_PRE1_RPP_HIST_MASK 0x00020000U
#define RPP_IRQ_ISS_RPP_MAIN_PRE1_RPP_HIST_SHIFT 17U
//! Slice: iss_rpp_main_pre1_rpp_acq_frame_in:
// set interrupt status: PRE1 Frame received at input.
#define RPP_IRQ_ISS_RPP_MAIN_PRE1_RPP_ACQ_FRAME_IN
#define RPP_IRQ_ISS_RPP_MAIN_PRE1_RPP_ACQ_FRAME_IN_MASK 0x00010000U
#define RPP_IRQ_ISS_RPP_MAIN_PRE1_RPP_ACQ_FRAME_IN_SHIFT 16U
//! Slice: iss_rpp_main_pre1_rpp_acq_inform_h_start_edge:
// set interrupt status: PRE1 hstart edge received.
#define RPP_IRQ_ISS_RPP_MAIN_PRE1_RPP_ACQ_INFORM_H_START_EDGE
#define RPP_IRQ_ISS_RPP_MAIN_PRE1_RPP_ACQ_INFORM_H_START_EDGE_MASK 0x00008000U
#define RPP_IRQ_ISS_RPP_MAIN_PRE1_RPP_ACQ_INFORM_H_START_EDGE_SHIFT 15U
//! Slice: iss_rpp_main_pre1_rpp_acq_inform_v_start_edge:
// set interrupt status: PRE1 vstart edge received.
#define RPP_IRQ_ISS_RPP_MAIN_PRE1_RPP_ACQ_INFORM_V_START_EDGE
#define RPP_IRQ_ISS_RPP_MAIN_PRE1_RPP_ACQ_INFORM_V_START_EDGE_MASK 0x00004000U
#define RPP_IRQ_ISS_RPP_MAIN_PRE1_RPP_ACQ_INFORM_V_START_EDGE_SHIFT 14U
//! Slice: iss_rpp_main_pre2_exm:
// set interrupt status: PRE2 Exposure measurement complete.
#define RPP_IRQ_ISS_RPP_MAIN_PRE2_EXM
#define RPP_IRQ_ISS_RPP_MAIN_PRE2_EXM_MASK 0x00002000U
#define RPP_IRQ_ISS_RPP_MAIN_PRE2_EXM_SHIFT 13U
//! Slice: iss_rpp_main_pre2_rpp_hist:
// set interrupt status: PRE2 Histogram measurement complete.
#define RPP_IRQ_ISS_RPP_MAIN_PRE2_RPP_HIST
#define RPP_IRQ_ISS_RPP_MAIN_PRE2_RPP_HIST_MASK 0x00001000U
#define RPP_IRQ_ISS_RPP_MAIN_PRE2_RPP_HIST_SHIFT 12U
//! Slice: iss_rpp_main_pre2_rpp_acq_frame_in:
// set interrupt status: PRE2 Frame received at input.
#define RPP_IRQ_ISS_RPP_MAIN_PRE2_RPP_ACQ_FRAME_IN
#define RPP_IRQ_ISS_RPP_MAIN_PRE2_RPP_ACQ_FRAME_IN_MASK 0x00000800U
#define RPP_IRQ_ISS_RPP_MAIN_PRE2_RPP_ACQ_FRAME_IN_SHIFT 11U
//! Slice: iss_rpp_main_pre2_rpp_acq_inform_h_start_edge:
// set interrupt status: PRE2 hstart edge received.
#define RPP_IRQ_ISS_RPP_MAIN_PRE2_RPP_ACQ_INFORM_H_START_EDGE
#define RPP_IRQ_ISS_RPP_MAIN_PRE2_RPP_ACQ_INFORM_H_START_EDGE_MASK 0x00000400U
#define RPP_IRQ_ISS_RPP_MAIN_PRE2_RPP_ACQ_INFORM_H_START_EDGE_SHIFT 10U
//! Slice: iss_rpp_main_pre2_rpp_acq_inform_v_start_edge:
// set interrupt status: PRE2 vstart edge received.
#define RPP_IRQ_ISS_RPP_MAIN_PRE2_RPP_ACQ_INFORM_V_START_EDGE
#define RPP_IRQ_ISS_RPP_MAIN_PRE2_RPP_ACQ_INFORM_V_START_EDGE_MASK 0x00000200U
#define RPP_IRQ_ISS_RPP_MAIN_PRE2_RPP_ACQ_INFORM_V_START_EDGE_SHIFT 9U
//! Slice: iss_rpp_main_pre3_exm:
// set interrupt status: PRE3 Exposure measurement complete.
#define RPP_IRQ_ISS_RPP_MAIN_PRE3_EXM
#define RPP_IRQ_ISS_RPP_MAIN_PRE3_EXM_MASK 0x00000100U
#define RPP_IRQ_ISS_RPP_MAIN_PRE3_EXM_SHIFT 8U
//! Slice: iss_rpp_main_pre3_rpp_hist:
// set interrupt status: PRE3 Histogram measurement complete.
#define RPP_IRQ_ISS_RPP_MAIN_PRE3_RPP_HIST
#define RPP_IRQ_ISS_RPP_MAIN_PRE3_RPP_HIST_MASK 0x00000080U
#define RPP_IRQ_ISS_RPP_MAIN_PRE3_RPP_HIST_SHIFT 7U
//! Slice: iss_rpp_main_pre3_rpp_acq_frame_in:
// set interrupt status: PRE3 Frame received at input.
#define RPP_IRQ_ISS_RPP_MAIN_PRE3_RPP_ACQ_FRAME_IN
#define RPP_IRQ_ISS_RPP_MAIN_PRE3_RPP_ACQ_FRAME_IN_MASK 0x00000040U
#define RPP_IRQ_ISS_RPP_MAIN_PRE3_RPP_ACQ_FRAME_IN_SHIFT 6U
//! Slice: iss_rpp_main_pre3_rpp_acq_inform_h_start_edge:
// set interrupt status: PRE3 hstart edge received.
#define RPP_IRQ_ISS_RPP_MAIN_PRE3_RPP_ACQ_INFORM_H_START_EDGE
#define RPP_IRQ_ISS_RPP_MAIN_PRE3_RPP_ACQ_INFORM_H_START_EDGE_MASK 0x00000020U
#define RPP_IRQ_ISS_RPP_MAIN_PRE3_RPP_ACQ_INFORM_H_START_EDGE_SHIFT 5U
//! Slice: iss_rpp_main_pre3_rpp_acq_inform_v_start_edge:
// set interrupt status: PRE3 vstart edge received.
#define RPP_IRQ_ISS_RPP_MAIN_PRE3_RPP_ACQ_INFORM_V_START_EDGE
#define RPP_IRQ_ISS_RPP_MAIN_PRE3_RPP_ACQ_INFORM_V_START_EDGE_MASK 0x00000010U
#define RPP_IRQ_ISS_RPP_MAIN_PRE3_RPP_ACQ_INFORM_V_START_EDGE_SHIFT 4U
//! Slice: iss_rpp_out_frame_out:
// set interrupt status: RPP_OUT Output frame complete.
#define RPP_IRQ_ISS_RPP_OUT_FRAME_OUT
#define RPP_IRQ_ISS_RPP_OUT_FRAME_OUT_MASK 0x00000008U
#define RPP_IRQ_ISS_RPP_OUT_FRAME_OUT_SHIFT 3U
//! Slice: iss_rpp_out_off:
// set interrupt status: RPP_OUT is off.
#define RPP_IRQ_ISS_RPP_OUT_OFF
#define RPP_IRQ_ISS_RPP_OUT_OFF_MASK 0x00000004U
#define RPP_IRQ_ISS_RPP_OUT_OFF_SHIFT 2U
//! Slice: iss_rpp_rmap_meas:
// set interrupt status: RMAP measurement complete.
#define RPP_IRQ_ISS_RPP_RMAP_MEAS
#define RPP_IRQ_ISS_RPP_RMAP_MEAS_MASK 0x00000002U
#define RPP_IRQ_ISS_RPP_RMAP_MEAS_SHIFT 1U
//! Slice: iss_rpp_rmap:
// set interrupt status: RMAP Output frame complete.
#define RPP_IRQ_ISS_RPP_RMAP
#define RPP_IRQ_ISS_RPP_RMAP_MASK 0x00000001U
#define RPP_IRQ_ISS_RPP_RMAP_SHIFT 0U

#endif /* __RPP_IRQ_FUN_REGS_MASK_H__ */
/*************************** EOF **************************************/
