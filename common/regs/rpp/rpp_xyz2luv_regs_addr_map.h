/*
//-----------------------------------------------------------------------------
// This is an unpublished work, the copyright in which vests in 
// DreamChip Technologies GmbH. The information contained herein is the property 
// of DreamChip Technologies GmbH and is supplied without liability for errors or 
// omissions. No part may be reproduced or used except as authorized by 
// contract or other written permission.
// Copyright(c) DreamChip Technologies GmbH, 2022. All rights reserved.
//-----------------------------------------------------------------------------
//
//       N-1         _ __             _
//        __        |  ||  /    1 \    |
//  X  = \    x  cos|  -- ( n + -  ) k |   k = 0,...,N-1
//   k   /__   n    |_  N  \    2 /   _|
//       n=0
//
//-----------------------------------------------------------------------------
// Module   : rpp_xyz2luv
//
// Purpose  : Register address map definition
//
// Creator  : DCT
//
// Comments : automatically generated by sig (4.0.5)
//            !!!!! DO NOT EDIT THIS FILE !!!!!
//-----------------------------------------------------------------------------
 * file rpp_xyz2luv_regs_addr_map.h
 *
 * Programming Language: C
 * Runtime Environment : ARM, WIN32, Linux, Solaris
 *
 * Description:
 *  generate full address map based on external definition of *_BASE macro
 */
/*****************************************************************************/

#ifndef __RPP_XYZ2LUV_REGS_ADDR_MAP_H__
#define __RPP_XYZ2LUV_REGS_ADDR_MAP_H__


#ifndef RPP_XYZ2LUV_BASE
#error You must #include the global memory map header file before rpp_xyz2luv_regs_addr_map.h and it must define RPP_XYZ2LUV_BASE
#endif /* RPP_XYZ2LUV_BASE */



// register group: rpp_xyz2luv_registers
#define RPP_RPP_XYZ2LUV_VERSION_REG (RPP_XYZ2LUV_BASE + 0x00000000)
#define RPP_RPP_XYZ2LUV_U_REF_REG (RPP_XYZ2LUV_BASE + 0x00000004)
#define RPP_RPP_XYZ2LUV_V_REF_REG (RPP_XYZ2LUV_BASE + 0x00000008)
#define RPP_RPP_XYZ2LUV_LUMA_OUT_FAC_REG (RPP_XYZ2LUV_BASE + 0x0000000C)
#define RPP_RPP_XYZ2LUV_CHROMA_OUT_FAC_REG (RPP_XYZ2LUV_BASE + 0x00000010)

// last index
#define RPP_XYZ2LUV_REGIDX_LAST      (RPP_XYZ2LUV_BASE + 0x00000014)
#define RPP_XYZ2LUV_REGIDX_LAST_OFFS 0x00000014

#endif /* __RPP_XYZ2LUV_REGS_ADDR_MAP_H__ */ 
/*************************** EOF **************************************/
