/*
//-----------------------------------------------------------------------------
// This is an unpublished work, the copyright in which vests in 
// DreamChip Technologies GmbH. The information contained herein is the property 
// of DreamChip Technologies GmbH and is supplied without liability for errors or 
// omissions. No part may be reproduced or used except as authorized by 
// contract or other written permission.
// Copyright(c) DreamChip Technologies GmbH, 2022. All rights reserved.
//-----------------------------------------------------------------------------
//
//       N-1         _ __             _
//        __        |  ||  /    1 \    |
//  X  = \    x  cos|  -- ( n + -  ) k |   k = 0,...,N-1
//   k   /__   n    |_  N  \    2 /   _|
//       n=0
//
//-----------------------------------------------------------------------------
// Module   : rpp_hist256
//
// Purpose  : Register address map definition
//
// Creator  : DCT
//
// Comments : automatically generated by sig (4.0.5)
//            !!!!! DO NOT EDIT THIS FILE !!!!!
//-----------------------------------------------------------------------------
 * file rpp_hist256_regs_addr_map.h
 *
 * Programming Language: C
 * Runtime Environment : ARM, WIN32, Linux, Solaris
 *
 * Description:
 *  generate full address map based on external definition of *_BASE macro
 */
/*****************************************************************************/

#ifndef __RPP_HIST256_REGS_ADDR_MAP_H__
#define __RPP_HIST256_REGS_ADDR_MAP_H__


#ifndef RPP_HIST256_BASE
#error You must #include the global memory map header file before rpp_hist256_regs_addr_map.h and it must define RPP_HIST256_BASE
#endif /* RPP_HIST256_BASE */



// register group: isp_histogram_module_registers
#define RPP_RPP_HIST256_VERSION_REG (RPP_HIST256_BASE + 0x00000000)
#define RPP_RPP_HIST256_MODE_REG (RPP_HIST256_BASE + 0x00000004)
#define RPP_RPP_HIST256_CHANNEL_SEL_REG (RPP_HIST256_BASE + 0x00000008)
#define RPP_RPP_HIST256_H_OFFS_REG (RPP_HIST256_BASE + 0x0000000C)
#define RPP_RPP_HIST256_V_OFFS_REG (RPP_HIST256_BASE + 0x00000010)
#define RPP_RPP_HIST256_H_SIZE_REG (RPP_HIST256_BASE + 0x00000014)
#define RPP_RPP_HIST256_V_SIZE_REG (RPP_HIST256_BASE + 0x00000018)
#define RPP_RPP_HIST256_SAMPLE_OFFSET_REG (RPP_HIST256_BASE + 0x0000001C)
#define RPP_RPP_HIST256_SAMPLE_SCALE_REG (RPP_HIST256_BASE + 0x00000020)
#define RPP_RPP_HIST256_MEAS_RESULT_ADDR_AUTOINCR_REG (RPP_HIST256_BASE + 0x00000024)
#define RPP_RPP_HIST256_MEAS_RESULT_ADDR_REG (RPP_HIST256_BASE + 0x00000028)
#define RPP_RPP_HIST256_MEAS_RESULT_DATA_REG (RPP_HIST256_BASE + 0x0000002C)
#define RPP_RPP_HIST256_LOG_ENABLE_REG (RPP_HIST256_BASE + 0x00000030)
#define RPP_RPP_HIST256_LOG_DX_LO_REG (RPP_HIST256_BASE + 0x00000034)
#define RPP_RPP_HIST256_LOG_DX_HI_REG (RPP_HIST256_BASE + 0x00000038)
#define RPP_RPP_HIST256_Y__0_REG (RPP_HIST256_BASE + 0x00000040)
#define RPP_RPP_HIST256_Y__1_REG (RPP_HIST256_BASE + 0x00000044)
#define RPP_RPP_HIST256_Y__2_REG (RPP_HIST256_BASE + 0x00000048)
#define RPP_RPP_HIST256_Y__3_REG (RPP_HIST256_BASE + 0x0000004C)
#define RPP_RPP_HIST256_Y__4_REG (RPP_HIST256_BASE + 0x00000050)
#define RPP_RPP_HIST256_Y__5_REG (RPP_HIST256_BASE + 0x00000054)
#define RPP_RPP_HIST256_Y__6_REG (RPP_HIST256_BASE + 0x00000058)
#define RPP_RPP_HIST256_Y__7_REG (RPP_HIST256_BASE + 0x0000005C)
#define RPP_RPP_HIST256_Y__8_REG (RPP_HIST256_BASE + 0x00000060)
#define RPP_RPP_HIST256_Y__9_REG (RPP_HIST256_BASE + 0x00000064)
#define RPP_RPP_HIST256_Y__10_REG (RPP_HIST256_BASE + 0x00000068)
#define RPP_RPP_HIST256_Y__11_REG (RPP_HIST256_BASE + 0x0000006C)
#define RPP_RPP_HIST256_Y__12_REG (RPP_HIST256_BASE + 0x00000070)
#define RPP_RPP_HIST256_Y__13_REG (RPP_HIST256_BASE + 0x00000074)
#define RPP_RPP_HIST256_Y__14_REG (RPP_HIST256_BASE + 0x00000078)
#define RPP_RPP_HIST256_Y__15_REG (RPP_HIST256_BASE + 0x0000007C)
#define RPP_RPP_HIST256_Y__16_REG (RPP_HIST256_BASE + 0x00000080)

// last index
#define RPP_HIST256_REGIDX_LAST      (RPP_HIST256_BASE + 0x00000084)
#define RPP_HIST256_REGIDX_LAST_OFFS 0x00000084

#endif /* __RPP_HIST256_REGS_ADDR_MAP_H__ */ 
/*************************** EOF **************************************/
