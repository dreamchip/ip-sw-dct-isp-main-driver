// ----------------------------------------------------------------------------
// This is an unpublished work, the copyright in which vests in
// DreamChip Technologies GmbH. The information contained herein is the
// property of DreamChip Technologies GmbH and is supplied without liability
// for errors or omissions. No part may be reproduced or used except as
// authorized by contract or other written permission.
// Copyright(c) DreamChip Technologies GmbH, 2022. All rights reserved.
// ----------------------------------------------------------------------------
//       N-1         _ __             _
//        __        |  ||  /    1 \    |
//  X  = \    x  cos|  -- ( n + -  ) k |   k = 0,...,N-1
//   k   /__   n    |_  N  \    2 /   _|
//       n=0
// ----------------------------------------------------------------------------
// Module   : rpp_main_post
// Purpose  : toplevel register-map
// Creator  : DCT
// Comments : automatically generated by sig2
//            !!!!! DO NOT EDIT THIS FILE !!!!!
// ----------------------------------------------------------------------------
/**
 * @file rpp_main_post_module_base_addr.h
 *
 * Provides defines for the toplevel register-map.
 *****************************************************************************/
#ifndef __RPP_MAIN_POST_MODULE_BASE_ADDR_H__
#define __RPP_MAIN_POST_MODULE_BASE_ADDR_H__

#ifndef RPP_MAIN_POST_BASE
#  error You must #include the global memory map header file before rpp_main_post_module_addr_map.h and it must define RPP_MAIN_POST_BASE
#endif /* RPP_MAIN_POST_BASE */

#define RPP_ADDR_BASE_MAIN_POST_AWB_GAIN                      (RPP_MAIN_POST_BASE + 0x00000500)
#define RPP_ADDR_BASE_MAIN_POST_FILT                          (RPP_MAIN_POST_BASE + 0x00000800)
#define RPP_ADDR_BASE_MAIN_POST_CCOR                          (RPP_MAIN_POST_BASE + 0x00000900)
#define RPP_ADDR_BASE_MAIN_POST_HIST                          (RPP_MAIN_POST_BASE + 0x00000A00)
#define RPP_ADDR_BASE_MAIN_POST_LTM                           (RPP_MAIN_POST_BASE + 0x00001000)
#define RPP_ADDR_BASE_MAIN_POST_LTM_MEAS                      (RPP_MAIN_POST_BASE + 0x00001200)
#define RPP_ADDR_BASE_MAIN_POST_AWB_MEAS                      (RPP_MAIN_POST_BASE + 0x00001700)
#define RPP_ADDR_BASE_MAIN_POST_RGBDENOISE                    (RPP_MAIN_POST_BASE + 0x00001800)
#define RPP_ADDR_BASE_MAIN_POST_SHRPCNR                       (RPP_MAIN_POST_BASE + 0x00001A00)

#define RPP_ADDR_RANGE_MAIN_POST_AWB_GAIN                     0x00000040
#define RPP_ADDR_RANGE_MAIN_POST_FILT                         0x00000100
#define RPP_ADDR_RANGE_MAIN_POST_CCOR                         0x00000040
#define RPP_ADDR_RANGE_MAIN_POST_HIST                         0x00000100
#define RPP_ADDR_RANGE_MAIN_POST_LTM                          0x00000200
#define RPP_ADDR_RANGE_MAIN_POST_LTM_MEAS                     0x00000080
#define RPP_ADDR_RANGE_MAIN_POST_AWB_MEAS                     0x00000080
#define RPP_ADDR_RANGE_MAIN_POST_RGBDENOISE                   0x00000200
#define RPP_ADDR_RANGE_MAIN_POST_SHRPCNR                      0x00000080

#endif /* __RPP_MAIN_POST_MODULE_BASE_ADDR_H__ */
