/******************************************************************************
 *
 * Copyright 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_mv_out_drv.h
 *
 * @brief   Interface of RPP Machine Vision Output pipeline unit driver.
 *
 *****************************************************************************/
#ifndef __RPP_MV_OUT_DRV_H__
#define __RPP_MV_OUT_DRV_H__

#include "rpp_config.h"
#include <rpp_hdr_types.h>

#include "rpp_gamma_out_drv.h"
#include "rpp_ccor_drv.h"
#include "rpp_xyz2luv_drv.h"
#include "rpp_mv_outregs_drv.h"
#include "rpp_is_drv.h"
#include "rpp_out_if_drv.h"

#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppMvOutDrv RPP - Machine Vision Output Pipeline Unit driver
 * @brief RPP - Machine Vision Output Pipeline Unit Driver
 *
 * This section contains definitions and functions for the machine vision
 * output pipeline stage of the RPP HDR.
 *
 * The of the machine vision output pipeline stage consists of a number of of
 * sub-units. The driver-specific structures for this pipeline stage,
 * therefore, summarizes the driver-specific data structures for the
 * included sub-units.
 *
 * Besides a common initialization routine that initializes all sub-units
 * as required, some helper functions are provided that are intended to
 * simplify the task of selecting one of the possible output images and the
 * corresponding image format.
 * @{
 *****************************************************************************/


/**************************************************************************//**
 * @brief defines the base address of the RPP_MV_OUT machine vision output pipeline stage
 *
 * This macro defines the address offset of the RPP_MV_OUT machine vision output pipeline
 * stage in relation to the base address of the RPPHDR itself.
 *****************************************************************************/
#define RPP_MV_OUT_OFFSET      0x0000b800


/**************************************************************************//**
 * @brief RPP Machine Vision Output specific driver structure
 *
 * This structure encapsulates all data required by the sub-units of the
 * RPP RPP_MV_OUT machine vision output pipeline stage.
 *****************************************************************************/
typedef struct {
    /** @brief driver-specific portion of the included GAMMA_OUT sub-unit */
    rpp_gamma_out_drv_t     gamma_out;
    /** @brief driver-specific portion of the included CCOR sub-unit */
    rpp_ccor_drv_t          ccor;
    /** @brief driver-specific portion of the included XYZ2LUV sub-unit */
    rpp_xyz2luv_drv_t       xyz2luv;
    /** @brief driver-specific portion of the included MV_OUTREGS sub-unit */
    rpp_mv_outregs_drv_t    mv_outregs;
    /** @brief driver-specific portion of the included IS sub-unit */
    rpp_is_drv_t            is;
    /** @brief driver-specific portion of the included OUT_IF sub-unit */
    rpp_out_if_drv_t        out_if;
} rpp_mv_out_drv_t;


/**************************************************************************//**
 * @brief Initialize the rpp_mv_out pipeline stage.
 * @param[in]       dev     rpp_device structure to be used for all register accesses
 * @param[in]       base    base address of RPP OUT_IF module
 * @param[inout]    drv     RPP_MV_OUT driver structure to be initialized
 * @return          0 on success, error-code otherwise
 *
 * This function initializes the @c drv structure for future uses of the drivers
 * for all sub-units included in the RPP_MV_OUT pipeline stage. If any
 * initialization error occurs, the function returns an error code.
 *****************************************************************************/
int rpp_mv_out_init(rpp_device * dev, uint32_t base, rpp_mv_out_drv_t * drv);

/**************************************************************************//**
 * @brief Prepare the rpp_mv_out pipeline stage for RGB output from the MAIN_POST pipeline.
 * @param[in]       drv     initialized RPP_MV_OUT driver structure describing the rpp_mv_out unit instance
 * @param[in]       input   the input from MAIN_POST to choose for output
 * @param[in]       curve   gamma curve to provide gamma-corrected sRGB output, optional
 * @return          0 on success, error-code otherwise
 *
 * This function prepares the Machine Vision Output pipeline for generating
 * RGB output with either 12 or 16 bits per component. The input multiplexer
 * for the Machine Vision Output is configured to accept data from the specified
 * tapping point of the MAIN_POST pipeline stage, which means that the full ISP
 * pipeline (pre-fusion stage, radiance mapping, post-fusion stage) must have
 * been set-up correctly.
 *
 * The input multiplexer setting also specifies the output bit width:
 *  - 16bit output is generated if the RPP_MV_OUT_IN_SEL_DEBAYERED input is selected
 *  - 12bit output is generated if another input is selected.
 * To understand this behaviour, remember that the image data before tone
 * mapping is still 24bit wide, while it is 12bit wide after tone mapping.
 *
 * The @c curve argument specifies the mapping from linear RGB to sRGB and
 * is optional. If omitted (specified as NULL pointer), the GAMMA_OUT unit
 * will be disabled.
 *****************************************************************************/
int rpp_mv_out_set_rgb(rpp_mv_out_drv_t * drv,
        rpp_mv_out_input_t input, rpp_gamma_out_curve_t * curve);


/**************************************************************************//**
 * @brief Prepare the rpp_mv_out pipeline stage for [s]RGB+Y output from the MAIN_POST pipeline.
 * @param[in]       drv     initialized RPP_MV_OUT driver structure describing the rpp_mv_out unit instance
 * @param[in]       do_srgb whether to output sRGB (gamma corrected) instead of linear RGB
 * @param[in]       input   the input from MAIN_POST to choose for output
 * @param[in]       curve   gamma curve to provide gamma-corrected sRGB output
 * @param[in]       rgb2yuv the colorspace conversion profile for Y calculation
 * @return          0 on success, error-code otherwise
 *
 * This function prepares the Machine Vision Output pipeline for generating
 * RGB+Y output with 8 bits per component (e.g. 4x8bit output pixels).
 * The input multiplexer for the Machine Vision Output is configured to accept
 * data from the specified tapping point of the MAIN_POST pipeline stage,
 * which means that the full ISP pipeline (pre-fusion stage, radiance mapping,
 * post-fusion stage) must have been set-up correctly.
 *
 * The @c do_srgb argument specifies (if non-zero) that the R,G,B values
 * to be output shall be gamma corrected. If this argument is set to zero,
 * the linear RGB values without gamma correction will be output instead.
 *
 * The @c curve argument specifies the mapping from linear RGB to sRGB and
 * is required in any case for the calculation of the luminance values.
 *
 * The @c rgb2yuv argument must always be provided and be set-up for a colorspace
 * conversion between RGB and YUV. The colorspace conversion is used to
 * calculate the Y values which are output along with R, G and B.
 *****************************************************************************/
int rpp_mv_out_set_rgby(rpp_mv_out_drv_t * drv, unsigned do_srgb,
        rpp_mv_out_input_t input, rpp_gamma_out_curve_t * curve,
        rpp_ccor_profile_t const * rgb2yuv);

/**************************************************************************//**
 * @brief Prepare the rpp_mv_out pipeline stage for YUV output from the MAIN_POST pipeline.
 * @param[in]       drv         initialized RPP_MV_OUT driver structure describing the rpp_mv_out unit instance
 * @param[in]       input       the input from MAIN_POST to choose for output
 * @param[in]       curve       gamma curve to provide gamma-corrected sRGB output
 * @param[in]       rgb2yuv     a color space conversion profile for RGB to YUV conversion
 * @param[in]       legal       whether to output YUV legal range or full range
 * @param[in]       method_422  the selected YUV 4:4:4 to 4:2:2 subsampling method
 * @param[in]       output_420  whether to output YUV 4:2:0 with dummy lines instead of YUV 4:2:2
 * @return          0 on success, error-code otherwise
 *
 * This function prepares the Machine Vision Output pipeline for generating
 * YUV output with either 12 bits per component or 24/16 bits per component.
 * The input multiplexer for the Machine Vision Output is configured to accept
 * data from the specified tapping point of the MAIN_POST pipeline stage,
 * which means that the full ISP pipeline (pre-fusion stage, radiance mapping,
 * post-fusion stage) must have been set-up correctly.
 *
 * The input multiplexer setting also specifies the output bit width:
 *  - 24bit Y, 16bit U/V output is generated if the RPP_MV_OUT_IN_SEL_DEBAYERED input is selected
 *  - 12bit Y, 12bit U/V output is generated if another input is selected.
 * To understand this behaviour, remember that the image data before tone
 * mapping is still 24bit wide, while it is 12bit wide after tone mapping.
 *
 * The @c curve argument specifies the mapping from linear RGB to sRGB for
 * the RGB to YUV conversion and is required.
 *
 * The @c rgb2yuv argument is required and specifies a colorspace conversion
 * profile for a RGB to YUV conversion. When the \c legal parameter is non-zero,
 * the colorspace conversion matrix will be rescaled to produce YUV samples
 * in the legal range (a.k.a. "studio swing" or "video levels") and hardware
 * clipping is enabled.
 *
 * The @c method_422 parameter specifies the algorithm to employ for chroma
 * subsampling from 4:4:4 down to 4:2:2, which is always performed.
 *
 * The @c output_420 flag specifies, if non-zero, that the output shall be
 * interleaved YUV 4:2:0 where the chroma components U/V contain dummy values
 * in even output lines and have to be ignored.
 *****************************************************************************/
int rpp_mv_out_set_yuv(rpp_mv_out_drv_t * drv,
        rpp_mv_out_input_t              input,
        rpp_gamma_out_curve_t *         curve,
        rpp_ccor_profile_t *            rgb2yuv,
        unsigned                        legal,
        rpp_mv_out_422_method_t         method_422,
        unsigned                        output_420
        );

/**************************************************************************//**
 * @brief Enumerated values specifying the Luv subsampling mode.
 *****************************************************************************/
typedef enum
{
    /** @brief turns chroma subsampling off. */
    RPP_MV_OUT_LUV_444,
    /** @brief enables horizontal chroma subsampling only. */
    RPP_MV_OUT_LUV_422,
    /** @brief enables horizontal and vertical chroma subsampling. */
    RPP_MV_OUT_LUV_420,
    RPP_MV_OUT_LUV_MAX = RPP_MV_OUT_LUV_420
} rpp_mv_out_luv_mode_t;

/**************************************************************************//**
 * @brief Prepare the rpp_mv_out pipeline stage for Luv output from the MAIN_POST pipeline.
 * @param[in]       drv         initialized RPP_MV_OUT driver structure describing the rpp_mv_out unit instance
 * @param[in]       input       the input from MAIN_POST to choose for output
 * @param[in]       subsampling the chroma subsampling specification for the output
 * @param[in]       method_422  the selected Luv 4:4:4 to 4:2:2 subsampling method
 * @param[in]       u_ref       white-point reference value for U in unsigned 3.10 fixed point format
 * @param[in]       v_ref       white-point reference value for V in unsigned 3.10 fixed point format
 * @param[in]       luma_fac    luminance output scaling factor in unsigned 1.7 fixed point format
 * @param[in]       chroma_fac  chrominance output scaling factor in unsigned 1.7 fixed point format
 * @return          0 on success, error-code otherwise
 *
 * This function prepares the Machine Vision Output pipeline for generating
 * Luv output with either 12/8 bits or 16/8 bits per component.
 * The input multiplexer for the Machine Vision Output is configured to accept
 * data from the specified tapping point of the MAIN_POST pipeline stage,
 * which means that the full ISP pipeline (pre-fusion stage, radiance mapping,
 * post-fusion stage) must have been set-up correctly.
 *
 * The input multiplexer setting also specifies the output bit width:
 *  - 16bit L, 8bit u/v output is generated if the RPP_MV_OUT_IN_SEL_DEBAYERED input is selected
 *  - 12bit Y, 8bit u/v output is generated if another input is selected.
 * To understand this behaviour, remember that the image data before tone
 * mapping is still 24bit wide, while it is 12bit wide after tone mapping.
 * 
 * The @c subsampling parameter specifies the amount of chroma subsampling
 * to perform on the output data.
 *
 * The @c method_422 parameter specifies the algorithm to employ for chroma
 * subsampling from 4:4:4 down to 4:2:2, which is always performed when
 * subsampling is enabled.
 *
 * The GAMMA_OUT unit will be bypassed for the generation of Luv output because
 * the colorspace conversion from RGB to XYZ is defined for linear RGB values
 * only.
 * The CCOR unit is programmed with a suitable default RGB to XYZ colorspace
 * conversion profile.
 *****************************************************************************/
int rpp_mv_out_set_luv(rpp_mv_out_drv_t * drv,
        rpp_mv_out_input_t              input,
        rpp_mv_out_luv_mode_t           subsampling,
        rpp_mv_out_422_method_t         method_422,
        uint16_t                        u_ref,
        uint16_t                        v_ref,
        uint8_t                         luma_fac,
        uint8_t                         chroma_fac
        );


/**************************************************************************//**
 * @brief Enable the rpp_mv_out pipeline stage to produce the given number of frames.
 * @param[in]   drv         initialized RPP_MV_OUT driver structure describing the rpp_mv_out unit instance
 * @param[in]   num_frames  number of frames to capture (0..1023)
 * @param[in]   data_mode   whether the output is of type DATA or VIDEO
 *
 * This function enables the Machine Vision Output to capture the given number
 * of frames and to produce the indicated output format.
 *
 * If the @c num_frames parameter is zero, the unit will produce an
 * infinite number of frames (e.g. run until it is explicitly stopped
 * again). Otherwise the unit will turn itself off again after having
 * produced the given number of output frames.
 *****************************************************************************/
int rpp_mv_out_enable(rpp_mv_out_drv_t * drv, unsigned num_frames, unsigned data_mode);

/**************************************************************************//**
 * @brief Explicitly disable the rpp_mv_out pipeline stage.
 * @param[in]   drv         initialized RPP_MV_OUT driver structure describing the rpp_mv_out unit instance
 *
 * This function is used to stop the Machine Vision Output when it is no
 * longer needed.
 *****************************************************************************/
int rpp_mv_out_disable(rpp_mv_out_drv_t * drv);

/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_MV_OUT_DRV_H__ */

