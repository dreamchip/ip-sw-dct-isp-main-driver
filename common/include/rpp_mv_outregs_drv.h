/******************************************************************************
 *
 * Copyright 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_mv_outregs_drv.h
 *
 * @brief   Interface of Output Selection unit driver
 *
 *****************************************************************************/
#ifndef __RPP_MV_OUTREGS_DRV_H__
#define __RPP_MV_OUTREGS_DRV_H__

#include "rpp_config.h"
#include <rpp_hdr_types.h>

#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppMvOutregsDrv RPP - Machine Vision Output Selection unit driver
 * RPP - Machine Vision Output Selection unit driver
 *
 * This driver controls the output selection unit for the machine vision output
 * of the RPP HDR pipeline. It is used to select from one one of several
 * sources to be directed towards the output interface of the pipeline,
 * as well as the image format and eventual subsampling options.
 *
 * The unit includes an input selection switch that can select from one of
 * three different inputs:
 *  - post-fusion pipeline after local tone mapping, denoising and sharpening
 *  - post-fusion pipeline after demosaicing
 *  - post-fusion pipeline after global tone mapping
 *
 * For each of the inputs it is also selectable whether the data received
 * on the input shall be blocked or discarded when the corresponding input
 * is not selected for output. In the RPP HDR, unused inputs shall be configured
 * to discard their data when unselected.
 *
 * Depending on the selected output format, the unit may automatically bypass
 * certain sub-units of the Machine Vision Output pipeline stage which, in
 * this case, need not be programmed. This includes:
 *  - GAMMA_OUT
 *  - CCOR
 *  - XYZ2LUV
 *  - 4:4:4 to 4:2:2 chroma subsampling
 *  - 4:2:2 to 4:2:0 chroma subsampling
 *
 * In contrast to the Human Vision Output, the Machine Vision output supports
 * a rather large number of output formats and larger bitwidth. Among the
 * output formats are:
 *  - RGBY, sRGB and linear RGB
 *  - YUV with various bit depths and chroma subsampling options
 *  - Luv with various bit depths and chroma subsampling options
 * 
 * YUV output requires that the GAMMA_OUT is programmed with a suitable
 * piece-wise linear curve definition and the CCOR unit is set-up for
 * an RGB-to-YUV conversion.
 *
 * Luv output bypasses the GAMMA_OUT unit and requires the CCOR unit
 * to be programmed with an RGB-to-XYZ colorspace conversion. The XYZ2LUV
 * converter, which gets enabled automatically, performs the final
 * transformation to the Luv colorspace.
 *
 * The difference between linear RGB and sRGB is the requirement to have
 * a gamma correction enabled for sRGB. CCOR is bypassed in hardware
 * in both cases.
 * In case of RGBY output, both, GAMMA_OUT and CCOR need to be set-up for
 * an RGB-to-YUV conversion to correctly calculate the luminance value Y.
 * @{
 *****************************************************************************/


/**************************************************************************//**
 * @brief Enumerates the supported inputs to the RPP HDR Machine Vision output
 *
 * This datatype describes the supported inputs to the RPP HDR Machine Vision Output
 * Selection unit which can be propagated to the output interface.
 *****************************************************************************/
typedef enum
{
    /** @brief selects the MAIN_POST data after demosaicing
     *
     * This value, when specified as input source for the Output Selection
     * unit, specifies to propagate the data of the MAIN_POST pipeline
     * stage after debayering towards the Machine Vision output interface.
     *
     * When choosing this value, the output image will not be tone-mapped
     * as tone mapping happens after debayering in the MAIN_POST pipeline
     * stage. */
    RPP_MV_OUT_IN_SEL_DEBAYERED     = 0x01,
    /** @brief selects the MAIN_POST data after global tone mapping
     *
     * This value, when specified as input source for the Output Selection
     * unit, specifies to propagate the data of the MAIN_POST pipeline
     * stage after the global tone-mapping step towards the output interface.
     *
     * When choosing this value, image will not have passed the local
     * tone mapping step and not gone through the denoising/sharpening
     * filters.
     */
    RPP_MV_OUT_IN_SEL_TONE_MAPPED   = 0x02,
    /** @brief selects the MAIN_POST data after local tone mapping, denoising and sharpening.
     * 
     * This value, when specified as input source for the Output Selection
     * unit, specifies to forward the final output of the MAIN_POST
     * pipeline stage towards the Machine Vision output interface.
     *
     * The image content is, therefore, similar to that of the Human Vision output.
     */
    RPP_MV_OUT_IN_SEL_DENOISED      = 0x04,
    RPP_MV_OUT_IN_SEL_MAX           = 0x07
} rpp_mv_out_input_t;


/**************************************************************************//**
 * @brief Enumerates the supported Machine Vision Output image formats
 *
 * This datatype describes the image formats which are available for output
 * by the RPP HDR Machine Vision Output Selection unit.
 *****************************************************************************/
typedef enum
{
    /** @brief Specifies that interleaved 2x12bit YUV 4:2:2 shall be generated.
     *
     * This value, when configured as output format for the Machine Vision Output,
     * specifies that Y/U, Y/V pixels (e.g. YUV 4:2:2 format) with 12bit per
     * Y and U/V component shall be generated. Each output pixel has 24 valid bits.
     */
    RPP_MV_OUT_FORMAT_YUV422_12     = 1,

    /** @brief Specifies that interleaved 2x12bit YUV 4:2:0 with dummy lines shall be generated.
     *
     * This value, when configured as output format for the Machine Vision Output,
     * specifies that Y/U, Y/V pixels with 12bit per Y and U/V component shall
     * be generated. Vertical subsampling to YUV 4:2:0 is achieved by filling
     * the U and V components in even lines with dummy data that must be
     * ignored. Otherwise each output pixel has 24 valid bits. */
    RPP_MV_OUT_FORMAT_YUV420_12     = 2,

    /** @brief Specifies that 16bit [s]RGB 4:4:4 shall be generated.
     *
     * This value, when configured as output format for the Machine Vision Output,
     * specifies that RGB pixels with 16bit per color component will be generated.
     * The output may be linear RGB or sRGB, depending on whether the GAMMA_OUT
     * unit is enabled or not. Each output pixel has 48 valid bits. */
    RPP_MV_OUT_FORMAT_SRGB_16       = 4,

    /** @brief Specifies that 12bit [s]RGB 4:4:4 shall be generated.
     *
     * This value, when configured as output format for the Machine Vision Output,
     * specifies that RGB pixels with 12bit per color component will be generated.
     * The output may be linear RGB or sRGB, depending on whether the GAMMA_OUT
     * unit is enabled or not. Each output pixel has 36 valid bits. */
    RPP_MV_OUT_FORMAT_SRGB_12       = 5,

    /** @brief Specifies that interleaved YUV 4:2:2 with 24/16 bit shall be generated.
     *
     * This value, when configured as output format for the Machine Vision Output,
     * specifies that Y/U, Y/V pixels (e.g. YUV 4:2:2 format) with 24bit per
     * Y value and 16bit per U/V value shall be generated. Each output pixel
     * has 40 valid bits. */
    RPP_MV_OUT_FORMAT_YUV422_24     = 6,

    /** @brief Specifies that interleaved YUV 4:2:0 with 24/16 bit shall be generated.
     *
     * This value, when configured as output format for the Machine Vision Output,
     * specifies that Y/U, Y/V pixels with 24bit per Y value and 16bit per U/V value
     * shall be generated. Vertical subsampling to YUV 4:2:0 is achieved by filling
     * the U and V components in even lines with dummy data that must be ignored.
     * Otherwise each output pixel has 40 valid bits. */
    RPP_MV_OUT_FORMAT_YUV420_24     = 7,

    /** @brief Specifies that interleaved Luv 4:4:4 with 16/8/8 bit shall be generated.
     *
     * This value, when configured as output format for the Machine Vision Output,
     * specifies that Luv pixels (e.g. 4:4:4 format) with 16bit per
     * L value and 8bit per u/v value shall be generated. Each output pixel
     * has 32 valid bits. */
    RPP_MV_OUT_FORMAT_LUV444_16     = 8,

    /** @brief Specifies that interleaved Luv 4:2:2 with 16/8 bit shall be generated.
     *
     * This value, when configured as output format for the Machine Vision Output,
     * specifies that Lu/Lv pixels (e.g. 4:2:2 format) with 16bit per
     * L value and 8bit per u/v value shall be generated. Each output pixel
     * has 24 valid bits. */
    RPP_MV_OUT_FORMAT_LUV422_16     = 9,

    /** @brief Specifies that interleaved Luv 4:2:0 with 16/8 bit shall be generated.
     *
     * This value, when configured as output format for the Machine Vision Output,
     * specifies that Lu/Lv pixels with 16bit per L value and 8bit per u/v value
     * shall be generated. Vertical subsampling to Luv 4:2:0 is achieved by filling
     * the u and v components in even lines with dummy data that must be ignored.
     * Otherwise each output pixel has 24 valid bits. */
    RPP_MV_OUT_FORMAT_LUV420_16     = 10,

    /** @brief Specifies that interleaved Luv 4:4:4 with 12/8/8 bit shall be generated.
     *
     * This value, when configured as output format for the Machine Vision Output,
     * specifies that Luv pixels (e.g. 4:4:4 format) with 12bit per
     * L value and 8bit per u/v value shall be generated. Each output pixel
     * has 28 valid bits. */
    RPP_MV_OUT_FORMAT_LUV444_12     = 11,

    /** @brief Specifies that interleaved Luv 4:2:2 with 12/8 bit shall be generated.
     *
     * This value, when configured as output format for the Machine Vision Output,
     * specifies that Lu/Lv pixels (e.g. 4:2:2 format) with 16bit per
     * L value and 8bit per u/v value shall be generated. Each output pixel
     * has 20 valid bits. */
    RPP_MV_OUT_FORMAT_LUV422_12     = 12,

    /** @brief Specifies that interleaved Luv 4:2:0 with 16/8 bit shall be generated.
     *
     * This value, when configured as output format for the Machine Vision Output,
     * specifies that Lu/Lv pixels with 16bit per Y value and 8bit per u/v value
     * shall be generated. Vertical subsampling to Luv 4:2:0 is achieved by filling
     * the u and v components in even lines with dummy data that must be ignored.
     * Otherwise each output pixel has 20 valid bits. */
    RPP_MV_OUT_FORMAT_LUV420_12     = 13,

    /** @brief Specifies that 8bit sRGBY shall be generated.
     *
     * This value, when configured as output format for the Machine Vision Output,
     * specifies that sRGB+Y pixels with 8bit per color component will be generated.
     * Gamma correction must always be enabled and gamma-corrected RGB values are
     * output. Each output pixel has 32 valid bits. */
    RPP_MV_OUT_FORMAT_SRGBY_8       = 14,

    /** @brief Specifies that linear 8bit RGBY shall be generated.
     *
     * This value, when configured as output format for the Machine Vision Output,
     * specifies that linear RGB + Y pixels with 8bit per color component will be
     * generated. Gamma correction must always be enabled for correct luminance
     * calculation, even though non-gamma-corrected RGB values are output.
     * Each output pixel has 32 valid bits. */
    RPP_MV_OUT_FORMAT_RGBY_8        = 15,

    RPP_MV_OUT_FORMAT_MAX = RPP_MV_OUT_FORMAT_RGBY_8
} rpp_mv_out_format_t;


/**************************************************************************//**
 * @brief Enumerates the supported Machine Vision Output 4:2:2 subsampling options
 *
 * This datatype describes the options which are supported for the YUV 4:4:4
 * to YUV 4:2:2 color subsampling by the RPP HDR Machine Vision Output
 * Selection unit.
 *****************************************************************************/
typedef enum
{
    /** @brief co-sited subsampling, sequence: Y0Cb0, Y1Cr0, for motion JPEG, MPEG2 mode */
    RPP_MV_OUT_422_CO_SITED        = 0,
    /** @brief co-sited interleaved subsampling, sequence: Y0Cb0, Y1Cr1, not recommended */
    RPP_MV_OUT_422_CO_SITED_2      = 1,
    /** non-co-sited subsampling with linear interpolation, sequence: Y0Cb01mean Y1Cr01mean, for still image JPEG, MPEG1 mode */
    RPP_MV_OUT_422_NON_CO_SITED    = 2,
    RPP_MV_OUT_422_MAX = RPP_MV_OUT_422_NON_CO_SITED
} rpp_mv_out_422_method_t;

/**************************************************************************//**
 * @brief RPP MV_OUTREGS specific driver structure
 *
 * This structure encapsulates all data required by the RPP MV_OUTREGS driver to
 * operate a specific instance of the RPP MV_OUTREGS on a given RPPHDR device.
 * The data fields contained in this structure are intended to be
 * private to the driver and not to be modified by any higher-level
 * software component.
 *
 * It is required to have a separate instance of this structure for each
 * separate RPP MV_OUTREGS module in a given RPPHDR device. Each of these structures
 * must be initialized before use by calling the function @c rpp_mv_outregs_init()
 * with suitable parameters.
 *
 * The structure is exported here so that instances can statically be declared
 * by higher-level software without the need for dynamic memory allocation.
 *****************************************************************************/
typedef struct {
    uint32_t        version;
    uint32_t        base;
    rpp_device *    dev;
} rpp_mv_outregs_drv_t;


/**************************************************************************//**
 * @brief Initialize the rpp mv_outregs module.
 * @param[in]       dev     rpp_device structure to be used for all register accesses
 * @param[in]       base    base address of RPP MV_OUTREGS module
 * @param[inout]    drv     MV_OUTREGS driver structure to be initialized
 * @return      0 on success, error code otherwise
 *
 * This function initializes the @c drv structure for future uses of the driver
 * on the given instance of the rpp mv_outregs module. It fills the @c drv structure
 * with data required for operating on the given instance and caches data that
 * describes the features of the mv_outregs module instance.
 *
 * This function needs to be called once for every instance of the rpp mv_outregs
 * module to operate, each time with a different @c drv argument intended for
 * that very instance of the rpp mv_outregs module only.
 *****************************************************************************/
int rpp_mv_outregs_init(rpp_device * dev, uint32_t base, rpp_mv_outregs_drv_t * drv);


/**************************************************************************//**
 * @brief Selects one of the supported image formats for output.
 * @param[in]   drv     initialized MV_OUTREGS driver structure describing the unit instance
 * @param[in]   format  the selected image format (@see rpp_mv_out_format_t)
 * @return      0 on success, error code otherwise
 *
 *
 * This function configures the output interface to generate one
 * of the image formats that are enumerated in rpp_mv_out_format_t.
 *****************************************************************************/
int rpp_mv_outregs_set_format(rpp_mv_outregs_drv_t * drv, rpp_mv_out_format_t format);

/**************************************************************************//**
 * @brief Retrieves the image format which is selected for output.
 * @param[in]   drv     initialized MV_OUTREGS driver structure describing the unit instance
 * @param[out]  format  the selected image format (@see rpp_mv_out_format_t)
 * @return      0 on success, error code otherwise
 *
 *
 * This function retrieves the currently selected output image format for the
 * Machine Vision Output from the unit.
 *****************************************************************************/
int rpp_mv_outregs_format(rpp_mv_outregs_drv_t * drv, rpp_mv_out_format_t * const format);


/**************************************************************************//**
 * @brief Selects one of the supported YUV 4:2:2 subsampling methods for the Machine Vision Output.
 * @param[in]   drv     initialized MV_OUTREGS driver structure describing the unit instance
 * @param[in]   method  the selected 4:2:2 subsampling method (@see rpp_mv_out_422_method_t)
 * @return      0 on success, error code otherwise
 *
 *
 * This function configures the output interface to use one of the
 * 4:4:4 to 4:2:2 chroma subsampling methods that are enumerated
 * in rpp_mv_out_422_method_t for generating YUV or Luv output on
 * the Machine Vision Output.
 *
 * This setting is only respected if the unit is configured 
 * with an image format of either YUV 4:2:2, YUV 4:2:0, Luv 4:2:2 or
 * Luv 4:2:0. In any other case, this setting has no effect.
 *****************************************************************************/
int rpp_mv_outregs_set_422_method(rpp_mv_outregs_drv_t * drv, rpp_mv_out_422_method_t method);

/**************************************************************************//**
 * @brief Retrieves the 4:2:2 chroma subsampling method for the Machine Vision Output from the unit.
 * @param[in]   drv     initialized MV_OUTREGS driver structure describing the unit instance
 * @param[out]  method  the selected 4:2:2 subsampling method (@see rpp_mv_out_422_method_t)
 * @return      0 on success, error code otherwise
 *
 * This function queries the unit for the currently selected
 * 4:4:4 to 4:2:2 chroma subsampling method for generating YUV or Luv
 * output on the Machine Vision Output.
 *****************************************************************************/
int rpp_mv_outregs_422_method(rpp_mv_outregs_drv_t * drv, rpp_mv_out_422_method_t * const method);

/**************************************************************************//**
 * @brief Configures the input multiplexer of the Machine Vision Output unit.
 * @param[in]   drv             initialized MV_OUTREGS driver structure describing the unit instance
 * @param[in]   unselected_mode whether to block or discard data at unused inputs
 * @param[in]   input_select    which of the inputs shall be selected
 * @return      0 on success, error code otherwise
 *
 * This function is used to program the input multiplexer of the Machine Vision
 * Output by
 *  - specifying what happens with the data on unselected inputs, and
 *  - selecting one of the inputs for propagation towards the Machine Vision output.
 *
 * The @c unselected_mode value is a bitmask of the enumerated values in
 * rpp_mv_out_input_t. For each input that is selected in @c unselected_mode,
 * unused data on the input will be discarded. For each input that is not
 * selected in @c unselected_mode, unused data on the input will be blocked.
 * For the RPP HDR, the @c unselected_mode shall be programmed to 0x07.
 *
 * The @c input_select parameter specifies which of the inputs to use
 * for output on the Machine Vision Output. It must be one of the values
 * enumerated in rpp_mv_out_input_t.
 *****************************************************************************/
int rpp_mv_outregs_set_mode(rpp_mv_outregs_drv_t * drv, uint8_t unselected_mode,
        rpp_mv_out_input_t input_select);

/**************************************************************************//**
 * @brief Retrieves the input multiplexer configuration of the Machine Vision Output unit.
 * @param[in]   drv             initialized MV_OUTREGS driver structure describing the unit instance
 * @param[out]  unselected_mode whether data at unused inputs is blocked or discarded
 * @param[out]  input_select    the selected input
 * @return      0 on success, error code otherwise
 *
 * This function queries the Machine Vision Output unit for the configuration
 * of the input multiplexer.
 *****************************************************************************/
int rpp_mv_outregs_mode(rpp_mv_outregs_drv_t * drv, uint8_t * const unselected_mode,
        rpp_mv_out_input_t * const input_select);



/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_MV_OUTREGS_DRV_H__ */

