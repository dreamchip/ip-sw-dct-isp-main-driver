/******************************************************************************
 *
 * Copyright 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_out_if_drv.h
 *
 * @brief   Interface of output interface unit driver
 *
 *****************************************************************************/
#ifndef __RPP_OUT_IF_DRV_H__
#define __RPP_OUT_IF_DRV_H__

#include "rpp_config.h"
#include <rpp_hdr_types.h>

#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppOutIfDrv RPP - Output Interface unit driver
 * RPP - Output Interface unit driver
 *
 * This driver controls the output interface unit of the RPP HDR
 * pipeline. It is used to enable or disable image output.
 * @{
 *
 *****************************************************************************/

/**************************************************************************//**
 * @brief RPP OUT_IF specific driver structure
 *
 * This structure encapsulates all data required by the RPP OUT_IF driver to
 * operate a specific instance of the RPP OUT_IF on a given RPPHDR device.
 * The data fields contained in this structure are intended to be
 * private to the driver and not to be modified by any higher-level
 * software component.
 *
 * It is required to have a separate instance of this structure for each
 * separate RPP OUT_IF module in a given RPPHDR device. Each of these structures
 * must be initialized before use by calling the function @c rpp_out_if_init()
 * with suitable parameters.
 *
 * The structure is exported here so that instances can statically be declared
 * by higher-level software without the need for dynamic memory allocation.
 *****************************************************************************/
typedef struct {
    uint32_t        version;
    uint32_t        base;
    rpp_device *    dev;
} rpp_out_if_drv_t;


/**************************************************************************//**
 * @brief Initialize the rpp out_if module.
 * @param[in]       dev     rpp_device structure to be used for all register accesses
 * @param[in]       base    base address of RPP OUT_IF module
 * @param[inout]    drv     OUT_IF driver structure to be initialized
 * @return      0 on success, error code otherwise
 *
 * This function initializes the @c drv structure for future uses of the driver
 * on the given instance of the rpp out_if module. It fills the @c drv structure
 * with data required for operating on the given instance and caches data that
 * describes the features of the out_if module instance.
 *
 * This function needs to be called once for every instance of the rpp out_if
 * module to operate, each time with a different @c drv argument intended for
 * that very instance of the rpp out_if module only.
 *****************************************************************************/
int rpp_out_if_init(rpp_device * dev, uint32_t base, rpp_out_if_drv_t * drv);


/**************************************************************************//**
 * @brief Enables or disables output of frames from the RPP HDR.
 * @param[in]   drv     initialized OUT_IF driver structure describing the is unit instance
 * @param[in]   on      whether to enable or disable the output
 * @return      0 on success, error code otherwise
 *
 * This function can be used to enable or disable image output.
 * The change takes effect at the next End of Frame signal.
 *****************************************************************************/
int rpp_out_if_set_active(rpp_out_if_drv_t * drv, const unsigned on);

/**************************************************************************//**
 * @brief Queries from the unit whether the output will be enabled or disabled.
 * @param[in]   drv     initialized OUT_IF driver structure describing the is unit instance
 * @param[out]  on      whether the output will be enabled or disabled
 * @return      0 on success, error code otherwise
 *
 * This function is used to query from the unit what the state of the
 * output will be for the next frame to be processed.
 *
 * This function does not return whether the unit is active at the time of asking,
 * use the rpp_out_if_active() for that.
 *****************************************************************************/
int rpp_out_if_next_active(rpp_out_if_drv_t * drv, unsigned * const on);

/**************************************************************************//**
 * @brief Queries from the unit whether the output currently is enabled or disabled.
 * @param[in]   drv     initialized OUT_IF driver structure describing the is unit instance
 * @param[out]  on      whether the output is enabled or disabled
 * @return      0 on success, error code otherwise
 *
 * This function is used to query from the unit what the state of the
 * output is at the time of asking. This information may be valid only
 * for the current frame, e.g. when the unit was programmed to change
 * the operational state of the output for the next frame.
 *****************************************************************************/
int rpp_out_if_active(rpp_out_if_drv_t * drv, unsigned * const on);

/**************************************************************************//**
 * @brief Programs the number of frames to output to the unit.
 * @param[in]   drv     initialized OUT_IF driver structure describing the is unit instance
 * @param[in]   num     the number of frames to output (0..1023)
 * @return      0 on success, error code otherwise
 *
 * This function is used to configure a number of output frames that
 * will be processed before the unit turns itself off again.
 *
 * A value of zero indicates that an infinite number of frames are
 * to be sampled, e.g. the unit must be turned off again by using
 * the rpp_out_if_set_active() function.
 *
 * A non-zero value indicates that, after enabling the image output,
 * it turns itself off again once the given number of frames has been
 * captured.
 *****************************************************************************/
int rpp_out_if_set_frames(rpp_out_if_drv_t * drv, const unsigned num);

/**************************************************************************//**
 * @brief Retrieves the number of frames to output from the unit.
 * @param[in]   drv     initialized OUT_IF driver structure describing the is unit instance
 * @param[out]  num     the number of frames being output (0..1023)
 * @return      0 on success, error code otherwise
 *
 * This function is query from the unit how many frames
 * are programmed to be captured before the unit turns itself off again.
 *
 * A value of zero indicates that an infinite number of frames are
 * to be sampled, e.g. the unit must be turned off again by using
 * the rpp_out_if_set_active() function.
 *
 * A non-zero value indicates that, after enabling the image output,
 * it turns itself off again once the given number of frames has been
 * captured.
 *****************************************************************************/
int rpp_out_if_frames(rpp_out_if_drv_t * drv, unsigned * const num);


/**************************************************************************//**
 * @brief Retrieves the number of remaining frames from the unit.
 * @param[in]   drv     initialized OUT_IF driver structure describing the is unit instance
 * @param[out]  num     the number of remaining frames (0..1023)
 * @return      0 on success, error code otherwise
 *
 * This function is query from the unit how many frames the unit
 * will still capture before it turns itself off again.
 *
 * Once the unit was started after a finite number of frames to be
 * captured was programmed, this function returns the value of a
 * counter that is decremented down to zero for each already captured
 * frame.
 *
 * If the unit was not started beforehand or programmed to capture an
 * infinite number of frames, the value returned by this function
 * is not defined.
 *****************************************************************************/
int rpp_out_if_remaining_frames(rpp_out_if_drv_t * drv, unsigned * const num);

/**************************************************************************//**
 * @brief Programs the output data type to the unit.
 * @param[in]   drv     initialized OUT_IF driver structure describing the is unit instance
 * @param[in]   data    whether the output data is of type data instead of video
 * @return      0 on success, error code otherwise
 *
 * This function configures the output for either video or data
 * output.
 *****************************************************************************/
int rpp_out_if_set_data_mode(rpp_out_if_drv_t * drv, const unsigned data);

/**************************************************************************//**
 * @brief Retrieves the output data type from the unit.
 * @param[in]   drv     initialized OUT_IF driver structure describing the is unit instance
 * @param[in]   data    whether the output data is of type data instead of video
 * @return      0 on success, error code otherwise
 *
 * This function retrieves from the unit whether the output is of type
 * data instead of type video.
 *****************************************************************************/
int rpp_out_if_data_mode(rpp_out_if_drv_t * drv, unsigned * const data);



/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_OUT_IF_DRV_H__ */

