/******************************************************************************
 *
 * Copyright 2020 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_awb_meas_drv.h
 *
 * @brief   Implementation of auto white balance measurement driver
 *
 *****************************************************************************/
#ifndef __RPP_AWB_MEAS_DRV_H__
#define __RPP_AWB_MEAS_DRV_H__

#include "rpp_config.h"
#include <rpp_hdr_types.h>
#include <rpp_ccor_drv.h>


#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppAwbMeasDrv RPP - White Balance Measurement Driver
 * RPP - White Balance Measurement Driver
 * @{
 *
 *****************************************************************************/


/**************************************************************************//**
 * @brief Structure describing Auto White Balance measurement conditions
 *
 * This structure contains all data that is required for setting up the white
 * balance measurement unit. There are two distinct measurement modes, one for
 * YUV data and one for RGB data.
 *
 * -# YUV Mode:
 *    For a pixel to be considered "near white" the luminance has to be in a defined 
 *    range between minimum and maximum luminance and the chrominance values Cb/Cr must
 *    be contained in a certain range as well (Figure 2). The following conditions must
 *    be met, based on the AWB threshold programming parameters:
 *   
 *                                                Y    >=  RPP_AWB_MIN_Y     @n
 *                                                Y    <=  RPP_AWB_MAX_Y     @n
 *                            (Cb - RPP_AWB_REF_CB)    >=  -RPP_AWB_MIN_C    @n
 *                            (Cr - RPP_AWB_REF_CR)    >=  -RPP_AWB_MIN_C    @n
 *    (Cr - RPP_AWB_REF_CR) + (Cb - RPP_AWB_REF_CB)    <=  RPP_AWB_MAX_CSUM  @n
 *    (Cr - RPP_AWB_REF_CR) + (Cb - RPP_AWB_REF_CB)    >=  -RPP_AWB_MAX_CSUM @n
 *    The values for _REF_CB, _REF_CR, _MIN_Y, _MAX_Y, _MIN_C and _MAX_CSUM
 *    can be programmed.
 * -# RGB Mode:
 *    The white balance algorithm relies on the gray world assumption and calculates the 
 *    new gain settings from all pixels inside the measurement window by just omitting
 *    the pixels with very high saturation, which have:
 *    - R >= RPP_AWB_MAX_R
 *    - G >= RPP_AWB_MAX_G
 *    - B >= RPP_AWB_MAX_B
 *    Here again, the _MAX_R, _MAX_G and _MAX_B values can be programmed.
 *
 * All the parameters in this structure that relate to YUV or RGB measurements
 * are interpreted as color values and must be smaller than 2^colorbits - 1
 * using the colorbits value reported for the awb_meas unit under consideration.
 *
 * A union is used to hold the measurement parameter sets for YUV and RGB
 * parameter sets, which means they are aliases to each other. This is done to
 * reduce the amount of memory required for this structure. Care must be
 * taken that the RGB measurement parameters are left untouched when YUV
 * parameters are programmed and vice versa.
 *
 *****************************************************************************/
typedef struct
{
    /*! @brief selects between YUV and RGB data.
     *
     * This field tells the driver whether the unit's registers shall be set-up for
     * RGB (1) or YUV (0) data processing. Depending on that setting, the driver
     * will respect different fields from this structure.
     */
    uint16_t rgb_valid;
    /*! @brief configures the number of frames to measure
     *
     * The unit is capable of performing a single measurement across
     * multiple frames, where the number of frames is allowed to range
     * from 1 to 8. This field must be set to the frame number minus 1,
     * that is:
     *  - value == 0 indicates 1 frame,
     *  - value == 1 indicates 2 frames,
     *  - ...
     *  - value == 7 indicates 8 frames
     */
    uint16_t frames;
    union {
        struct {
            uint32_t    ref_cb;     /**< @brief YUV measurement: reference Cb value (e.g. _REF_CB)                  */
            uint32_t    ref_cr;     /**< @brief YUV measurement: reference Cr value (e.g. _REF_CR)                  */
            uint32_t    max_y;      /**< @brief YUV measurement: luminance maximum value (e.g. _MAX_Y)              */
            uint32_t    min_y;      /**< @brief YUV measurement: luminance minimum value (e.g. _MIN_Y)              */
            uint32_t    max_csum;   /**< @brief YUV measurement: chrominance sum maximum value (e.g. _MAX_CSUM)     */
            uint32_t    min_c;      /**< @brief YUV measurement: chrominance minimum value (e.g. _MIN_C)            */
        } yuv;
        struct {
            uint32_t    max_b;      /**< @brief RGB measurement: blue maximum value (e.g. _MAX_B)                   */
            uint32_t    max_r;      /**< @brief RGB measurement: red maximum value (e.g. _MAX_R)                    */
            uint32_t    __unused;   /* provide this field here to make the structure match the register layout      */
            uint32_t    max_g;      /**< @brief RGB measurement: green maximum value (e.g. _MAX_G)                  */
        } rgb;
    };
} rpp_awb_meas_cond_t;


/**************************************************************************//**
 * @brief RPP Auto white balance measurement results
 *****************************************************************************/
typedef struct
{
    uint32_t rgb_valid;         /**< indicate if rgb values or yuv values are valid. 0: 'yuv' else 'rgb'*/
    uint32_t white_count;       /**< Auto white balance white pixel count */
    union {
        struct {
            uint32_t mean_y;     /**< Auto white balance measured mean value */
            uint32_t mean_cb;    /**< Auto white balance measured mean value */
            uint32_t mean_cr;    /**< Auto white balance measured mean value */
        } yuv;
        struct {
            uint32_t mean_g;     /**< Auto white balance measured mean value */
            uint32_t mean_b;     /**< Auto white balance measured mean value */
            uint32_t mean_r;     /**< Auto white balance measured mean value */
        } rgb;
    };
} rpp_awb_meas_t;


/**************************************************************************//**
 * @brief RPP Auto white balance measurement mode
 *
 * This enumerated value describes the possible operation modes for
 * performing the auto white balance measurement.
 *****************************************************************************/
enum rpp_awb_meas_mode_e
{
    RPP_AWB_MEAS_MODE_DISABLE   = 0,            /**< no measurement */
    RPP_AWB_MEAS_MODE_RGB       = 0x00010002U,  /**< RGB based measurement mode */
    RPP_AWB_MEAS_MODE_YUV       = 0x00000002U,  /**< near white discrimination mode using YCbCr color space with Y_MAX compare disabled */
    RPP_AWB_MEAS_MODE_YUV_YMAX  = 0x00000006U,  /**< near white discrimination mode using YCbCr color space with Y_MAX compare enabled */
};


/**************************************************************************//**
 * @brief RPP AWB_MEAS specific driver structure
 *
 * This structure encapsulates all data required by the RPP AWB_MEAS driver to
 * operate a specific instance of the RPP AWB_MEAS unit on a given RPPHDR device.
 * The data fields contained in this structure are intended to be
 * private to the driver and not to be modified by any higher-level
 * software component.
 *
 * It is required to have a separate instance of this structure for each
 * separate RPP AWB_MEAS unit in a given RPPHDR device. Each of these structures
 * must be initialized before use by calling the function @c rpp_awb_meas_init()
 * with suitable parameters.
 *
 * After initialization, the @c colorbits member variable is set to the
 * number of bits per color value. This information is needed for the
 * preparation of the measurement conditions and the interpretion of measurement
 * results.
 *
 * The structure is exported here so that instances can statically be declared
 * by higher-level software without the need for dynamic memory allocation.
 *****************************************************************************/
typedef struct {
    uint16_t        version;
    uint16_t        colorbits;
    uint32_t        base;
    rpp_device *    dev;
} rpp_awb_meas_drv_t;

/**************************************************************************//**
 * @brief Initialize the rpp awb measurement module.
 * @param[in]       dev     rpp_device structure to be used for all register accesses
 * @param[in]       base    base address of RPP AWB_MEAS module
 * @param[inout]    drv     AWB_MEAS  driver structure to be initialized
 * @return      0 on success, error-code otherwise
 *
 * This function initializes the @c drv structure for future uses of the driver
 * on the given instance of the rpp awb_meas unit. It fills the @c drv structure
 * with data required for operating on the given instance and caches data that
 * describes the features of the awb_meas unit instance.
 *
 * This function needs to be called once for every instance of the rpp awb_meas
 * unit to operate, each time with a different @c drv argument intended for
 * that very instance of the rpp awb_meas unit only.
 *****************************************************************************/
int rpp_awb_meas_init(rpp_device * dev, uint32_t base, rpp_awb_meas_drv_t * drv);

/**************************************************************************//**
 * @brief Disable the rpp awb measurement module.
 * @param[in]   drv     initialized AWB_MEAS driver structure describing the awb_meas unit instance
 * @return      0 on success, error-code otherwise
 *
 * This function disables the awb_meas unit. If disabled, image data passes
 * through the unit without being measured and the result registers will not
 * be refreshed.
 *****************************************************************************/
int rpp_awb_meas_disable(rpp_awb_meas_drv_t * drv);

/**************************************************************************//**
 * @brief Set measurement mode in rpp awb measurement module.
 * @param[in]   drv     initialized AWB_MEAS driver structure describing the awb_meas unit instance
 * @param[in]   mode    operation mode to set (@see rpp_awb_meas_mode_e)
 * @return      0 on success, error-code otherwise
 *
 * This function enables operation of the awb_meas unit to behave as indicated
 * by the @c mode argument. The following parameters must have been programmed
 * to the unit before enabling operation:
 *  - a measurement window
 *  - a set of measurement conditions matching the selected operation mode
 *  - for YUV measurements the set-up of the built-in CCOR sub-unit is required,
 *    for RGB measurements the CCOR sub-unit is bypassed.
 *****************************************************************************/
int rpp_awb_meas_set_mode(rpp_awb_meas_drv_t * drv, const unsigned mode);

/**************************************************************************//**
 * @brief Return current measurement mode from rpp awb_meas unit.
 * @param[in]   drv     initialized AWB_MEAS driver structure describing the awb_meas unit instance
 * @param[out]  mode    current operation mode (@see rpp_awb_meas_mode_e)
 * @return      0 on success, error-code otherwise
 *
 * This function reads the currently programmed operation mode from the
 * unit and supplies the value in the @c mode argument when returning
 * from this function.
 *
 * In case of errors retrieving this information, the function returns an
 * error code and the value in @c mode should not be considered valid.
 *****************************************************************************/
int rpp_awb_meas_mode(rpp_awb_meas_drv_t * drv, unsigned * const mode);

/**************************************************************************//**
 * @brief Program color conversion coefficients to the rpp awb_meas unit.
 * @param[in]   drv     initialized AWB_MEAS driver structure describing the awb_meas unit instance
 * @param[in]   p       profile to set (@see rpp_ccor_profile_t)
 * @return      0 on success, error-code otherwise
 *
 * This function programs the supplied color conversion profile (matrix
 * coefficients plus offset vector) to the CCOR sub-unit inside the
 * awb_meas unit.
 *
 * This is required when performing measurements in the YUV colorspace.
 * Operation in RGB colorspace bypasses the CCOR sub-unit so that it does
 * not need to be programmed.
 * *****************************************************************************/
int rpp_awb_meas_set_profile(rpp_awb_meas_drv_t * drv, const rpp_ccor_profile_t * const p);

/**************************************************************************//**
 * @brief Retrieve color conversion coefficient from the awb_meas unit.
 * @param[in]   drv     initialized AWB_MEAS driver structure describing the awb_meas unit instance
 * @param[out]  p       current profile (@see rpp_ccor_profile_t)
 * @return      0 on success, error-code otherwise
 *
 * This function reads the currently configured color conversion profile
 * from the unit and supplies the data in the supplied @c p argument.
 *
 * In case of failures retrieving the data from the unit, the function
 * returns an error code and the data in @c p shall not be considered valid.
 *****************************************************************************/
int rpp_awb_meas_profile(rpp_awb_meas_drv_t * drv, rpp_ccor_profile_t * const p);


/**************************************************************************//**
 * @brief Program the measurement window into the rpp awb_meas unit.
 * @param[in]   drv     initialized AWB_MEAS driver structure describing the awb_meas unit instance
 * @param[in]   win     measurment resolution to set (@see rpp_window_t)
 * @return      0 on success, error-code otherwise
 *
 * This function is used to program the measurement window to the unit that
 * selects the part of the image that shall be considered for measurement.
 * The window must always be programmed before selecting an operation mode.
 *****************************************************************************/
int rpp_awb_meas_set_window(rpp_awb_meas_drv_t * drv, const rpp_window_t * win);

/**************************************************************************//**
 * @brief Retrieves the current measurment window from the rpp awb measurement module.
 * @param[in]   drv     initialized AWB_MEAS driver structure describing the awb_meas unit instance
 * @param[out]  win     holds current measurment resolution on return (@see rpp_window_t)
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int rpp_awb_meas_window(rpp_awb_meas_drv_t * drv, rpp_window_t * const win);


/**************************************************************************//**
 * @brief Programs the measurement conditions to the rpp awb_meas unit.
 * @param[in]   drv         initialized AWB_MEAS driver structure describing the awb_meas unit instance
 * @param[in]   condition   white balance measurement parameters (@see rpp_awb_meas_cond_t)
 * @return      0 on success, error-code otherwise
 *
 * This function is used to program the parameters to the rpp awb_meas
 * unit that are used to decide which pixels inside the measurement window
 * are actually considered for measurement.
 *
 * Depending on the color space the measurement will be performed in, a
 * different set of parameters must be supplied. All possible parameters can,
 * however, be stored in the supplied @c condition argument.
 *****************************************************************************/
int rpp_awb_meas_set_condition(rpp_awb_meas_drv_t * drv, const rpp_awb_meas_cond_t * condition);

/**************************************************************************//**
 * @brief Retrieves the current measurement conditions from the rpp awb_meas unit.
 * @param[in]   drv         initialized AWB_MEAS driver structure describing the awb_meas unit instance
 * @param[out]  condition   white balance measurement parameters (@see rpp_awb_meas_cond_t)
 * @return      0 on success, error-code otherwise
 *
 * This function retrieves the current measurement conditions from the awb_meas
 * unit and supplies the data in the supplied @c condition argument.
 *
 * The @c rgb_valid member is determined automatically from the currently
 * configured operation mode. Therefore, this function only returns a
 * meaningful value in that field if the unit has already been set operating.
 *****************************************************************************/
int rpp_awb_meas_condition(rpp_awb_meas_drv_t * drv, rpp_awb_meas_cond_t * const condition);


/**************************************************************************//**
 * @brief Retrieves the current measurement results from the rpp awb_meas unit.
 * @param[in]   drv     initialized AWB_MEAS driver structure describing the awb_meas unit instance
 * @param[out]  meas    current mean values (@see rpp_awb_meas_t)
 * @return      0 on success, error-code otherwise
 *
 * This function is used to retrieve the current set of measurement results
 * from the unit. The data is supplied in the provided @c meas argument.
 *
 * The determined mean values will only contain valid data if the unit has
 * been operational for sufficiently to complete the measurement.
 *
 * The @c rgb_valid member is determined automatically from the currently
 * configured operation mode. Therefore, this function only returns a
 * meaningful value in that field if the unit has not yet been stopped.
 *****************************************************************************/
int rpp_awb_meas(rpp_awb_meas_drv_t * drv, rpp_awb_meas_t * const meas);

/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_AWB_MEAS_DRV_H__ */

