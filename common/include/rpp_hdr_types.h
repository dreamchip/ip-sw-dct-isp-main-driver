/******************************************************************************
 *
 * Copyright 2019, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_hdr_types.h
 *
 * @brief   General types for the RPP-HDR pipeline.
 *
 *****************************************************************************/
#ifndef __RPP_HDR_TYPES_H__
#define __RPP_HDR_TYPES_H__

#include "rpp_config.h"

#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppHdrTypes RPP - General type definitions
 * RPP - General type definitions
 * @{
 *****************************************************************************/

/**************************************************************************//**
 * @brief The type declares an image window typically used for configuring
 *        measurement or cropping resolutions.
 *****************************************************************************/
typedef struct
{
    uint16_t    hoff;   /**< horizontal offset */
    uint16_t    voff;   /**< vertical offset */
    uint16_t    width;  /**< image width */
    uint16_t    height; /**< image height */
} rpp_window_t;

/**************************************************************************//**
 * @def RPP_SET_WINDOW(cfg,v)
 * Macro to configure a window  (@ref rpp_window_t) 
 *****************************************************************************/
#define RPP_SET_WINDOW(w,a,b,c,d)   { w.hoff=a; w.voff=b; w.width=c; w.height=d; }

/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_HDR_TYPES_H__ */

