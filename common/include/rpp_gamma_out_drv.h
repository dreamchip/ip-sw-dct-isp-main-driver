/******************************************************************************
 *
 * Copyright 2019 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_gamma_out_drv.h
 *
 * @brief   Interface of gamma-out driver
 *
 *****************************************************************************/
#ifndef __RPP_GAMMA_OUT_DRV_H__
#define __RPP_GAMMA_OUT_DRV_H__

#include "rpp_config.h"
#include <rpp_hdr_types.h>

#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppGammaOutDrv RPP - Gamma Out Driver
 * RPP - Gamma Out Driver
 * @{
 *
 *****************************************************************************/

/**************************************************************************//**
 * @brief Number of curve points.
 *****************************************************************************/
#define GAMMA_OUT_NUM_SAMPLES               ( 17u )

/**************************************************************************//**
 * @def SET_GAMMA_OUT_Y(m,i,v)
 * Macro to set a gamma value in curve.
 *****************************************************************************/
#define SET_GAMMA_OUT_Y(m,i,v)              ( m.y[i] = (v) )

/**************************************************************************//**
 * @def GET_GAMMA_OUT_Y(m,i)
 * Macro to get a gamma value from curve.
 *****************************************************************************/
#define GET_GAMMA_OUT_Y(m,i)                ( m.y[i] )

/**************************************************************************//**
 * @def SET_GAMMA_OUT_BT709(c,colorbits)
 * @param c         variable holding the gamma curve (no pointer)
 * @param colorbits variable holding the colorbits for which to generate the curve
 *
 * This macro defines a BT.709 gamma curve. The @c colorbits value passed to this
 * function must be either 12 or 24.
 *****************************************************************************/
#define SET_GAMMA_OUT_BT709(c, colorbits)               \
{                                                       \
         if (colorbits == 12) c = preset_bt709_12b;     \
    else if (colorbits == 24) c = preset_bt709_24b;     \
    else c.mode = RPP_GAMMA_OUT_X_MODE_MAX;             \
}

/**************************************************************************//**
 * @def SET_GAMMA_OUT_SRGB(c,colorbits)
 * @param c         variable holding the gamma curve (no pointer)
 * @param colorbits variable holding the colorbits for which to generate the curve
 *
 * This macro defines a sRGB gamma curve. The @c colorbits value passed to this
 * function must be either 12 or 24.
 *****************************************************************************/
#define SET_GAMMA_OUT_SRGB(c, colorbits)                \
{                                                       \
         if (colorbits == 12) c = preset_srgb_12b;      \
    else if (colorbits == 24) c = preset_srgb_24b;      \
    else c.mode = RPP_GAMMA_OUT_X_MODE_MAX;             \
}

/**************************************************************************//**
 * @brief Type of segmentation on the x-axis.
 *****************************************************************************/
enum x_segmentation_mode_e
{
    RPP_GAMMA_OUT_X_MODE_LOG = 0,   /**< logarithmic scale */
    RPP_GAMMA_OUT_X_MODE_EQU = 1,   /**< equidistant scale */
    RPP_GAMMA_OUT_X_MODE_MAX,       /**< max number of supported scale types */
};

/**************************************************************************//**
 * @brief The type declares a gamma curve.
 *****************************************************************************/
typedef struct
{
    uint32_t    mode;                       /**< x segmentation mode */
    uint32_t    y[GAMMA_OUT_NUM_SAMPLES];   /**< curve samples */
} rpp_gamma_out_curve_t;

static const rpp_gamma_out_curve_t preset_bt709_12b =
{
    RPP_GAMMA_OUT_X_MODE_LOG,
    { 0, 288, 541, 730, 887, 1146, 1360, 1547, 1714, 2007, 2261, 2489, 2889, 3237, 3549, 3833, 4095 }
};

static const rpp_gamma_out_curve_t preset_bt709_24b =
{
    RPP_GAMMA_OUT_X_MODE_LOG,
    { 0, 1179936, 2215627, 2991582, 3634608, 4694594, 5572994, 6337109, 7020977, 8220906, 9264718, 10198910, 11838061, 13263951, 14540094, 15703821, 16777215 }
};

static const rpp_gamma_out_curve_t preset_srgb_12b =
{
    RPP_GAMMA_OUT_X_MODE_LOG,
    { 0, 539, 794, 982, 1136, 1386, 1591, 1768, 1926, 2200, 2436, 2646, 3012, 3327, 3607, 3862, 4095 }
};

static const rpp_gamma_out_curve_t preset_srgb_24b =
{
    RPP_GAMMA_OUT_X_MODE_LOG,
    { 0, 2206512, 3254313, 4023116, 4652959, 5679188, 6519928, 7245111, 7889778, 9012032, 9980036, 10840563, 12338591, 13630722, 14779388, 15821021, 16777215 }
};

/**************************************************************************//**
 * @brief RPP GAMMA_OUT specific driver structure
 *
 * This structure encapsulates all data required by the RPP GAMMA_OUT driver to
 * operate a specific instance of the RPP GAMMA_OUT unit on a given RPPHDR device.
 * The data fields contained in this structure are intended to be
 * private to the driver and not to be modified by any higher-level
 * software component.
 *
 * It is required to have a separate instance of this structure for each
 * separate RPP GAMMA_OUT unit in a given RPPHDR device. Each of these structures
 * must be initialized before use by calling the function @c rpp_gamma_out_init()
 * with suitable parameters.
 *
 * After initialization, the @c colorbits member variable is set to the
 * number of bits per color value. This information is needed for the
 * preparation of the gamma curves.
 *
 * The structure is exported here so that instances can statically be declared
 * by higher-level software without the need for dynamic memory allocation.
 *****************************************************************************/
typedef struct {
    uint16_t        version;
    uint16_t        colorbits;
    uint32_t        base;
    rpp_device *    dev;
} rpp_gamma_out_drv_t;


/**************************************************************************//**
 * @brief Initialize the rpp gamma out module.
 * @param[in]       dev     rpp_device structure to be used for all register accesses
 * @param[in]       base    base address of RPP-GAMMA-OUT module
 * @param[inout]    drv     GAMMA_OUT driver structure to be initialized
 * @return      0 on success, error-code otherwise
 *
 * This function initializes the @c drv structure for future uses of the driver
 * on the given instance of the rpp gamma_out unit. It fills the @c drv structure
 * with data required for operating on the given instance and caches data that
 * describes the features of the gamma_out unit instance.
 *
 * This function needs to be called once for every instance of the rpp gamma_out
 * unit to operate, each time with a different @c drv argument intended for
 * that very instance of the rpp gamma_out unit only.
 *****************************************************************************/
int rpp_gamma_out_init(rpp_device * dev, uint32_t base, rpp_gamma_out_drv_t * drv);

/**************************************************************************//**
 * @brief Enable or disable the rpp gamma out module.
 * @param[in]   drv     initialized GAMMA_OUT driver structure describing the gamma_out unit instance
 * @param[in]   on      enable state to set (0: disable, otherwise enable)
 * @return      0 on success, error-code otherwise
 *
 * If the unit is disabled, image data flows through the unit without any
 * modifications. If enabled, a suitable gamma curve must have been programmed
 * in advance.
 *****************************************************************************/
int rpp_gamma_out_enable(rpp_gamma_out_drv_t * drv, const unsigned on);

/**************************************************************************//**
 * @brief Return enable status of the rpp gamma out module.
 * @param[in]   drv     initialized GAMMA_OUT driver structure describing the gamma_out unit instance
 * @param[out]  on      current enable state (0: disabled, otherwise enabled)
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int rpp_gamma_out_enabled(rpp_gamma_out_drv_t * drv, unsigned * const on);

/**************************************************************************//**
 * @brief Set gamma curve configuration in the rpp gamma out module.
 * @param[in]   drv     initialized GAMMA_OUT driver structure describing the gamma_out unit instance
 * @param[in]   curve   pointer to an initialized gamma curve descriptor
 * @return      0 on success, error-code otherwise
 *
 * This function programs a new gamma curve into the unit. There are
 * restrictions on the values of the curve points that depend on the
 * number of @c colorbits reported for the unit instance:
 *  - the curve points must be smaller than 2^colorbits
 *
 * If the data supplied does not match these criteria, the function returns
 * an error code.
 *****************************************************************************/
int rpp_gamma_out_set_curve
(
    rpp_gamma_out_drv_t *               drv,
    const rpp_gamma_out_curve_t * const curve
);

/**************************************************************************//**
 * @brief Get the current gamma curve configuration from the rpp gamma out module.
 * @param[in]   drv     initialized GAMMA_OUT driver structure describing the gamma_out unit instance
 * @param[out]  curve   pointer to a gamma curve descriptor
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int rpp_gamma_out_curve
(
    rpp_gamma_out_drv_t *         drv,
    rpp_gamma_out_curve_t * const curve
);

/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_GAMMA_OUT_DRV_H__ */

