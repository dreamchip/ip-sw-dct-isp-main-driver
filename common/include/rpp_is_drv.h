/******************************************************************************
 *
 * Copyright 2019 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_is_drv.h
 *
 * @brief   Interface of image stabilization cropping driver
 *
 *****************************************************************************/
#ifndef __RPP_IS_DRV_H__
#define __RPP_IS_DRV_H__

#include "rpp_config.h"
#include <rpp_hdr_types.h>

#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppIsDrv RPP - Image Stabilization Driver
 * RPP - Image Stabilization Cropping Driver
 * @{
 *
 *****************************************************************************/

/**************************************************************************//**
 * @brief RPP IS specific driver structure
 *
 * This structure encapsulates all data required by the RPP IS driver to
 * operate a specific instance of the RPP IS on a given RPPHDR device.
 * The data fields contained in this structure are intended to be
 * private to the driver and not to be modified by any higher-level
 * software component.
 *
 * It is required to have a separate instance of this structure for each
 * separate RPP IS module in a given RPPHDR device. Each of these structures
 * must be initialized before use by calling the function @c rpp_is_init()
 * with suitable parameters.
 *
 * The structure is exported here so that instances can statically be declared
 * by higher-level software without the need for dynamic memory allocation.
 *****************************************************************************/
typedef struct {
    uint32_t        version;
    uint32_t        base;
    rpp_device *    dev;
} rpp_is_drv_t;


/**************************************************************************//**
 * @brief Initialize the rpp is module.
 * @param[in]       dev     rpp_device structure to be used for all register accesses
 * @param[in]       base    base address of RPP IS module
 * @param[inout]    drv     IS driver structure to be initialized
 * @return      0 on success, error code otherwise
 *
 * This function initializes the @c drv structure for future uses of the driver
 * on the given instance of the rpp is module. It fills the @c drv structure
 * with data required for operating on the given instance and caches data that
 * describes the features of the is module instance.
 *
 * This function needs to be called once for every instance of the rpp is
 * module to operate, each time with a different @c drv argument intended for
 * that very instance of the rpp is module only.
 *****************************************************************************/
int rpp_is_init(rpp_device * dev, uint32_t base, rpp_is_drv_t * drv);

/**************************************************************************//**
 * @brief Program an output cropping window for the next frame to the rpp is module.
 * @param[in]   drv     initialized IS driver structure describing the is unit instance
 * @param[in]   w       window to set (@see rpp_window_t)
 * @return      0 on success, error code otherwise
 *
 * This function programs the cropping window into the image stabilization
 * unit. This process must happen initially, e.g. before image processing
 * is actually started, to get a valid output image.
 *
 * Cropping window update is also intended to happen during normal operation.
 * Provided that a motion detection sensor is available and that the output
 * image shall be smaller than the captured image, the cropping window
 * offsets may be moved around in the captured image to compensate for detected
 * motions while keeping the output image size constant.
 *
 * @note: Care must be taken to ensure that the cropped window is always fully
 * contained within the captured image, e.g. (w->hoff + w->width) <= capture_width
 * and (w->voff + w->height) < capture_height.
 *
 * @note: The newly programmed window will not be activated immediately, but
 * only for the next frame to be processed.
 *****************************************************************************/
int rpp_is_set_window(rpp_is_drv_t * drv, const rpp_window_t * const w);

/**************************************************************************//**
 * @brief Retrieve the output cropping window for the next frame from the rpp is module.
 * @param[in]   drv     initialized IS driver structure describing the is unit instance
 * @param[out]  w       current cropping window geometry (@see rpp_window_t)
 * @return      0 on success, error code otherwise
 *
 * This function retrieves the currently programmed output cropping window from
 * the unit and stores the data in the provided structure. The window geometry
 * retrieved from the unit may not yet be active at the time of asking, it may
 * not be activated before the next frame is processed. To retrieve the currently
 * active frame, use rpp_is_active_window().
 *****************************************************************************/
int rpp_is_next_window(rpp_is_drv_t * drv, rpp_window_t * const w);

/**************************************************************************//**
 * @brief Retrieve the currently active cropping window from the rpp is module.
 * @param[in]   drv     initialized IS driver structure describing the is unit instance
 * @param[out]  w       current cropping window geometry (@see rpp_window_t)
 * @return      0 on success, error code otherwise
 *
 * This function retrieves the currently active output cropping window from
 * the unit and stores the data in the provided structure. The window geometry
 * may be valid for the current frame only and change for the next frame. To
 * learn about the cropping window used for the next frame, use the
 * rpp_is_next_window() function.
 *****************************************************************************/
int rpp_is_active_window(rpp_is_drv_t * drv, rpp_window_t * const w);


/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_IS_DRV_H__ */

