/******************************************************************************
 *
 * Copyright 2019 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_exm_drv.h
 *
 * @brief   Interface of exposure measurment driver
 *
 *****************************************************************************/
#ifndef __RPP_EXM_DRV_H__
#define __RPP_EXM_DRV_H__

#include "rpp_config.h"
#include <rpp_hdr_types.h>

#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppExmDrv RPP - Exposure Measurement Driver
 * @brief RPP - Exposure Measurement Driver
 *
 * This unit performs exposure measurements on RGB image data.
 * Both, full RGB pixels (with an R, G, and B component per
 * pixel), and interleaved Bayer data (a single component image with different
 * color interpretation R, GR, B, GB per pixel) can be handled.
 *
 * For exposure measurement, the picture is divided into a
 * grid of 5x5 cells. For each cell, the mean luminance value
 * is calculated over all the pixels in the cell and returned
 * as measured result. Depending on the bit width of the data path,
 * the measured results will have a different value range:
 *  - 8bit results for 12bit data paths
 *  - 20bit results for the 24bit data path.
 *
 * To perform the measurements, an operation mode (and data
 * channel) must be supplied, as well as a formula to calculate
 * the brightness from the color data, and an observation window
 * that may cover all or just a part of the active image area.
 * The window width and height must be divisible by 5 so that the
 * 5x5 subwindow cell grid can be defined.
 *
 * @note All configuration of the unit is subject to register
 * shadowing and will not have an immediate effect. Any configuration
 * programmed to the unit will be activated synchronized with an
 * end-of-frame signal when the shadow register update has been
 * requested in the top-level RPP unit register.
 * The currently active configuration cannot be retrieved from
 * the unit.
 * @{
 *
 *****************************************************************************/

/**************************************************************************//**
 * @brief This definition defines the number of coefficients of the color
 *        space conversion in the exposure unit.
 *****************************************************************************/
#define RPP_EXM_COEFF_NUM  ( 4u )

/**************************************************************************//**
 * @brief This definition defines the number of measurmed mean values in the
 *        exposure measurment unit.
 *****************************************************************************/
#define RPP_EXM_MEANS_NUM  ( 5u * 5u )

/**************************************************************************//**
 * @brief Defines an RPP exposure color conversion coefficient vector.
 *
 * During exposure measurements, incoming color pixels must be
 * converted to a luminance (e.g. brightness) value. This data type
 * represents a vector that, when multiplied with a pixel vector
 * whose components are the individual color values 
 * (either a 3-component RGB pixel or a 4-component Bayer cluster), results
 * in a scalar luminance value. The vector can be interpreted as
 * the first line of an RGB => YUV color conversion matrix.
 *
 * This vector has 4 components so that when operating in
 * Bayer mode, green pixels in red lines and green pixels in
 * blue lines can be weighted differently. When operating in
 * RGB mode, the 4th coefficient is set to zero.
 *
 * The coefficient values are represented as unsigned 8-bit
 * fixed point numbers with a fractional part of 7 bit (FP1.7
 * format), where 1.0 is represented by a value of 128.
 *****************************************************************************/
typedef uint32_t rpp_exm_csm_t[RPP_EXM_COEFF_NUM];

/**************************************************************************//**
 * @brief This type declares a RPP exposure mean value matrix.
 *****************************************************************************/
typedef uint32_t rpp_exm_mean_t[RPP_EXM_MEANS_NUM];

/**************************************************************************//**
 * @def SET_EXM_CSM_COEFF(m,i,v)
 * Macro to set a color conversion coefficient <i>i</i>
 * in a color conversion matrix (@ref rpp_exm_csm_t).
 *****************************************************************************/
#define SET_EXM_CSM_COEFF(m,i,v)    ( m[i] = (v) )

/**************************************************************************//**
 * @def GET_EXM_CSM_COEFF(m,i)
 * Macro to get a color conversion coefficient <i>i</i>
 * from a color conversion matrix (@ref rpp_exm_csm_t).
 *****************************************************************************/
#define GET_EXM_CSM_COEFF(m,i)      ( m[i] )

/**************************************************************************//**
 * @def SET_EXM_BT601_RGB_2_Y(f)
 * This Macro defines conversion coefficients for a BT.601 RGB
 * to YUV conversion. Following formula shows this conversion.
 *
 * \f[
 *      Y_{OUT}
 *      =
 *      \begin{bmatrix}
 *           0.2990 &  0.5870 &  0.1140 \\
 *      \end{bmatrix}
 *      *
 *      \begin{bmatrix} R_{IN}  \\ G_{IN}  \\ B_{IN}  \end{bmatrix}
 * \f]
 *
 * @note: Keep balancing by making sure the coefficient sum is 1.
 *****************************************************************************/
#define SET_EXM_BT601_RGB_2_Y(f)    \
{                                   \
    SET_EXM_CSM_COEFF(f, 0,  38 );  \
    SET_EXM_CSM_COEFF(f, 1,  75 );  \
    SET_EXM_CSM_COEFF(f, 2,  15 );  \
    SET_EXM_CSM_COEFF(f, 3,   0 );  \
}

/**************************************************************************//**
 * @def SET_EXM_BT601_BAYER_2_Y(f)
 * This Macro defines conversion coefficients for a BT.601 BAYER
 * to YUV conversion.
 *****************************************************************************/
#define SET_EXM_BT601_BAYER_2_Y(f)  \
{                                   \
    SET_EXM_CSM_COEFF(f, 0,  38 );  \
    SET_EXM_CSM_COEFF(f, 1,  75 );  \
    SET_EXM_CSM_COEFF(f, 2,  15 );  \
    SET_EXM_CSM_COEFF(f, 3,  75 );  \
}

/**************************************************************************//**
 * @def SET_EXM_REC709_RGB_2_Y(f)
 * This Macro defines conversion coefficients for a BT.601 RGB
 * to YUV conversion. Following formula shows this conversion.
 *
 * \f[
 *      Y_{OUT}
 *      =
 *      \begin{bmatrix}
 *           0.2126 &  0.7152 &  0.0722 \\
 *      \end{bmatrix}
 *      *
 *      \begin{bmatrix} R_{IN}  \\ G_{IN}  \\ B_{IN}  \end{bmatrix}
 * \f]

 * @note: Keep balancing by making sure the coefficient sum is 1.
 *****************************************************************************/
#define SET_EXM_REC709_RGB_2_Y(f)   \
{                                   \
    SET_EXM_CSM_COEFF(f, 0, 27);    \
    SET_EXM_CSM_COEFF(f, 1, 92);    \
    SET_EXM_CSM_COEFF(f, 2,  9);    \
    SET_EXM_CSM_COEFF(f, 3,  0 );   \
}
/**************************************************************************//**
 * @def SET_EXM_REC709_BAYER_2_Y(f)
 * This Macro defines conversion coefficients for a BT.601 BAYER
 * to YUV conversion.
 *****************************************************************************/
#define SET_EXM_REC709_BAYER_2_Y(f) \
{                                   \
    SET_EXM_CSM_COEFF(f, 0, 27);    \
    SET_EXM_CSM_COEFF(f, 1, 92);    \
    SET_EXM_CSM_COEFF(f, 2,  9);    \
    SET_EXM_CSM_COEFF(f, 3, 92);    \
}

/**************************************************************************//**
 * @def SET_EXM_RGB_2_R(f)
 * This Macro defines conversion coefficients for a exposure measurement
 * of red component. Following formula shows this conversion.
 *
 * \f[
 *      Y_{OUT}
 *      =
 *      \begin{bmatrix}
 *           1.0 &  0.0 &  0.0 \\
 *      \end{bmatrix}
 *      *
 *      \begin{bmatrix} R_{IN}  \\ G_{IN}  \\ B_{IN}  \end{bmatrix}
 * \f]
 *
 * @note: Keep balancing by making sure the coefficient sum is 1.
 *****************************************************************************/
#define SET_EXM_RGB_2_R(f)          \
{                                   \
    SET_EXM_CSM_COEFF(f, 0, 128 );  \
    SET_EXM_CSM_COEFF(f, 1,   0 );  \
    SET_EXM_CSM_COEFF(f, 2,   0 );  \
    SET_EXM_CSM_COEFF(f, 3,   0 );  \
}

/**************************************************************************//**
 * @def SET_EXM_RGB_2_G(f)
 * This Macro defines conversion coefficients for a exposure measurement
 * of green component. Following formula shows this conversion.
 *
 * \f[
 *      Y_{OUT}
 *      =
 *      \begin{bmatrix}
 *           0.0 &  1.0 &  0.0 \\
 *      \end{bmatrix}
 *      *
 *      \begin{bmatrix} R_{IN}  \\ G_{IN}  \\ B_{IN}  \end{bmatrix}
 * \f]
 *
 * @note: Keep balancing by making sure the coefficient sum is 1.
 *****************************************************************************/
#define SET_EXM_RGB_2_G(f)          \
{                                   \
    SET_EXM_CSM_COEFF(f, 0,   0 );  \
    SET_EXM_CSM_COEFF(f, 1, 128 );  \
    SET_EXM_CSM_COEFF(f, 2,   0 );  \
    SET_EXM_CSM_COEFF(f, 3,   0 );  \
}

/**************************************************************************//**
 * @def SET_EXM_RGB_2_B(f)
 * This Macro defines conversion coefficients for a exposure measurement
 * of green component. Following formula shows this conversion.
 *
 * \f[
 *      Y_{OUT}
 *      =
 *      \begin{bmatrix}
 *           0.0 &  0.0 &  1.0 \\
 *      \end{bmatrix}
 *      *
 *      \begin{bmatrix} R_{IN}  \\ G_{IN}  \\ B_{IN}  \end{bmatrix}
 * \f]
 *
 * @note: Keep balancing by making sure the coefficient sum is 1.
 *****************************************************************************/
#define SET_EXM_RGB_2_B(f)          \
{                                   \
    SET_EXM_CSM_COEFF(f, 0,   0 );  \
    SET_EXM_CSM_COEFF(f, 1,   0 );  \
    SET_EXM_CSM_COEFF(f, 2, 128 );  \
    SET_EXM_CSM_COEFF(f, 3,   0 );  \
}

/**************************************************************************//**
 * @brief RPP exposure measurement modes
 *****************************************************************************/
enum rpp_exm_measure_mode_e
{
    RPP_EXM_MEASURE_MODE_DISABLE  = 0,    /**< disable exposure measurment */
    RPP_EXM_MEASURE_MODE_Y        = 1,    /**< converted to Y by using coefficients (red, green and blue) */
    RPP_EXM_MEASURE_MODE_BAYER    = 2,    /**< measure red bayer component */
    RPP_EXM_MEASURE_MODE_MAX,
};

/**************************************************************************//**
 * @brief RPP exm channel (measurement/tap point in pipeline)
 *****************************************************************************/
enum rpp_exm_measure_channel_e
{
    RPP_EXM_MEASURE_CHANNEL_0 = 0,      /**< measurement after ACQ (acquisition) */
    RPP_EXM_MEASURE_CHANNEL_1 = 1,      /**< measurement after BLS (blacklevel correction) */
    RPP_EXM_MEASURE_CHANNEL_2 = 2,      /**< measurement after GAMMA_IN (de-gamma/input linearization correction) */
    RPP_EXM_MEASURE_CHANNEL_3 = 3,      /**< measurement after LSC4 (lense-shade correction) */
    RPP_EXM_MEASURE_CHANNEL_4 = 4,      /**< measurement after AWB_GAIN (wb gain correction) */
    RPP_EXM_MEASURE_CHANNEL_5 = 5,      /**< measurement after DPCC (defect pixel correction) */
    RPP_EXM_MEASURE_CHANNEL_6 = 6,      /**< measurement after DPF (bilateral filtering) */
    RPP_EXM_MEASURE_CHANNEL_7 = 7,      /**< not used */
    RPP_EXM_MEASURE_CHANNEL_MAX = RPP_EXM_MEASURE_CHANNEL_7,
};


/**************************************************************************//**
 * @brief RPP EXM specific driver structure
 *
 * This structure encapsulates all data required by the RPP EXM driver to
 * operate a specific instance of the RPP EXM on a given RPPHDR device.
 * The data fields contained in this structure are intended to be
 * private to the driver and not to be modified by any higher-level
 * software component.
 *
 * It is required to have a separate instance of this structure for each
 * separate RPP EXM unit in a given RPPHDR device. Each of these structures
 * must be initialized before use by calling the function @c rpp_exm_init()
 * with suitable parameters.
 *
 * After initialization, the @c resultbits member variable is set to the
 * number of bits per measurement result value. This information is needed
 * for the interpretation of measurement results.
 *
 * The structure is exported here so that instances can statically be declared
 * by higher-level software without the need for dynamic memory allocation.
 *****************************************************************************/
typedef struct {
    uint16_t        version;
    uint16_t        resultbits;
    uint32_t        base;
    rpp_device *    dev;
} rpp_exm_drv_t;

/**************************************************************************//**
 * @brief Initialize the rpp exm unit.
 * @param[in]       dev     rpp_device structure to be used for all register accesses
 * @param[in]       base    base address of RPP EXM unit 
 * @param[inout]    drv     EXM driver structure to be initialized
 * @return          0 on success, error code otherwise
 *****************************************************************************/
int rpp_exm_init(rpp_device * dev, uint32_t base, rpp_exm_drv_t * drv);

/**************************************************************************//**
 * @brief Disable the rpp exm unit.
 * @param[in]   drv     initialized EXM driver structure describing the exm unit instance
 * @return      0 on success, error code otherwise
 *
 * This function is used to disable the exposure measurement unit. When disabled,
 * the result registers will no longer be updated.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_exm_disable(rpp_exm_drv_t * drv);

/**************************************************************************//**
 * @brief Program measurement mode to the rpp exm unit.
 * @param[in]   drv     initialized EXM driver structure describing the exm unit instance
 * @param[in]   mode    specifies the operation mode and input data type (@see rpp_exm_measure_mode_e)
 * @param[in]   channel specifies the input data source (@see rpp_exm_measure_channel_e)
 * @return      0 on success, error code otherwise
 *
 * This function enables the EXM unit to measure image data of the selected type
 * (3 component RGB or interleaved Bayer data). The channel argument describes
 * the source of the image data, several possible sources are available.
 * An error code is returned by this function if the supplied arguments are
 * not within the allowed value ranges.
 *
 * @note The selected input data type (RGB or Bayer) must be provided by the
 * selected input channel.
 *
 * @note When the EXM unit is disabled using this function
 * (e.g. \c mode == RPP_EXM_MEASURE_MODE_DISABLE), the value of the
 * channel argument is ignored.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_exm_set_mode(rpp_exm_drv_t * drv,
        const unsigned mode, const unsigned channel);

/**************************************************************************//**
 * @brief Retrieve current measurement mode from the rpp exm unit.
 * @param[in]   drv     initialized EXM driver structure describing the exm unit instance
 * @param[out]  mode    selected operation mode and input data type (@see rpp_exm_measure_mode_e)
 * @param[out]  channel selected input data source (@see rpp_exm_measure_channel_e)
 * @return      0 on success, error code otherwise
 *****************************************************************************/
int rpp_exm_mode(rpp_exm_drv_t * drv,
        unsigned * const mode, unsigned * const channel);

/**************************************************************************//**
 * @brief Program Y color conversion coefficients to the rpp exm unit.
 * @param[in]   drv     initialized EXM driver structure describing the exm unit instance
 * @param[in]   csm     color conversion coefficients (@see rpp_exm_csm_t)
 * @return      0 on success, error code otherwise
 *
 * This function is used to supply color conversion coefficients for
 * the unit that is used to calculate a luminance value from the
 * incoming color pixel data. The coefficients are in unsigned FP1.7
 * format (8bit unsigned integer values), where 1.0 is represented by
 * a value of 128.
 *
 * The color scaling vector must be set-up according to the selected
 * operation mode, that is, differently for RGB or Bayer input data.
 * A number of default conversion coefficients are available from
 * macros in the driver's header file.
 *
 * The function returns an error when any of the supplied conversion
 * coefficients are not within the allowed range.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_exm_set_csm(rpp_exm_drv_t * drv, const rpp_exm_csm_t csm);

/**************************************************************************//**
 * @brief Retrieve Y color conversion coefficients from the rpp exm unit.
 * @param[in]   drv     initialized EXM driver structure describing the exm unit instance
 * @param[out]  csm     current color conversion coefficients (@see rpp_exm_csm_t)
 * @return      0 on success, error code otherwise
 *****************************************************************************/
int rpp_exm_csm(rpp_exm_drv_t * drv, rpp_exm_csm_t csm);

/**************************************************************************//**
 * @brief Programs the exposure measurement window to the unit.
 * @param[in]   drv     initialized EXM driver structure describing the exm unit instance
 * @param[in]   win     measurement window to configure (@see rpp_window_t)
 * @return      0 on success, error code otherwise
 *
 * This function is used to supply an exposure measurement window
 * for the unit that is used to limit exposure measurements to the
 * selected pixels. Note that the window width and height both must be
 * divisible by 5 so that the 5x5 subwindow grid, for which the individual
 * mean luminance values are calculated, is well defined.
 *
 * @note In case of measuring Bayer data, the window's hoff and voff shall
 * be even. Furthermore, the window's width and height shall
 * be divisible by 10. This is to ensure that the measurement subwindow
 * borders do not cut any Bayer clusters in half.
 *
 * @warning After adding the measured exposure samples, the unit requires some
 * time to complete calculations of the averages (which involves lengthy integer
 * divisions). The window programmed here must stop at least two lines before
 * the actual End of Frame, or put into pseudo code:
 * @code
 * (win->voff + win->height) < (image_height - 2)
 * @endcode
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_exm_set_meas_window(rpp_exm_drv_t * drv, const rpp_window_t * win);

/**************************************************************************//**
 * @brief Retrieves the exposure measurement window from the unit.
 * @param[in]   drv     initialized EXM driver structure describing the exm unit instance
 * @param[out]  win     current measurement window (@see rpp_window_t)
 * @return      0 on success, error code otherwise
 *
 * This function is used to retrieve the current measurement window
 * from the unit. The geometry is returned in the supplied structure.
 *****************************************************************************/
int rpp_exm_meas_window(rpp_exm_drv_t * drv, rpp_window_t * const win);

/**************************************************************************//**
 * @brief Retrieve the measured mean luminance values from the rpp exm unit.
 * @param[in]   drv     initialized EXM driver structure describing the exm unit instance
 * @param[out]  means   current mean values (@see rpp_exm_mean_t)
 * @return      0 on success, error code otherwise
 *
 * This function is used to retrieve the measurement results from the unit after
 * measurement is complete.
 * The average values are stored in bins, with one bin for each of the
 * measurement subwindows which are arranged in an evenly spaced 5x5 grid
 * across the configured measurement window.
 *
 * The number of valid data bits per bin value (e.g. the numerical limit of
 * the measured values) depends on the bit width of the data path and is
 * contained in the @c resultbits variable of the driver structure @c drv.
 *****************************************************************************/
int rpp_exm_means(rpp_exm_drv_t * drv, rpp_exm_mean_t means);

/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_EXM_DRV_H__ */

