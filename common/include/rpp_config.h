/******************************************************************************
 *
 * Copyright 2017 - 2021, Dream Chip Technologies GmbH. All rights reserved
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_config.h
 *
 * @brief   There are some configuration needed when using this RPP low level
 *          driver within different OS'es, bare-metal or on C-reference models.
 *          This configurations are extracted here.
 *
 *****************************************************************************/
#ifndef __RPP_CONFIG_H__
#define __RPP_CONFIG_H__

#include <linux/types.h>
#include "rpp_helper.h"

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppHdrConfig RPP HDR - Compile-time configuration
 * RPP HDR - Compile-time configuration
 * @{
 *****************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif

/* forward declaration */
typedef struct rpp_device rpp_device;


/**************************************************************************//**
 * @brief Function to initialize a generic device descriptor.
 * @param[inout]    dev     device descriptor to initialize
 * @param[in]       argc    number of arguments to be used during initialization
 * @param[in]       argv    arguments to be used during initialization
 *
 * This function must be provided by a platform-specific backend
 * to initialize the generic device descriptor to be used for
 * all RPP HDR-specific accesses. Most notably, it must fill in
 * all of the function pointers and provide a context for the
 * functions to operate on in dev->priv.
 *
 * To be provided with information about the "location" of the device,
 * the platform-specific backend may make use of the supplied
 * argc/argv variables which allow to pass those information in a
 * generic way. The caller of this function must actually be aware
 * of the platform-specific backend being used and the implications
 * for storing data in argc/argv as required by the type of backend.
 *****************************************************************************/
extern int rpp_device_init(rpp_device * dev, int argc, const char **argv);


/**************************************************************************//**
 * @brief Generic device descriptor for register access
 *
 * This structure encapsulates register access to the RPPHDR register space
 * and is used by every low-level driver.
 * 
 * The methods defined in this structure allow a driver to read and write
 * to the RPPHDR registers without explicitly knowing how these registers
 * are accessed.
 *
 * An application must prepare a ready-to-use version of this structure
 * with all function pointers and device-private data before calling any
 * of the driver functions. Equivalently, the information in this structure
 * must remain valid until after the last driver function has been called.
 *
 * The field 'priv' is for rpp_device internal use and can hold any data
 * required for the register access to be performed, such as:
 *  - RPPHDR offset address for direct register access via address bus
 *  - RPPHDR mapped register space address for access via ioremap() or mmap()
 *  - RPPHDR device descriptor, device address, bus number and speed for register access via I2C
 *  - RPPHDR device descriptor, bus number, chip select and SPI mode for register access via SPI.
 *
 * @todo Encapsulate interrupt handling here, e.g. allow requesting a certain
 * IRQ and to specify a handler for the IRQ that is called when the IRQ is raised.
 * @todo Encapsulate DMA handling here for streaming data returned from hist256, dpcc,
 * and image input/output interfaces.
 *****************************************************************************/

struct rpp_device {
    /* @brief private data pointer for device access methods
     *
     * This parameter holds a pointer that can be used by a specific implementation
     * of the RPPHDR device access methods to encapsulate access-specific details.
     *
     * The code that provides the function pointers within this structure shall make
     * sure that all parameters that need to be known for device access are
     * accessible via this pointer. This usually includes defining a custom data
     * structure that is also known by the functions whose pointers are stored here,
     * filling an instance of that structure with suitable data and placing a pointer
     * to that instance into the \c priv member besides the function's pointers.
     *
     * The functions whose pointers are stored in this structure shall know how
     * to interpret the data stored in the \c priv pointer and use it to access
     * the device.
     */
    void *  priv;
    /*! @brief function to read one register from the device
     * @param[in]   dev         valid rpp_device structure instance
     * @param[in]   unit_base   offset of the sub-unit within the RPPHDR address space
     * @param[in]   reg_off     offset of the register inside the sub-unit of the RPPHDR
     * @param[out]  regval      on successful return, the register value is stored there
     * @return  0 on success, -1 on errors
     *
     * This function reads one register from the RPPHDR device.
     * The caller should make sure that the address requested
     * here is valid. The register to be read is calculated as
     * @code
     * rpp_hdr[unit_base + reg_off]
     * @endcode
     * where @c unit_base and @c reg_off must be given in bytes and be aligned to
     * a 32bit boundary. @c unit_base is the base address of the corresponding sub-unit
     * (e.g. MAIN_PRE_1_ACQ, MAIN_PRE_1_BLS, MAIN_PRE_1_GAMMA_IN) within the RPPHDR, and
     * @c reg_off is the offset of the requested register inside the sub-unit.
     *
     * The caller should also make sure that the memory pointed to by
     * @c regval contains sufficients space for one 32-bit register.
     *
     * On success, the function returns a value of \c 0. When errors are encountered
     * while accessing the RPPHDR device (which may happen via I2C or SPI buses),
     * the function returns \c -1 and the data returned in \c regval shall not
     * be considered valid.
     */
    int     (*readl)(rpp_device * dev, uint32_t unit_base, uint32_t reg_off, uint32_t * regval);

    /*! @brief function to write one register to the device
     * @param[in]   dev         valid rpp_device structure instance
     * @param[in]   unit_base   offset of the sub-unit within the RPPHDR address space
     * @param[in]   reg_off     offset of the register inside the sub-unit of the RPPHDR
     * @param[in]   regval      value to write to the RPPHDR register
     * @return 0 on success, -1 on errors
     *
     * This function writes one register to the RPPHDR device.
     * The caller should make sure that the address being written to
     * is valid. The register to be written is calculated as
     * @code
     * rpp_hdr[unit_base + reg_off]
     * @endcode
     * where @c unit_base and @c reg_off must be given in bytes and be aligned to
     * a 32bit boundary. @c unit_base is the base address of the corresponding sub-unit
     * (e.g. MAIN_PRE_1_ACQ, MAIN_PRE_1_BLS, MAIN_PRE_1_GAMMA_IN) within the RPPHDR, and
     * @c reg_off is the offset of the requested register inside the sub-unit.
     *
     * On success, the function returns a value of \c 0. When errors are encountered
     * while accessing the RPPHDR device (which may happen via I2C or SPI buses),
     * the function returns \c -1 and the register shall be assumed to not holding
     * the specified value.
     */
    int     (*writel)(rpp_device * dev, uint32_t unit_base, uint32_t reg_off, uint32_t regval);
    /*! @brief function to read multiple registers from the device
     * @param[in]   dev         valid rpp_device structure instance
     * @param[in]   unit_base   offset of the sub-unit within the RPPHDR address space
     * @param[in]   reg_off     offset of the first register inside the sub-unit of the RPPHDR
     * @param[in]   nregs       number of registers to read
     * @param[in]   increment   address increment for subsequent register reads
     * @param[out]  regvals     on successful return, the register values are stored there
     * @return  0 on success, -1 on errors
     *
     * This function reads multiple registers from the RPPHDR device.
     * The caller should make sure that all the addresses requested
     * here are valid. The registers to be read are calculated as
     * @code
     * rpp_hdr[unit_base + reg_off + i * increment], 0 <= i < nregs
     * @endcode
     * where @c unit_base and @c reg_off must be given in bytes and be aligned to
     * a 32bit boundary. @c unit_base is the base address of the corresponding sub-unit
     * (e.g. MAIN_PRE_1_ACQ, MAIN_PRE_1_BLS, MAIN_PRE_1_GAMMA_IN) within the RPPHDR, and
     * @c reg_off is the offset of the first requested register inside the sub-unit.
     * The @c increment is allowed to be zero, in which case the same register address is
     * read multiple times. This is useful for indirect RAM access with a separate
     * indirect address register that is auto-incremented with every access to the
     * indirect data register.
     *
     * The caller should also make sure that the memory pointed to by
     * @c regvals contains sufficients space for at least \c nregs 32-bit registers.
     *
     * On success, the function returns a value of \c 0. When errors are encountered
     * while accessing the RPPHDR device (which may happen via I2C or SPI buses),
     * the function returns \c -1 and the data returned in \c regvals shall not
     * be considered valid.
     */
    int (*read_regs)
        (
         rpp_device * dev, uint32_t unit_base, uint32_t reg_off,
         uint32_t nregs, uint32_t increment, uint32_t * regvals
        );

    /*! @brief function to write multiple registers to the device
     * @param[in]   dev         valid rpp_device structure instance
     * @param[in]   unit_base   offset of the sub-unit within the RPPHDR address space
     * @param[in]   reg_off     offset of the register inside the sub-unit of the RPPHDR
     * @param[in]   nregs       number of registers to write
     * @param[in]   increment   address increment for subsequent register writes
     * @param[in]   regvals     values to write to the RPPHDR registers
     * @return 0 on success, -1 on errors
     *
     * This function writes multiple values to registers in the RPPHDR device.
     * The caller should make sure that the addresses being written to
     * are valid. The registers to be written are calculated as
     * @code
     * rpp_hdr[unit_base + reg_off + i * increment], 0 <= i < nregs
     * @endcode
     * where @c unit_base and @c reg_off must be given in bytes and be aligned to
     * a 32bit boundary. @c unit_base is the base address of the corresponding sub-unit
     * (e.g. MAIN_PRE_1_ACQ, MAIN_PRE_1_BLS, MAIN_PRE_1_GAMMA_IN) within the RPPHDR, and
     * @c reg_off is the offset of the first requested register inside the sub-unit.
     *
     * The @c increment is allowed to be zero, in which case the same register address is
     * written to multiple times. This is useful for indirect RAM access with a separate
     * indirect address register that is auto-incremented with every access to the
     * indirect data register.
     *
     * The caller should also make sure that the memory pointed to by
     * @c regvals holds enough data for at least \c nregs 32-bit registers.
     *
     * On success, the function returns a value of \c 0. When errors are encountered
     * while accessing the RPPHDR device (which may happen via I2C or SPI buses),
     * the function returns \c -1 and the registers shall be assumed to not holding
     * the specified values.
     */
    int (*write_regs)
        (
         rpp_device * dev, uint32_t unit_base, uint32_t reg_off,
         uint32_t nregs, uint32_t increment, const uint32_t * regvals
        );
};

/**************************************************************************//**
 * @def DRV_VERSION
 * @brief Version of this software package
 *****************************************************************************/
#define DRV_VERSION                     ( '0.2.0' )

/**************************************************************************//**
 * @def EFAULT
 * Definition of a general error code wich is IEEE compliant and may be missed
 * when compiling. Needed by RPP HDR low level driver.
 *****************************************************************************/
#ifndef EFAULT
#  define EFAULT    ( 14 )
#endif  // !EFAULT

/**************************************************************************//**
 * @def EBUSY
 * Definition of busy error code wich is IEEE compliant and may be missed
 * when compiling. Needed by RPP HDR low level driver.
 *****************************************************************************/
#ifndef EBUSY
#  define EBUSY     ( 16 )
#endif  // !EBUSY

/**************************************************************************//**
 * @def ENODEV
 * Definition of no-device error code wich is IEEE compliant and may be missed
 * when compiling. Needed by RPP HDR low level driver.
 *****************************************************************************/
#ifndef ENODEV
#  define ENODEV    ( 19 )
#endif  // !ENODEV

/**************************************************************************//**
 * @def EINVAL
 * Definition of no-device error code wich is IEEE compliant and may be missed
 * when compiling. Needed by RPP HDR low level driver.
 *****************************************************************************/
#ifndef EINVAL
#  define EINVAL    ( 22 )
#endif  // !ENODEV

/**************************************************************************//**
 * @def ERANGE
 * Definition of a general error code wich is IEEE compliant and may be missed
 * when compiling. Needed by RPP HDR low level driver.
 *****************************************************************************/
#ifndef ERANGE
#  define ERANGE    ( 34 )
#endif  // !EFAULT

/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_CONFIG_H__ */

