/******************************************************************************
 *
 * Copyright 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_outregs_drv.h
 *
 * @brief   Interface of Output Selection unit driver
 *
 *****************************************************************************/
#ifndef __RPP_OUTREGS_DRV_H__
#define __RPP_OUTREGS_DRV_H__

#include "rpp_config.h"
#include <rpp_hdr_types.h>

#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppOutregsDrv RPP - Human Vision Output Selection unit driver
 * RPP - Human Vision Output Selection unit driver
 *
 * This driver controls the output selection unit for the human vision output
 * of the RPP HDR pipeline. It is used to select from one one of several
 * sources to be directed towards the output interface of the pipeline,
 * as well as the image format and eventual subsampling options.
 *
 * The unit includes an input selection switch that can select from one of
 * four different inputs:
 *  - the ISP RGB image data from the MAIN_POST pipeline
 *  - the MAIN_PRE_1 ACQ YUV 4:2:2 image data (RPP_lite, ISP Bypass)
 *  - the MAIN_PRE_2 ACQ YUV 4:2:2 image data (RPP_lite, ISP Bypass)
 *  - the MAIN_PRE_3 ACQ YUV 4:2:2 image data (RPP_lite, ISP Bypass)
 *
 * For each of the inputs it is also selectable whether the data received
 * on the input shall be blocked or discarded when the corresponding input
 * is not selected for output. In the RPP HDR, unused inputs shall be configured
 * to discard their data when unselected.
 *
 * Depending on the selected input source the unit additionally respects
 * the output image format setting to automatically enable or disable
 * YUV 4:4:4 to YUV 4:2:2 or 4:2:0 subsampling.
 * @{
 *
 *****************************************************************************/


/**************************************************************************//**
 * @brief Enumerates the supported inputs to the RPP HDR Human Vision output
 *
 * This datatype describes the supported inputs to the RPP HDR Human Vision Output
 * Selection unit which can be propagated to the output interface.
 *****************************************************************************/
typedef enum
{
    /** @brief selects the MAIN_POST RGB data for output.
     * 
     * This value, when specified as input source for the Output Selection
     * unit, specifies to propagate the MAIN_POST pipeline output (which
     * is RGB888 data with 3x12bit per pixel) towards the output interface.
     *
     * When choosing this value, the whole ISP pipeline must be set-up to
     * actually process BAYER data at the input and produce valid RGB data
     * at the end of the MAIN_POST pipeline. */
    RPP_OUT_IN_SEL_MAIN = 0x01,
    /** @brief selects the MAIN_PRE_1 RPP_lite YUV 4:2:2 data for output.
     *
     * This value, when specified as input source for the Output Selection
     * unit, specifies to propagate the YUV 4:2:2 output from the MAIN_PRE_1
     * ACQ (which is 2x12bit per pixel) towards the output interface.
     *
     * When choosing this value, the MAIN_PRE_1 ACQ must be set-up to acquire
     * YUV 4:2:2 input from either the Sensor Interface or the DMA input
     * and output that data on the RPP_lite port, while the ISP pipeline
     * is bypassed.
     *
     * This setting also allows the associated GAMMA_OUT and CCOR sub-units
     * to be left unconfigured as they are bypassed in hardware. */
    RPP_OUT_IN_SEL_PRE1 = 0x02,
    /** @brief selects the MAIN_PRE_2 RPP_lite YUV 4:2:2 data for output.
     *
     * This value, when specified as input source for the Output Selection
     * unit, specifies to propagate the YUV 4:2:2 output from the MAIN_PRE_2
     * ACQ (which is 2x12bit per pixel) towards the output interface.
     *
     * When choosing this value, the MAIN_PRE_2 ACQ must be set-up to acquire
     * YUV 4:2:2 input from either the Sensor Interface or the DMA input
     * and output that data on the RPP_lite port, while the ISP pipeline
     * is bypassed.
     *
     * This setting also allows the associated GAMMA_OUT and CCOR sub-units
     * to be left unconfigured as they are bypassed in hardware. */
    RPP_OUT_IN_SEL_PRE2 = 0x04,
    /** @brief selects the MAIN_PRE_3 RPP_lite YUV 4:2:2 data for output.
     *
     * This value, when specified as input source for the Output Selection
     * unit, specifies to propagate the YUV 4:2:2 output from the MAIN_PRE_3
     * ACQ (which is 2x12bit per pixel) towards the output interface.
     *
     * When choosing this value, the MAIN_PRE_3 ACQ must be set-up to acquire
     * YUV 4:2:2 input from either the Sensor Interface or the DMA input
     * and output that data on the RPP_lite port, while the ISP pipeline
     * is bypassed.
     *
     * This setting also allows the associated GAMMA_OUT and CCOR sub-units
     * to be left unconfigured as they are bypassed in hardware. */
    RPP_OUT_IN_SEL_PRE3 = 0x08,
    RPP_OUT_IN_SEL_MAX  = 0x0F
} rpp_out_input_t;


/**************************************************************************//**
 * @brief Enumerates the supported Human Vision Output image formats
 *
 * This datatype describes the image formats which are available for output
 * by the RPP HDR Human Vision Output Selection unit.
 *****************************************************************************/
typedef enum
{
    /** @brief specifies that 3x8bit RGB pixels shall be generated.
     *
     * This value, when configured as output format for the Human Vision Output,
     * specifies that RGB pixels with 8bit per component shall be  generated.
     *
     * This setting is only valid when the output of the MAIN_POST pipeline
     * stage has been selected as input for the Human Vision Output. A suitable
     * gamma correction curve must be programmed into the associated GAMMA_OUT
     * sub-unit, the associated CCOR and 4:2:2/4:2:0 sub-sampling units are
     * bypassed in hardware and need not be set-up.
     */
    RPP_OUT_FORMAT_RGB8     = 0,
    /** @brief Specifies that interleaved 2x12bit YUV 4:2:2 shall be generated.
     *
     * This value, when configured as output format for the Human Vision Output,
     * specifies that Y/U, Y/V pixels (e.g. YUV 4:2:2 format) with 12bit per
     * Y and U/V component shall be generated.
     *
     * This setting is always valid, independent of the selected input source.
     * However, when the output of the MAIN_POST pipeline is selected as
     * input for the Human Vision Output, a suitable gamma correction and
     * an RGB-to-YUV colorspace conversion must be programmed into the
     * associated GAMMA_OUT and CCOR sub-units. Additionally, a valid method
     * for the 4:2:2 subsampling shall be selected. */
    RPP_OUT_FORMAT_YUV422   = 1,
    /** @brief Specifies that interleaved 2x12bit YUV 4:2:0 with dummy lines shall be generated.
     *
     * This value, when configured as output format for the Human Vision Output,
     * specifies that Y/U, Y/V pixels with 12bit per Y and U/V component shall
     * be generated. Vertical subsampling to YUV 4:2:0 is achieved by filling
     * the U and V components in even lines with dummy data that must be
     * ignored.
     *
     * This setting is always valid, independent of the selected input source.
     * However, when the output of the MAIN_POST pipeline is selected as
     * input for the Human Vision Output, a suitable gamma correction and
     * an RGB-to-YUV colorspace conversion must be programmed into the
     * associated GAMMA_OUT and CCOR sub-units. Additionally, a valid method
     * for the 4:2:2 subsampling shall be selected. */
    RPP_OUT_FORMAT_YUV420   = 2,
    RPP_OUT_FORMAT_MAX = RPP_OUT_FORMAT_YUV420
} rpp_out_format_t;


/**************************************************************************//**
 * @brief Enumerates the supported Human Vision Output 4:2:2 subsampling options
 *
 * This datatype describes the options which are supported for the YUV 4:4:4
 * to YUV 4:2:2 color subsampling by the RPP HDR Human Vision Output
 * Selection unit.
 *****************************************************************************/
typedef enum
{
    /** @brief co-sited subsampling, sequence: Y0Cb0, Y1Cr0, for motion JPEG, MPEG2 mode */
    RPP_OUT_422_CO_SITED        = 0,
    /** @brief co-sited interleaved subsampling, sequence: Y0Cb0, Y1Cr1, not recommended */
    RPP_OUT_422_CO_SITED_2      = 1,
    /** non-co-sited subsampling with linear interpolation, sequence: Y0Cb01mean Y1Cr01mean, for still image JPEG, MPEG1 mode */
    RPP_OUT_422_NON_CO_SITED    = 2,
    RPP_OUT_422_MAX = RPP_OUT_422_NON_CO_SITED
} rpp_out_422_method_t;

/**************************************************************************//**
 * @brief RPP OUTREGS specific driver structure
 *
 * This structure encapsulates all data required by the RPP OUTREGS driver to
 * operate a specific instance of the RPP OUTREGS on a given RPPHDR device.
 * The data fields contained in this structure are intended to be
 * private to the driver and not to be modified by any higher-level
 * software component.
 *
 * It is required to have a separate instance of this structure for each
 * separate RPP OUTREGS module in a given RPPHDR device. Each of these structures
 * must be initialized before use by calling the function @c rpp_outregs_init()
 * with suitable parameters.
 *
 * The structure is exported here so that instances can statically be declared
 * by higher-level software without the need for dynamic memory allocation.
 *****************************************************************************/
typedef struct {
    uint32_t        version;
    uint32_t        base;
    rpp_device *    dev;
} rpp_outregs_drv_t;


/**************************************************************************//**
 * @brief Initialize the rpp outregs module.
 * @param[in]       dev     rpp_device structure to be used for all register accesses
 * @param[in]       base    base address of RPP OUTREGS module
 * @param[inout]    drv     OUTREGS driver structure to be initialized
 * @return      0 on success, error code otherwise
 *
 * This function initializes the @c drv structure for future uses of the driver
 * on the given instance of the rpp outregs module. It fills the @c drv structure
 * with data required for operating on the given instance and caches data that
 * describes the features of the outregs module instance.
 *
 * This function needs to be called once for every instance of the rpp outregs
 * module to operate, each time with a different @c drv argument intended for
 * that very instance of the rpp outregs module only.
 *****************************************************************************/
int rpp_outregs_init(rpp_device * dev, uint32_t base, rpp_outregs_drv_t * drv);


/**************************************************************************//**
 * @brief Selects one of the supported image formats for output.
 * @param[in]   drv     initialized OUTREGS driver structure describing the unit instance
 * @param[in]   format  the selected image format (@see rpp_out_format_t)
 * @return      0 on success, error code otherwise
 *
 *
 * This function configures the output interface to generate one
 * of the image formats that are enumerated in rpp_out_format_t.
 *
 * When selecting one of the image formats, please note the restrictions on
 * the selection of the input source associated with that format and
 * the requirement for further configuration of the Human Vision Output
 * sub-units.
 *****************************************************************************/
int rpp_outregs_set_format(rpp_outregs_drv_t * drv, rpp_out_format_t format);

/**************************************************************************//**
 * @brief Retrieves the image format which is selected for output.
 * @param[in]   drv     initialized OUTREGS driver structure describing the unit instance
 * @param[out]  format  the selected image format (@see rpp_out_format_e)
 * @return      0 on success, error code otherwise
 *
 *
 * This function retrieves the currently selected output image format for the
 * Human Vision Output from the unit.
 *****************************************************************************/
int rpp_outregs_format(rpp_outregs_drv_t * drv, rpp_out_format_t * const format);


/**************************************************************************//**
 * @brief Selects one of the supported YUV 4:2:2 subsampling methods for the Human Vision Output.
 * @param[in]   drv     initialized OUTREGS driver structure describing the unit instance
 * @param[in]   method  the selected 4:2:2 subsampling method (@see rpp_out_422_method_t)
 * @return      0 on success, error code otherwise
 *
 *
 * This function configures the output interface to use one of the
 * 4:4:4 to 4:2:2 chroma subsampling methods that are enumerated
 * in rpp_out_422_method_t for generating YUV output on the Human Vision Output.
 *
 * This setting is only respected if the unit is configured to select the
 * output of the MAIN_POST pipeline as input for the Human Vision Output
 * with an image format of either YUV 4:2:2 or YUV 4:2:0. In any other
 * case, this setting has no effect.
 *****************************************************************************/
int rpp_outregs_set_422_method(rpp_outregs_drv_t * drv, rpp_out_422_method_t method);

/**************************************************************************//**
 * @brief Retrieves the 4:2:2 chroma subsampling method for the Human Vision Output from the unit.
 * @param[in]   drv     initialized OUTREGS driver structure describing the unit instance
 * @param[out]  method  the selected 4:2:2 subsampling method (@see rpp_out_422_method_t)
 * @return      0 on success, error code otherwise
 *
 * This function queries the unit for the currently selected
 * 4:4:4 to 4:2:2 chroma subsampling method for generating YUV output on the
 * Human Vision Output.
 *****************************************************************************/
int rpp_outregs_422_method(rpp_outregs_drv_t * drv, rpp_out_422_method_t * const method);

/**************************************************************************//**
 * @brief Configures the input multiplexer of the Human Vision Output unit.
 * @param[in]   drv             initialized OUTREGS driver structure describing the unit instance
 * @param[in]   unselected_mode whether to block or discard data at unused inputs
 * @param[in]   input_select    which of the inputs shall be selected
 * @return      0 on success, error code otherwise
 *
 * This function is used to program the input multiplexer of the Human Vision
 * Output by
 *  - specifying what happens with the data on unselected inputs, and
 *  - selecting one of the inputs for propagation towards the Human Vision Output.
 *
 * The @c unselected_mode value is a bitmask of the enumerated values in
 * rpp_out_input_t. For each input that is selected in @c unselected_mode,
 * unused data on the input will be discarded. For each input that is not
 * selected in @c unselected_mode, unused data on the input will be blocked.
 * For the RPP HDR, the @c unselected_mode shall be programmed to 0x0F.
 *
 * The @c input_select parameter specifies which of the inputs to use
 * for output on the Human Vision Output. It must be one of the values
 * enumerated in rpp_out_input_t.
 *****************************************************************************/
int rpp_outregs_set_mode(rpp_outregs_drv_t * drv, uint8_t unselected_mode,
        rpp_out_input_t input_select);

/**************************************************************************//**
 * @brief Retrieves the input multiplexer configuration of the Human Vision Output unit.
 * @param[in]   drv             initialized OUTREGS driver structure describing the unit instance
 * @param[out]  unselected_mode whether data at unused inputs is blocked or discarded
 * @param[out]  input_select    the selected input
 * @return      0 on success, error code otherwise
 *
 * This function queries the Human Vision Output unit for the configuration
 * of the input multiplexer.
 *****************************************************************************/
int rpp_outregs_mode(rpp_outregs_drv_t * drv, uint8_t * const unselected_mode,
        rpp_out_input_t * const input_select);



/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_OUTREGS_DRV_H__ */

