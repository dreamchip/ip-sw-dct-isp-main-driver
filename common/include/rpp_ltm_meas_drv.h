/******************************************************************************
 *
 * Copyright 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_ltm_meas_drv.h
 *
 * @brief   Implementation of Local Tone Mapping Measurement unit driver
 *
 *****************************************************************************/
#ifndef __RPP_LTM_MEAS_DRV_H__
#define __RPP_LTM_MEAS_DRV_H__

#include "rpp_config.h"
#include <rpp_hdr_types.h>
#include <rpp_ccor_drv.h>


#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppLtmMeasDrv RPP - Local Tone Mapping Measurement Unit Driver
 * RPP - Local Tone Mapping Measurement Unit Driver
 *
 * This driver is responsible for controlling the measurement
 * unit dedicated to deriving parameters for controlling the
 * Local Tone Mapping module.
 * 
 * It calculates statistics over the image data in a given
 * observation window. The image data in (R,G,B) format is first
 * converted to a scalar luminance Y because tone mapping
 * must take the brightness of pixels into account. The
 * different weights for each color channel are configurable.
 *
 * The unit then calculates a histogram of the luminance values
 * of the pixels and also remembers the min/max luminance values
 * as well as a measure for the geometric mean of the luminance.
 * An 1 bit minimum is applied to the luminance values when
 * calculating the geometric mean to avoid the result becoming
 * zero.
 *
 * The purpose of the histogram is to calculate percentiles of
 * the luminance with high resolution towards lower luminance
 * values. The histogram is divided into 128 bins into which
 * luminance values are sorted. The lower 2 bins are sub-divided
 * into 2 x 64 bins, which gives a total of 254 bins, of which the
 * bottom 128 bins are of high resolution and the upper 126 bins
 * are of lower resolution.
 *
 * Up to 8 thresholds for the luminance percentiles can be
 * configured, where the values have to be given in pixels and
 * must be calculated considering the configured window geometry,
 * e.g. for the 90% percentile a value of
 * @code
 * round(90 * width * height / 100)
 * @endcode
 * must be supplied. The unit determines the histogram
 * bin numbers that satisfy the configured percentiles and supplies
 * them in output registers. From these bin numbers, the corresponding
 * luminance is reconstructed.
 *
 * @note All configuration of the unit is subject to register
 * shadowing and will not have an immediate effect. Any configuration
 * programmed to the unit will be activated synchronized with an
 * end-of-frame signal when the shadow register update has been
 * requested in the top-level RPP unit register.
 * The currently active configuration cannot be retrieved from
 * the unit.
 * @{
 *
 *****************************************************************************/



/**************************************************************************//**
 * @brief Macro defining the maximum number of percentiles that can be calculated.
 *****************************************************************************/
#define RPP_LTM_MEAS_NUM_PRC    8

/**************************************************************************//**
 * @brief RPP LTM_MEAS specific driver structure
 *
 * This structure encapsulates all data required by the RPP LTM_MEAS driver to
 * operate a specific instance of the RPP LTM_MEAS unit on a given RPPHDR device.
 * The data fields contained in this structure are intended to be
 * private to the driver and not to be modified by any higher-level
 * software component.
 *
 * It is required to have a separate instance of this structure for each
 * separate RPP LTM_MEAS unit in a given RPPHDR device. Each of these structures
 * must be initialized before use by calling the function @c rpp_ltm_meas_init()
 * with suitable parameters.
 *
 * The structure is exported here so that instances can statically be declared
 * by higher-level software without the need for dynamic memory allocation.
 *****************************************************************************/
typedef struct {
    uint16_t        version;
    uint16_t        colorbits;
    uint32_t        base;
    rpp_device *    dev;
} rpp_ltm_meas_drv_t;

/**************************************************************************//**
 * @brief Initialize the rpp ltm measurement module.
 * @param[in]       dev     rpp_device structure to be used for all register accesses
 * @param[in]       base    base address of RPP LTM_MEAS module
 * @param[inout]    drv     LTM_MEAS  driver structure to be initialized
 * @return      0 on success, error-code otherwise
 *
 * This function initializes the @c drv structure for future uses of the driver
 * on the given instance of the rpp ltm_meas unit. It fills the @c drv structure
 * with data required for operating on the given instance and caches data that
 * describes the features of the ltm_meas unit instance.
 *
 * This function needs to be called once for every instance of the rpp ltm_meas
 * unit to operate, each time with a different @c drv argument intended for
 * that very instance of the rpp ltm_meas unit only.
 *****************************************************************************/
int rpp_ltm_meas_init(rpp_device * dev, uint32_t base, rpp_ltm_meas_drv_t * drv);

/**************************************************************************//**
 * @brief Enable or disable the rpp ltm measurement unit.
 * @param[in]   drv     initialized LTM_MEAS driver structure describing the ltm_meas unit instance
 * @param[in]   on      enable state to program
 * @return      0 on success, error-code otherwise
 *
 * This function enables or disables the ltm_meas unit. If disabled, image data passes
 * through the unit without being measured and the result registers will not
 * be refreshed.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_ltm_meas_enable(rpp_ltm_meas_drv_t * drv, unsigned on);

/**************************************************************************//**
 * @brief Retrieves enable state from the rpp ltm measurement unit.
 * @param[in]   drv     initialized LTM_MEAS driver structure describing the ltm_meas unit instance
 * @param[out]  on      current enable state
 * @return      0 on success, error-code otherwise
 *
 * This function retrieves the enable state from the unit.
 *****************************************************************************/
int rpp_ltm_meas_enabled(rpp_ltm_meas_drv_t * drv, unsigned * const on);

/**************************************************************************//**
 * @brief Programs the observation window into the ltm_meas unit.
 * @param[in]   drv     initialized LTM_MEAS driver structure describing the ltm_meas unit instance
 * @param[in]   win     the measurement window geometry
 * @return      0 on success, error-code otherwise
 *
 * This function is used to configure the window geometry on which
 * the rpp_ltm_meas unit shall operate. All pixels inside the
 * observation window are considered for histogram calculation.
 *
 * @note The observation window must not extend beyond the input
 * image geometry or the behaviour of the unit will be undefined.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_ltm_meas_set_meas_window(rpp_ltm_meas_drv_t * drv, rpp_window_t const * const win);

/**************************************************************************//**
 * @brief Retrieves the observation window from the ltm_meas unit.
 * @param[in]   drv     initialized LTM_MEAS driver structure describing the ltm_meas unit instance
 * @param[out]  win     the measurement window geometry
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int rpp_ltm_meas_meas_window(rpp_ltm_meas_drv_t * drv, rpp_window_t * const win);

/**************************************************************************//**
 * @brief Programs the color channel weights for calculating the pixel luminance from RGB data.
 * @param[in]   drv     initialized LTM_MEAS driver structure describing the ltm_meas unit instance
 * @param[in]   red     weight for the red color channel (FP0.10 fixed point number)
 * @param[in]   green   weight for the green color channel (FP0.10 fixed point number)
 * @param[in]   blue    weight for the blue color channel (FP0.10 fixed point number)
 * @return      0 on success, error-code otherwise
 *
 * This function is used to configure the weights of the different color channels
 * for calculating the luminance of pixels. The formula used is:
 * @f$Y = w_{red} \cdot R + w_{green} \cdot G + w_{blue} \cdot B@f$.
 *
 * The weights must be passed as fixed point integers with only a 10 bit fractional
 * part, e.g. a weight value of 0.5 is represented by @f$0.5 * 1024 = 512@f$. It is
 * required that all the weights sum up to a total of 1023.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_ltm_meas_set_weights(rpp_ltm_meas_drv_t * drv, uint16_t red,
        uint16_t green, uint16_t blue);

/**************************************************************************//**
 * @brief Retrieves the color channel weights for calculating the pixel luminance from RGB data.
 * @param[in]   drv     initialized LTM_MEAS driver structure describing the ltm_meas unit instance
 * @param[out]  red     weight for the red color channel (FP0.10 fixed point number)
 * @param[out]  green   weight for the green color channel (FP0.10 fixed point number)
 * @param[out]  blue    weight for the blue color channel (FP0.10 fixed point number)
 * @return      0 on success, error-code otherwise
 *
 * This function is used to retrive the weights of the different color channels
 * from the unit that are used for calculating the luminance of pixels.
 *****************************************************************************/
int rpp_ltm_meas_weights(rpp_ltm_meas_drv_t * drv, uint16_t * const red,
        uint16_t * const green, uint16_t * const blue);

/**************************************************************************//**
 * @brief Programs the luminance percentile thresholds to calculate into the unit.
 * @param[in]   drv     initialized LTM_MEAS driver structure describing the ltm_meas unit instance
 * @param[in]   num     number of percentile values to program (0..RPP_LTM_MEAS_NUM_PRC)
 * @param[in]   values  array of up to RPP_LTM_MEAS_NUM_PRC values
 * @return      0 on success, error-code otherwise
 *
 * This function is used to configure the percentiles of the luminance
 * that shall be calculated. The values configured here are not actually
 * percents, but the thresholds (e.g. number of pixels from the observation
 * window) that correspond to the wanted percentage.
 * The array passed as argument to this function must contain at most
 * RPP_LTM_MEAS_NUM_PRC and at least \c num values.
 * 
 * @note The percentiles are programmed into the corresponding
 * unit registers only for values that have actually been passed to this
 * function. Unused percentile threshold registers are left as they are.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_ltm_meas_set_prc_thresholds(rpp_ltm_meas_drv_t * drv, unsigned num,
        uint32_t const * const values);

/**************************************************************************//**
 * @brief Retrieves the luminance percentile thresholds to calculate from the unit.
 * @param[in]   drv     initialized LTM_MEAS driver structure describing the ltm_meas unit instance
 * @param[in]   num     number of percentile values to retrieve (0..RPP_LTM_MEAS_NUM_PRC)
 * @param[out]  values  array of up to RPP_LTM_MEAS_NUM_PRC values
 * @return      0 on success, error-code otherwise
 *
 * This function retrieves the first @c num values of the configured percentile
 * thresholds from the unit and returns the values in the supplied array.
 *****************************************************************************/
int rpp_ltm_meas_prc_thresholds(rpp_ltm_meas_drv_t * drv, unsigned num,
        uint32_t * const values);

/**************************************************************************//**
 * @brief Retrieves the calculated luminance percentiles from the unit.
 * @param[in]   drv     initialized LTM_MEAS driver structure describing the ltm_meas unit instance
 * @param[in]   num     number of percentile values to retrieve (0..RPP_LTM_MEAS_NUM_PRC)
 * @param[out]  values  array of up to RPP_LTM_MEAS_NUM_PRC values
 * @return      0 on success, error-code otherwise
 *
 * This function retrieves the first @c num values of the calculated
 * percentiles from the unit and returns the values in the supplied array.
 *****************************************************************************/
int rpp_ltm_meas_get_prc(rpp_ltm_meas_drv_t * drv, unsigned num,
        uint32_t * const values);

/**************************************************************************//**
 * @brief Retrieves the calculated image statistics from the unit
 * @param[in]   drv     initialized LTM_MEAS driver structure describing the ltm_meas unit instance
 * @param[out]  lmin    calculated minimum luminance value
 * @param[out]  lmax    calculated maximum luminance value
 * @param[out]  lgmean  measure for the geometric mean of the luminance values
 * @return      0 on success, error-code otherwise
 *
 * This function returns the following statistical parameters:
 *  - lmin = minimum luminance value detected
 *  - lmax = maximum luminance value detected
 *  - lgmean = measure of geometric mean of the luminance values
 *
 * The geometric mean is calculated according to the following
 * formula:
 * @f[ L' = min(1, L) @f]
 * @f[ lg_{mean} = \root {N} \of {\prod_{n=1}^N L_{n}'} @f]
 * where \c N is the number of pixels in the observation window.
 *
 * The unit calculates only a measure for the geometric mean,
 * which is the non-normalized and pre-scaled log2 of the
 * actual geometric mean as shown in the following formula:
 * @f[
 * LGMEAN = \frac{1}{32.0} \cdot \sum_{n=1}^N log2(L_{n}') = \frac{N}{32} * log2(lg_{mean})
 * @f]
 * so that lg<sub>mean</sub> must be calculated as
 * @f[
 * lg_{mean} = pow(2.0, LGMEAN \cdot 32.0 / N)
 * @f]
 * The logarithmic version of the geometric mean calculation
 * is required because otherwise the product of N integer color
 * values would consume an enormous amount of bits for the result.
 * It is the responsibility of the caller to complete the calculation
 * of lg<sub>mean</sub> using the formula stated above.
 *****************************************************************************/
int rpp_ltm_meas_get_stats(rpp_ltm_meas_drv_t * drv, uint32_t * const lmin,
        uint32_t * const lmax, uint32_t * const lgmean);


/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_LTM_MEAS_DRV_H__ */

