/******************************************************************************
 *
 * Copyright 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_main_post_drv.h
 *
 * @brief   Interface of RPP Post-Fusion processing pipeline unit driver.
 *
 *****************************************************************************/
#ifndef __RPP_MAIN_POST_DRV_H__
#define __RPP_MAIN_POST_DRV_H__

#include "rpp_config.h"
#include <rpp_hdr_types.h>

#include "rpp_awb_gain_drv.h"
#include "rpp_filt_drv.h"
#include "rpp_ccor_drv.h"
#include "rpp_cac_drv.h"
#include "rpp_ccor_drv.h"
#include "rpp_hist_drv.h"
#include "rpp_ltm_drv.h"
#include "rpp_ltm_meas_drv.h"
#include "rpp_awb_meas_drv.h"
#include "rpp_rgbdenoise_drv.h"
#include "rpp_shrpcnr_drv.h"

#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppMainPostDrv RPP - Post-Fusion Processing Pipeline Unit Driver
 * @brief RPP - Post-Fusion Processing Pipeline Unit Driver
 *
 * This section contains definitions and functions for the post-fusion
 * processing pipeline stage of the RPP HDR.
 *
 * The of the post processing pipeline stage consists of a number of of
 * sub-units. The driver-specific structures for this pipeline stage,
 * therefore, summarizes the driver-specific data structures for the
 * included sub-units.
 *
 * There is a common initialization routine that initializes all sub-units
 * as required.
 * @{
 *****************************************************************************/


/**************************************************************************//**
 * @brief RPP Post-Fusion Processing Pipeline specific driver structure
 *
 * This structure encapsulates all data required by the sub-units of the
 * RPP RPP_MAIN_POST post-processing pipeline stage.
 *****************************************************************************/
typedef struct {
    /** @brief driver-specific portion of the included AWB_GAIN sub-unit */
    rpp_awb_gain_drv_t      awb_gain;
    /** @brief driver-specific portion of the included FILT sub-unit */
    rpp_filt_drv_t          filt;
    /** @brief driver-specific portion of the included CAC sub-unit */
    rpp_cac_drv_t           cac;
    /** @brief driver-specific portion of the included CCOR sub-unit */
    rpp_ccor_drv_t          ccor;
    /** @brief driver-specific portion of the included HIST sub-unit */
    rpp_hist_drv_t          hist;
    /** @brief driver-specific portion of the included LTM sub-unit */
    rpp_ltm_drv_t           ltm;
    /** @brief driver-specific portion of the included LTM_MEAS sub-unit */
    rpp_ltm_meas_drv_t      ltm_meas;
    /** @brief driver-specific portion of the included AWB_MEAS sub-unit */
    rpp_awb_meas_drv_t      awb_meas;
    /** @brief driver-specific portion of the included RGBDENOISE sub-unit */
    rpp_rgbdenoise_drv_t    rgbdenoise;
    /** @brief driver-specific portion of the included SHRPCNR sub-unit */
    rpp_shrpcnr_drv_t       shrpcnr;
} rpp_main_post_drv_t;


/**************************************************************************//**
 * @brief Initialize the rpp_main_post pipeline stage.
 * @param[in]       dev     rpp_device structure to be used for all register accesses
 * @param[in]       base    base address of RPP MAIN_POST module
 * @param[inout]    drv     RPP_MAIN_POST driver structure to be initialized
 * @return          0 on success, error-code otherwise
 *
 * This function initializes the @c drv structure for future uses of the drivers
 * for all sub-units included in the RPP_MAIN_POST pipeline stage. If any
 * initialization error occurs, the function returns an error code.
 *****************************************************************************/
int rpp_main_post_init(rpp_device * dev, uint32_t base, rpp_main_post_drv_t * drv);

/**************************************************************************//**
 * @brief perform basic set-up of rpp_main_post pipeline stage.
 * @param[in]   drv     initialized RPP_MAIN_POST driver structure describing the rpp_main_post unit instance
 * @param[in]   ccor    CCOR profile to be used for color improvement
 * @param[in]   rgb2yuv CCOR profile to be used for RGBDENOISE RGB-to-YUV conversion
 * @param[in]   yuv2rgb CCOR profile to be used for SHRPCNR YUV-to-RGB conversion
 * @param[in]   width   processed image width
 * @return      0 on success, error-code otherwise
 *
 * This function sets up some basic configuration that is always required
 * to be performed when the MAIN_POST pipeline stage is operated.
 *
 * The @c ccor parameter specifies a profile to be used for color improvements
 * in RGB color space. It can be used to improve brightness, saturation,
 * or contrast of the images. It always needs to be programmed but may
 * be set to an identity mapping if no color improvement is needed.
 *
 * The RGBDENOISE and SHRPCNR units perform their calculations in the YUV
 * colorspace but receive and submit RGB data. The parameters for the
 * conversion between these color spaces must always be programmed, even
 * when RGBDENOISE and/or SHRPCNR functions are disabled.
 *
 * Furthermore, some sub-units make use of buffers that store the image data
 * of several consecutive lines of the image for vertical image processing.
 * These line buffers need to be programmed with the width of a line.
 *
 * It is assumed that these parameters are of a rather static nature and
 * do not change from frame to frame. However, this function can be called
 * again if an adjustment of the parameters is needed.
 *
 * @note All the parameters are subject to register-shadowing and require
 * that the top-level RPP unit is requested to update the shadow registers
 * before the changes become effective.
 *****************************************************************************/
int rpp_main_post_base_setup(
        rpp_main_post_drv_t *               drv,
        rpp_ccor_profile_t const * const    ccor,
        rpp_ccor_profile_t const * const    rgb2yuv,
        rpp_ccor_profile_t const * const    yuv2rgb,
        uint32_t                            width
        );


/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_MAIN_POST_DRV_H__ */

