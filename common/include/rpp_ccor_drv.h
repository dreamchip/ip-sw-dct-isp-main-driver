/******************************************************************************
 *
 * Copyright 2019 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_ccor_drv.h
 *
 * @brief   Interface of color correction driver
 *
 *****************************************************************************/
#ifndef __RPP_CCOR_DRV_H__
#define __RPP_CCOR_DRV_H__

#include "rpp_config.h"
#include <rpp_hdr_types.h>

#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppCcorDrv RPP - Color Correction Driver
 * RPP - Color Correction Driver
 * @{
 *
 *****************************************************************************/

/**************************************************************************//**
 * @brief This definition defines the number of coefficients of the color
 * correction matrix. 
 *****************************************************************************/
#define RPP_CCOR_COEFF_NO   ( 3u * 3u )

/**************************************************************************//**
 * @brief This definition defines the number of offsets of the color
 * correction vector.
 *****************************************************************************/
#define RPP_CCOR_OFF_NO     ( 3u )

/**************************************************************************//**
 * @brief The type declares a color correction configuration (Color Correction
 * Profile) containing a 3x3 matrix and 1x3 offset vector.
 *
 * The color correction always uses 3-component pixels (e.g. RGB, YUV, ...) and
 * can be used for color transformations in the same color space or even for
 * color space conversions.
 *
 * It exists in two variants, which are described by the following formulas:
 * -# Variant with post-offset application:
 *   \f[
 *      \begin{bmatrix} OUT_1 \\ OUT_2 \\ OUT_3 \end{bmatrix}
 *      =
 *      \begin{bmatrix}
 *          Coeff_0 & Coeff_1 & Coeff_2 \\
 *          Coeff_3 & Coeff_4 & Coeff_5 \\
 *          Coeff_6 & Coeff_7 & Coeff_8
 *      \end{bmatrix}
 *      *
 *      \begin{bmatrix} IN_1 \\ IN_2 \\ IN_3 \end{bmatrix}
 *      +
 *      \begin{bmatrix} Off_1 \\ Off_2 \\ Off_3 \end{bmatrix}
 *   \f]
 * -# Variant with pre-offset application:
 *   \f[
 *      \begin{bmatrix} OUT_1 \\ OUT_2 \\ OUT_3 \end{bmatrix}
 *      =
 *      \begin{bmatrix}
 *          Coeff_0 & Coeff_1 & Coeff_2 \\
 *          Coeff_3 & Coeff_4 & Coeff_5 \\
 *          Coeff_6 & Coeff_7 & Coeff_8
 *      \end{bmatrix}
 *      *
 *      \begin{bmatrix} (IN_1 + Off_1) \\ (IN_2 + Off_2) \\ (IN_3 + Off_3) \end{bmatrix}
 *   \f]
 *
 * Whether the offsets are applied before or after the matrix multiplication
 * depends on the hardware configuration of the CCOR unit instance and is not
 * configurable. This property can, however, be queried from the unit.
 *
 * The matrix coefficients are fix-point numbers of the format Q4.12 (=16 Bit).
 * It ranges from -8.0 (0x8000) to 7.99976 (0x7fff).
 *
 * @code
 * // pseudo c code when using floats
 * float coeff   = 1.772f;
 * float v       = coeff * 4096.0f;
 * int16_t value = (int16_t)roundf( v );    // value = 7258
 * ...
 * SET_CCOR_COEFF(&ccm, 7, v);
 * ...
 * @endcode
 *
 * While the matrix coefficients are simply factors that don't depend on the
 * bit width of color samples, the offsets do. Therefore they must be prepared
 * according to the supported bit width of the unit.
 *****************************************************************************/
typedef struct
{
    int32_t     coeff[RPP_CCOR_COEFF_NO];    /**< 3x3 color conversion matrix */
    int32_t     off[RPP_CCOR_OFF_NO];        /**< 1x3 color conversion offset vector */
} rpp_ccor_profile_t;

/**************************************************************************//**
 * @def SET_CCOR_COEFF(f,i,v)
 * Macro to set a color correction coefficient <i>i</i>
 * in a color correction profile (@ref rpp_ccor_profile_t).
 * @def SET_CCOR_OFF(f,i,v)
 * Macro to set a color correction offset <i>i</i>
 * in a color correction profile (@ref rpp_ccor_profile_t).
 *****************************************************************************/
#define SET_CCOR_COEFF(f,i,v)   ( (f)->coeff[i] = (v) )
#define SET_CCOR_OFF(f,i,v)     ( (f)->off[i]   = (v) )

/**************************************************************************//**
 * @def GET_CCOR_COEFF(f,i)
 * Macro to get a color correction coefficient <i>i</i>
 * from a color correction profile (@ref rpp_ccor_profile_t).
 * @def GET_CCOR_OFF(f,i)
 * Macro to get a color correction offset <i>i</i>
 * from a color correction profile (@ref rpp_ccor_profile_t).
 *****************************************************************************/
#define GET_CCOR_COEFF(f,i)     ( (f)->coeff[i] )
#define GET_CCOR_OFF(f,i)       ( (f)->off[i]   )

#include "rpp_csm_def.h"

/**************************************************************************//**
 * @def SET_CCOR_BYPASS(f)
 * This Macro defines color correction coefficients and offsets for a bypass. 
 * Following formula represents this bypass.
 *
 * \f[
 *      \begin{bmatrix} OUT_{1} \\ OUT_{2} \\ OUT_{3} \end{bmatrix}
 *      =
 *      \begin{bmatrix}
 *          1.0 & 0.0 & 0.0 \\
 *          0.0 & 1.0 & 0.0 \\
 *          0.0 & 0.0 & 1.0
 *      \end{bmatrix}
 *      *
 *      \begin{bmatrix} IN_{1}  \\ IN_{2}  \\ IN_{3}  \end{bmatrix}
 *      +
 *      \begin{bmatrix} 0.0 \\ 0.0 \\ 0.0 \end{bmatrix}
 * \f]
 *****************************************************************************/
#define SET_CCOR_BYPASS(f)          \
{                                   \
    SET_CSM_BYPASS((f)->coeff);     \
    SET_OFF_BYPASS((f)->off);       \
}

/**************************************************************************//**
 * @def SET_CCOR_BT601_RGB_2_YUV(f, colorbits)
 * This Macro defines conversion coefficients for a BT.601 RGB
 * to YUV conversion. Following formula shows this conversion.
 *
 * \f[
 *      \begin{bmatrix} Y_{OUT} \\ Cb_{OUT} \\ Cr_{OUT} \end{bmatrix}
 *      =
 *      \begin{bmatrix}
 *           0.2990 &  0.5870 &  0.1140 \\
 *          -0.1687 & -0.3313 &  0.5000 \\
 *           0.5000 & -0.4187 & -0.0813
 *      \end{bmatrix}
 *      *
 *      \begin{bmatrix} R_{IN}  \\ G_{IN}  \\ B_{IN}  \end{bmatrix}
 *      +
 *      \begin{bmatrix} 0.0 \\ 0.5 \\ 0.5 \end{bmatrix}
 * \f]
 * The floating point offsets will be extended to signed fixed point values
 * with (colorbits+1) width as required by the unit.
 *****************************************************************************/
#define SET_CCOR_BT601_RGB_2_YUV(f, colorbits)  \
{                                               \
    SET_CSM_BT601_RGB_2_YUV((f)->coeff);        \
    SET_OFF_RGB_2_YUV((f)->off, colorbits);     \
}

/**************************************************************************//**
 * @def SET_CCOR_BT601_RGB_2_YVU(f, colorbits)
 * This Macro defines conversion coefficients for a BT.601 RGB
 * to YVU conversion. Following formula shows this conversion.
 *
 * \f[
 *      \begin{bmatrix} Y_{OUT} \\ Cr_{OUT} \\ Cb_{OUT} \end{bmatrix}
 *      =
 *      \begin{bmatrix}
 *           0.2990 &  0.5870 &  0.1140 \\
 *           0.5000 & -0.4187 & -0.0813 \\
 *          -0.1687 & -0.3313 &  0.5000
 *      \end{bmatrix}
 *      *
 *      \begin{bmatrix} R_{IN}  \\ G_{IN}  \\ B_{IN}  \end{bmatrix}
 *      +
 *      \begin{bmatrix} 0.0 \\ 0.5 \\ 0.5 \end{bmatrix}
 * \f]
 * The floating point offsets will be extended to signed fixed point values
 * with (colorbits+1) width as required by the unit.
 *****************************************************************************/
#define SET_CCOR_BT601_RGB_2_YVU(f, colorbits)  \
{                                               \
    SET_CSM_BT601_RGB_2_YVU((f)->coeff);        \
    SET_OFF_RGB_2_YUV((f)->off, colorbits);     \
}

/**************************************************************************//**
 * @def SET_CCOR_BT709_RGB_2_YUV(f, colorbits)
 * This Macro defines conversion coefficients for a BT.709 RGB
 * to YUV conversion. Following formula shows this conversion.
 *
 * \f[
 *      \begin{bmatrix} Y_{OUT} \\ Cb_{OUT} \\ Cr_{OUT} \end{bmatrix}
 *      =
 *      \begin{bmatrix}
 *           0.2126 &  0.7152 &  0.0722 \\
 *          -0.1146 & -0.3854 &  0.5000 \\
 *           0.5000 & -0.4552 & -0.0458
 *      \end{bmatrix}
 *      *
 *      \begin{bmatrix} R_{IN}  \\ G_{IN}  \\ B_{IN}  \end{bmatrix}
 *      +
 *      \begin{bmatrix} 0.0 \\ 0.5 \\ 0.5 \end{bmatrix}
 * \f]
 * The floating point offsets will be extended to signed fixed point values
 * with (colorbits+1) width as required by the unit.
 *****************************************************************************/
#define SET_CCOR_BT709_RGB_2_YUV(f, colorbits)  \
{                                               \
    SET_CSM_BT709_RGB_2_YVU((f)->coeff);        \
    SET_OFF_RGB_2_YUV((f)->off, colorbits);     \
}


/**************************************************************************//**
 * @def SET_CCOR_BT709_RGB_2_YVU(f, colorbits)
 * This Macro defines conversion coefficients for a BT.709 RGB
 * to YUV conversion. Following formula shows this conversion.
 *
 * \f[
 *      \begin{bmatrix} Y_{OUT} \\ Cr_{OUT} \\ Cb_{OUT} \end{bmatrix}
 *      =
 *      \begin{bmatrix}
 *           0.2126 &  0.7152 &  0.0722 \\
 *           0.5000 & -0.4552 & -0.0458 \\
 *          -0.1146 & -0.3854 &  0.5000
 *      \end{bmatrix}
 *      *
 *      \begin{bmatrix} R_{IN}  \\ G_{IN}  \\ B_{IN}  \end{bmatrix}
 *      +
 *      \begin{bmatrix} 0.0 \\ 0.5 \\ 0.5 \end{bmatrix}
 * \f]
 * The floating point offsets will be extended to signed fixed point values
 * with (colorbits+1) width as required by the unit.
 *****************************************************************************/
#define SET_CCOR_BT709_RGB_2_YVU(f, colorbits)  \
{                                               \
    SET_CSM_BT709_RGB_2_YVU((f)->coeff);        \
    SET_OFF_RGB_2_YUV((f)->off, colorbits);     \
}


/**************************************************************************//**
 * @def SET_CCOR_YUV_2_BT601_RGB(f, colorbits)
 * @brief initialize the CCOR profile for a YUV to BT.601 RGB conversion
 *
 * The conversion is illustrated by the following formula:
 * @f[
 *      \left(\begin{array}{c} R_{OUT} \\ G_{OUT} \\ B_{OUT} \end{array}\right)
 *      =
 *      \left[\begin{array}{ccc}
 *          1.0000 &  0.0000 &  1.4020 \\
 *          1.0000 & -0.3441 & -0.7141 \\
 *          1.0000 &  1.7720 &  0.0000
 *      \end{array}\right]
 *      *
 *      \left(\begin{array}{c} Y_{IN} - 0.0 \\ U_{IN} - 0.5 \\ V_{IN} - 0.5 \end{array}\right)
 *  @f]
 *
 * The floating point offsets will be extended to signed fixed point values
 * with (colorbits+1) width as required by the unit.
 *
 * @note This profile can be used on CCOR instances that apply the offset before
 * matrix multiplication, not afterwards.
 *****************************************************************************/
#define SET_CCOR_YUV_2_BT601_RGB(f, colorbits)  \
{                                               \
    SET_CSM_YUV_2_BT601_RGB((f)->coeff);        \
    SET_OFF_YUV_2_RGB((f)->off, colorbits);     \
}

/**************************************************************************//**
 * @def SET_CCOR_YVU_2_BT601_RGB(f, colorbits)
 * @brief initialize the CCOR profile for a YVU to BT.601 RGB conversion
 *
 * The conversion is illustrated by the following formula:
 * @f[
 *      \left(\begin{array}{c} R_{OUT} \\ G_{OUT} \\ B_{OUT} \end{array}\right)
 *      =
 *      \left[\begin{array}{ccc}
 *          1.0000 &  1.4020 &  0.0000  \\
 *          1.0000 & -0.7141 & -0.3441  \\
 *          1.0000 &  0.0000 &  1.7720
 *      \end{array}\right]
 *      *
 *      \left(\begin{array}{c} Y_{IN} - 0.0 \\ V_{IN} - 0.5 \\ U_{IN} - 0.5 \end{array}\right)
 *  @f]
 *
 * The floating point offsets will be extended to signed fixed point values
 * with (colorbits+1) width as required by the unit.
 *
 * @note This profile can be used on CCOR instances that apply the offset before
 * matrix multiplication, not afterwards.
 *****************************************************************************/
#define SET_CCOR_YVU_2_BT601_RGB(f, colorbits)  \
{                                               \
    SET_CSM_YVU_2_BT601_RGB((f)->coeff);        \
    SET_OFF_YUV_2_RGB((f)->off, colorbits);     \
}


/**************************************************************************//**
 * @def SET_CCOR_YUV_2_BT709_RGB(f, colorbits)
 * @brief initialize the CCOR profile for a YUV to BT.709 RGB conversion
 *
 * The conversion is illustrated by the following formula:
 * @f[
 *      \left(\begin{array}{c} R_{OUT} \\ G_{OUT} \\ B_{OUT} \end{array}\right)
 *      =
 *      \left[\begin{array}{ccc}
 *          1.0000 &  0.0000 &  1.5748 \\
 *          1.0000 & -0.1873 & -0.4681 \\
 *          1.0000 & -1.8556 &  0.0000
 *      \end{array}\right]
 *      *
 *      \left(\begin{array}{c} Y_{IN} - 0.0 \\ U_{IN} - 0.5 \\ V_{IN} - 0.5 \end{array}\right)
 *  @f]
 *
 * The floating point offsets will be extended to signed fixed point values
 * with (colorbits+1) width as required by the unit.
 *
 * @note This profile can be used on CCOR instances that apply the offset before
 * matrix multiplication, not afterwards.
 *****************************************************************************/
#define SET_CCOR_YUV_2_BT709_RGB(f, colorbits)  \
{                                               \
    SET_CSM_YUV_2_BT709_RGB((f)->coeff);        \
    SET_OFF_YUV_2_RGB((f)->off, colorbits);     \
}

/**************************************************************************//**
 * @def SET_CCOR_YVU_2_BT709_RGB(f, colorbits)
 * @brief initialize the CCOR profile for a YVU to BT.709 RGB conversion
 *
 * The conversion is illustrated by the following formula:
 * @f[
 *      \left(\begin{array}{c} R_{OUT} \\ G_{OUT} \\ B_{OUT} \end{array}\right)
 *      =
 *      \left[\begin{array}{ccc}
 *          1.0000 &  1.5748 &  0.0000 \\
 *          1.0000 & -0.4681 & -0.1873 \\
 *          1.0000 &  0.0000 & -1.8556
 *      \end{array}\right]
 *      *
 *      \left(\begin{array}{c} Y_{IN} - 0.0 \\ V_{IN} - 0.5 \\ U_{IN} - 0.5 \end{array}\right)
 *  @f]
 *
 * The floating point offsets will be extended to signed fixed point values
 * with (colorbits+1) width as required by the unit.
 *
 * @note This profile can be used on CCOR instances that apply the offset before
 * matrix multiplication, not afterwards.
 *****************************************************************************/
#define SET_CCOR_YVU_2_BT709_RGB(f, colorbits)  \
{                                               \
    SET_CSM_YVU_2_BT709_RGB((f)->coeff);        \
    SET_OFF_YUV_2_RGB((f)->off, colorbits);     \
}


/**************************************************************************//**
 * @def SET_CCOR_RGB2XYZ(f)
 * This Macro defines color correction coefficients and offsets for a conversion
 * from RGB to CIE XYZ colorspace.
 * Following formula represents this conversion.
 *
 * \f[
 *      \begin{bmatrix} OUT_{X} \\ OUT_{Y} \\ OUT_{Z} \end{bmatrix}
 *      =
 *      \begin{bmatrix}
 *          0.4123 & 0.3574 & 0.1804 \\
 *          0.2124 & 0.7151 & 0.0720 \\
 *          0.0596 & 0.1191 & 0.9504
 *      \end{bmatrix}
 *      *
 *      \begin{bmatrix} IN_{R}  \\ IN_{G}  \\ IN_{B}  \end{bmatrix}
 *      +
 *      \begin{bmatrix} 0.0 \\ 0.0 \\ 0.0 \end{bmatrix}
 * \f]
 *****************************************************************************/
#define SET_CCOR_RGB2XYZ(f)         \
{                                   \
    SET_CSM_RGB_2_XYZ((f)->coeff);  \
    SET_OFF_BYPASS((f)->off);       \
}





/**************************************************************************//**
 * @brief enumerated values describing the type of color clipping after transformation
 *
 * This enumerated value lists possible choices for specifying the clipping
 * to perform on transformed pixels, provided that the CCOR unit under
 * consideration supports the setting.
 */
typedef enum rpp_ccor_range_e
{
    /*! \brief clip color samples to the full color value range (full swing)
     *
     * This setting specifies that transformed color samples will be clipped
     * to the full color range, that is 0..2^(colorbits) - 1.
     */
    RPP_CCOR_RANGE_FULL  = 0,
    /*! \brief clip color samples to YUV legal range (studio swing)
     *
     * This setting specifies that the transformed color samples will be clipped
     * to a narrower range, also known as "legal range", "studio swing" or
     * "video levels". This setting is mainly useful for RGB-to-YUV conversions
     * where the YUV color samples may need to be restricted to Y=16..235 and
     * C=16..240 for 8bit color samples. When clipping to legal range, the unit
     * will shift the min/max limits according to the bit width of color samples.
     */
    RPP_CCOR_RANGE_LEGAL = 1,

    RPP_CCOR_RANGE_MAX  = RPP_CCOR_RANGE_LEGAL,
} rpp_ccor_range_t;

/**************************************************************************//**
 * @brief RPP CCOR specific driver structure
 *
 * This structure encapsulates all data required by the RPP CCOR driver to
 * operate a specific instance of the RPP CCOR on a given RPPHDR device.
 * The data fields contained in this structure are intended to be
 * private to the driver and not to be modified by any higher-level
 * software component.
 *
 * It is required to have a separate instance of this structure for each
 * separate RPP CCOR module in a given RPPHDR device. Each of these structures
 * must be initialized before use by calling the function @c rpp_ccor_init()
 * with suitable parameters.
 *
 * After initialization, the @c colorbits member variable is set to the
 * number of bits per color value. This information is needed for the
 * interpretation of measured or configured black levels.
 *
 * The structure is exported here so that instances can statically be declared
 * by higher-level software without the need for dynamic memory allocation.
 *****************************************************************************/
typedef struct {
    uint8_t         version;
    uint8_t         props;
    uint16_t        colorbits;
    uint32_t        base;
    rpp_device *    dev;
} rpp_ccor_drv_t;



/**************************************************************************//**
 * @brief Initialize the rpp ccor module.
 * @param[in]       dev     rpp_device structure to be used for all register accesses
 * @param[in]       base    base address of RPP-CCOR module
 * @param[inout]    drv     CCOR driver structure to be initialized
 * @return      0 on success, error-code otherwise
 *
 * This function initializes the @c drv structure for future uses of the driver
 * on the given instance of the rpp ccor module. It fills the @c drv structure
 * with data required for operating on the given instance and caches data that
 * describes the features of the ccor module instance.
 *
 * This function needs to be called once for every instance of the rpp ccor
 * module to operate, each time with a different @c drv argument intended for
 * that very instance of the rpp ccor module only.
 *****************************************************************************/
int rpp_ccor_init(rpp_device * dev, uint32_t base, rpp_ccor_drv_t * drv);

/**************************************************************************//**
 * @brief Program a color conversion profile to the unit.
 * @param[in]   drv     initialized CCOR driver structure describing the ccor module instance
 * @param[in]   p       profile to set
 * @return      0 on success, error-code otherwise
 *
 * This function is used to program the color conversion profile to the unit.
 * The behaviour of the unit regarding the color conversion profile depends
 * on the configuration of the unit and is variable with respect to:
 *  - Post- or pre-application of the offset. This is a hardware configuration
 *    and can not be changed programmatically. Whether the unit applies offsets
 *    before or after matrix multiplication can be queried using the
 *    rpp_ccor_has_pre_offset() function.
 *  - Clipping to full or legal range. Full range clipping means that all color
 *    values supported by the bit width of color samples can be generated by
 *    the unit. Clipping to legal range is mainly useful for color space
 *    conversions from RGB to YUV and limits the output range to Y=16..235
 *    and Cb/Cr=16..240 in the 8bit case. Whether clipping to legal range
 *    is supported by the unit can be queried using the rpp_ccor_has_legal_range()
 *    function and can be enabled for Y and/or C separately using the
 *    rpp_ccor_set_range() or rpp_ccor_set_range_and_profile() functions.
 *****************************************************************************/
int rpp_ccor_set_profile(rpp_ccor_drv_t * drv, const rpp_ccor_profile_t * const p);

/**************************************************************************//**
 * @brief Retrieve the color conversion profile from the unit.
 * @param[in]   drv     initialized CCOR driver structure describing the ccor module instance
 * @param[in]   p       current profile
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int rpp_ccor_profile(rpp_ccor_drv_t * drv, rpp_ccor_profile_t * const p);

/**************************************************************************//**
 * @brief Retrieve the hardware's offset application configuration.
 * @param[in]   drv     initialized CCOR driver structure describing the ccor module instance
 * @return      whether offset vector is applied after (0) or before (1) matrix multiplication, or negative error code
 *
 * This function returns whether the hardware is set-up to apply the offset
 * vector to the color sample before or after matrix multiplication.
 *
 * A return value of "1" indicates that the offset vector is applied before
 * matrix multiplication. This is true only for units dedicated to
 * YUV-to-RGB colorspace conversions.
 *
 * The information is retrieved from cached information that is available
 * after driver initialization and does not need any register access.
 *****************************************************************************/
int rpp_ccor_has_pre_offset(const rpp_ccor_drv_t * drv);

/**************************************************************************//**
 * @brief Retrieve the hardware's legal range clipping capability.
 * @param[in]   drv     initialized CCOR driver structure describing the ccor module instance
 * @return      whether the unit supports clipping to legal range (1) or not (0), or negative error code
 *
 * This function returns whether the hardware is set-up to support
 * clipping of color samples to legal range. If this function returns a
 * non-zero value, the functions rpp_ccor_set_range() and 
 * rpp_ccor_set_range_and_profile() can be used to setup clipping to
 * legal range instead of full range.
 *
 *****************************************************************************/
int rpp_ccor_has_legal_range(const rpp_ccor_drv_t * drv);

/**************************************************************************//**
 * @brief Program the unit's legal range clipping setting.
 * @param[in]   drv     initialized CCOR driver structure describing the ccor module instance
 * @param[in]   luma    one of rpp_ccor_range_t values for luminance clipping
 * @param[in]   chroma  one of rpp_ccor_range_t values for chrominance clipping
 * @return      0 on success, error code otherwise
 *
 * This function is used to program the legal range settings for luminance
 * and chrominance separately. Note that RPP_CCOR_RANGE_LEGAL is only allowed
 * to be specified if the hardware configuration actually allows legal range
 * clipping.
 *
 *****************************************************************************/
int rpp_ccor_set_range(
        rpp_ccor_drv_t *    drv,
        rpp_ccor_range_t    luma,
        rpp_ccor_range_t    chroma
        );

/**************************************************************************//**
 * @brief Retrieve the unit's legal range clipping setting.
 * @param[in]   drv     initialized CCOR driver structure describing the ccor module instance
 * @param[in]   luma    holds one of rpp_ccor_range_t values for luminance clipping on return
 * @param[in]   chroma  holds one of rpp_ccor_range_t values for chrominance clipping on return
 * @return      0 on success, error code otherwise
 *
 * This function is used to retrieve the legal range settings for luminance
 * and chrominance separately from the unit.
 *
 *****************************************************************************/
int rpp_ccor_get_range(
        rpp_ccor_drv_t *    drv,
        rpp_ccor_range_t *  luma,
        rpp_ccor_range_t *  chroma
        );


/**************************************************************************//**
 * @brief Program the unit's legal range clipping setting and profile.
 * @param[in]   drv     initialized CCOR driver structure describing the ccor module instance
 * @param[in]   luma    one of rpp_ccor_range_t values for luminance clipping
 * @param[in]   chroma  one of rpp_ccor_range_t values for chrominance clipping
 * @param[in]   p       color correction profile
 * @return      0 on success, error code otherwise
 *
 * This function is used to program the legal range settings for luminance
 * and chrominance as well as a color correction profile with one
 * function call. Note that RPP_CCOR_RANGE_LEGAL is only allowed
 * to be specified if the hardware configuration actually allows legal range
 * clipping.
 *
 * When clipping to legal range is activated for luminance or chrominance,
 * the corresponding matrix coefficients will automatically be re-scaled so
 * that the full color range provided as input is best mapped to the output
 * range and clipping artifacts are reduced.
 *
 *****************************************************************************/
int rpp_ccor_set_range_and_profile(
        rpp_ccor_drv_t *        drv,
        rpp_ccor_range_t        luma,
        rpp_ccor_range_t        chroma,
        const rpp_ccor_profile_t * const p
        );


/**************************************************************************//**
 * @brief Internal function to program a profile to the unit.
 * @param[in]   dev     rpp_device structure to be used for register accesses
 * @param[in]   base    base address of RPP-CCOR module
 * @param[in]   offset  offset of the first CCOR coefficient register
 * @param[in]   colorbits   the number of valid bits per color sample
 * @param[in]   p       current profile
 * @return      0 on success, error-code otherwise
 *
 * This function is available for use by other low-level drivers which include
 * a CCOR sub-unit.
 *****************************************************************************/
int rpp_ccor_write_profile(
        rpp_device *    dev,
        uint32_t        base,
        uint32_t        offset,
        uint32_t        colorbits,
        const rpp_ccor_profile_t * const p
        );

/**************************************************************************//**
 * @brief Internal function to retrieve the profile from the unit.
 * @param[in]   dev     rpp_device structure to be used for register accesses
 * @param[in]   base    base address of RPP-CCOR module
 * @param[in]   offset  offset of the first CCOR coefficient register
 * @param[in]   colorbits   the number of valid bits per color sample
 * @param[out]  p       where to place the profile
 * @return      0 on success, error-code otherwise
 *
 * This function is available for use by other low-level drivers which include
 * a CCOR sub-unit.
 *****************************************************************************/
int rpp_ccor_read_profile(
        rpp_device *    dev,
        uint32_t        base,
        uint32_t        offset,
        uint32_t        colorbits,
        rpp_ccor_profile_t * const p);


/**************************************************************************//**
 * @brief Internal function to check the validity of a CCOR profile.
 * @param[in]   colorbits   the number of valid bits per color sample
 * @param[out]  p           where to place the profile
 * @return      0 on success, error-code otherwise
 *
 * This function is available for use by other low-level drivers which include
 * a CCOR sub-unit.
 *****************************************************************************/
int rpp_ccor_check_profile(uint32_t colorbits, const rpp_ccor_profile_t * const p);


/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_CCOR_DRV_H__ */

