/******************************************************************************
 *
 * Copyright 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_shrpcnr_drv.h
 *
 * @brief   Interface of SHRPCNR unit driver
 *
 *****************************************************************************/
#ifndef __RPP_SHRPCNR_DRV_H__
#define __RPP_SHRPCNR_DRV_H__

#include "rpp_config.h"
#include "rpp_ccor_drv.h"
#include <rpp_hdr_types.h>

#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppShrpcnrDrv RPP - Sharpening, CNR and Desaturation unit driver
 * RPP - Sharpening, Chroma Noise Reduction and Desaturation unit driver
 *
 * The Sharpening part of the unit aims at improving
 * image quality by sharpening the image and reverting the effect of
 * smoothing edges that is introduced by denoising. It works by employing
 * a high-pass filter locally around a luminance sample (which gives a non-negligible
 * response especially across sharp edges, e.g. steep gradients), amplify
 * the filter response with a configurable factor and add the result to the
 * input luminance sample.
 *
 * The sharpening filter uses two generic parameters, the sharpening
 * strength and a sharpening threshold. The sharpening strength determines
 * the amplification factor, where a higher value results in a stronger
 * sharpening effect. The sharpening threshold is used to cut-off filter
 * responses below the given threshold to not amplify small-amplitude noise
 * in the image. Here, larger values result in a smaller sharpening effect.
 * Additionally, the matrix that actually implements the high-pass filter
 * can be specified.
 *
 * The chroma noise reduction part of the unit, that attempts to smoothen
 * noise from the chroma components of the image, can independently be
 * enabled. It operates separately on each chroma component using a
 * 5x5 filter kernel. An edge detection check is applied in each
 * of the horizontal, vertical and both diagonal directions using a
 * Laplacian filter kernel. The direction with the minimum of the
 * absolute filter response is selected for noise reduction. If the
 * absolute filter response value exceeds a configurable threshold,
 * noise reduction is performed as median filtering of the 5 kernel
 * values in the selected direction.
 *
 * The Contrast Adaptive Desaturation (CAD) part of the unit can
 * independently be enabled to remove color noise on high contrast
 * edges. This filter is applied on the input data before any further sharpening,
 * color noise reduction or desaturation.
 *
 * The Desaturation (or Chroma Reduction) part of the unit, which
 * reduces color saturation for dark video content, can independently
 * be enabled. If enabled, desaturation is executed if the luminance Y
 * falls below a configurable threshold and will linearly increase with
 * (threshold - Y). The slope of the linear desaturation function is
 * also configurable.
 *
 * The unit operates in YCbCr colorspace and receives the YCbCr data
 * from the rpp_rgbdenoise unit. After processing by this unit, the image
 * data needs to be converted back to RGB colorspace. For this, a
 * color correction profile of class rpp_module.ccor_profile type
 * must be passed to function set_profile(). The profile shall be
 * one that converts from YUV to RGB. Note that the offset vector
 * specified in the ccor_profile is applied to the YUV pixels
 * @b before matrix multiplication.
 *
 * @note It is important that the correct image width is specified
 * before this unit is being enabled in any kind. This can be done
 * using the function rpp_shrpcnr_set_linewidth().
 *
 * @note All the configuration of the unit is subject to register
 * shadowing and will not have an immediate effect. New configurations
 * programmed to the unit will be activated synchronized with an
 * end-of-frame signal when the shadow register update has been
 * requested in the top-level RPP unit register.
 * The currently active configuration cannot be retrieved from
 * the unit.
 * @{
 *
 *****************************************************************************/

/**************************************************************************//**
 * @brief Number of sharpen high-pass filter coefficients.
 *
 * The high-pass filter used for sharpening is a 3x3 matrix having 9
 * coefficients in total.
 *****************************************************************************/
#define RPP_SHRPCNR_COEFF_NUM (3u * 3u)

/**************************************************************************//**
 * @brief Datatype describing the set of sharpening high-pass filter coefficients.
 *
 * This data type is used to represent the 3x3 coefficients of the
 * high-pass filter matrix. The coefficients are allowed to be from the
 * following set of values: n = [-8, -4, -2, -1, 0, 1, 2, 4, 8], where the
 * real coefficient value is calculated as \c C=n/8, e.g. -1 <= C <= 1.
 * These values are chosen to represent the divisions by simple bit shifts.
 *
 * When preparing the filter matrix, the coefficients shall sum up to zero.
 *****************************************************************************/
typedef int8_t rpp_shrpcnr_coeffs_t[RPP_SHRPCNR_COEFF_NUM];

/**************************************************************************//**
 * @brief Sets default high-pass filter coefficients for omnidirectional filtering.
 * @param c     array of sharpening coefficients
 *
 * This macro prepares a set of high-pass filter coefficients that
 * result in an omnidirectional gradient search. It is defined as:
 * @f[
 *    \left(
 *     \begin{array}{rrr}
 *     -1 & -1 & -1 \\\\ -1 & 8 & -1 \\\\ -1 & -1 & -1
 *     \end{array}
 *    \right)
 * @f]
 *****************************************************************************/
#define RPP_SHRPCNR_HPF0(c) \
{                           \
    c[0] = -1;              \
    c[1] = -1;              \
    c[2] = -1;              \
    c[3] = -1;              \
    c[4] =  8;              \
    c[5] = -1;              \
    c[6] = -1;              \
    c[7] = -1;              \
    c[8] = -1;              \
}

/**************************************************************************//**
 * @brief Sets default high-pass filter coefficients for x/y filtering.
 * @param c     array of sharpening coefficients
 *
 * This macro prepares a set of high-pass filter coefficients that
 * result in an horizonal and vertical gradient search. It is defined as:
 * @f[
 *    \left(
 *     \begin{array}{rrr}
 *      0 & -1 &  0 \\\\ -1 & 4 & -1 \\\\  0 & -1 &  0
 *     \end{array}
 *    \right)
 * @f]
 *****************************************************************************/
#define RPP_SHRPCNR_HPF1(c) \
{                           \
    c[0] =  0;              \
    c[1] = -1;              \
    c[2] =  0;              \
    c[3] = -1;              \
    c[4] =  4;              \
    c[5] = -1;              \
    c[6] =  0;              \
    c[7] = -1;              \
    c[8] =  0;              \
}



/**************************************************************************//**
 * @brief Structure holding the parameter set for the CAD part of the unit.
 *
 * This structure is used to hold the parameters that control the operation
 * of the Contrast Adaptive Desaturation (CAD) part of the unit.
 *
 * The CAD unit provides an (inverse) strength level that causes the effect
 * introduced by CAD to become smaller with increasing level.
 *
 * Furthermore there are a number of thresholds that define the regions, in the
 * U/V color plane, that shall be excluded from CAD processing.
 *
 * There is one set of thresholds, @c umin and @c umax that defines the region
 * around Cb = 0 to be excluded from CAD processing. Two further sets of
 * min/max thresholds define regions around Cr = 0 to be excluded from
 * CAD processing, where one set (@c uneg_vmin, @c uneg_vmax) is considered
 * for pixels that have Cb < 0, and the other set (@c upos_vmin, @c upos_vmax)
 * is considered for pixels that have Cb > 0.
 *
 * In every situation, the min threshold value must be smaller than or equal
 * to the corresponding max threshold.
 *
 * The hardware automatically subtracts the offsets configured in the CCOR
 * unit from the Cb/Cr values to generate signed input for the CAD unit.
 *****************************************************************************/
typedef struct {
    /** @brief CAD restoration level
     *
     * The restoration level from 0 to 7 determines how strong the
     * desaturation caused by the CAD is mitigated. A value of 0 means
     * no restoration and causes maximum desaturation. At level 7 most
     * colors will be fully restored, causing the CAD to have only small
     * impact on the image. The default level 3 should be good compromise
     * for most scenarios. */
    uint8_t     restore_lvl;
    /** @brief minimum Cb value to exclude from CAD processing.
     *
     * This value specifies that only pixels having (Cb - Cb_0)
     * smaller than this value shall be considered for CAD processing.
     */
    int16_t     umin;
    /** @brief maximum Cb value to exclude from CAD processing.
     *
     * This value specifies that only pixels having (Cb - Cb_0)
     * larger than this value shall be considered for CAD processing.
     */
    int16_t     umax;
    /** @brief minimum Cr value at negative Cb to exclude from CAD processing.
     *
     * This value specifies that pixels with (Cb < Cb_0) shall be considered
     * for CAD processing if, at the same time, (Cr - Cr_0) is smaller than
     * this value. */
    int16_t     uneg_vmin;
    /** @brief maximum Cr value at negative Cb to exclude from CAD processing.
     *
     * This value specifies that pixels with (Cb < Cb_0) shall be considered
     * for CAD processing if, at the same time, (Cr - Cr_0) is larger than
     * this value. */
    int16_t     uneg_vmax;
    /** @brief minimum Cr value at positive Cb to exclude from CAD processing.
     *
     * This value specifies that pixels with (Cb >= Cb_0) shall be considered
     * for CAD processing if, at the same time, (Cr - Cr_0) is smaller than
     * this value. */
    int16_t     upos_vmin;
    /** @brief maximum Cr value at positive Cb to exclude from CAD processing.
     *
     * This value specifies that pixels with (Cb >= Cb_0) shall be considered
     * for CAD processing if, at the same time, (Cr - Cr_0) is larger than
     * this value. */
    int16_t     upos_vmax;

} rpp_shrpcnr_cad_params_t;

/**************************************************************************//**
 * @brief RPP SHRPCNR specific driver structure
 *
 * This structure encapsulates all data required by the RPP SHRPCNR driver to
 * operate a specific instance of the RPP SHRPCNR unit on a given RPPHDR device.
 * The data fields contained in this structure are intended to be
 * private to the driver and not to be modified by any higher-level
 * software component.
 *
 * It is required to have a separate instance of this structure for each
 * separate RPP SHRPCNR unit in a given RPPHDR device. Each of these structures
 * must be initialized before use by calling the function @c rpp_shrpcnr_init()
 * with suitable parameters.
 *
 * After initialization, the @c colorbits member variable is set to the
 * number of bits per color value. This information is needed for the
 * preparation of the colorspace transformation CCOR profile as well as
 * knowing the numerical bounds of the various thresholds.
 *
 * The structure is exported here so that instances can statically be declared
 * by higher-level software without the need for dynamic memory allocation.
 *****************************************************************************/
typedef struct {
    uint16_t        version;
    uint16_t        colorbits;
    uint32_t        base;
    rpp_device *    dev;
} rpp_shrpcnr_drv_t;


/**************************************************************************//**
 * @brief Initialize the rpp shrpcnr module.
 * @param[in]       dev     rpp_device structure to be used for all register accesses
 * @param[in]       base    base address of RPP-SHRPCNR module
 * @param[inout]    drv     SHRPCNR driver structure to be initialized
 * @return      0 on success, error-code otherwise
 *
 * This function initializes the @c drv structure for future uses of the driver
 * on the given instance of the rpp shrpcnr unit. It fills the @c drv structure
 * with data required for operating on the given instance and caches data that
 * describes the features of the shrpcnr unit instance.
 *
 * This function needs to be called once for every instance of the rpp shrpcnr
 * unit to operate, each time with a different @c drv argument intended for
 * that very instance of the rpp shrpcnr unit only.
 *****************************************************************************/
int rpp_shrpcnr_init(rpp_device * dev, uint32_t base, rpp_shrpcnr_drv_t * drv);

/**************************************************************************//**
 * @brief Program the image width for the line buffer.
 * @param[in]   drv     initialized SHRPCNR driver structure describing the shrpcnr unit instance
 * @param[in]   width   the width of the image(s) to be processed
 * @return      0 on success, error-code otherwise
 *
 * This function is used to program the image width into the
 * unit. The information is needed for the line buffers that
 * buffer subsequent lines for application of the filter kernels.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_shrpcnr_set_linewidth(rpp_shrpcnr_drv_t * drv, uint16_t width);

/**************************************************************************//**
 * @brief Retrieve the image width for the line buffer from the unit
 * @param[in]   drv     initialized SHRPCNR driver structure describing the shrpcnr unit instance
 * @param[in]   width   the currently programmed width of the image(s)
 * @return      0 on success, error-code otherwise
 *
 * This function retrieves the currently programmed image width from the
 * unit.
 *****************************************************************************/
int rpp_shrpcnr_linewidth(rpp_shrpcnr_drv_t * drv, uint16_t * const width);

/**************************************************************************//**
 * @brief Enable or disable the sharpening part of the shrpcnr unit.
 * @param[in]   drv     initialized SHRPCNR driver structure describing the shrpcnr unit instance
 * @param[in]   on      enable state to set (0: disable, otherwise enable)
 * @return      0 on success, error-code otherwise
 *
 * This function enables or disables the sharpening part of the shrpcnr unit.
 * If enabled, a suitable sharpening filter matrix and sharpening filter
 * parameters must be programmed.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_shrpcnr_shrp_enable(rpp_shrpcnr_drv_t * drv, const unsigned on);

/**************************************************************************//**
 * @brief Return enable status of the sharpening part of the rpp shrpcnr unit.
 * @param[in]   drv     initialized SHRPCNR driver structure describing the shrpcnr unit instance
 * @param[out]  on      current enable state (0: disabled, otherwise enabled)
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int rpp_shrpcnr_shrp_enabled(rpp_shrpcnr_drv_t * drv, unsigned * const on);

/**************************************************************************//**
 * @brief Program sharpening parameters into the rpp shrpcnr module.
 * @param[in]   drv         initialized SHRPCNR driver structure describing the shrpcnr unit instance
 * @param[in]   strength    strength of the sharpening filter (0..255)
 * @param[in]   threshold   sharpening threshold (0..4095)
 * @return      0 on success, error-code otherwise
 *
 * This function is used to configure the generic sharpening parameters.
 * The @c strength indicates how strong the high-pass filter response
 * shall be blended with the input image (higher values result in
 * stronger sharpening). This factor is an FP1.7 fixed point number
 * where 1.0 is represented by a value of 128.
 *
 * The @c threshold parameter provides a bound for the high-pass filter response.
 * This value is used to avoid amplifying noise too much by suppressing sharpening
 * for small gradients. Higher value means less sharpening for smooth edges.
 * Threshold zero means no coring, so all gradients are treated the same.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_shrpcnr_set_shrp_params(rpp_shrpcnr_drv_t * drv, uint8_t strength,
        uint16_t threshold);

/**************************************************************************//**
 * @brief Retrieve the currently programmed sharpening parameters from the unit.
 * @param[in]   drv         initialized SHRPCNR driver structure describing the shrpcnr unit instance
 * @param[out]  strength    strength of the sharpening filter
 * @param[out]  threshold   sharpening threshold
 * @return      0 on success, error-code otherwise
 *
 * This function retrieves the currently programmed sharpening parameters
 * from the unit and returns them.
 *****************************************************************************/
int rpp_shrpcnr_shrp_params
(
    rpp_shrpcnr_drv_t * drv,
    uint8_t * const     strength,
    uint16_t * const    threshold
);

/**************************************************************************//**
 * @brief Program sharpening filter coefficients into the rpp shrpcnr unit.
 * @param[in]   drv     initialized SHRPCNR driver structure describing the shrpcnr unit instance
 * @param[in]   coeffs  the sharpening filter coefficients
 * @return      0 on success, error-code otherwise
 *
 * This function is used to configure a custom high-pass filter
 * matrix from the given set of coefficients for sharpening.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_shrpcnr_set_shrp_filter_coeff(rpp_shrpcnr_drv_t * drv,
        const rpp_shrpcnr_coeffs_t coeffs);

/**************************************************************************//**
 * @brief Retrieve the currently programmed sharpening filter coefficients from the unit.
 * @param[in]   drv     initialized SHRPCNR driver structure describing the shrpcnr unit instance
 * @param[out]  coeffs  the sharpening filter coefficients
 * @return      0 on success, error-code otherwise
 *
 * This function retrieves the currently programmed high-pass filter
 * coefficients from the unit and returns them.
 *****************************************************************************/
int rpp_shrpcnr_shrp_filter_coeff(rpp_shrpcnr_drv_t * drv,
        rpp_shrpcnr_coeffs_t coeffs);


/**************************************************************************//**
 * @brief Enable or disable the CNR part of the shrpcnr unit.
 * @param[in]   drv     initialized SHRPCNR driver structure describing the shrpcnr unit instance
 * @param[in]   on      enable state to set (0: disable, otherwise enable)
 * @return      0 on success, error-code otherwise
 *
 * This function enables or disables the color noise reduction (e.g.
 * color smoothing filter) part of the shrpcnr unit.
 * If enabled, suitable threshold parameters must have been programmed.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_shrpcnr_cnr_enable(rpp_shrpcnr_drv_t * drv, const unsigned on);

/**************************************************************************//**
 * @brief Return enable status of the CNR part of the shrpcnr unit.
 * @param[in]   drv     initialized SHRPCNR driver structure describing the shrpcnr unit instance
 * @param[out]  on      current enable state (0: disabled, otherwise enabled)
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int rpp_shrpcnr_cnr_enabled(rpp_shrpcnr_drv_t * drv, unsigned * const on);

/**************************************************************************//**
 * @brief Programs the chroma noise reduction thresholds into the unit
 * @param[in]   drv         initialized SHRPCNR driver structure describing the shrpcnr unit instance
 * @param[in]   thres_cb    threshold for applying CNR to chroma Cb values
 * @param[in]   thres_cr    threshold for applying CNR to chroma Cr values
 * @return      0 on success, error-code otherwise
 *
 * This function is used to configure the thresholds for the
 * chroma noise reduction function of the unit. Independent
 * thresholds for both, Cb and Cr chroma components, need
 * to be supplied. The thresholds specify minimum values for
 * the Laplace filter response to apply chroma noise reduction
 * for.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_shrpcnr_set_cnr_thresholds(rpp_shrpcnr_drv_t * drv,
        uint16_t thres_cb, uint16_t thres_cr);

/**************************************************************************//**
 * @brief Retrieves the chroma noise reduction thresholds from the unit
 * @param[in]   drv         initialized SHRPCNR driver structure describing the shrpcnr unit instance
 * @param[in]   thres_cb    threshold applied to CNR to chroma Cb values
 * @param[in]   thres_cr    threshold applied to CNR to chroma Cr values
 * @return      0 on success, error-code otherwise
 *
 * This function is used to retrieve the currently configured
 * thresholds for the chroma noise reduction function from the unit.
 *****************************************************************************/
int rpp_shrpcnr_cnr_thresholds(rpp_shrpcnr_drv_t * drv,
        uint16_t * const thres_cb, uint16_t * const thres_cr);

/**************************************************************************//**
 * @brief Enable or disable the CAD part of the shrpcnr unit.
 * @param[in]   drv     initialized SHRPCNR driver structure describing the shrpcnr unit instance
 * @param[in]   on      enable state to set (0: disable, otherwise enable)
 * @return      0 on success, error-code otherwise
 *
 * This function enables or disables the Contrast Adaptive Desaturation (e.g.
 * CAD) part of the shrpcnr unit.
 * If enabled, suitable CAD parameters must have been programmed.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_shrpcnr_cad_enable(rpp_shrpcnr_drv_t * drv, const unsigned on);

/**************************************************************************//**
 * @brief Return enable status of the CAD part of the shrpcnr unit.
 * @param[in]   drv     initialized SHRPCNR driver structure describing the shrpcnr unit instance
 * @param[out]  on      current enable state (0: disabled, otherwise enabled)
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int rpp_shrpcnr_cad_enabled(rpp_shrpcnr_drv_t * drv, unsigned * const on);

/**************************************************************************//**
 * @brief Program contrast adaptive desaturation parameters into the rpp shrpcnr unit.
 * @param[in]   drv     initialized SHRPCNR driver structure describing the shrpcnr unit instance
 * @param[in]   params  the CAD parameter set
 * @return      0 on success, error-code otherwise
 *
 * This function is used to configure the parameters used by the Contrast
 * Adaptive Desaturation (CAD) algorithm into the unit.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_shrpcnr_set_cad_params(rpp_shrpcnr_drv_t * drv,
        const rpp_shrpcnr_cad_params_t * const params);

/**************************************************************************//**
 * @brief Retrieve the currently programmed CAD parameters from the unit.
 * @param[in]   drv     initialized SHRPCNR driver structure describing the shrpcnr unit instance
 * @param[out]  params  the CAD parameter set
 * @return      0 on success, error-code otherwise
 *
 * This function retrieves the currently programmed CAD parameter
 * values from the unit and returns them.
 *****************************************************************************/
int rpp_shrpcnr_cad_params(rpp_shrpcnr_drv_t * drv,
        rpp_shrpcnr_cad_params_t * const params);


/**************************************************************************//**
 * @brief Enable or disable the chroma desaturation part of the shrpcnr unit.
 * @param[in]   drv     initialized SHRPCNR driver structure describing the shrpcnr unit instance
 * @param[in]   on      enable state to set (0: disable, otherwise enable)
 * @return      0 on success, error-code otherwise
 *
 * This function enables or disables the generic chroma desaturation
 * (or chroma reduction) part of the shrpcnr unit.
 * If enabled, suitable desaturation parameters must have been programmed.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_shrpcnr_desat_enable(rpp_shrpcnr_drv_t * drv, const unsigned on);

/**************************************************************************//**
 * @brief Return enable status of the chroma desaturation part of the shrpcnr unit.
 * @param[in]   drv     initialized SHRPCNR driver structure describing the shrpcnr unit instance
 * @param[out]  on      current enable state (0: disabled, otherwise enabled)
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int rpp_shrpcnr_desat_enabled(rpp_shrpcnr_drv_t * drv, unsigned * const on);

/**************************************************************************//**
 * @brief Program chroma desaturation parameters into the rpp shrpcnr module.
 * @param[in]   drv         initialized SHRPCNR driver structure describing the shrpcnr unit instance
 * @param[in]   threshold   luma threshold for applying desaturation
 * @param[in]   slope       slope of the linear desaturation function (0..63)
 * @param[in]   div         additional scale factor for the slope specified as right-shift bit count (0..7)
 * @return      0 on success, error-code otherwise
 *
 * This function is used to configure the parameters for the
 * chroma desaturation function of the unit. The @c threshold
 * gives a limit for applying desaturation. It is only performed
 * for pixels which have a luma value of Y < threshold.
 *
 * The parameters @c slope and @c div determine the steepness of
 * the linear desaturation curve. The chroma scaling factor *F* is
 * determined as
 * @f[
 *     F = 1.0 - \frac{(threshold - Y) \cdot slope}{2^{div + 8}}
 * @f]
 * and is clipped to the range 0.0 to 1.0.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_shrpcnr_set_desat_params(rpp_shrpcnr_drv_t * drv,
        uint16_t threshold, uint8_t slope, uint8_t div);

/**************************************************************************//**
 * @brief Retrieve the currently programmed chroma desaturation parameters from the unit.
 * @param[in]   drv         initialized SHRPCNR driver structure describing the shrpcnr unit instance
 * @param[out]  threshold   luma threshold for applying desaturation
 * @param[out]  slope       slope of the linear desaturation function (0..63)
 * @param[out]  div         additional scale factor for the slope specified as right-shift bit count (0..7)
 * @return      0 on success, error-code otherwise
 *
 * This function retrieves the currently programmed chroma desaturation parameters
 * from the unit and returns them.
 *****************************************************************************/
int rpp_shrpcnr_desat_params
(
    rpp_shrpcnr_drv_t * drv,
    uint16_t * const    threshold,
    uint8_t * const     slope,
    uint8_t * const     div
);



/**************************************************************************//**
 * @brief Program YUV to RGB transformation parameters into the rpp shrpcnr module.
 * @param[in]   drv     initialized SHRPCNR driver structure describing the shrpcnr unit instance
 * @param[in]   profile pointer to an initialized CCOR profile instance
 * @return      0 on success, error-code otherwise
 *
 * This function programs a new set of parameters for performing the
 * YUV to RGB colorspace conversion. The new parameter set is not activated
 * immediately because it is subject to shadow register handling. It may
 * become valid only for the next frame.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_shrpcnr_set_profile
(
    rpp_shrpcnr_drv_t *                 drv,
    const rpp_ccor_profile_t * const    profile
);


/**************************************************************************//**
 * @brief Retrieve programmed YUV to RGB transformation parameters from the rpp shrpcnr module.
 * @param[in]   drv     initialized SHRPCNR driver structure describing the shrpcnr unit instance
 * @param[out]  profile pointer to an CCOR profile instance
 * @return      0 on success, error-code otherwise
 *
 * This function retrieves from the unit the set of parameters for the YUV to
 * RGB transformation.
 *****************************************************************************/
int rpp_shrpcnr_profile
(
    rpp_shrpcnr_drv_t *         drv,
    rpp_ccor_profile_t * const  profile
);

/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_SHRPCNR_DRV_H__ */

