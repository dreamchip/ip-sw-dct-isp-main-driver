/******************************************************************************
 *
 * Copyright 2019 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_rmap_meas_drv.h
 *
 * @brief   Interface of radiance mapping measurement unit driver
 *
 *****************************************************************************/
#ifndef __RPP_RMAP_MEAS_DRV_H__
#define __RPP_RMAP_NEAS_DRV_H__

#include "rpp_config.h"
#include <rpp_hdr_types.h>

#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppRmapMeasDrv RPP - Radiance Mapping Measurement Unit Driver
 * @brief RPP - Radiance Mapping Measurement Unit Driver
 *
 * The radiance mapping measurement unit is used to calculate the mapping factors for
 * the short and the very short exposure: @c map_fac_s and map_fac_vs. The factors
 * define the sensitivity quotient between the long / short and the long / very short
 * exposure.
 *
 * In order to calculate these factors only such values shall be taken into account which
 * are neither over- nor underexposed in both exposures under consideration i.e. for the
 * short exposure factor the samples shall be not over-/underexposed in short and long
 * exposure and for the very short factor the samples shall not be over-/underexposed in
 * very short and short exposure.
 * 
 * For over-/underexposure these thresholds are defined.
 *  - VS_MIN: Defines threshold for underexposure for very short exposure channel
 *  - S_MIN: Defines threshold for underexposure for short exposures channel
 *  - S_MAX: Defines threshold for overexposure for very short exposure channel
 *  - L_MAX: Defines threshold for overexposure for long exposure channel
 * 
 * Accumulation results of all samples s with @n
 * s_veryshort > VS_MIN && S_MAX > s_short > S_MIN @n
 * are stored to "vss_short" result for s_short and to "vss_vshort" result for s_veryshort.
 *
 * Accumulation results of all samples s with @n
 * s_long < L_MAX && S_MAX > s_short > S_MIN @n
 * are stored to "ls_short" result for s_short and to "ls_long" result for s_long.
 * 
 * The accumulation results are 64-bit wide.
 * The mapping factors can be calculated by software to:
 *  - map_fac_s = ls_long / ls_short
 *  - map_fac_vs = map_fac_s * vss_short / vss_vshort
 * 
 * Measurements are performed in a defined observation window that can be configured
 * along with two parameters @c h_step_inc and @c v_stepsize that determine
 * which pixels inside the window are to be measured or skipped, respectively.
 * Please refer to the documentation of the "rpp_hist" driver for a detailed
 * explanation of these parameters.
 *
 * @note All configuration of the unit is subject to register
 * shadowing and will not have an immediate effect. Any configuration
 * programmed to the unit will be activated synchronized with an
 * end-of-frame signal when the shadow register update has been
 * requested in the top-level RPP unit register.
 * The currently active configuration cannot be retrieved from
 * the unit.
 * @{
 *
 *****************************************************************************/

/**************************************************************************//**
 * @brief RPP RMAP_MEAS specific driver structure
 *
 * This structure encapsulates all data required by the RPP RMAP_MEAS driver to
 * operate a specific instance of the RPP RMAP_MEAS on a given RPPHDR device.
 * The data fields contained in this structure are intended to be
 * private to the driver and not to be modified by any higher-level
 * software component.
 *
 * It is required to have a separate instance of this structure for each
 * separate RPP RMAP_MEAS module in a given RPPHDR device. Each of these structures
 * must be initialized before use by calling the function @c rpp_rmap_meas_init()
 * with suitable parameters.
 *
 * After initialization, the @c colorbits_high and @c colorbits_low member
 * variables are set to the number of bits per color value in the
 * long (colorbits_high) or short / very_short (colorbits_low) exposure
 * paths. This information is needed for the configuration of suitable
 * thresholds.
 *
 * The structure is exported here so that instances can statically be declared
 * by higher-level software without the need for dynamic memory allocation.
 *****************************************************************************/
typedef struct {
    uint16_t        version;
    uint8_t         colorbits_high;
    uint8_t         colorbits_low;
    uint32_t        base;
    rpp_device *    dev;
} rpp_rmap_meas_drv_t;


/**************************************************************************//**
 * @brief Datatype defining the measurement result.
 *****************************************************************************/
typedef struct rpp_rmap_sums_s {
    /** @brief short vs. very short sensitivity: very short sums.
     *
     * This value contains the sums of correctly exposed pixels in the
     * very short exposure frame calculated from the short vs. very short
     * frame comparison.
     */
    uint64_t    vss_vshort;
    /** @brief short vs. very short sensitivity: short sums.
     *
     * This value contains the sums of correctly exposed pixels in the
     * short exposure frame calculated from the short vs. very short
     * frame comparison.
     */
    uint64_t    vss_short;
    /** @brief short vs. long sensitivity: short sums.
     *
     * This value contains the sums of correctly exposed pixels in the
     * short exposure frame calculated from the short vs. long
     * frame comparison.
     */
    uint64_t    ls_short;
    /** @brief short vs. long sensitivity: long sums.
     *
     * This value contains the sums of correctly exposed pixels in the
     * long exposure frame calculated from the short vs. long
     * frame comparison.
     */
    uint64_t    ls_long;
} rpp_rmap_sums_t;

/**************************************************************************//**
 * @brief RPP rmap measurement modes
 *****************************************************************************/
enum rpp_rmap_measure_mode_e
{
    RPP_RMAP_MEASURE_MODE_DISABLE   = 0, /**< disable rmap measurement */
    RPP_RMAP_MEASURE_MODE_ENABLE    = 1, /**< common block measurement */
    RPP_RMAP_MEASURE_MODE_MAX,
};

/**************************************************************************//**
 * @brief This type defines the RPP rmap meas sub-sample configuration.
 * The sub-sample configuration consists of 2 step-sizes. One in vertical
 * direction which depends on the selected mode (@see rpp_rmap_measure_mode_e).
 *
 * If mode=RPP_RMAP_MEASURE_MODE_COMMON_BLOCK:
 *  -# v_stepsize=1 => process every input line
 *  -# v_stepsize=2 => precess every second input line
 *  -# v_stepsize=3 => precess every third input line
 *  ...
 *
 * The horizontal step increment is used for horizontal sub-sampling. To
 * process every valid input pixel h_step_inc has to be set to 2^16. This
 * means the value is a fixed point number in format FP1.15.
 *
 * If mode=RPP_RMAP_MEASURE_MODE_COMMON_BLOCK:
 *  h_step_inc=2^16         => process every input pixel
 *  h_step_inc=2^15         => process every second input pixel
 *  h_step_inc=ceil(2^16/3) => process every third input pixel
 *  h_step_inc=2^14         => process every forth input pixel
 *  ...
 *
 * @note Sub-sampling is used to avoid bin overruns in the intensity sums.
 *****************************************************************************/
typedef struct rpp_rmap_step_s
{
    uint32_t v_stepsize;     /**< vertical step increment */
    uint32_t h_step_inc;    /**< horizontal step increment */
} rpp_rmap_step_t;

/**************************************************************************//**
 * @brief Initialize the rpp rmap_meas module.
 * @param[in]       dev     rpp_device structure to be used for all register accesses
 * @param[in]       base    base address of RPP RMAP_MEAS module
 * @param[inout]    drv     RMAP_MEAS driver structure to be initialized
 * @return      0 on success, error code otherwise
 *
 * This function initializes the @c drv structure for future uses of the driver
 * on the given instance of the rpp rmap_meas module. It fills the @c drv structure
 * with data required for operating on the given instance and caches data that
 * describes the features of the rmap_meas module instance.
 *
 * This function needs to be called once for every instance of the rpp rmap_meas
 * module to operate, each time with a different @c drv argument intended for
 * that very instance of the rpp rmap_meas module only.
 *****************************************************************************/
int rpp_rmap_meas_init(rpp_device * dev, uint32_t base, rpp_rmap_meas_drv_t * drv);

/**************************************************************************//**
 * @brief Program operation mode to the rpp rmap_meas module.
 * @param[in]   drv     initialized RMAP_MEAS driver structure describing the rmap_meas module instance
 * @param[in]   mode    operation mode
 * @return      0 on success, error code otherwise
 *
 * Enables or disables radiance mapping measurement operation.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_rmap_meas_enable(rpp_rmap_meas_drv_t * drv, const unsigned mode);

/**************************************************************************//**
 * @brief Retrieve operation mode from the rpp rmap_meas module.
 * @param[in]   drv     initialized RMAP_MEAS driver structure describing the rmap_meas module instance
 * @param[out]  mode    current measurement mode
 * @return      0 on success, error code otherwise
 *****************************************************************************/
int rpp_rmap_meas_enabled(rpp_rmap_meas_drv_t * drv, unsigned * const mode);

/**************************************************************************//**
 * @brief Program the measurement window to the rpp rmap_meas unit.
 * @param[in]   drv     initialized RMAP_MEAS driver structure describing the rmap_meas module instance
 * @param[in]   win     observation window to configure (@see rpp_window_t)
 * @param[in]   steps   sub-sampling configuration to configure (@see rpp_rmap_step_t)
 * @return      0 on success, error code otherwise
 *
 * This function defines the observation window for radiance mapping measurement
 * as well as the horizontal and vertical pixel skipping parameters for
 * the pixel selection logic. For a description of the @c h_step_inc and
 * @c v_stepsize parameters, please refer to the documentation of the
 * rpp_hist driver.
 *
 * @note The pixel skipping is used to prevent measurement bins from
 * overflowing.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_rmap_meas_set_meas_window(rpp_rmap_meas_drv_t * drv,
        const rpp_window_t * win, const rpp_rmap_step_t * steps);

/**************************************************************************//**
 * @brief Retrieves the current  measurement window from the rpp rmap_meas unit.
 * @param[in]   drv     initialized RMAP_MEAS driver structure describing the rmap_meas module instance
 * @param[out]  win     current observation window (@see rpp_window_t)
 * @param[out]  steps   current sub-sampling configuration (@see rpp_rmap_step_t)
 * @return      0 on success, error code otherwise
 *****************************************************************************/
int rpp_rmap_meas_window(rpp_rmap_meas_drv_t * drv,
        rpp_window_t * const win, rpp_rmap_step_t * const steps);

/**************************************************************************//**
 * @brief Programs measurement thresholds to the rpp rmap_meas unit.
 * @param[in]   drv     initialized RMAP_MEAS driver structure describing the rmap_meas module instance
 * @param[out]  vs_min  min intensity value of very short exposure frame
 * @param[out]  s_min   min intensity value of short exposure frame
 * @param[out]  s_max   max intensity value of short exposure frame
 * @param[out]  l_max   max intensity value of long exposure frame
 * @return      0 on success, error code otherwise
 *
 * This function configures the thresholds to be used by the unit for
 * calculating the required statistics. The threshold for long exposure
 * samples must be a color value with a resolution of drv->colorbits_high,
 * all other thresholds must be color values of drv->colorbits_low resolution.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_rmap_meas_set_thresholds(rpp_rmap_meas_drv_t * drv, const uint16_t vs_min,
        const uint16_t s_min, const uint16_t s_max, const uint32_t l_max);

/**************************************************************************//**
 * @brief Retrieves current measurement thresholds from the rpp rmap_meas unit.
 * @param[in]   drv     initialized RMAP_MEAS driver structure describing the rmap_meas module instance
 * @param[out]  vs_min  min intensity value of very short exposure frame
 * @param[out]  s_min   min intensity value of short exposure frame
 * @param[out]  s_max   max intensity value of short exposure frame
 * @param[out]  l_max   max intensity value of long exposure frame
 * @return      0 on success, error code otherwise
 *
 * This function retrieves the currently configured measurement thresholds
 * from the unit.
 *****************************************************************************/
int rpp_rmap_meas_thresholds(rpp_rmap_meas_drv_t * drv, uint16_t * const vs_min,
        uint16_t * const s_min, uint16_t * const s_max, uint32_t * const l_max);

/**************************************************************************//**
 * @brief Retrieves the measured result from the unit.
 * @param[in]   drv     initialized RMAP_MEAS driver structure describing the rmap_meas module instance
 * @param[out]  sums    measurement results
 * @return      0 on success, error code otherwise
 *
 * This function reads the 64-bit counters that represent the measurement results
 * and returns them.
 *
 * These values are computed as described above and shall be used to calculate
 * the radiance mapping factors @c map_fac_s and @c map_fac_vs.
 *****************************************************************************/
int rpp_rmap_meas_sums(rpp_rmap_meas_drv_t * drv, rpp_rmap_sums_t * sums);

/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_RMAP_MEAS_DRV_H__ */

