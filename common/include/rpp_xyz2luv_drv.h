/******************************************************************************
 *
 * Copyright 2019 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_xyz2luv_drv.h
 *
 * @brief   Interface of white-balance gain driver
 *
 *****************************************************************************/
#ifndef __RPP_XYZ2LUV_DRV_H__
#define __RPP_XYZ2LUV_DRV_H__

#include "rpp_config.h"
#include <rpp_hdr_types.h>

#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppXyz2luvDrv RPP - CIE XYZ to LUV Unit Driver
 * RPP - CIE XYZ to LUV Unit Driver
 *
 * This driver is responsible for controlling the operation
 * of hardware units that perform the CIE XYZ to CIE LUV
 * color space conversion. There are some parameters that
 * can be configured to optimize this conversion:
 *  - U/V white point reference values
 *  - Luma/Chroma output factors
 * @{
 *****************************************************************************/

/**************************************************************************//**
 * @brief RPP XYZ2LUV specific driver structure
 *
 * This structure encapsulates all data required by the RPP XYZ2LUV driver to
 * operate a specific instance of the RPP XYZ2LUV on a given RPPHDR device.
 * The data fields contained in this structure are intended to be
 * private to the driver and not to be modified by any higher-level
 * software component.
 *
 * It is required to have a separate instance of this structure for each
 * separate RPP XYZ2LUV module in a given RPPHDR device. Each of these structures
 * must be initialized before use by calling the function @c rpp_xyz2luv_init()
 * with suitable parameters.
 *
 * The structure is exported here so that instances can statically be declared
 * by higher-level software without the need for dynamic memory allocation.
 *****************************************************************************/
typedef struct {
    uint32_t        version;
    uint32_t        base;
    rpp_device *    dev;
} rpp_xyz2luv_drv_t;


/**************************************************************************//**
 * @brief Initialize the rpp xyz2luv module.
 * @param[in]       dev     rpp_device structure to be used for all register accesses
 * @param[in]       base    base address of RPP XYZ2LUV module
 * @param[inout]    drv     XYZ2LUV driver structure to be initialized
 * @return      0 on success, error-code otherwise
 *
 * This function initializes the @c drv structure for future uses of the driver
 * on the given instance of the rpp xyz2luv module. It fills the @c drv structure
 * with data required for operating on the given instance and caches data that
 * describes the features of the xyz2luv module instance.
 *
 * This function needs to be called once for every instance of the rpp xyz2luv
 * module to operate, each time with a different @c drv argument intended for
 * that very instance of the rpp xyz2luv module only.
 *****************************************************************************/
int rpp_xyz2luv_init(rpp_device * dev, uint32_t base, rpp_xyz2luv_drv_t * drv);

/**************************************************************************//**
 * @brief configures the U/V white point reference values
 * @param[in]   drv     initialized XYZ2LUV driver structure describing the xyz2luv unit instance
 * @param[in]   u_ref    pre-calculated value for U in unsigned 3.10 fixed point format
 * @param[in]   v_ref    pre-calculated value for V in unsigned 0.10 fixed point format
 * @return      0 on success, error-code otherwise
 *
 * This function is used to configure the pre-calculated white point reference
 * values for the U and V chroma components. They can be calculated similar
 * to the following code:
 * @code
 * U_ref = (int)[4*X_ref/(X_ref + 15*Y_ref + 3*Z_ref) *2^10]
 * V_ref = (int)[9*Y_ref/(X_ref + 15*Y_ref + 3*Z_ref) *2^10]
 * @endcode
 *
 *****************************************************************************/
int rpp_xyz2luv_set_ref(rpp_xyz2luv_drv_t * drv, const uint16_t u_ref,
        const uint16_t v_ref);

/**************************************************************************//**
 * @brief Retrieves the U/V white point reference values
 * @param[in]   drv     initialized XYZ2LUV driver structure describing the xyz2luv unit instance
 * @param[out]  u_ref    configured value for U in unsigned 3.10 fixed point format
 * @param[out]  v_ref    configured value for V in unsigned 0.10 fixed point format
 * @return      0 on success, error-code otherwise
 *
 * This function is used to retrieve the currently configured white point reference
 * values for the U and V chroma components from the unit.
 *****************************************************************************/
int rpp_xyz2luv_ref(rpp_xyz2luv_drv_t * drv, uint16_t * const u_ref,
        uint16_t * const v_ref);

/**************************************************************************//**
 * @brief configures the luma/chroma scaling factors to adapt the output value range
 * @param[in]   drv     initialized XYZ2LUV driver structure describing the xyz2luv unit instance
 * @param[in]   luma     luminance output scaling factor in unsigned 1.7 fixed point format
 * @param[in]   chroma   chrominance output scaling factor in unsigned 1.7 fixed point format
 * @return      0 on success, error-code otherwise
 *
 * This function is used to configure scaling factors for luminance and chrominance
 * values which can be used to adjust the corresponding value range at the output.
 *****************************************************************************/
int rpp_xyz2luv_set_output_factors(rpp_xyz2luv_drv_t * drv, const uint8_t luma,
        const uint8_t chroma);

/**************************************************************************//**
 * @brief retrieves the luma/chroma scaling factors to adapt the output value range
 * @param[in]   drv     initialized XYZ2LUV driver structure describing the xyz2luv unit instance
 * @param[out]  luma     luminance output scaling factor in unsigned 1.7 fixed point format
 * @param[out]  chroma   chrominance output scaling factor in unsigned 1.7 fixed point format
 * @return      0 on success, error-code otherwise
 *
 * This function is used to retrieve scaling factors for luminance and chrominance
 * values from the unit.
 *****************************************************************************/
int rpp_xyz2luv_output_factors(rpp_xyz2luv_drv_t * drv, uint8_t * const luma,
        uint8_t * const chroma);


/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_XYZ2LUV_DRV_H__ */

