/******************************************************************************
 *
 * Copyright 2019 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_awb_gain_drv.h
 *
 * @brief   Interface of white-balance gain driver
 * 
 *****************************************************************************/
#ifndef __RPP_AWB_GAIN_DRV_H__
#define __RPP_AWB_GAIN_DRV_H__

#include "rpp_config.h"

#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppAwbGainDrv RPP - White Balance Gain Driver
 * RPP - White Balance Gain Driver
 * @{
 *****************************************************************************/

/**************************************************************************//**
 * @brief RPP AWB_GAIN specific driver structure
 *
 * This structure encapsulates all data required by the RPP AWB_GAIN driver to
 * operate a specific instance of the RPP AWB_GAIN on a given RPPHDR device.
 * The data fields contained in this structure are intended to be
 * private to the driver and not to be modified by any higher-level
 * software component.
 *
 * It is required to have a separate instance of this structure for each
 * separate RPP AWB_GAIN module in a given RPPHDR device. Each of these structures
 * must be initialized before use by calling the function @c rpp_awb_gain_init()
 * with suitable parameters.
 *
 * The structure is exported here so that instances can statically be declared
 * by higher-level software without the need for dynamic memory allocation.
 *****************************************************************************/
typedef struct {
    uint32_t        version;
    uint32_t        base;
    rpp_device *    dev;
} rpp_awb_gain_drv_t;


/**************************************************************************//**
 * @brief Initialize the rpp awb gain module.
 * @param[in]       dev     rpp_device structure to be used for all register accesses
 * @param[in]       base    base address of RPP AWB_GAIN module
 * @param[inout]    drv     AWB_GAIN driver structure to be initialized
 * @return      0 on success, error-code otherwise
 *
 * This function initializes the @c drv structure for future uses of the driver
 * on the given instance of the rpp awb_gain module. It fills the @c drv structure
 * with data required for operating on the given instance and caches data that
 * describes the features of the awb_gain module instance.
 *
 * This function needs to be called once for every instance of the rpp awb_gain
 * module to operate, each time with a different @c drv argument intended for
 * that very instance of the rpp awb_gain module only.
 *****************************************************************************/
int rpp_awb_gain_init(rpp_device * dev, uint32_t base, rpp_awb_gain_drv_t * drv);

/**************************************************************************//**
 * @brief Enable or disable the rpp awb_gain module.
 * @param[in]   drv     initialized AWB_GAIN driver structure describing the awb_gain unit instance
 * @param[in]   on      enable state to set (0: disable, otherwise enable)
 * @return      0 on success, error-code otherwise
 *
 * If disabled, image data passes through the unit unmodified. If enabled,
 * suitable gain factors must have been programmed in advance.
 *****************************************************************************/
int rpp_awb_gain_enable(rpp_awb_gain_drv_t * drv, const unsigned on);

/**************************************************************************//**
 * @brief Return enable status of the rpp awb gain module.
 * @param[in]   drv     initialized AWB_GAIN driver structure describing the awb_gain unit instance
 * @param[out]  on      current enable state (0: disabled, otherwise enabled)
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int rpp_awb_gain_enabled(rpp_awb_gain_drv_t * drv, unsigned * const on);

/**************************************************************************//**
 * @brief Programs the white-balance gains to the rpp awb_gain unit.
 * @param[in]   drv         initialized AWB_GAIN driver structure describing the awb_gain unit instance
 * @param[in]   red         gain for the red bayer component
 * @param[in]   green_red   gain for the green bayer component in red lines
 * @param[in]   blue        gain for the blue bayer component
 * @param[in]   green_blue  gain for the green bayer component in blue lines
 * @return      0 on success, error-code otherwise
 *
 * The "gain" values are factors by which the corresponding color samples
 * are multiplied. Multiple factors need to be specified, there is one
 * factor for each component of the Bayer cluster.
 *
 * Following formula illustrates this:@n@n
 *
 * \f$Red_{out} = red * Red_{in}\f$
 *
 * Only positive factors can be specified.
 * The gain stage clips the resulting value to the allowed range
 * if the product becomes larger than the maximum color value.
 *
 * The gain factors are given as integer number with 18 bit width, but
 * they represent fractional numbers in FP6.12 fix-point number format,
 * which resolves to an integer part of 6bit capacity (0..63) and a
 * fractional part of 12bit (e.g. a resolution of 1/4096). The conversion
 * from floating point numbers to the fix-point number representation
 * works as in the following example:
 *
 * @code
 * // pseudo c code when using floats
 * float g_red      = 1.23f;
 * float c_red      = g_red * 4096.0f;              // c_red = 5038.08
 * uint32_t v_red   = (uint32_t) roundf( c_red );   // v_red = 5038
 * ...
 * res = rpp_awb_set_gains( drv , v_red, .... ); 
 * assert( res == 0 );
 * ...
 * @endcode
 *
  *****************************************************************************/
int rpp_awb_gain_set_gains
(
    rpp_awb_gain_drv_t * drv,
    const uint32_t red,
    const uint32_t green_red,
    const uint32_t blue,
    const uint32_t green_blue
);

/**************************************************************************//**
 * @brief Read the white-balance gains from the rpp awb_gain unit.
 * @param[in]   drv         initialized AWB_GAIN driver structure describing the awb_gain unit instance
 * @param[out]  red         current gain for the red bayer component
 * @param[out]  green_red   current gain for the green bayer component in red lines
 * @param[out]  blue        current gain for the blue bayer component
 * @param[out]  green_blue  current gain for the green bayer component in blue lines
 * @return      0 on success, error-code otherwise
 *
 * The "gain" values are factors by which the corresponding color samples
 * are multiplied. Multiple factors need to be specified, there is one
 * factor for each component of the Bayer cluster.
 *
 * Following formula illustrates this:@n@n
 *
 * \f$Red_{out} = red * Red_{in}\f$
 *
 * Only positive factors can be specified.
 * The gain stage clips the resulting value to the allowed range
 * if the product becomes larger than the maximum color value.
 *
 * The gain factors are given as integer number with 18 bit width, but
 * they represent fractional numbers in FP6.12 fix-point number format,
 * which resolves to an integer part of 6bit capacity (0..63) and a
 * fractional part of 12bit (e.g. a resolution of 1/4096). The conversion
 * from fix-point number representation to floating point values
 * works as in the following example:

 * @code
 * // pseudo c code when using floats
 * float g_red;
 * uint32_t v_red;
 * ...
 * res = rpp_awb_gains( RPP_AWB_GAIN_BASE, &v_red, .... ); 
 * assert( res == 0 );
 *
 * g_red = (float)v_red / 4096.0f;
 * ...
 * @endcode
 *****************************************************************************/
int rpp_awb_gain_gains
(
    rpp_awb_gain_drv_t * drv,
    uint32_t * const red,
    uint32_t * const green_red,
    uint32_t * const blue,
    uint32_t * const green_blue
);

/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_AWB_GAIN_DRV_H__ */

