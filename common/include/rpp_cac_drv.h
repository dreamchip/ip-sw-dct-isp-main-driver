/******************************************************************************
 *
 * Copyright 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, dcactributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permcacsion of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_cac_drv.h
 *
 * @brief   Interface of Chromatic Aberration Correction unit driver
 *
 *****************************************************************************/
#ifndef __RPP_CAC_DRV_H__
#define __RPP_CAC_DRV_H__

#include "rpp_config.h"
#include <rpp_hdr_types.h>

#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppCacDrv RPP - Chromatic Aberration Correction unit driver
 * RPP - Chromatic Aberration Correction Unit Driver
 *
 * @note All configuration of the unit is subject to register
 * shadowing and will not have an immediate effect. Any configuration
 * programmed to the unit will be activated synchronized with an
 * end-of-frame signal when the shadow register update has been
 * requested in the top-level RPP unit register.
 * The currently active configuration cannot be retrieved from
 * the unit.
 * @{
 *
 *****************************************************************************/

/**************************************************************************//**
 * @brief Defines horizontal correction clipping modes.
 *
 * In some areas of the image the calculated displacement of red or
 * blue pixels due to the chromatic aberration may become larger than
 * reasonable.
 *
 * The values enumerated here define the different supported options
 * for clipping the horizontal displacement of red or blue pixels.
 *****************************************************************************/
typedef enum {
    /*! @brief Limit horizontal displacement to +/-4 pixel. */
    RPP_CAC_H_CLIP_MODE_FIXED   = 0,
    /*! @brief Dynamically limit horizontal displacement to +/-4 or +/-5 pixels.
     *
     * Depending on pixel position inside the Bayer cluster, this option makes
     * the unit switch dynamically between a maximum displacement of +/-4
     * or of +/-5 pixels.
     */
    RPP_CAC_H_CLIP_MODE_DYNAMIC = 1,
    RPP_CAC_H_CLIP_MODE_INVALID
} rpp_cac_h_clip_mode_t;



/**************************************************************************//**
 * @brief Defines vertical correction clipping modes.
 * In some areas of the image the calculated displacement of red or 
 * blue pixels due to the chromatic aberration may become larger than
 * reasonable.
 *
 * The values enumerated here define the different supported options
 * for clipping the vertical displacement of red or blue pixels.
 */
typedef enum {
    /** @brief @brief Limit vertical displacement to +/-2 lines.
     * This option configures the unit to limit the vertical displacement
     * to +/-2 lines and always enables the chroma low-pass filter.
     */
    RPP_CAC_V_CLIP_MODE_FIXED_2 = 0b00,
    /** @brief Limit vertical displacement to +/-3 lines.
     * This option configures the unit to limit the vertical displacement
     * to +/-3 lines and dynamically enables the chroma low-pass filter.
     */
    RPP_CAC_V_CLIP_MODE_FIXED_3 = 0b01,
    /** @brief Dynamically limit vertical displacement to +/- 3 or +/-4 lines.
     * This option configures the unit to limit the vertical displacement
     * to either +/-3 lines or +/-4 lines depending on the position of the
     * pixel inside the Bayer cluster.
     */
    RPP_CAC_V_CLIP_MODE_DYNAMIC = 0b10,
    RPP_CAC_V_CLIP_MODE_INVALID
} rpp_cac_v_clip_mode_t;

/**************************************************************************//**
 * @brief Per-color coefficients for radial shift calculation
 *
 * This structure holds coefficients that are used in the
 * cubic interpolation of the radial shift of a given color
 * component, according to the following formula:
 * @f[
 *     S_i = A_i \cdot r + B_i \cdot r^2 + C_i \cdot r^3
 * @f]
 * where i is one of red or blue, and A, B, C are
 * the linear, quadratic and cubic coefficients, respectively.
 * 
 * This structure holds all the different coefficients. Each of
 * the coefficients is in the format of a signed FP5.4 number
 * with a value range of -16 .. 15.9375. A value of 1.0
 * is represented by 16.
 *****************************************************************************/
typedef struct {
    /** @brief linear coefficient for red radial shift calculation */
    int16_t     a_red;
    /** @brief quadratic coefficient for red radial shift calculation */
    int16_t     b_red;
    /** @brief cubic coefficient for red radial shift calculation */
    int16_t     c_red;
    /** @brief linear coefficient for blue radial shift calculation */
    int16_t     a_blue;
    /** @brief quadratic coefficient for blue radial shift calculation */
    int16_t     b_blue;
    /** @brief cubic coefficient for blue radial shift calculation */
    int16_t     c_blue;
} rpp_cac_coeffs_t;

/**************************************************************************//**
 * @brief Per-direction normalization parameters
 *
 * This structure holds normalization parameters for calculation
 * of image coordinates x_d and y_d relative to optical center
 * of the image. For each direction, there is a 4-bit shift
 * parameter ranging from 0..15, and a 5-bit factor ranging
 * from 0..31.
 *****************************************************************************/
typedef struct {
    /** @brief horizontal normalization shift parameter */
    uint8_t     x_ns;
    /** @brief horizontal normalization factor parameter */
    uint8_t     x_nf;
    /** @brief vertical normalization shift parameter */
    uint8_t     y_ns;
    /** @brief vertical normalization factor parameter */
    uint8_t     y_nf;
} rpp_cac_norm_t;


/**************************************************************************//**
 * @brief RPP CAC specific driver structure
 *
 * This structure encapsulates all data required by the RPP CAC driver to
 * operate a specific instance of the RPP CAC on a given RPPHDR device.
 * The data fields contained in this structure are intended to be
 * private to the driver and not to be modified by any higher-level
 * software component.
 *
 * It is required to have a separate instance of this structure for each
 * separate RPP CAC module in a given RPPHDR device. Each of these structures
 * must be initialized before use by calling the function @c rpp_cac_init()
 * with suitable parameters.
 *
 * The structure is exported here so that instances can statically be declared
 * by higher-level software without the need for dynamic memory allocation.
 *****************************************************************************/
typedef struct {
    uint32_t        version;
    uint32_t        base;
    rpp_device *    dev;
} rpp_cac_drv_t;


/**************************************************************************//**
 * @brief Initialize the rpp cac module.
 * @param[in]       dev     rpp_device structure to be used for all regcacter accesses
 * @param[in]       base    base address of RPP CAC module
 * @param[inout]    drv     CAC driver structure to be initialized
 * @return      0 on success, error code otherwise
 *
 * This function initializes the @c drv structure for future uses of the driver
 * on the given instance of the rpp cac module. It fills the @c drv structure
 * with data required for operating on the given instance and caches data that
 * describes the features of the cac module instance.
 *
 * This function needs to be called once for every instance of the rpp cac
 * module to operate, each time with a different @c drv argument intended for
 * that very instance of the rpp cac module only.
 *****************************************************************************/
int rpp_cac_init(rpp_device * dev, uint32_t base, rpp_cac_drv_t * drv);

/**************************************************************************//**
 * @brief Enable or disable the RPP CAC unit
 * @param[in]   drv     initialized CAC driver structure describing the cac unit instance
 * @param[in]   on      enable state to set (0: disable, otherwise enable)
 * @param[in]   h_start preload value for horizontal CAC counter
 * @param[in]   v_start preload value for vertical CAC counter
 * @return      0 on success, error code otherwise
 *
 * This function enables or disables the CAC unit. When enabled,
 * the unit also needs to be programmed with coordinates indicating
 * the optical center of the image using the @c h_start and @c v_start
 * parameters. These shall be calculated as follows:
 * @f[
 *      h_{start} = (width / 2 + h\_center\_offset)
 *      v_{start} = (height / 2 + v\_center\_offset)
 * @f]
 * where the horizontal and vertical center offsets are the difference
 * between the image center and the optical image center. The parameters
 * are used to initialize pixel/line counters that count down from
 * line/frame start, become zero at the optical center and get
 * negative beyond.
 *
 * The @c h_start and @c v_start parameters are only required
 * when the unit is enabled and must lie between 1 and 8191.
 * An error code is returned should these parametes be required and not
 * be within the allowed value range
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_cac_enable(
        rpp_cac_drv_t *     drv,
        unsigned const      on,
        uint16_t const      h_start,
        uint16_t const      v_start
        );

/**************************************************************************//**
 * @brief Return enable status of the rpp cac module.
 * @param[in]   drv     initialized CAC driver structure describing the cac module instance
 * @param[out]  on      current CAC enable state (0: disabled, otherwise enabled)
 * @return      0 on success, error code otherwise
 *****************************************************************************/
int rpp_cac_enabled(rpp_cac_drv_t * drv, unsigned * const on);

/**************************************************************************//**
 * @brief Return enable status and optical center coordinates.
 * @param[in]   drv     initialized CAC driver structure describing the cac unit instance
 * @param[out]  on      current CAC enable state (0: disabled, otherwise enabled)
 * @param[out]  h_start current preload value for horizontal CAC counter
 * @param[out]  v_start current preload value for vertical CAC counter
 * @return      0 on success, error code otherwise
 *
 * This function returns whether the unit is enabled or not,
 * and also returns the horizontal and vertical coordinates of the
 * optical image center.
 *****************************************************************************/
int rpp_cac_get_state(
        rpp_cac_drv_t *     drv,
        unsigned * const    on,
        uint16_t * const    h_start,
        uint16_t * const    v_start
        );


/**************************************************************************//**
 * @brief Function to configure the horizontal and vertical displacement clipping modes.
 * @param[in]   drv     initialized CAC driver structure describing the cac unit instance
 * @param[in]   h_clip   one of the enumerated values in @c CAC_H_CLIP_MODE
 * @param[in]   v_clip   one of the enumerated values in @c CAC_V_CLIP_MODE
 * @return      0 on success, error code otherwise
 *
 * This function configures how the unit shall limit the calculated
 * displacement of red or blue pixels in image areas where the displacement
 * becomes overly large.
 *
 * Different options are available for limiting in horizontal and vertical
 * directions.
 *
 * An error code is returns should any of the parameters not be within
 * the allowed range.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_cac_set_clip_mode(
        rpp_cac_drv_t *             drv,
        rpp_cac_h_clip_mode_t const h_clip,
        rpp_cac_v_clip_mode_t const v_clip
        );

/**************************************************************************//**
 * @brief Function to retrieve the horizontal and vertical displacement clipping modes.
 * @param[in]   drv     initialized CAC driver structure describing the cac unit instance
 * @param[out]  h_clip  contains horizontal clipping mode on return
 * @param[out]  v_clip  contains vertical clipping mode on return
 * @return      0 on success, error code otherwise
 *
 * This function retrieves how the unit shall limit the calculated
 * displacement of red or blue pixels in image areas where the displacement
 * becomes overly large.
 *****************************************************************************/
int rpp_cac_clip_mode(
        rpp_cac_drv_t *                 drv,
        rpp_cac_h_clip_mode_t * const   h_clip,
        rpp_cac_v_clip_mode_t * const   v_clip
        );

/**************************************************************************//**
 * @brief Function to configure coefficients for radial shift calculation.
 * @param[in]   drv     initialized CAC driver structure describing the cac unit instance
 * @param[in]   coeffs  structure holding all the coefficients
 * @return      0 on success, error code otherwise
 *
 * This function is used to program the coefficients into the unit that
 * are used for the calculation of the radial shift. The shift is calculated
 * for the blue and red color separately.
 *
 * An error code is returns should any of the parameters in @c coeffs not be within
 * the allowed range.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_cac_set_coeffs(rpp_cac_drv_t * drv, rpp_cac_coeffs_t const * const coeffs);


/**************************************************************************//**
 * @brief Function to retrieve the coefficients for radial shift calculation.
 * @param[in]   drv     initialized CAC driver structure describing the cac unit instance
 * @param[out]  coeffs  contains all the coefficients on return
 * @return      0 on success, error code otherwise
 *
 * This function retrieves the linear, quadratic and cubic coefficients
 * for the calculation of the radial red/blue shift from the unit and
 * returns the data in the supplied structure.
 *****************************************************************************/
int rpp_cac_coeffs(rpp_cac_drv_t * drv, rpp_cac_coeffs_t * const coeffs);

/**************************************************************************//**
 * @brief Function to configure the normalization function parameters
 * @param[in]   drv     initialized CAC driver structure describing the cac unit instance
 * @param[in]   params  structure holding all the normalization function parameters
 * @return      0 on success, error code otherwise
 *
 * This function is used to program the horizontal and vertical
 * normalization parameters for calculation of image coordinates
 * x_d and y_d relative to the optical center.
 *
 * An error code is returns should any of the parameters in @c params not be within
 * the allowed range.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_cac_set_norm(rpp_cac_drv_t * drv, rpp_cac_norm_t const * const params);


/**************************************************************************//**
 * @brief Function to retrieve the normalization function parameters.
 * @param[in]   drv     initialized CAC driver structure describing the cac unit instance
 * @param[out]  params  contains all the normalization function parameters on return
 * @return      0 on success, error code otherwise
 *
 * This function retrieves the horizontal and vertical
 * normalization parameters for calculation of image coordinates
 * x_d and y_d relative to the optical center from the unit and
 * returns the data in the supplied structure.
 *****************************************************************************/
int rpp_cac_norm(rpp_cac_drv_t * drv, rpp_cac_norm_t * const params);


/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_CAC_DRV_H__ */

