/******************************************************************************
 *
 * Copyright 2019 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_dpf_drv.h
 *
 * @brief   Interface of Denoising Pre-Filter Driver
 *
 *****************************************************************************/
#ifndef __RPP_DPF_DRV_H__
#define __RPP_DPF_DRV_H__

#include "rpp_config.h"
#include <rpp_hdr_types.h>

#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppDpfDrv RPP - Denoising Pre-Filter DPF (bilateral denoising)  Driver
 * RPP - Denoising Pre-Filter DPF (bilateral denoising) Driver
 *
 * The Denoising Pre-Filter unit attempts to remove signal noise
 * from the raw image sensor data using a sophisticated mathematical
 * model of the noise. The noise is independently removed from
 * each of the color channels in the Bayer pattern.
 *
 * The technique requires knowledge of the noise standard deviation,
 * which is dependent on the unprocessed intensity level of the pixel.
 * This information is programmed to the unit using two piece-wise
 * linear approximations with 17 points each, separately for green
 * and red/blue pixels. This linear approximation is called Noise Level
 * Lookup Table. In order to reconstruct the unprocessed intensity
 * level of the pixels, the inverse gains of the preceeding AWB_GAIN unit
 * need to be programmed to the unit as well.
 *
 * Beside the sensor and lens properties itself, the noise level function
 * also depends on the gain (both, analog and digital gain) programmed
 * into the sensor. It is therefore required to re-program the NLL
 * tables when the programmed sensor gain changes significantly.
 *
 * The filter also has a spatial component. It considers the photometric
 * distance between pixel intensities in the neighbourhood and the center
 * pixel. The strength of the spatial filtering itself and the weight of
 * an intensity difference (depending on the distance between the center
 * and the neighbouring pixel) must be programmed.
 *
 * @note All configuration of the unit is subject to register
 * shadowing and will not have an immediate effect. Any configuration
 * programmed to the unit will be activated synchronized with an
 * end-of-frame signal when the shadow register update has been
 * requested in the top-level RPP unit register.
 * The currently active configuration cannot be retrieved from
 * the unit.
 * @{
 *
 *****************************************************************************/

/**************************************************************************//**
 * @brief This type describes the Denoising Pre-Filter (DPF) operation mode.
 *
 * This data type is used to gain access to the various
 * flags that can individually be enabled or disabled to modify
 * the unit's mode of operation.
 * @note All the attributes in this structure are of Boolean type
 * and must be set to 0 or 1, respectively.
 *****************************************************************************/
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
typedef union rpp_dpf_mode_u {
    uint32_t mode;
    struct {
        /*@ @brief Whether filter processing is enabled at all or disabled. */
        uint32_t enable              : 1;   // bit 0
        /*@ @brief Whether filter processing for blue pixels is disabled. */
        uint32_t b_filter_off        : 1;   // bit 1
        /*@ @brief Whether filter processing for green pixels in blue lines is disabled. */
        uint32_t gb_filter_off       : 1;   // bit 2
        /*@ @brief Whether filter processing for green pixels in red lines is disabled. */
        uint32_t gr_filter_off       : 1;   // bit 3
        /*@ @brief Whether filter processing for red pixels is disabled. */
        uint32_t r_filter_off        : 1;   // bit 4
        /** @brief Whether a 9x9 (5x5 active) pixel filter kernel for Red/Blue is used.
         * If disabled, a wide filter of 13x9 (7x9 active) pixels is used. */
        uint32_t rb_filter_size9x9   : 1;   // bit 5
        /** @brief Whether optimized logarithmic like segmentation for Noise Level Lookup (NLL) are enabled. */
        uint32_t nll_equ_segm        : 1;   // bit 6
        uint32_t _unused1            : 1;   // bit 7
        /** @brief Specifies whether the LSC gains are processed.
         * If disabled, an LSC gain factor of 1.0 is assumed. */
        uint32_t lsc_gain_en         : 1;   // bit 8
        /** @brief Specifies whether the configured noise function gains are used. */
        uint32_t use_nf_gain         : 1;   // bit 9
        uint32_t _unused2            : 22;  // bits 10:31
    };
} rpp_dpf_mode_t;
#pragma GCC diagnostic pop


/**************************************************************************//**
 * @brief Datatype describing the spatial filter strength per color channel.
 *
 * Filter strength of the filter is determined by a weight. Default is a weight
 * of 1, higher weight increases the filter strength. In the fields of this
 * structure, the unsigned 8 bit value 1.0/weight is stored in FP2.6 format,
 * separately for red, blue and green pixels.
 *
 * This is illustrated by the following examples:
 *  - weight = 0.251    => value = 255
 *  - weight = 0.5      => value = 128
 *  - weight = 1        => value = 64 (default)
 *  - weight = 1.25     => value = 51
 *  - weight = 2        => value = 32
 *  - ...
 *  - weight = 64       => value = 1
 *  - weight = max      => value = 0
 *****************************************************************************/
typedef struct rpp_dpf_spatial_strength_s {
    /** @brief inverse spatial filter strength for red pixels. */
    uint8_t red;
    /** @brief inverse spatial filter strength for green pixels. */
    uint8_t green;
    /** @brief inverse spatial filter strength for blue pixels. */
    uint8_t blue;
} rpp_dpf_spatial_strength_t;


#define RPP_DPF_COEFF_NUM (6u)

/**************************************************************************//**
 * @brief Datatype describing the spatial filter coefficients.
 *
 * This helper structure holds the sets of spatial filter coefficients
 * individually for different color components:
 *  - one set for both of the green components (gr, gb)
 *  - one set for both of the red/blue components
 *
 * Each set contains 6 coefficients that represent the
 * register values s_weight_g1..6 or s_weight_rb1..6.
 *
 * Each coefficient must be an FP1.4 number between 0.0 and 1.0,
 * which means that the configured values are allowed to range from
 * 0 to 16.
 *
 *****************************************************************************/
typedef struct rpp_dpf_spatial_weight_s {
    /** @brief spatial filter coefficients for green pixels. */
    uint8_t g [RPP_DPF_COEFF_NUM];
    /** @brief spatial filter coefficients for red and blue pixels. */
    uint8_t rb[RPP_DPF_COEFF_NUM];
} rpp_dpf_spatial_weight_t;


#define RPP_DPF_NLL_COEFF_NUM (17u)

/**************************************************************************//**
 * @brief Datatype describing the noise level lookup function.
 *
 * This helper struct contains two arrays, each holding 17 integer values
 * between 1 and 1023 that represent the piece-wise linear approximation
 * of the noise level lookup function. The values represent FP0.10 fixed
 * point numbers.
 *
 * There are different noise level lookup functions for green and
 * non-green (red, blue) color channels.
 *
 * The two different arrays are for different color components.
 *****************************************************************************/
typedef struct rpp_dpf_nll_coeff_s {
    /** @brief Noise Level Lookup (NLL) function for green pixels */
    uint16_t coeff_g [RPP_DPF_NLL_COEFF_NUM];
    /** @brief Noise Level Lookup (NLL) function for red and blue pixels */
    uint16_t coeff_rb[RPP_DPF_NLL_COEFF_NUM];
} rpp_dpf_nll_coeff_t;


/**************************************************************************//**
 * @brief RPP DPF specific driver structure
 *
 * This structure encapsulates all data required by the RPP DPF driver to
 * operate a specific instance of the RPP DPF on a given RPPHDR device.
 * The data fields contained in this structure are intended to be
 * private to the driver and not to be modified by any higher-level
 * software component.
 *
 * It is required to have a separate instance of this structure for each
 * separate RPP DPF module in a given RPPHDR device. Each of these structures
 * must be initialized before use by calling the function @c rpp_dpf_init()
 * with suitable parameters.
 *
 * The structure is exported here so that instances can statically be declared
 * by higher-level software without the need for dynamic memory allocation.
 *****************************************************************************/
typedef struct {
    uint32_t        version;
    uint32_t        base;
    rpp_device *    dev;
} rpp_dpf_drv_t;

/**************************************************************************//**
 * @brief Initialize rpp dpf module.
 * @param[in]       dev     rpp_device structure to be used for all register accesse
 * @param[in]       base    base address of RPP DPF module
 * @param[inout]    drv     DPF driver structure to be initialized
 * @return      0 on success, error code otherwise
 *
 * This function initializes the @c drv structure for future uses of the driver
 * on the given instance of the rpp dpf module. It fills the @c drv structure
 * with data required for operating on the given instance and caches data that
 * describes the features of the dpf module instance.
 *
 * This function needs to be called once for every instance of the rpp dpf
 * module to operate, each time with a different @c drv argument intended for
 * that very instance of the rpp dpf module only.
 *****************************************************************************/
int rpp_dpf_init(rpp_device * dev, uint32_t base, rpp_dpf_drv_t * drv);

/**************************************************************************//**
 * @brief Enable/disable RPP DPF unit and set operation mode.
 * @param[in]   drv     initialized DPF driver structure describing the dpf unit instance
 * @param[in]   mode    selected operation mode details
 * @return      0 on success, error code otherwise
 *
 * This function is used to enable or disable the DPF unit. When enabled,
 * the other flags in the supplied argument indicate how exactly the unit
 * is supposed to operation. Various subfunctions of the unit regarding
 * DPF processing can individually be enabled or disabled.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_dpf_set_mode(rpp_dpf_drv_t * drv, const rpp_dpf_mode_t* const mode);

/**************************************************************************//**
 * @brief Get details about the current DPF unit operation mode.
 * @param[in]   drv     initialized DPF driver structure describing the dpf unit instance
 * @param[out]  mode    current operation mode details
 * @return      0 on success, error code otherwise
 *
 * This function is used to retrieve the current operation mode details
 * from the unit. The information contained in @c mode on successful
 * return from this function indicates whether the unit is enabled at all
 * and, if enabled, how the unit is configured to operate.
 *****************************************************************************/
int rpp_dpf_mode(rpp_dpf_drv_t * drv, rpp_dpf_mode_t* const mode);

/**************************************************************************//**
 * @brief Set rpp dpf spatial filter strength.
 * @param[in]   drv         initialized DPF driver structure describing the dpf unit instance
 * @param[in]   strength    spatial red, green and blue filter strengths
 * @return      0 on success, error code otherwise
 *
 * This function is used to program a strength for the spatial filter to the unit,
 * a separate filter strength is configured for each color channel. Note the
 * format of the filter strength specification in the associated structure
 * documentation.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_dpf_set_spatial_strength(rpp_dpf_drv_t * drv,
        const rpp_dpf_spatial_strength_t* const strength);

/**************************************************************************//**
 * @brief Get rpp dpf spatial filter strength.
 * @param[in]   drv         initialized DPF driver structure describing the dpf unit instance
 * @param[out]  strength    spatial red, green and blue filters
 * @return      0 on success, error code otherwise
 *
 * This function retrieves the filter strength values from the unit
 * and returns the data in the supplied structure.
 *****************************************************************************/
int rpp_dpf_spatial_strength(rpp_dpf_drv_t * drv, rpp_dpf_spatial_strength_t* const strength);

/**************************************************************************//**
 * @brief Programs the spatial filter coefficients.
 * @param[in]   drv     initialized DPF driver structure describing the dpf unit instance
 * @param[in]   weight  spatial filter coefficients for red/blue channels
 * @return      0 on success, error code otherwise
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_dpf_set_spatial_weight(rpp_dpf_drv_t * drv, const rpp_dpf_spatial_weight_t* const weight);

/**************************************************************************//**
 * @brief Get rpp dpf spatial filter coefficients
 * @param[in]   drv     initialized DPF driver structure describing the dpf unit instance
 * @param[out]  weight  filter coefficients for red/blue channels
 * @return      0 on success, error code otherwise
 *****************************************************************************/
int rpp_dpf_spatial_weight(rpp_dpf_drv_t * drv, rpp_dpf_spatial_weight_t* const weight);

/**************************************************************************//**
 * @brief Set rpp dpf noise level lookup function curves.
 * @param[in]   drv         initialized DPF driver structure describing the dpf unit instance
 * @param[in]   nll_coeff   noise level lookup function curves
 * @return      0 on success, error code otherwise
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_dpf_set_nll_coeff(rpp_dpf_drv_t * drv, const rpp_dpf_nll_coeff_t* const nll_coeff);

/**************************************************************************//**
 * @brief Get rpp dpf noise level lookup function curve.
 * @param[in]   drv         initialized DPF driver structure describing the dpf unit instance
 * @param[out]  nll_coeff   noise level lookup function curves
 * @return      0 on success, error code otherwise
 *****************************************************************************/
int rpp_dpf_nll_coeff(rpp_dpf_drv_t * drv, rpp_dpf_nll_coeff_t* const nll_coeff);

/**************************************************************************//**
 * @brief Program white-balance gains into the rpp dpf unit.
 * @param[in]   drv         initialized DPF driver structure describing the dpf unit instance
 * @param[in]   red         gain factor for the red Bayer component
 * @param[in]   green_red   gain factor for the green Bayer component in red lines
 * @param[in]   green_blue  gain factor for the green Bayer component in blue lines
 * @param[in]   blue        gain factor for the blue Bayer component
 * @return      0 on success, error code otherwise
 *
 * This function is used to tell the DPF unit about the white-balance gains
 * configured to the preceeding awb_gain unit so that the effect of the
 * awb_gain can be reversed for filtering.
 *
 * The gain factors are interpreted as FP4.8 fixed point numbers (12bit wide)
 * and must be between 1..4095. A value of 1.0 is represented by 256.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_dpf_set_nf_gain(rpp_dpf_drv_t * drv,
        const uint16_t red,
        const uint16_t green_red,
        const uint16_t green_blue,
        const uint16_t blue
        );

/**************************************************************************//**
 * @brief Get rpp dpf noise function gain.
 * @param[in]   drv     initialized DPF driver structure describing the dpf unit instance
 * @param[out]  red         gain factor for the red Bayer component
 * @param[out]  green_red   gain factor for the green Bayer component in red lines
 * @param[out]  green_blue  gain factor for the green Bayer component in blue lines
 * @param[out]  blue        gain factor for the blue Bayer component
 * @return      0 on success, error code otherwise
 *****************************************************************************/
int rpp_dpf_nf_gain(rpp_dpf_drv_t * drv,
        uint16_t * const    red,
        uint16_t * const    green_red,
        uint16_t * const    green_blue,
        uint16_t * const    blue
        );

/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_DPF_DRV_H__ */

