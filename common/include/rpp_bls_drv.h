/******************************************************************************
 *
 * Copyright 2019 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_bls_drv.h
 *
 * @brief   Interface of blacklevel measurment and substraction driver
 * 
 *****************************************************************************/
#ifndef __RPP_BLS_DRV_H__
#define __RPP_BLS_DRV_H__

#include "rpp_config.h"
#include "rpp_hdr_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppBlsDrv RPP - Black Level Substraction Driver
 * RPP - Black Level Substraction Driver
 * @{
 *****************************************************************************/

/**************************************************************************//**
 * @def RPP_BLS_MAX_WINDOWS
 * @brief Max number of measurment windows.
 *****************************************************************************/
 #define RPP_BLS_MAX_WINDOWS        ( 2u )

/**************************************************************************//**
 * @def RPP_BLS_MAX_EXPONENT
 * @brief Max exponent value to calculate number of samples to measure by
 *        the formula:@n  \f$n_{samples} = 2^{exponent}\f$
 *****************************************************************************/
 #define RPP_BLS_MAX_EXPONENT       ( 18u )

/**************************************************************************//**
 * @brief RPP BLS specific driver structure
 *
 * This structure encapsulates all data required by the RPP BLS driver to
 * operate a specific instance of the RPP BLS on a given RPPHDR device.
 * The data fields contained in this structure are intended to be
 * private to the driver and not to be modified by any higher-level
 * software component.
 *
 * It is required to have a separate instance of this structure for each
 * separate RPP BLS module in a given RPPHDR device. Each of these structures
 * must be initialized before use by calling the function @c rpp_bls_init()
 * with suitable parameters.
 *
 * After initialization, the @c colorbits member variable is set to the
 * number of bits per color value. This information is needed for the
 * interpretation of measured or configured black levels.
 *
 * The structure is exported here so that instances can statically be declared
 * by higher-level software without the need for dynamic memory allocation.
 *****************************************************************************/
typedef struct {
    uint16_t        version;
    uint16_t        colorbits;
    uint32_t        base;
    rpp_device *    dev;
} rpp_bls_drv_t;


/**************************************************************************//**
 * @brief Initialize the rpp bls driver.
 * @param[in]       dev     rpp_device structure to be used for all register accesses
 * @param[in]       base    base address of RPP-BLS module
 * @param[inout]    drv     BLS driver structure to be initialized
 * @return          0 on success, error-code otherwise
 *
 * This function initializes the @c drv structure for future uses of the driver
 * on the given instance of the rpp bls module. It fills the @c drv structure
 * with data required for operating on the given instance and caches data that
 * describes the features of the bls module instance.
 *
 * This function needs to be called once for every instance of the rpp bls
 * module to operate, each time with a different @c drv argument intended for
 * that very instance of the rpp bls module only.
 *****************************************************************************/
int rpp_bls_init(rpp_device * dev, uint32_t base, rpp_bls_drv_t * drv);

/**************************************************************************//**
 * @brief Enable or disable the rpp bls module.
 * @param[in]   drv     initialized BLS driver structure describing the bls module instance
 * @param[in]   on      enable/disable RPP BLS module (0: disable, otherwise enable)
 * @return      0 on success, error-code otherwise
 *
 * This function can be used to enable or disable blacklevel measurement
 * and blacklevel correction independently.
 *
 * @note When blacklevel measurement is enabled, suitable measurement values
 * must have been configured in advance. Otherwise the behaviour of the unit
 * is undefined.
 *
 * @note When blacklevel correction is enabled but measurement is
 * disabled, suitable fixed blacklevels must have been configured
 * in advance.
 *****************************************************************************/
int rpp_bls_enable(rpp_bls_drv_t * drv, const unsigned on);

/**************************************************************************//**
 * @brief Return enable state of the rpp bls module.
 *
 * @param[in]   drv     initialized BLS driver structure describing the bls module instance
 * @param[out]  on      current enable state (0: disable, otherwise enable)
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int rpp_bls_enabled(rpp_bls_drv_t * drv, unsigned * const on);

/**************************************************************************//**
 * @brief Setup fixed blacklevel values for a Bayer cluster in the rpp bls module.
 *
 * This function is used to set-up a set of fixed black levels, one value
 * for each component in the Bayer cluster, which are used by the unit
 * for blacklevel correction when blacklevel measurement is disabled.
 *
 * @note The fixed blacklevel depends on the position inside Bayer cluster.
 *       The driver control software has to take care of it. 
 *
 *       A bayer cluster is defined as follows:
 *        
 *         |       |       |
 *         | :---: | :---: |
 *         |   A   |   B   |
 *         |   C   |   D   |
 *
 *       Example when using RG/GB Bayer pattern: 
 *         * A is blacklevel for the red Bayer component
 *         * B is blacklevel for the green Bayer component in red lines
 *         * C is blacklevel for the green Bayer component in blue lines
 *         * D is blacklevel for the blue Bayer component
 *
 * @param[in]   drv     initialized BLS driver structure describing the bls module instance
 * @param[in]   a       black level of the Bayer component A
 * @param[in]   b       black level of the Bayer component B
 * @param[in]   c       black level of the Bayer component C
 * @param[in]   d       black level of the Bayer component D
 *
 * @note The valid range is from -(2^colorbits) to 2^(colorbits-1). Negative values
 *       are added to the intensity value.
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int rpp_bls_set_black_level
(
    rpp_bls_drv_t * drv, 
    const int32_t a,
    const int32_t b,
    const int32_t c,
    const int32_t d
);

/**************************************************************************//**
 * @brief Return the configured fixed blacklevel values. 
 *
 * @param[in]   drv     initialized BLS driver structure describing the bls module instance
 * @param[out]  a       current black level of the bayercomponent A
 * @param[out]  b       current black level of the bayercomponent B
 * @param[out]  c       current black level of the bayercomponent C
 * @param[out]  d       current black level of the bayercomponent D
 * @return      0 on success, error-code otherwise
 *
 * This function returns the fixed blacklevels that have been configured
 * in the unit beforehand.
 * @sa rpp_bls_set_black_level()
 *****************************************************************************/
int rpp_bls_get_black_level
(
    rpp_bls_drv_t * drv, 
    int32_t * const a,
    int32_t * const b,
    int32_t * const c,
    int32_t * const d
);

/**************************************************************************//**
 * @brief Return the measured black level values. 
 *
 * @param[in]   drv     initialized BLS driver structure describing the bls module instance
 * @param[out]  a       measured black level for the bayercomponent A
 * @param[out]  b       measured black level for the bayercomponent B
 * @param[out]  c       measured black level for the bayercomponent C
 * @param[out]  d       measured black level for the bayercomponent D
 *
 * @return      0 on success, error-code otherwise
 *
 * This function reads the measured black level values from the unit.
 * Meaningful values are only returned if blacklevel measurement has
 * actually been enabled and at least a single frame was processed
 * by the unit.
 *****************************************************************************/
int rpp_bls_get_measured_black_level
(
    rpp_bls_drv_t * drv, 
    int32_t * const a,
    int32_t * const b,
    int32_t * const c,
    int32_t * const d
);

/**************************************************************************//**
 * @brief Verify measurement windows and compute number of samples.
 *
 * @param[in]   num      number of measurement windows (max 2)
 * @param[in]   wnd      array of measurement windows
 * @param[out]  exponent exponent of the power of 2 (num_samples = 2^exponent)
 *
 * For each pixel in any of the measurement windows, blacklevel
 * measurement sums up the color sample values into an accumulator
 * corresponding to the component in the Bayer cluster.
 * For calculation of proper average values, the accumulated value
 * must be divided by the number of samples. To reduce the division
 * to a simple shift operation, the total number of pixels in all
 * measurement windows must sum up to a power-of-two.
 *
 * This function checks whether this condition is fulfilled and returns
 * an error code if not. Otherwise the power-of-two exponent that
 * needs to be programmed to the unit is returned in the @c exponent
 * parameter.
 *
 * @note: The window widths and heights must be even to cover a whole Bayer
 * cluster. Otherwise the average value would be inaccurate because all
 * accumulation results are divided by the same number. An error is
 * returned if this condition is not fulfilled.
 *
 * @note: This function needs no register access and can be called without
 * a pointer to the driver-specific data structure.
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int rpp_bls_verify_windows
(
    const unsigned          num,
    const rpp_window_t *    wnd,
    uint32_t * const        exponent 
);

/**************************************************************************//**
 * @brief Program measurement windows to the rpp bls module.
 * @param[in]   drv     initialized BLS driver structure describing the bls module instance
 * @param[in]   num         number of measurement windows
 * @param[in]   wnd         array of measurment windows
 * @param[in]   exponent    exponent of power of 2 (num_samples = 2^exponent)
 * @return      0 on success, error-code otherwise
 *
 * For each pixel in any of the measurement windows, blacklevel
 * measurement sums up the color sample values into an accumulator
 * corresponding to the component in the Bayer cluster.
 * For calculation of proper average values, the accumulated value
 * must be divided by the number of samples. To reduce the division
 * to a simple shift operation, the total number of pixels in all
 * measurement windows must sum up to a power-of-two.
 *
 * The function rpp_bls_verify_windows() must be used to verify that
 * this condition is fulfilled and to calculate the power-of-two
 * exponent that needs to be programmed to the unit.
 *****************************************************************************/
int rpp_bls_set_measurement_window
(
    rpp_bls_drv_t *         drv,
    const unsigned          num,
    const rpp_window_t *    wnd,
    const uint32_t          exponent
);

/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_BLS_DRV_H__ */

