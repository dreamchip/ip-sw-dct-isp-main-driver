/******************************************************************************
 *
 * Copyright 2019 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_gamma_in_drv.h
 *
 * @brief   Interface of degamma driver
 *
 *****************************************************************************/
#ifndef __RPP_GAMMA_IN_DRV_H__
#define __RPP_GAMMA_IN_DRV_H__

#include "rpp_config.h"

#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppGammaInDrv RPP - Degamma Driver
 * RPP - Degamma Driver
 * @{
 *
 * The sensor gamma-in correction is used to adapt the image signal processing to
 * the characteristics of the attached sensor device.
 *
 * This correction is typically a non-linear gradient, so this curve is represented
 * by piece-wise linear approximation. The whole range is divided into 16 sections
 * defined by x-interval dx1...16 and their begin and end values in y direction
 * y0...y16.
 *
 * @image html degamma_curve.png "Gamma Curve Definition" width=\textwidth
 * @image latex degamma_curve.png "Gamma Curve Definition" width=\textwidth
 *
 * Three gamma curves can be defined, one for each color component red, green, and
 * blue. The interval widths in x direction are to be defined in a 2^(value + offset) notation,
 * where "value" has to be written to the register and offset is either 4 for
 * instances with colorbits <= 20, or 8 for instances with colorbits >20.
 * This is illustrated by the following examples:
 *  -# 12bit data path (offset=4, value=0..7)
 *   - value = 0, dx = 2^(0 + 4) = 2^4 = 16
 *   - value = 1, dx = 2^(1 + 4) = 2^5 = 32
 *   - ...
 *   - value = 7, dx = 2^(7 + 4) = 2^11 = 2048
 *  -# 24bit data path (offset = 8, value = 0..15)
 *   - value = 0, dx = 2^(0 + 8) = 2^8 = 256
 *   - value = 1, dx = 2^(1 + 8) = 2^9 = 512
 *   - ...
 *   - value = 15, dx = 2^(15 + 8) = 2^23 = 8388608
 *
 * A typical application where de-gamma should be used is when a picture of a gray
 * step chart results in different colors of the gray patches. In this case a non-linear
 * characteristic of the sensor is present, which should be corrected by separately
 * adapted correction curves of the R, G and B channels.
 *
 * The gamma_in unit can also be used to perform a decompression of piecewise linear
 * (PWL) compressed input data.
 *
 *****************************************************************************/

/**************************************************************************//**
 * @brief Maximal number of curve points.
 *****************************************************************************/
#define RPP_GAMMA_IN_MAX_SAMPLES    ( 17u )

/**************************************************************************//**
 * @brief The type declares a gamma-in curve sample point.
 *****************************************************************************/
typedef struct
{
    uint32_t    x;      /**< x value */
    uint32_t    red;    /**< red value */
    uint32_t    green;  /**< green value */
    uint32_t    blue;   /**< blue value */
} rpp_gamma_in_curve_sample_t;

/**************************************************************************//**
 * @brief RPP GAMMA_IN specific driver structure
 *
 * This structure encapsulates all data required by the RPP GAMMA_IN driver to
 * operate a specific instance of the RPP GAMMA_IN unit on a given RPPHDR device.
 * The data fields contained in this structure are intended to be
 * private to the driver and not to be modified by any higher-level
 * software component.
 *
 * It is required to have a separate instance of this structure for each
 * separate RPP GAMMA_IN unit in a given RPPHDR device. Each of these structures
 * must be initialized before use by calling the function @c rpp_gamma_in_init()
 * with suitable parameters.
 *
 * After initialization, the @c colorbits member variable is set to the
 * number of bits per color value. This information is needed for the
 * preparation of the gamma curves.
 *
 * The structure is exported here so that instances can statically be declared
 * by higher-level software without the need for dynamic memory allocation.
 *****************************************************************************/
typedef struct {
    uint16_t        version;
    uint16_t        colorbits;
    uint32_t        base;
    rpp_device *    dev;
} rpp_gamma_in_drv_t;

/**************************************************************************//**
 * @brief Initialize the rpp gamma-in module.
 * @param[in]       dev     rpp_device structure to be used for all register accesses
 * @param[in]       base    base address of RPP GAMMA_IN unit
 * @param[inout]    drv     GAMMA_IN driver structure to be initialized
 * @return          0 on success, error-code otherwise
 *
 * This function initializes the @c drv structure for future uses of the driver
 * on the given instance of the rpp gamma_in unit. It fills the @c drv structure
 * with data required for operating on the given instance and caches data that
 * describes the features of the gamma_in unit instance.
 *
 * This function needs to be called once for every instance of the rpp gamma_in
 * unit to operate, each time with a different @c drv argument intended for
 * that very instance of the rpp gamma_in unit only.
 *****************************************************************************/
int rpp_gamma_in_init(rpp_device * dev, uint32_t base, rpp_gamma_in_drv_t * drv);

/**************************************************************************//**
 * @brief Enable or disable the rpp gamma-in module.
 * @param[in]   drv     initialized GAMMA_IN driver structure describing the gamma_in unit instance
 * @param[in]   on      0: disable, enable otherwise
 * @return      0 on success, error-code otherwise
 *
 * If the unit is disabled, image data flows through the unit without any
 * modifications. If enabled, a suitable gamma curve must have been programmed
 * in advance.
 *****************************************************************************/
int rpp_gamma_in_enable(rpp_gamma_in_drv_t * drv, const unsigned on);

/**************************************************************************//**
 * @brief Return enable state of the rpp gamma-in module.
 * @param[in]   drv     initialized GAMMA_IN driver structure describing the gamma_in unit instance
 * @param[out]  on      0: disabled, 1: enabled
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int rpp_gamma_in_enabled(rpp_gamma_in_drv_t * drv, unsigned * on);

/**************************************************************************//**
 * @brief Set de-gamma curves
 * @param[in]   drv     initialized GAMMA_IN driver structure describing the gamma_in unit instance
 * @param[in]   samples curve samples
 * @return      0 on success, error-code otherwise
 *
 * This function programs a new de-gamma curve into the unit. There are
 * restrictions on the values of the curve points that depend on the
 * number of @c colorbits reported for the unit instance:
 *  - the @c red, @c green and @c blue curve points must be smaller than 2^colorbits
 *  - the @c dx values must be 0..7 for 12bit instances, and 0..15 for
 *    20bit or 24bit instances.
 *  - the number of curve points to be specified is 17
 * If the data supplied does not match these criteria, the function returns
 * an error code.
 *****************************************************************************/
int rpp_gamma_in_set_curves(rpp_gamma_in_drv_t * drv,
                        const rpp_gamma_in_curve_sample_t * samples);

/**************************************************************************//**
 * @brief Read current de-gamma curves
 * @param[in]   drv     initialized GAMMA_IN driver structure describing the gamma_in unit instance
 * @param[out]  samples curve samples
 * @return      0 on success, error-code otherwise
 *
 * This function reads the gamma curves for each color component from the unit
 * and returns the data in the supplied @c samples argument. The @c samples
 * argument must point to enough reserved memory for 17 gamma curve samples.
 *****************************************************************************/
int rpp_gamma_in_curves(rpp_gamma_in_drv_t * drv,
                    rpp_gamma_in_curve_sample_t * const samples);

/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_GAMMA_IN_DRV_H__ */

