/******************************************************************************
 *
 * Copyright 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_out_drv.h
 *
 * @brief   Interface of RPP Human Vision Output pipeline unit driver.
 *
 *****************************************************************************/
#ifndef __RPP_OUT_DRV_H__
#define __RPP_OUT_DRV_H__

#include "rpp_config.h"
#include <rpp_hdr_types.h>

#include "rpp_gamma_out_drv.h"
#include "rpp_ccor_drv.h"
#include "rpp_outregs_drv.h"
#include "rpp_is_drv.h"
#include "rpp_out_if_drv.h"

#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppOutDrv RPP - Human Vision Output Pipeline Unit driver
 * @brief RPP - Human Vision Output Pipeline Unit Driver
 *
 * This section contains definitions and functions for the human vision
 * output pipeline stage of the RPP HDR.
 *
 * The of the human vision output pipeline stage consists of a number of of
 * sub-units. The driver-specific structures for this pipeline stage,
 * therefore, summarizes the driver-specific data structures for the
 * included sub-units.
 *
 * Besides a common initialization routine that initializes all sub-units
 * as required, some helper functions are provided that are intended to
 * simplify the task of selecting one of the possible output images and the
 * corresponding image format.
 * @{
 *****************************************************************************/


/**************************************************************************//**
 * @brief defines the base address of the RPP_OUT human vision output pipeline stage
 *
 * This macro defines the address offset of the RPP_OUT human vision output pipeline
 * stage in relation to the base address of the RPPHDR itself.
 *****************************************************************************/
#define RPP_OUT_OFFSET      0x00000000


/**************************************************************************//**
 * @brief RPP Human Vision Output specific driver structure
 *
 * This structure encapsulates all data required by the sub-units of the
 * RPP RPP_OUT human vision output pipeline stage.
 *****************************************************************************/
typedef struct {
    /** @brief driver-specific portion of the included GAMMA_OUT sub-unit */
    rpp_gamma_out_drv_t     gamma_out;
    /** @brief driver-specific portion of the included CCOR sub-unit */
    rpp_ccor_drv_t          ccor;
    /** @brief driver-specific portion of the included OUTREGS sub-unit */
    rpp_outregs_drv_t       outregs;
    /** @brief driver-specific portion of the included IS sub-unit */
    rpp_is_drv_t            is;
    /** @brief driver-specific portion of the included OUT_IF sub-unit */
    rpp_out_if_drv_t        out_if;
} rpp_out_drv_t;


/**************************************************************************//**
 * @brief Initialize the rpp_out pipeline stage.
 * @param[in]       dev     rpp_device structure to be used for all register accesses
 * @param[in]       base    base address of RPP OUT_IF module
 * @param[inout]    drv     RPP_OUT driver structure to be initialized
 * @return          0 on success, error-code otherwise
 *
 * This function initializes the @c drv structure for future uses of the drivers
 * for all sub-units included in the RPP_OUT pipeline stage. If any
 * initialization error occurs, the function returns an error code.
 *****************************************************************************/
int rpp_out_init(rpp_device * dev, uint32_t base, rpp_out_drv_t * drv);

/**************************************************************************//**
 * @brief Prepare the rpp_out pipeline stage for RGB output from the MAIN_POST pipeline.
 * @param[in]       drv     initialized RPP_OUT driver structure describing the rpp_out unit instance
 * @param[in]       curve   gamma curve to provide gamma-corrected sRGB output
 * @return          0 on success, error-code otherwise
 *
 * This function prepares the Human Vision Output pipeline for generating
 * RGB output with 8bit per component. The input multiplexer for the
 * Human Vision Output is configured to accept data from the MAIN_POST
 * pipeline stage, which means that the full ISP pipeline (pre-fusion stage,
 * radiance mapping, post-fusion stage) must have been set-up correctly.
 *
 * The @c curve argument specifies the mapping from linear RGB to sRGB and
 * is required.
 *****************************************************************************/
int rpp_out_set_isp_rgb(rpp_out_drv_t * drv, rpp_gamma_out_curve_t * curve);


/**************************************************************************//**
 * @brief Prepare the rpp_out pipeline stage for YUV output from the MAIN_POST pipeline.
 * @param[in]       drv         initialized RPP_OUT driver structure describing the rpp_out unit instance
 * @param[in]       curve       gamma curve to provide gamma-corrected sRGB output
 * @param[in]       rgb2yuv     a color space conversion profile for RGB to YUV conversion
 * @param[in]       legal       whether to output YUV legal range or full range
 * @param[in]       method_422  the selected YUV 4:4:4 to 4:2:2 subsampling method
 * @param[in]       output_420  whether to output YUV 4:2:0 with dummy lines instead of YUV 4:2:2
 * @return          0 on success, error-code otherwise
 *
 * This function prepares the Human Vision Output pipeline for generating
 * YUV output with 12bit per component. The input multiplexer for the
 * Human Vision Output is configured to accept data from the MAIN_POST
 * pipeline stage, which means that the full ISP pipeline (pre-fusion stage,
 * radiance mapping, post-fusion stage) must have been set-up correctly.
 *
 * The @c curve argument specifies the mapping from linear RGB to sRGB and
 * is required.
 *
 * The @c rgb2yuv argument is required and specifies a colorspace conversion
 * profile for a RGB to YUV conversion. When the \c legal parameter is non-zero,
 * the colorspace conversion matrix will be rescaled to produce YUV samples
 * in the legal range (a.k.a. "studio swing" or "video levels") and hardware
 * clipping is enabled.
 *
 * The @c method_422 parameter specifies the algorithm to employ for chroma
 * subsampling from 4:4:4 down to 4:2:2, which is always performed.
 *
 * The @c output_420 flag specifies, if non-zero, that the output shall be
 * interleaved YUV 4:2:0 where the chroma components U/V contain dummy values
 * in even output lines and have to be ignored.
 *****************************************************************************/
int rpp_out_set_isp_yuv(rpp_out_drv_t * drv,
        rpp_gamma_out_curve_t *     curve,
        rpp_ccor_profile_t *        rgb2yuv,
        unsigned                    legal,
        rpp_out_422_method_t        method_422,
        unsigned                    output_420
        );


/**************************************************************************//**
 * @brief Prepare the rpp_out pipeline stage for YUV output from PRE1 ACQ.
 * @param[in]       drv         initialized RPP_OUT driver structure describing the rpp_out unit instance
 * @param[in]       output_420  whether to output YUV 4:2:0 with dummy lines instead of YUV 4:2:2
 * @return          0 on success, error-code otherwise
 *
 * This function prepares the Human Vision Output pipeline for generating
 * YUV output with 12bit per component. The input multiplexer for the
 * Human Vision Output is configured to accept data from the ACQ in the
 * PRE_1 pre-fusion pipeline. The ACQ must have been configured
 * to read YUV data that is output on the RPP_lite port of the ACQ.
 *
 * The @c output_420 flag specifies, if non-zero, that the output shall be
 * interleaved YUV 4:2:0 where the chroma components U/V contain dummy values
 * in even output lines and have to be ignored.
 *****************************************************************************/
int rpp_out_set_pre1_yuv(rpp_out_drv_t * drv, unsigned output_420);

/**************************************************************************//**
 * @brief Prepare the rpp_out pipeline stage for YUV output from PRE2 ACQ.
 * @param[in]       drv         initialized RPP_OUT driver structure describing the rpp_out unit instance
 * @param[in]       output_420  whether to output YUV 4:2:0 with dummy lines instead of YUV 4:2:2
 * @return          0 on success, error-code otherwise
 *
 * This function prepares the Human Vision Output pipeline for generating
 * YUV output with 12bit per component. The input multiplexer for the
 * Human Vision Output is configured to accept data from the ACQ in the
 * PRE_2 pre-fusion pipeline. The ACQ must have been configured
 * to read YUV data that is output on the RPP_lite port of the ACQ.
 *
 * The @c output_420 flag specifies, if non-zero, that the output shall be
 * interleaved YUV 4:2:0 where the chroma components U/V contain dummy values
 * in even output lines and have to be ignored.
 *****************************************************************************/
int rpp_out_set_pre2_yuv(rpp_out_drv_t * drv, unsigned output_420);

/**************************************************************************//**
 * @brief Prepare the rpp_out pipeline stage for YUV output from PRE3 ACQ.
 * @param[in]       drv         initialized RPP_OUT driver structure describing the rpp_out unit instance
 * @param[in]       output_420  whether to output YUV 4:2:0 with dummy lines instead of YUV 4:2:2
 * @return          0 on success, error-code otherwise
 *
 * This function prepares the Human Vision Output pipeline for generating
 * YUV output with 12bit per component. The input multiplexer for the
 * Human Vision Output is configured to accept data from the ACQ in the
 * PRE_3 pre-fusion pipeline. The ACQ must have been configured
 * to read YUV data that is output on the RPP_lite port of the ACQ.
 *
 * The @c output_420 flag specifies, if non-zero, that the output shall be
 * interleaved YUV 4:2:0 where the chroma components U/V contain dummy values
 * in even output lines and have to be ignored.
 *****************************************************************************/
int rpp_out_set_pre3_yuv(rpp_out_drv_t * drv, unsigned output_420);

/**************************************************************************//**
 * @brief Enable the rpp_out pipeline stage to produce the given number of frames.
 * @param[in]   drv         initialized RPP_OUT driver structure describing the rpp_out unit instance
 * @param[in]   num_frames  number of frames to capture (0..1023)
 * @param[in]   data_mode   whether the output is of type DATA or VIDEO
 *
 * This function enables the Human Vision Output to capture the given number
 * of frames and to produce the indicated output format.
 *
 * If the @c num_frames parameter is zero, the unit will produce an
 * infinite number of frames (e.g. run until it is explicitly stopped
 * again). Otherwise the unit will turn itself off again after having
 * produced the given number of output frames.
 *****************************************************************************/
int rpp_out_enable(rpp_out_drv_t * drv, unsigned num_frames, unsigned data_mode);

/**************************************************************************//**
 * @brief Explicitly disable the rpp_out pipeline stage.
 * @param[in]   drv         initialized RPP_OUT driver structure describing the rpp_out unit instance
 *
 * This function is used to stop the Human Vision Output when it is no
 * longer needed.
 *****************************************************************************/
int rpp_out_disable(rpp_out_drv_t * drv);

/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_OUT_DRV_H__ */

