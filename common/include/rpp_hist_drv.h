/******************************************************************************
 *
 * Copyright 2019 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_hist_drv.h
 *
 * @brief   Interface of histogram measurment driver
 *
 *****************************************************************************/
#ifndef __RPP_HIST_DRV_H__
#define __RPP_HIST_DRV_H__

#include "rpp_config.h"
#include <rpp_hdr_types.h>

#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppHistDrv RPP - Histogram Measurement Driver
 * RPP - Histogram Measurement Driver
 *
 * A histogram measurement unit appears in pre-fusion
 * pipeline stages as well as in the post-fusion stage.
 * The possible channels (input data sources) for which histograms can be
 * calculated depend on the position of the histogram
 * unit and are described by the @c rpp_hist_measure_channel_e enumerations.
 *
 * There are 32 histogram bins (20 bit each) which can hold
 * an accumulation result of up to 2^20 - 1.
 *
 * Each histogram measurement module is preceded by capture module which executes a
 * pre-selection of the samples to be measured. These pre-selection modes are
 * supported:
 *
 * - Subsampling:
 *   - The parameter v_stepsize is a 7 bit vertical offset between lines to be
 *     considered for measurement. Value 0 is not allowed. In bayer domain the
 *     programmed value is internally multiplied by 2.
 *   - The parameter h_step_inc is 17 bit counter increment. The counter is
 *     incremented by h_step_inc with each input pixel. A sample is considered
 *     for measurement if the accumulation result is greater than 2^16. The
 *     counter value is then corrected by a value of -2^16. A programmed value of
 *     2^16 marks all pixels valid.
 * - Weighted windowing: @n
 *   A grid of 5x5 measurement windows is used, where x offset, y offset and
 *   window size are independently programmable. All 25 windows must be inside
 *   the margins of the sensor array. A set of weights, one for each subwindow,
 *   can be individually set per window in the range 0...16. Weight in this context
 *   means that the respective bin is not incremented by 1, but by the weight
 *   value. By setting a weight to 0 the respective window values are not
 *   considered.
 * - Linear Combination: @n
 *   The histogram is calculated for a single color component or for a linear
 *   combination of the color components which only makes sense in RGB
 *   domain. Linear combination is calculated using a color conversion vector
 *   that is multiplied with the input pixel color components and, therefore,
 *   arbitrarily programmable.
 * - Offset/Shift Logic: @n
 *   An offset/shift logic allows a refinement of the histogram to special
 *   dynamic ranges of interest using the register fields sample_offset and
 *   sample_shift. There is a sample offset which is subtracted from the
 *   measurement value before applying a left shift:
 *   @code
 *   S_out = (S_in – sample_offset) << sample_shift
 *   @endcode
 *   With this logic the histogram can focus on a special dynamic range inside
 *   the complete dynamic range of the input sample.
 *
 * @note All configuration of the unit is subject to register
 * shadowing and will not have an immediate effect. Any configuration
 * programmed to the unit will be activated synchronized with an
 * end-of-frame signal when the shadow register update has been
 * requested in the top-level RPP unit register.
 * The currently active configuration cannot be retrieved from
 * the unit.
 * @{
 *
 *****************************************************************************/

/**************************************************************************//**
 * @brief This definition defines the number of coefficients of the color
 *        space conversion in the histogram module.
 *****************************************************************************/
#define RPP_HIST_COEFF_NUM  ( 3u )

/**************************************************************************//**
 * @brief This definition defines the number of measurment bins in the
 *        histogram module.
 *****************************************************************************/
#define RPP_HIST_BIN_NUM    ( 32u )

/**************************************************************************//**
 * @brief Macro to define the number of weights in the histogram unit.
 *
 * A weight is a associated with each of the 5x5 subwindows in the overall
 * observation window in which the histogram is measured. The weights allow
 * a definition of spatial regions of interest.
 *
 * The number of weights is equal to the number of subwindows.
 *****************************************************************************/
#define RPP_HIST_WEIGHT_NUM ( 5u * 5u )

/**************************************************************************//**
 * @brief Macro to define the maximum possible weight for a single sub-window.
 *
 * This macro defines the maximum possible weight for a single subwindow of
 * the overall observation window. By default, without defining spatial
 * regions of interest, all weights are 1.
 */
#define RPP_HIST_MAX_WEIGHT 16

/**************************************************************************//**
 * @brief Datatype defining a Color Conversion Vector.
 *
 * This type declares a color conversion vector with coefficients that
 * convert an incoming RGB tuple to a single luminance value that is
 * finally used in the histogram calculation according to the following
 * formula:
 * @f[
 * Y = \left(\begin{array}{ccc} C0 & C1 & C2 \end{array}\right) \cdot
 * \left(\begin{array}{c} R \\ G \\ B \end{array}\right)
 * @f]
 *
 *
 * @note To calculate the histogram over one of the color components of the
 * RGB pixel, set the corresponding coefficient to 1.0 an all others to zero.
 *****************************************************************************/
typedef uint32_t rpp_hist_csm_t[RPP_HIST_COEFF_NUM];

/**************************************************************************//**
 * @brief This type declares the storage for calculated histogram values.
 *
 * The histogram values range from 0 to (2^20 - 1) and are to be interpreted
 * as an unsigned FP16.4 fixed point value, e.g. having a 16bit integer value
 * and 4 fractional bits. Conversion to floating point numbers is achieved by
 * dividing the number by 16.
 *****************************************************************************/
typedef uint32_t rpp_hist_bins_t[RPP_HIST_BIN_NUM];

/**************************************************************************//**
 * @brief This type declares the storage of the histogram subwindow weights.
 *
 * This datatype declares an array of 25 elements, one for each of the 25
 * subwindows of the configured measurement window. Each of the weights
 * must be in the range 0..RPP_HIST_MAX_WEIGHT.
 *****************************************************************************/
typedef uint8_t rpp_hist_weights_t[RPP_HIST_WEIGHT_NUM];

/**************************************************************************//**
 * @def SET_HIST_WEIGHTS(m,v)
 * Macro to set a all weights to same value.
 *****************************************************************************/
#define SET_HIST_WEIGHTS(m,v)               \
{                                           \
    unsigned i;                             \
    for (i=0u; i<RPP_HIST_WEIGHT_NUM; i++)  \
    {                                       \
        m[i] = v;                           \
    }                                       \
}

/**************************************************************************//**
 * @def SET_HIST_CSM_COEFF(m,i,v)
 * Macro to set a color conversion coefficient <i>i</i>
 * in a color conversion matrix (@ref rpp_hist_csm_t).
 *****************************************************************************/
#define SET_HIST_CSM_COEFF(m,i,v)    ( m[i] = (v) )

/**************************************************************************//**
 * @def GET_HIST_CSM_COEFF(m,i)
 * Macro to get a color conversion coefficient <i>i</i>
 * from a color conversion matrix (@ref rpp_hist_csm_t).
 *****************************************************************************/
#define GET_HIST_CSM_COEFF(m,i)      ( m[i] )

/**************************************************************************//**
 * @def SET_HIST_RGB_2_R(f)
 * This Macro defines conversion coefficients for a histogram measurement
 * of red component. Following formula shows this conversion.
 *
 * \f[
 *      Y_{OUT}
 *      =
 *      \begin{bmatrix}
 *           1.0 &  0.0 &  0.0 \\
 *      \end{bmatrix}
 *      *
 *      \begin{bmatrix} R_{IN}  \\ G_{IN}  \\ B_{IN}  \end{bmatrix}
 * \f]
 *
 * @note: Keep balancing by making sure the coefficient sum is 1.
 *****************************************************************************/
#define SET_HIST_RGB_2_R(f)         \
{                                   \
    SET_HIST_CSM_COEFF(f, 0, 128 ); \
    SET_HIST_CSM_COEFF(f, 1,   0 ); \
    SET_HIST_CSM_COEFF(f, 2,   0 ); \
}

/**************************************************************************//**
 * @def SET_HIST_RGB_2_G(f)
 * This Macro defines conversion coefficients for a histogram measurement
 * of green component. Following formula shows this conversion.
 *
 * \f[
 *      Y_{OUT}
 *      =
 *      \begin{bmatrix}
 *           0.0 &  1.0 &  0.0 \\
 *      \end{bmatrix}
 *      *
 *      \begin{bmatrix} R_{IN}  \\ G_{IN}  \\ B_{IN}  \end{bmatrix}
 * \f]
 *
 * @note: Keep balancing by making sure the coefficient sum is 1.
 *****************************************************************************/
#define SET_HIST_RGB_2_G(f)         \
{                                   \
    SET_HIST_CSM_COEFF(f, 0,   0 ); \
    SET_HIST_CSM_COEFF(f, 1, 128 ); \
    SET_HIST_CSM_COEFF(f, 2,   0 ); \
}

/**************************************************************************//**
 * @def SET_HIST_RGB_2_B(f)
 * This Macro defines conversion coefficients for a histogram measurement
 * of green component. Following formula shows this conversion.
 *
 * \f[
 *      Y_{OUT}
 *      =
 *      \begin{bmatrix}
 *           0.0 &  0.0 &  1.0 \\
 *      \end{bmatrix}
 *      *
 *      \begin{bmatrix} R_{IN}  \\ G_{IN}  \\ B_{IN}  \end{bmatrix}
 * \f]
 *
 * @note: Keep balancing by making sure the coefficient sum is 1.
 *****************************************************************************/
#define SET_HIST_RGB_2_B(f)         \
{                                   \
    SET_HIST_CSM_COEFF(f, 0,   0 ); \
    SET_HIST_CSM_COEFF(f, 1,   0 ); \
    SET_HIST_CSM_COEFF(f, 2, 128 ); \
}

/**************************************************************************//**
 * @def SET_HIST_BT601_RGB_2_Y(f)
 * This Macro defines conversion coefficients for a BT.601 RGB
 * to YUV conversion. Following formula shows this conversion.
 *
 * \f[
 *      Y_{OUT}
 *      =
 *      \begin{bmatrix}
 *           0.2990 &  0.5870 &  0.1140 \\
 *      \end{bmatrix}
 *      *
 *      \begin{bmatrix} R_{IN}  \\ G_{IN}  \\ B_{IN}  \end{bmatrix}
 * \f]
 *
 * @note: Keep balancing by making sure the coefficient sum is 1.
 *****************************************************************************/
#define SET_HIST_BT601_RGB_2_Y(f)   \
{                                   \
    SET_HIST_CSM_COEFF(f, 0,  38 ); \
    SET_HIST_CSM_COEFF(f, 1,  75 ); \
    SET_HIST_CSM_COEFF(f, 2,  15 ); \
}

/**************************************************************************//**
 * @def SET_HIST_REC709_RGB_2_Y(f)
 * This Macro defines conversion coefficients for a BT.601 RGB
 * to YUV conversion. Following formula shows this conversion.
 *
 * \f[
 *      Y_{OUT}
 *      =
 *      \begin{bmatrix}
 *           0.2126 &  0.7152 &  0.0722 \\
 *      \end{bmatrix}
 *      *
 *      \begin{bmatrix} R_{IN}  \\ G_{IN}  \\ B_{IN}  \end{bmatrix}
 * \f]

 * @note: Keep balancing by making sure the coefficient sum is 1.
 *****************************************************************************/
#define SET_HIST_REC709_RGB_2_Y(f)  \
{                                   \
    SET_HIST_CSM_COEFF(f, 0, 27);   \
    SET_HIST_CSM_COEFF(f, 1, 92);   \
    SET_HIST_CSM_COEFF(f, 2,  9);   \
}

/**************************************************************************//**
 * @brief RPP histogram measurement modes
 *****************************************************************************/
enum rpp_hist_measure_mode_e
{
    RPP_HIST_MEASURE_MODE_DISABLE  = 0,    /**< disable histogram measurment */
    RPP_HIST_MEASURE_MODE_Y        = 1,    /**< converted to Y by using coefficients (red, green and blue) */
    RPP_HIST_MEASURE_MODE_BAYER_R  = 2,    /**< measure red bayer component */
    RPP_HIST_MEASURE_MODE_BAYER_GR = 3,    /**< measure green bayer component in red lines */
    RPP_HIST_MEASURE_MODE_BAYER_B  = 4,    /**< measure blue bayer component */
    RPP_HIST_MEASURE_MODE_BAYER_GB = 5,    /**< measure green bayer component in blue lines */
    RPP_HIST_MEASURE_MODE_MAX,
};

/**************************************************************************//**
 * @brief RPP histogram channel (measurement/tap point in pipeline)
 *****************************************************************************/
enum rpp_hist_measure_channel_e
{
    /** @brief measurement after ACQ (acquisition).
     * This setting is allowed for PRE-FUSION pipelines only. */
    RPP_HIST_MEASURE_CHANNEL_0 = 0,
    /** @brief measurement after HIST (blacklevel correction).
     * This setting is allowed for PRE-FUSION pipelines only. */
    RPP_HIST_MEASURE_CHANNEL_1 = 1,
    /** @brief measurement after GAMMA_IN (de-gamma/input linearization correction).
     * This setting is allowed for PRE-FUSION pipelines only. */
    RPP_HIST_MEASURE_CHANNEL_2 = 2,
    /** @brief measurement after LSC4 (lense-shade correction).
     * This setting is allowed for PRE-FUSION pipelines only. */
    RPP_HIST_MEASURE_CHANNEL_3 = 3,
    /** @brief measurement after AWB_GAIN (wb gain correction).
     * This setting is allowed for PRE-FUSION pipelines as well as
     * for the POST-FUSION pipeline. */
    RPP_HIST_MEASURE_CHANNEL_4 = 4,
    /** @brief measurement after DPCC (defect pixel correction).
     * This setting is allowed for PRE-FUSION pipelines only. */
    RPP_HIST_MEASURE_CHANNEL_5 = 5,
    /** @brief measurement after DPF (bilateral filtering).
     * This setting is allowed for PRE-FUSION pipelines only. */
    RPP_HIST_MEASURE_CHANNEL_6 = 6,
    /** @brief measurement after FILT (demosaicing).
     * This setting is allowed for the POST-FUSION pipeline only. */
    RPP_HIST_MEASURE_CHANNEL_7 = 7,
    RPP_HIST_MEASURE_CHANNEL_MAX,
};

/**************************************************************************//**
 * @brief This type defines the RPP histogram sub-sample configuration.
 * The sub-sample configuration consists of 2 step-sizes. One in vertical
 * direction which depends on the selected mode (@see rpp_hist_measure_mode_e).
 *
 * If mode=RPP_MEASURE_MODE_Y:
 *  -# v_step_inc=1 => process every input line
 *  -# v_step_inc=2 => precess every second input line
 *  -# v_step_inc=3 => precess every third input line
 *  ...
 * If mode=RPP_MEASURE_MODE_BAYER_XXX
 *  -# v_step_inc=1 => process every second input line
 *  -# v_step_inc=2 => process every forth input line
 *  -# v_step_inc=3 => process every sixth input line
 *  ...
 *
 * The horizontal step increment is used for horizontal sub-sampling. To
 * process every valid input pixel h_step_inc has to be set to 2^16. This
 * means the value is a fix-point number in format Q1.16.
 *
 * If mode=RPP_MEASURE_MODE_Y:
 *  h_step_inc=2^16         => process every input pixel
 *  h_step_inc=2^15         => process every second input pixel
 *  h_step_inc=ceil(2^16/3) => process every third input pixel
 *  h_step_inc=2^14         => process every forth input pixel
 *  ...
 *
 * If mode=RPP_MEASURE_MODE_XXX:
 *  h_step_inc=2^16         => process every "valid" input pixel
 *  h_step_inc=2^15         => process every second "valid" input pixel
 *  h_step_inc=ceil(2^16/3) => process every third "valid" input pixel
 *  h_step_inc=2^14         => process every forth "valid" input pixel
 *  ...
 *  "valid" input pixel means an intensity value of the selected bayer
 *  component. Unselected bayer componentens are skipped.
 *
 * @note Sub-sampling is used to avoid bin overruns in the measurement
 *       module. The max bin counter value is 2^20-1.
 *****************************************************************************/
typedef struct rpp_hist_step_s
{
    uint32_t v_stepsize;     /**< vertical step size */
    uint32_t h_step_inc;    /**< horizontal step increment */
} rpp_hist_step_t;


/**************************************************************************//**
 * @brief RPP HIST specific driver structure
 *
 * This structure encapsulates all data required by the RPP HIST driver to
 * operate a specific instance of the RPP HIST on a given RPPHDR device.
 * The data fields contained in this structure are intended to be
 * private to the driver and not to be modified by any higher-level
 * software component.
 *
 * It is required to have a separate instance of this structure for each
 * separate RPP HIST module in a given RPPHDR device. Each of these structures
 * must be initialized before use by calling the function @c rpp_hist_init()
 * with suitable parameters.
 *
 * After initialization, the @c colorbits member variable is set to the
 * number of bits per color value. This information is needed for the
 * the configuration of the offset when configuring the dynamic range of
 * interest (@sa set_sample_range() ).
 *
 * The structure is exported here so that instances can statically be declared
 * by higher-level software without the need for dynamic memory allocation.
 *****************************************************************************/
typedef struct {
    uint16_t        version;
    uint16_t        colorbits;
    uint32_t        base;
    rpp_device *    dev;
} rpp_hist_drv_t;


/**************************************************************************//**
 * @brief Initialize the rpp hist module.
 * @param[in]       dev     rpp_device structure to be used for all register accesses
 * @param[in]       base    base address of RPP HIST module
 * @param[inout]    drv     HIST driver structure to be initialized
 * @return          0 on success, error code otherwise
 *
 * This function initializes the @c drv structure for future uses of the driver
 * on the given instance of the rpp hist module. It fills the @c drv structure
 * with data required for operating on the given instance and caches data that
 * describes the features of the hist module instance.
 *
 * This function needs to be called once for every instance of the rpp hist
 * module to operate, each time with a different @c drv argument intended for
 * that very instance of the rpp hist unit only.
 *****************************************************************************/
int rpp_hist_init(rpp_device * dev, uint32_t base, rpp_hist_drv_t * drv);

/**************************************************************************//**
 * @brief Disable the rpp hist module.
 * @param[in]   drv     initialized HIST driver structure describing the hist module instance
 * @return      0 on success, error code otherwise
 *
 * This function disables the hist unit and prevents further histogram data
 * from being calculated.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_hist_disable(rpp_hist_drv_t * drv);

/**************************************************************************//**
 * @brief Program the measurement mode to the rpp hist module.
 * @param[in]   drv     initialized HIST driver structure describing the hist module instance
 * @param[in]   mode    measurement mode to set (@see rpp_hist_measure_mode_e)
 * @param[in]   channel input data source to configure (@see rpp_hist_measure_channel_e)
 * @return      0 on success, error code otherwise
 *
 * This function enables or disables histogram measurements.
 * If @c mode equals RPP_HIST_MEASURE_MODE_DISABLE, measurements are disabled.
 *
 * Otherwise histogram measurements are performed on the selected
 * @c channel (valid values for which depend on the position of the unit in
 * the pipeline) and the selected measurement mode.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_hist_set_mode(rpp_hist_drv_t * drv,
        const unsigned mode, const unsigned channel);

/**************************************************************************//**
 * @brief Retrieve the current measurement mode from the rpp hist module.
 * @param[in]   drv     initialized HIST driver structure describing the hist module instance
 * @param[out]  mode    current measurement mode (@see rpp_hist_measure_mode_e)
 * @param[out]  channel current input data source selected (@see rpp_hist_measure_channel_e)
 * @return      0 on success, error code otherwise
 *
 * This function returns the currently configured operation mode
 * of the unit along with the selected input data source.
 *****************************************************************************/
int rpp_hist_mode(rpp_hist_drv_t * drv,
        unsigned * const mode, unsigned * const channel);

/**************************************************************************//**
 * @brief Program Y color conversion coefficients to the rpp hist module.
 * @param[in]   drv     initialized HIST driver structure describing the hist module instance
 * @param[in]   csm     color conversion coefficients
 * @return      0 on success, error code otherwise
 *
 * This allows configuration of a color conversion vector that is
 * used to calculate a luminance value from an input of 3-component
 * RGB pixels. This conversion vector is only used when the operation
 * mode is set to RPP_HIST_MEASURE_MODE_Y. In case of Bayer input data, one of
 * the elements of the Bayer cluster can be selected for histogram
 * measurements.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_hist_set_csm(rpp_hist_drv_t * drv, const rpp_hist_csm_t csm);

/**************************************************************************//**
 * @brief Retrieve Y color conversion coefficient from the rpp hist module.
 * @param[in]   drv     initialized HIST driver structure describing the hist module instance
 * @param[out]  csm     current color conversion coefficients (@see rpp_hist_csm_t)
 * @return      0 on success, error code otherwise
 *
 * This function retrieves the color conversion coefficients currently
 * programmed into the unit and returns them in the supplied data structure.
 *****************************************************************************/
int rpp_hist_csm(rpp_hist_drv_t * drv, rpp_hist_csm_t csm);

/**************************************************************************//**
 * @brief Set sample range in rpp hist module.
 * @param[in]   drv     initialized HIST driver structure describing the hist module instance
 * @param[in]   shift   sample shift
 * @param[in]   offset  substraction offset
 * @return      0 on success, error code otherwise
 *
 * This function sets the offset and shift parameters that can be used
 * to define a dynamic region of interest.
 * The @c shift parameter must be in the range 0..(colorbits-5). This
 * restriction guarantees that at least the bin resolution (32 bins,
 * e.g. 5 bits) remains from the color value after left shifting.
 *
 * The offset parameter is a color value and
 * depends on the color bitwidth of the pipeline stage where the unit is
 * embedded, and can be determined from the @c colorbits member variable
 * in @c drv.
 *
 * The following formula describes this how the shift and offset parameters
 * are applied to the data under consideration:
 * \f[
 *      sample_{HIST}
 *      =
 *      (sample_{IN} - offset) << shift
 * \f]
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_hist_set_sample_range(rpp_hist_drv_t * drv,
        const unsigned shift, const uint32_t offset);

/**************************************************************************//**
 * @brief Retrieve sample range from the rpp hist module.
 * @param[in]   drv     initialized HIST driver structure describing the hist module instance
 * @param[out]  shift   sample shift
 * @param[out]  offset  substraction offset
 * @return      0 on success, error code otherwise
 *
 * This function retrieves the currently configured offset and shift
 * parameters that define a dynamic region of interest from the unit
 * and returns them.
 *****************************************************************************/
int rpp_hist_sample_range(rpp_hist_drv_t * drv,
        unsigned * const shift, uint32_t * const offset);

/**************************************************************************//**
 * @brief This sets the measurement window and subsampling options of the rpp hist module.
 * @param[in]   drv     initialized HIST driver structure describing the hist module instance
 * @param[in]   win     measurement window to program (@see rpp_window_t)
 * @param[in]   steps   subsampling configuration to program (@see rpp_hist_step_t)
 * @return      0 on success, error code otherwise
 *
 * This function defines the observation window for histogram measurements
 * as well as the horizontal and vertical pixel skipping parameters for
 * the preselection logic.
 *
 * The window geometry specified here is divided into a grid of 5x5 sub-windows,
 * which requires the window width and height each to be an integer multiple of 5.
 * Different weights for each sub-window can be specified, allowing to define
 * spatial regions of interest.
 *
 * The @c steps parameter defines rules for horizontal and vertical subsampling
 * of pixels.
 *
 * @c v_stepsize is a number between 1 and 255 that indicates which lines to select
 * for measurement, e.g.
 *  - v_stepsize == 1: measure every line
 *  - v_stepsize == 2: measure every second line
 *  - ...
 *
 * @c h_step_inc defines a value that is added to an internal 16bit counter
 * for each pixel inside the measurement window. The pixel that makes the
 * counter overflow is selected for histogram measurement and 2^16 is
 * subtracted from the counter again. If the counter does not overflow,
 * the pixel is skipped.
 * 
 * Therefore, setting @c h_step_inc to a value of 2^16 selects every
 * horizontal pixel inside the measurement window, a value of 2^15 every
 * second and so on. In other words, @c h_step_inc can be considered
 * a FP1.16 floating point value.
 *
 * @note In case of Bayer data measurements, the preselection logic operates on
 * Bayer clusters instead of pixels and lines, which means that every
 * (2 * @c v_stepsize)-th line is included in measurements, and @c h_step_inc
 * is added to the internal counter for every occurrence of the selected Bayer
 * component only.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_hist_set_meas_window( rpp_hist_drv_t * drv,
        const rpp_window_t * win, const rpp_hist_step_t * steps);

/**************************************************************************//**
 * @brief Retrieves current measurment window and subsampling options from the rpp hist module.
 * @param[in]   drv     initialized HIST driver structure describing the hist module instance
 * @param[out]  win     current measurment window (@see rpp_window_t)
 * @param[out]  steps   current subsampling parameters (@see rpp_hist_step_t)
 * @return      0 on success, error code otherwise
 *
 * This function retrieves the currently configured measurement window and subsampling
 * parameters.
 *****************************************************************************/
int rpp_hist_meas_window(rpp_hist_drv_t * drv,
        rpp_window_t * const win, rpp_hist_step_t * const steps);

/**************************************************************************//**
 * @brief Configures the measurement weights for the 25 measurement subwindows.
 * @param[in]   drv     initialized HIST driver structure describing the hist module instance
 * @param[in]   weights weights to set (@see rpp_hist_weights_t)
 * @return      0 on success, error code otherwise
 *
 * This function is used to configure the measurement subwindow weights
 * to be used by the unit.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_hist_set_weights(rpp_hist_drv_t * drv, const rpp_hist_weights_t weights);

/**************************************************************************//**
 * @brief Return the current weights from the rpp histogram module.
 * @param[in]   drv     initialized HIST driver structure describing the hist module instance
 * @param[out]  weights current weights (@see rpp_hist_weights_t)
 * @return      0 on success, error code otherwise
 *****************************************************************************/
int rpp_hist_weights(rpp_hist_drv_t * drv, rpp_hist_weights_t weights);

/**************************************************************************//**
 * @brief Return the measured histogram from rpp hist module.
 * @param[in]   drv     initialized HIST driver structure describing the hist module instance
 * @param[out]  bins    current histogram (@see rpp_hist_bins_t)
 * @return      0 on success, error code otherwise
 *****************************************************************************/
int rpp_hist_hist(rpp_hist_drv_t * drv, rpp_hist_bins_t bins);

/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_HIST_DRV_H__ */

