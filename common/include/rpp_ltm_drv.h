/******************************************************************************
 *
 * Copyright 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_ltm_drv.h
 *
 * @brief   Interface of Local Tone Mapping unit driver
 *
 *****************************************************************************/
#ifndef __RPP_LTM_DRV_H__
#define __RPP_LTM_DRV_H__

#include "rpp_config.h"
#include <rpp_hdr_types.h>

#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppLtmDrv RPP - Local Tone Mapping Unit Driver
 * RPP - Local Tone Mapping Unit Driver
 *
 * The Local Tone Mapping unit maps the high color resolutions of
 * HDR images to smaller color resolutions of rendering devices. By
 * taking the darkest and brightest areas of the HDR images into account
 * locally and globally, all image details are preserved  while the global
 * image contrast is kept as high as possible. Therefore, no image
 * information is lost in the finally displayed images while they still
 * look natural.
 *
 * The unit employs both, a Global Tone Mapping procedure and a Local Tone
 * Mapping procedure. The Global Mapping applies a piece-wise linear tone
 * curve of 48 segments to the incoming data, using it to map 24-bit
 * luminance input values to 12-bit luminance output values. The curve can
 * be calculated with the help of statistical parameters retrieved from the
 * @ref RppLtmMeasDrv "ltm_meas" unit.
 *
 * The Local Tone Mapping algorithm modifies the Global Tone Mapping
 * with a local operator to preserve the global balance between bright
 * and dim areas while maintaining the perceived local contrast.
 *
 * Besides the global tone mapping curve, the unit must be programmed with
 * a variety of numerical parameters that must be determined from
 * LTM measurement results and a few free parameters.
 *
 * @note It is important that the correct image width is specified
 * before this unit is being enabled. This can be done
 * using the function rpp_ltm_set_linewidth().
 *
 * @note All configuration of the unit is subject to register
 * shadowing and will not have an immediate effect. Any configuration
 * programmed to the unit will be activated synchronized with an
 * end-of-frame signal when the shadow register update has been
 * requested in the top-level RPP unit register.
 * The currently active configuration cannot be retrieved from
 * the unit.
 * @{
 *
 *****************************************************************************/

/**************************************************************************//**
 * @brief Maximal number of curve points.
 *****************************************************************************/
#define RPP_LTM_MAX_CURVE_POINTS    ( 49u )

/**************************************************************************//**
 * @brief This type declares a global tone mapping curve.
 *
 * This structure is intended to hold all required parameters specifying
 * the piece-wise linear definition of the tone mapping curve for the
 * rpp_ltm unit.
 *
 * The dx array holds the shift offsets used to compute the x-axis segment
 * sizes and are allowed to be in the range between 0 and 15. Here, the
 * \c dx[0] value is required to be zero and is ignored when programming
 * the unit.
 *
 * The y array defines the curve by specifying the y-axis values at the
 * borders of the x-axis segments. The first value \c y[0] specifies the start
 * of the curve at \c x=0. All values must be between 0 and
 * \c 2<sup>outbitwidth</sup>-1.
 *
 *****************************************************************************/
typedef struct
{
    /** @brief Array of 49 elements holding the definition of the X segment sizes.
     *
     * The first element of this array must be zero and is ignored when programming
     * the unit registers. The other \c dx[i] elements in this array represent shift
     * bit counts used for defining the width of the i-th x axis segment as described in
     * the documentation.
     */
    uint8_t     dx[RPP_LTM_MAX_CURVE_POINTS];
    /** @brief array of 49 y-axis coordinate elements defining the tone mapping curve */
    uint32_t    y [RPP_LTM_MAX_CURVE_POINTS];
} rpp_ltm_curve_t;

/**************************************************************************//**
 * @brief This type declares necessary numerical parameters for local tone mapping.
 *
 * This data structure holds all the numerical parameters that influcence the
 * local tone mapping algorithm.
 *****************************************************************************/
typedef struct {
    /** @brief L0w constant for the local tonemapping algorithm in 0.20 fixed point representation,  valid range [1/4095, 1.0) */
    uint32_t    L0w;
    /** @brief reciprocal of the L0w constant for the local tonemapping algorithm in 12.10 fixed point representation, valid range [0.0, 4096.0) */
    uint32_t    L0w_r;
    /** @brief L0d constant for the local tonemapping algorithm in 0.20 fixed point representation, valid range [1/4095, 1.0) */
    uint32_t    L0d;
    /** @brief reciprocal of the L0d constant for the local tonemapping algorithm in 12.10 fixed point representation, valid range [0.0, 4096.0) */
    uint32_t    L0d_r;
    /** @brief kmd constant for the local tonemapping algorithm in 4.12 fixed point representation, valid range [2.322, 13.999] */
    uint16_t    kmd;
    /** @brief kMd constant for the local tonemapping algorithm in 4.12 fixed point representation, valid range [2.585, 14.322] */
    uint16_t    kMd;
    /** @brief kdDiff constant for the local tonemapping algorithm in 0.12 fixed point representation, valid range [0.263, 0.332] */
    uint16_t    kdDiff;
    /** @brief reciprocal of the kdDiff constant for the local tonemapping algorithm in 2.12 fixed point representation, valid range [3.012, 3.802] */
    uint16_t    kdDiff_r;
    /** @brief kw constant for the local tonemapping algorithm in 4.12 fixed point representation, valid range [1.0, 12.0] */
    uint16_t    kw;
    /** @brief reciprocal of the kw constant for the local tonemapping algorithm in 0.16 fixed point representation, valid range [1/12, 1.0) */
    uint16_t    kw_r;
    /** @brief contrast gain for the local tonemapping algorithm in 0.16 fixed point representation, valid range [0.0, 1.0), default is 0.1 (= 0x199A) */
    uint16_t    cgain;
    /** @brief reciprocal of the 99.9% percentile of the luma image in 0.48 fixed point representation, valid range [0.0, 1.0) */
    uint64_t    LprcH_r;
} rpp_ltm_param_t;

/**************************************************************************//**
 * @brief RPP LTM specific driver structure
 *
 * This structure encapsulates all data required by the RPP LTM driver to
 * operate a specific instance of the RPP LTM on a given RPPHDR device.
 * The data fields contained in this structure are intended to be
 * private to the driver and not to be modified by any higher-level
 * software component.
 *
 * It is required to have a separate instance of this structure for each
 * separate RPP LTM module in a given RPPHDR device. Each of these structures
 * must be initialized before use by calling the function @c rpp_ltm_init()
 * with suitable parameters.
 *
 * The structure is exported here so that instances can statically be declared
 * by higher-level software without the need for dynamic memory allocation.
 *****************************************************************************/
typedef struct {
    uint32_t        version;
    uint32_t        base;
    rpp_device *    dev;
} rpp_ltm_drv_t;


/**************************************************************************//**
 * @brief Initialize the rpp ltm driver.
 * @param[in]       dev     rpp_device structure to be used for all register accesses
 * @param[in]       base    base address of RPP-LTM module
 * @param[inout]    drv     LTM driver structure to be initialized
 * @return          0 on success, error-code otherwise
 *
 * This function initializes the @c drv structure for future uses of the driver
 * on the given instance of the rpp ltm module. It fills the @c drv structure
 * with data required for operating on the given instance and caches data that
 * describes the features of the ltm module instance.
 *
 * This function needs to be called once for every instance of the rpp ltm
 * module to operate, each time with a different @c drv argument intended for
 * that very instance of the rpp ltm module only.
 *****************************************************************************/
int rpp_ltm_init(rpp_device * dev, uint32_t base, rpp_ltm_drv_t * drv);

/**************************************************************************//**
 * @brief Enable or disable the rpp ltm module.
 * @param[in]   drv     initialized LTM driver structure describing the ltm unit instance
 * @param[in]   on      enable/disable RPP LTM unit (0: disable, otherwise enable)
 * @return      0 on success, error-code otherwise
 *
 * This function can be used to enable or disable the local tone mapping unit.
 * When enabled, a suitable global tone mapping curve, the local tone mapping
 * parameters as well as the width of the image to be processed needs to
 * have been programmed in advance.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_ltm_enable(rpp_ltm_drv_t * drv, const unsigned on);

/**************************************************************************//**
 * @brief Returns whether the RPP LTM unit is enabled or disabled.
 * @param[in]   drv     initialized LTM driver structure describing the ltm unit instance
 * @param[in]   on      enable/disable status (0: disabled, otherwise enabled)
 * @return      0 on success, error-code otherwise
 *
 * This function queries the current enable status from the unit and returns it.
 *****************************************************************************/
int rpp_ltm_enabled(rpp_ltm_drv_t * drv, unsigned * const on);

/**************************************************************************//**
 * @brief Program the image width for the line buffer.
 * @param[in]   drv     initialized LTM driver structure describing the ltm unit instance
 * @param[in]   width   the width of the image(s) to be processed
 * @return      0 on success, error-code otherwise
 *
 * This function is used to program the image width into the
 * unit. The information is needed for the line buffers that
 * buffer subsequent image lines for image processing.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_ltm_set_linewidth(rpp_ltm_drv_t * drv, uint16_t width);

/**************************************************************************//**
 * @brief Retrieve the image width for the line buffer from the unit
 * @param[in]   drv     initialized LTM driver structure describing the ltm unit instance
 * @param[in]   width   the currently programmed width of the image(s)
 * @return      0 on success, error-code otherwise
 *
 * This function retrieves the currently programmed image width from the
 * unit.
 *****************************************************************************/
int rpp_ltm_linewidth(rpp_ltm_drv_t * drv, uint16_t * const width);

/**************************************************************************//**
 * @brief Programs the color channel weights for calculating the pixel luminance from RGB data.
 * @param[in]   drv     initialized LTM driver structure describing the ltm unit instance
 * @param[in]   red     weight for the red color channel (FP0.10 fixed point number)
 * @param[in]   green   weight for the green color channel (FP0.10 fixed point number)
 * @param[in]   blue    weight for the blue color channel (FP0.10 fixed point number)
 * @return      0 on success, error-code otherwise
 *
 * This function is used to configure the weights of the different color channels
 * for calculating the luminance of pixels. The formula used is:
 * @f$Y = w_{red} \cdot R + w_{green} \cdot G + w_{blue} \cdot B@f$.
 *
 * The weights must be passed as fixed point integers with only a 10 bit fractional
 * part, e.g. a weight value of 0.5 is represented by @f$0.5 * 1024 = 512@f$. It is
 * required that all the weights sum up to a total of 1023.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_ltm_set_weights(rpp_ltm_drv_t * drv, uint16_t red,
        uint16_t green, uint16_t blue);

/**************************************************************************//**
 * @brief Retrieves the color channel weights for calculating the pixel luminance from RGB data.
 * @param[in]   drv     initialized LTM driver structure describing the ltm unit instance
 * @param[out]  red     weight for the red color channel (FP0.10 fixed point number)
 * @param[out]  green   weight for the green color channel (FP0.10 fixed point number)
 * @param[out]  blue    weight for the blue color channel (FP0.10 fixed point number)
 * @return      0 on success, error-code otherwise
 *
 * This function is used to retrive the weights of the different color channels
 * from the unit that are used for calculating the luminance of pixels.
 *****************************************************************************/
int rpp_ltm_weights(rpp_ltm_drv_t * drv, uint16_t * const red,
        uint16_t * const green, uint16_t * const blue);

/**************************************************************************//**
 * @brief Programs the curve for global tone mapping.
 * @param[in]   drv     initialized LTM driver structure describing the ltm unit instance
 * @param[in]   curve   the tone mapping curve
 * @return      0 on success, error-code otherwise
 *
 * This function is used to set the curve used for employing the
 * global tone mapping. The curve shall be calculated using the
 * values derived from the ltm_meas unit for a recently processed
 * image (Reinhard curve).
 *
 * The curve specifies a piecewise linear approximation using a
 * total of 48 line segments. The curve is specified similar to
 * the gamma curves in the @ref RppGammaInDrv "gamma_in" or
 * @ref RppHist256Drv "hist256" unit drivers.
 *
 * The segmentation of the x axis is
 * specified by passing bit shift offsets from which the total
 * widths of the x axis segments are calculated as follows:
 * @f$\Delta{}x[i] = 1 << (curve.dx[i] + 8), i == 1..48 @f$
 * The input value dx[0] shall be set to zero and is ignored when
 * configuring the unit.
 *
 * The \c curve.y[i] values specify the y axis value of the curve
 * at the segment borders, with curve.y[0] being the start value
 * of the curve at x == 0. The maximum value allowed for curve.y[i]
 * is 4095 for a 12bit output.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_ltm_set_tonecurve(rpp_ltm_drv_t * drv, rpp_ltm_curve_t const * const curve);


/**************************************************************************//**
 * @brief Retrieves the curve for global tone mapping from the unit.
 * @param[in]   drv     initialized LTM driver structure describing the ltm unit instance
 * @param[in]   curve   the tone mapping curve
 * @return      0 on success, error-code otherwise
 *
 * This function retrieves the currently configured global tone
 * mapping curve from the unit and returns the data in the supplied structure.
 *****************************************************************************/
int rpp_ltm_tonecurve(rpp_ltm_drv_t * drv, rpp_ltm_curve_t * const curve);

/**************************************************************************//**
 * @brief Programs the Local Tone Mapping parameters into the unit.
 * @param[in]   drv     initialized LTM driver structure describing the ltm unit instance
 * @param[in]   params  the local tone mapping parameters
 * @return      0 on success, error-code otherwise
 *
 * This function set the parameters that influence the local
 * tone mapping algorithm.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_ltm_set_ltm_params(rpp_ltm_drv_t * drv,
        rpp_ltm_param_t const * const params);

/**************************************************************************//**
 * @brief Retrieves the Local Tone Mapping parameters from the unit.
 * @param[in]   drv     initialized LTM driver structure describing the ltm unit instance
 * @param[in]   params  the local tone mapping parameters
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int rpp_ltm_ltm_params(rpp_ltm_drv_t * drv, rpp_ltm_param_t * const params);
 
/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_LTM_DRV_H__ */

