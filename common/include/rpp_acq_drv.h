/******************************************************************************
 *
 * Copyright 2019 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_acq_drv.h
 *
 * @brief   Interface of input acquisition driver
 * 
 *****************************************************************************/
#ifndef __RPP_ACQ_DRV_H__
#define __RPP_ACQ_DRV_H__

#include "rpp_config.h"
#include <rpp_hdr_types.h>

#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppAcqDrv RPP - Input Acquisition Driver
 * RPP - Input Acquisition Driver
 *
 * The input acquisition unit is a flexible unit for image
 * acquisition. It handles a variety of input formats and
 * synchronization signalling on the sensor interface:
 * -# Synchronization
 *   - BT.601 synchronization (H_SYNC/V_SYNC signal lines)
 *   - BT.656 synchronization (H_SYNC/V_SYNC embedded in special
 *     data words)
 *   - DATA mode employing only a DATA_VALID signal line
 * -# Input formats
 *   - Bayer input with 1 pixel per clock cycle
 *   - YUV 4:2:2 input with 1 pixel per clock cycle
 *     (alternating YU, YV samples)
 *   - YUV 4:2:2 input with 1 pixel per 2 clock cycles
 *     (individual Y, U, Y, V samples, each on a separate
 *     clock cycle)
 *   - RAW input with 1 sample per clock cycle
 * 
 * The combination of synchronization signalling and input
 * format is selected using the @c set_mode() function in
 * any configuration mode.
 *
 * A DMA interface is available for providing the RPP HDR
 * with image data from memory instead of the sensor input.
 * The DMA interface supports the same input formats with
 * slight differences:
 *  - the input samples are always 16bit wide, the sample
 *    dynamic range is not configurable
 *  - the input must always be MSB-aligned
 *  - an option to unpack 4 12bit input samples from 3
 *    16bit DMA words exists
 *  - the synchronization setting (BT.601, BT.656) is ignored
 *    and the input DMA engine must provide exact @c hend /
 *    @c vend signals to indicate END-OF-LINE or END-OF-FRAME,
 *    respectively
 * 
 * Besides the two inputs, there are four different output
 * interfaces, some of which are delivered with image data
 * according to the configuration of the units. The output
 * interfaces are:
 *  - A BAYER data interface towards the BLS unit for black
 *    level measurement. This output is delivered by the input
 *    formatter with data received from the sensor interface
 *    and receives no data if the input formatter is disabled.
 *  - A BAYER data interface towards the RPP Bayer pipeline
 *    (e.g. the ISP data path). Data delivered here will
 *    flow through the whole MAIN_PRE_x pipeline, the radiance
 *    mapping, and the MAIN_POST pipeline towards the Human
 *    Vision and Machine Vision outputs.
 *  - A YUV data interface towards the RPP_OUT Human Vision
 *    Output interface. Data delivered to this output will
 *    bypass all of the ISP pipeline stages and flows directly
 *    to the human vision output interface, where it may be
 *    selected for output. There is no connection to the
 *    Machine Vision output from here.
 *  - A RAW OUT DMA output interface that can be used to store
 *    acquired image data to memory without any processing at
 *    all.
 * 
 * The acquired data flows through several processing blocks
 * and routing switches inside the Acquisition Unit. A schematic
 * diagram of the processing blocks is given in the User Manual
 * and must be known to understand the following programming
 * guidelines. A different configuration than the one indicated
 * may be attempted but may result in errorneous behaviour of
 * the input acquisition unit.
 * 
 * The unit supports two different configuration modes: the
 * @c initial configuration mode that is active after RESET
 * and the @c alternative configuration mode that gives more
 * flexibility.
 * 
 * @section initial_conf Initial Configuration Mode
 * 
 * In this configuration mode, data is routed through the latency
 * fifo in most of the use cases, a fifo that intends to smoothen
 * out considerable input data jitter or ISP processing latencies. The
 * @c latency_input_fifo_selection property determines whether
 * FIFO input will be acquired from the sensor interface or the DMA
 * interface.
 *             
 * The selected input format determines the output to which
 * the data will be directed:
 *  - Bayer pixels from sensor or input DMA: to the RPP Bayer pipeline,
 *    there is no output towards RAW OUT.
 *  - YUV pixels from sensor or input DMA: to the RPP_OUT Human Vision
 *    Output stage (bypassing all ISP functionality), there is no
 *    output towards RAW OUT.
 *  - RAW data from sensor interface: to RAW DMA output
 *    (set @c fifo_input = 0). Note that
 *    there is no output towards RPP_OUT or RPP Bayer.
 *  - RAW data from input DMA:
 *    - If @c fifo_input == 1
 *      the data is routed through the FIFO towards RAW OUT.
 *    - Otherwise if @c fifo_input == 0, the
 *      FIFO and OUTFORM are bypassed and the
 *      data is directed towards RPP_OUT or RPP Bayer
 *      depending on the @c input_type_dma property. For YUV output
 *      the samples must be in YU, YV format (e.g. 1 pixel per clock cycle)
 *      because that is the format required by RPP_OUT and the CCIR_ALIGN
 *      unit is bypassed. This mode is not supported by the simulator.
 *
 * The following muxer setting properties in the acquisition properties
 * are ignored in this configuration mode:
 *  - @c raw_out_sel
 *  - @c mux_dma_sel
 *  - @c yuv_out_sel
 * 
 * @section alternate_conf Alternative Configuration Mode
 *
 * In this configuration mode, the settings of all data routing
 * switches must explicitly be programmed according to the
 * type of input data and the outputs to which the data shall
 * be directed. This gives a finer-grained control over the
 * active outputs and allows modes of operation that are not
 * possible to achive with the initial configuration mode.
 * 
 * It is suggested to use the latency FIFO for operation for
 * a smooth processing of pixels (any data to be acquired from
 * the sensor interface must be routed through the FIFO, anyway).
 *
 * The DIRECT_RAW switch is controlled by the @c raw_out_sel
 * property and allows the FIFO data to be routed to RAW DMA
 * and/or RPP Bayer/RPP_OUT. Dual output (towards the RPP
 * and towards the RAW DMA output) is possible. 
 *
 * The MUX_BACK_DMA switch is controlled by the @c mux_dma_sel
 * property and determines whether FIFO data or DMA input data
 * is forwarded towards RPP Bayer/RPP_OUT. If DMA input is
 * selected, it is required that the DMA input data is @b not
 * directed towards the FIFO (e.g. @c fifo_input == 0).
 * 
 * The DIRECT_YUV switch is controlled by the @c yuv_out_sel
 * property and determines which of the RPP Bayer and RPP_OUT
 * Human Vision outputs shall be supplied with data. This
 * naturally depends on the type of input data: for YUV data
 * the RPP_OUT and for Bayer data the RPP Bayer pipeline
 * must be selected.
 * 
 * @subsection acq_bayer Bayer Data Processing
 * 
 * The configuration of Bayer data processing is straightforward.
 * 
 * When the sensor interface is used, the following properties
 * must be programmed:
 *  - @c bayer_pattern must be set to the effective Bayer pattern
 *    after input cropping at the INFORM stage
 *  - @c input_type must be set to a value indicating the
 *    number of valid input bits per sample
 *  - @c sensor_align_in must be set if the valid input
 *    bits per sample are connected to the least-significant
 *    data lines of the sensor interface, otherwise it must be
 *    cleared
 *  - @c sensor_in_2samples and @c dma_out_2samples
 *    flags must be cleared
 *  - @c ccir_align_enable must be off
 * The input acqusition window must be programmed correctly and
 * the INFORM unit must be enabled using the enable() function.
 * 
 * When the DMA input interface is used, the same properties
 * must be configured, with the following differences:
 *  - @c bayer_pattern must be set to the Bayer pattern of the image
 *    in memory.
 *  - The INFORM unit is bypassed and shall be disabled. INFORM
 *    cropping is not performed, e.g. the whole in-memory image
 *    is acquired. OUTFORM cropping may still be employed if the FIFO
 *    is not bypassed.
 *  - @c dma_in_2samples must be cleared
 *
 * @subsection acq_yuv YUV Data Processing
 * 
 * YUV data input implies that, when being directed towards the
 * RPP, it will bypass all ISP features and be sent to the
 * RPP_OUT Human Vision Output directly. The RPP_OUT input
 * selection must be programmed according to this situation
 * for correct operation.
 *
 * There are two possible choices for YUV data acquisition:
 * -# One pixel per clock cycle (combined YU, YV input samples):
 *  - This mode can only be used meaningfully on the sensor interface
 *    when the incoming YUV 4:2:2 data spans the whole width of the
 *    sensor interface.
 *  - For 24bit instances of the ACQ (MAIN_PRE_1) this means that 12bit
 *    Y is connected to bits 23:12 of the sensor interface and 12bit U/V
 *    to bits 11:0. The @c sensor_in_2samples property must
 *    be set to 1 and @c input_type must be programmed to 24bit.
 *  - For 12bit instances of the ACQ (MAIN_PRE_2/3) this means that
 *    6bit Y is connected to bits 11:6 of the sensor interface and
 *    6bit U/V to bits 5:0. The @c sensor_in_2samples
 *    property must be set to 1 and @c input_type to 12bit.
 *  - When this mode is used with the DMA input interface, each
 *    16bit DMA sample must be arranged as follows: Y is found
 *    in bits 15:8 and U/V alternatingly in bits 7:0. The
 *    @c dma_in_2samples property must be set to 1 and
 *    the @c unpack_dma_in property must be cleared to 0.
 *  - When this mode is chosen, the CCIR alignment must be disabled.
 *    This also means that no sample re-ordering takes place. Care
 *    must be taken (either by INFORM cropping or DMA memory
 *    preparation) that the sample input sequence is such that the
 *    first sample in each line is an YU pair.
 *  - The RAW OUT property @c dma_out_2samples must
 *    also be set to 1 if there is any output towards RAW OUT.
 * -# One pixel per 2 clock cycles (individual Y, U, Y, V input
 *    samples)
 *  - This mode requires that the data is routed through the FIFO
 *    because the CCIR alignment unit is present in that path only.
 *  - All possible input bit widths of the sensor interface can
 *    be used. The individual luminance / chrominance samples
 *    are re-aligned as needed. The CCIR alignment unit will use
 *    at most 12bit per component.
 *  - The @c sensor_in_2samples or @c dma_in_2samples
 *    property must be cleared to 0 depending on the input source.
 *  - For input via DMA, the @c unpack_dma_in property
 *    may be used. If enabled, the valid sample bits shall be contained
 *    in the top-most bits of the unpacked 12bit samples. If disabled,
 *    the valid sample bits shall be contained in the top-most bits of the
 *    unpacked 16bit DMA samples. Unused bits shall be cleared to 0.
 *  - The @c ccir_align_enable property must be set to 1 if output is directed
 *    towards RPP_OUT. The @c ccir_seq property must be set to the Y, U, V sample
 *    ordering after INFORM cropping (on the sensor interface) or as found
 *    in memory (for DMA input).
 *  - The @c ccir_align_enable property may be enabled if output is directed
 *    to RAW OUT alone to produce YU, YV DMA output. The property may be
 *    disabled if output is to RAW OUT alone, in which case the data is
 *    copied from input to output in Y, U, Y, V format without reordering. The
 *    @c dma_out_2samples property shall be set to the same value
 *    as @c ccir_align_enable in either case.
 *  - The @c pack_dma_out property of the RAW OUT DMA output shall
 *    not be set to 1 if @c ccir_align_enable is set. It may be set or cleared
 *    if @c ccir_align_enable is cleared.
 * 
 * @note The _2samples_per_cc_en flags are only used during conversion of
 * the sample between different bit widths. There are the input bit width
 * (sensor interface: 12/24 bit, DMA interface 16 bit), internal bit width
 * (24bit) and output bit width (RAW OUT: 16bit). When set, the samples
 * are cut in half, the halves are adopted separately and merged to form
 * the sample in the target bit width. For other samples than YU, YV this
 * processing causes errorneous data to be calculated.
 * 
 * 
 * @section acq_win Cropping Window Configuration
 *
 * There are a number of constraints that have to be considered when
 * configuring Input or Output Formatter cropping windows.
 *
 *  -# For sensor input, both INFORM and OUTFORM cropping windows are
 *     active. For Bayer data acquisition, the INFORM cropping window
 *     is usually configured to @b include the optical black colums and
 *     lines delivered by the sensor for blacklevel measurement by the
 *     BLS sub-unit inside the same MAIN_PRE_x pipeline stage. The OUTFORM
 *     cropping window is set-up to @b remove the optical black pixels and
 *     passing only active pixels to the ISP. In any other cases, the
 *     INFORM cropping window may be set-up to only include active pixels
 *     from the sensor.
 *  -# For DMA input routed through the FIFO, the OUTFORM cropping window
 *     is active.
 *  -# For DMA input that bypasses the FIFO, no cropping is performed.
 *
 * The input formatter counts valid input samples (one per
 * clock cycle). Note that the @c sensor_in_2samples property
 * does not cause each clock cycle to be counted twice.
 * If the acquisition mode is configured to BAYER or YUV input, the input
 * formatter ensures that an even number of samples per line is captured
 * by rounding the acquisition window width down to the next even number.
 * The acquisition window width and height must fulfil the following
 * conditions:
 *  - They are not allowed to be zero.
 *  - (h_offset + width) must not be larger than the number of pixels per line in the input.
 *  - (v_offset + height) must not be larger than the number of lines in the input.
 * 
 * The output formatter counts samples emitted by the FIFO. This number may differ
 * from the number of input samples under the following conditions:
 *  - For FIFO input from the DMA, the @c unpack_dma_in property
 *    causes the number of DMA input samples to @b increase by a factor of 4/3
 *    when enabled.
 *  - The @c ccir_align_enable property, when enabled, causes the number of input
 *    samples to @b decrease by a factor of 2.
 *  - When both properties are enabled, their effects add up.
 * 
 * If the acquisition mode is programmed to BAYER or YUV input, the output formatter ensures
 * that both, the horizontal cropping offset and the cropping width are even numbers by
 * rounding them down to the nearest even number.
 * The output formatter cropping window geometry must fulfil the following
 * conditions:
 *  - The width/height are not allowed to be zero.
 *  - (h_offset + width) must not be larger than the number of pixels per line emitted by the FIFO.
 *  - (v_offset + height) must not be larger than the number of lines emitted by the FIFO.
 *
 * @{
 *****************************************************************************/

/**************************************************************************//**
 * @brief RPP ACQ sample edges
 *****************************************************************************/
enum rpp_acq_samples_edge_e
{
    RPP_ACQ_FALLING_EDGE = 0,   /**< definition for falling edge sampling */
    RPP_ACQ_RISING_EDGE  = 1    /**< definition for rising edge sampling */
};

/**************************************************************************//**
 * @brief RPP ACQ input modes (sample modes)
 *****************************************************************************/
enum rpp_acq_mode_e
{
    RPP_ACQ_MODE_RAW_BT601      = 0,    /**< RAW picture with BT.601 sync (ISP bypass, capturing to memory) */
    RPP_ACQ_MODE_YUV_BT656      = 1,    /**< ITU-R BT.656 (YUV with embedded sync) */
    RPP_ACQ_MODE_YUV_BT601      = 2,    /**< ITU-R BT.601 (YUV input with H and Vsync signals) */
    RPP_ACQ_MODE_BAYER_BT601    = 3,    /**< Bayer RGB processing with H and Vsync signals */
    RPP_ACQ_MODE_DATA           = 4,    /**< data mode (ISP bypass, sync signals interpreted as data enable) */
    RPP_ACQ_MODE_BAYER_BT656    = 5,    /**< Bayer RGB processing with BT.656 synchronizatio */
    RPP_ACQ_MODE_RAW_BT656      = 6,    /**< RAW picture with ITU-R BT.656 synchronization (ISP bypass, capturing to memory) */
    RPP_ACQ_MODE_MAX,
};

/**************************************************************************//**
 * @brief RPP ACQ bayer pattern
 *****************************************************************************/
enum rpp_acq_bayer_pattern_e 
{
    RPP_ACQ_BAYPAT_RGGB = 0b00, /**< first line: RGRG..., second line: GBGB..., etc. */
    RPP_ACQ_BAYPAT_GRBG = 0b01, /**< first line: GRGR..., second line: BGBG..., etc. */
    RPP_ACQ_BAYPAT_GBRG = 0b10, /**< first line: GBGB..., second line: RGRG..., etc. */
    RPP_ACQ_BAYPAT_BGGR = 0b11, /**< first line: BGBG..., second line: GRGR..., etc. */
};

/**************************************************************************//**
 * @brief RPP ACQ ccir sequence 
 *****************************************************************************/
enum rpp_acq_ccir_sequence_e 
{
    RPP_ACQ_CCIR_SEQ_YCbYCr = 0b00, /**< sample sequence Y,Cb,Y,Cr */
    RPP_ACQ_CCIR_SEQ_YCrYCb = 0b01, /**< sample sequence Y,Cr,Y,Cb */
    RPP_ACQ_CCIR_SEQ_CbYCrY = 0b10, /**< sample sequence Cb,Y,Cr,Y */
    RPP_ACQ_CCIR_SEQ_CrYCbY = 0b11, /**< sample sequence Cr,Y,Cb,Y */
    
    RPP_ACQ_CCIR_SEQ_UNUSED = RPP_ACQ_CCIR_SEQ_YCbYCr, /**< use if not needed */
};

/**************************************************************************//**
 * @brief RPP field selection
 *****************************************************************************/
enum rpp_acq_field_selection_e
{
    RPP_ACQ_FIELD_ALL   = 0b00, /**< sample both fields (even and odd) */
    RPP_ACQ_FIELD_EVEN  = 0b01, /**< sample only even fields */
    RPP_ACQ_FIELD_ODD   = 0b10, /**< sample only odd fields */
    
    RPP_ACQ_FIELD_UNUSED    = RPP_ACQ_FIELD_ALL,
};

/**************************************************************************//**
 * @brief RPP ACQ fifo input selection
 *****************************************************************************/
enum rpp_acq_input_e 
{
    RPP_ACQ_INPUT_FORMATTER = 0b0,  /**< capture interface (input formatter) */
    RPP_ACQ_INPUT_DMA       = 0b1,  /**< memory interface */
};

/**************************************************************************//**
 * @brief RPP input sample bit width on external interface
 *
 * This enumerated value defines possible choices for the bit width of the
 * external interface, that is, how many data lines are wired to the
 * external interface. The alignment of the data, e.g. whether the data lines
 * are wired to the LSBs or MSBs of the external interface, needs to be
 * specified with a different setting.
 *
 * Input bit width specificiations of up to 12bit are valid for all ACQ
 * instances, whereas input bit width specifications larger than 12bit are
 * only valid for ACQ instances supporting high-resolution samples.
 *****************************************************************************/
enum rpp_acq_input_type_e
{
    RPP_ACQ_INPUT_TYPE_8b       = 0b000,    /**< @brief 8bit external interface                     */
    RPP_ACQ_INPUT_TYPE_10b      = 0b001,    /**< @brief 10bit external interface                    */
    RPP_ACQ_INPUT_TYPE_12b      = 0b010,    /**< @brief 12bit external interface                    */
    RPP_ACQ_INPUT_TYPE_14b      = 0b011,    /**< @brief 14bit external interface                    */
    RPP_ACQ_INPUT_TYPE_16b      = 0b100,    /**< @brief 16bit external interface                    */
    RPP_ACQ_INPUT_TYPE_18b      = 0b101,    /**< @brief 18bit external interface                    */
    RPP_ACQ_INPUT_TYPE_20b      = 0b110,    /**< @brief 20bit external interface                    */
    RPP_ACQ_INPUT_TYPE_24b      = 0b111,    /**< @brief 24bit external interface                    */
    RPP_ACQ_INPUT_TYPE_MAX,               /**< upper boarder value for range check */
    RPP_ACQ_INPUT_TYPE_UNUSED   = RPP_ACQ_INPUT_TYPE_8b,   /**< if unused */
};

/**************************************************************************//**
 * @brief RPP input type on dma interface
 *
 * This enumerated value defines possible choices for the data type of input
 * on the DMA interface.
 *****************************************************************************/
enum rpp_acq_dma_input_type_e
{
    RPP_ACQ_DMA_INPUT_TYPE_YUV    = 0b0,  /**< @brief input on DMA interface is YUV data.   */
    RPP_ACQ_DMA_INPUT_TYPE_BAYER  = 0b1,  /**< @brief input on DMA interface is BAYER data. */
    RPP_ACQ_DMA_INPUT_TYPE_UNUSED = RPP_ACQ_DMA_INPUT_TYPE_YUV,
};

/**************************************************************************//**
 * @brief RPP ACQ input alignment
 *
 * This enumerated value defines possible choices for the alignment of valid
 * bits in ACQ input samples.
 *
 * In case of MSB aligned data, the valid bits are found in the high bits of
 * each data sample, and the least significant bits of the data sample are
 * to be ignored.
 *
 * In case of LSB aligned dat, the valid bits are found in the least significant
 * bits of each data sample, and the highest bits of the data sample are
 * to be ignored.
 *****************************************************************************/
enum rpp_acq_align_e
{
    RPP_ACQ_ALIGN_MSB = 0b0,    /**< @brief input is aligned to MSBs, e.g. LSBs of samples are to be ignored. */
    RPP_ACQ_ALIGN_LSB = 0b1,    /**< @brief input is aligned to LSBs, e.g. MSBs of samples are to be ignored. */
};

/**************************************************************************//**
 * @brief RPP ACQ latency FIFO output muxer settings
 *
 * This enumerated value defines possible choices for the configuration of
 * the muxer that distributes the latency FIFO data. That data can be directed
 * to the DMA output interface, to the ISP, or to both.
 *
 *****************************************************************************/
enum rpp_acq_raw_out_sel_e
{
    RPP_ACQ_RAW_OUT_ISP     = 0b00, /**< @brief FIFO output is directed towards the YUV or Bayer ISP paths, not towards DMA output. */
    RPP_ACQ_RAW_OUT_DMA     = 0b01, /**< @brief FIFO output is directed towards DMA output, not towards YUV or Bayer ISP paths.     */
    RPP_ACQ_RAW_OUT_BOTH    = 0b11  /**< @brief FIFO output is directed towards DMA output and YUV/Bayer ISP paths.                 */
};


/**************************************************************************//**
 * @brief RPP ACQ ISP output source setting
 *
 * This enumerated value defines possible choices for the configuration of
 * the muxer that chooses the source for the ACQ output towards the ISP.
 * It controls whether latency FIFO data or DMA input data are forwarded
 * towards the YUV/Bayer ISP paths.
 *
 *****************************************************************************/
enum rpp_acq_mux_dma_sel_e
{
    RPP_ACQ_MUX_DMA_SEL_FIFO    = 0b0, /**< @brief FIFO output is directed towards the YUV/Bayer ISP paths. */
    RPP_ACQ_MUX_DMA_SEL_DMA     = 0b1, /**< @brief DMA input is directed towards YUV/Bayer ISP paths.       */
};

/**************************************************************************//**
 * @brief RPP ACQ ISP output target setting
 *
 * This enumerated value defines possible choices for the configuration of
 * the muxer that chooses the data path to which ISP data is directed. It
 * controls whether ISP data is directed to the Bayer path (including all
 * ISP processing) or the YUV path (bypassing most ISP processing).
 *
 *****************************************************************************/
enum rpp_acq_yuv_out_sel_e
{
    RPP_ACQ_YUV_OUT_SEL_BAYER   = 0b0, /**< @brief ISP data is directed towards the Bayer path. */
    RPP_ACQ_YUV_OUT_SEL_YUV     = 0b1, /**< @brief ISP data is directed towards the YUV path.   */
};


/**************************************************************************//**
 * @brief RPP input acquisition properties
 *
 * This complex structure defines several properties of the input acquisition
 * unit. Some of the settings are only considered when the alternative
 * configuration mode is enabled, others only when that mode is disabled.
 * Some properties are always respected.
 *
 * When alternative configuration mode is enabled, some settings in this
 * structure enable direct control over the data routing inside the ACQ unit.
 * Please refer to the RPPHDR User's Manual on detailed information about
 * the routing possibilities.
 *
 * If alternative configuration mode is disabled, the routing of data samples
 * inside the ACQ unit is described by the configurable acquision mode, which
 * selects the routing options depending on a data type description for the
 * sensor interface.
 *
 * Other settings in this structure describe the way data bits have to be
 * sampled by the ACQ.
 *****************************************************************************/
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
typedef union
{
    uint32_t flags;
    struct {
        /*! @brief sampling edge
         *
         * This value describes whether data is sampled on positive or negative edges.
         * Must be set to one of the enumerated values in @c rpp_acq_samples_edge_e */
        uint32_t sample_edge:1;
        /*! @brief HSYNC polarity
         *
         * This value describes whether the HSYNC signal is high-active (0)
         * or low-active (1). */
        uint32_t hsync_polarity:1;
        /*! @brief VSYNC polarity
         *
         * This value describes whether the VSYNC signal is high-active (0)
         * or low-active (1). */
        uint32_t vsync_polarity:1;
        /*! @brief bayer pattern after INFORM cropping
         *
         * This value describes the Bayer pattern of acquired image data
         * that is present after cropping at the INFORM stage. Therefore,
         * this value is different from the Bayer pattern of the sensor when
         * the horizontal and/or vertical INFORM cropping window offsets
         * are programmed to uneven values.
         *
         * This value may need to be reprogrammed when the horizontal
         * or vertical offsets of the INFORM cropping window are reprogrammed.
         *
         * Allowed values for this field are given by the \c rpp_acq_bayer_pattern_e
         * enum.
         */
        uint32_t bayer_pattern:2;
        /*! @brief enable the CCIR alignment unit
         *
         * The CCIR alignment unit must be enabled if the input is
         * YUV 4:2:2 data with one pixel per two clock cycles, and the image
         * data shall be directed to RPP_OUT. The block is responsible for
         * collecting related Y and C samples, for merging and reordering them
         * so that subsequent processing stages do not need to care about
         * the sample order.
         *
         * Setting this value to '1' enables the CCIR alignment unit
         * which is required for described YUV 4:2:2 data routed through the FIFO.
         * Setting this value to '0' disables the CCIR alignment unit.
         */
        uint32_t ccir_align_enable:1;
        uint32_t __unused1:1;
        /*! @brief YUV 4:2:2 component order
         *
         * This value describes the ordering of Y, Cb and Cr components. This
         * member shall be set to one of the values described by
         * @c rpp_acq_ccir_sequence_e and is used for re-ordering the individual
         * input samples in the CCIR alignment unit to provide YU, YV
         * samples towards the RPP_OUT interface.
         */
        uint32_t ccir_sequence:2;
        /*! @brief fields to sample for YUV 4:2:2 interlaced video input
         *
         * This value describes the fields to sample when the input are
         * interlaced YUV 4:2:2 video frames. This member shall be set to
         * one of the values described by @c rpp_acq_field_selection_e.
         */
        uint32_t field_selection:2;
        /*! @brief configure field swap for YUV 4:2:2 interlaced video input
         *
         * This values allows to enable swapping odd and even field identifiers
         * when the input are interlaced YUV 4:2:2 video frames. Setting this
         * member to '1' changes odd fields to even fields and vice versa.
         * Setting this member to '0' leaves the field identifiers unchanged.
         */
        uint32_t swap_fields:1;
        /*! @brief configures the data width on the sensor interface
         *
         * This value describes the amount of valid data bits on the sensor
         * interface. This member shall be set to one of the values described
         * by @c rpp_acq_input_type_e.
         *
         * The value configured here shall not specify more bits than the width
         * of the sensor interface.
         */
        uint32_t input_type:3;
        /*! @brief configures the input source for the latency fifo
         *
         * This value describes which of the available external interfaces
         * (sensor interface, DMA interface) provides input to the latency
         * fifo. This member shall be set to one of the values described by
         * @c rpp_acq_input_e.
         *
         * When set to @c RPP_ACQ_INPUT_FORMATTER, the sensor interface
         * data will be routed to the latency fifo. Otherwise, the DMA interface
         * data will be routed to the latency fifo.
         */
        uint32_t fifo_input:1;
        /*! @brief enables 16bit to 12bit unpack feature on DMA input
         *
         * This value allows to enable the feature that unpacks 3x16bit samples
         * to 4x12bit samples for input from the DMA interface. A value of '1'
         * enables that feature.
         *
         * If enabled, each set of 3 16bit input samples are decomposed and
         * rearranged to produce a total of 4 12bit samples, a feature that is
         * useful to reduce RAM requirements for BAYER image data using only
         * 12bit color depth.
         *
         * This feature is incompatible with the dma_in_2samples feature for
         * YUV 4:2:2 input which requires that all the 16 bits of each DMA sample
         * are used for 8bit Y plus 8bit C data.
         */
        uint32_t unpack_dma_in:1;
        /*! @brief enables 12bit to 16bit pack feature on DMA output
         *
         * This value allows to enable the feature that packs 4x12bit samples
         * to 3x16bit samples for output on the DMA output interface. A value of '1'
         * enables that feature.
         *
         * If enabled, each set of 4 12bit output samples are decomposed and
         * rearranged to produce a total of 3 16bit samples, a feature that is
         * useful to reduce RAM requirements for BAYER image data using only
         * 12bit color depth.
         *
         * This feature is incompatible with the dma_out_2samples feature for
         * YUV 4:2:2 output which requires that all the 16 bits of each DMA sample
         * are used for 8bit Y plus 8bit C data.
         */
        uint32_t pack_dma_out:1;
        /*! @brief specifies the data type on the DMA interface
         *
         * When alternative configuration mode is disabled, this value
         * specifies the type of data that is received on the input DMA interface.
         * In case of the input DMA data bypassing the FIFO, this setting determines
         * the setting of the automatic @c yuv_out_sel selection.
         * It shall be set to one of the values described by
         * @c rpp_acq_dma_input_type_e.
         *
         * When alternative configuration mode is enabled, the value of the
         * @c yuv_out_sel property must be configured 
         */
        uint32_t input_type_dma:1;
        /*! @brief for YUV 4:2:2 input, specifies Y+C samples on the sensor interface
         *
         * This setting can be used to inform the acquisition unit that the
         * sensor interface receives YUV 4:2:2 samples that are composed of an Y
         * and a C part. Setting this member to '1' enables that feature.
         *
         * This setting is used when adoping sensor input samples to the internal bit
         * width which is always 24bit. Specifying this setting to "1" causes data
         * corruption for any other input data type.
         */
        uint32_t sensor_in_2samples:1;
        /*! @brief for YUV 4:2:2 DMA output, enables Y+C samples on the output DMA interface
         *
         * This setting can be used to inform the output DMA interface that
         * the output samples consist of YUV 4:2:2 samples that are composed of an Y
         * and a C part. Setting this member to '1' enables this feature.
         *
         * This setting is used when adoping internal 24bit samples to the DMA bit
         * width which is 16bit. Specifying this setting to "1" causes data
         * corruption for any other output data type.
         *
         * A setting of '1' is incompatible with the @c pack_dma_out feature.
         */
        uint32_t dma_out_2samples:1;
        /*! @brief for YUV 4:2:2 input, enables Y+C samples on the input DMA interface
         *
         * This setting can be used to inform the acquisition unit that the 
         * input DMA interface receives YUV 4:2:2 samples that are composed of an
         * 8bit Y and an 8bit C part. Setting this member to '1' enables that feature.
         *
         * This setting is used when adoping 16bit input DMA samples to the internal bit
         * width which is always 24bit. Specifying this setting to "1" causes data
         * corruption for any other input data type.
         *
         * A setting of '1' is incompatible with the @c unpack_dma_in feature.
         */
        uint32_t dma_in_2samples:1;
        /*! @brief configures the latency FIFO output muxer
         *
         * This value configures the latency FIFO output muxer to
         * direct FIFO data to the acquisition output DMA, the ISP
         * or both. This member shall be set to one of the values described by
         * @c rpp_acq_raw_out_sel_e.
         *
         * This setting is only considered in alternative configuration
         * mode and ignored otherwise.
         */
        uint32_t raw_out_sel:2;
        /*! @brief configures the ISP output source muxer
         *
         * This value configures the muxer that selects the source for
         * the YUV/Bayer outputs towards the ISP. This member shall be set
         * to one of the values described by @c rpp_acq_mux_dma_sel_e.
         *
         * This setting is only considered in alternative configuation
         * mode and ignored otherwise.
         *
         * Setting this member to @c RPP_ACQ_MUX_DMA_SEL_DMA is only allowed
         * if the @c fifo_input member is set to @c RPP_ACQ_INPUT_FORMATTER
         * because otherwise there is no data flowing from the DMA input towards
         * this muxer.
         */
        uint32_t mux_dma_sel:1;
        /*! @brief configures the ISP output target setting
         *
         * This value configures whether data directed to the ISP is
         * routed via the Bayer data path or the YUV data path. This member
         * shall be set to one of the values described by @c rpp_acq_yuv_out_sel_e.
         *
         * This setting is only considered in alternative configuration
         * mode and ignored otherwise.
         */
        uint32_t yuv_out_sel:1;
        uint32_t __unused2:4;
        /*! @brief specifies wether sensor input is MSB or LSB aligned
         *
         * This setting can be used to tell the RPP ACQ unit whether
         * data received on the external sensor interface is LSB aligned
         * or MSB aligned. This member shall be set to one of the values
         * described by @c rpp_acq_align_e.
         */
        uint32_t sensor_align_in:1;
        uint32_t __unused3:1;
    };
} rpp_acq_properties_t;
#pragma GCC diagnostic pop


/**************************************************************************//**
 * @brief RPP ACQ specific driver structure
 *
 * This structure encapsulates all data required by the RPP ACQ driver to
 * operate a specific instance of the RPP ACQ on a given RPPHDR device.
 * The data fields contained in this structure are intended to be
 * private to the driver and not to be modified by any higher-level
 * software component.
 *
 * It is required to have a separate instance of this structure for each
 * separate RPP ACQ module in a given RPPHDR device. Each of these structures
 * must be initialized before use by calling the function @c rpp_acq_init()
 * with suitable parameters.
 *
 * The structure is exported here so that instances can statically be declared
 * by higher-level software without the need for dynamic memory allocation.
 *****************************************************************************/
typedef struct {
    uint32_t        version;
    uint32_t        base;
    rpp_device *    dev;
} rpp_acq_drv_t;


/**************************************************************************//**
 * @brief Initialize the driver for a certain instance of the rpp acq module.
 *
 * @param[in]       dev     rpp_device structure to be used for all register accesses
 * @param[in]       base    base address of RPP-ACQ module
 * @param[inout]    drv     ACQ driver structure to be initialized
 * @return          0 on success, error-code otherwise
 *
 * This function initializes the @c drv structure for future uses of the driver
 * on the given instance of the rpp acq module. It fills the @c drv structure
 * with data required for operating on the given instance and caches data that
 * describes the features of the acq module instance.
 *
 * This function needs to be called once for every instance of the rpp acq
 * module to operate, each time with a different @c drv argument intended for
 * that very instance of the rpp acq module only.
 *****************************************************************************/
int rpp_acq_init(rpp_device * dev, uint32_t base, rpp_acq_drv_t * drv);

/**************************************************************************//**
 * @brief Enable or disable the RPP ACQ INFORM unit.
 * @param[in]   drv     initialized ACQ driver structure describing the acq module instance
 * @param[in]   on      INFORM enable state to set (0: disable, otherwise enable)
 * @return      0 on success, error code otherwise
 *
 * Enables or disables the input formatter of this module to start/stop
 * capturing with the next v-sync.
 *****************************************************************************/
int rpp_acq_enable(rpp_acq_drv_t * drv, const unsigned on);

/**************************************************************************//**
 * @brief Return enable status of the rpp acq module.
 * @param[in]   drv     initialized ACQ driver structure describing the acq module instance
 * @param[out]  on      current INFORM enable state (0: disabled, otherwise enabled)
 * @return      0 on success, error code otherwise
 *
 * @note Enabling this module start the capturing with the next v-sync.
 *****************************************************************************/
int rpp_acq_enabled(rpp_acq_drv_t * drv, unsigned * const on);

/**************************************************************************//**
 * @brief Set RPP acquisition mode.
 * @param[in]   drv     initialized ACQ driver structure describing the acq module instance
 * @param[in]   mode    acquisition control (@see rpp_acq_mode_e)
 * @return      0 on success, error-code otherwise
 *
 * This function configures the RPP acquisition mode to one of the values described
 * by @c rpp_acq_mode_e. If alternative configuration mode is disabled, this
 * setting determines the input data type, the input data source (sensor interface
 * or DMA interface) as well as the routing of data inside the ACQ unit.
 *
 * If alternative configuration mode is enabled, this setting only
 * determines the type of input data on the selected input data source, and the
 * routing of data inside the RPP ACQ unit explicitly needs to be configured
 * using the @c rpp_acq_set_properties() function.
 *****************************************************************************/
int rpp_acq_set_mode(rpp_acq_drv_t * drv, const unsigned mode);

/**************************************************************************//**
 * @brief Return current RPP acquisition mode.
 * @param[in]   drv     initialized ACQ driver structure describing the acq module instance
 * @param[out]  mode    acquisition mode (@see rpp_acq_mode_e)
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int rpp_acq_mode(rpp_acq_drv_t * drv, unsigned * const mode);

/**************************************************************************//**
 * @brief Set RPP alternative configuration mode.
 * @param[in]   drv     initialized ACQ driver structure describing the acq module instance
 * @param[in]   on      whether to enable ACQ alternative configuration mode
 * @return      0 on success, error-code otherwise
 *
 * This function enables or disables the RPP acquisition alternative configuration
 * mode. If alternative configuration mode is enabled, the
 * routing of data inside the RPP ACQ unit explicitly needs to be configured
 * using the @c rpp_acq_set_properties() function.
 *****************************************************************************/
int rpp_acq_set_alternative_mode(rpp_acq_drv_t * drv, const unsigned on);

/**************************************************************************//**
 * @brief Return current RPP acquisition alternative configuration mode.
 * @param[in]   drv     initialized ACQ driver structure describing the acq module instance
 * @param[out]  on      enable status of alternative configuration mode
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int rpp_acq_alternative_mode(rpp_acq_drv_t * drv, unsigned * const on);

/**************************************************************************//**
 * @brief Set rpp acq input acquisition properties.
 * @param[in]   drv     initialized ACQ driver structure describing the acq module instance
 * @param[in]   prop    propterties to set (@see rpp_acq_properties_t)
 * @return      0 on success, error-code otherwise
 *
 * This function can be used to set-up a multitude of parameters that
 * influence the input acquisition. See the description of the various fields
 * in @c rpp_acq_properties_t for details about the various configuration
 * options.
 *****************************************************************************/
int rpp_acq_set_properties(rpp_acq_drv_t * drv, const rpp_acq_properties_t prop);

/**************************************************************************//**
 * @brief Return current rpp acq input acquisition properties.
 * @param[in]   drv     initialized ACQ driver structure describing the acq module instance
 * @param[out]  prop    propterties to set (@see rpp_acq_properties_t)
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int rpp_acq_properties(rpp_acq_drv_t * drv, rpp_acq_properties_t * const prop);

/**************************************************************************//**
 * @brief set input acquisition and the ISP output window
 * @param[in]   drv     initialized ACQ driver structure describing the acq module instance
 * @param[in]   in      sensor input acquisition window
 * @param[in]   out     ISP output cropping window
 * @return      0 on success, error-code otherwise
 *
 * This function configures two windows for data acquisition.
 *
 * The first window is used by the input formatter to select all pixels
 * received from the sensor interface. In regular operation, this window
 * will include all black pixels reported by the Bayer image sensor around
 * the active image area. The data acquired by the input formatter, including
 * the black areas, is sent to the subsequent BLS module for black level
 * detection.
 *
 * The second window is used to configure the output formatter that follows
 * the latency FIFO and is used crop these black areas away, so that only
 * active pixels remain for further ISP processing. The output window geometry
 * is given relative to the input acquisition window and must be fully contained
 * within the acquisition window.
 *****************************************************************************/
int rpp_acq_set_acq_window
(
    rpp_acq_drv_t *         drv,
    const rpp_window_t *    in,
    const rpp_window_t *    out
);

/**************************************************************************//**
 * @brief read input acquisition and the ISP output window
 * @param[in]   drv     initialized ACQ driver structure describing the acq module instance
 * @param[out]  in      input resolution
 * @param[out]  out     output resolution
 * @return      0 on success, error-code otherwise
 *
 * This function retrieves the two windows for image acquisition.
 * @see rpp_acq_set_acq_window() for a description of the windows.
 *****************************************************************************/
int rpp_acq_get_acq_window
(
    rpp_acq_drv_t *         drv,
    rpp_window_t * const    in,
    rpp_window_t * const    out
);

/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_ACQ_DRV_H__ */

