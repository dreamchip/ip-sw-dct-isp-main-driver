/******************************************************************************
 *
 * Copyright 2019 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_filt_drv.h
 *
 * @brief   Interface of debayer filter driver
 *
 *****************************************************************************/
#ifndef __RPP_FILT_DRV_H__
#define __RPP_FILT_DRV_H__

#include "rpp_config.h"
#include <rpp_hdr_types.h>

#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppFiltDrv RPP - Debayering and Filter Unit Driver
 * RPP - Debayering and Filter Unit Driver
 * @{
 *****************************************************************************/

/**************************************************************************//**
 * @brief max number of detail-level
 *****************************************************************************/
#define RPP_FILT_MAX_DETAIL_LVL     ( 10u )

/**************************************************************************//**
 * @brief max number of denoise-level
 *****************************************************************************/
#define RPP_FILT_MAX_DENOISE_LVL    ( 10u )


/**************************************************************************//**
 * @brief Structure holding all filter parameters
 *
 * This structure holds all parameters that are relevant to the configuration
 * of the filtering part of the unit. For a detailed description of the
 * function of the parameters refer to the chapter for Debayering Programming
 * of user manual.
 *
 * The dynamic noise reduction and sharpening function included in the unit
 * applies complex analysis on a 5x5 sample matrix (still in Bayer format)
 * around each pixel to determine a so-called "level of detail", which is
 * used to select from 5 different sharpening/blurring levels to apply around
 * that sample:
 *  -# the unit determines the sum of all gradients in the 5x5 sample matrix
 *     in horizontal direction
 *  -# the unit determines the sum of all gradients in the 5x5 sample matrix
 *     in vertical direction
 *  -# the unit determines the average luminance in the 5x5 sample matrix
 *  -# the unit clips and scales the average luminance using the configurable
 *     parameters @c luma_gain, @c luma_kink and @c luma_min
 *  -# the unit calculates an effective sum of gradients @c sum_grad by
 *     adding horizontal and vertical gradient sums and multiplies the sum
 *     with the average luminance
 *  -# the calculated @c sum_grad is compared to configurable thresholds
 *     to map it to one of the 5 levels of detail.
 *
 * The determined level of detail has the following properties:
 *  - Dark areas of the image are considered noisy rather than detail-rich
 *    are are more likely to get smoothed.
 *  - Bright areas of the image with little contrast are considered noisy
 *    rather than detail-rich are are more likely to get smoothed.
 *  - Bright areas of the image with high contrast are considered detail-rich
 *    and are stronly sharpened.
 *  - It is configurable which luminance values constitute dark or bright with
 *    a smooth linear transition in between.
 *  - It is configurable which @c sum_grad values map to the levels of detail.
 *  - The sharpening factors that determine the amount of blurring or
 *    sharpening to be done for each level of detail are configurable.
 */
typedef struct {
    /*! @brief General operation mode of the filter
     *
     * A value of zero indicates a green filter static mode
     * where only @c fac_mid is used,
     * a non-zero value indicates that dynamic noise reduction
     * and sharpening is enabled.
     *
     * @note This setting is subject to register
     * shadowing and will not have an immediate effect. A new setting
     * programmed to the unit will be activated, synchronized with an
     * end-of-frame signal, when the shadow register update has been
     * requested in the top-level RPP unit register.
     */
    uint8_t     filt_mode;
    /*! @brief green filter stage 1 select (0..8)
     *
     * This value between 0 and 8 selects the 1st stage green filter strength,
     * where a value of 0 selects maximum blurring, 4 is the default and indicates
     * medium blurring, and 8 specifies minimal blurring.
     *
     * @note This setting is subject to register
     * shadowing and will not have an immediate effect. A new setting
     * programmed to the unit will be activated, synchronized with an
     * end-of-frame signal, when the shadow register update has been
     * requested in the top-level RPP unit register.
     */
    uint8_t     filt_lp_select;
    /*! @brief Horizontal chroma filter mode (0..3)
     *
     * This value determines the behaviour of the horizontal chroma filter.
     * - 0: horizontal chroma filter bypass
     * - 1: horizontal chroma filter 1 static mask = [10 12 10]
     * - 2: horizontal chroma filter 2 (dynamic blur1)
     * - 3: default: horizontal chroma filter 3 (dynamic blur2)
     *
     * @note This setting is subject to register
     * shadowing and will not have an immediate effect. A new setting
     * programmed to the unit will be activated, synchronized with an
     * end-of-frame signal, when the shadow register update has been
     * requested in the top-level RPP unit register.
     */
    uint8_t     chroma_h_mode;
    /*! @brief Vertical chroma filter mode (0..3)
     *
     * This value determines the behaviour of the vertical chroma filter.
     * - 0: vertical chroma filter bypass
     * - 1: vertical chroma filter 1 static [8 16 8]
     * - 2: vertical chroma filter 2 static [10 12 10]
     * - 3: default: vertical chroma filter 3 static [12 8 12]
     *
     * @note This setting is subject to register
     * shadowing and will not have an immediate effect. A new setting
     * programmed to the unit will be activated, synchronized with an
     * end-of-frame signal, when the shadow register update has been
     * requested in the top-level RPP unit register.
     */
    uint8_t     chroma_v_mode;
    /*! @brief Gain value for the luma weight function (0..7)
     *
     * The gain factor selected by this setting is for luma_gain
     * ranging increasing from 0 to 7:
     * 0.5, 1.0, 1.5, 2.0, 3.0, 4.0, 6.0. */
    uint8_t     luma_gain;
    /*! @brief kink value of the luma weight function
     *
     * This value determines a lower cut-off value for the luminance
     * weight. If the luminance is smaller than this value,
     * the calculated luma weight function is set to @c luma_min.
     * If the luminance is higher than this value, it is multiplied
     * by the luma gain factor and clipped to maximum luminance
     * to calculate the luma weight function.
     */
    uint16_t    luma_kink;
    /*! @brief offset value for the luma weight function
     *
     * This value determines the minimum value calculated for
     * the luma weight function. */
    uint16_t    luma_min;
    /*! @brief Sharpening factor for highest level of detail 4. */
    uint16_t    fac_sh1;
    /*! @brief Sharpening factor for high level of detail 3. */
    uint16_t    fac_sh0;
    /*! @brief Sharpening factor for medium level of detail 2. */
    uint16_t    fac_mid;
    /*! @brief Sharpening factor for low level of detail 1. */
    uint16_t    fac_bl0;
    /*! @brief Sharpening factor for lowest level of detail 0. */
    uint16_t    fac_bl1;
    /*! @brief Filter threshold for highest level of detail 4.
     *
     * If the response is larger than this @c thres_sh1,
     * the highest level of detail 4 is detected and @c fac_sh1
     * is used for the output.
     */
    uint32_t    thresh_sh1;
    /*! @brief Filter threshold for high level of detail 3.
     *
     * If the filter response is @c thresh_sh0 <= @c sum_grad <= @c thresh_sh1,
     * the level of detail 3 is detected and @c fac_sh0 is used for
     * the output.
     */
    uint32_t    thresh_sh0;
    /*! @brief Filter threshold for low level of detail 1.
     *
     * If the filter response is @c thresh_bl1 <= @c sum_grad <= @c thresh_bl0,
     * the level of detail 1 is detected and @c fac_bl0 is used for
     * the output.
     */
    uint32_t    thresh_bl0;
    /*! @brief Filter threshold for lowest level of detail 0.
     *
     * If the filter response is @c thresh_bl0 < @c sum_grad,
     * the level of detail 0 is detected and @c fac_bl1 is used for
     * the output.
     */
    uint32_t    thresh_bl1;
} rpp_filt_params_t;



/**************************************************************************//**
 * @brief RPP FILT specific driver structure
 *
 * This structure encapsulates all data required by the RPP FILT driver to
 * operate a specific instance of the RPP FILT on a given RPPHDR device.
 * The data fields contained in this structure are intended to be
 * private to the driver and not to be modified by any higher-level
 * software component.
 *
 * It is required to have a separate instance of this structure for each
 * separate RPP FILT module in a given RPPHDR device. Each of these structures
 * must be initialized before use by calling the function @c rpp_filt_init()
 * with suitable parameters.
 *
 * The structure is exported here so that instances can statically be declared
 * by higher-level software without the need for dynamic memory allocation.
 *****************************************************************************/
typedef struct {
    uint32_t        version;
    uint32_t        base;
    rpp_device *    dev;
} rpp_filt_drv_t;



/**************************************************************************//**
 * @brief Initialize the rpp filt module.
 * @param[in]       dev     rpp_device structure to be used for all register accesses
 * @param[in]       base    base address of RPP FILT module
 * @param[inout]    drv     FILT driver structure to be initialized
 * @return      0 on success, error code otherwise
 *
 * This function initializes the @c drv structure for future uses of the driver
 * on the given instance of the rpp filt module. It fills the @c drv structure
 * with data required for operating on the given instance and caches data that
 * describes the features of the filt module instance.
 *
 * This function needs to be called once for every instance of the rpp filt
 * module to operate, each time with a different @c drv argument intended for
 * that very instance of the rpp filt module only.
 *****************************************************************************/
int rpp_filt_init(rpp_device * dev, uint32_t base, rpp_filt_drv_t * drv);

/**************************************************************************//**
 * @brief Enable or disable the filter function of the unit.
 * @param[in]   drv     initialized FILT driver structure describing the filt unit instance
 * @param[in]   on      enable state to set (0: disable, otherwise enable)
 * @return      0 on success, error code otherwise
 *
 * This function enables or disables the filter function of the unit, independent
 * of the debayering function. This is useful in cases where there is grayscale
 * input to the unit instead of Bayer data and allows the grayscale image
 * to be filtered.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_filt_enable(rpp_filt_drv_t * drv, const unsigned on);

/**************************************************************************//**
 * @brief Returns the enable status of the filter function of the unit.
 * @param[in]   drv     initialized FILT driver structure describing the filt unit instance
 * @param[out]  on      current enable state (0: disabled, otherwise enabled)
 * @return      0 on success, error code otherwise
 *****************************************************************************/
int rpp_filt_enabled(rpp_filt_drv_t * drv, unsigned * const on);

/**************************************************************************//**
 * @brief Easy filter unit setup.
 * @param[in]   drv     initialized FILT driver structure describing the filt unit instance
 * @param[in]   detail  detail level (edge enhancement level) to set
 * @param[in]   denoise denoise level (blurring filter level) to set
 * @return      0 on success, error code otherwise
 *
 * This function can be used for a quick setup of the filter function of the
 * unit. It selects suitable defaults from built-in data tables requiring only
 * two parameters:
 *  - a detail level that controls edge detection and sharpening
 *  - a denoise level that controls blurring in edge-free locations
 *
 * This function shall not be called while the RPP HDR is activated and
 * streaming image data.
 *****************************************************************************/
int rpp_filt_set_filter_level(rpp_filt_drv_t * drv,
        uint8_t const detail, uint8_t const denoise);

/**************************************************************************//**
 * @brief Configures demosaicing parameters to the unit
 * @param[in]   drv         initialized FILT driver structure describing the filt unit instance
 * @param[in]   bypass      whether demosaicing shall be bypassed
 * @param[in]   threshold   Threshold for demosaicing texture detection
 * @return      0 on success, error code otherwise
 *
 * This function is used to configure the demosaicing part of the unit.
 * In case of grayscale input data it allows demosaicing to be bypassed.
 * By default, demosaicing is enabled.
 *
 * This function also configures the threshold for Bayer
 * demosaicing texture detection. If demosaicing is enabled,
 * this value is compared to the difference of the
 * vertical and horizontal texture indicators to decide
 * whether the vertical or horizontal texture flag must be set.
 * A value of 0x00 indicates maximum edge detection, whereas a
 * value of 0xFFFF effectively disables texture detection.
 *****************************************************************************/
int rpp_filt_set_demosaic_params(rpp_filt_drv_t * drv,
        const unsigned bypass, const uint16_t threshold);

/**************************************************************************//**
 * @brief Retrieves demosaicing parameters from the unit
 * @param[in]   drv         initialized FILT driver structure describing the filt unit instance
 * @param[in]   bypass      contains whether demosaicing is bypassed on return
 * @param[in]   threshold   contains threshold for demosaicing texture detection on return
 * @return      0 on success, error code otherwise
 *
 * This function retrieves operational parameters of the demosaicing part of
 * the unit. 
 *****************************************************************************/
int rpp_filt_demosaic_params(rpp_filt_drv_t * drv,
        unsigned * const bypass, uint16_t * const threshold);


/**************************************************************************//**
 * @brief Programs detailed filter parameters to the unit
 * @param[in]   drv         initialized FILT driver structure describing the filt unit instance
 * @param[in]   params      structure holding all filter parameters.
 * @return      0 on success, error code otherwise
 *
 * This function programs all relevant parameters for the filtering part into
 * the unit. There is a huge number of parameters in this structure that
 * consistently needs to be programmed. The best approach is to configure
 * the filter parameters basically to one of the detail/denoise levels using
 * the rpp_filt_set_filter_level() function.
 *
 * If the result is not satisfactory, it is possible to read-back the configured
 * parameter set, adjust individial parameters in the returned parameter set,
 * and re-configure the new set using this function.
 *
 * @note Part of the parameters (operation modes and flags) are subject to
 * register shadowing while others (luma weight, sharpening factors and
 * thresholds) are not. Care has to be taken, therefore, when this function
 * is called while the unit is activated and streaming image data.
 *****************************************************************************/
int rpp_filt_set_filter_params(rpp_filt_drv_t * drv, const rpp_filt_params_t * const params);

/**************************************************************************//**
 * @brief Retrieves detailed filter parameters from the unit.
 * @param[in]   drv         initialized FILT driver structure describing the filt unit instance
 * @param[in]   params      structure holding all filter parameters.
 * @return      0 on success, error code otherwise
 *
 * This function retrieves all relevant parameters for the filtering part from
 * the unit. 
 *****************************************************************************/
int rpp_filt_filter_params(rpp_filt_drv_t * drv, rpp_filt_params_t * const params);


/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_FILT_DRV_H__ */


