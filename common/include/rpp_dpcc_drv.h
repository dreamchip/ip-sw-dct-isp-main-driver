/******************************************************************************
 *
 * Copyright 2019 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_dpcc_drv.h
 *
 * @brief   Interface of Defect Pixel Cluster Correction (DPCC) driver
 *
 *****************************************************************************/
#ifndef __RPP_DPCC_DRV_H__
#define __RPP_DPCC_DRV_H__

#include "rpp_config.h"
#include <rpp_hdr_types.h>

#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppDpccDrv RPP - Defect Pixel Cluster Correction unit driver
 * RPP - Defect Pixel Cluster Correction unid driver
 * @{
 *
 * An image sensor may have a certain number of defective pixels that may be
 * the result of manufacturing faults, failures during normal operation, or
 * variations in pixel voltage levels based on temperature or exposure.
 * A wide class of pixel defects may be characterized as:
 * 
 * - <b>dead</b> (always low): The read-out value of this pixel stuck to ground.
 *   After analog to digital (AD) conversion the pixel has a value close to Zero.
 * - <b>hot</b> (always high): he read-out value is more than proportional to the
 *   incoming light. With increasing exposure time the pixel gets brighter
 *   than its neighbor(s).
 * - <b>stuck</b> (to a certain value): The read out value of this pixel is stuck
 *   to supply voltage. After analog to digital conversion the pixel has a
 *   value close to 255 on an 8 bit representation.
 * - <b>blinky</b> (undefined): The read-out value of this pixel undefined (floating).
 *   After analog to digital (AD) conversion the pixel has a random value.
 *
 * These anomalies can further be characterized as static (always present)
 * or dynamic (as a function of exposure or temperature).
 * 
 * This unit tries to detect defective pixels or small pixel clusters and mitigate
 * their effect(s). There are various strategies for detecting defective pixels
 * which may be employed.
 * A list of known bad pixels can be programmed into the unit. These pixels can
 * be considered for bad pixel detection if enabled. Besides that, dynamic and
 * programmable bad pixel detection strategies exist.
 * 
 * Central to the strategy configuration is the member class dpcc_config, which
 * summarizes the enablement of the various detection methods and their
 * associated parameter(s). The unit allows the configuration of 3 pre-defined
 * detection profiles (named Set 1 to Set 3) that each one further differentiates
 * between settings for green pixels and red/blue pixel(s). A pixel is detected
 * to be defective by a detection method set if and only if @b all of the enabled
 * checks detect the pixel as defective (logical AND).
 * 
 * It is configurable which of the detection method sets shall be employed.
 * Besides the configurable method sets there is also a built-in hard-coded
 * set of detection method(s). Multiple sets can be enabled at the same time,
 * in that case a pixel is considered defective if @b any of the method sets
 * evaluates to @c true (logical OR).
 * Any or all of the detection method sets can be instructed to notify
 * the x,y-coordinates of the detected bad pixels to the defect pixel port
 * which should be connected to a DMA write engine for storage.
 *****************************************************************************/

/**************************************************************************//**
 * @brief RPP DPCC detection method set identifier
 *****************************************************************************/
enum rpp_dpcc_method_set_e {
    RPP_DPCC_METHOD_SET_1   = 0,    /**< detection method set 1 */
    RPP_DPCC_METHOD_SET_2   = 1,    /**< detection method set 2 */
    RPP_DPCC_METHOD_SET_3   = 2,    /**< detection method set 3 */
    RPP_DPCC_METHOD_SET_NUM,        /**< number of defect pixel method sets */
};


/**************************************************************************//**
 * @brief RPP DPCC defective pixel correction modes
 *****************************************************************************/
typedef enum rpp_dpcc_interpolation_e {
    DPCC_INTERPOLATION_MEDIAN_4 = 0,    /**< @brief use a 4-pixel median (2x2) for interpolation            */
    DPCC_INTERPOLATION_MEDIAN_5 = 1,    /**< @brief use a 5-pixel median (2x2 + center) for interpolation   */
    DPCC_INTERPOLATION_MEDIAN_9 = 2,    /**< @brief use a 9-pixel median (3x3) for interpolation            */
    RPP_DPCC_NUM_INTERPOLATION,         /**< number of defect pixel correction methods                      */
} rpp_dpcc_interpolation_t;

/**************************************************************************//**
 * @brief RPP DPCC Bad Pixel Table entry description
 *****************************************************************************/
typedef struct rpp_dpcc_bpt_s {
    uint16_t pos_y;                     /**< @brief Y position of the bad pixel */
    uint16_t pos_x;                     /**< @brief X position of the bad pixel */
} rpp_dpcc_bpt_t;



/**************************************************************************//**
 * @brief RPP DPCC method set configuration
 *****************************************************************************/
typedef struct rpp_dpcc_method_s
{
    /* peak gradient check configuration */
    uint8_t     pg_enabled;         /**< @brief whether the peak gradient check is enabled or disabled. */
    uint8_t     pg_factor;          /**< @brief peak gradient factor configuration */

    /* line check configuration */
    uint8_t     lc_enabled;         /**< @brief whether the line check is enabled or disabled */
    uint8_t     lc_factor;          /**< @brief the line check Mean Absolute Difference factor configuration */
    /**< @brief The line check threshold configuration.
     * Depending on the bit width of the datapath where the DPCC unit is embedded,
     * the threshold is either 8bit (12bit datapath) or 16bit wide (24bit datapath).
     */
    uint16_t    lc_threshold;

    /* rank order check configuration */
    uint8_t     ro_enabled;         /**< @brief whether the rank order check is enabled or disabled.    */
    uint8_t     ro_limit;           /**< @brief rank order limit configuration.                         */

    /* rank neighbour difference check configuration */
    uint8_t     rnd_enabled;        /**< @brief whether the rank neighbour difference check is enabled or disabled. */
    uint8_t     rnd_threshold;      /**< @brief rank neighbour difference threshold configuration                   */
    uint8_t     rnd_offset;         /**< @brief Differential Rank Offset configuration                              */

    /* rank gradient check configuration */
    uint8_t     rg_enabled;         /**< @brief whether the rank gradient check is enabled or disabled. */
    uint8_t     rg_factor;          /**< @brief rank gradient factor configuration. */

} rpp_dpcc_method_t;

/**************************************************************************//**
 * @brief Macro to initialize a DPCC method set with all checks disabled and
 * all check parameters set to zero.
 *****************************************************************************/
#define RPP_DPCC_METHOD_INIT(s)  {                              \
    (s).pg_enabled = (s).pg_factor = 0;                         \
    (s).lc_enabled = (s).lc_factor = (s).lc_threshold = 0;      \
    (s).ro_enabled = 0; (s).ro_limit = 0;                       \
    (s).rnd_enabled = (s).rnd_threshold = 0;                    \
    (s).rnd_offset = 1;                                         \
    (s).rg_enabled = (s).rg_factor = 0;                         \
}

/**************************************************************************//**
 * @brief Macro to set the Peak Gradient check configuration in the DPCC method set.
 * @param s         the DPCC method set to update
 * @param enabled   whether to disable (0) or enable (otherwise) the check
 * @param factor    the peak gradient factor to configure.
 *****************************************************************************/
#define RPP_DPCC_CHECK_SET_PG(s, enabled, factor) {     \
    (s).pg_enabled = enabled;                           \
    (s).pg_factor = (enabled) ? factor : 0;             \
}

/**************************************************************************//**
 * @brief Macro to set the Line check configuration in the DPCC method set.
 * @param s         the DPCC method set to update
 * @param enabled   whether to disable (0) or enable (otherwise) the check
 * @param factor    the line check Mean Absolute Difference factor to configure.
 * @param threshold the line check threshold to configure.
 *****************************************************************************/
#define RPP_DPCC_CHECK_SET_LC(s, enabled, factor, threshold) {  \
    (s).lc_enabled = enabled;                                   \
    (s).lc_factor = (enabled) ? factor : 0;                     \
    (s).lc_threshold = (enabled) ? threshold : 0;               \
}

/**************************************************************************//**
 * @brief Macro to set the Rank Order check configuration in the DPCC method set.
 * @param s         the DPCC method set to update
 * @param enabled   whether to disable (0) or enable (otherwise) the check
 * @param limit     Rank Order Limit
 *****************************************************************************/
#define RPP_DPCC_CHECK_SET_RO(s, enabled, limit) {  \
    (s).ro_enabled = enabled;                       \
    (s).ro_limit = (enabled) ? limit : 0;           \
}

/**************************************************************************//**
 * @brief Macro to set the Rank Neighbour Difference check configuration in the DPCC method set.
 * @param s         the DPCC method set to update
 * @param enabled   whether to disable (0) or enable (otherwise) the check
 * @param threshold the rank neighbour difference threshold to configure.
 * @param offset    differential rank offset to configure.
 *****************************************************************************/
#define RPP_DPCC_CHECK_SET_RND(s, enabled, threshold, offset) { \
    (s).rnd_enabled = enabled;                                  \
    (s).rnd_threshold = (enabled) ? threshold : 0;              \
    (s).rnd_offset    = (enabled) ? offset    : 1;              \
}

/**************************************************************************//**
 * @brief Macro to set the Rank Gradient check configuration in the DPCC method set.
 * @param s         the DPCC method set to update
 * @param enabled   whether to disable (0) or enable (otherwise) the check
 * @param factor    the rank gradient factor to configure.
 *****************************************************************************/
#define RPP_DPCC_CHECK_SET_RG(s, enabled, factor) {     \
    (s).rg_enabled = enabled;                           \
    (s).rg_factor = (enabled) ? factor : 0;             \
}

/**************************************************************************//**
 * @brief Macro to get the Peak Gradient check configuration from a DPCC method set.
 * @param s         the DPCC method set to query
 * @param enabled   whether the check is disabled (0) or enabled (otherwise)
 * @param factor    the configured peak gradient factor.
 *****************************************************************************/
#define RPP_DPCC_CHECK_GET_PG(s, enabled, factor) {     \
    enabled = (s).pg_enabled;                           \
    factor = (enabled) ? (s).pg_factor : 0;             \
}

/**************************************************************************//**
 * @brief Macro to get the Line check configuration from a DPCC method set.
 * @param s         the DPCC method set to query
 * @param enabled   whether the check is disabled (0) or enabled (otherwise)
 * @param factor    the configured line check factor.
 * @param threshold the configured line check threshold.
 *****************************************************************************/
#define RPP_DPCC_CHECK_GET_LC(s, enabled, factor, threshold) {  \
    enabled = (s).lc_enabled;                                   \
    factor = (enabled) ? (s).lc_factor : 0;                     \
    threshold = (enabled) ? (s).lc_threshold : 0;               \
}

/**************************************************************************//**
 * @brief Macro to get the Rank Order check configuration from a DPCC method set.
 * @param s         the DPCC method set to query
 * @param enabled   whether the check is disabled (0) or enabled (otherwise)
 * @param limit     Rank Order Limit
 *****************************************************************************/
#define RPP_DPCC_CHECK_GET_RO(s, enabled, limit) {  \
    enabled = (s).ro_enabled;                       \
    limit = (enabled) ? (s).ro_limit : 0;           \
}

/**************************************************************************//**
 * @brief Macro to get the Rank Neighbour Difference check configuration from a DPCC method set.
 * @param s         the DPCC method set to query
 * @param enabled   whether the check is disabled (0) or enabled (otherwise)
 * @param threshold the configured rank neighbour difference threshold.
 * @param offset    differential rank offset
 *****************************************************************************/
#define RPP_DPCC_CHECK_GET_RND(s, enabled, threshold, offset) { \
    enabled = (s).rnd_enabled;                                  \
    threshold = (enabled) ? (s).rnd_threshold : 0;              \
    offset    = (enabled) ? (s).rnd_offset    : 1;              \
}

/**************************************************************************//**
 * @brief Macro to get the Rank Gradient check configuration from a DPCC method set.
 * @param s         the DPCC method set to query
 * @param enabled   whether the check is disabled (0) or enabled (otherwise)
 * @param factor    the configured rank gradient factor.
 *****************************************************************************/
#define RPP_DPCC_CHECK_GET_RG(s, enabled, factor) {     \
    enabled = (s).rg_enabled;                           \
    factor = (enabled) ? (s).rg_factor : 0;             \
}

/**************************************************************************//**
 * @brief RPP DPCC specific driver structure
 *
 * This structure encapsulates all data required by the RPP DPCC driver to
 * operate a specific instance of the RPP DPCC on a given RPPHDR device.
 * The data fields contained in this structure are intended to be
 * private to the driver and not to be modified by any higher-level
 * software component.
 *
 * It is required to have a separate instance of this structure for each
 * separate RPP DPCC unit in a given RPPHDR device. Each of these structures
 * must be initialized before use by calling the function @c rpp_dpcc_init()
 * with suitable parameter(s).
 *
 * After initialization, the @c colorbits member variable is set to the
 * number of bits per color value. This information is needed for the
 * configuration of the Line Check threshold.
 *
 * The structure is exported here so that instances can statically be declared
 * by higher-level software without the need for dynamic memory allocation.
 *****************************************************************************/
typedef struct {
    uint16_t        version;
    uint16_t        colorbits;
    uint32_t        base;
    rpp_device *    dev;
} rpp_dpcc_drv_t;



/**************************************************************************//**
 * @brief Initialize the rpp dpcc unit.
 * @param[in]       dev     rpp_device structure to be used for all register accesses
 * @param[in]       base    base address of RPP DPCC unit
 * @param[inout]    drv     DPCC driver structure to be initialized
 * @return          0 on success, error code otherwise
 *
 * This function initializes the @c drv structure for future uses of the driver
 * on the given instance of the rpp dpcc unit. It fills the @c drv structure
 * with data required for operating on the given instance and caches data that
 * describes the features of the dpcc unit instance.
 *
 * This function needs to be called once for every instance of the rpp dpcc
 * unit to operate, each time with a different @c drv argument intended for
 * that very instance of the rpp dpcc unit only.
 *****************************************************************************/
int rpp_dpcc_init(rpp_device * dev, uint32_t base, rpp_dpcc_drv_t * drv);

/**************************************************************************//**
 * @brief Enable or disable the rpp dpcc unit.
 * @param[in]   drv     initialized DPCC driver structure describing the dpcc unit instance
 * @param[in]   on      enable state to set (0: disable, otherwise enable)
 * @return      0 on success, error code otherwise
 *****************************************************************************/
int rpp_dpcc_enable(rpp_dpcc_drv_t * drv, const unsigned on);

/**************************************************************************//**
 * @brief Return enable status of the rpp dpcc unit.
 * @param[in]   drv     initialized DPCC driver structure describing the dpcc unit instance
 * @param[out]  on      enable state to set (0: disable, otherwise enable)
 * @return      0 on success, error code otherwise
 *****************************************************************************/
int rpp_dpcc_enabled(rpp_dpcc_drv_t * drv, unsigned * const on);

/**************************************************************************//**
 * @brief Program a defect pixel detection method set to the rpp dpcc unit.
 * @param[in]   drv         initialized DPCC driver structure describing the dpcc unit instance
 * @param[in]   m_id        method set identifier (@see rpp_dpcc_method_set_e) 
 * @param[in]   green       method set configuration for green pixels
 * @param[in]   red_blue    method set configuration for red and blue pixels
 * @return      0 on success, error code otherwise
 *
 * This function is used to provide the unit with configuration data for
 * a defect pixel detection method set. The number of the set to be programmed
 * as well as the detection method details (e.g. which detection methods are
 * enabled or disabled and the associated detection parameters) are specified
 * separately for green pixels and red/blue pixel(s).
 *****************************************************************************/
int rpp_dpcc_set_method
(
    rpp_dpcc_drv_t *                drv,
    const unsigned                  m_id,
    const rpp_dpcc_method_t * const green,
    const rpp_dpcc_method_t * const red_blue
);

/**************************************************************************//**
 * @brief Retrieve a defect pixel detection method set from the rpp dpcc unit.
 * @param[in]   drv         initialized DPCC driver structure describing the dpcc unit instance
 * @param[in]   m_id        method set identifier (@see rpp_dpcc_method_set_e) 
 * @param[out]  green       method set configuration for green pixels
 * @param[out]  red_blue    method set configuration for red and blue pixels
 * @return      0 on success, error code otherwise
 *
 * This function is used to retrieve the indicated defective pixel detection
 * method set from the unit.
 *****************************************************************************/
int rpp_dpcc_method
(
    rpp_dpcc_drv_t *            drv,
    const unsigned              m_id,
    rpp_dpcc_method_t * const   green,
    rpp_dpcc_method_t * const   red_blue
);

/**************************************************************************//**
 * @brief Program enabled defect pixel detection methods to the RPP DPCC unit.
 * @param[in]   drv         initialized DPCC driver structure describing the dpcc unit instance
 * @param[in]   use_set1    whether the detection method set #1 shall be enabled
 * @param[in]   use_set2    whether the detection method set #2 shall be enabled
 * @param[in]   use_set3    whether the detection method set #3 shall be enabled
 * @param[in]   use_fixed   whether the built-in unconfigurable detection method set shall be enabled
 * @return      0 on success, error code otherwise
 *
 * This function is used to provide the unit with information about which
 * of the detection sets shall be processed and which shall not. Each of the
 * @c use_set1, @c use_set2 and @c use_set3 parameters specifies for the
 * indicated detection method set whether it is disabled (0) or enabled (otherwise).
 *****************************************************************************/
int rpp_dpcc_set_used_sets
(
    rpp_dpcc_drv_t *    drv,
    uint8_t             use_set1,
    uint8_t             use_set2,
    uint8_t             use_set3,
    uint8_t             use_fixed
);

/**************************************************************************//**
 * @brief Retrieve enabled defect pixel detection methods from the RPP DPCC unit.
 * @param[in]   drv         initialized DPCC driver structure describing the dpcc unit instance
 * @param[out]  use_set1    whether the detection method set #1 is enabled
 * @param[out]  use_set2    whether the detection method set #2 is enabled
 * @param[out]  use_set3    whether the detection method set #3 is enabled
 * @param[out]  use_fixed   whether the built-in unconfigurable detection method set is enabled
 * @return      0 on success, error code otherwise
 *
 * This function is used to query information about enabled defect pixel
 * detection method sets from the unit.
 *****************************************************************************/
int rpp_dpcc_used_sets
(
    rpp_dpcc_drv_t *    drv,
    uint8_t *           use_set1,
    uint8_t *           use_set2,
    uint8_t *           use_set3,
    uint8_t *           use_fixed
);


/**************************************************************************//**
 * @brief Program defect pixel detection correction methods to the RPP DPCC unit.
 * @param[in]   drv         initialized DPCC driver structure describing the dpcc unit instance
 * @param[in]   mode_gr     defect pixel correction method for green pixels
 * @param[in]   mode_rb     defect pixel correction method for red/blue pixels
 * @return      0 on success, error code otherwise
 *
 * This function is used to provide the unit with information about how
 * defective pixels detected by applying the detection method sets shall be
 * repaired. The correction mode is specified separately for green and red/blue
 * pixel(s).
 *****************************************************************************/
int rpp_dpcc_set_output_mode
(
    rpp_dpcc_drv_t *            drv,
    rpp_dpcc_interpolation_t    mode_gr,
    rpp_dpcc_interpolation_t    mode_rb
);

/**************************************************************************//**
 * @brief Retrieve defect pixel detection correction methods from the RPP DPCC unit.
 * @param[in]   drv         initialized DPCC driver structure describing the dpcc unit instance
 * @param[out]  mode_gr     defect pixel correction method for green pixels
 * @param[out]  mode_rb     defect pixel correction method for red/blue pixels
 * @return      0 on success, error code otherwise
 *
 * This function queries from the unit how defective pixels detected by
 * applying the detection method sets will be
 * repaired. The correction mode is specified separately for green and red/blue
 * pixel(s).
 *****************************************************************************/
int rpp_dpcc_output_mode
(
    rpp_dpcc_drv_t *            drv,
    rpp_dpcc_interpolation_t *  mode_gr,
    rpp_dpcc_interpolation_t *  mode_rb
);

/**************************************************************************//**
 * @brief Enable or disable use of the Bad Pixel Table (BPT).
 * @param[in]   drv         initialized DPCC driver structure describing the dpcc unit instance
 * @param[in]   enabled     whether use of the Bad Pixel Table shall be enabled or not
 * @param[in]   mode_gr     BPT defect pixel correction method for green pixels
 * @param[in]   mode_rb     BPT defect pixel correction method for red/blue pixels
 * @return      0 on success, error code otherwise
 *
 * The unit can make use of a Bad Pixel Table, e.g. a table with max. 2048 entries
 * specifying positions of well-known defective pixel(s).
 * This function is used to provide the unit with information about whether
 * the table entries shall be considered or not. If enabled, the function
 * also tells the unit how pixels found in the BPT shall be repaired.
 *****************************************************************************/
int rpp_dpcc_bpt_enable
(
    rpp_dpcc_drv_t *            drv,
    unsigned                    enabled,
    rpp_dpcc_interpolation_t    mode_gr,
    rpp_dpcc_interpolation_t    mode_rb
);

/**************************************************************************//**
 * @brief Query use of the Bad Pixel Table (BPT) from the unit.
 * @param[in]   drv         initialized DPCC driver structure describing the dpcc unit instance
 * @param[in]   enabled     whether use of the Bad Pixel Table is enabled or not
 * @param[in]   mode_gr     BPT defect pixel correction method for green pixels
 * @param[in]   mode_rb     BPT defect pixel correction method for red/blue pixels
 * @return      0 on success, error code otherwise
 *
 * The unit can make use of a Bad Pixel Table, e.g. a table with max. 2048 entries
 * specifying positions of well-known defective pixel(s).
 * This function is used to query from the unit whether
 * the table entries are considered or not. The function
 * also queries from the unit how pixels found in the BPT are repaired.
 *****************************************************************************/
int rpp_dpcc_bpt_enabled
(
    rpp_dpcc_drv_t *            drv,
    unsigned *                  enabled,
    rpp_dpcc_interpolation_t *  mode_gr,
    rpp_dpcc_interpolation_t *  mode_rb
);

/**************************************************************************//**
 * @brief Enable/disable report of defect pixel detection by the RPP DPCC unit.
 * @param[in]   drv         initialized DPCC driver structure describing the dpcc unit instance
 * @param[in]   report_set1 whether detections from method set #1 shall be reported
 * @param[in]   report_set2 whether detections from method set #2 shall be reported
 * @param[in]   report_set3 whether detections from method set #3 shall be reported
 * @param[in]   report_fixed    whether detections from the built-in method set shall be reported
 * @return      0 on success, error code otherwise
 *
 * This function enables or disables the sending of detected bad pixels to the
 * defect pixel port for notification to higher-level software. Each detection
 * method set can independently be instructed to report detected bad pixel(s).
 * Higher-level software may want to process the reported bad pixels and
 * eventually include them in the BPT.
 *
 * The unit can not include detected bad pixels in the BPT directly because
 * the BPT entries need to be sorted in pixel scan order, and simply appending
 * to the table would destroy that order. Therefore, the detected bad pixels
 * are reported to higher-level software which may choose to update the BPT
 * correctly.
 *****************************************************************************/
int rpp_dpcc_bpt_write_enable
(
    rpp_dpcc_drv_t *    drv,
    uint8_t             report_set1,
    uint8_t             report_set2,
    uint8_t             report_set3,
    uint8_t             report_fixed
);

/**************************************************************************//**
 * @brief Query status of defect pixel detection reports by the RPP DPCC unit.
 * @param[in]   drv         initialized DPCC driver structure describing the dpcc unit instance
 * @param[out]  report_set1 whether detections from method set #1 are reported
 * @param[out]  report_set2 whether detections from method set #2 are reported
 * @param[out]  report_set3 whether detections from method set #3 are reported
 * @param[out]  report_fixed    whether detections from the built-in method set are reported
 * @return      0 on success, error code otherwise
 *
 * This function queries from the unit whether or not bad pixels detected by
 * a defect pixel detection method set are sent to the defect pixel port for
 * notification to higher-level software.
 *
 * This setting is independently returned for each of the method detection set(s).
 *****************************************************************************/
int rpp_dpcc_bpt_write_enabled
(
    rpp_dpcc_drv_t *    drv,
    uint8_t *           report_set1,
    uint8_t *           report_set2,
    uint8_t *           report_set3,
    uint8_t *           report_fixed
);

/**************************************************************************//**
 * @brief Program the Bad Pixel table to the unit.
 * @param[in]   drv         initialized DPCC driver structure describing the dpcc unit instance
 * @param[in]   size        number of supplied Bad Pixel Table entries
 * @param[in]   pixels      array of @c size Bad Pixel Table entries
 * @return      0 on success, error code otherwise
 *
 * The unit can make use of a Bad Pixel Table, e.g. a table with max. 2048 entries
 * specifying positions of well-known defective pixel(s).
 *
 * This function programs the table of bad pixels into the unit. The entries
 * in the array must have been sorted in pixel scan order, that is:
 *  - the array must be sorted by increasing Y coordinate
 *  - all entries for the same Y coordinate must be sorted by increasing X coordinate
 *
 * If that constraint is not fulfilled, the behaviour of the unit will
 * not be as intended.
 *****************************************************************************/
int rpp_dpcc_bpt_set_table
(
    rpp_dpcc_drv_t *        drv,
    unsigned                size,
    const rpp_dpcc_bpt_t *  pixels
);

/**************************************************************************//**
 * @brief Retrieve the Bad Pixel table from the unit.
 * @param[in]       drv         initialized DPCC driver structure describing the dpcc unit instance
 * @param[inout]    size        number of supplied Bad Pixel Table entries on entry and of retrieved entries on exit
 * @param[out]      pixels      array of @c size Bad Pixel Table entries
 * @return      0 on success, error code otherwise
 *
 * The unit can make use of a Bad Pixel Table, e.g. a table with max. 2048 entries
 * specifying positions of well-known defective pixel(s).
 *
 * This function is used to retrieve the Bad Pixel Table entries from the unit
 * and return them in the supplied data structure. The @c size parameter must
 * specify the number of available entries in the @c pixels array and the function
 * retrieves at most this number of entries from the table.
 * When the function returns, the @c size parameter is updated to contain
 * the number of entries actually fetched, which may be less than the number
 * of requested entries.
 *****************************************************************************/
int rpp_dpcc_bpt_table
(
    rpp_dpcc_drv_t *    drv,
    unsigned *          size,
    rpp_dpcc_bpt_t *    pixels
);




/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_DPCC_DRV_H__ */

