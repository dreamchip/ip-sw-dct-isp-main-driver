/******************************************************************************
 *
 * Copyright 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_hdr_drv.h
 *
 * @brief   Interface of RPP HDR pipeline unit driver.
 *
 *****************************************************************************/
#ifndef __RPP_HDR_DRV_H__
#define __RPP_HDR_DRV_H__

#include "rpp_config.h"
#include <rpp_hdr_types.h>

#include "rpp_main_pre_drv.h"
#include "rpp_rmap_drv.h"
#include "rpp_rmap_meas_drv.h"
#include "rpp_main_post_drv.h"
#include "rpp_out_drv.h"
#include "rpp_mv_out_drv.h"

#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppHdrToplvlDrv RPP - HDR Image Processing Pipeline Unit Driver
 * @brief RPP - HDR Image Processing Pipeline Unit Driver
 *
 * This section contains definitions and functions for the RPP HDR
 * image processing pipeline.
 *
 * The RPP HDR consists of a number of pipeline stages:
 *  - Pre-Fusion acquisition pipelines MAIN_PRE_1, MAIN_PRE_2 and, optionally, MAIN_PRE_3
 *  - Fusion sub-units RMAP and RMAP_MEAS for radiance-mapping
 *  - Post-Fusion processing pipelines MAIN_POST
 *  - Human Vision output pipeline with format conversion and output control
 *  - Machine Vision output pipeline with format conversion and output control
 *
 * This driver is a high-level driver that provides a common initialization
 * routine for all the pipeline stages and provides high-level control
 * over the RPP HDR Unit's Interrupt controller, frame-synchronous register
 * updates, synchronous Input/Output enable and the unit's soft-reset.
 * @{
 *****************************************************************************/


/* maximum number of MAIN_PRE pipelines. */
#define RPP_MAIN_PRE_NUM    3

/**************************************************************************//**
 * @brief RPP HDR Interrupt flag description
 *
 * This datatype describes the available interrupts supported by the RPP HDR
 * units. An instance of this datatype must be passed to all functions
 * dealing with interrupts.
 *****************************************************************************/
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
typedef union {
    uint32_t value;
    struct {
        
        /** @brief represents the RMAP IRQ
         *
         * This flag, when specified, represents the IRQ of the RMAP sub-unit.
         * It is raised when the radiance mapping has completed for a frame. */
        uint32_t    rmap_irq:1;
        /** @brief represents the RMAP_MEAS IRQ
         *
         * This flag, when specified, represents the IRQ of the RMAP_MEAS sub-unit
         * that is raised when the radiance measurements have been completed for
         * a frame. */
        uint32_t    rmap_meas_irq:1;


        /** @brief represents the RPP_OUT OFF IRQ
         *
         * This flag, when specified, represents the IRQ of the output interface
         * in the Human Vision output pipeline stage that is raised when the output
         * interface is turned off. */
        uint32_t    out_off_irq:1;
        /** @brief represents the RPP_OUT FRAME_OUT IRQ
         *
         * This flag, when specified, represents the IRQ of the output interface
         * in the Human Vision output pipeline stage that is raised when the output
         * of a frame has completed. */
        uint32_t    out_frame_out_irq:1;


        /** @brief represents the MAIN_PRE3 ACQ V_START_EDGE IRQ
         *
         * This flag, when specified, represents the IRQ of the ACQ sub-unit in the
         * MAIN_PRE3 pipeline stage that is raised when an active VSYNC condition
         * has been detected. */
        uint32_t    main_pre3_acq_vsync_irq:1;
        /** @brief represents the MAIN_PRE3 ACQ H_START_EDGE IRQ
         *
         * This flag, when specified, represents the IRQ of the ACQ sub-unit in the
         * MAIN_PRE3 pipeline stage that is raised when an active HSYNC condition
         * has been detected. */
        uint32_t    main_pre3_acq_hsync_irq:1;
        /** @brief represents the MAIN_PRE3 ACQ FRAME_IN IRQ
         *
         * This flag, when specified, represents the IRQ of the ACQ sub-unit in the
         * MAIN_PRE3 pipeline stage that is raised when an input frame has been
         * acquired. */
        uint32_t    main_pre3_acq_frame_in_irq:1;
        /** @brief represents the MAIN_PRE3 HIST IRQ
         *
         * This flag, when specified, represents the IRQ of the HIST sub-unit in the
         * MAIN_PRE3 pipeline stage that is raised when the unit has completed the
         * histogram calculation for a frame. */
        uint32_t    main_pre3_hist_irq:1;
        /** @brief represents the MAIN_PRE3 EXM IRQ
         *
         * This flag, when specified, represents the IRQ of the EXM sub-unit in the
         * MAIN_PRE3 pipeline stage that is raised when the unit has completed the
         * exposure measurement for a frame. */
        uint32_t    main_pre3_exm_irq:1;


        /** @brief represents the MAIN_PRE2 ACQ V_START_EDGE IRQ
         *
         * This flag, when specified, represents the IRQ of the ACQ sub-unit in the
         * MAIN_PRE2 pipeline stage that is raised when an active VSYNC condition
         * has been detected. */
        uint32_t    main_pre2_acq_vsync_irq:1;
        /** @brief represents the MAIN_PRE2 ACQ H_START_EDGE IRQ
         *
         * This flag, when specified, represents the IRQ of the ACQ sub-unit in the
         * MAIN_PRE2 pipeline stage that is raised when an active HSYNC condition
         * has been detected. */
        uint32_t    main_pre2_acq_hsync_irq:1;
        /** @brief represents the MAIN_PRE2 ACQ FRAME_IN IRQ
         *
         * This flag, when specified, represents the IRQ of the ACQ sub-unit in the
         * MAIN_PRE2 pipeline stage that is raised when an input frame has been
         * acquired. */
        uint32_t    main_pre2_acq_frame_in_irq:1;
        /** @brief represents the MAIN_PRE2 HIST IRQ
         *
         * This flag, when specified, represents the IRQ of the HIST sub-unit in the
         * MAIN_PRE2 pipeline stage that is raised when the unit has completed the
         * histogram calculation for a frame. */
        uint32_t    main_pre2_hist_irq:1;
        /** @brief represents the MAIN_PRE2 EXM IRQ
         *
         * This flag, when specified, represents the IRQ of the EXM sub-unit in the
         * MAIN_PRE2 pipeline stage that is raised when the unit has completed the
         * exposure measurement for a frame. */
        uint32_t    main_pre2_exm_irq:1;


        /** @brief represents the MAIN_PRE1 ACQ V_START_EDGE IRQ
         *
         * This flag, when specified, represents the IRQ of the ACQ sub-unit in the
         * MAIN_PRE1 pipeline stage that is raised when an active VSYNC condition
         * has been detected. */
        uint32_t    main_pre1_acq_vsync_irq:1;
        /** @brief represents the MAIN_PRE1 ACQ H_START_EDGE IRQ
         *
         * This flag, when specified, represents the IRQ of the ACQ sub-unit in the
         * MAIN_PRE1 pipeline stage that is raised when an active HSYNC condition
         * has been detected. */
        uint32_t    main_pre1_acq_hsync_irq:1;
        /** @brief represents the MAIN_PRE1 ACQ FRAME_IN IRQ
         *
         * This flag, when specified, represents the IRQ of the ACQ sub-unit in the
         * MAIN_PRE1 pipeline stage that is raised when an input frame has been
         * acquired. */
        uint32_t    main_pre1_acq_frame_in_irq:1;
        /** @brief represents the MAIN_PRE1 HIST IRQ
         *
         * This flag, when specified, represents the IRQ of the HIST sub-unit in the
         * MAIN_PRE1 pipeline stage that is raised when the unit has completed the
         * histogram calculation for a frame. */
        uint32_t    main_pre1_hist_irq:1;
        /** @brief represents the MAIN_PRE1 EXM IRQ
         *
         * This flag, when specified, represents the IRQ of the EXM sub-unit in the
         * MAIN_PRE1 pipeline stage that is raised when the unit has completed the
         * exposure measurement for a frame. */
        uint32_t    main_pre1_exm_irq:1;


        /** @brief represents the MAIN_POST LTM_MEAS IRQ
         *
         * This flag, when specified, represents the IRQ of the LTM_MEAS sub-unit
         * in the MAIN_POST pipeline stage that is raised when the unit has completed
         * LTM measurements on a frame. */
        uint32_t   main_post_ltm_meas_irq:1;
        /** @brief represents the MAIN_POST HIST IRQ
         *
         * This flag, when specified, represents the IRQ of the HIST sub-unit
         * in the MAIN_POST pipeline stage that is raised when the unit has completed
         * histogram calculations for a frame. */
        uint32_t    main_post_hist_irq:1;
        /** @brief represents the MAIN_POST AWB_MEAS IRQ
         *
         * This flag, when specified, represents the IRQ of the AWB_MEAS sub-unit
         * in the MAIN_POST pipeline stage that is raised when the unit has completed
         * white-balance measurements for a frame. */
        uint32_t    main_post_awb_meas_irq:1;


        /** @brief represents the MV_OUT OFF IRQ
         *
         * This flag, when specified, represents the IRQ of the output interface
         * in the Machine Vision output pipeline stage that is raised when the output
         * interface is turned off. */
        uint32_t    mv_out_off_irq:1;
        /** @brief represents the MV_OUT FRAME_OUT IRQ
         *
         * This flag, when specified, represents the IRQ of the output interface
         * in the Machine Vision output pipeline stage that is raised when the output
         * of a frame has completed. */
        uint32_t    mv_out_frame_out_irq:1;


        /** @brief represents the MAIN_PRE1 DPCC IRQ
         *
         * This flag, when specified, represents the IRQ of the DPCC sub-unit
         * in the MAIN_PRE1 pipeline stage. It is raised when the sub-unit has
         * sent the detected defective pixels to the output DMA. */
        uint32_t    main_pre1_dpcc_irq:1;
        /** @brief represents the MAIN_PRE2 DPCC IRQ
         *
         * This flag, when specified, represents the IRQ of the DPCC sub-unit
         * in the MAIN_PRE2 pipeline stage. It is raised when the sub-unit has
         * sent the detected defective pixels to the output DMA. */
        uint32_t    main_pre2_dpcc_irq:1;
        /** @brief represents the MAIN_PRE3 DPCC IRQ
         *
         * This flag, when specified, represents the IRQ of the DPCC sub-unit
         * in the MAIN_PRE3 pipeline stage. It is raised when the sub-unit has
         * sent the detected defective pixels to the output DMA. */
        uint32_t    main_pre3_dpcc_irq:1;


        /** @brief represents the MAIN_PRE1 hist256 IRQ
         *
         * This flag, when specified, represents the IRQ of the hist256 sub-unit
         * in the MAIN_PRE1 pipeline stage. It is raised when the sub-unit has
         * sent the calculated histogram to the output DMA. */
        uint32_t    main_pre1_hist256_irq:1;

        /* remaining flags, not assigned and to be masked. */
        uint32_t    _unused:4;
    };
} rpp_hdr_irqflag_t;
#pragma GCC diagnostic pop

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
typedef union {
    uint32_t value;
    struct {
        uint32_t main_pre3_acq_outform_size_err:1;   // MAIN_PRE3_ACQ_OUTFORM_SIZE_ERR  0x00000001U
        uint32_t main_pre3_acq_inform_size_err:1;    // MAIN_PRE3_ACQ_INFORM_SIZE_ERR   0x00000002U
        uint32_t main_pre2_acq_outform_size_err:1;   // MAIN_PRE2_ACQ_OUTFORM_SIZE_ERR  0x00000004U
        uint32_t main_pre2_acq_inform_size_err:1;    // MAIN_PRE2_ACQ_INFORM_SIZE_ERR   0x00000008U
        uint32_t main_pre1_acq_outform_size_err:1;   // MAIN_PRE1_ACQ_OUTFORM_SIZE_ERR  0x00000010U
        uint32_t main_pre1_acq_inform_size_err:1;    // MAIN_PRE1_ACQ_INFORM_SIZE_ERR   0x00000020U
        uint32_t main_pre1_acq_fifo_ovflw:1;         // MAIN_PRE1_ACQ_FIFO_OVFLW        0x00000040U
        uint32_t main_pre2_acq_fifo_ovflw:1;         // MAIN_PRE2_ACQ_FIFO_OVFLW        0x00000080U
        uint32_t main_pre3_acq_fifo_ovflw:1;         // MAIN_PRE3_ACQ_FIFO_OVFLW        0x00000100U
        uint32_t is_out_size_err:1;                  // IS_OUT_SIZE_ERR                 0x00000200U
        uint32_t mv_out_size_err:1;                  // MV_OUT_SIZE_ERR                 0x00000400U
        uint32_t hdr_out_time_fault:1;               // HDR_OUT_TIME_FAULT              0x00000800U
        uint32_t hdr_out_size_fault:1;               // HDR_OUT_SIZE_FAULT              0x00001000U
        uint32_t hdr_mv_out_time_fault:1;            // HDR_MV_OUT_TIME_FAULT           0x00002000U
        uint32_t hdr_mv_out_size_fault:1;            // HDR_MV_OUT_SIZE_FAULT           0x00004000U
        uint32_t main_time_fault:1;                  // MAIN_TIME_FAULT                 0x00008000U
        uint32_t main_size_fault:1;                  // MAIN_SIZE_FAULT                 0x00010000U
        uint32_t main_pre1_time_fault:1;             // MAIN_PRE1_TIME_FAULT            0x00020000U
        uint32_t main_pre1_size_fault:1;             // MAIN_PRE1_SIZE_FAULT            0x00040000U
        uint32_t main_pre2_time_fault:1;             // MAIN_PRE2_TIME_FAULT            0x00080000U
        uint32_t main_pre2_size_fault:1;             // MAIN_PRE2_SIZE_FAULT            0x00100000U
        uint32_t main_pre3_time_fault:1;             // MAIN_PRE3_TIME_FAULT            0x00200000U
        uint32_t main_pre3_size_fault:1;             // MAIN_PRE3_SIZE_FAULT            0x00400000U
        uint32_t _unused:9;
    };
} rpp_hdr_fmuflag_t;
#pragma GCC diagnostic pop

/**************************************************************************//**
 * @brief RPP HDR Image Processing Pipeline specific driver structure
 *
 * This structure encapsulates all data required by the sub-units and pipeline
 * stages of the RPP HDR Image Processing pipeline.
 *
 * @note If a functional MAIN_PRE_3 pipeline stage is not available in
 * the design, the MAIN_PRE_3 entry of the driver-specific structure shall
 * not be used.
 *****************************************************************************/
typedef struct {
    rpp_device *            dev;
    uint32_t                base;

    /** @brief contains the RPP HDR unit's version number after initialization */
    uint16_t                version;
    /** @brief contains the number of MAIN_PRE pipelines after initialization */
    uint16_t                inputs;

    /** @brief driver-specific portion of the included MAIN_PRE_1 pipeline stage */
    rpp_main_pre_drv_t      main_pre1;
    /** @brief driver-specific portion of the included MAIN_PRE_2 pipeline stage */
    rpp_main_pre_drv_t      main_pre2;
    /** @brief driver-specific portion of the included MAIN_PRE_3 pipeline stage.
     *
     * @note This pipeline stage may not be included in the present RPP HDR design. */
    rpp_main_pre_drv_t      main_pre3;
    /** @brief driver-specific portion of the included RMAP sub-unit */
    rpp_rmap_drv_t          rmap;
    /** @brief driver-specific portion of the included RMAP_MEAS sub-unit */
    rpp_rmap_meas_drv_t     rmap_meas;
    /** @brief driver-specific portion of the included MAIN_POST pipeline stage */
    rpp_main_post_drv_t     main_post;
    /** @brief driver-specific portion of the included RPP_OUT pipeline stage for Human Vision. */
    rpp_out_drv_t           out;
    /** @brief driver-specific portion of the included RPP_MV_OUT pipeline stage for Machine Vision. */
    rpp_mv_out_drv_t        mv_out;
} rpp_hdr_drv_t;


/**************************************************************************//**
 * @brief Initialize the included pipeline stages and sub-units.
 * @param[in]       dev     rpp_device structure to be used for all register accesses
 * @param[in]       inputs  the number of available MAIN_PRE_x acquisition pipelines
 * @param[inout]    drv     RPP_HDR driver structure to be initialized
 * @return          0 on success, error-code otherwise
 *
 * This function initializes the @c drv structure for future uses of the drivers
 * for all sub-units included in the RPP_HDR pipeline. If any
 * initialization error occurs, the function returns an error code.
 *
 * The @c inputs parameter specifies the number of acquisition pipelines
 * available in the given RPP HDR implementation. It can be set to either
 * two or three, other values are not allowed.
 *****************************************************************************/
int rpp_hdr_init(rpp_device * dev, uint32_t base, unsigned inputs, rpp_hdr_drv_t * drv);

/**************************************************************************//**
 * @brief Globally enable or disable the sensor interfaces of the acquisition units.
 * @param[in]   drv     initialized RPP_HDR driver structure describing the rpp unit instance
 * @param[in]   on      enable state to set (0: disabled, otherwise enabled)
 * @return  0 on success, error-code otherwise
 *
 * This function finally activates the sensor interfaces of all MAIN_PRE_X
 * acquisition pipelines synchronously. Each MAIN_PRE_x pipeline stage ACQ
 * sub-unit that is going to be used for image processing must have been
 * enabled in advance.
 *
 * The register programmed here holds or releases all enabled ACQ sensor
 * interfaces (input formatter, INFORM) synchronously. ACQ DMA inputs are
 * not affected by this setting.
 *****************************************************************************/
int rpp_hdr_input_enable(rpp_hdr_drv_t * drv, unsigned on);

/**************************************************************************//**
 * @brief Returns the global enable or disable state the sensor interfaces of the acquisition units.
 * @param[in]   drv     initialized RPP_HDR driver structure describing the rpp unit instance
 * @param[out]  on      input enable state (0: disabled, otherwise enabled)
 * @return  0 on success, error-code otherwise
 *
 * This function retrieves the global ACQ input formatter enable state
 * from the unit and returns it.
 *****************************************************************************/
int rpp_hdr_input_enabled(rpp_hdr_drv_t * drv, unsigned * const on);

/**************************************************************************//**
 * @brief Globally enable the output interfaces.
 * @param[in]   drv     initialized RPP_HDR driver structure describing the rpp unit instance
 * @param[in]   hv_on   whether to enable Human Vision output (0: not enabled, otherwise enabled)
 * @param[in]   mv_on   whether to enable Machine Vision output (0: not enabled, otherwise enabled)
 * @return  0 on success, error-code otherwise
 *
 * This function can be used to synchronously activate both of the output
 * interfaces of the RPP HDR. Both outputs can separately be enabled using
 * the driver functions of the corresponding pipeline stage. The programming
 * delay, however, may cause the interfaces to become enabled at different
 * times.
 *
 * This function provides a mechanism to synchronously enable both output
 * interfaces at the same time.
 *****************************************************************************/
int rpp_hdr_output_enable(rpp_hdr_drv_t * drv, unsigned hv_on, unsigned mv_on);

/**************************************************************************//**
 * @brief Globally disable the output interfaces.
 * @param[in]   drv     initialized RPP_HDR driver structure describing the rpp unit instance
 * @param[in]   hv_off  whether to disable Human Vision output (0: not disabled, otherwise disabled)
 * @param[in]   mv_off  whether to disable Machine Vision output (0: not disabled, otherwise disabled)
 * @return  0 on success, error-code otherwise
 *
 * This function can be used to synchronously deactivate both of the output
 * interfaces of the RPP HDR. Both outputs can separately be disabled using
 * the driver functions of the corresponding pipeline stage. The programming
 * delay, however, may cause the interfaces to become disabled at different
 * times.
 *
 * This function provides a mechanism to synchronously disable both output
 * interfaces at the same time.
 *****************************************************************************/
int rpp_hdr_output_disable(rpp_hdr_drv_t * drv, unsigned hv_off, unsigned mv_off);

/**************************************************************************//**
 * @brief Globally request shadow register updates.
 * @param[in]   drv     initialized RPP_HDR driver structure describing the rpp unit instance
 * @param[in]   force   whether to immediately update shadow registers (0: at frame end, otherwise immediately)
 * @return  0 on success, error-code otherwise
 *
 * This function must be called to request all shadow registers in the RPP HDR
 * unit to be updated.
 *
 * Some of the configuration of the RPP HDR is not activated immediately when
 * the configuration registers are programmed. Instead, the unit operates
 * using copies of the configuration registers (shadow registers, mostly
 * inaccessible themselves) which are kept at their current value until
 * the unit is informed to copy the configuration register values into
 * the shadow registers. The shadow register update, when requested, will
 * be performed synchronously by all sub-units once processing of the current
 * frame is complete (at the next end-of-frame signal).
 *
 * This mechanism allows to consistently apply a new image processing
 * configuration during operation without causing defective intermediate
 * images when the reprogramming extends over multiple registers of multiple
 * sub-units.
 *
 * The procedure to apply a new configuration to the RPP HDR during live
 * operation shall be as follows:
 *  -# reprogram the registers of all sub-units to their new values
 *  -# call this function to request an update of the shadow registers
 *     with the @c force flag set to 0
 *  -# wait for an end-of-frame interrupt
 *  -# eventually go back to step #1.
 *
 * @note There is no way to acquire from the unit whether a shadow register
 * update has already been requested, or to cancel a shadow register update
 * request, so it is mandatory to wait for the next end-of-frame
 * signal (at which time the currently programmed configuration
 * will be activated) before continuing to reprogram the RPP HDR.
 *
 * When programming the RPP HDR while being inoperational (e.g. during
 * startup), the @c force flag can be set to a non-zero value. This causes
 * the configuration registers to be copied into the shadow registers
 * without delay and the configuration will already be active for the
 * first frame after the RPP HDR is enabled. Doing so during live
 * operation is not allowed.
 */
int rpp_hdr_shadow_update(rpp_hdr_drv_t * drv, unsigned force);

/**************************************************************************//**
 * @brief Enable access to safety registers
 * @param[in]   drv initialized RPP_HDR driver structure describing the rpp unit instance
 * @param[in]   on  whether to enable access to safety registers (0: disabled, otherwise enabled)
 * @return  0 on success, error-code otherwise
 *
 * Write access to safety relevant registers (e.g. registers from the fault
 * management unit (FMU)) is forbidden by default. To enable write access to
 * those registers use this function and set the @c on argument to 1.
 *
 * After write access is done the protection should be turned on again to
 * protect against unwanted tempering.
 *****************************************************************************/
int rpp_hdr_set_fmu_safe_access_enable(rpp_hdr_drv_t * drv, unsigned on);

/**************************************************************************//**
 * @brief Get access status of safety registers
 * @param[in]   drv initialized RPP_HDR driver structure describing the rpp unit instance
 * @param[in]   on  whether access to safety registers is currently enabled (0: disabled, otherwise enabled)
 * @return  0 on success, error-code otherwise
 *
 * This function retrieves the current access enable status of safety relevant
 * registers.
 *****************************************************************************/
int rpp_hdr_get_fmu_safe_access_enable(rpp_hdr_drv_t * drv, unsigned * const on);

/**************************************************************************//**
 * @brief Assert or de-assert SOFT_RESET of the RPP HDR unit.
 * @param[in]   drv     initialized RPP_HDR driver structure describing the rpp unit instance
 * @param[in]   assert  whether to assert or deassert soft reset (0: deassert, otherwise assert)
 * @return  0 on success, error-code otherwise
 *
 * This function is used to perform a soft reset of the unit. The soft reset
 * is issued by calling this function with the @c assert parameter set to a
 * non-zero value and will persist until it is explicitly released by calling
 * this function again with @c assert set to zero.
 *
 * Note that the soft-reset may need to be asserted for a certain amount
 * of time for correct operation.
 *
 * The soft reset will cause all registers to assume their default values
 * which implies that the whole RPP HDR needs to be reprogrammed when the
 * soft reset is released.
 *****************************************************************************/
int rpp_hdr_soft_reset(rpp_hdr_drv_t * drv, unsigned assert);

/**************************************************************************//**
 * @brief Program the set of enabled IRQs into the unit.
 * @param[in]   drv     initialized RPP_HDR driver structure describing the rpp unit instance
 * @param[in]   flags   the individual IRQs to be enabled or disabled.
 * @return  0 on success, error-code otherwise
 *
 * This function updates the set of enabled IRQs in the RPP HDR. For each of
 * the IRQ flags in the @c flags argument it must be specified whether the
 * corresponding IRQ shall be enabled (1) or disabled (0).
 *****************************************************************************/
int rpp_hdr_irqflags_enable(rpp_hdr_drv_t * drv, rpp_hdr_irqflag_t const * const flags);

/**************************************************************************//**
 * @brief Retrieve the set of enabled IRQs from the unit.
 * @param[in]   drv     initialized RPP_HDR driver structure describing the rpp unit instance
 * @param[in]   flags   the individual IRQ enable states
 * @return  0 on success, error-code otherwise
 *
 * This function retrieves the set of enabled IRQs from the RPP HDR. Each of
 * the IRQ flags in the @c flags argument will be updated to indicate whether
 * the corresponding IRQ is enabled (1) or disabled (0).
 *****************************************************************************/
int rpp_hdr_irqflags_enabled(rpp_hdr_drv_t * drv, rpp_hdr_irqflag_t * const flags);

 /**************************************************************************//**
 * @brief Retrieve the set of active IRQs from the unit.
 * @param[in]   drv     initialized RPP_HDR driver structure describing the rpp unit instance
 * @param[in]   flags   the individual IRQ active states
 * @return  0 on success, error-code otherwise
 *
 * This function retrieves the set of active IRQs from the RPP HDR. Each of
 * the IRQ flags in the @c flags argument will be updated to indicate whether
 * the corresponding IRQ is active (1) or inactive (0).
 *
 * The function automatically clears the active IRQs in the unit so that,
 * when this function is called again, only those interrupts are marked
 * active that have been raised again in between.
 *****************************************************************************/
int rpp_hdr_irqflags_get(rpp_hdr_drv_t * drv, rpp_hdr_irqflag_t * const flags);

/**************************************************************************//**
 * @brief Program the set of enabled FAULTs into the unit.
 * @param[in]   drv     initialized RPP_HDR driver structure describing the rpp unit instance
 * @param[in]   flags   the individual FAULTSs to be enabled or disabled.
 * @return  0 on success, error-code otherwise
 *
 * This function updates the set of enabled FALTSs in the RPP HDR. For each of
 * the FAULT flags in the @c flags argument it must be specified whether the
 * corresponding FAULT shall be enabled (1) or disabled (0).
 *****************************************************************************/
int rpp_hdr_fmuflags_enable(rpp_hdr_drv_t * drv, rpp_hdr_fmuflag_t const * const flags);

/**************************************************************************//**
 * @brief Retrieve the set of enabled FAULTs from the unit.
 * @param[in]   drv     initialized RPP_HDR driver structure describing the rpp unit instance
 * @param[in]   flags   the individual FAULT enable states
 * @return  0 on success, error-code otherwise
 *
 * This function retrieves the set of enabled FAULTs from the RPP HDR. Each of
 * the FAULT flags in the @c flags argument will be updated to indicate whether
 * the corresponding FAULT is enabled (1) or disabled (0).
 *****************************************************************************/
int rpp_hdr_fmuflags_enabled(rpp_hdr_drv_t * drv, rpp_hdr_fmuflag_t * const flags);

 /**************************************************************************//**
 * @brief Retrieve the set of active FAULTs from the unit.
 * @param[in]   drv     initialized RPP_HDR driver structure describing the rpp unit instance
 * @param[in]   flags   the individual FAULT active states
 * @return  0 on success, error-code otherwise
 *
 * This function retrieves the set of active FAULTs from the RPP HDR. Each of
 * the FAULT flags in the @c flags argument will be updated to indicate whether
 * the corresponding FAULT is active (1) or inactive (0).
 *
 * The function automatically clears the active FAULTs in the unit so that,
 * when this function is called again, only those interrupts are marked
 * active that have been raised again in between.
 *****************************************************************************/
int rpp_hdr_fmuflags_get(rpp_hdr_drv_t * drv, rpp_hdr_fmuflag_t * const flags);

/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_HDR_DRV_H__ */

