/******************************************************************************
 *
 * Copyright 2019 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_rgbdenoise_drv.h
 *
 * @brief   Interface of RGBDENOISE unit driver
 *
 *****************************************************************************/
#ifndef __RPP_RGBDENOISE_DRV_H__
#define __RPP_RGBDENOISE_DRV_H__

#include "rpp_config.h"
#include "rpp_ccor_drv.h"
#include <rpp_hdr_types.h>

#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppRgbDenoiseDrv RPP - RGB Denoising Unit Driver
 * RPP - RGB Denoising Unit Driver
 *
 * The RGB Denoising unit provides ways to improve RGB image data.
 * The unit actually operates in YCrCb color space and converts
 * incoming pixel data from RGB to YUV on-the-fly. All calculations
 * are performed separately (with different filter parameters)
 * for luminance and chrominance values.
 *
 * Denoising is an operation that removes high-frequent "noise"
 * from the image to smoothen the appearance of areas with similar
 * colors. All configuration parameters for the denoising operation
 * are contained in a structure called rpp_rgbdenoise_cfg_t.
 * An instance of such a structure, readily set-up, must be passed to
 * the rpp_rgbdenoise_set_denoise_config() function to actually configure the
 * relevant parameters.
 *
 * Additionally, the color conversion matrix from RGB to YCrCb
 * color space must be defined. The back transform from YCbCr to
 * RGB must be configured in the adjacent @ref RppShrpcnrDrv
 * "rpp_shrpcnr" unit.
 *
 * The denoising and colorspace transformation parameters are subject
 * to register shadowing, which means
 * that these registers can be updated during normal operation without
 * having an immediate effect on the output. The new values will only be
 * used when explicitly requested, normally synchronized with the
 * end-of-frame.
 *
 * @{
 *
 *****************************************************************************/

/**************************************************************************//**
 * @brief Number of intensity filter coefficients.
 *
 * There are two different sets of intensity filter coefficients: one for
 * luminance values and one for chrominance values.
 *****************************************************************************/
#define RPP_RGBDENOISE_IF_COEFF_NUM 32

/**************************************************************************//**
 * @brief Number of spatial filter coefficients.
 *
 * There are two different sets of spatial filter coefficients: one for
 * luminance values and one for chrominance values.
 *****************************************************************************/
#define RPP_RGBDENOISE_SF_COEFF_NUM 4

/**************************************************************************//**
 * @brief Maximum value of intensity shift parameters.
 *
 * This macro defines the maximum value that is allowed for any of the
 * intensity shift parameters.
 *****************************************************************************/
#define RPP_RGBDENOISE_MAX_SHIFT    9


/**************************************************************************//**
 * @brief Datatype describing the set of denoising filter parameters.
 *****************************************************************************/
typedef struct
{
    /** @brief Intensity filter coefficients for luminance.
     *
     * This member holds the intensity filter coefficients for
     * denoising luminance values. The sum of the coefficients
     * must not be zero, each is 4 bit wide. */
    uint8_t     if_cy[RPP_RGBDENOISE_IF_COEFF_NUM];
    /** @brief Intensity filter coefficients for chrominance.
     *
     * This member holds the intensity filter coefficients for
     * denoising chrominance values. The sum of the coefficients
     * must not be zero, each is 4 bit wide. */
    uint8_t     if_cc[RPP_RGBDENOISE_IF_COEFF_NUM];
    /** @brief Spatial filter coefficients for luminance.
     *
     * This member holds the spatial filter coefficients for
     * denoising luminance values. The sum of the coefficients
     * must not be zero, each is 4 bit wide. */
    uint8_t     sf_cy[RPP_RGBDENOISE_SF_COEFF_NUM];
    /** @brief Spatial filter coefficients for chrominance.
     *
     * This member holds the spatial filter coefficients for
     * denoising chrominance values. The sum of the coefficients
     * must not be zero, each is 4 bit wide. */
    uint8_t     sf_cc[RPP_RGBDENOISE_SF_COEFF_NUM];
    /** @brief Intensity difference shift value for luminance.
     *
     * This value is the number of bits by which to right-shift
     * luminance intensity difference values, which means that
     * they are divided by 2^shift_y.
     *
     * The maximum value allowed is given by RPP_RGBDENOISE_MAX_SHIFT. */
    uint8_t     shift_y;
    /** @brief Intensity difference shift value for chrominance.
     *
     * This value is the number of bits by which to right-shift
     * chrominance intensity difference values, which means that
     * they are divided by 2^shift_c.
     *
     * The maximum value allowed is given by RPP_RGBDENOISE_MAX_SHIFT. */
    uint8_t     shift_c;
} rpp_rgbdenoise_cfg_t;


/**************************************************************************//**
 * @brief RPP RGBDENOISE specific driver structure
 *
 * This structure encapsulates all data required by the RPP RGBDENOISE driver to
 * operate a specific instance of the RPP RGBDENOISE unit on a given RPPHDR device.
 * The data fields contained in this structure are intended to be
 * private to the driver and not to be modified by any higher-level
 * software component.
 *
 * It is required to have a separate instance of this structure for each
 * separate RPP RGBDENOISE unit in a given RPPHDR device. Each of these structures
 * must be initialized before use by calling the function @c rpp_rgbdenoise_init()
 * with suitable parameters.
 *
 * After initialization, the @c colorbits member variable is set to the
 * number of bits per color value. This information is needed for the
 * preparation of the colorspace transformation CCOR profile.
 *
 * The structure is exported here so that instances can statically be declared
 * by higher-level software without the need for dynamic memory allocation.
 *****************************************************************************/
typedef struct {
    uint16_t        version;
    uint16_t        colorbits;
    uint32_t        base;
    rpp_device *    dev;
} rpp_rgbdenoise_drv_t;


/**************************************************************************//**
 * @brief Initialize the rpp rgbdenoise module.
 * @param[in]       dev     rpp_device structure to be used for all register accesses
 * @param[in]       base    base address of RPP-RGBDENOISE module
 * @param[inout]    drv     RGBDENOISE driver structure to be initialized
 * @return      0 on success, error-code otherwise
 *
 * This function initializes the @c drv structure for future uses of the driver
 * on the given instance of the rpp rgbdenoise unit. It fills the @c drv structure
 * with data required for operating on the given instance and caches data that
 * describes the features of the rgbdenoise unit instance.
 *
 * This function needs to be called once for every instance of the rpp rgbdenoise
 * unit to operate, each time with a different @c drv argument intended for
 * that very instance of the rpp rgbdenoise unit only.
 *****************************************************************************/
int rpp_rgbdenoise_init(rpp_device * dev, uint32_t base, rpp_rgbdenoise_drv_t * drv);

/**************************************************************************//**
 * @brief Enable or disable the rpp rgbdenoise module.
 * @param[in]   drv     initialized RGBDENOISE driver structure describing the rgbdenoise unit instance
 * @param[in]   on      enable state to set (0: disable, otherwise enable)
 * @return      0 on success, error-code otherwise
 *
 * Even if the unit is disabled, a suitable colorspace transformation
 * profile must be programmed because the RGB to YUV conversion is
 * always performed and passed on to the next unit in the pipeline stage.
 *
 * If the unit is enabled, a suitable denoising configuration must also
 * have been programmed.
 *****************************************************************************/
int rpp_rgbdenoise_enable(rpp_rgbdenoise_drv_t * drv, const unsigned on);

/**************************************************************************//**
 * @brief Return enable status of the rpp rgbdenoise module.
 * @param[in]   drv     initialized RGBDENOISE driver structure describing the rgbdenoise unit instance
 * @param[out]  on      current enable state (0: disabled, otherwise enabled)
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int rpp_rgbdenoise_enabled(rpp_rgbdenoise_drv_t * drv, unsigned * const on);

/**************************************************************************//**
 * @brief Program denoising configuration into the rpp rgbdenoise module.
 * @param[in]   drv     initialized RGBDENOISE driver structure describing the rgbdenoise unit instance
 * @param[in]   config  pointer to an initialized denoising parameter set descriptor
 * @return      0 on success, error-code otherwise
 *
 * This function programs a new set of denoising parameters into the
 * rpp_rgbdenoise unit. The new parameter set is not activated immediately
 * because it is subject to shadow register handling. It may become valid
 * only for the next frame.
 *****************************************************************************/
int rpp_rgbdenoise_set_denoise_config
(
    rpp_rgbdenoise_drv_t *              drv,
    const rpp_rgbdenoise_cfg_t * const  config
);

/**************************************************************************//**
 * @brief Retrieve the denoising configuration for the next frame from the rpp rgbdenoise module.
 * @param[in]   drv     initialized RGBDENOISE driver structure describing the rgbdenoise unit instance
 * @param[out]  config  pointer to an denoising parameter set descriptor
 * @return      0 on success, error-code otherwise
 *
 * This function retrieves from the unit the set of programmed denoising filter
 * parameters. These values may not be active before the next frame is
 * processed (if an update of the shadow registers has been requested).
 *****************************************************************************/
int rpp_rgbdenoise_denoise_config
(
    rpp_rgbdenoise_drv_t *          drv,
    rpp_rgbdenoise_cfg_t * const    config
);


/**************************************************************************//**
 * @brief Program RGB to YUV transformation parameters into the rpp rgbdenoise module.
 * @param[in]   drv     initialized RGBDENOISE driver structure describing the rgbdenoise unit instance
 * @param[in]   profile pointer to an initialized CCOR profile instance
 * @return      0 on success, error-code otherwise
 *
 * This function programs a new set of parameters for performing the
 * RGB to YUV colorspace conversion. The new parameter set is not activated
 * immediately because it is subject to shadow register handling. It may
 * become valid only for the next frame.
 *****************************************************************************/
int rpp_rgbdenoise_set_profile
(
    rpp_rgbdenoise_drv_t *              drv,
    const rpp_ccor_profile_t * const    profile
);


/**************************************************************************//**
 * @brief Retrieve RGB to YUV transformation parameters for the next frame from the rpp rgbdenoise module.
 * @param[in]   drv     initialized RGBDENOISE driver structure describing the rgbdenoise unit instance
 * @param[out]  profile pointer to an CCOR profile instance
 * @return      0 on success, error-code otherwise
 *
 * This function retrieves from the unit the set of parameters for the RGB to
 * YUV transformation. These values may not be active before the next frame is
 * processed (if an update of the shadow registers has been requested).
 *****************************************************************************/
int rpp_rgbdenoise_profile
(
    rpp_rgbdenoise_drv_t *      drv,
    rpp_ccor_profile_t * const  profile
);


/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_RGBDENOISE_DRV_H__ */

