/******************************************************************************
 *
 * Copyright 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_hist256_drv.h
 *
 * @brief   Interface of 256 bin histogram unit driver
 *
 *****************************************************************************/
#ifndef __RPP_HIST256_DRV_H__
#define __RPP_HIST256_DRV_H__

#include "rpp_config.h"
#include <rpp_hdr_types.h>

#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppHist256Drv RPP - 256 bin Histogram unit driver
 * RPP - 256 bin Histogram Measurement unit driver
 *
 * This driver performs a histogram evaluation of the incoming
 * image data and counts the pixels into one of 256 bins, depending
 * on their color value. There are four separate sets of 256 bins,
 * one for each color component of the Bayer RGB image.
 *
 * The pixels to be considered for measurements can firstly be defined
 * by an observation window that may cover only parts of the full image.
 *
 * Secondly, there is an option to define a region of interest in color
 * space. A scaling factor and an offset can be defined. After subtracting
 * the offset and scaling the color values, the color samples are clipped
 * to the color value range.
 *
 * Thirdly, a "gamma" curve can be applied to the color samples before they
 * are sorted into the histogram bins. This is useful for, e.g., transforming
 * the color samples onto a logarithmic scale before calculating the
 * histogram. This "gamma" correction is typically
 * a non-linear gradient, so this curve is represented
 * by piece-wise linear approximation. The whole range is divided into 16 sections
 * defined by x-axis (input color value) segments dx1...16 and their begin
 * and end values on the y-axis (output color value) y0...y16.
 * 
 * @image html degamma_curve.png "Gamma Curve Definition" width=\textwidth
 * @image latex degamma_curve.png "Gamma Curve Definition" width=\textwidth
 * 
 * The segment widths in x direction (input color) are to be defined
 * in a 2^(value + 8) notation, where "value" has to be written
 * to the register and is between 0 and 15. This is illustrated by the
 * following examples:
 *  - value = 0, dx = 2^(0 + 8) = 2^8 = 256
 *  - value = 1, dx = 2^(1 + 8) = 2^9 = 512
 *  - ...
 *  - value = 15, dx = 2^(15 + 8) = 2^23 = 8388608
 *
 * This scheme allows for a high precision at very low color values
 * where the logarithm is most non-linear.
 * If the x-axis segment widths are defined to not cover the whole input color range,
 * the last segment is automatically linearly extended, which means that the curve
 * is assumed to continue with the slope of the last segment. On the opposite, if the
 * x-axis is defined to span more than the input color range, only the relevant part
 * of the curve is used and the remaining part is ignored.
 *
 * @note There is no interface defined by the driver to read the measured
 * histogram values from the unit. Instead, the unit will stream the measurement
 * results to a data port, which may have a separate processing unit or
 * a DMA attached.
 *
 * @note All configuration of the unit is subject to register
 * shadowing and will not have an immediate effect. Any configuration
 * programmed to the unit will be activated synchronized with an
 * end-of-frame signal when the shadow register update has been
 * requested in the top-level RPP unit register.
 * The currently active configuration cannot be retrieved from
 * the unit.
 * @{
 *
 *****************************************************************************/


/**************************************************************************//**
 * @brief Maximum number of hist256 gamma curve points.
 *****************************************************************************/
#define RPP_HIST256_CURVE_POINTS    17

/**************************************************************************//**
 * @brief data type representing a piece-wise linear transformation curve for the hist256 module
 *
 * This structure is intended to hold all required parameters specifying
 * the piece-wise linear definition of the color samples transformation curve
 * for the rpp_hist256 unit.

 * The dx array holds the shift offsets used to compute the x-axis segment
 * sizes and are allowed to be in the range between 0 and 15. Here, the
 * \c dx[0] value is required to be zero and is ignored when programming
 * the unit.

 * The y array defines the curve by specifying the y-axis values at the
 * borders of the x-axis segments. The first value \c y[0] specifies the start
 * of the curve at \c x=0. All values must be between 0 and
 * \c 2<sup>bitwidth</sup>-1.
 *****************************************************************************/
typedef struct rpp_hist256_curve_s
{
    uint8_t     dx[RPP_HIST256_CURVE_POINTS];
    uint32_t    y[RPP_HIST256_CURVE_POINTS];
} rpp_hist256_curve_t;


/**************************************************************************//**
 * @brief Macro to set a point in the transformation curve.
 * @param c     curve to modify (structure, not pointer)
 * @param idx   index of curve point to modify
 * @param ptx   dx-value to configure for the curve point
 * @param pty   y-value to configure for the curve point
 *****************************************************************************/
#define RPP_HIST256_CURVE_SET_POINT(c, idx, ptx, pty)   \
    c.dx[idx] = ptx;                                    \
    c.y[idx]  = pty;


/**************************************************************************//**
 * @brief Macro to set the curve to a 24-bit LOG2 curve
 * @param c     curve to modify (structure, not pointer)
 *
 * This macro prepares the curve for mapping input image color samples onto
 * a logarithmic scale. After taking the log2() of the color samples, the
 * logarithmic values are scaled up to fill the whole value range of 24-bit
 * color samples according to the following formula
 * @code
 * y = log2(x) / colorbits * (1 << colorbits) with colorbits == 24,
 * @endcode
 * The dx values have been chosen to fit the log2() curve most closely.
 *****************************************************************************/
#define RPP_HIST256_CURVE_SET_LOG2(c)                   \
{                                                       \
    RPP_HIST256_CURVE_SET_POINT(c,  0,  0, 0);          \
    RPP_HIST256_CURVE_SET_POINT(c,  1,  0, 5592405);    \
    RPP_HIST256_CURVE_SET_POINT(c,  2,  0, 6291456);    \
    RPP_HIST256_CURVE_SET_POINT(c,  3,  1, 6990506);    \
    RPP_HIST256_CURVE_SET_POINT(c,  4,  2, 7689557);    \
    RPP_HIST256_CURVE_SET_POINT(c,  5,  3, 8388608);    \
    RPP_HIST256_CURVE_SET_POINT(c,  6,  4, 9087658);    \
    RPP_HIST256_CURVE_SET_POINT(c,  7,  5, 9786709);    \
    RPP_HIST256_CURVE_SET_POINT(c,  8,  6, 10485759);   \
    RPP_HIST256_CURVE_SET_POINT(c,  9,  7, 11184810);   \
    RPP_HIST256_CURVE_SET_POINT(c, 10,  8, 11883861);   \
    RPP_HIST256_CURVE_SET_POINT(c, 11,  9, 12582911);   \
    RPP_HIST256_CURVE_SET_POINT(c, 12, 11, 13690880);   \
    RPP_HIST256_CURVE_SET_POINT(c, 13, 12, 14545394);   \
    RPP_HIST256_CURVE_SET_POINT(c, 14, 13, 15314026);   \
    RPP_HIST256_CURVE_SET_POINT(c, 15, 14, 16046145);   \
    RPP_HIST256_CURVE_SET_POINT(c, 16, 15, 16761333);   \
}

/**************************************************************************//**
 * @brief RPP histogram channel (measurement/tap point in pipeline)
 *****************************************************************************/
typedef enum rpp_hist256_measure_channel_e
{
    /** @brief measurement after ACQ (acquisition). */
    RPP_HIST256_MEASURE_CHANNEL_0 = 0,
    /** @brief measurement after HIST256 (blacklevel correction). */
    RPP_HIST256_MEASURE_CHANNEL_1 = 1,
    /** @brief measurement after GAMMA_IN (de-gamma/input linearization correction). */
    RPP_HIST256_MEASURE_CHANNEL_2 = 2,
    /** @brief measurement after LSC4 (lense-shade correction). */
    RPP_HIST256_MEASURE_CHANNEL_3 = 3,
    /** @brief measurement after DPCC (defect pixel correction). */
    RPP_HIST256_MEASURE_CHANNEL_4 = 4,
    /** @brief image data after RMAP (radiance mapping, e.g. HDR fusion) */
    RPP_HIST256_MEASURE_CHANNEL_5 = 5,
    /** @brief image data after post-fusion AWB_GAIN (white balance gain stage) */
    RPP_HIST256_MEASURE_CHANNEL_6 = 6,
    RPP_HIST256_MEASURE_CHANNEL_MAX = RPP_HIST256_MEASURE_CHANNEL_6,
} rpp_hist256_measure_channel_t;


/**************************************************************************//**
 * @brief RPP HIST256 specific driver structure
 *
 * This structure encapsulates all data required by the RPP HIST256 driver to
 * operate a specific instance of the RPP HIST256 on a given RPPHDR device.
 * The data fields contained in this structure are intended to be
 * private to the driver and not to be modified by any higher-level
 * software component.
 *
 * It is required to have a separate instance of this structure for each
 * separate RPP HIST256 module in a given RPPHDR device. Each of these structures
 * must be initialized before use by calling the function @c rpp_hist256_init()
 * with suitable parameters.
 *
 * The structure is exported here so that instances can statically be declared
 * by higher-level software without the need for dynamic memory allocation.
 *****************************************************************************/
typedef struct {
    uint32_t        version;
    uint32_t        base;
    rpp_device *    dev;
} rpp_hist256_drv_t;


/**************************************************************************//**
 * @brief Initialize the rpp hist256 module.
 * @param[in]       dev     rpp_device structure to be used for all register accesses
 * @param[in]       base    base address of RPP HIST256 module
 * @param[inout]    drv     HIST256 driver structure to be initialized
 * @return          0 on success, error code otherwise
 *
 * This function initializes the @c drv structure for future uses of the driver
 * on the given instance of the rpp hist256 module. It fills the @c drv structure
 * with data required for operating on the given instance and caches data that
 * describes the features of the hist256 module instance.
 *
 * This function needs to be called once for every instance of the rpp hist256
 * module to operate, each time with a different @c drv argument intended for
 * that very instance of the rpp hist256 unit only.
 *****************************************************************************/
int rpp_hist256_init(rpp_device * dev, uint32_t base, rpp_hist256_drv_t * drv);

/**************************************************************************//**
 * @brief Enable or disable the rpp hist256 module.
 * @param[in]   drv     initialized HIST256 driver structure describing the hist256 module instance
 * @param[in]   enabled whether the unit shall be enabled or disabled.
 * @param[in]   channel input data source to configure (@see rpp_hist256_measure_channel_e)
 * @return      0 on success, error code otherwise
 *
 * This function enables or disables histogram measurements. If a value of
 * zero is passed in @c enabled, the unit will be disabled.
 *
 * Otherwise histogram measurements are performed on the selected
 * @c channel.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_hist256_enable(rpp_hist256_drv_t * drv,
        const unsigned enabled, const rpp_hist256_measure_channel_t channel);

/**************************************************************************//**
 * @brief Query whether the HIST256 unit is enabled and which channel to measure.
 * @param[in]   drv     initialized HIST256 driver structure describing the hist256 module instance
 * @param[out]  enabled current enable status
 * @param[out]  channel current input data source selected (@see rpp_hist256_measure_channel_e)
 * @return      0 on success, error code otherwise
 *
 * This function returns the current enable status
 * of the unit along with the selected input data source.
 *****************************************************************************/
int rpp_hist256_enabled(rpp_hist256_drv_t * drv,
        unsigned * const enabled, rpp_hist256_measure_channel_t * const channel);

/**************************************************************************//**
 * @brief Programs the hist256 unit with a measurement window.
 * @param[in]   drv     initialized HIST256 driver structure describing the hist256 module instance
 * @param[in]   win     measurement window to program (@see rpp_window_t)
 * @return      0 on success, error code otherwise
 *
 * This function defines the observation window for histogram measurements.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_hist256_set_meas_window( rpp_hist256_drv_t * drv, const rpp_window_t * win);

/**************************************************************************//**
 * @brief Retrieves current measurment window from the rpp hist256 module.
 * @param[in]   drv     initialized HIST256 driver structure describing the hist256 module instance
 * @param[out]  win     current measurment window (@see rpp_window_t)
 * @return      0 on success, error code otherwise
 *
 * This function retrieves the currently configured measurement window.
 *****************************************************************************/
int rpp_hist256_meas_window(rpp_hist256_drv_t * drv, rpp_window_t * const win);

/**************************************************************************//**
 * @brief Program dynamic range of interest to the rpp hist256 module.
 * @param[in]   drv     initialized HIST256 driver structure describing the hist256 module instance
 * @param[in]   scale   unsigned FP2.8 fixed point value
 * @param[in]   offset  offset to subtract from samples
 * @return      0 on success, error code otherwise
 *
 * This function sets the offset and scale parameters that can be used
 * to define a dynamic region of interest.
 *
 * The @c offset parameter
 * is a color value that is subtracted from color samples before scaling.
 * The scaling factor is an unsigned FP2.8 fixed point value that can be
 * used to multiply offset-corrected color samples with a factor between
 * 0.0 and 3.9961. A factor of 1.0 is represented by an integer value
 * of 256.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_hist256_set_sample_range(rpp_hist256_drv_t * drv,
        const uint16_t scale, const uint32_t offset);

/**************************************************************************//**
 * @brief Retrieve dynamic range of interest from the rpp hist256 module.
 * @param[in]   drv     initialized HIST256 driver structure describing the hist256 module instance
 * @param[out]  scale   unsigned FP2.8 fixed point value
 * @param[out]  offset  offset subtracted from samples
 * @return      0 on success, error code otherwise
 *
 * This function retrieves the currently configured offset and scale
 * parameters that define a dynamic region of interest from the unit
 * and returns them.
 *****************************************************************************/
int rpp_hist256_sample_range(rpp_hist256_drv_t * drv,
        uint16_t * const scale, uint32_t * const offset);


/**************************************************************************//**
 * @brief Program a curve for color sample transformation before histogram evaluation.
 * @param[in]   drv     initialized HIST256 driver structure describing the hist256 module instance
 * @param[in]   curve   the piece-wise linear transformation curve to program
 * @return      0 on success, error code otherwise
 *
 * This function programs a new piece-wise linear transformation curve into the
 * unit to be used for transforming the incoming color samples before histogram
 * evaluation but after prescaling. The curve is not being used
 * unless sample transformation is actually enabled.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_hist256_set_curve(rpp_hist256_drv_t * drv,
        const rpp_hist256_curve_t * const curve);

/**************************************************************************//**
 * @brief Retrieve color sample transformation curve from the unit.
 * @param[in]   drv     initialized HIST256 driver structure describing the hist256 module instance
 * @param[out]  curve   the piece-wise linear transformation curve
 * @return      0 on success, error code otherwise
 *
 * This function retrieves the currently configured piece-wise linear transformation
 * curve from the unit.
 *****************************************************************************/
int rpp_hist256_curve(rpp_hist256_drv_t * drv, rpp_hist256_curve_t * const curve);


/**************************************************************************//**
 * @brief Enable or disable piece-wise linear color sample transformation
 * @param[in]   drv     initialized HIST256 driver structure describing the hist256 module instance
 * @param[in]   on      whether the piece-wise linear transformation is to be enabled
 * @return      0 on success, error code otherwise
 *
 * This function enables or disables the piece-wise linear transformation in the
 * unit, which is used for transforming the incoming color samples before histogram
 * evaluation but after prescaling.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_hist256_gamma_enable(rpp_hist256_drv_t * drv, unsigned on);

/**************************************************************************//**
 * @brief Returns whether piece-wise linear color sample transformation is enabled.
 * @param[in]   drv     initialized HIST256 driver structure describing the hist256 module instance
 * @param[out]  on      whether the piece-wise linear transformation is enabled
 * @return      0 on success, error code otherwise
 *
 * This function retrieves from the unit whether the incoming color samples
 * shall be transformed by a piece-wise linear "gamma" curve after prescaling
 * but before histogram evaluation.
 *****************************************************************************/
int rpp_hist256_gamma_enabled(rpp_hist256_drv_t * drv, unsigned * const on);


/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_HIST256_DRV_H__ */

