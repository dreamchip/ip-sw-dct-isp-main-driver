/******************************************************************************
 *
 * Copyright 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_main_pre_drv.h
 *
 * @brief   Interface of RPP pre-fusion pipeline unit driver.
 *
 *****************************************************************************/
#ifndef __RPP_MAIN_PRE_DRV_H__
#define __RPP_MAIN_PRE_DRV_H__

#include "rpp_config.h"
#include <rpp_hdr_types.h>

#include "rpp_acq_drv.h"
#include "rpp_bls_drv.h"
#include "rpp_gamma_in_drv.h"
#include "rpp_lsc4_drv.h"
#include "rpp_awb_gain_drv.h"
#include "rpp_dpcc_drv.h"
#include "rpp_dpf_drv.h"
#include "rpp_hist_drv.h"
#include "rpp_hist256_drv.h"
#include "rpp_exm_drv.h"

#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppMainPreDrv RPP - Pre-Fusion Pipeline Unit Driver
 * @brief RPP - Pre-Fusion Pipeline Unit Driver
 *
 * This section contains definitions and functions for the pre-fusion pipeline
 * stages of the RPP HDR.
 *
 * @note Note that there are two different types of pre-fusion
 * pipelines: the MAIN_PRE_1 pipeline with high-bitwidth (24bit) support and
 * the MAIN_PRE_2 and/or MAIN_PRE_3 pipelines with low-bitwidth (12bit) support.
 *
 * The of the pre-fusion pipeline stages consists of a number of of sub-units.
 * The driver-specific structures for the pre-fusion pipelines, therefore, summarizes
 * the driver-specific data structures for the included sub-units, and only includes
 * a common initialization routine that helps with the probing of the sub-units.
 * The remaining functionality of the sub-units must be controlled using the
 * corresponding functions of the sub-unit drivers themselfs.
 *
 * @warning It may be possible that the MAIN_PRE_3 pipeline is not always available.
 * Whether or not a third pre-fusion pipeline is actually available may not be
 * possible to be detected at runtime. If it is known a-priory that the third
 * pre-fusion pipeline is not available, all functions relating to that pipeline
 * shall not be called.
 * @{
 *****************************************************************************/


/**************************************************************************//**
 * @brief RPP MAIN_PRE specific driver structure
 *
 * This structure encapsulates all data required by the sub-units of the
 * RPP_MAIN_PRE pre-fusion pipeline stage.
 *
 * Note that the MAIN_PRE_1 pipeline does not only differ in the
 * supported bitwidth of color samples, but also includes the HIST256 unit,
 * for which an additional driver-specific data structure needs to be
 * included.
 *****************************************************************************/
typedef struct {
    /** @brief driver-specific portion of the included ACQ sub-unit */
    rpp_acq_drv_t           acq;
    /** @brief driver-specific portion of the included BLS sub-unit */
    rpp_bls_drv_t           bls;
    /** @brief driver-specific portion of the included GAMMA_IN sub-unit */
    rpp_gamma_in_drv_t      gamma_in;
    /** @brief driver-specific portion of the included LSC4 sub-unit */
    rpp_lsc4_drv_t          lsc4;
    /** @brief driver-specific portion of the included AWB_GAIN sub-unit */
    rpp_awb_gain_drv_t      awb_gain;
    /** @brief driver-specific portion of the included DPCC sub-unit */
    rpp_dpcc_drv_t          dpcc;
    /** @brief driver-specific portion of the included DPF sub-unit */
    rpp_dpf_drv_t           dpf;
    /** @brief driver-specific portion of the included HIST sub-unit */
    rpp_hist_drv_t          hist;
    /** @brief driver-specific portion of the included HIST256 sub-unit */
    rpp_hist256_drv_t       hist256;
    /** @brief driver-specific portion of the included EXM sub-unit */
    rpp_exm_drv_t           exm;
} rpp_main_pre_drv_t;


/**************************************************************************//**
 * @brief Initialize the rpp main_pre_1 pipeline stage.
 * @param[in]       dev     rpp_device structure to be used for all register accesses
 * @param[in]       base    base address of RPP MAIN_PRE module
 * @param[in]       index   index of the MAIN_PRE module
 * @param[inout]    drv     MAIN_PRE_1 driver structure to be initialized
 * @return          0 on success, error-code otherwise
 *
 * This function initializes the @c drv structure for future uses of the drivers
 * for all sub-units included in the MAIN_PRE_1 pipeline stage. If any
 * initialization error occurs, the function returns an error code.
 *****************************************************************************/
int rpp_main_pre_init(rpp_device * dev, uint32_t base, uint32_t index, rpp_main_pre_drv_t * drv);


/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_MAIN_PRE_DRV_H__ */

