/******************************************************************************
 *
 * Copyright 2019 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_rmap_drv.h
 *
 * @brief   Interface of radiance mapping unit driver
 *
 *****************************************************************************/
#ifndef __RPP_RMAP_DRV_H__
#define __RPP_RMAP_DRV_H__

#include "rpp_config.h"
#include <rpp_hdr_types.h>

#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppRmapDrv RPP - Radiance Mapping Unit Driver
 * @brief RPP - Radiance Mapping Unit Driver
 *
 * The Radiance Mapping unit performs the fusion of captured images
 * with different exposures to produce one output
 * HDR frame, while preserving all tiny details of the captured
 * images. The current implementation of the HDR_RPP contains a radiance
 * mapping for up to 3 exposures, 1xHIGHBITS (long exposure) and max. 2xLOWBITS
 * (short and, optionally, very short exposure), in a bracketed frame. The dynamic
 * range of the output is HIGHBITS. The HIGHBITS and LOWBITS values can be retrieved
 * from the driver-specific data structure, they are stored in the @c colorbits_high
 * and @c colorbits_low member variables after initialization.
 *
 * This unit can be configured to either perform a HDR fusion of the
 * two or three input exposure samples, or pass-through any of the exposure
 * samples to the output. The corresponding function is rpp_rmap_set_mode(), which
 * is also used to configure the dynamic range (e.g. actual input bitdepth) of
 * the long exposure path which needs to be know for proper radiance mapping.
 * The setting here is the same as for the @c input_type property of the ACQ 
 * unit in the corresponding pre-fusion pipeline.
 *
 * Mapping control requires extensive usage of image statistics from
 * dedicated measurement modules.
 *
 * The radiance mapping data path executes the overlay of the exposures. For this the
 * software must program the 2 mapping factors _FAC_SHORT and _FAC_VERY_SHORT
 * which can be derived with the help of the radiance mapping measurement module.
 *
 * There are regions in the dynamic range of the picture where the output data only
 * originates from a single exposure (long without mapping and short and very short
 * multiplied with their respective mapping factor). In regions which are dark but not yet
 * underexposed in one exposure and bright but not yet overexposed in the neighboring
 * longer exposure blending is executed.
 * The blending factors BLE long and BLE short are calculated automatically on a pixel-by-
 * pixel basis, the thresholds defining the blending regions need to be programmed to
 *  - MIN/MAX_THRES_SHORT for short to very short blending
 *  - MIN/MAX_THRES_LONG for long to short blending
 *
 * The blending factor decreases linearly from 1 to 0 in the range from
 * MAX_THRES to MIN_THRES. The step size for the blending factor
 * shall be calculated in FP12.4 precision as:
 * @f[ STEPSIZE = 2^{colorbits + 4} / (MAX\_THRES - MIN\_THRES) @f]
 * where @c MAX_THRES and @c MIN_THRES shall be chosen sufficiently distinct to limit
 * the stepsize to a maximum of 2048.0.
 *
 * Inside the LONG/SHORT blending region an output sample value is calculated to
 * @f[ s_{out} = s_{long} \cdot BLE_{long} + (s_{short} \cdot (1-BLE_{long})) @f]
 * Inside the SHORT/VERYSHORT blending region an output sample value is calculated to
 * @f[ s_{out} = s_{short} \cdot BLE_{short} + (s_{very_short} \cdot (1-BLE_{short})) @f]
 * In order to compensate effects of over-exposure after white balance the radiance
 * mapping can be programmed with the inverse white balance gains for red and blue for
 * each exposure using WBGAIN_LONG/SHORT/VSHORT_RED/BLUE. This white balance
 * inversion is applied if the respective the value of the current input sample is greater
 * than WBTHRESHOLD_LONG/SHORT/VERYSHORT. The effect is sensor-specific and
 * should be analyzed during calibration. Typically, the thresholds are set to
 * @f$ 2^{bitwidth}-1 - blacklevel @f$.
 * For very short exposure an additional offset might be subtracted.
 *
 * @note It is important that the correct image width is specified
 * before this unit is being enabled. This can be done
 * using the function rpp_rmap_set_hsize().
 *
 * @note Some of the configuration of the unit is subject to register
 * shadowing and will not have an immediate effect. Such configuration,
 * programmed to the unit, will be activated synchronized with an
 * end-of-frame signal when the shadow register update has been
 * requested in the top-level RPP unit register.
 * The currently active configuration cannot be retrieved from
 * the unit.
 * @{
 *
 *****************************************************************************/

/**************************************************************************//**
 * @brief RPP RMAP specific driver structure
 *
 * This structure encapsulates all data required by the RPP RMAP driver to
 * operate a specific instance of the RPP RMAP on a given RPPHDR device.
 * The data fields contained in this structure are intended to be
 * private to the driver and not to be modified by any higher-level
 * software component.
 *
 * It is required to have a separate instance of this structure for each
 * separate RPP RMAP module in a given RPPHDR device. Each of these structures
 * must be initialized before use by calling the function @c rpp_rmap_init()
 * with suitable parameters.
 *
 * After initialization, the @c colorbits_high and @c colorbits_low member
 * variables are set to the number of bits per color value in the
 * long (colorbits_high) or short / very_short (colorbits_low) exposure
 * paths. This information is needed for the configuration of suitable
 * thresholds.
 *
 * The structure is exported here so that instances can statically be declared
 * by higher-level software without the need for dynamic memory allocation.
 *****************************************************************************/
typedef struct {
    uint16_t        version;
    uint8_t         colorbits_high;
    uint8_t         colorbits_low;
    uint32_t        base;
    rpp_device *    dev;
} rpp_rmap_drv_t;


/**************************************************************************//**
 * @brief Enumerations describing the possible modes of operations.
 *
 * These enumerated values described the different possible modes of operation.
 * They select between the bypass of one of the exposure samples (without
 * performing any HDR fusion and needing no further configuration), the fusion
 * of the two LONG/SHORT exposure samples, or the fusion of three exposure 
 * samples (LONG/SHORT/VERY_SHORT).
 */
typedef enum rpp_rmap_mode_e
{
    /** @brief Selects fusion of three exposure samples.
     *
     * This setting selects the fusion of three exposure samples.
     * All thresholds, factors, and inverse whitebalance gains must
     * be configured.
     *
     * @warning This setting must not be used unless all three exposure
     * samples are actually available, e.g. all three pre-fusion pipelines
     * of the RPP_HDR are fully operational.
     */
    RPP_RMAP_MODE_FUSE_3EXP,
    /** @brief Selects fusion of two exposure samples.
     *
     * This setting selects the fusion of the LONG and SHORT exposure
     * paths to an HDR output image. The very short exposure example
     * is ignored. */
    RPP_RMAP_MODE_FUSE_2EXP,
    /** @brief Selects bypass of the long exposure sample.
     *
     * This setting causes the long exposure sample to be output
     * by the unit without any processing, ignoring the other
     * exposure samples. */
    RPP_RMAP_MODE_BYPASS_PRE1,
    /** @brief Selects bypass of the short exposure sample.
     *
     * This setting causes the short exposure sample to be output
     * by the unit without any processing, ignoring the other
     * exposure samples. */
    RPP_RMAP_MODE_BYPASS_PRE2,
    /** @brief Selects bypass of the very short exposure sample.
     *
     * This setting causes the very short exposure sample to be output
     * by the unit without any processing, ignoring the other
     * exposure samples. */
    RPP_RMAP_MODE_BYPASS_PRE3,
    RPP_RMAP_MODE_INVALID
} rpp_rmap_mode_t;


/**************************************************************************//**
 * @brief Enumerations describing the possible long exposure dynamic range options.
 *
 * These enumerated values described the different possible dynamic range setting
 * for the long exposure path, e.g. the actual bit width of the input to the
 * long pre-fusion pipeline. The bit width chosen here shall be the same that
 * was selected as @c input_type setting of the corresponding ACQ unit in
 * the long pre-fusion pipeline.
 */
typedef enum rpp_rmap_long_exp_bits_e
{
    RPP_RMAP_LONG_EXP_12BIT,
    RPP_RMAP_LONG_EXP_14BIT,
    RPP_RMAP_LONG_EXP_16BIT,
    RPP_RMAP_LONG_EXP_18BIT,
    RPP_RMAP_LONG_EXP_20BIT,
    RPP_RMAP_LONG_EXP_24BIT,
    RPP_RMAP_LONG_EXP_INVALID
} rpp_rmap_long_exp_bits_t;



/**************************************************************************//**
 * @brief Initialize the rpp rmap module.
 * @param[in]       dev     rpp_device structure to be used for all register accesses
 * @param[in]       base    base address of RPP RMAP module
 * @param[inout]    drv     RMAP driver structure to be initialized
 * @return          0 on success, error code otherwise
 *****************************************************************************/
int rpp_rmap_init(rpp_device * dev, uint32_t base, rpp_rmap_drv_t * drv);

/**************************************************************************//**
 * @brief Configure radiance mapping operation mode.
 * @param[in]   drv         initialized RMAP driver structure describing the rmap module instance
 * @param[in]   mode        operation mode to configure
 * @param[in]   long_bits   dynamic range of long exposure input
 * @return      0 on success, error code otherwise
 *
 * This function selects an operation mode for the unit and additionally informs
 * the unit about the dynamic range of the long exposure input.
 *
 * The long exposure dynamic range is required to be known by the unit for
 * prescaling the long exposure color samples back to their original value
 * if HDR fusion is to be performed. The setting is ignored in any of the
 * bypass modes.
 *****************************************************************************/
int rpp_rmap_set_mode(rpp_rmap_drv_t * drv, rpp_rmap_mode_t mode,
        rpp_rmap_long_exp_bits_t long_bits);

/**************************************************************************//**
 * @brief Retrieves the currently programmed operation mode from the unit.
 * @param[in]   drv         initialized RMAP driver structure describing the rmap module instance
 * @param[out]  mode        current mode of operation
 * @param[out]  long_bits   current dynamic range of long exposure input
 * @return      0 on success, error code otherwise
 *
 * This function retrieves the current operation mode and long exposure
 * input bit width from the unit.
 *****************************************************************************/
int rpp_rmap_enabled(rpp_rmap_drv_t * drv, rpp_rmap_mode_t * mode,
        rpp_rmap_long_exp_bits_t * long_bits);

/**************************************************************************//**
 * @brief Program inverse white-balance gains for the long exposure sample.
 * @param[in]   drv     initialized RMAP driver structure describing the rmap module instance
 * @param[in]   th      threshold, when gains become active
 * @param[in]   gain_r  inverse red gain
 * @param[in]   gain_b  inverse blue gain
 * @return      0 on success, error code otherwise
 *
 * This function programs the inverse white balance gains and thresholds for
 * the long exposure sample. The inverse white balance correction is applied
 * to prevent an overexposure of the fusion result.
 *
 * In any of the bypass operation modes, these settings do not need to be
 * programmed.
 *
 * @note The threshold is a color value with a maximum range indicated by
 * @c drv->colorbits_high.
 * @note The inverse gain is an FP0.10 fixed point value and allowed to range
 * from 0 to 0.99902..(1023/1024).
 *****************************************************************************/
int rpp_rmap_set_wb_long(rpp_rmap_drv_t * drv,
        const uint32_t th, const uint16_t gain_r, const uint16_t gain_b);

/**************************************************************************//**
 * @brief Retrieve current inverse white-balance gains for the long exposure sample.
 * @param[in]   drv     initialized RMAP driver structure describing the rmap module instance
 * @param[out]  th      current threshold
 * @param[out]  gain_r  current inverse red gain
 * @param[out]  gain_b  current inverse blue gain
 * @return      0 on success, error code otherwise
 *
 * This function retrieves the current inverse white balance configuration
 * for the long exposure sample from the unit.
 *****************************************************************************/
int rpp_rmap_wb_long(rpp_rmap_drv_t * drv,
        uint32_t * const th, uint16_t * const gain_r, uint16_t * const gain_b);

/**************************************************************************//**
 * @brief Program inverse white-balance gains for the short exposure sample.
 * @param[in]   drv     initialized RMAP driver structure describing the rmap module instance
 * @param[in]   th      threshold, when gains become active
 * @param[in]   gain_r  inverse red gain
 * @param[in]   gain_b  inverse blue gain
 * @return      0 on success, error code otherwise
 *
 * This function programs the inverse white balance gains and thresholds for
 * the short exposure sample. The inverse white balance correction is applied
 * to prevent an overexposure of the fusion result.
 *
 * In any of the bypass operation modes, these settings do not need to be
 * programmed.
 *
 * @note The threshold is a color value with a maximum range indicated by
 * @c drv->colorbits_low.
 * @note The inverse gain is an FP0.10 fixed point value and allowed to range
 * from 0 to 0.99902..(1023/1024).
 *****************************************************************************/
int rpp_rmap_set_wb_short(rpp_rmap_drv_t * drv,
        const uint32_t th, const uint16_t gain_r, const uint16_t gain_b);

/**************************************************************************//**
 * @brief Retrieve current inverse white-balance gains for the short exposure sample.
 * @param[in]   drv     initialized RMAP driver structure describing the rmap module instance
 * @param[out]  th      current threshold
 * @param[out]  gain_r  current inverse red gain
 * @param[out]  gain_b  current inverse blue gain
 * @return      0 on success, error code otherwise
 *
 * This function retrieves the current inverse white balance configuration
 * for the short exposure sample from the unit.
 *****************************************************************************/
int rpp_rmap_wb_short(rpp_rmap_drv_t * drv,
        uint32_t * const th, uint16_t * const gain_r, uint16_t * const gain_b);

/**************************************************************************//**
 * @brief Program inverse white-balance gains for the very short exposure sample.
 * @param[in]   drv     initialized RMAP driver structure describing the rmap module instance
 * @param[in]   th      threshold, when gains become active
 * @param[in]   gain_r  inverse red gain
 * @param[in]   gain_b  inverse blue gain
 * @return      0 on success, error code otherwise
 *
 * This function programs the inverse white balance gains and thresholds for
 * the very short exposure sample. The inverse white balance correction is applied
 * to prevent an overexposure of the fusion result.
 *
 * These settings do not need to be programmed in any of the bypass modes
 * or when fusing the long/short exposure samples only.
 *
 * @note The threshold is a color value with a maximum range indicated by
 * @c drv->colorbits_low.
 * @note The inverse gain is an FP0.10 fixed point value and allowed to range
 * from 0 to 0.99902..(1023/1024).
 *****************************************************************************/
int rpp_rmap_set_wb_vshort(rpp_rmap_drv_t * drv,
        const uint32_t th, const uint16_t gain_r, const uint16_t gain_b);

/**************************************************************************//**
 * @brief Retrieve current inverse white-balance gains for the very short exposure sample.
 * @param[in]   drv     initialized RMAP driver structure describing the rmap module instance
 * @param[out]  th      current threshold
 * @param[out]  gain_r  current inverse red gain
 * @param[out]  gain_b  current inverse blue gain
 * @return      0 on success, error code otherwise
 *
 * This function retrieves the current inverse white balance configuration
 * for the very short exposure sample from the unit.
 *****************************************************************************/
int rpp_rmap_wb_vshort(rpp_rmap_drv_t * drv,
        uint32_t * const th, uint16_t * const gain_r, uint16_t * const gain_b);

/**************************************************************************//**
 * @brief Program radiance mapping factors to the unit.
 * @param[in]   drv     initialized RMAP driver structure describing the rmap module instance
 * @param[in]   map_vs  mapping factor for very short exposure samples
 * @param[in]   map_s   mapping factor for short exposure samples
 * @return      0 on success, error code otherwise
 *
 * This function configures the two mapping factors for short and very short exposure
 * images. These mapping factors can be deduced from the rmap_meas module.
 *
 * @note Simplified formula to understand the mapping factors.
 * \f[
 *      i_{out} = map_{s} * ( map_{vs}*i_{vshort} + i_{short} ) + i_{long}
 * \f]
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_rmap_set_map_fac(rpp_rmap_drv_t * drv,
        const uint16_t map_vs, const uint16_t map_s);

/**************************************************************************//**
 * @brief Retrieve current mapping factors.
 * @param[in]   drv     initialized RMAP driver structure describing the rmap module instance
 * @param[out]  map_vs  mapping factor for vshort into short
 * @param[out]  map_s   mapping factor for short' into long
 * @return      0 on success, error code otherwise
 *****************************************************************************/
int rpp_rmap_map_fac(rpp_rmap_drv_t * drv,
        uint16_t * const map_vs, uint16_t * const map_s);

/**************************************************************************//**
 * @brief function to configure parameters for short / very short exposure blending
 * @param[in]   drv         initialized RMAP driver structure describing the rmap module instance
 * @param[in]   th_min      lower threshold defining the blending area
 * @param[in]   th_max      upper threshold defining the blending area
 * @param[in]   stepsize    step size for the blending factor
 * @return      0 on success, error code otherwise
 *
 * This function configures the parameters that control the blending operation
 * between short and very short exposure images.
 * 
 * The thresholds must be color values that define the blending area, with a
 * resolution of @c drv->colorbits_low bits:
 *  - If the short exposure samples are between the min/max thresholds,
 *    blending between short and very short exposures is performed using
 *    a dynamically adjusted blending factor:
 *    @code
 *        f_ble = (th_max - s_short) * stepsize
 *        s_vss = s_short * map_fac_s * f_ble + s_vshort * map_fac_vs * (1 - f_ble)
 *    @endcode
 *  - If the short exposure samples are >th_max, the short exposure is assumed
 *    to be overexposed and the very short data (multiplied with map_fac_vs)
 *    is used without blending.
 *  - If the short exposure samples are <th_min, the very short image is assumed
 *    to be underexposed and the short samples (multiplied with map_fac_s)
 *    are used without blending.
 *
 * The stepsize is a 16-bit integer in the range 0x0000..0x8000 that is
 * interpreted as 12.4 fixed point number with a value range of 0..2048.0.
 * It represents the slope that allows the blending factor to smoothly increment
 * from 0.0 to 1.0 between @c th_min and @c th_max and shall be calculated
 * as
 * @code
 * stepsize = (1 << (drv->colorbits_low + 4)) / (th_max - th_min)
 * @endcode
 *
 * @note This configuration is not required in any of the bypass modes or when
 * fusing only long/short exposure samples.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_rmap_set_blending_short(rpp_rmap_drv_t * drv,
        const uint32_t th_min, const uint32_t th_max, const uint16_t stepsize);

/**************************************************************************//**
 * @brief Reads and returns the parameters for short / very short exposure blending
 * @param[in]   drv         initialized RMAP driver structure describing the rmap module instance
 * @param[out]  th_min      lower threshold defining the blending area
 * @param[out]  th_max      upper threshold for defining the blending area
 * @param[out]  stepsize    step size for the blending factor
 * @return      0 on success, error code otherwise
 *****************************************************************************/
int rpp_rmap_blending_short(rpp_rmap_drv_t * drv,
        uint32_t * const th_min, uint32_t * const th_max, uint16_t * const stepsize);

/**************************************************************************//**
 * @brief function to configure parameters for long / short exposure blending
 * @param[in]   drv         initialized RMAP driver structure describing the rmap module instance
 * @param[in]   th_min      lower threshold defining the blending area
 * @param[in]   th_max      upper threshold defining the blending area
 * @param[in]   stepsize    step size for the blending factor
 * @return      0 on success, error code otherwise
 *
 * This function configures the parameters that control the blending operation
 * between long and short exposure images.
 * 
 * The thresholds must be color values that define the blending area, with a
 * resolution of @c drv->colorbits_high bits:
 *  - If the long exposure samples are between the min/max thresholds,
 *    blending between long exposure and short/vshort fusion result is
 *    performed using a dynamically adjusted blending factor:
 *    @code
 *        f_ble = (th_max - s_long) * stepsize
 *        s_out = s_long * f_ble + s_vss * (1 - f_ble)
 *    @endcode
 *  - If the long exposure samples are >th_max, the long exposure is assumed
 *    to be overexposed and the short/very short fusion result
 *    is used without blending.
 *  - If the long exposure samples are <th_min, the short image is assumed
 *    to be underexposed and the long samples are used without blending.
 *
 * The stepsize is a 16-bit integer in the range 0x0000..0x8000 that is
 * interpreted as 12.4 fixed point number with a value range of 0..2048.0.
 * It represents the slope that allows the blending factor to smoothly increment
 * from 0.0 to 1.0 between @c th_min and @c th_max and shall be calculated
 * as
 * @code
 * stepsize = (1 << (drv->colorbits_high + 4)) / (th_max - th_min)
 * @endcode
 *
 * @note This configuration is not required in any of the bypass modes.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_rmap_set_blending_long(rpp_rmap_drv_t * drv,
        const uint32_t th_min, const uint32_t th_max, const uint16_t stepsize);

/**************************************************************************//**
 * @brief Reads and returns the parameters for long / short exposure blending
 * @param[in]   drv         initialized RMAP driver structure describing the rmap module instance
 * @param[out]  th_min      lower threshold defining the blending area
 * @param[out]  th_max      upper threshold defining the blending area
 * @param[out]  stepsize    step size for the blending factor
 * @return      0 on success, error code otherwise
 *****************************************************************************/
int rpp_rmap_blending_long(rpp_rmap_drv_t * drv,
        uint32_t * const th_min, uint32_t * const th_max, uint16_t * const stepsize);

/**************************************************************************//**
 * @brief Program the horizontal image size to the rmap unit.
 * @param[in]   drv     initialized RMAP driver structure describing the rmap module instance
 * @param[in]   hsize   horizontal image size
 * @return      0 on success, error code otherwise
 *
 * This function configures the actual width of the image to be processed. The
 * @c hsize parameter must be an integer value between 0 and 16383.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_rmap_set_hsize(rpp_rmap_drv_t * drv, const uint16_t hsize);

/**************************************************************************//**
 * @brief Retrieve the horizontal image size from the rmap unit.
 * @param[in]   drv     initialized RMAP driver structure describing the rmap module instance
 * @param[out]  hsize   current horizontal image size
 * @return      0 on success, error code otherwise
 *****************************************************************************/
int rpp_rmap_hsize(rpp_rmap_drv_t * drv, uint16_t * const hsize);

/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_RMAP_DRV_H__ */

