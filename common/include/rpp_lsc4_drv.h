/******************************************************************************
 *
 * Copyright 2019 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_lsc4_drv.h
 *
 * @brief   Interface of lens shade correction driver
 *
 *****************************************************************************/
#ifndef __RPP_LSC4_DRV_H__
#define __RPP_LSC4_DRV_H__

#include "rpp_config.h"

#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************************//**
 * @addtogroup RppHdrDrv RPP HDR - Low Level Driver
 * @brief RPP HDR - Low Level Driver
 * @{
 *
 * @defgroup RppLscDrv RPP - Lens Shade Correction Driver
 * RPP - Lens Shade Correction Driver
 * @{
 *
 * The aim of the lens shading correction algorithm is to achieve a constant
 * sensitivity across the entire frame after correction. Therefore each incoming
 * pixel value PIN(x,y) is multiplied by a correction factor F(x,y). The 
 * correction factor depends on the coordinates of the pixel within the frame.
 *
 * The corrected pixel value PCOR(x,y) is calculated according:
 * 
 * \f$PCOR(x,y) = PIN(x,y) * F(x,y)\f$
 *
 * In order to increase the precision of the correction function, the frame
 * is divided into 16 sectors in x and y dimension. The coordinates of each
 * sector are programmable. Furthermore the lens shading correction parameters
 * are programmable independently for each sector and for each color component
 * within a sector. The sector coordinates apply for each color component. 
 * This unit will be designed so that either the lens shading correction parameters
 * apply for all color components or each color component gets its own lens shading
 * correction parameters.
 *
 * @image html lsc_sectors.png "Lens Shading Correction Sectors" width=\textwidth
 * @image latex lsc_sectors.png "Lens Shading Correction Sectors" width=\textwidth
 *
 * To reduce the memory for storing the coordinates of the different sectors, only
 * the horizontal and vertical sizes of the sectors are programmed.
 * 
 * Within each sector, the correction function F(x,y) can be expressed as a Bilinear
 * Interpolation Function. These functions in different areas are correlated, because
 * of the requirement that the correction function must be continuous and smooth.
 *
 *****************************************************************************/

/**************************************************************************//**
 * @def RPP_LSC4_NUM_TABLES
 * @brief The lsc4 RAM can take up to 2 sets of correction values (tables). One
 * segment is for configuration and the other one for pipeline processing. The
 * segment to use is selectable by a RAM adress and a table id.
 *****************************************************************************/
#define RPP_LSC4_NUM_TABLES         ( 2u)

/**************************************************************************//**
 * @def RPP_LSC4_GRID_SIZE
 * @brief Defines the number of frame intersections.
 *
 * This macro defines the number of parts into which a frame is divided
 * for the purpose of Lens Shade Correction. For each part, the size of
 * the part and a gradient need to be configured.
 *
 * The number of frame intersections in horizontal and vertical direction is
 * the same and, therefore, only a single constant needs to be defined.
 *****************************************************************************/
#define RPP_LSC4_GRID_SIZE          ( 16u)

/**************************************************************************//**
 * @def RPP_LSC4_TABLE_WIDTH
 * @brief Horizontal size of correction table.
 *
 * The correction table specifies the values of the correction factors at each
 * of the grid points. Since in horizontal direction, the factor at the start
 * of the first grid cell also needs to be programmed, this value is one more
 * that the number of intersections.
 *****************************************************************************/
#define RPP_LSC4_TABLE_WIDTH        (RPP_LSC4_GRID_SIZE + 1)

/**************************************************************************//**
 * @def RPP_LSC4_TABLE_HEIGHT
 * @brief Vertical size of correction table
 *
 * The correction table specifies the values of the correction factors at each
 * of the grid points. Since in vertical direction, the factor at the start
 * of the first grid cell also needs to be programmed, this value is one more
 * that the number of intersections.
 *****************************************************************************/
#define RPP_LSC4_TABLE_HEIGHT       (RPP_LSC4_GRID_SIZE + 1)

/**************************************************************************//**
 * @def RPP_LSC4_TABLE_SIZE
 * @brief Size of correction table (number of correction values)
 *****************************************************************************/
#define RPP_LSC4_TABLE_SIZE         (RPP_LSC4_TABLE_WIDTH * RPP_LSC4_TABLE_HEIGHT)

/**************************************************************************//**
 * @brief Lens shade grid configuration. 
 *****************************************************************************/
typedef struct 
{
    uint16_t x_grad[RPP_LSC4_GRID_SIZE];   /**< factor in x direction */
    uint16_t y_grad[RPP_LSC4_GRID_SIZE];   /**< factor in y direction */
    uint16_t x_size[RPP_LSC4_GRID_SIZE];   /**< x segment size */
    uint16_t y_size[RPP_LSC4_GRID_SIZE];   /**< y segment size */
} rpp_lsc4_grid_t;

/**************************************************************************//**
 * @brief Lens shade correction values. One matrix per bayer component.
 *****************************************************************************/
typedef struct 
{
    uint16_t r[RPP_LSC4_TABLE_SIZE];    /**< correction values red */
    uint16_t gr[RPP_LSC4_TABLE_SIZE];   /**< correction values green/red */
    uint16_t b[RPP_LSC4_TABLE_SIZE];    /**< correction values blue */
    uint16_t gb[RPP_LSC4_TABLE_SIZE];   /**< correction values green/blue */
} rpp_lsc4_values_t;


/**************************************************************************//**
 * @brief RPP LSC4 specific driver structure
 *
 * This structure encapsulates all data required by the RPP LSC4 driver to
 * operate a specific instance of the RPP LSC4 on a given RPPHDR device.
 * The data fields contained in this structure are intended to be
 * private to the driver and not to be modified by any higher-level
 * software component.
 *
 * It is required to have a separate instance of this structure for each
 * separate RPP LSC4 module in a given RPPHDR device. Each of these structures
 * must be initialized before use by calling the function @c rpp_lsc4_init()
 * with suitable parameters.
 *
 * The structure is exported here so that instances can statically be declared
 * by higher-level software without the need for dynamic memory allocation.
 *****************************************************************************/
typedef struct {
    uint32_t        version;
    uint32_t        base;
    rpp_device *    dev;
} rpp_lsc4_drv_t;



/**************************************************************************//**
 * @brief Initialize the rpp lsc4 driver.
 * @param[in]       dev     rpp_device structure to be used for all register accesses
 * @param[in]       base    base address of RPP LSC4 moduleA
 * @param[inout]    drv     LSC4 driver structure to be initialized
 * @return          0 on success, error code otherwise
 *****************************************************************************/
int rpp_lsc4_init(rpp_device * dev, uint32_t base, rpp_lsc4_drv_t * drv);

/**************************************************************************//**
 * @brief Enable or disable the rpp lsc4 module.
 * @param[in]   drv     initialized LSC4 driver structure describing the lsc4 module instance
 * @param[in]   on      enable state to set (0: disable, otherwise enable)
 * @return      0 on success, error code otherwise
 *
 * This function is used to enable or disable the LSC4 unit.
 *
 * @note This configuration is subject to register
 * shadowing and will not have an immediate effect. New configuration
 * programmed to the unit will be activated, synchronized with an
 * end-of-frame signal, when the shadow register update has been
 * requested in the top-level RPP unit register.
 *****************************************************************************/
int rpp_lsc4_enable(rpp_lsc4_drv_t * drv, const unsigned on);

/**************************************************************************//**
 * @brief Return enable status of the rpp lsc4 module.
 * @param[in]   drv     initialized LSC4 driver structure describing the lsc4 module instance
 * @param[out]  on      current enable state (0: disabled, otherwise enabled)
 * @return      0 on success, error code otherwise
 *
 * This function returns whether the unit has been programmed to be enabled
 * or disabled. It does not tell whether the unit is currently active.
 * Use the rpp_lsc4_active() function for that.
 *****************************************************************************/
int rpp_lsc4_enabled(rpp_lsc4_drv_t * drv, unsigned * const on);

/**************************************************************************//**
 * @brief Return activity status of the rpp lsc4 module.
 * @param[in]   drv     initialized LSC4 driver structure describing the lsc4 module instance
 * @param[out]  on      current activity state (0: inactive, otherwise active)
 * @return      0 on success, error code otherwise
 *
 * This function returns whether the unit is active at the time of asking.
 * 
 * @note Changing the unit's enable state does not take effect before completion
 * of the current frame, so this function may return a different result than
 * rpp_lsc4_enabled().
 *****************************************************************************/
int rpp_lsc4_active(rpp_lsc4_drv_t * drv, unsigned * const on);


/**************************************************************************//**
 * @brief Programs which lens shade correction table is enabled.
 * @param[in]   drv     initialized LSC4 driver structure describing the lsc4 module instance
 * @param[in]   tid     table identifier (0: table 0, 1: table 1)
 * @return      0 on success, error code otherwise
 *
 * The LSC4 contains two internal tables for the correction factors at the
 * grid points: a currently active one and an inactive one. The active one
 * is used by the unit for correction and shall not be modified while the
 * unit is active. The inactive one can freely be programmed at any time.
 * 
 * This function programs the number of the table to the unit which shall
 * be active for the next frame.
 *
 *****************************************************************************/
int rpp_lsc4_set_table_id(rpp_lsc4_drv_t * drv, const unsigned tid);

/**************************************************************************//**
 * @brief Retrieves which lens shade correction table is enabled for the next frame.
 * @param[in]   drv     initialized LSC4 driver structure describing the lsc4 module instance
 * @param[in]   tid     table identifier (0: table 0, 1: table 1)
 * @return      0 on success, error code otherwise
 *
 * The LSC4 contains two internal tables for the correction factors at the
 * grid points: a currently active one and an inactive one. The active one
 * is used by the unit for correction and shall not be modified while the
 * unit is active. The inactive one can freely be programmed at any time.
 * 
 * This function retrieves the number of the table from the unit which shall
 * be active for the next frame. It does not well which of the tables is
 * actively being used by the unit, use the rpp_lsc4_active_table_id()
 * function for that.
 *
 *****************************************************************************/
int rpp_lsc4_next_table_id(rpp_lsc4_drv_t * drv, unsigned * const tid);

/**************************************************************************//**
 * @brief Retrieves the currently active lens shade correction table.
 * @param[in]   drv     initialized LSC4 driver structure describing the lsc4 module instance
 * @param[in]   tid     table identifier (0: table 0, 1: table 1)
 * @return      0 on success, error code otherwise
 *
 * The LSC4 contains two internal tables for the correction factors at the
 * grid points: a currently active one and an inactive one. The active one
 * is used by the unit for correction and shall not be modified while the
 * unit is active. The inactive one can freely be programmed at any time.
 * 
 * This function retrieves the number of the table from the unit which is
 * active at the time of asking.
 *
 * @note Changing the unit's active table ID does not take effect before
 * completion of the current frame, so this function may return a different
 * result than rpp_lsc4_get_table_id().
 *****************************************************************************/
int rpp_lsc4_active_table_id(rpp_lsc4_drv_t * drv, unsigned * const tid);

/**************************************************************************//**
 * @brief Program lens shade correction table.
 * @param[in]   drv     initialized LSC4 driver structure describing the lsc4 module instance
 * @param[in]   tid     table identifier (0: table 0, 1: table 1)
 * @param[in]   values  correction values to set (17*17 values per bayer component)
 * @return      0 on success, error code otherwise
 *
 * The LSC4 contains two internal tables for the correction factors at the
 * grid points: a currently active one and an inactive one. The active one
 * is used by the unit for correction and shall not be modified while the
 * unit is active. The inactive one can freely be programmed at any time.
 * A function to swap active and inactive tables is available.
 *
 * This function configures the correction factors for each of the bayer
 * components. The @c tid argument indicates into which of the two tables
 * the values are to be programmed. Higher-level software shall make sure
 * that the @c tid argument always points to the currently inactive table.
 * Re-programming the active table would cause artifacts in the output image
 * due to inconsistent data.
 *
 *****************************************************************************/
int rpp_lsc4_set_table
(
    rpp_lsc4_drv_t *    drv,
    const unsigned      tid,
    const rpp_lsc4_values_t * const   values
);

/**************************************************************************//**
 * @brief Retrieve lens shade correction table.
 * @param[in]   drv     initialized LSC4 driver structure describing the lsc4 module instance
 * @param[in]   tid     table identifier (0: table 0, 1: table 1)
 * @param[in]   values  holds correction factors at return from this function
 * @return      0 on success, error code otherwise
 *
 * The LSC4 contains two internal tables for the correction factors at the
 * grid points: a currently active one and an inactive one. The active one
 * is used by the unit for correction and shall not be modified while the
 * unit is active. The inactive one can freely be programmed at any time.
 * A function to swap active and inactive tables is available.
 *
 * This function retrieves the correction factors for each of the bayer
 * components from the unit and stores them in the supplied argument.
 * The @c tid argument indicates from which of the two tables
 * the values are to be retrieved. Note that it is always possible to
 * retrieve both tables without interfering with the correction process.
 *
 *****************************************************************************/
int rpp_lsc4_table
(
    rpp_lsc4_drv_t *    drv,
    const unsigned      tid,
    rpp_lsc4_values_t * const   values
);

/**************************************************************************//**
 * @brief Program lens shade grid coordinates and gradients.
 * @param[in]   drv     initialized LSC4 driver structure describing the lsc4 module instance
 * @param[in]   values  grid configuration values
 * @return      0 on success, error code otherwise
 *
 * This function is used to program the grid coordinates and the gradients
 * required for the bilinear interpolation function.
 *
 * Other than for the correction factors, there is no active and inactive set
 * of the grid definition and the grid shall not be programmed while the unit
 * is active.
 *****************************************************************************/
int rpp_lsc4_set_grid
(
    rpp_lsc4_drv_t *    drv,
    const rpp_lsc4_grid_t * const values
);

/**************************************************************************//**
 * @brief Retrieve the current lens shade grid coordinates and gradients.
 * @param[in]   drv     initialized LSC4 driver structure describing the lsc4 module instance
 * @param[out]  values  grid configuration values 
 * @return      0 on success, error code otherwise
 *
 * This function is used to program the grid coordinates and the gradients
 * required for the bilinear interpolation function.
 *****************************************************************************/
int rpp_lsc4_grid
(
    rpp_lsc4_drv_t *    drv,
    rpp_lsc4_grid_t * const values
);

/**************************************************************************//**
 * @} @}
 *****************************************************************************/
#ifdef __cplusplus
}
#endif

#endif /* __RPP_LSC4_DRV_H__ */

