/******************************************************************************
 *
 * Copyright 2019 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_rmap_drv.c
 *
 * @brief   Implementation of radiance mapping unit driver
 *
 *****************************************************************************/
#include <rpp_rmap_drv.h>

// make sure that the base is 0 in the auto-generated
// header-file, before including it
#define RPP_RMAP24_BASE 0
#include <rpp_rmap24_regs_addr_map.h>
#include <rpp_rmap24_regs_mask.h>

/******************************************************************************
 * local definitions
 *****************************************************************************/

static inline int check_valid(const rpp_rmap_drv_t * drv)
{
    return (drv && drv->dev && drv->colorbits_high && drv->colorbits_low);
}


/******************************************************************************
 * rpp_rmap_init
 *****************************************************************************/
int rpp_rmap_init(rpp_device * dev, uint32_t base, rpp_rmap_drv_t * drv)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !dev || !drv )
    {
        return ( -EINVAL );
    }

    drv->dev = dev;
    drv->base = base;
    drv->colorbits_high = 0;
    drv->colorbits_low = 0;
    drv->version = 0;

    /* read and parse the version register */
    ret = READL(RPP_RPP_RMAP_DATA_VERSION_REG, &v);
    if (!ret) {
        drv->version = REG_GET_SLICE(v, RPP_RMAP_DATA_RMAP_DATA_VERSION);
        switch(drv->version) {
            case 0x08:
                /* 20bit long data path */
                drv->colorbits_high = 20;
                drv->colorbits_low = 12;
                break;
            case 0x09:
                /* 24bit long data path */
                drv->colorbits_high = 24;
                drv->colorbits_low = 12;
                break;
            default:
                ret = -ENODEV;
        }
    }

    return ret;
}

static const union mode_conf_u {
    uint8_t     val;
    struct {
        uint8_t     enable:1;
        uint8_t     bypass_vs:1;
        uint8_t     bypass_l:1;
        uint8_t     twopipes:1;
    };
} mode_conf[] = {
    /* note: uninitialized fields below are automatically
     * zeroed by the compiler. */
    {   /* RPP_RMAP_MODE_FUSE_3EXP */
        .enable     = 1,
    },
    {   /* RPP_RMAP_MODE_FUSE_2EXP */
        .enable     = 1,
        .twopipes   = 1,
    },
    {   /* RPP_RMAP_MODE_BYPASS_PRE1 */
        .bypass_l   = 1,
    },
    {   /* RPP_RMAP_MODE_BYPASS_PRE2 */
        .enable     = 0,
    },
    {   /* RPP_RMAP_MODE_BYPASS_PRE3 */
        .bypass_vs  = 1,
    }
};

/******************************************************************************
 * rpp_rmap_enable
 *****************************************************************************/
int rpp_rmap_set_mode(rpp_rmap_drv_t * drv, rpp_rmap_mode_t mode,
        rpp_rmap_long_exp_bits_t long_bits)
{
    uint32_t v;
    int ret;
    static const uint8_t bits[] = {12, 14, 16, 18, 20, 24};

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    /* range check */
    if (
            (long_bits >= RPP_RMAP_LONG_EXP_INVALID) ||
            (bits[long_bits] > drv->colorbits_high)  ||
            (mode >= RPP_RMAP_MODE_INVALID)
       )
    {
        return -ERANGE;
    }

    /* now, read and update the CTRL register. */
    ret = READL(RPP_RPP_RMAP_CTRL_REG, &v);
    if (!ret) {
        const union mode_conf_u * m = &mode_conf[mode];
        REG_SET_SLICE(v, RPP_RMAP_DATA_LONG_DYNAMIC_RANGE,  long_bits);
        REG_SET_SLICE(v, RPP_RMAP_DATA_USE_LONG_AND_SHORT,  m->twopipes);
        REG_SET_SLICE(v, RPP_RMAP_DATA_BYPASS_LONG,         m->bypass_l);
        REG_SET_SLICE(v, RPP_RMAP_DATA_BYPASS_VERY_SHORT,   m->bypass_vs);
        REG_SET_SLICE(v, RPP_RMAP_DATA_RMAP_ENABLE,         m->enable);
        ret = WRITEL(RPP_RPP_RMAP_CTRL_REG, v);
    }

    return ret;
}

/******************************************************************************
 * rpp_rmap_enabled
 *****************************************************************************/
int rpp_rmap_enabled(rpp_rmap_drv_t * drv, rpp_rmap_mode_t * mode,
        rpp_rmap_long_exp_bits_t * long_bits)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !mode || !long_bits)
    {
        return ( -EINVAL );
    }

    ret = READL(RPP_RPP_RMAP_CTRL_REG, &v);
    if (!ret) {
        rpp_rmap_mode_t i;
        union mode_conf_u tmp = {0};
        tmp.enable      = REG_GET_SLICE(v, RPP_RMAP_DATA_RMAP_ENABLE);
        tmp.bypass_vs   = REG_GET_SLICE(v, RPP_RMAP_DATA_BYPASS_VERY_SHORT);
        tmp.bypass_l    = REG_GET_SLICE(v, RPP_RMAP_DATA_BYPASS_LONG);
        tmp.twopipes    = REG_GET_SLICE(v, RPP_RMAP_DATA_USE_LONG_AND_SHORT);
        *long_bits      = REG_GET_SLICE(v, RPP_RMAP_DATA_LONG_DYNAMIC_RANGE);
        *mode           = 0;
        for (i = 0; i < RPP_RMAP_MODE_INVALID; ++i)
        {
            if (mode_conf[i].val == tmp.val)
            {
                *mode = i;
                break;
            }
        }
    }

    return ret;
}

/******************************************************************************
 * rpp_rmap_set_wb_long
 *****************************************************************************/
int rpp_rmap_set_wb_long(rpp_rmap_drv_t * drv,
        const uint32_t th, const uint16_t gain_r, const uint16_t gain_b)
{
    uint32_t th_max;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    th_max = UNSIGNED_MAX(drv->colorbits_high);
    /* range check */
    if (
            (th > th_max)                                      ||
            REG_RANGE_CHECK(gain_r, RPP_RMAP_DATA_WBGAIN_LR)   ||
            REG_RANGE_CHECK(gain_b, RPP_RMAP_DATA_WBGAIN_LB)
       )
    {
        return ( -ERANGE );
    }

    ret = WRITEL(RPP_RPP_RMAP_WBTHRESHOLD_LONG_REG, th);
    if (!ret)
        ret = WRITEL(RPP_RPP_RMAP_WBGAIN_LONG_RED_REG, gain_r);
    if (!ret)
        ret = WRITEL(RPP_RPP_RMAP_WBGAIN_LONG_BLUE_REG, gain_b);

    return ret;
}

/******************************************************************************
 * rpp_rmap_wb_long
 *****************************************************************************/
int rpp_rmap_wb_long(rpp_rmap_drv_t * drv,
        uint32_t * const th, uint16_t * const gain_r, uint16_t * const gain_b)
{
    uint32_t tmp;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !th || !gain_r || !gain_b )
    {
        return ( -EINVAL );
    }

    ret = READL(RPP_RPP_RMAP_WBTHRESHOLD_LONG_REG, &tmp);
    if (!ret) {
        *th = tmp & UNSIGNED_MAX(drv->colorbits_high);
        ret = READL(RPP_RPP_RMAP_WBGAIN_LONG_RED_REG, &tmp);
    }
    if (!ret) {
        *gain_r = REG_GET_SLICE(tmp, RPP_RMAP_DATA_WBGAIN_LR);
        ret = READL(RPP_RPP_RMAP_WBGAIN_LONG_BLUE_REG, &tmp);
    }
    if (!ret) {
        *gain_b = REG_GET_SLICE(tmp, RPP_RMAP_DATA_WBGAIN_LB);
    }

    return ret;
}

/******************************************************************************
 * rpp_rmap_set_wb_short
 *****************************************************************************/
int rpp_rmap_set_wb_short(rpp_rmap_drv_t * drv,
        const uint32_t th, const uint16_t gain_r, const uint16_t gain_b)
{
    uint32_t th_max;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    th_max = UNSIGNED_MAX(drv->colorbits_low);
    /* range check */
    if (
            (th > th_max)                                       ||
            REG_RANGE_CHECK(gain_r, RPP_RMAP_DATA_WBGAIN_SR)    ||
            REG_RANGE_CHECK(gain_b, RPP_RMAP_DATA_WBGAIN_SB)
       )
    {
        return ( -ERANGE );
    }

    ret = WRITEL(RPP_RPP_RMAP_WBTHRESHOLD_SHORT_REG, th);
    if (!ret)
        ret = WRITEL(RPP_RPP_RMAP_WBGAIN_SHORT_RED_REG, gain_r);
    if (!ret)
        ret = WRITEL(RPP_RPP_RMAP_WBGAIN_SHORT_BLUE_REG, gain_b);

    return ret;
}

/******************************************************************************
 * rpp_rmap_wb_short
 *****************************************************************************/
int rpp_rmap_wb_short(rpp_rmap_drv_t * drv,
        uint32_t * const th, uint16_t * const gain_r, uint16_t * const gain_b)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !th || !gain_r || !gain_b )
    {
        return ( -EINVAL );
    }

    ret = READL(RPP_RPP_RMAP_WBTHRESHOLD_SHORT_REG, &v);
    if (!ret) {
        *th = v & UNSIGNED_MAX(drv->colorbits_low);
        ret = READL(RPP_RPP_RMAP_WBGAIN_SHORT_RED_REG, &v);
    }
    if (!ret) {
        *gain_r = REG_GET_SLICE(v, RPP_RMAP_DATA_WBGAIN_SR);
        ret = READL(RPP_RPP_RMAP_WBGAIN_SHORT_BLUE_REG, &v);
    }
    if (!ret) {
        *gain_b = REG_GET_SLICE(v, RPP_RMAP_DATA_WBGAIN_SB);
    }

    return ret;
}

/******************************************************************************
 * rpp_rmap_set_wb_vshort
 *****************************************************************************/
int rpp_rmap_set_wb_vshort(rpp_rmap_drv_t * drv,
        const uint32_t th, const uint16_t gain_r, const uint16_t gain_b)
{
    uint32_t th_max;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    th_max = UNSIGNED_MAX(drv->colorbits_low);

    /* range check */
    if (
            (th > th_max)                                       ||
            REG_RANGE_CHECK(gain_r, RPP_RMAP_DATA_WBGAIN_VSR)   ||
            REG_RANGE_CHECK(gain_b, RPP_RMAP_DATA_WBGAIN_VSB)
       )
    {
        return ( -ERANGE );
    }

    ret = WRITEL(RPP_RPP_RMAP_WBTHRESHOLD_VERYSHORT_REG, th);
    if (!ret)
        ret = WRITEL(RPP_RPP_RMAP_WBGAIN_VSHORT_RED_REG, gain_r);
    if (!ret)
        ret = WRITEL(RPP_RPP_RMAP_WBGAIN_VSHORT_BLUE_REG, gain_b);

    return ret;
}

/******************************************************************************
 * rpp_rmap_wb_vshort
 *****************************************************************************/
int rpp_rmap_wb_vshort(rpp_rmap_drv_t * drv,
        uint32_t * const th, uint16_t * const gain_r, uint16_t * const gain_b)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !th || !gain_r || !gain_b )
    {
        return ( -EINVAL );
    }

    ret = READL(RPP_RPP_RMAP_WBTHRESHOLD_VERYSHORT_REG, &v);
    if (!ret) {
        *th = v & UNSIGNED_MAX(drv->colorbits_low);
        ret = READL(RPP_RPP_RMAP_WBGAIN_VSHORT_RED_REG, &v);
    }
    if (!ret) {
        *gain_r = REG_GET_SLICE(v, RPP_RMAP_DATA_WBGAIN_VSR);
        ret = READL(RPP_RPP_RMAP_WBGAIN_VSHORT_BLUE_REG, &v);
    }
    if (!ret) {
        *gain_b = REG_GET_SLICE(v, RPP_RMAP_DATA_WBGAIN_VSB);
    }

    return ret;
}

/******************************************************************************
 * rpp_rmap_set_map_fac
 *****************************************************************************/
int rpp_rmap_set_map_fac(rpp_rmap_drv_t * drv,
        const uint16_t map_vs, const uint16_t map_s)
{
    uint32_t regs[2];

    /* simple base address check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    /* range check */
    if (
            REG_RANGE_CHECK(map_vs, RPP_RMAP_DATA_MAP_FAC_VS) ||
            REG_RANGE_CHECK(map_s , RPP_RMAP_DATA_MAP_FAC_S)
       )
    {
        return ( -ERANGE );
    }

    regs[0] = map_s;
    regs[1] = map_vs;
    return WRITE_REGS(RPP_RPP_RMAP_MAP_FAC_SHORT_REG, regs, 2);
}

/******************************************************************************
 * rpp_rmap_map_fac
 *****************************************************************************/
int rpp_rmap_map_fac(rpp_rmap_drv_t * drv,
        uint16_t * const map_vs, uint16_t * const map_s)
{
    uint32_t regs[2];
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !map_vs || !map_s )
    {
        return ( -EINVAL );
    }

    ret = READ_REGS(RPP_RPP_RMAP_MAP_FAC_SHORT_REG, regs, 2);
    if (!ret) {
        *map_s  = REG_GET_SLICE(regs[0], RPP_RMAP_DATA_MAP_FAC_S);
        *map_vs = REG_GET_SLICE(regs[1], RPP_RMAP_DATA_MAP_FAC_VS);
    }

    return ret;
}

/******************************************************************************
 * rpp_rmap_set_blending_short
 *****************************************************************************/
int rpp_rmap_set_blending_short(rpp_rmap_drv_t * drv,
        const uint32_t th_min, const uint32_t th_max, const uint16_t stepsize)
{
    uint32_t regs[3];
    uint32_t th_limit;
    uint16_t step_limit;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    th_limit = UNSIGNED_MAX(drv->colorbits_low);
    step_limit = 0x8000;

    /* range check */
    if (
            (th_min > th_limit) || (th_min >= th_max)   ||
            (th_max > th_limit)                         ||
            (stepsize > step_limit)
       )
    {
        return ( -ERANGE );
    }

    regs[0] = th_min;
    regs[1] = th_max;
    regs[2] = stepsize;

    return WRITE_REGS(RPP_RPP_RMAP_MIN_THRES_SHORT_REG, regs, 3);
}

/******************************************************************************
 * rpp_rmap_blending_short
 *****************************************************************************/
int rpp_rmap_blending_short(rpp_rmap_drv_t * drv,
        uint32_t * const th_min, uint32_t * const th_max, uint16_t * const stepsize)
{
    uint32_t regs[3];
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !th_min || !th_max || !stepsize )
    {
        return ( -EINVAL );
    }

    ret = READ_REGS(RPP_RPP_RMAP_MIN_THRES_SHORT_REG, regs, 3);
    if (!ret) {
        *th_min = regs[0] & UNSIGNED_MAX(drv->colorbits_low);
        *th_max = regs[1] & UNSIGNED_MAX(drv->colorbits_low);
        *stepsize = REG_GET_SLICE(regs[2], RPP_RMAP_DATA_STEPSIZE_S);
    }

    return ret;
}

/******************************************************************************
 * rpp_rmap_set_blending_long
 *****************************************************************************/
int rpp_rmap_set_blending_long(rpp_rmap_drv_t * drv,
        const uint32_t th_min, const uint32_t th_max, const uint16_t stepsize)
{
    uint32_t regs[3];
    uint32_t th_limit;
    uint16_t step_limit;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    th_limit = UNSIGNED_MAX(drv->colorbits_high);
    step_limit = 0x8000;

    /* range check */
    if (
            (th_min > th_limit) || (th_min >= th_max)   ||
            (th_max > th_limit)                         ||
            (stepsize > step_limit)
       )
    {
        return ( -ERANGE );
    }

    regs[0] = th_min;
    regs[1] = th_max;
    regs[2] = stepsize;

    return WRITE_REGS(RPP_RPP_RMAP_MIN_THRES_LONG_REG, regs, 3);
}

/******************************************************************************
 * rpp_rmap_blending_long
 *****************************************************************************/
int rpp_rmap_blending_long(rpp_rmap_drv_t * drv,
        uint32_t * const th_min, uint32_t * const th_max, uint16_t * const stepsize)
{
    uint32_t regs[3];
    uint32_t th_limit;
    int ret;
    /* simple sanity checks */
    if ( !check_valid(drv) || !th_min || !th_max || !stepsize )
    {
        return ( -EINVAL );
    }

    th_limit = UNSIGNED_MAX(drv->colorbits_high);
    ret = READ_REGS(RPP_RPP_RMAP_MIN_THRES_LONG_REG, regs, 3);

    if (!ret) {
        *th_min = regs[0] & th_limit;
        *th_max = regs[1] & th_limit;
        *stepsize = REG_GET_SLICE(regs[2], RPP_RMAP_DATA_STEPSIZE_L);
    }

    return ret;
}

/******************************************************************************
 * rpp_rmap_set_hsize
 *****************************************************************************/
int rpp_rmap_set_hsize(rpp_rmap_drv_t * drv, const uint16_t hsize)
{
    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    if ( REG_RANGE_CHECK(hsize, RPP_RMAP_DATA_CFG_CLB_LINESIZE) )
    {
        return ( -ERANGE );
    }

    return WRITEL(RPP_RPP_RMAP_CLB_LINESIZE_REG, hsize);
}

/******************************************************************************
 * rpp_rmap_hsize
 *****************************************************************************/
int rpp_rmap_hsize(rpp_rmap_drv_t * drv, uint16_t * const hsize)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !hsize )
    {
        return ( -EINVAL );
    }

    ret = READL(RPP_RPP_RMAP_CLB_LINESIZE_REG, &v);
    if (!ret) {
        *hsize = REG_GET_SLICE(v, RPP_RMAP_DATA_CFG_CLB_LINESIZE);
    }

    return ret;
}


