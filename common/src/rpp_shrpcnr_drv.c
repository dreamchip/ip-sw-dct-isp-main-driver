/******************************************************************************
 *
 * Copyright 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_shrpcnr_drv.c
 *
 * @brief   Implementation of SHRPCNR unit driver
 *
 *****************************************************************************/
#include <rpp_shrpcnr_drv.h>

// make sure that the base is 0 in the auto-generated
// header-file, before including it
#define RPP_SHRPCNR_BASE 0
#include <rpp_shrpcnr_regs_addr_map.h>
#include <rpp_shrpcnr_regs_mask.h>

/******************************************************************************
 * local definitions
 *****************************************************************************/

static inline int check_valid(const rpp_shrpcnr_drv_t * drv)
{
    return (drv && drv->dev && drv->colorbits);
}

/******************************************************************************
 * rpp_shrpcnr_init
 *****************************************************************************/
int rpp_shrpcnr_init(rpp_device * dev, uint32_t base, rpp_shrpcnr_drv_t * drv)
{
    uint32_t v;
    int ret;

    /* simple base address check */
    if ( !dev || !drv )
    {
        return ( -EINVAL );
    }

    drv->dev = dev;
    drv->base = base;
    drv->version = 0;
    drv->colorbits = 0;

    /* check that rpp-bls module exists */
    ret = READL(RPP_RPP_SHRPCNR_VERSION_REG, &v );
    if (!ret)
    {
        drv->version = REG_GET_SLICE(v, RPP_SHRPCNR_VERSION);

        /* currently, there are only 12bit versions of the unit. */
        if (drv->version)
        {
            drv->colorbits = 12;
        } else {
            ret = -ENODEV;
        }
    }

    return ret;
}
/******************************************************************************
 * rpp_shrpcnr_set_linewidth
 *****************************************************************************/
int rpp_shrpcnr_set_linewidth(rpp_shrpcnr_drv_t * drv, uint16_t width)
{
    /* simple sanity check. */
    if ( !check_valid(drv) )
    {
        return -EINVAL;
    }

    /* range-check parameter */
    if ( REG_RANGE_CHECK(width, RPP_SHRPCNR_CLB_LINESIZE) )
    {
        return -ERANGE;
    }

    /* program the register. */
    return WRITEL(RPP_RPP_SHRPCNR_CLB_LINESIZE_REG, width);
}

/******************************************************************************
 * rpp_shrpcnr_linewidth
 *****************************************************************************/
int rpp_shrpcnr_linewidth(rpp_shrpcnr_drv_t * drv, uint16_t * const width)
{
    uint32_t v;
    int ret;

    /* simple sanity checks. */
    if ( !check_valid(drv) || !width )
    {
        return -EINVAL;
    }

    /* read and parse the configuration register. */
    ret = READL(RPP_RPP_SHRPCNR_CLB_LINESIZE_REG, &v);
    if (!ret) {
        *width = REG_GET_SLICE(v, RPP_SHRPCNR_CLB_LINESIZE);
    }

    return ret;
}

/******************************************************************************
 * rpp_shrpcnr_shrp_enable
 *****************************************************************************/
int rpp_shrpcnr_shrp_enable(rpp_shrpcnr_drv_t * drv, const unsigned on)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    ret = READL(RPP_RPP_SHRPCNR_CTRL_REG, &v);
    if (!ret)
    {
        REG_SET_SLICE(v, RPP_SHRPCNR_SHARPEN_EN, !!on);
        ret = WRITEL(RPP_RPP_SHRPCNR_CTRL_REG, v);
    }

    return ret;
}

/******************************************************************************
 * rpp_shrpcnr_shrp_enabled
 *****************************************************************************/
int rpp_shrpcnr_shrp_enabled(rpp_shrpcnr_drv_t * drv, unsigned * const on)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) || !on )
    {
        return ( -EINVAL );
    }

    ret = READL(RPP_RPP_SHRPCNR_CTRL_REG, &v);
    if (!ret)
    {
        *on = REG_GET_SLICE(v, RPP_SHRPCNR_SHARPEN_EN);
    }

    return ret;
}

/******************************************************************************
 * rpp_shrpcnr_set_shrp_params
 *****************************************************************************/
int rpp_shrpcnr_set_shrp_params(rpp_shrpcnr_drv_t * drv, uint8_t strength,
        uint16_t threshold)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return -EINVAL;
    }

    /* range-check parameters. */
    if (
            REG_RANGE_CHECK(strength, RPP_SHRPCNR_SHARP_FACTOR) ||
            REG_RANGE_CHECK(threshold, RPP_SHRPCNR_CORING_THR)
       )
    {
        return -ERANGE;
    }

    /* read and modify the configuration register. */
    ret = READL(RPP_RPP_SHRPCNR_PARAM_REG, &v);
    if (!ret) {
        REG_SET_SLICE(v, RPP_SHRPCNR_SHARP_FACTOR, strength);
        REG_SET_SLICE(v, RPP_SHRPCNR_CORING_THR, threshold);
        ret = WRITEL(RPP_RPP_SHRPCNR_PARAM_REG, v);
    }

    return ret;
}

/******************************************************************************
 * rpp_shrpcnr_shrp_params
 *****************************************************************************/
int rpp_shrpcnr_shrp_params
(
    rpp_shrpcnr_drv_t * drv,
    uint8_t * const     strength,
    uint16_t * const    threshold
)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !strength || !threshold )
    {
        return -EINVAL;
    }

    /* read and parse the configuration register. */
    ret = READL(RPP_RPP_SHRPCNR_PARAM_REG, &v);
    if (!ret) {
        *strength   = REG_GET_SLICE(v, RPP_SHRPCNR_SHARP_FACTOR);
        *threshold  = REG_GET_SLICE(v, RPP_SHRPCNR_CORING_THR);
    }

    return ret;
}

static const uint8_t  coeff_map[] = {
    3 + 4 + 8,      /* -8: shift count + sign + enabled */
    128,            /* -7: invalid */
    128,            /* -6: invalid */
    128,            /* -5: invalid */
    2 + 4 + 8,      /* -4: shift count + sign + enabled */
    128,            /* -3: invalid */
    1 + 4 + 8,      /* -2: shift count + sign + enabled */
    0 + 4 + 8,      /* -1: shift count + sign + enabled */
    0,              /*  0: disabled but allowed         */
    0 + 0 + 8,      /*  1: shift count + nosign + enabled   */
    1 + 0 + 8,      /*  2: shift count + nosign + enabled   */
    128,            /*  3: invalid */
    2 + 0 + 8,      /*  4: shift count + nosign + enabled   */
    128,            /*  5: invalid */
    128,            /*  6: invalid */
    128,            /*  7: invalid */
    3 + 0 + 8,      /*  8: shift count + nosign + enabled   */
};

static const uint8_t    coeff_map_r[] = {
    1,              /* index 0: shift count 0 + nosign  */
    2,              /* index 1: shift count 1 + nosign  */
    4,              /* index 2: shift count 2 + nosign  */
    8,              /* index 3: shift count 3 + nosign  */
    -1,             /* index 4: shift count 0 + sign    */
    -2,             /* index 5: shift count 1 + sign    */
    -4,             /* index 6: shift count 2 + sign    */
    -8,             /* index 7: shift count 3 + sign    */
};

/******************************************************************************
 * rpp_shrpcnr_set_shrp_filter_coeff
 *****************************************************************************/
int rpp_shrpcnr_set_shrp_filter_coeff(rpp_shrpcnr_drv_t * drv,
        const rpp_shrpcnr_coeffs_t coeffs)
{
    uint32_t regs[2];
    unsigned i;

    const uint64_t max_lookup_val = UNSIGNED_MAX(RPP_SHRPCNR_MAT_12_SHIFT);

    /* The first 6 coefficients are in the first register (with a 4bit shift),
     * the other 3 coefficients are in a second register. This means that our
     * pack_regs() function cannot be used, it would do an 8/1 packing.
     *
     * This driver fits all the individual register slices, densly packed,
     * into a temporary UINT64_T accumulator with sufficient capacity to hold all
     * elements and uses bitshift/masking operations to extract the individual
     * register values afterwards. */
    uint64_t tmp = 0;

    /* simple sanity checks. */
    if ( !check_valid(drv) || !coeffs )
    {
        return -EINVAL;
    }

    /* range-check the coefficients */
    for (i = 0; i < RPP_SHRPCNR_COEFF_NUM; ++i)
    {
        uint64_t    lookup_val;
        int8_t      coeff = coeffs[i];
        unsigned    shift;

        /* check that the coefficients are in range for
         * the lookup array */
        if ( (coeff < -8) || (coeff > 8) )
        {
            return -ERANGE;
        }

        /* lookup the configuration value from the lookup table */
        lookup_val = coeff_map[coeff + 8];

        /* Check that the coefficient is one of the known values.
         * Unsupported values are set to "128". */
        if ( lookup_val > max_lookup_val )
        {
            return -ERANGE;
        }

        /* add the current register value to the temporary value accumulator */
        shift = i * RPP_SHRPCNR_MAT_12_SHIFT;
        tmp = tmp + (lookup_val << shift);
    }
    /* extract the lower 24 bits containing 6 of the coefficients */
    regs[0] = tmp & 0xFFFFFF;
    /* extract the upper bits containing the remaining 3 coefficients. */
    regs[1] = tmp >> 24;

    return WRITE_REGS(RPP_RPP_SHRPCNR_MAT_1_REG, regs, 2);
}

/******************************************************************************
 * rpp_shrpcnr_shrp_filter_coeff
 *****************************************************************************/
int rpp_shrpcnr_shrp_filter_coeff(rpp_shrpcnr_drv_t * drv,
        rpp_shrpcnr_coeffs_t coeffs)
{
    uint32_t regs[2];
    int ret;

    /* simple sanity checks. */
    if ( !check_valid(drv) || !coeffs )
    {
        return -EINVAL;
    }

    /* read and parse the matrix configuration registers. */
    ret = READ_REGS(RPP_RPP_SHRPCNR_MAT_1_REG, regs, 2);
    if (!ret) {
        unsigned i;

        /* compose a temporary 64-bit accumulator holding all the coefficients
         * densely packed. */
        uint64_t tmp = regs[0] & 0xFFFFFF;
        tmp += (uint64_t)regs[1] << 24;

        /* loop over all coefficients and extract them. */
        for (i = 0; i < RPP_SHRPCNR_COEFF_NUM; ++i) {
            /* check whether the coefficient is enabled. if not,
             * set it's value to zero, otherwise calculate the
             * coefficient value from sign/shift using the lookup table. */
            if (tmp & RPP_SHRPCNR_MAT_11_EN_MASK) {
                coeffs[i] = coeff_map_r[tmp & RPP_SHRPCNR_MAT_11_MASK];
            } else {
                coeffs[i] = 0;
            }
            /* skip this value now for the next iteration. */
            tmp = tmp >> RPP_SHRPCNR_MAT_12_SHIFT;
        }
    }

    return ret;
}


/******************************************************************************
 * rpp_shrpcnr_cnr_enable
 *****************************************************************************/
int rpp_shrpcnr_cnr_enable(rpp_shrpcnr_drv_t * drv, const unsigned on)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return -EINVAL;
    }

    /* read and modify the control register */
    ret = READL(RPP_RPP_SHRPCNR_CTRL_REG, &v);
    if (!ret) {
        REG_SET_SLICE(v, RPP_SHRPCNR_CNR_EN, !!on);
        ret = WRITEL(RPP_RPP_SHRPCNR_CTRL_REG, v);
    }

    return ret;
}

/******************************************************************************
 * rpp_shrpcnr_cnr_enabled
 *****************************************************************************/
int rpp_shrpcnr_cnr_enabled(rpp_shrpcnr_drv_t * drv, unsigned * const on)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !on )
    {
        return -EINVAL;
    }

    /* read and modify the control register */
    ret = READL(RPP_RPP_SHRPCNR_CTRL_REG, &v);
    if (!ret) {
        *on = REG_GET_SLICE(v, RPP_SHRPCNR_CNR_EN);
    }

    return ret;
}


/******************************************************************************
 * rpp_shrpcnr_set_cnr_thresholds
 *****************************************************************************/
int rpp_shrpcnr_set_cnr_thresholds(rpp_shrpcnr_drv_t * drv,
        uint16_t thres_cb, uint16_t thres_cr)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return -EINVAL;
    }

    /* range-check the arguments */
    if (
            REG_RANGE_CHECK(thres_cb, RPP_SHRPCNR_CNR_THRES_CB) ||
            REG_RANGE_CHECK(thres_cr, RPP_SHRPCNR_CNR_THRES_CR)
       )
    {
        return -ERANGE;
    }

    /* read and modify the configuration register. */
    ret = READL(RPP_RPP_SHRPCNR_CNR_THRES_REG, &v);
    if (!ret) {
        REG_SET_SLICE(v, RPP_SHRPCNR_CNR_THRES_CB, thres_cb);
        REG_SET_SLICE(v, RPP_SHRPCNR_CNR_THRES_CR, thres_cr);
        ret = WRITEL(RPP_RPP_SHRPCNR_CNR_THRES_REG, v);
    }

    return ret;
}

/******************************************************************************
 * rpp_shrpcnr_cnr_thresholds
 *****************************************************************************/
int rpp_shrpcnr_cnr_thresholds(rpp_shrpcnr_drv_t * drv,
        uint16_t * const thres_cb, uint16_t * const thres_cr)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !thres_cb || !thres_cr )
    {
        return -EINVAL;
    }

    /* read and parse the configuration register. */
    ret = READL(RPP_RPP_SHRPCNR_CNR_THRES_REG, &v);
    if (!ret) {
        *thres_cb = REG_GET_SLICE(v, RPP_SHRPCNR_CNR_THRES_CB);
        *thres_cr = REG_GET_SLICE(v, RPP_SHRPCNR_CNR_THRES_CR);
    }

    return ret;
}

/******************************************************************************
 * rpp_shrpcnr_cad_enable
 *****************************************************************************/
int rpp_shrpcnr_cad_enable(rpp_shrpcnr_drv_t * drv, const unsigned on)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return -EINVAL;
    }

    /* read and modify the control register */
    ret = READL(RPP_RPP_SHRPCNR_CTRL_REG, &v);
    if (!ret) {
        REG_SET_SLICE(v, RPP_SHRPCNR_CAD_EN, !!on);
        ret = WRITEL(RPP_RPP_SHRPCNR_CTRL_REG, v);
    }

    return ret;
}

/******************************************************************************
 * rpp_shrpcnr_cad_enabled
 *****************************************************************************/
int rpp_shrpcnr_cad_enabled(rpp_shrpcnr_drv_t * drv, unsigned * const on)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !on )
    {
        return -EINVAL;
    }

    /* read and modify the control register */
    ret = READL(RPP_RPP_SHRPCNR_CTRL_REG, &v);
    if (!ret) {
        *on = REG_GET_SLICE(v, RPP_SHRPCNR_CAD_EN);
    }

    return ret;
}


/******************************************************************************
 * rpp_shrpcnr_set_cad_params
 *****************************************************************************/
int rpp_shrpcnr_set_cad_params(rpp_shrpcnr_drv_t * drv,
        const rpp_shrpcnr_cad_params_t * const params)
{
    uint32_t regs[4] = {0, };
    int16_t cmin, cmax;

    /* simple sanity checks. */
    if ( !check_valid(drv) || !params )
    {
        return -EINVAL;
    }

    /* the U/V min/max values are signed and have one bit more
     * than the usual colorbits (for the sign). */
    cmin = SIGNED_MIN(drv->colorbits + 1);
    cmax = SIGNED_MAX(drv->colorbits + 1);

    /* range-check the supplied parameters. */
    if (
            REG_RANGE_CHECK(params->restore_lvl,RPP_SHRPCNR_CAD_RESTORE_LVL)        ||
            (params->umin < cmin)       || (params->umin > cmax)                    ||
            (params->umax < cmin)       || (params->umax > cmax)                    ||
            (params->uneg_vmin < cmin)  || (params->uneg_vmin > cmax)               ||
            (params->uneg_vmax < cmin)  || (params->uneg_vmax > cmax)               ||
            (params->upos_vmin < cmin)  || (params->upos_vmin > cmax)               ||
            (params->upos_vmax < cmin)  || (params->upos_vmax > cmax)               ||
            (params->umin > params->umax) || (params->uneg_vmin > params->uneg_vmax)||
            (params->upos_vmin > params->upos_vmax)
       )
    {
        return -ERANGE;
    }

    /* Now prepare the registers to write. The masking in the REG_SET_SLICE macro
     * removes the extraneous sign bits. */
    REG_SET_SLICE(regs[0], RPP_SHRPCNR_CAD_RESTORE_LVL,         params->restore_lvl);
    REG_SET_SLICE(regs[1], RPP_SHRPCNR_CAD_THRESH_UNEG_VMIN,    params->uneg_vmin);
    REG_SET_SLICE(regs[1], RPP_SHRPCNR_CAD_THRESH_UPOS_VMAX,    params->uneg_vmax);
    REG_SET_SLICE(regs[2], RPP_SHRPCNR_CAD_THRESH_UPOS_VMIN,    params->upos_vmin);
    REG_SET_SLICE(regs[2], RPP_SHRPCNR_CAD_THRESH_UPOS_VMAX,    params->upos_vmax);
    REG_SET_SLICE(regs[3], RPP_SHRPCNR_CAD_THRESH_UMIN,         params->umin);
    REG_SET_SLICE(regs[3], RPP_SHRPCNR_CAD_THRESH_UMAX,         params->umax);
    return WRITE_REGS(RPP_RPP_SHRPCNR_CAD_RESTORE_LVL_REG, regs, 4);
}

/******************************************************************************
 * rpp_shrpcnr_cad_params
 *****************************************************************************/
int rpp_shrpcnr_cad_params(rpp_shrpcnr_drv_t * drv,
        rpp_shrpcnr_cad_params_t * const params)
{
    uint32_t regs[4];
    int ret;

    /* simple sanity checks. */
    if ( !check_valid(drv) || !params )
    {
        return -EINVAL;
    }

    /* read and parse the configuration registers. */
    ret = READ_REGS(RPP_RPP_SHRPCNR_CAD_RESTORE_LVL_REG, regs, 4);
    if (!ret) {
        params->restore_lvl = REG_GET_SLICE(regs[0], RPP_SHRPCNR_CAD_RESTORE_LVL);
        params->uneg_vmin   = REG_GET_SLICE(regs[1], RPP_SHRPCNR_CAD_THRESH_UNEG_VMIN);
        params->uneg_vmax   = REG_GET_SLICE(regs[1], RPP_SHRPCNR_CAD_THRESH_UPOS_VMAX);
        params->upos_vmin   = REG_GET_SLICE(regs[2], RPP_SHRPCNR_CAD_THRESH_UPOS_VMIN);
        params->upos_vmax   = REG_GET_SLICE(regs[2], RPP_SHRPCNR_CAD_THRESH_UPOS_VMAX);
        params->umin        = REG_GET_SLICE(regs[3], RPP_SHRPCNR_CAD_THRESH_UMIN);
        params->umax        = REG_GET_SLICE(regs[3], RPP_SHRPCNR_CAD_THRESH_UMAX);
        /* all the thresholds are signed numbers and need sign extension after extraction */
        params->uneg_vmin   = sign_extend(params->uneg_vmin, drv->colorbits + 1);
        params->uneg_vmax   = sign_extend(params->uneg_vmax, drv->colorbits + 1);
        params->upos_vmin   = sign_extend(params->upos_vmin, drv->colorbits + 1);
        params->upos_vmax   = sign_extend(params->upos_vmax, drv->colorbits + 1);
        params->umin        = sign_extend(params->umin,      drv->colorbits + 1);
        params->umax        = sign_extend(params->umax,      drv->colorbits + 1);
    }

    return ret;
}

/******************************************************************************
 * rpp_shrpcnr_desat_enable
 *****************************************************************************/
int rpp_shrpcnr_desat_enable(rpp_shrpcnr_drv_t * drv, const unsigned on)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return -EINVAL;
    }

    /* read and modify the control register */
    ret = READL(RPP_RPP_SHRPCNR_CTRL_REG, &v);
    if (!ret) {
        REG_SET_SLICE(v, RPP_SHRPCNR_DESAT_EN, !!on);
        ret = WRITEL(RPP_RPP_SHRPCNR_CTRL_REG, v);
    }

    return ret;
}

/******************************************************************************
 * rpp_shrpcnr_desat_enabled
 *****************************************************************************/
int rpp_shrpcnr_desat_enabled(rpp_shrpcnr_drv_t * drv, unsigned * const on)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !on )
    {
        return -EINVAL;
    }

    /* read and modify the control register */
    ret = READL(RPP_RPP_SHRPCNR_CTRL_REG, &v);
    if (!ret) {
        *on = REG_GET_SLICE(v, RPP_SHRPCNR_DESAT_EN);
    }

    return ret;
}

/******************************************************************************
 * rpp_shrpcnr_set_desat_params
 *****************************************************************************/
int rpp_shrpcnr_set_desat_params(rpp_shrpcnr_drv_t * drv,
        uint16_t threshold, uint8_t slope, uint8_t div)
{
    uint32_t regs[2] = {0, };

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return -EINVAL;
    }

    /* range-check the parameters. */
    if (
            REG_RANGE_CHECK(threshold,  RPP_SHRPCNR_CRED_THRES) ||
            REG_RANGE_CHECK(slope,      RPP_SHRPCNR_CRED_SLOPE) ||
            REG_RANGE_CHECK(div,        RPP_SHRPCNR_CRED_DIV)
       )
    {
        return -ERANGE;
    }

    /* prepare the register values for writing. */
    REG_SET_SLICE(regs[0], RPP_SHRPCNR_CRED_THRES,  threshold);
    REG_SET_SLICE(regs[1], RPP_SHRPCNR_CRED_SLOPE,  slope);
    REG_SET_SLICE(regs[1], RPP_SHRPCNR_CRED_DIV,    div);

    return WRITE_REGS(RPP_RPP_SHRPCNR_CRED_THRES_REG, regs, 2);
}

/******************************************************************************
 * rpp_shrpcnr_desat_params
 *****************************************************************************/
int rpp_shrpcnr_desat_params
(
    rpp_shrpcnr_drv_t * drv,
    uint16_t * const    threshold,
    uint8_t * const     slope,
    uint8_t * const     div
)
{
    uint32_t regs[2] = {0, };
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !threshold || !slope || !div )
    {
        return -EINVAL;
    }

    /* read and parse the configuration registers. */
    ret = READ_REGS(RPP_RPP_SHRPCNR_CRED_THRES_REG, regs, 2);
    if (!ret) {
        *threshold = REG_GET_SLICE(regs[0], RPP_SHRPCNR_CRED_THRES);
        *slope     = REG_GET_SLICE(regs[1], RPP_SHRPCNR_CRED_SLOPE);
        *div       = REG_GET_SLICE(regs[1], RPP_SHRPCNR_CRED_DIV);
    }

    return ret;
}

/******************************************************************************
 * rpp_shrpcnr_set_profile
 *****************************************************************************/
int rpp_shrpcnr_set_profile
(
    rpp_shrpcnr_drv_t *                 drv,
    const rpp_ccor_profile_t * const    profile
)
{
    int ret;
    unsigned i;

    rpp_ccor_profile_t tmp;

    /* simple sanity checks */
    if ( !check_valid(drv) || !profile )
    {
        return -EINVAL;
    }

    /* Check that the offsets are all negative. The sign is
     * assumed to always be present (a YUV-to-RGB transform
     * with offsets pre-applied) and not programmed. Strip
     * it, however, for correct programming. The input
     * profile needs to copied for that.
     */
    tmp = *profile;
    for (i = 0; i < RPP_CCOR_OFF_NO; ++i) {
        if (tmp.off[i] > 0)
        {
            return -ERANGE;
        }
        tmp.off[i] = -tmp.off[i];
    }

    /* check the profile */
    ret = rpp_ccor_check_profile(drv->colorbits, &tmp);


    /* and program the profile if successful */
    if (!ret) {
        ret = rpp_ccor_write_profile(
                drv->dev, drv->base,
                RPP_RPP_SHRPCNR_YUV2RGB_CCOR_COEFF_0_REG,
                drv->colorbits, &tmp
                );
    }

    return ret;
}

/******************************************************************************
 * rpp_shrpcnr_profile
 *****************************************************************************/
int rpp_shrpcnr_profile
(
    rpp_shrpcnr_drv_t *      drv,
    rpp_ccor_profile_t * const  profile
)
{
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !profile )
    {
        return -EINVAL;
    }

    ret = rpp_ccor_read_profile(
            drv->dev, drv->base,
            RPP_RPP_SHRPCNR_YUV2RGB_CCOR_COEFF_0_REG,
            drv->colorbits, profile
            );
    if (!ret) {
        /* when reading the profile back, make sure the offsets
         * are all negative. Note that the sign-bit is not
         * present in the hardware and must be added "manually". */
        unsigned i;
        for (i = 0; i < RPP_CCOR_OFF_NO; ++i)
        {
            profile->off[i] = -profile->off[i];
        }
    }

    return ret;
}


