/******************************************************************************
 *
 * Copyright 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_mv_out_drv.c
 *
 * @brief   Implementation of RPP Machine Vision Output pipeline unit driver.
 *
 *****************************************************************************/
#include <rpp_mv_out_drv.h>

#define RPP_MV_OUT_BASE  (0)
#include <rpp_mv_out_module_base_addr.h>


/******************************************************************************
 * local definitions
 *****************************************************************************/

#define DO_INIT(mod, base)                                  \
    if (!ret) {                                             \
        ret = rpp_ ## mod ## _init(dev, base, &(drv->mod)); \
    }

/******************************************************************************
 * rpp_mv_out_init
 *****************************************************************************/
int rpp_mv_out_init(rpp_device * dev, uint32_t base, rpp_mv_out_drv_t * drv)
{
    int ret = 0;

    /* simple sanity check */
    if ( !dev || !drv )
    {
        return ( -EINVAL );
    }

    /* call the initialization routines of all sub-units. */
    DO_INIT(gamma_out,  base + RPP_ADDR_BASE_MV_OUT_GAMMA_OUT);
    DO_INIT(ccor,       base + RPP_ADDR_BASE_MV_OUT_CCOR);
    DO_INIT(xyz2luv,    base + RPP_ADDR_BASE_MV_OUT_XYZ2LUV);
    DO_INIT(mv_outregs, base + RPP_ADDR_BASE_MV_OUT_MV_OUTREGS);
    DO_INIT(is,         base + RPP_ADDR_BASE_MV_OUT_IS);
    DO_INIT(out_if,     base + RPP_ADDR_BASE_MV_OUT_OUT_IF);

    return ret;
}

/******************************************************************************
 * rpp_mv_out_set_rgb
 *****************************************************************************/
int rpp_mv_out_set_rgb(rpp_mv_out_drv_t * drv,
        rpp_mv_out_input_t input, rpp_gamma_out_curve_t * curve)
{
    int ret = 0;
    rpp_mv_out_format_t fmt;

    /* simple sanity checks. */
    if ( !drv ) {
        return -EINVAL;
    }

    fmt = (input == RPP_MV_OUT_IN_SEL_DEBAYERED) ?
        RPP_MV_OUT_FORMAT_SRGB_16   :   RPP_MV_OUT_FORMAT_SRGB_12;

    /* configure the gamma curve if wanted. otherwise disable GAMMA_OUT. */
    if (curve)
        ret = rpp_gamma_out_set_curve(&drv->gamma_out, curve);

    if (!ret)
        ret = rpp_gamma_out_enable(&drv->gamma_out, (curve) ? 1 : 0);

    /* CCOR is bypassed in hardware... */

    /* configure the OUTREGS input multiplexer */
    if (!ret)
        ret = rpp_mv_outregs_set_mode(&drv->mv_outregs,
                RPP_MV_OUT_IN_SEL_MAX, input);

    /* configure the OUTREGS output format */
    if (!ret)
        ret = rpp_mv_outregs_set_format(&drv->mv_outregs, fmt);

    /* that's it, already. */
    return ret;
}

/******************************************************************************
 * rpp_mv_out_set_rgby
 *****************************************************************************/
int rpp_mv_out_set_rgby(rpp_mv_out_drv_t * drv, unsigned do_srgb,
        rpp_mv_out_input_t input, rpp_gamma_out_curve_t * curve,
        rpp_ccor_profile_t const * rgb2yuv)
{
    int ret = 0;
    rpp_mv_out_format_t fmt;
    rpp_ccor_range_t    range = RPP_CCOR_RANGE_FULL;

    /* simple sanity checks. */
    if ( !drv || !curve || !rgb2yuv ) {
        return -EINVAL;
    }

    /* get the correct output format */
    fmt = (do_srgb) ? RPP_MV_OUT_FORMAT_SRGBY_8 : RPP_MV_OUT_FORMAT_RGBY_8;

    /* configure the gamma curve. */
    ret = rpp_gamma_out_set_curve(&drv->gamma_out, curve);
    if (!ret)
        ret = rpp_gamma_out_enable(&drv->gamma_out, 1);

    /* configure the CCOR profile */
    if (!ret)
        ret = rpp_ccor_set_range_and_profile(&drv->ccor, range, range, rgb2yuv);

    /* configure the OUTREGS input multiplexer */
    if (!ret)
        ret = rpp_mv_outregs_set_mode(&drv->mv_outregs,
                RPP_MV_OUT_IN_SEL_MAX, input);

    /* configure the OUTREGS output format */
    if (!ret)
        ret = rpp_mv_outregs_set_format(&drv->mv_outregs, fmt);

    /* that's it, already. */
    return ret;
}

/******************************************************************************
 * rpp_mv_out_set_yuv
 *****************************************************************************/
int rpp_mv_out_set_yuv(rpp_mv_out_drv_t * drv,
        rpp_mv_out_input_t              input,
        rpp_gamma_out_curve_t *         curve,
        rpp_ccor_profile_t *            rgb2yuv,
        unsigned                        legal,
        rpp_mv_out_422_method_t         method_422,
        unsigned                        output_420
        )
{
    int ret;
    rpp_mv_out_format_t    fmt;
    enum rpp_ccor_range_e       range;

    /* simple sanity checks. */
    if ( !drv || !curve || !rgb2yuv )
    {
        return -EINVAL;
    }
   
    fmt = (input == RPP_MV_OUT_IN_SEL_DEBAYERED) ?
        RPP_MV_OUT_FORMAT_YUV422_24 : RPP_MV_OUT_FORMAT_YUV422_12;

    /* if 420 output is selected, the format must be incremented by 1. */
    fmt += !!output_420;
    /* select legal range. */
    range = legal ? RPP_CCOR_RANGE_LEGAL : RPP_CCOR_RANGE_FULL;

    /* configure the gamma curve. */
    ret = rpp_gamma_out_set_curve(&drv->gamma_out, curve);
    if (!ret)
        ret = rpp_gamma_out_enable(&drv->gamma_out, 1);

    /* configure the color-space conversion. */
    if (!ret)
        ret = rpp_ccor_set_range_and_profile(&drv->ccor,
                range, range, rgb2yuv);

    /* configure the OUTREGS input multiplexer */
    if (!ret)
        ret = rpp_mv_outregs_set_mode(&drv->mv_outregs,
                RPP_MV_OUT_IN_SEL_MAX, input);
    /* configure the OUTREGS output format */
    if (!ret)
        ret = rpp_mv_outregs_set_format(&drv->mv_outregs, fmt);
    /* configure the OUTREGS 4:2:2 subsampling method. */
    if (!ret)
        ret = rpp_mv_outregs_set_422_method(&drv->mv_outregs, method_422);

    /* that's it already. */
    return ret;
}


/******************************************************************************
 * rpp_mv_out_set_luv
 *****************************************************************************/
int rpp_mv_out_set_luv(rpp_mv_out_drv_t * drv,
        rpp_mv_out_input_t              input,
        rpp_mv_out_luv_mode_t           subsampling,
        rpp_mv_out_422_method_t         method_422,
        uint16_t                        u_ref,
        uint16_t                        v_ref,
        uint8_t                         luma_fac,
        uint8_t                         chroma_fac
        )
{
    int ret;
    rpp_mv_out_format_t     fmt;
    rpp_ccor_profile_t      rgb2xyz;
    rpp_ccor_range_t        range = RPP_CCOR_RANGE_FULL;

    /* simple sanity check */
    if ( !drv )
    {
        return -EINVAL;
    }

    /* range-check parameters. */
    if (subsampling > RPP_MV_OUT_LUV_MAX)
    {
        return -ERANGE;
    }

    /* select the output bit width according to the selected input. */
    fmt = (input == RPP_MV_OUT_IN_SEL_DEBAYERED) ?
        RPP_MV_OUT_FORMAT_LUV444_16 : RPP_MV_OUT_FORMAT_LUV444_12;

    /* add the sub-sampling option to the output format. */
    fmt += subsampling;

    /* get the default RGB => XYZ transformation ready. */
    SET_CCOR_RGB2XYZ(&rgb2xyz);

    /* disable GAMMA_OUT */
    ret = rpp_gamma_out_enable(&drv->gamma_out, 0);

    /* program CCOR */
    if (!ret)
        ret = rpp_ccor_set_range_and_profile(&drv->ccor, range, range, &rgb2xyz);

    /* program XYZ2LUV */
    if (!ret)
        ret = rpp_xyz2luv_set_ref(&drv->xyz2luv, u_ref, v_ref);
    if (!ret)
        ret = rpp_xyz2luv_set_output_factors(&drv->xyz2luv, luma_fac, chroma_fac);

    /* configure the OUTREGS input multiplexer */
    if (!ret)
        ret = rpp_mv_outregs_set_mode(&drv->mv_outregs,
                RPP_MV_OUT_IN_SEL_MAX, input);
    /* configure the OUTREGS output format */
    if (!ret)
        ret = rpp_mv_outregs_set_format(&drv->mv_outregs, fmt);
    /* configure the OUTREGS 4:2:2 subsampling method, if required */
    if (!ret && subsampling)
        ret = rpp_mv_outregs_set_422_method(&drv->mv_outregs, method_422);

    /* that's it already. */
    return ret;
}

/******************************************************************************
 * rpp_mv_out_enable
 *****************************************************************************/
int rpp_mv_out_enable(rpp_mv_out_drv_t * drv, unsigned num_frames, unsigned data_mode)
{
    int ret;

    /* simple sanity check. */
    if ( !drv )
    {
        return -EINVAL;
    }

    /* configure the number of output frames */
    ret = rpp_out_if_set_frames(&drv->out_if, num_frames);
    
    /* configure DATA vs. VIDEO mode. */
    if (!ret)
        ret = rpp_out_if_set_data_mode(&drv->out_if, data_mode);

    /* and set the interface active at next FRAME_START */
    if (!ret)
        rpp_out_if_set_active(&drv->out_if, 1);

    return ret;
}

/******************************************************************************
 * rpp_mv_out_disable
 *****************************************************************************/
int rpp_mv_out_disable(rpp_mv_out_drv_t * drv)
{
    /* simple sanity check. */
    if ( !drv )
    {
        return -EINVAL;
    }

    return rpp_out_if_set_active(&drv->out_if, 0);
}

