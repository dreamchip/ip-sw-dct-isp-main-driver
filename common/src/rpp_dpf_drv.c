/******************************************************************************
 *
 * Copyright 2019 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_dpf_drv.c
 *
 * @brief   Implementation of DPF (bilateral denoising) unit driver
 *
 *****************************************************************************/
#include "rpp_dpf_drv.h"

// make sure that the base is 0 in the auto-generated
// header-file, before including it
#define RPP_DPF_BASE 0
#include "rpp_dpf_regs_addr_map.h"
#include "rpp_dpf_regs_mask.h"

/******************************************************************************
 * local definitions
 *****************************************************************************/

static inline int check_valid(const rpp_dpf_drv_t * drv)
{
    return (drv && drv->dev);
}

/******************************************************************************
 * rpp_dpf_init
 *****************************************************************************/
int rpp_dpf_init(rpp_device * dev, uint32_t base, rpp_dpf_drv_t * drv)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !dev || !drv ) {
        return ( -EINVAL );
    }

    drv->dev = dev;
    drv->base = base;
    drv->version = 0;
    /* read and parse the version register. */
    ret = READL(RPP_RPP_DPF_VERSION_REG, &v);
    if (!ret)
    {
        drv->version = REG_GET_SLICE( v, RPP_DPF_DPF_VERSION);
        /* check whether the unit is compatible with this driver.
         * versions earlier than 4 did have a different register
         * interface (e.g. just one set of NLL function coefficients)
         * and cannot be used. */
        if (drv->version < 4)
        {
            ret = -ENODEV;
        }
    }

    return ret;
}


/******************************************************************************
 * rpp_dpf_set_mode
 *****************************************************************************/
int rpp_dpf_set_mode(rpp_dpf_drv_t * drv, const rpp_dpf_mode_t* const mode)
{
    rpp_dpf_mode_t tmp;

    /* simple sanity checks */
    if ( !check_valid(drv) || !mode)
    {
        return ( -EINVAL );
    }

    /* copy the 'mode' register contents into our temporary
     * mode structure. */
    tmp.mode = mode->mode;

    /* clean out unused flags. */
    tmp._unused1 = 0;
    tmp._unused2 = 0;

    /* now write the clean register value. */
    return WRITEL(RPP_RPP_DPF_MODE_REG, tmp.mode);
}

/******************************************************************************
 * rpp_dpf_mode
 *****************************************************************************/
int rpp_dpf_mode(rpp_dpf_drv_t * drv, rpp_dpf_mode_t* const mode)
{
    /* simple sanity checks */
    if ( !check_valid(drv) || !mode)
    {
        return ( -EINVAL );
    }

    /* the bits in the 'mode' arguments have been chosen to match the
     * register slice layout of the register, so a simple readl() here
     * will suffer to have the data decoded. */
    return READL(RPP_RPP_DPF_MODE_REG, &(mode->mode));
}

/******************************************************************************
 * rpp_dpf_set_spatial_strength
 *****************************************************************************/
int rpp_dpf_set_spatial_strength(rpp_dpf_drv_t * drv,
        const rpp_dpf_spatial_strength_t* const strength)
{
    uint32_t regs[3];

    /* simple sanity checks */
    if ( !check_valid(drv) || !strength )
    {
        return (-EINVAL);
    }

    /* range-check the supplied parameters. */
    if(
            (REG_RANGE_CHECK(strength->red,   RPP_DPF_INV_WEIGHT_R)) ||
            (REG_RANGE_CHECK(strength->blue,  RPP_DPF_INV_WEIGHT_B)) ||
            (REG_RANGE_CHECK(strength->green, RPP_DPF_INV_WEIGHT_G))
      )
    {
        return (-ERANGE);
    }

    /* the data has been range-checked and the values are in adjacent
     * registers without additional slices in there. */
    regs[0] = strength->red;
    regs[1] = strength->green;
    regs[2] = strength->blue;
    return WRITE_REGS(RPP_RPP_DPF_STRENGTH_R_REG, regs, 3);
}

/******************************************************************************
 * rpp_dpf_spatial_strength
 *****************************************************************************/
int rpp_dpf_spatial_strength(rpp_dpf_drv_t * drv,
        rpp_dpf_spatial_strength_t* const strength)
{
    uint32_t regs[3];
    int ret;

    /* simple sanity checks. */
    if ( !check_valid(drv) || !strength)
    {
        return (-EINVAL);
    }

    /* read the adjacent registers in one operation. */
    ret = READ_REGS(RPP_RPP_DPF_STRENGTH_R_REG, regs, 3);

    /* if successful, decode the data. */
    if (!ret) {
        strength->red   = REG_GET_SLICE(regs[0], RPP_DPF_INV_WEIGHT_R);
        strength->green = REG_GET_SLICE(regs[1], RPP_DPF_INV_WEIGHT_G);
        strength->blue  = REG_GET_SLICE(regs[2], RPP_DPF_INV_WEIGHT_B);
    }

    return ret;
}

/******************************************************************************
 * rpp_dpf_set_spatial_weight
 *****************************************************************************/
int rpp_dpf_set_spatial_weight(rpp_dpf_drv_t * drv, const rpp_dpf_spatial_weight_t* const weight)
{
    uint32_t regs[4];
    unsigned i;

    /* simple sanity checks. */
    if ( !check_valid(drv) || !weight)
    {
        return (-EINVAL);
    }

    /* all the weights must have a maximum of 16. */
    for (i = 0; i < RPP_DPF_COEFF_NUM; ++i)
    {
        if (
                (weight->g [i] > 16) ||
                (weight->rb[i] > 16)
           )
        {
            return -ERANGE;
        }
    }

    /* now, pack the register values closely. */
    PACK_REGS(
            RPP_DPF_COEFF_NUM,
            RPP_DPF_S_WEIGHT_G2_SHIFT,
            RPP_DPF_S_WEIGHT_G1_MASK,
            weight->g, regs
            );
    PACK_REGS(
            RPP_DPF_COEFF_NUM,
            RPP_DPF_S_WEIGHT_G2_SHIFT,
            RPP_DPF_S_WEIGHT_G1_MASK,
            weight->rb, regs + 2
            );

    /* and write the registers efficiently. */
    return WRITE_REGS(RPP_RPP_DPF_S_WEIGHT_G_1_4_REG, regs, 4);
}

/******************************************************************************
 * rpp_dpf_spatial_weight
 *****************************************************************************/
int rpp_dpf_spatial_weight(rpp_dpf_drv_t * drv, rpp_dpf_spatial_weight_t* const weight)
{
    uint32_t regs[4];
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !weight)
    {
        return (-EINVAL);
    }

    /* read the registers */
    ret = READ_REGS(RPP_RPP_DPF_S_WEIGHT_G_1_4_REG, regs, 4);

    /* if successful, unpack the arrays */
    if (!ret) {
        UNPACK_REGS(
                RPP_DPF_COEFF_NUM,
                RPP_DPF_S_WEIGHT_G2_SHIFT,
                RPP_DPF_S_WEIGHT_G1_MASK,
                regs, weight->g
                );
        UNPACK_REGS(
                RPP_DPF_COEFF_NUM,
                RPP_DPF_S_WEIGHT_G2_SHIFT,
                RPP_DPF_S_WEIGHT_G1_MASK,
                regs + 2, weight->rb
                );
    }

    return ret;
}

/******************************************************************************
 * rpp_dpf_set_nll_coeff
 *****************************************************************************/
int rpp_dpf_set_nll_coeff(rpp_dpf_drv_t * drv, const rpp_dpf_nll_coeff_t* const nll_coeff)
{
    uint32_t regs[RPP_DPF_NLL_COEFF_NUM];
    unsigned i;
    int ret;
    uint16_t vmax = RPP_DPF_NLL_G_COEFF_N_MASK;

    /* simple sanity checks */
    if ( !check_valid(drv) || !nll_coeff )
    {
        return -EINVAL;
    }

    /* range-check arguments */
    for (i = 0; i < RPP_DPF_NLL_COEFF_NUM; ++i)
    {
        if (
                (nll_coeff->coeff_g [i] < 1) || (nll_coeff->coeff_g [i] > vmax) ||
                (nll_coeff->coeff_rb[i] < 1) || (nll_coeff->coeff_rb[i] > vmax)
            )
        {
            return -ERANGE;
        }
        /* the passed values are uint16_t but the registers are 32bit wide,
         * therefore a copy is needed. */
        regs[i] = nll_coeff->coeff_g[i];
    }

    /* if the range check was successful and the register array has
     * been prepared, write all the green registers now. */
    ret = WRITE_REGS(RPP_RPP_DPF_NLL_G_COEFF__0_REG, regs, RPP_DPF_NLL_COEFF_NUM);

    /* if successful, copy the r/b coefficients and write them as well. */
    if (!ret) {
        for (i = 0; i < RPP_DPF_NLL_COEFF_NUM; ++i)
        {
            regs[i] = nll_coeff->coeff_rb[i];
        }
        ret = WRITE_REGS(RPP_RPP_DPF_NLL_RB_COEFF__0_REG, regs, RPP_DPF_NLL_COEFF_NUM);
    }

    return ret;
}

/******************************************************************************
 * rpp_dpf_nll_coeff
 *****************************************************************************/
int rpp_dpf_nll_coeff(rpp_dpf_drv_t * drv, rpp_dpf_nll_coeff_t* const nll_coeff)
{
    uint32_t regs[RPP_DPF_NLL_COEFF_NUM];
    unsigned i;
    int ret;

    /* simple sanity checks... */
    if ( !check_valid(drv) || !nll_coeff)
    {
        return (-EINVAL);
    }

    /* read all green registers in one go. */
    ret = READ_REGS(RPP_RPP_DPF_NLL_G_COEFF__0_REG, regs, RPP_DPF_NLL_COEFF_NUM);

    /* if successful, decode the data. */
    if (!ret)
    {
        for (i = 0; i < RPP_DPF_NLL_COEFF_NUM; ++i) {
            nll_coeff->coeff_g [i] = REG_GET_SLICE(regs[i], RPP_DPF_NLL_G_COEFF_N);
        }
        /* read the red/blue registers as well */
        ret = READ_REGS(RPP_RPP_DPF_NLL_RB_COEFF__0_REG, regs, RPP_DPF_NLL_COEFF_NUM);
    }

    /* if successful, decode the data. */
    if (!ret)
    {
        for (i = 0; i < RPP_DPF_NLL_COEFF_NUM; ++i) {
            nll_coeff->coeff_rb[i] = REG_GET_SLICE(regs[i], RPP_DPF_NLL_RB_COEFF_N);
        }
    }

    return ret;
}

/******************************************************************************
 * rpp_dpf_set_nf_gain
 *****************************************************************************/
int rpp_dpf_set_nf_gain(rpp_dpf_drv_t * drv,
        const uint16_t red,
        const uint16_t green_red,
        const uint16_t green_blue,
        const uint16_t blue
        )
{
    uint32_t regs[4];

    /* simple sanity checks */
    if ( !check_valid(drv) )
    {
        return (-EINVAL);
    }

    /* range-check the arguments */
    if (
            (REG_RANGE_CHECK(red,           RPP_DPF_DPF_NF_GAIN_R))  ||
            (REG_RANGE_CHECK(green_red,     RPP_DPF_DPF_NF_GAIN_GR)) ||
            (REG_RANGE_CHECK(green_blue,    RPP_DPF_DPF_NF_GAIN_GB)) ||
            (REG_RANGE_CHECK(blue,          RPP_DPF_DPF_NF_GAIN_B))
       )
    {
        return (-ERANGE);
    }

    regs[0] = red;
    regs[1] = green_red;
    regs[2] = green_blue;
    regs[3] = blue;
    return WRITE_REGS(RPP_RPP_DPF_NF_GAIN_R_REG, regs, 4);
}

/******************************************************************************
 * rpp_dpf_nf_gain
 *****************************************************************************/
int rpp_dpf_nf_gain(rpp_dpf_drv_t * drv,
        uint16_t * const red,
        uint16_t * const green_red,
        uint16_t * const green_blue,
        uint16_t * const blue
        )
{
    uint32_t regs[4];
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !red || !green_red || !green_blue || !blue )
    {
        return (-EINVAL);
    }

    /* read the registers */
    ret = READ_REGS(RPP_RPP_DPF_NF_GAIN_R_REG, regs, 4);

    /* if successful, decode the data */
    if (!ret) {
        *red        = REG_GET_SLICE(regs[0], RPP_DPF_DPF_NF_GAIN_R);
        *green_red  = REG_GET_SLICE(regs[1], RPP_DPF_DPF_NF_GAIN_GR);
        *green_blue = REG_GET_SLICE(regs[2], RPP_DPF_DPF_NF_GAIN_GB);
        *blue       = REG_GET_SLICE(regs[3], RPP_DPF_DPF_NF_GAIN_B);
    }
    
    return ret;
}
