/******************************************************************************
 *
 * Copyright 2019, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_hist_drv.c
 *
 * @brief   Implementation of histogram measurement driver
 *
 *****************************************************************************/
#include <rpp_hist_drv.h>

// make sure that the base is 0 in the auto-generated
// header-file, before including it
#define RPP_HIST_BASE 0
#include <rpp_hist_regs_addr_map.h>
#include <rpp_hist_regs_mask.h>

/******************************************************************************
 * local definitions
 *****************************************************************************/

static inline int check_valid(const rpp_hist_drv_t * drv)
{
    return (drv && drv->dev && drv->colorbits);
}

/******************************************************************************
 * rpp_hist_init
 *****************************************************************************/
int rpp_hist_init(rpp_device * dev, uint32_t base, rpp_hist_drv_t * drv)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !dev || !drv )
    {
        return ( -EINVAL );
    }
    drv->dev = dev;
    drv->base = base;
    drv->version = 0;
    drv->colorbits = 0;

    /* read and parse the version register. */
    ret = READL(RPP_RPP_HIST_VERSION_REG, &v);
    if (!ret) {
        drv->version = REG_GET_SLICE(v, RPP_HIST_HIST_VERSION);
        switch(drv->version) {
            case 0x03:
                /* 12bit version */
                drv->colorbits = 12;
                break;
            case 0x04:
                /* 20bit version */
                drv->colorbits = 20;
                break;
            case 0x05:
                /* 24bit version */
                drv->colorbits = 24;
                break;
            default:
                /* unsupported version */
                ret = -ENODEV;
        }
    }
    if (!ret) {
        /* program the CTRL register to allow shadow updates. */
        v = 0;
        REG_SET_SLICE(v, RPP_HIST_HIST_UPDATE_ENABLE, 1);
        ret = WRITEL(RPP_RPP_HIST_CTRL_REG, v);
    }

    return ret;
}

/******************************************************************************
 * rpp_hist_disable
 *****************************************************************************/
int rpp_hist_disable(rpp_hist_drv_t * drv)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    ret = READL(RPP_RPP_HIST_MODE_REG, &v);
    if (!ret)
    {
        REG_SET_SLICE( v, RPP_HIST_HIST_MODE, RPP_HIST_MEASURE_MODE_DISABLE );
        ret = WRITEL(RPP_RPP_HIST_MODE_REG, v);
    }

    return ret;
}

/******************************************************************************
 * rpp_hist_set_mode
 *****************************************************************************/
int rpp_hist_set_mode(rpp_hist_drv_t * drv,
        const unsigned mode, const unsigned channel)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    /* range check */
    if (
            (mode >= RPP_HIST_MEASURE_MODE_MAX) ||
            (channel >= RPP_HIST_MEASURE_CHANNEL_MAX)
        )
    {
        return ( -ERANGE );
    }

    ret = READL(RPP_RPP_HIST_MODE_REG, &v);
    if (!ret) {
        REG_SET_SLICE( v, RPP_HIST_HIST_MODE, mode);
        ret = WRITEL(RPP_RPP_HIST_MODE_REG, v);
    }

    if (!ret)
    {
        ret = READL(RPP_RPP_HIST_CHANNEL_SEL_REG, &v);
    }
    if (!ret)
    {
        REG_SET_SLICE( v, RPP_HIST_CHANNEL_SELECT, channel );
        ret = WRITEL(RPP_RPP_HIST_CHANNEL_SEL_REG, v);
    }

    return ret;
}

/******************************************************************************
 * rpp_hist_mode
 *****************************************************************************/
int rpp_hist_mode(rpp_hist_drv_t * drv,
        unsigned * const mode, unsigned * const channel)
{
    uint32_t v;
    int ret;

    /* simple base address check */
    if ( !check_valid(drv) || !mode || !channel )
    {
        return ( -EINVAL );
    }

    ret = READL(RPP_RPP_HIST_MODE_REG, &v);
    if (!ret)
    {
        *mode = REG_GET_SLICE( v, RPP_HIST_HIST_MODE );
        ret = READL(RPP_RPP_HIST_CHANNEL_SEL_REG, &v);
    }

    if (!ret)
    {
        *channel = REG_GET_SLICE( v, RPP_HIST_CHANNEL_SELECT);
    }

    return ret;
}

/******************************************************************************
 * rpp_hist_set_csm
 *****************************************************************************/
int rpp_hist_set_csm(rpp_hist_drv_t * drv, const rpp_hist_csm_t csm)
{
    uint32_t regs[3];

    /* simple base address check */
    if ( !check_valid(drv) || !csm )
    {
        return ( -EINVAL );
    }

    /* range check */
    if (
            REG_RANGE_CHECK(GET_HIST_CSM_COEFF(csm, 0), RPP_HIST_COEFF_R) ||
            REG_RANGE_CHECK(GET_HIST_CSM_COEFF(csm, 1), RPP_HIST_COEFF_G) ||
            REG_RANGE_CHECK(GET_HIST_CSM_COEFF(csm, 2), RPP_HIST_COEFF_B)
        )
    {
        return ( -ERANGE );
    }

    regs[0] = GET_HIST_CSM_COEFF(csm, 0);
    regs[1] = GET_HIST_CSM_COEFF(csm, 1);
    regs[2] = GET_HIST_CSM_COEFF(csm, 2);

    return WRITE_REGS(RPP_RPP_HIST_COEFF_R_REG, regs, 3);
}

/******************************************************************************
 * rpp_hist_csm
 *****************************************************************************/
int rpp_hist_csm(rpp_hist_drv_t * drv, rpp_hist_csm_t csm)
{
    uint32_t regs[3];
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !csm )
    {
        return ( -EINVAL );
    }

    ret = READ_REGS(RPP_RPP_HIST_COEFF_R_REG, regs, 3);

    if (!ret) {
        SET_HIST_CSM_COEFF(csm, 0, REG_GET_SLICE( regs[0], RPP_HIST_COEFF_R ));
        SET_HIST_CSM_COEFF(csm, 1, REG_GET_SLICE( regs[1], RPP_HIST_COEFF_G ));
        SET_HIST_CSM_COEFF(csm, 2, REG_GET_SLICE( regs[2], RPP_HIST_COEFF_B ));
    }

    return ret;
}

/******************************************************************************
 * rpp_hist_set_sample_range
 *****************************************************************************/
int rpp_hist_set_sample_range(rpp_hist_drv_t * drv,
        const unsigned shift, const uint32_t offset)
{
    uint32_t v;
    uint32_t off_max, shift_max, off_mask;
    int ret;

    /* simple base address check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    shift_max = drv->colorbits - 5;
    off_max = UNSIGNED_MAX(drv->colorbits);
    off_mask = off_max << RPP_HIST_SAMPLE_OFFSET_SHIFT;
    /* range check */
    if ( (shift > shift_max) || (offset > off_max) )
    {
        return ( -ERANGE );
    }

    ret = READL(RPP_RPP_HIST_SAMPLE_RANGE_REG, &v);
    if (!ret)
    {
        REG_SET_SLICE( v, RPP_HIST_SAMPLE_SHIFT, shift);
        /* no REG_SET_SLICE because of dynamic bitwidth in offset. */
        v = (v & ~off_mask) | (offset << RPP_HIST_SAMPLE_OFFSET_SHIFT);
        ret = WRITEL(RPP_RPP_HIST_SAMPLE_RANGE_REG, v);
    }

    return ret;
}

/******************************************************************************
 * rpp_hist_sample_range
 *****************************************************************************/
int rpp_hist_sample_range(rpp_hist_drv_t * drv,
        unsigned * const shift, uint32_t * const offset)
{
    uint32_t v;
    uint32_t off_mask;
    int ret;

    /* simple base address check */
    if ( !check_valid(drv) || !shift || !offset )
    {
        return ( -EINVAL );
    }

    off_mask = UNSIGNED_MAX(drv->colorbits);
    
    ret = READL(RPP_RPP_HIST_SAMPLE_RANGE_REG, &v);
    if (!ret) {
        *shift  = REG_GET_SLICE( v, RPP_HIST_SAMPLE_SHIFT );
        /* no REG_GET_SLICE() here because of dynamic bitwidth in offset. */
        *offset = (v >> RPP_HIST_SAMPLE_OFFSET_SHIFT) & off_mask;
    }

    return ret;
}

/******************************************************************************
 * rpp_hist_set_meas_window
 *****************************************************************************/
int rpp_hist_set_meas_window(rpp_hist_drv_t * drv,
        const rpp_window_t * win, const rpp_hist_step_t * steps)
{
    uint32_t regs[4];
    int ret;
    uint32_t nw, nh;

    /* simple base address check */
    if ( !check_valid(drv) || !win || !steps )
    {
        return ( -EINVAL );
    }

    if ( (win->width % 5) || (win->height % 5) )
    {
        return ( -EINVAL );
    }

    nw = win->width / 5;
    nh = win->height / 5;
    /* range check */
    if (
            REG_RANGE_CHECK(win->hoff    , RPP_HIST_HIST_H_OFFSET)  ||
            REG_RANGE_CHECK(win->voff    , RPP_HIST_HIST_V_OFFSET)  ||
            REG_RANGE_CHECK(nw           , RPP_HIST_HIST_H_SIZE)    ||
            REG_RANGE_CHECK(nh           , RPP_HIST_HIST_V_SIZE)    ||
            REG_RANGE_CHECK(steps->v_stepsize, RPP_HIST_V_STEPSIZE) ||
            REG_RANGE_CHECK(steps->h_step_inc, RPP_HIST_H_STEP_INC)
        )
    {
        return ( -ERANGE );
    }

    regs[0] = win->hoff;
    regs[1] = win->voff;
    regs[2] = nw;
    regs[3] = nh;

    /* measurement window */
    ret = WRITE_REGS(RPP_RPP_HIST_H_OFFS_REG, regs, 4);

    /* sub-sampling configuration */
    if (!ret) {
        uint32_t v = 0;
        REG_SET_SLICE(v, RPP_HIST_V_STEPSIZE, steps->v_stepsize);
        REG_SET_SLICE(v, RPP_HIST_H_STEP_INC, steps->h_step_inc);
        ret = WRITEL(RPP_RPP_HIST_SUBSAMPLING_REG, v);
    }

    if (!ret) {
        /* for interrupt raising */
        /* height-1u => raise interrupt after last line in measurement window */
        ret = WRITEL(RPP_RPP_HIST_LAST_MEAS_LINE_REG,  win->height-1u);
    }
    if (!ret) {
        /* don't support "fast" channel switching */
        ret = WRITEL(RPP_RPP_HIST_FORCED_UPD_START_LINE_REG, 0);
    }

    return ret;
}

/******************************************************************************
 * rpp_hist_meas_window
 *****************************************************************************/
int rpp_hist_meas_window(rpp_hist_drv_t * drv,
        rpp_window_t * const win, rpp_hist_step_t * const steps)
{
    uint32_t regs[4];
    int ret;

    /* simple base address check */
    if ( !check_valid(drv) || !win || !steps )
    {
        return ( -EINVAL );
    }

    /* measurement window */
    ret = READ_REGS(RPP_RPP_HIST_H_OFFS_REG, regs, 4);
    if (!ret) {
        win->hoff   = REG_GET_SLICE(regs[0], RPP_HIST_HIST_H_OFFSET);
        win->voff   = REG_GET_SLICE(regs[1], RPP_HIST_HIST_V_OFFSET);
        win->width  = REG_GET_SLICE(regs[2], RPP_HIST_HIST_H_SIZE) * 5u;
        win->height = REG_GET_SLICE(regs[3], RPP_HIST_HIST_V_SIZE) * 5u;
        ret = READL(RPP_RPP_HIST_SUBSAMPLING_REG, &(regs[0]));
    }

    if (!ret) {
        steps->v_stepsize = REG_GET_SLICE(regs[0], RPP_HIST_V_STEPSIZE);
        steps->h_step_inc = REG_GET_SLICE(regs[0], RPP_HIST_H_STEP_INC);
    }

    return ret;
}

#define NR_DX_REGS NUM_REGS(RPP_HIST_WEIGHT_NUM, RPP_HIST_HIST_WEIGHT_10_SHIFT)

/******************************************************************************
 * rpp_hist_set_weights
 *****************************************************************************/
int rpp_hist_set_weights(rpp_hist_drv_t * drv, const rpp_hist_weights_t weights)
{
    uint32_t regs[NR_DX_REGS];
    unsigned i;

    /* simple sanity checks */
    if ( !check_valid(drv) || !weights )
    {
        return ( -EINVAL );
    }

    /* Range check - the maximum value for the weights is
     * not given by the register slice (5bits: 0..31), but
     * limited by the histogram counter bit width and shall
     * be no larger than 16. */
    for (i = 0; i < RPP_HIST_WEIGHT_NUM; ++i)
    {
        if ( weights[i] > RPP_HIST_MAX_WEIGHT )
        {
            return ( -ERANGE );
        }
    }

    PACK_REGS(
            RPP_HIST_WEIGHT_NUM,
            RPP_HIST_HIST_WEIGHT_10_SHIFT,
            RPP_HIST_HIST_WEIGHT_00_MASK,
            weights,
            regs
            );

    return WRITE_REGS(RPP_RPP_HIST_WEIGHT_00TO30_REG, regs, NR_DX_REGS);
}

/******************************************************************************
 * rpp_hist_weights
 *****************************************************************************/
int rpp_hist_weights(rpp_hist_drv_t * drv, rpp_hist_weights_t weights)
{
    uint32_t regs[NR_DX_REGS];
    int ret;

    /* simple base address check */
    if ( !check_valid(drv) || !weights )
    {
        return ( -EINVAL );
    }

    ret = READ_REGS(RPP_RPP_HIST_WEIGHT_00TO30_REG, regs, NR_DX_REGS);
    if (!ret) {
        UNPACK_REGS(
                RPP_HIST_WEIGHT_NUM,
                RPP_HIST_HIST_WEIGHT_10_SHIFT,
                RPP_HIST_HIST_WEIGHT_00_MASK,
                regs,
                weights
                );
    }

    return ret;
}

/******************************************************************************
 * rpp_hist_hist
 *****************************************************************************/
int rpp_hist_hist(rpp_hist_drv_t * drv, rpp_hist_bins_t bins)
{
    int ret;

    /* simple base address check */
    if ( !check_valid(drv) || !bins )
    {
        return ( -EINVAL );
    }

    ret = READ_REGS(RPP_RPP_HIST_BIN__0_REG, bins, RPP_HIST_BIN_NUM);

    if (!ret)
    {
        unsigned i;
        for (i = 0; i < RPP_HIST_BIN_NUM; ++i)
        {
            bins[i] = REG_GET_SLICE(bins[i], RPP_HIST_HIST_BIN);
        }
    }

    return ret;
}

