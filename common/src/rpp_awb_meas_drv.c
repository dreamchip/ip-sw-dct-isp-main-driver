/******************************************************************************
 *
 * Copyright 2020 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_awb_meas_drv.c
 *
 * @brief   Implementation of auto white balance measurement driver
 *
 *****************************************************************************/
#include <rpp_awb_meas_drv.h>

// make sure that the base is 0 in the auto-generated
// header-file, before including it
#define RPP_AWB_MEAS_24_BASE 0
#include <rpp_awb_meas_24_regs_addr_map.h>
#include <rpp_awb_meas_24_regs_mask.h>

/******************************************************************************
 * local definitions
 *****************************************************************************/

static inline int check_valid(const rpp_awb_meas_drv_t * drv)
{
    return (drv && drv->dev && drv->colorbits);
}


/******************************************************************************
 * rpp_awb_meas_init
 *****************************************************************************/
int rpp_awb_meas_init(rpp_device * dev, uint32_t base, rpp_awb_meas_drv_t * drv)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !dev || !drv )
    {
        return ( -EINVAL );
    }
    drv->dev = dev;
    drv->base = base;
    drv->version = 0;
    drv->colorbits = 0;

    /* read and interpret the version register. */
    ret = READL(RPP_RPP_AWB_MEAS_VERSION_REG, &v);
    if (!ret)
    {
        drv->version = REG_GET_SLICE(v, RPP_AWB_MEAS_AWB_MEAS_VERSION);
        switch(drv->version)
        {
            case 0x01:
                drv->colorbits = 12;
                break;
            case 0x02:
                drv->colorbits = 20;
                break;
            case 0x03:
                drv->colorbits = 24;
                break;
            default:
                ret = -ENODEV;
        }
    }

    return ret;
}

/******************************************************************************
 * rpp_awb_meas_disable
 *****************************************************************************/
int rpp_awb_meas_disable(rpp_awb_meas_drv_t * drv)
{
    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    return WRITEL(RPP_RPP_AWB_MEAS_PROP_REG, RPP_AWB_MEAS_MODE_DISABLE);
}

/******************************************************************************
 * rpp_awb_meas_set_mode
 *****************************************************************************/
int rpp_awb_meas_set_mode(rpp_awb_meas_drv_t * drv, const unsigned mode)
{
    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    /* Range check. Make sure to only allow valid operation modes. */
    switch(mode) {
        case RPP_AWB_MEAS_MODE_DISABLE:
        case RPP_AWB_MEAS_MODE_RGB:
        case RPP_AWB_MEAS_MODE_YUV:
        case RPP_AWB_MEAS_MODE_YUV_YMAX:
            break;
        default:
            return ( -ERANGE );
    }

    /* The mode can directly be written to the register, it contains
     * valid data only. */
    return WRITEL(RPP_RPP_AWB_MEAS_PROP_REG, mode);
}

/******************************************************************************
 * rpp_awb_meas_mode
 *****************************************************************************/
int rpp_awb_meas_mode(rpp_awb_meas_drv_t * drv, unsigned * const mode)
{
    /* simple sanity checks */
    if ( !check_valid(drv) || !mode )
    {
        return ( -EINVAL );
    }

    /* read the register value directly into the user-supplied pointer. */
    return READL(RPP_RPP_AWB_MEAS_PROP_REG, mode);
}

/******************************************************************************
 * rpp_awb_meas_set_profile
 *****************************************************************************/
int rpp_awb_meas_set_profile(rpp_awb_meas_drv_t * drv, const rpp_ccor_profile_t * const p)
{
    int ret;

    /* simple sanity checks. */
    if ( !check_valid(drv) || !p )
    {
        return -EINVAL;
    }

    /* ask the CCOR driver to check the profile */
    ret = rpp_ccor_check_profile(drv->colorbits, p);

    /* if successful, ask the CCOR driver to program the profile */
    if (!ret)
    {
        ret = rpp_ccor_write_profile(
                drv->dev,
                drv->base,
                RPP_RPP_AWB_MEAS_CCOR_COEFF__0_REG,
                drv->colorbits,
                p
                );
    }

    return ret;
}

/******************************************************************************
 * rpp_awb_meas_csm
 *****************************************************************************/
int rpp_awb_meas_profile(rpp_awb_meas_drv_t * drv, rpp_ccor_profile_t * const p)
{
    /* simple sanity checks */
    if ( !check_valid(drv) || !p )
    {
        return -EINVAL;
    }

    /* Ask the CCOR driver to read the profile from the unit's registers. */
    return rpp_ccor_read_profile(
            drv->dev,
            drv->base,
            RPP_RPP_AWB_MEAS_CCOR_COEFF__0_REG,
            drv->colorbits,
            p
            );
}

/******************************************************************************
 * rpp_awb_meas_set_window
 *****************************************************************************/
int rpp_awb_meas_set_window(rpp_awb_meas_drv_t * drv, const rpp_window_t * win)
{
    uint32_t regs[4];

    /* simple sanity checks */
    if ( !check_valid(drv) || !win )
    {
        return ( -EINVAL );
    }

    /* range check */
    if (
            REG_RANGE_CHECK(win->hoff  , RPP_AWB_MEAS_AWB_H_OFFS)  ||
            REG_RANGE_CHECK(win->voff  , RPP_AWB_MEAS_AWB_V_OFFS)  ||
            REG_RANGE_CHECK(win->width , RPP_AWB_MEAS_AWB_H_SIZE)  ||
            REG_RANGE_CHECK(win->height, RPP_AWB_MEAS_AWB_V_SIZE)
       )
    {
        return ( -ERANGE );
    }

    /* The window registers are adjacent in memory and can be
     * programmed using efficient multiple-register-writes. */
    regs[0] = win->hoff;
    regs[1] = win->voff;
    regs[2] = win->width;
    regs[3] = win->height;
    return WRITE_REGS(RPP_RPP_AWB_MEAS_H_OFFS_REG, regs, 4);
}

/******************************************************************************
 * rpp_awb_meas_window
 *****************************************************************************/
int rpp_awb_meas_window(rpp_awb_meas_drv_t * drv, rpp_window_t * const win)
{
    uint32_t regs[4];
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !win )
    {
        return ( -EINVAL );
    }

    /* Read the window geometry using efficient multiple-register-reads,
     * the registers are adjacent in register space. */
    ret = READ_REGS(RPP_RPP_AWB_MEAS_H_OFFS_REG, regs, 4);

    /* copy and mask the values if successful. */
    if (!ret)
    {
        win->hoff   = REG_GET_SLICE( regs[0], RPP_AWB_MEAS_AWB_H_OFFS );
        win->voff   = REG_GET_SLICE( regs[1], RPP_AWB_MEAS_AWB_V_OFFS );
        win->width  = REG_GET_SLICE( regs[2], RPP_AWB_MEAS_AWB_H_SIZE );
        win->height = REG_GET_SLICE( regs[3], RPP_AWB_MEAS_AWB_V_SIZE );
    }

    return ret;
}

/******************************************************************************
 * rpp_awb_meas_set_condition
 *****************************************************************************/
int rpp_awb_meas_set_condition(rpp_awb_meas_drv_t * drv, const rpp_awb_meas_cond_t * condition)
{
    int ret;
    uint32_t cmask;
    const uint32_t * vals;
    unsigned i;
    unsigned n = 6;         /* default for YUV check */
    unsigned ignore = 6;    /* default for YUV check */
    uint32_t regs[6] = {0, };

    /* simple sanity checks */
    if ( !check_valid(drv) || !condition )
    {
        return ( -EINVAL );
    }

    /* check whether the number of frames is correct. */
    if ( REG_RANGE_CHECK(condition->frames, RPP_AWB_MEAS_AWB_FRAMES) )
    {
        return -ERANGE;
    }

    /* get the correct bitmask for unsigned color values. */
    cmask = UNSIGNED_MAX(drv->colorbits);
    
    /* Get a pointer to the values and the number of values to
     * check/program. The latter depends on whether RGB or YUV
     * measurement is wanted. */
    vals = &(condition->yuv.ref_cb);

    /* if RGB mode is active, change defaults for
     * how many fields to check and which field to ignore.
     */
    if (condition->rgb_valid) {
        n = 4;          // just count 4 registers
        ignore = 2;     // ignore the __unused value in the union
    }

    /* do a range check on the required number of values. */
    for (i = 0; i < n; ++i)
    {
        if ((i != ignore) && (vals[i] > cmask))
        {
            return -ERANGE;
        }
        /* copy the value over to the register cache for writing */
        regs[i] = vals[i];
    }

    /* program the number-of-frames register. */
    ret = WRITEL(RPP_RPP_AWB_MEAS_FRAMES_REG, condition->frames);

    /* if successful, program the numerical limits. */
    if (!ret)
    {
        ret = WRITE_REGS(RPP_RPP_AWB_MEAS_REF_CB__MAX_B_REG, regs, n);
    }

    return ret;
}

/******************************************************************************
 * rpp_awb_meas_set_condition
 *****************************************************************************/
int rpp_awb_meas_condition(rpp_awb_meas_drv_t * drv, rpp_awb_meas_cond_t * const condition)
{
    int ret;
    uint32_t v;

    /* simple base address check */
    if ( !check_valid(drv) || !condition )
    {
        return ( -EINVAL );
    }

    /* get rgb mode from hardware */
    ret = READL(RPP_RPP_AWB_MEAS_PROP_REG, &v);

    if (!ret) {
        condition->rgb_valid = REG_GET_SLICE( v, RPP_AWB_MEAS_AWB_MEAS_MODE );

        /* read number of frames from the unit. */
        ret = READL(RPP_RPP_AWB_MEAS_FRAMES_REG, &v);
    }
    
    if (!ret) {
        uint32_t * vals;

        condition->frames = REG_GET_SLICE( v, RPP_AWB_MEAS_AWB_FRAMES );

        /* Read limit configuration from hardware. Always read the
         * full set of 6 limits: higher-level software is free to
         * ignore unwanted values. */
        vals = &(condition->yuv.ref_cb);
        ret = READ_REGS(RPP_RPP_AWB_MEAS_REF_CB__MAX_B_REG, vals, 6);
    }

    return ret;
}

/******************************************************************************
 * rpp_awb_meas
 *****************************************************************************/
int rpp_awb_meas(rpp_awb_meas_drv_t * drv, rpp_awb_meas_t * const meas)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !meas )
    {
        return ( -EINVAL );
    }

    /* get rgb mode from hardware */
    ret = READL(RPP_RPP_AWB_MEAS_PROP_REG, &v);

    if (!ret) {
        meas->rgb_valid = REG_GET_SLICE( v, RPP_AWB_MEAS_AWB_MEAS_MODE );

        /* read measured data directly into the structure. */
        ret = READ_REGS(RPP_RPP_AWB_MEAS_WHITE_CNT_REG, &(meas->white_count), 4);
    }

    /* if successful, mask retrieved values. */
    if (!ret) {
        uint32_t cmask = UNSIGNED_MAX(drv->colorbits);

        meas->white_count   &= RPP_AWB_MEAS_AWB_WHITE_CNT_MASK;

        /* If no pixel matches the measurement conditions, white_count
         * register will contain zero and the average calculation
         * result in a division by zero: sum=0 / count=0. That makes
         * the result registers have invalid data. The driver can fix
         * that, though, and return zero, which can be expected since
         * the sum is zero.
         * Implement this by setting the register mask to "0" if
         * the count is zero. Subsequent masking will clear the values.*/
        if (!meas->white_count)
            cmask = 0;

        /* Due to member aliasing via union's, only YUV values need
         * to be color-masked. This affects RGB values automatically. */
        meas->yuv.mean_y    &= cmask;
        meas->yuv.mean_cb   &= cmask;
        meas->yuv.mean_cr   &= cmask;
    }

    return ret;
}
