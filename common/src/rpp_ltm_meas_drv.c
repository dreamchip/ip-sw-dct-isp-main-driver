/******************************************************************************
 *
 * Copyright 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_ltm_meas_drv.c
 *
 * @brief   Implementation of Local Tone Mapping Measurement unit driver
 *
 *****************************************************************************/
#include <rpp_ltm_meas_drv.h>

// make sure that the base is 0 in the auto-generated
// header-file, before including it
#define RPP_LTM_MEAS_BASE 0
#include <rpp_ltm_meas_regs_addr_map.h>
#include <rpp_ltm_meas_regs_mask.h>

/******************************************************************************
 * local definitions
 *****************************************************************************/


#define HIST_BITS_LO    7u                                      /* bit width of low-resolution histogram bins                       */
#define HIST_BITS_HI    6u                                      /* bit width of high-resolution histogram bins                      */
#define HIST_HR_BINS    2u                                      /* number of low-resolution bins being replaced by high-resolution  */

#define HIST_BINS_LO    ((1u << HIST_BITS_LO) - HIST_HR_BINS)   /* number of low-resolution bins at higher color values             */
#define HIST_BINS_HI    ((1u << HIST_BITS_HI) * HIST_HR_BINS)   /* number of high-resolution bins at lowest color values            */
#define HIST_BINS_TOTAL (HIST_BINS_LO + HIST_BINS_HI)           /* total number of bins                                             */

static inline int check_valid(const rpp_ltm_meas_drv_t * drv)
{
    return (drv && drv->dev && drv->colorbits);
}


/******************************************************************************
 * rpp_ltm_meas_init
 *****************************************************************************/
int rpp_ltm_meas_init(rpp_device * dev, uint32_t base, rpp_ltm_meas_drv_t * drv)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !dev || !drv )
    {
        return ( -EINVAL );
    }
    drv->dev = dev;
    drv->base = base;
    drv->version = 0;
    drv->colorbits = 0;

    /* read and interpret the version register. */
    ret = READL(RPP_RPP_LTM_MEAS_VERSION_REG, &v);
    if (!ret)
    {
        drv->version = REG_GET_SLICE(v, RPP_LTM_MEAS_LTM_MEAS_VERSION);
        switch(drv->version)
        {
            case 0x01:
                /* initial 24bit version */
                drv->colorbits = 24;
                break;
            default:
                /* other versions are as-yet unknown */
                ret = -ENODEV;
        }
    }

    return ret;
}

/******************************************************************************
 * rpp_ltm_meas_enable
 *****************************************************************************/
int rpp_ltm_meas_enable(rpp_ltm_meas_drv_t * drv, unsigned on)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    /* read and update the control register. */
    ret = READL(RPP_RPP_LTM_MEAS_CTRL_REG, &v);
    if (!ret) {
        REG_SET_SLICE(v, RPP_LTM_MEAS_LTM_MEAS_ENABLE, !!on);
        ret = WRITEL(RPP_RPP_LTM_MEAS_CTRL_REG, v);
    }

    return ret;
}

/******************************************************************************
 * rpp_ltm_meas_enabled
 *****************************************************************************/
int rpp_ltm_meas_enabled(rpp_ltm_meas_drv_t * drv, unsigned * const on)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !on )
    {
        return ( -EINVAL );
    }

    /* read and parse the control register. */
    ret = READL(RPP_RPP_LTM_MEAS_CTRL_REG, &v);
    if (!ret) {
        *on = REG_GET_SLICE(v, RPP_LTM_MEAS_LTM_MEAS_ENABLE);
    }

    return ret;
}


/******************************************************************************
 * rpp_ltm_meas_set_meas_window
 *****************************************************************************/
int rpp_ltm_meas_set_meas_window(rpp_ltm_meas_drv_t * drv, rpp_window_t const * const win)
{
    uint32_t regs[4];

    /* simple sanity checks. */
    if ( !check_valid(drv) || !win )
    {
        return -EINVAL;
    }

    /* perform range-checking on the argument */
    if (
            REG_RANGE_CHECK(win->hoff,  RPP_LTM_MEAS_LTM_MEAS_H_OFFSET) ||
            REG_RANGE_CHECK(win->voff,  RPP_LTM_MEAS_LTM_MEAS_V_OFFSET) ||
            REG_RANGE_CHECK(win->width, RPP_LTM_MEAS_LTM_MEAS_H_SIZE)   ||
            REG_RANGE_CHECK(win->height, RPP_LTM_MEAS_LTM_MEAS_V_SIZE)
       )
    {
        return -ERANGE;
    }

    /* prepare the register array and program the coordinates. */
    regs[0] = win->hoff;
    regs[1] = win->voff;
    regs[2] = win->width;
    regs[3] = win->height;
    return WRITE_REGS(RPP_RPP_LTM_MEAS_H_OFFS_REG, regs, 4);
}

/******************************************************************************
 * rpp_ltm_meas_meas_window
 *****************************************************************************/
int rpp_ltm_meas_meas_window(rpp_ltm_meas_drv_t * drv, rpp_window_t * const win)
{
    uint32_t regs[4];
    int ret;

    /* simple sanity checks. */
    if ( !check_valid(drv) || !win )
    {
        return -EINVAL;
    }

    /* read and parse the window registers. */
    ret = READ_REGS(RPP_RPP_LTM_MEAS_H_OFFS_REG, regs, 4);
    if (!ret) {
        win->hoff   = REG_GET_SLICE(regs[0], RPP_LTM_MEAS_LTM_MEAS_H_OFFSET);
        win->voff   = REG_GET_SLICE(regs[1], RPP_LTM_MEAS_LTM_MEAS_V_OFFSET);
        win->width  = REG_GET_SLICE(regs[2], RPP_LTM_MEAS_LTM_MEAS_H_SIZE);
        win->height = REG_GET_SLICE(regs[3], RPP_LTM_MEAS_LTM_MEAS_V_SIZE);
    }

    return ret;
}


/******************************************************************************
 * rpp_ltm_meas_set_weights
 *****************************************************************************/
int rpp_ltm_meas_set_weights(rpp_ltm_meas_drv_t * drv, uint16_t red,
        uint16_t green, uint16_t blue)
{
    uint32_t v =  0;
    uint32_t sum = red + green + blue;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return -EINVAL;
    }

    /* range-check parameters */
    if (
            REG_RANGE_CHECK(red,    RPP_LTM_MEAS_LTM_MEAS_R_WEIGHT) ||
            REG_RANGE_CHECK(green,  RPP_LTM_MEAS_LTM_MEAS_G_WEIGHT) ||
            REG_RANGE_CHECK(blue,   RPP_LTM_MEAS_LTM_MEAS_B_WEIGHT) ||
            (sum != RPP_LTM_MEAS_LTM_MEAS_R_WEIGHT_MASK)                /* the weights must sum up to 1023 */
       )
    {
        return -ERANGE;
    }

    /* compose the register value */
    REG_SET_SLICE(v, RPP_LTM_MEAS_LTM_MEAS_R_WEIGHT, red);
    REG_SET_SLICE(v, RPP_LTM_MEAS_LTM_MEAS_G_WEIGHT, green);
    REG_SET_SLICE(v, RPP_LTM_MEAS_LTM_MEAS_B_WEIGHT, blue);

    return WRITEL(RPP_RPP_LTM_MEAS_RGB_WEIGHTS_REG, v);
}

/******************************************************************************
 * rpp_ltm_meas_weights
 *****************************************************************************/
int rpp_ltm_meas_weights(rpp_ltm_meas_drv_t * drv, uint16_t * const red,
        uint16_t * const green, uint16_t * const blue)
{
    uint32_t v =  0;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !red || !green || !blue )
    {
        return -EINVAL;
    }

    /* read and parse the configuration register. */
    ret = READL(RPP_RPP_LTM_MEAS_RGB_WEIGHTS_REG, &v);
    if (!ret) {
        *red =      REG_GET_SLICE(v, RPP_LTM_MEAS_LTM_MEAS_R_WEIGHT);
        *green =    REG_GET_SLICE(v, RPP_LTM_MEAS_LTM_MEAS_G_WEIGHT);
        *blue =     REG_GET_SLICE(v, RPP_LTM_MEAS_LTM_MEAS_B_WEIGHT);
    }

    return ret;
}

/******************************************************************************
 * rpp_ltm_meas_set_prc_thresholds
 *****************************************************************************/
int rpp_ltm_meas_set_prc_thresholds(rpp_ltm_meas_drv_t * drv, unsigned num,
        uint32_t const * const values)
{
    unsigned i;

    /* simple sanity checks */
    if ( !check_valid(drv) || (num && !values) )
    {
        return -EINVAL;
    }

    /* simple case: check for nothing to do */
    if (!num)
    {
        return 0;
    }

    /* range-check the arguments. */
    if (num > RPP_LTM_MEAS_NUM_PRC)
    {
        return -ERANGE;
    }

    for (i = 0; i < num; ++i)
    {
        if ( REG_RANGE_CHECK(values[i], RPP_LTM_MEAS_LTM_MEAS_PRC_THRESH) )
        {
            return -ERANGE;
        }
    }

    /* the values are in correct format, just write them out */
    return WRITE_REGS(RPP_RPP_LTM_MEAS_PRC_THRESH__0_REG, values, num);
}

/******************************************************************************
 * rpp_ltm_meas_prc_thresholds
 *****************************************************************************/
int rpp_ltm_meas_prc_thresholds(rpp_ltm_meas_drv_t * drv, unsigned num,
        uint32_t * const values)
{
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || (num && !values) )
    {
        return -EINVAL;
    }

    /* simple case: check for nothing to do */
    if (!num)
    {
        return 0;
    }

    /* range-check the arguments. */
    if (num > RPP_LTM_MEAS_NUM_PRC)
    {
        return -ERANGE;
    }

    /* read the registers into the provided array. */
    ret = READ_REGS(RPP_RPP_LTM_MEAS_PRC_THRESH__0_REG, values, num);

    /* if successful, mask the relevant bits out */
    if (!ret) {
        unsigned i;
        for (i = 0; i < num; ++i) {
            values[i] = REG_GET_SLICE(values[i], RPP_LTM_MEAS_LTM_MEAS_PRC_THRESH);
        }
    }

    return ret;
}

/******************************************************************************
 * rpp_ltm_meas_get_prc
 *****************************************************************************/
/* Put this into a separate function that can be called by
 * the unit tests. Is not part of the driver API, though,
 * and is explicitly declared in the unit test via
 * "extern int ...();"
 */
int rpp_ltm_meas_decode_prc(rpp_ltm_meas_drv_t * drv, unsigned num,
        uint32_t const * const bins, uint32_t * const values)
{
        unsigned i;
        uint32_t prcMax = UNSIGNED_MAX(drv->colorbits);

        if (num > RPP_LTM_MEAS_NUM_PRC)
        {
            return -ERANGE;
        }

        for (i = 0; i < num; ++i) {

            uint32_t bitwidth;
            uint32_t bin = REG_GET_SLICE(bins[i], RPP_LTM_MEAS_LTM_MEAS_PRC);
            uint32_t prcVal;

            /*
             * Check whether the bin number from the register is out-of-range.
             * if so, this indicates an invalid threshold and the output value
             * is set to -1.
             */
            if (bin >= HIST_BINS_TOTAL) {
                values[i] = 0xFFFFFFFF;
                continue;
            }

            /* get the histogram bin number bit width for reconstructing the
             * luminance values. */
            bitwidth = HIST_BITS_LO;

            /* check if the bin number is one of the high-resolution bins at
             * the lower end of the high-resolution bins on the bottom of
             * the histogram. */
            if (bin < HIST_BINS_HI) {
                /* if so, increment the histogram bin's bit
                 * width by the additional bit width of the
                 * high-resolution bins. */
                bitwidth += HIST_BITS_HI;
            } else {
                /* otherwise determine the low-res bin number by subtracting
                 * the hi-resolution bin number and adding two for the replaced
                 * low-res bins */
                bin = bin - HIST_BINS_HI + HIST_HR_BINS;
            }

            /* shift the histogram number by the appropriate amount of bits to
             * reconstruct luminance. */
            prcVal = bin << (drv->colorbits - bitwidth);

            /* add half a bin size to minimize the average error. */
            prcVal += 1 << (drv->colorbits - bitwidth - 1);

            /* clip the value to valid color range */
            prcVal = (prcVal < prcMax) ? prcVal : prcMax;

            /* and store the output value */
            values[i] = prcVal;
    }

    return 0;
}

int rpp_ltm_meas_get_prc(rpp_ltm_meas_drv_t * drv, unsigned num,
        uint32_t * const values)
{
    uint32_t regs[RPP_LTM_MEAS_NUM_PRC];
    int ret;

    /* simple sanity checks. */
    if ( !check_valid(drv) || (num && !values) )
    {
        return -EINVAL;
    }

    /* simple case: check for nothing to do. */
    if (!num)
    {
        return 0;
    }

    /* range-check the argument */
    if (num > RPP_LTM_MEAS_NUM_PRC)
    {
        return -ERANGE;
    }

    /* read the registers into a temporary array */
    ret = READ_REGS(RPP_RPP_LTM_MEAS_PRC__0_REG, regs, num);

    /* if successful, parse the register values and calculate the
     * percentiles from the bin numbers. */
    if (!ret)
    {
        ret = rpp_ltm_meas_decode_prc(drv, num, regs, values);
    }

    return ret;
}


/******************************************************************************
 * rpp_ltm_meas_get_stats
 *****************************************************************************/
int rpp_ltm_meas_get_stats(rpp_ltm_meas_drv_t * drv, uint32_t * const lmin,
        uint32_t * const lmax, uint32_t * const lgmean)
{
    uint32_t regs[3];
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !lmin || !lmax || !lgmean )
    {
        return -EINVAL;
    }

    /* read and parse the result registers. */
    ret = READ_REGS(RPP_RPP_LTM_MEAS_L_MIN_REG, regs, 3);
    if (!ret) {
        *lmin   = REG_GET_SLICE(regs[0], RPP_LTM_MEAS_LMIN);
        *lmax   = REG_GET_SLICE(regs[1], RPP_LTM_MEAS_LMAX);
        *lgmean = REG_GET_SLICE(regs[2], RPP_LTM_MEAS_LGMEAN);
    }

    return ret;
}


