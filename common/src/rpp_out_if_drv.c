/******************************************************************************
 *
 * Copyright 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_out_if_drv.c
 *
 * @brief   Implementation of Output Interface unit driver
 *
 *****************************************************************************/
#include <rpp_out_if_drv.h>

// make sure that the base is 0 in the auto-generated
// header-file, before including it
#define RPP_OUT_IF_BASE 0
#include <rpp_out_if_regs_addr_map.h>
#include <rpp_out_if_regs_mask.h>

/******************************************************************************
 * local definitions
 *****************************************************************************/

static inline int check_valid(const rpp_out_if_drv_t * drv)
{
    return (drv && drv->dev && drv->version);
}

/******************************************************************************
 * rpp_out_if_init
 *****************************************************************************/
int rpp_out_if_init(rpp_device * dev, uint32_t base, rpp_out_if_drv_t * drv)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !dev || !base || !drv )
    {
        return ( -EINVAL );
    }
    drv->dev = dev;
    drv->base = base;
    drv->version = 0;

    /* read and parse the version register. */
    ret = READL(RPP_RPP_OUT_IF_VERSION_REG, &v);
    if (!ret)
    {
        drv->version = REG_GET_SLICE(v, RPP_OUT_IF_OUT_IF_VERSION);
        if (!drv->version)
        {
            ret = -ENODEV;
        }
    }

    return ret;
}

/******************************************************************************
 * rpp_out_if_set_active
 *****************************************************************************/
int rpp_out_if_set_active(rpp_out_if_drv_t * drv, const unsigned on)
{
    uint32_t v = 0;
    uint32_t reg;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return -EINVAL;
    }

    /* get the register to actually write to. */
    reg = (on) ? RPP_RPP_OUT_IF_ON_REG : RPP_RPP_OUT_IF_OFF_REG;

    /* prepare the value to write. */
    REG_SET_SLICE(v, RPP_OUT_IF_RPP_ON, 1);

    return WRITEL(reg, v);
}


/******************************************************************************
 * rpp_out_if_next_active
 *****************************************************************************/
int rpp_out_if_next_active(rpp_out_if_drv_t * drv, unsigned * const on)
{
    uint32_t v;
    int ret;

    /* simple sanity checks. */
    if ( !check_valid(drv) || !on )
    {
        return -EINVAL;
    }

    ret = READL(RPP_RPP_FLAGS_SHD_REG, &v);

    if (!ret) {
        /* check whether the unit is enabled and not to be turned off */
        uint8_t be_on   = REG_GET_SLICE(v, RPP_OUT_IF_RPP_ON_STORED);
        uint8_t be_off  = REG_GET_SLICE(v, RPP_OUT_IF_RPP_OFF_STORED);
        /* the OFF_STORED flag indicates that the unit will be turned off
         * at next frame end (self-clearing), whereas ON_STORED tells that
         * the unit will be or is already enabled.
         * Therefore, if both flags are cleared, the unit is already off
         * and not to be enabled. */
        *on = (be_on && !be_off) ? 1 : 0;
    }

    return ret;
}

/******************************************************************************
 * rpp_out_if_active
 *****************************************************************************/
int rpp_out_if_active(rpp_out_if_drv_t * drv, unsigned * const on)
{
    uint32_t v;
    int ret;

    /* simple sanity checks. */
    if ( !check_valid(drv) || !on )
    {
        return -EINVAL;
    }

    /* read the SHADOW register and check whether the unit is
     * currently active. */
    ret = READL(RPP_RPP_FLAGS_SHD_REG, &v);
    if (!ret) {
        *on = REG_GET_SLICE(v, RPP_OUT_IF_RPP_ON_SHD);
    }

    return ret;
}

/******************************************************************************
 * rpp_out_if_set_frames
 *****************************************************************************/
int rpp_out_if_set_frames(rpp_out_if_drv_t * drv, const unsigned num)
{
    uint32_t v;
    int ret;

    /* simple sanity check. */
    if ( !check_valid(drv) )
    {
        return -EINVAL;
    }

    /* range-check the argument */
    if ( REG_RANGE_CHECK(num, RPP_OUT_IF_NR_FRAMES) )
    {
        return -ERANGE;
    }

    /* read the frame count register and update it. */
    ret = READL(RPP_RPP_OUT_IF_NR_FRAMES_REG, &v);
    if (!ret) {
        REG_SET_SLICE(v, RPP_OUT_IF_NR_FRAMES, num);
        ret = WRITEL(RPP_RPP_OUT_IF_NR_FRAMES_REG, v);
    }

    return ret;
}

/******************************************************************************
 * rpp_out_if_frames
 *****************************************************************************/
int rpp_out_if_frames(rpp_out_if_drv_t * drv, unsigned * const num)
{
    uint32_t v;
    int ret;

    /* simple sanity checks. */
    if ( !check_valid(drv) || !num )
    {
        return -EINVAL;
    }

    /* read and parse the frame count register */
    ret = READL(RPP_RPP_OUT_IF_NR_FRAMES_REG, &v);
    if (!ret) {
        *num = REG_GET_SLICE(v, RPP_OUT_IF_NR_FRAMES);
    }

    return ret;
}

/******************************************************************************
 * rpp_out_if_remaining_frames
 *****************************************************************************/
int rpp_out_if_remaining_frames(rpp_out_if_drv_t * drv, unsigned * const num)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !num )
    {
        return -EINVAL;
    }

    /* read and parse the remaining frame count register. */
    ret = READL(RPP_RPP_OUT_IF_NR_FRAMES_CNT_REG, &v);
    if (!ret) {
        *num = REG_GET_SLICE(v, RPP_OUT_IF_NR_FRAMES_CNT);
    }

    return ret;
}

/******************************************************************************
 * rpp_out_if_set_data_mode
 *****************************************************************************/
int rpp_out_if_set_data_mode(rpp_out_if_drv_t * drv, const unsigned data)
{
    uint32_t v;
    int ret;

    /* simple sanity check. */
    if ( !check_valid(drv) )
    {
        return -EINVAL;
    }

    /* read the corresponding register and modify it. */
    ret = READL(RPP_RPP_OUT_IF_MODE_REG, &v);
    if (!ret) {
        REG_SET_SLICE(v, RPP_OUT_IF_DATA_MODE, !!data);
        ret = WRITEL(RPP_RPP_OUT_IF_MODE_REG, v);
    }
    return ret;
}

/******************************************************************************
 * rpp_out_if_data_mode
 *****************************************************************************/
int rpp_out_if_data_mode(rpp_out_if_drv_t * drv, unsigned * const data)
{
    uint32_t v;
    int ret;

    /* simple sanity checks. */
    if ( !check_valid(drv) || !data )
    {
        return -EINVAL;
    }

    /* read and parse the data mode register. */
    ret = READL(RPP_RPP_OUT_IF_MODE_REG, &v);
    if (!ret) {
        *data = REG_GET_SLICE(v, RPP_OUT_IF_DATA_MODE);
    }
    return ret;
}


