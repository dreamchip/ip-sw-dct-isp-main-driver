/******************************************************************************
 *
 * Copyright 2019 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_dpcc_drv.c
 *
 * @brief   Implementation of Defect Pixel Cluster Correction unit driver
 *
 *****************************************************************************/
#include <rpp_dpcc_drv.h>

// make sure that the base is 0 in the auto-generated
// header-file, before including it
#define RPP_DPCC_BASE 0
#include <rpp_dpcc_regs_addr_map.h>
#include <rpp_dpcc_regs_mask.h>

/******************************************************************************
 * local definitions
 *****************************************************************************/

static const uint32_t method_set_addr[RPP_DPCC_METHOD_SET_NUM] =
{
    RPP_RPP_DPCC_METHODS_SET_1_REG,
    RPP_RPP_DPCC_METHODS_SET_2_REG,
    RPP_RPP_DPCC_METHODS_SET_3_REG
};

static const uint32_t check_base[RPP_DPCC_METHOD_SET_NUM] =
{
    RPP_RPP_DPCC_LINE_THRESH_1_REG,
    RPP_RPP_DPCC_LINE_THRESH_2_REG,
    RPP_RPP_DPCC_LINE_THRESH_3_REG
};

static inline int check_valid(const rpp_dpcc_drv_t * drv)
{
    return (drv && drv->dev && drv->colorbits);
}


/******************************************************************************
 * rpp_dpcc_init
 *****************************************************************************/
int rpp_dpcc_init(rpp_device * dev, uint32_t base, rpp_dpcc_drv_t * drv)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !dev || !drv )
    {
        return ( -EINVAL );
    }
    drv->dev = dev;
    drv->base = base;
    drv->version = 0;
    drv->colorbits = 0;

    /* read and parse the version register. */
    ret = READL(RPP_RPP_DPCC_VERSION_REG, &v);
    if (!ret) {
        drv->version = REG_GET_SLICE(v, RPP_DPCC_DPCC_VERSION);
        switch(drv->version) {
            case 0x02:
            case 0x04:
            case 0x06:
                /* current 12bit version(s) of the core */
                drv->colorbits = 12;
                break;
            case 0x03:
            case 0x05:
            case 0x07:
                /* current 24bit version(s) of the core with
                 * extended Line Check Threshold of 16bit width */
                drv->colorbits = 24;
                break;
            default:
                /* all other versions are unhandled. */
                ret = -ENODEV ;
        }
    }

    return ret;
}

/******************************************************************************
 * rpp_dpcc_enable
 *****************************************************************************/
int rpp_dpcc_enable(rpp_dpcc_drv_t * drv, const unsigned on)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    ret = READL(RPP_RPP_DPCC_MODE_REG, &v);
    if (!ret)
    {
        REG_SET_SLICE(v, RPP_DPCC_DPCC_ENABLE,      !!on);
        REG_SET_SLICE(v, RPP_DPCC_STAGE1_ENABLE,    !!on);
        ret = WRITEL(RPP_RPP_DPCC_MODE_REG, v);
    }

    return ret;
}

/******************************************************************************
 * rpp_dpcc_enabled
 *****************************************************************************/
int rpp_dpcc_enabled(rpp_dpcc_drv_t * drv, unsigned * const on)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !on )
    {
        return ( -EINVAL );
    }

    ret = READL(RPP_RPP_DPCC_MODE_REG, &v);
    if (!ret)
    {
        *on = REG_GET_SLICE(v, RPP_DPCC_DPCC_ENABLE);
    }

    return ret;
}


static int rpp_dpcc_check_method_set(unsigned lc_thr_mask, const rpp_dpcc_method_t * const set)
{
    uint32_t on, p1, p2;

    /* range-check peak gradient parameters: factor */
    RPP_DPCC_CHECK_GET_PG(*set, on, p1);
    if (REG_RANGE_CHECK(p1, RPP_DPCC_PG_FAC_1_G))
    {
        return -ERANGE;
    }

    /* range-check line check parameters: mad_factor and threshold    */
    RPP_DPCC_CHECK_GET_LC(*set, on, p1, p2);
    if (
            REG_RANGE_CHECK(p1, RPP_DPCC_LINE_MAD_FAC_1_G) ||
            (p2 > lc_thr_mask)
       )
    {
        return -ERANGE;
    }

    /* range-check rank order parameters: limit */
    RPP_DPCC_CHECK_GET_RO(*set, on, p1);
    if (REG_RANGE_CHECK(p1, RPP_DPCC_RO_LIM_1_G))
    {
        return -ERANGE;
    }

    /* range-check rank neighbour difference parameters: threshold, offset */
    RPP_DPCC_CHECK_GET_RND(*set, on, p1, p2);
    if (
            REG_RANGE_CHECK(p1, RPP_DPCC_RND_THR_1_G)   ||
            REG_RANGE_CHECK(p2, RPP_DPCC_RND_OFFS_1_G)  ||
            (p2 == 0)           /* the RND offset is not allowed to be zero */
       )
    {
        return -ERANGE;
    }

    /* range-check rank gradient parameters: factor */
    RPP_DPCC_CHECK_GET_RG(*set, on, p1);
    if (REG_RANGE_CHECK(p1, RPP_DPCC_RG_FAC_1_G))
    {
        return -ERANGE;
    }

    /* all successful. */
    return 0;
}

/******************************************************************************
 * rpp_dpcc_set_method
 *****************************************************************************/
int rpp_dpcc_set_method
(
    rpp_dpcc_drv_t *                drv,
    const unsigned                  method_id,
    const rpp_dpcc_method_t * const green,
    const rpp_dpcc_method_t * const rb
)
{
    uint32_t on, p1, p2;        /* method parameters                        */
    uint32_t ro_g, ro_rb;       /* rank order limit param                   */
    uint32_t rndo_g, rndo_rb;   /* rank neighbour difference offset param   */
    uint32_t flags;             /* enable flag register cache               */
    uint32_t parm[5] = {0, };   /* method parameter register cache          */
    uint32_t parm2[2];          /* RO limit and RND offset register cache   */
    uint32_t regoff;            /* method parameter register offset         */
    uint32_t lc_thr_bits;       /* line check threshold bits                */
    uint32_t lc_thr_mask;       /* line check threshold bitmask             */
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !green || !rb)
    {
        return ( -EINVAL );
    }

    /* range-check the arguments. */
    if (method_id >= RPP_DPCC_METHOD_SET_NUM)
    {
        return -ERANGE;
    }

    /* the threshold is 8bit wide for 12bit instances and 16bit wide for
     * 24bit instances of the DPCC unit. */
    lc_thr_bits = (drv->colorbits > 12) ? 16 : 8;
    lc_thr_mask = UNSIGNED_MAX(lc_thr_bits);

    /* perform range checks. */
    ret = rpp_dpcc_check_method_set(lc_thr_mask, green);
    if (!ret)
    {
        ret = rpp_dpcc_check_method_set(lc_thr_mask, rb);
    }

    /* read the RO limit and RND offset registers which contain
     * settings for all sets in a single register. */
    if (!ret)
    {
        ret = READ_REGS(RPP_RPP_DPCC_RO_LIMITS_REG, parm2, 2);
    }

    if (ret)
    {
        return ret;
    }

    /* initialize basic register caches. */
    flags = 0;
    regoff = check_base[method_id];

    /* configure line check parameters. */
    RPP_DPCC_CHECK_GET_LC(*green, on, p1, p2);
    REG_SET_SLICE(flags,    RPP_DPCC_LC_GREEN1_ENABLE,      !!on);
    REG_SET_SLICE(parm[1],  RPP_DPCC_LINE_MAD_FAC_1_G,      p1);
    parm[0] |= (p2 & lc_thr_mask);

    RPP_DPCC_CHECK_GET_LC(*rb, on, p1, p2);
    REG_SET_SLICE(flags,    RPP_DPCC_LC_RED_BLUE1_ENABLE,   !!on);
    REG_SET_SLICE(parm[1],  RPP_DPCC_LINE_MAD_FAC_1_RB,     p1);
    parm[0] |= (p2 & lc_thr_mask) << lc_thr_bits;

    /* configure peak gradient check parameters */
    RPP_DPCC_CHECK_GET_PG(*green, on, p1);
    REG_SET_SLICE(flags,    RPP_DPCC_PG_GREEN1_ENABLE,      !!on);
    REG_SET_SLICE(parm[2],  RPP_DPCC_PG_FAC_1_G,            p1);
    
    RPP_DPCC_CHECK_GET_PG(*rb, on, p1);
    REG_SET_SLICE(flags,    RPP_DPCC_PG_RED_BLUE1_ENABLE,   !!on);
    REG_SET_SLICE(parm[2],  RPP_DPCC_PG_FAC_1_RB,           p1);

    /* configure rank neighbour difference check parameters */
    RPP_DPCC_CHECK_GET_RND(*green, on, p1, rndo_g);
    REG_SET_SLICE(flags,    RPP_DPCC_RND_GREEN1_ENABLE,     !!on);
    REG_SET_SLICE(parm[3],  RPP_DPCC_RND_THR_1_G,           p1);

    RPP_DPCC_CHECK_GET_RND(*rb, on, p1, rndo_rb);
    REG_SET_SLICE(flags,    RPP_DPCC_RND_RED_BLUE1_ENABLE,  !!on);
    REG_SET_SLICE(parm[3],  RPP_DPCC_RND_THR_1_RB,          p1);

    /* configure rank gradient check parameters */
    RPP_DPCC_CHECK_GET_RG(*green, on, p1);
    REG_SET_SLICE(flags,    RPP_DPCC_RG_GREEN1_ENABLE,      !!on);
    REG_SET_SLICE(parm[4],  RPP_DPCC_RG_FAC_1_G,            p1);

    RPP_DPCC_CHECK_GET_RG(*rb, on, p1);
    REG_SET_SLICE(flags,    RPP_DPCC_RG_RED_BLUE1_ENABLE,   !!on);
    REG_SET_SLICE(parm[4],  RPP_DPCC_RG_FAC_1_RB,           p1);

    /* configure rank order check parameters. */
    RPP_DPCC_CHECK_GET_RO(*green, on, ro_g);
    REG_SET_SLICE(flags,    RPP_DPCC_RO_GREEN1_ENABLE,      !!on);

    RPP_DPCC_CHECK_GET_RO(*rb, on, ro_rb);
    REG_SET_SLICE(flags,    RPP_DPCC_RO_RED_BLUE1_ENABLE,   !!on);


    switch(method_id) {
        case RPP_DPCC_METHOD_SET_3:
            REG_SET_SLICE(parm2[0], RPP_DPCC_RO_LIM_3_G,    ro_g);
            REG_SET_SLICE(parm2[0], RPP_DPCC_RO_LIM_3_RB,   ro_rb);
            REG_SET_SLICE(parm2[1], RPP_DPCC_RND_OFFS_3_G,  rndo_g);
            REG_SET_SLICE(parm2[1], RPP_DPCC_RND_OFFS_3_RB, rndo_rb);
            break;
        case RPP_DPCC_METHOD_SET_2:
            REG_SET_SLICE(parm2[0], RPP_DPCC_RO_LIM_2_G,    ro_g);
            REG_SET_SLICE(parm2[0], RPP_DPCC_RO_LIM_2_RB,   ro_rb);
            REG_SET_SLICE(parm2[1], RPP_DPCC_RND_OFFS_2_G,  rndo_g);
            REG_SET_SLICE(parm2[1], RPP_DPCC_RND_OFFS_2_RB, rndo_rb);
            break;
        default:
            REG_SET_SLICE(parm2[0], RPP_DPCC_RO_LIM_1_G,    ro_g);
            REG_SET_SLICE(parm2[0], RPP_DPCC_RO_LIM_1_RB,   ro_rb);
            REG_SET_SLICE(parm2[1], RPP_DPCC_RND_OFFS_1_G,  rndo_g);
            REG_SET_SLICE(parm2[1], RPP_DPCC_RND_OFFS_1_RB, rndo_rb);
    }

    /* now, write the registers back. */
    ret = WRITE_REGS(RPP_RPP_DPCC_RO_LIMITS_REG, parm2, 2);
    if (!ret)
    {
        ret = WRITE_REGS(regoff, parm, 5);
    }
    if (!ret)
    {
        ret = WRITEL(method_set_addr[method_id], flags);
    }

    return ret;
}

/******************************************************************************
 * rpp_dpcc_method
 *****************************************************************************/
int rpp_dpcc_method
(
    rpp_dpcc_drv_t *            drv,
    const unsigned              method_id,
    rpp_dpcc_method_t * const   green,
    rpp_dpcc_method_t * const   rb
)
{
    uint32_t parm[5] = {0, };   /* method parameter register value          */
    uint32_t parm2[2];          /* RO limit and RND offset register value   */
    uint32_t flags;             /* method enable register value             */
    uint32_t on, p1, p2;        /* method parameters                        */
    uint32_t ro_g, ro_rb;       /* rank order limits                        */
    uint32_t rndo_g, rndo_rb;   /* rank neighbour difference offsets        */
    uint32_t lc_thr_bits;       /* line check threshold bits                */
    uint32_t lc_thr_mask;       /* line check threshold bitmask             */
    int ret;

    /* simple base address check */
    if ( !check_valid(drv) || !green || !rb )
    {
        return ( -EINVAL );
    }

    /* range-check the arguments. */
    if (method_id >= RPP_DPCC_METHOD_SET_NUM)
    {
        return -ERANGE;
    }

    /* the threshold is 8bit wide for 12bit instances and 16bit wide for
     * 24bit instances of the DPCC unit. */
    lc_thr_bits = (drv->colorbits > 12) ? 16 : 8;
    lc_thr_mask = UNSIGNED_MAX(lc_thr_bits);

    RPP_DPCC_METHOD_INIT(*green);
    RPP_DPCC_METHOD_INIT(*rb);

    /* read all the registers. */
    ret = READL(method_set_addr[method_id], &flags);
    if (!ret)
    {
        ret = READ_REGS(check_base[method_id], parm, 5);
    }
    if (!ret)
    {
        ret = READ_REGS(RPP_RPP_DPCC_RO_LIMITS_REG, parm2, 2);
    }
    if (ret)
    {
        return ret;
    }

    /* decode parameters stored in registers with values for each set. */
    switch(method_id) {
        case RPP_DPCC_METHOD_SET_3:
            ro_g    = REG_GET_SLICE(parm2[0], RPP_DPCC_RO_LIM_3_G);
            ro_rb   = REG_GET_SLICE(parm2[0], RPP_DPCC_RO_LIM_3_RB);
            rndo_g  = REG_GET_SLICE(parm2[1], RPP_DPCC_RND_OFFS_3_G);
            rndo_rb = REG_GET_SLICE(parm2[1], RPP_DPCC_RND_OFFS_3_RB);
            break;
        case RPP_DPCC_METHOD_SET_2:
            ro_g    = REG_GET_SLICE(parm2[0], RPP_DPCC_RO_LIM_2_G);
            ro_rb   = REG_GET_SLICE(parm2[0], RPP_DPCC_RO_LIM_2_RB);
            rndo_g  = REG_GET_SLICE(parm2[1], RPP_DPCC_RND_OFFS_2_G);
            rndo_rb = REG_GET_SLICE(parm2[1], RPP_DPCC_RND_OFFS_2_RB);
            break;
        default:
            ro_g    = REG_GET_SLICE(parm2[0], RPP_DPCC_RO_LIM_1_G);
            ro_rb   = REG_GET_SLICE(parm2[0], RPP_DPCC_RO_LIM_1_RB);
            rndo_g  = REG_GET_SLICE(parm2[1], RPP_DPCC_RND_OFFS_1_G);
            rndo_rb = REG_GET_SLICE(parm2[1], RPP_DPCC_RND_OFFS_1_RB);
    }
    /* decode line check parameters. */
    on = REG_GET_SLICE(flags,    RPP_DPCC_LC_GREEN1_ENABLE);
    p1 = REG_GET_SLICE(parm[1],  RPP_DPCC_LINE_MAD_FAC_1_G);
    p2 = parm[0] & lc_thr_mask;
    RPP_DPCC_CHECK_SET_LC(*green, on, p1, p2);

    on = REG_GET_SLICE(flags,    RPP_DPCC_LC_RED_BLUE1_ENABLE);
    p1 = REG_GET_SLICE(parm[1],  RPP_DPCC_LINE_MAD_FAC_1_RB);
    p2 = (parm[0] >> lc_thr_bits) & lc_thr_mask;
    RPP_DPCC_CHECK_SET_LC(*rb, on, p1, p2);

    /* decode peak gradient check parameters */
    on = REG_GET_SLICE(flags,    RPP_DPCC_PG_GREEN1_ENABLE);
    p1 = REG_GET_SLICE(parm[2],  RPP_DPCC_PG_FAC_1_G);
    RPP_DPCC_CHECK_SET_PG(*green, on, p1);
    
    on = REG_GET_SLICE(flags,    RPP_DPCC_PG_RED_BLUE1_ENABLE);
    p1 = REG_GET_SLICE(parm[2],  RPP_DPCC_PG_FAC_1_RB);
    RPP_DPCC_CHECK_SET_PG(*rb, on, p1);

    /* configure rank neighbour difference check parameters */
    on = REG_GET_SLICE(flags,    RPP_DPCC_RND_GREEN1_ENABLE);
    p1 = REG_GET_SLICE(parm[3],  RPP_DPCC_RND_THR_1_G);
    RPP_DPCC_CHECK_SET_RND(*green, on, p1, rndo_g);

    on = REG_GET_SLICE(flags,    RPP_DPCC_RND_RED_BLUE1_ENABLE);
    p1 = REG_GET_SLICE(parm[3],  RPP_DPCC_RND_THR_1_RB);
    RPP_DPCC_CHECK_SET_RND(*rb, on, p1, rndo_rb);

    /* configure rank gradient check parameters */
    on = REG_GET_SLICE(flags,    RPP_DPCC_RG_GREEN1_ENABLE);
    p1 = REG_GET_SLICE(parm[4],  RPP_DPCC_RG_FAC_1_G);
    RPP_DPCC_CHECK_SET_RG(*green, on, p1);

    on = REG_GET_SLICE(flags,    RPP_DPCC_RG_RED_BLUE1_ENABLE);
    p1 = REG_GET_SLICE(parm[4],  RPP_DPCC_RG_FAC_1_RB);
    RPP_DPCC_CHECK_SET_RG(*rb, on, p1);

    /* configure rank order check parameters. */
    on = REG_GET_SLICE(flags,    RPP_DPCC_RO_GREEN1_ENABLE);
    RPP_DPCC_CHECK_SET_RO(*green, on, ro_g);

    on = REG_GET_SLICE(flags,    RPP_DPCC_RO_RED_BLUE1_ENABLE);
    RPP_DPCC_CHECK_SET_RO(*rb, on, ro_rb);

    return ( 0 );
}

/******************************************************************************
 * rpp_dpcc_set_used_sets
 *****************************************************************************/
int rpp_dpcc_set_used_sets
(
    rpp_dpcc_drv_t *    drv,
    uint8_t             use_set1,
    uint8_t             use_set2,
    uint8_t             use_set3,
    uint8_t             use_fixed
)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return -EINVAL;
    }

    ret = READL(RPP_RPP_DPCC_SET_USE_REG, &v);
    if (!ret)
    {
        REG_SET_SLICE(v, RPP_DPCC_STAGE1_USE_SET_1,     !!use_set1);
        REG_SET_SLICE(v, RPP_DPCC_STAGE1_USE_SET_2,     !!use_set2);
        REG_SET_SLICE(v, RPP_DPCC_STAGE1_USE_SET_3,     !!use_set3);
        REG_SET_SLICE(v, RPP_DPCC_STAGE1_USE_FIX_SET,   !!use_fixed);
        ret = WRITEL(RPP_RPP_DPCC_SET_USE_REG, v);
    }

    return ret;
}

/******************************************************************************
 * rpp_dpcc_used_sets
 *****************************************************************************/
int rpp_dpcc_used_sets
(
    rpp_dpcc_drv_t *    drv,
    uint8_t *           use_set1,
    uint8_t *           use_set2,
    uint8_t *           use_set3,
    uint8_t *           use_fixed
)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !use_set1 || !use_set2 || !use_set3 || !use_fixed)
    {
        return -EINVAL;
    }

    ret = READL(RPP_RPP_DPCC_SET_USE_REG, &v);
    if (!ret) {
        *use_set1   = REG_GET_SLICE(v, RPP_DPCC_STAGE1_USE_SET_1);
        *use_set2   = REG_GET_SLICE(v, RPP_DPCC_STAGE1_USE_SET_2);
        *use_set3   = REG_GET_SLICE(v, RPP_DPCC_STAGE1_USE_SET_3);
        *use_fixed  = REG_GET_SLICE(v, RPP_DPCC_STAGE1_USE_FIX_SET);
    }
    
    return ret;
}

/******************************************************************************
 * rpp_dpcc_set_output_mode
 *****************************************************************************/

static inline void decode_mode(rpp_dpcc_interpolation_t mode, uint32_t * inc_center, uint32_t * use_3x3)
{
    *use_3x3 =      (mode == DPCC_INTERPOLATION_MEDIAN_9) ? 1 : 0;
    *inc_center =   (mode == DPCC_INTERPOLATION_MEDIAN_5) ? 1 : 0;
}

static inline int encode_mode(uint32_t inc_center, uint32_t use_3x3)
{
    return (use_3x3) ? DPCC_INTERPOLATION_MEDIAN_9 :
        (inc_center) ? DPCC_INTERPOLATION_MEDIAN_5 : DPCC_INTERPOLATION_MEDIAN_4;
}

int rpp_dpcc_set_output_mode
(
    rpp_dpcc_drv_t *            drv,
    rpp_dpcc_interpolation_t    mode_gr,
    rpp_dpcc_interpolation_t    mode_rb
)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return -EINVAL;
    }

    /* range checks... */
    if (
            (mode_gr >= RPP_DPCC_NUM_INTERPOLATION) ||
            (mode_rb >= RPP_DPCC_NUM_INTERPOLATION)
       )
    {
        return -ERANGE;
    }

    /* read and update the register. */
    ret = READL(RPP_RPP_DPCC_OUTPUT_MODE_REG, &v);
    if (!ret) {
        uint32_t incl_center;
        uint32_t use_3x3;

        /* configure the green pixel correction mode. */
        decode_mode(mode_gr, &incl_center, &use_3x3);
        REG_SET_SLICE(v, RPP_DPCC_STAGE1_INCL_GREEN_CENTER, incl_center);
        REG_SET_SLICE(v, RPP_DPCC_STAGE1_G_3X3,             use_3x3);

        /* configure the red/blue pixel correction mode. */
        decode_mode(mode_rb, &incl_center, &use_3x3);
        REG_SET_SLICE(v, RPP_DPCC_STAGE1_INCL_RB_CENTER,    incl_center);
        REG_SET_SLICE(v, RPP_DPCC_STAGE1_RB_3X3,            use_3x3);

        /* and program the updated register value */
        ret = WRITEL(RPP_RPP_DPCC_OUTPUT_MODE_REG, v);
    }

    return ret;
}

/******************************************************************************
 * rpp_dpcc_output_mode
 *****************************************************************************/
int rpp_dpcc_output_mode
(
    rpp_dpcc_drv_t *            drv,
    rpp_dpcc_interpolation_t *  mode_gr,
    rpp_dpcc_interpolation_t *  mode_rb
)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !mode_gr || !mode_rb)
    {
        return -EINVAL;
    }

    /* read the register. */
    ret = READL(RPP_RPP_DPCC_OUTPUT_MODE_REG, &v);
    if (!ret) {
        uint32_t incl_center;
        uint32_t use_3x3;

        /* decode the green pixel correction mode. */
        incl_center = REG_GET_SLICE(v, RPP_DPCC_STAGE1_INCL_GREEN_CENTER);
        use_3x3     = REG_GET_SLICE(v, RPP_DPCC_STAGE1_G_3X3);
        *mode_gr    = encode_mode(incl_center, use_3x3);

        /* decode the red/blue pixel correction mode. */
        incl_center = REG_GET_SLICE(v, RPP_DPCC_STAGE1_INCL_RB_CENTER);
        use_3x3     = REG_GET_SLICE(v, RPP_DPCC_STAGE1_RB_3X3);
        *mode_rb    = encode_mode(incl_center, use_3x3);
    }
    return ret;
}

/******************************************************************************
 * rpp_dpcc_bpt_enable
 *****************************************************************************/
int rpp_dpcc_bpt_enable
(
    rpp_dpcc_drv_t *            drv,
    unsigned                    enabled,
    rpp_dpcc_interpolation_t    mode_gr,
    rpp_dpcc_interpolation_t    mode_rb
)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return -EINVAL;
    }

    /* range checks */
    if (enabled) {
        if (
                (mode_gr >= RPP_DPCC_NUM_INTERPOLATION) ||
                (mode_rb >= RPP_DPCC_NUM_INTERPOLATION)
           )
        {
            return -ERANGE;
        }
    }

    /* read the BPT control register and update it */
    ret = READL(RPP_RPP_DPCC_BPT_CTRL_REG, &v);
    if (!ret) {
        uint32_t incl_center;
        uint32_t use_3x3;

        decode_mode(mode_gr, &incl_center, &use_3x3);
        REG_SET_SLICE(v, RPP_DPCC_BPT_INCL_GREEN_CENTER,    incl_center);
        REG_SET_SLICE(v, RPP_DPCC_BPT_G_3X3,                use_3x3);

        decode_mode(mode_rb, &incl_center, &use_3x3);
        REG_SET_SLICE(v, RPP_DPCC_BPT_INCL_RB_CENTER,       incl_center);
        REG_SET_SLICE(v, RPP_DPCC_BPT_RB_3X3,               use_3x3);

        REG_SET_SLICE(v, RPP_DPCC_BPT_COR_EN,               !!enabled);
        
        ret = WRITEL(RPP_RPP_DPCC_BPT_CTRL_REG, v);
    }
    return ret;
}

/******************************************************************************
 * rpp_dpcc_bpt_enabled
 *****************************************************************************/
int rpp_dpcc_bpt_enabled
(
    rpp_dpcc_drv_t *            drv,
    unsigned *                  enabled,
    rpp_dpcc_interpolation_t *  mode_gr,
    rpp_dpcc_interpolation_t *  mode_rb
)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !enabled || !mode_gr || !mode_rb )
    {
        return -EINVAL;
    }

    /* read the BPT control register. */
    ret = READL(RPP_RPP_DPCC_BPT_CTRL_REG, &v);
    if (!ret) {
        uint32_t incl_center;
        uint32_t use_3x3;

        incl_center = REG_GET_SLICE(v, RPP_DPCC_BPT_INCL_GREEN_CENTER);
        use_3x3     = REG_GET_SLICE(v, RPP_DPCC_BPT_G_3X3);
        *mode_gr    = encode_mode(incl_center, use_3x3);

        incl_center = REG_GET_SLICE(v, RPP_DPCC_BPT_INCL_RB_CENTER);
        use_3x3     = REG_GET_SLICE(v, RPP_DPCC_BPT_RB_3X3);
        *mode_rb    = encode_mode(incl_center, use_3x3);

        *enabled    = REG_GET_SLICE(v, RPP_DPCC_BPT_COR_EN);
    }

    return ret;
}


/******************************************************************************
 * rpp_dpcc_bpt_write_enable
 *****************************************************************************/
int rpp_dpcc_bpt_write_enable
(
    rpp_dpcc_drv_t *    drv,
    uint8_t             report_set1,
    uint8_t             report_set2,
    uint8_t             report_set3,
    uint8_t             report_fixed
)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return -EINVAL;
    }

    /* read the BPT control register and update it */
    ret = READL(RPP_RPP_DPCC_BPT_CTRL_REG, &v);
    if (!ret) {
        /* use a binary OR on all individual flags to check whether
         * defect pixel output shall be enabled globally. */
        uint32_t on = report_set1 | report_set2 | report_set3 | report_fixed;

        REG_SET_SLICE(v, RPP_DPCC_BPT_USE_SET_1,    !!report_set1);
        REG_SET_SLICE(v, RPP_DPCC_BPT_USE_SET_2,    !!report_set2);
        REG_SET_SLICE(v, RPP_DPCC_BPT_USE_SET_3,    !!report_set3);
        REG_SET_SLICE(v, RPP_DPCC_BPT_USE_FIX_SET,  !!report_fixed);
        REG_SET_SLICE(v, RPP_DPCC_BPT_DET_EN,       !!on);
        ret = WRITEL(RPP_RPP_DPCC_BPT_CTRL_REG, v);
    }

    return ret;
}

/******************************************************************************
 * rpp_dpcc_bpt_write_enable
 *****************************************************************************/
int rpp_dpcc_bpt_write_enabled
(
    rpp_dpcc_drv_t *    drv,
    uint8_t *           report_set1,
    uint8_t *           report_set2,
    uint8_t *           report_set3,
    uint8_t *           report_fixed
)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !report_set1 || !report_set2 || !report_set3 || !report_fixed )
    {
        return -EINVAL;
    }

    /* read and parse the BPT control register */
    ret = READL(RPP_RPP_DPCC_BPT_CTRL_REG, &v);
    if (!ret) {
        uint32_t on;
        on              = REG_GET_SLICE(v, RPP_DPCC_BPT_DET_EN);
        /* if BPT detection is disabled globally, return all ZERO flags */
        if (!on)
        {
            v = 0;
        }
        *report_set1    = REG_GET_SLICE(v, RPP_DPCC_BPT_USE_SET_1);
        *report_set2    = REG_GET_SLICE(v, RPP_DPCC_BPT_USE_SET_2);
        *report_set3    = REG_GET_SLICE(v, RPP_DPCC_BPT_USE_SET_3);
        *report_fixed   = REG_GET_SLICE(v, RPP_DPCC_BPT_USE_FIX_SET);
    }

    return ret;
}

/******************************************************************************
 * rpp_dpcc_bpt_set_table
 *****************************************************************************/
int rpp_dpcc_bpt_set_table
(
    rpp_dpcc_drv_t *        drv,
    unsigned                size,
    const rpp_dpcc_bpt_t *  pixels
)
{
    uint32_t i, n, v;
    int ret = 0;
    /* each coordinate is a 12bit value, and the shift
     * between X and Y part is 16bit. */
    uint32_t xmask  = UNSIGNED_MAX(13);
    uint32_t ymask  = UNSIGNED_MAX(12);
    uint32_t yshift = 16;
    /* get the maximum table address. */
    uint32_t amax   = RPP_DPCC_BP_TABLE_ADDR_MASK;
    /* copy of the array pointer. */
    const rpp_dpcc_bpt_t * tmp = pixels;

    /* simple sanity checks */
    if ( !check_valid(drv) || (size && !pixels) )
    {
        return -EINVAL;
    }

    n = size;
    /* perform a range check on the table size */
    if ( n > amax)
    {
        return -ERANGE;
    }

    /* check coordinate limits and the order of entries. */

    v = 0;
    for (i = 0; i < n; ++i, ++tmp) {
        uint32_t regval;

        /* check that the coordinates are in range. */
        if ( (tmp->pos_x > xmask) || (tmp->pos_y > ymask) )
        {
            return -ERANGE;
        }

        /* check that the new coordinate is larger than
         * the previous one (in pixel scan order). Allow
         * the very first coordinate to be zero. */
        regval = tmp->pos_x + (tmp->pos_y << yshift);
        if ( i && (regval <= v) )
        {
            return -ERANGE;
        }
        /* remember the current coordinate for the next round. */
        v = regval;
    }

    tmp = pixels;
    /* to conserve memory, use chunks of 16 items size */
    for (i = 0; !ret && (i < n); i += 16) {
        uint32_t j;
        uint32_t regs[16];
        uint32_t num = n - i;
        num = (num < 16) ? num : 16;

        for (j = 0; j < num; ++j) {
            regs[j] = tmp->pos_x + (tmp->pos_y << yshift);
            ++tmp;
        }

        /* programm the address register */
        if (!ret)
        {
            ret = WRITEL(RPP_RPP_DPCC_BP_TADDR_REG, i);
        }

        /* and program the table entries */
        if (!ret)
        {
            ret = WRITE_RAM(RPP_RPP_DPCC_BP_POSITION_REG, regs, num);
        }
    }

    /* check whether there's still space for the end-of-table marker. */
    if (!ret && (n < amax))
    {
        ret = WRITEL(RPP_RPP_DPCC_BP_POSITION_REG, RPP_DPCC_BP_POSITION_MASK);
    }

    /* and, if no error yet, also write the table size register.
     * it is unused by the hardware, but tells us how many valid
     * entries the table contains. */
    if (!ret)
    {
        ret = WRITEL(RPP_RPP_DPCC_BP_NUMBER_REG, n);
    }

    return ret;
}


/******************************************************************************
 * rpp_dpcc_bpt_table
 *****************************************************************************/
int rpp_dpcc_bpt_table
(
    rpp_dpcc_drv_t *    drv,
    unsigned *          size,
    rpp_dpcc_bpt_t *    pixels
)
{
    int ret;
    uint32_t i, n, v;
    rpp_dpcc_bpt_t * tmp = pixels;
    /* each coordinate is a 12bit value, and the shift
     * between X and Y part is 16bit. */
    uint32_t xmask  = UNSIGNED_MAX(13);
    uint32_t ymask  = UNSIGNED_MAX(12);
    uint32_t yshift = 16;

    /* simple sanity checks. */
    if ( !check_valid(drv) || !size )
    {
        return -EINVAL;
    }
    if ( *size && !pixels ) {
        return -EINVAL;
    }

    /* get the number of available table entries. */
    n = 0;
    ret = READL(RPP_RPP_DPCC_BP_NUMBER_REG, &v);
    if (!ret)
    {
        n = REG_GET_SLICE(v, RPP_DPCC_BP_POSITION);
    }
    /* get the minimum of requested vs. available. */
    n = (n < *size) ? n : *size;

    /* if nothing is to be retrieved, return now. */
    if (!n) {
        *size = 0;
        return ret;
    }

    /* otherwise read the table in chunks of 16 entries
     * to be conservative with our RAM. */
    for (i = 0; !ret && (i < n); i += 16)
    {
        uint32_t regs[16];
        uint32_t num = n - i;
        /* limit the current read size to 16 at most. */
        num = (num < 16) ? num : 16;

        /* programm the address register */
        if (!ret)
        {
            ret = WRITEL(RPP_RPP_DPCC_BP_TADDR_REG, i);
        }

        if (!ret)
        {
            ret = READ_RAM(RPP_RPP_DPCC_BP_POSITION_REG, regs, num);
        }

        if (!ret) {
            /* if successful, parse the coordinates. */
            uint32_t j;
            for (j = 0; j < num; ++j) {
                tmp->pos_x = regs[j] & xmask;
                tmp->pos_y = (regs[j] >> yshift) & ymask;
                ++tmp;
            }
        }

    }

    /* if successful, update the number of entries read. */
    if (!ret)
    {
        *size = n;
    }
    return ret;
}

