/******************************************************************************
 *
 * Copyright 2019 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_exm_drv.c
 *
 * @brief   Implementation of exposure measurement driver
 *
 *****************************************************************************/
#include <rpp_exm_drv.h>

// make sure that the base is 0 in the auto-generated
// header-file, before including it
#define RPP_EXM_BASE 0
#include <rpp_exm_regs_addr_map.h>
#include <rpp_exm_regs_mask.h>

/******************************************************************************
 * local definitions
 *****************************************************************************/

static inline int check_valid(const rpp_exm_drv_t * drv)
{
    return (drv && drv->dev && drv->resultbits);
}


/******************************************************************************
 * rpp_exm_init
 *****************************************************************************/
int rpp_exm_init(rpp_device * dev, uint32_t base, rpp_exm_drv_t * drv)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !dev || !drv )
    {
        return ( -EINVAL );
    }
    drv->dev = dev;
    drv->base = base;
    drv->version = 0;
    drv->resultbits = 0;

    /* read and parse the version register. */
    ret = READL(RPP_RPP_EXM_VERSION_REG, &v);
    if (!ret)
    {
        drv->version = REG_GET_SLICE(v, RPP_EXM_EXM_VERSION);
        switch(drv->version)
        {
            case 0x01:
                /* this is a 12bit instance with 8bit results. */
                drv->resultbits = 8;
                break;
            case 0x03:
                /* this is a 24bit instance with 20bit results. */
                drv->resultbits = 20;
                break;
            case 0x02:
            default:
                /* unhandled or invalid versions. */
                ret = -ENODEV;
        }
    }

    /* initialize a few control flags for continuous operation. */
    if (!ret) {
        ret = READL(RPP_RPP_EXM_CTRL_REG, &v);
    }
    if (!ret) {
        /* allow register updates on top-level RPP HDR request. */
        REG_SET_SLICE(v, RPP_EXM_EXM_UPDATE_ENABLE, 1);
        ret = WRITEL(RPP_RPP_EXM_CTRL_REG, v);
    }

    return ret;
}

/******************************************************************************
 * rpp_exm_disable
 *****************************************************************************/
int rpp_exm_disable(rpp_exm_drv_t * drv)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    /* read the EXM mode register and set to _MODE_DISABLE if successful */
    ret = READL(RPP_RPP_EXM_MODE_REG, &v);
    if (!ret)
    {
        REG_SET_SLICE( v, RPP_EXM_EXM_MODE, RPP_EXM_MEASURE_MODE_DISABLE );
        ret = WRITEL(RPP_RPP_EXM_MODE_REG, v);
    }

    return ret;
}

/******************************************************************************
 * rpp_exm_set_mode
 *****************************************************************************/
int rpp_exm_set_mode(rpp_exm_drv_t * drv,
        const unsigned mode, const unsigned channel)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    /* range-check parameters */
    if (mode >= RPP_EXM_MEASURE_MODE_MAX)
    {
        return -ERANGE;
    }

    /* channel parameter is only relevant if the unit
     * is going to be enabled. */
    if (
            (mode != RPP_EXM_MEASURE_MODE_DISABLE) && 
            (channel >= RPP_EXM_MEASURE_CHANNEL_MAX)
       )
    {
        return -ERANGE;
    }

    /* read the MODE register and update it to the selected mode. */
    ret = READL(RPP_RPP_EXM_MODE_REG, &v);
    if (!ret)
    {
        REG_SET_SLICE( v, RPP_EXM_EXM_MODE, mode);
        ret = WRITEL(RPP_RPP_EXM_MODE_REG, v);
    }

    /* if the unit is disabled, don't program the channel register. */
    if (ret || (mode == RPP_EXM_MEASURE_MODE_DISABLE))
    {
        return ret;
    }

    /* update the channel register. */
    ret = READL(RPP_RPP_EXM_CHANNEL_SEL_REG, &v);
    if (!ret)
    {
        REG_SET_SLICE( v, RPP_EXM_CHANNEL_SELECT, channel );
        ret = WRITEL(RPP_RPP_EXM_CHANNEL_SEL_REG, v);
    }

    return ret;
}

/******************************************************************************
 * rpp_exm_mode
 *****************************************************************************/
int rpp_exm_mode(rpp_exm_drv_t * drv,
        unsigned * const mode, unsigned * const channel)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !mode || !channel )
    {
        return ( -EINVAL );
    }

    /* read the MODE register */
    ret = READL(RPP_RPP_EXM_MODE_REG, &v);
    if (!ret)
    {
        /* if successful, extract the value */
        *mode = REG_GET_SLICE( v, RPP_EXM_EXM_MODE );
        /* and read the CHANNEL_SEL register next. */
        ret = READL(RPP_RPP_EXM_CHANNEL_SEL_REG, &v);
    }
    if (!ret)
    {
        /* if still successful, extract the channel. */
        *channel = REG_GET_SLICE( v, RPP_EXM_CHANNEL_SELECT );
    }

    return ret;
}

/******************************************************************************
 * rpp_exm_set_csm
 *****************************************************************************/
int rpp_exm_set_csm(rpp_exm_drv_t * drv, const rpp_exm_csm_t csm)
{
    /* simple sanity checks */
    if ( !check_valid(drv) || !csm )
    {
        return ( -EINVAL );
    }

    /* range-check the supplied values. */
    if (
            REG_RANGE_CHECK(GET_EXM_CSM_COEFF(csm, 0), RPP_EXM_COEFF_R)    ||
            REG_RANGE_CHECK(GET_EXM_CSM_COEFF(csm, 1), RPP_EXM_COEFF_G_GR) ||
            REG_RANGE_CHECK(GET_EXM_CSM_COEFF(csm, 2), RPP_EXM_COEFF_B)    ||
            REG_RANGE_CHECK(GET_EXM_CSM_COEFF(csm, 3), RPP_EXM_COEFF_GB) )
    {
        return ( -ERANGE );
    }

    /* The supplied values are uint32_t's as are the registers, and have been
     * range-checked properly. There are no other slices in the registers, so
     * use efficient multiple-register writes here. */
    return WRITE_REGS(RPP_RPP_EXM_COEFF_R_REG, csm, 4);
}

/******************************************************************************
 * rpp_exm_csm
 *****************************************************************************/
int rpp_exm_csm(rpp_exm_drv_t * drv, rpp_exm_csm_t csm)
{
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !csm )
    {
        return ( -EINVAL );
    }

    /* the supplied storage is uint32_t as are the registers. this
     * means efficients multiple-register reads can be used, followed
     * by masking just to be sure. */
    ret = READ_REGS(RPP_RPP_EXM_COEFF_R_REG, csm, 4);
    if (!ret)
    {
        csm[0] &= RPP_EXM_COEFF_R_MASK;
        csm[1] &= RPP_EXM_COEFF_G_GR_MASK;
        csm[2] &= RPP_EXM_COEFF_B_MASK;
        csm[3] &= RPP_EXM_COEFF_GB_MASK;
    }

    return ret;
}

/******************************************************************************
 * rpp_exm_set_meas_window
 *****************************************************************************/
int rpp_exm_set_meas_window(rpp_exm_drv_t * drv, const rpp_window_t * win)
{
    uint32_t regs[5];
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !win )
    {
        return ( -EINVAL );
    }

    if ( (win->width % 5) || (win->height % 5) )
    {
        return ( -EINVAL );
    }

    /* calculate coordinates for the first sub-window. */
    regs[0] = win->hoff;
    regs[1] = win->voff;
    regs[2] = win->width / 5;
    regs[3] = win->height / 5;
    /* The next value is for the  RPP_EXM_FORCED_UPD_START_LINE
     * register used for fast channel switching. This is not
     * currently supported by this driver, so set the measurement
     * start line to zero. */
    regs[4] = 0;
    
    /* range-check the window coordinates. */
    if (
            REG_RANGE_CHECK(regs[0],    RPP_EXM_EXM_H_OFFSET)  ||
            REG_RANGE_CHECK(regs[1],    RPP_EXM_EXM_V_OFFSET)  ||
            REG_RANGE_CHECK(regs[2],    RPP_EXM_EXM_H_SIZE)    ||
            REG_RANGE_CHECK(regs[3],    RPP_EXM_EXM_V_SIZE)
       )
    {
        return ( -ERANGE );
    }

    /* program the measurement window and the "fast channel switching" start line. */
    ret = WRITE_REGS(RPP_RPP_EXM_H_OFFS_REG, regs, 5);

    /* program the image line after which an interrupt will be raised. This shall
     * be programmed to a value larger than (win->voff + win->height - 1) to
     * allow divisions for average calculation to complete. */
    if (!ret)
    {
        regs[0] = win->voff + win->height + 1;
        ret = WRITEL(RPP_RPP_EXM_LAST_MEAS_LINE_REG, regs[0]);
    }

    return ret;
}

/******************************************************************************
 * rpp_exm_meas_window
 *****************************************************************************/
int rpp_exm_meas_window(rpp_exm_drv_t * drv, rpp_window_t * const win)
{
    uint32_t regs[4];
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !win )
    {
        return ( -EINVAL );
    }

    /* read the window registers using efficient multiple-register reads. */
    ret = READ_REGS(RPP_RPP_EXM_H_OFFS_REG, regs, 4);

    /* if successful, extract and re-scale the data. */
    if (!ret)
    {
        win->hoff   = REG_GET_SLICE(regs[0], RPP_EXM_EXM_H_OFFSET);
        win->voff   = REG_GET_SLICE(regs[1], RPP_EXM_EXM_V_OFFSET);
        win->width  = REG_GET_SLICE(regs[2], RPP_EXM_EXM_H_SIZE) * 5u;
        win->height = REG_GET_SLICE(regs[3], RPP_EXM_EXM_V_SIZE) * 5u;
    }

    return ret;
}

/******************************************************************************
 * rpp_exm_means
 *****************************************************************************/
int rpp_exm_means(rpp_exm_drv_t * drv, rpp_exm_mean_t means)
{
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !means )
    {
        return ( -EINVAL );
    }

    /* read the values into the supplied array. */
    ret = READ_REGS(RPP_RPP_EXM_MEAN__0_REG, means, RPP_EXM_MEANS_NUM);

    /* if successful, mask the values according to the
     * valid result bit width. */
    if (!ret)
    {
        uint32_t mask = UNSIGNED_MAX(drv->resultbits);
        unsigned i;
        for (i = 0; i < RPP_EXM_MEANS_NUM; ++i)
        {
            means[i] &= mask;
        }
    }

    return ret;
}


