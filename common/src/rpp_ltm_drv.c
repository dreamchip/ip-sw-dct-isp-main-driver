/******************************************************************************
 *
 * Copyright 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_ltm_drv.c
 *
 * @brief   Implementation of Local Tone Mapping unit driver
 *
 *****************************************************************************/
#include <rpp_ltm_drv.h>

// make sure that the base is 0 in the auto-generated
// header-file, before including it
#define RPP_LTM_BASE 0
#include <rpp_ltm_regs_addr_map.h>
#include <rpp_ltm_regs_mask.h>

/******************************************************************************
 * local definitions
 *****************************************************************************/

static inline int check_valid(const rpp_ltm_drv_t * drv)
{
    return (drv && drv->dev && drv->version);
}



/******************************************************************************
 * rpp_ltm_init
 *****************************************************************************/
int rpp_ltm_init(rpp_device * dev, uint32_t base, rpp_ltm_drv_t * drv)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !dev || !drv )
    {
        return ( -EINVAL );
    }

    /* initialize the driver structure */
    drv->dev = dev;
    drv->base = base;
    drv->version = 0;

    /* check that rpp ltm module exists by reading the version register. */
    ret = READL(RPP_RPP_LTM_VERSION_REG, &v);

    if (!ret)
    {
        drv->version = REG_GET_SLICE(v, RPP_LTM_LTM_VERSION);
        if ( !drv->version )
        {
            ret = -ENODEV;
        }
    }

    return ret;
}

/******************************************************************************
 * rpp_ltm_enable
 *****************************************************************************/
int rpp_ltm_enable(rpp_ltm_drv_t * drv, const unsigned on)
{
    uint32_t v;
    int ret;

    /* simple base address check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    ret = READL(RPP_RPP_LTM_CTRL_REG, &v);
    if (!ret)
    {
        REG_SET_SLICE(v, RPP_LTM_LTM_ENABLE, !!on);
        ret = WRITEL(RPP_RPP_LTM_CTRL_REG, v);
    }
    return ret;
}

/******************************************************************************
 * rpp_ltm_enabled
 *****************************************************************************/
int rpp_ltm_enabled(rpp_ltm_drv_t * drv, unsigned * const on)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !on )
    {
        return ( -EINVAL );
    }

    ret = READL(RPP_RPP_LTM_CTRL_REG, &v);
    if (!ret)
    {
        *on      = REG_GET_SLICE(v, RPP_LTM_LTM_ENABLE);
    }

    return ret;
}


/******************************************************************************
 * rpp_ltm_set_linewidth
 *****************************************************************************/
int rpp_ltm_set_linewidth(rpp_ltm_drv_t * drv, uint16_t width)
{
    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return -EINVAL;
    }

    /* range-check the parameter */
    if ( REG_RANGE_CHECK(width, RPP_LTM_CLB_LINESIZE) )
    {
        return -ERANGE;
    }

    return WRITEL(RPP_RPP_LTM_CLB_LINESIZE_REG, width);
}


/******************************************************************************
 * rpp_ltm_linewidth
 *****************************************************************************/
int rpp_ltm_linewidth(rpp_ltm_drv_t * drv, uint16_t * const width)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !width )
    {
        return -EINVAL;
    }

    /* read and parse the register value */
    ret = READL(RPP_RPP_LTM_CLB_LINESIZE_REG, &v);
    if (!ret) {
        *width = REG_GET_SLICE(v, RPP_LTM_CLB_LINESIZE);
    }

    return ret;
}

/******************************************************************************
 * rpp_ltm_set_weights
 *****************************************************************************/
int rpp_ltm_set_weights(rpp_ltm_drv_t * drv, uint16_t red,
        uint16_t green, uint16_t blue)
{
    uint32_t v = 0;
    uint32_t sum = red + green + blue;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return -EINVAL;
    }

    /* range-check the parameters */
    if (
            REG_RANGE_CHECK(red,    RPP_LTM_LTM_R_WEIGHT) ||
            REG_RANGE_CHECK(green,  RPP_LTM_LTM_G_WEIGHT) ||
            REG_RANGE_CHECK(blue,   RPP_LTM_LTM_B_WEIGHT) ||
            (sum != RPP_LTM_LTM_R_WEIGHT_MASK)              /* the weights must sum up to 1023 */
       )
    {
        return -ERANGE;
    }

    REG_SET_SLICE(v, RPP_LTM_LTM_R_WEIGHT, red);
    REG_SET_SLICE(v, RPP_LTM_LTM_G_WEIGHT, green);
    REG_SET_SLICE(v, RPP_LTM_LTM_B_WEIGHT, blue);

    return WRITEL(RPP_RPP_LTM_RGB_WEIGHTS_REG, v);
}

/******************************************************************************
 * rpp_ltm_weights
 *****************************************************************************/
int rpp_ltm_weights(rpp_ltm_drv_t * drv, uint16_t * const red,
        uint16_t * const green, uint16_t * const blue)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !red || !green || !blue )
    {
        return -EINVAL;
    }

    /* read and parse the coefficient's register */
    ret = READL(RPP_RPP_LTM_RGB_WEIGHTS_REG, &v);
    if (!ret) {
        *red    = REG_GET_SLICE(v, RPP_LTM_LTM_R_WEIGHT);
        *green  = REG_GET_SLICE(v, RPP_LTM_LTM_G_WEIGHT);
        *blue   = REG_GET_SLICE(v, RPP_LTM_LTM_B_WEIGHT);
    }

    return ret;
}

/******************************************************************************
 * rpp_ltm_set_tonecurve
 *****************************************************************************/
#define NR_DX_REGS NUM_REGS(RPP_LTM_MAX_CURVE_POINTS, RPP_LTM_LTM_DY2_SHIFT)

int rpp_ltm_set_tonecurve(rpp_ltm_drv_t * drv, rpp_ltm_curve_t const * const curve)
{
    uint32_t regs[NR_DX_REGS];
    unsigned i;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !curve )
    {
        return -EINVAL;
    }

    /* range-check the arguments. note that dx[0] must be set to zero. */
    if ( curve->dx[0] )
    {
        return -ERANGE;
    }

    /* now, go on and check numerical limits for all points. */
    for (i = 0; i < RPP_LTM_MAX_CURVE_POINTS; ++i)
    {
        if (
                REG_RANGE_CHECK(curve->dx[i],   RPP_LTM_LTM_DY1)  ||
                REG_RANGE_CHECK(curve->y[i],    RPP_LTM_TONECURVE_YM)
           )
        {
            return -ERANGE;
        }
    }

    /* pack the DX registers tightly for programming. */
    PACK_REGS(
            RPP_LTM_MAX_CURVE_POINTS,
            RPP_LTM_LTM_DY2_SHIFT,
            RPP_LTM_LTM_DY1_MASK,
            curve->dx + 1,
            regs
            );
    ret = WRITE_REGS(RPP_RPP_LTM_TONECURVE_1_REG, regs, NR_DX_REGS);

    /* also program the Y coordinates of the curve. */
    if (!ret) {
        ret = WRITE_REGS(RPP_RPP_LTM_TONECURVE_YM__0_REG,
                curve->y, RPP_LTM_MAX_CURVE_POINTS);
    }

    return ret;
}

/******************************************************************************
 * rpp_ltm_tonecurve
 *****************************************************************************/
int rpp_ltm_tonecurve(rpp_ltm_drv_t * drv, rpp_ltm_curve_t * const curve)
{
    uint32_t regs[NR_DX_REGS];
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !curve )
    {
        return -EINVAL;
    }

    /* read the DX values and unpack them. */
    ret = READ_REGS(RPP_RPP_LTM_TONECURVE_1_REG, regs, NR_DX_REGS);
    if (!ret) {
        UNPACK_REGS(
                RPP_LTM_MAX_CURVE_POINTS,
                RPP_LTM_LTM_DY2_SHIFT,
                RPP_LTM_LTM_DY1_MASK,
                regs,
                curve->dx + 1
                );
        /* set curve->dx[0] to zero as required */
        curve->dx[0] = 0;

        /* and also read the Y array directly into
         * the output structure. */
        ret = READ_REGS(RPP_RPP_LTM_TONECURVE_YM__0_REG,
                curve->y, RPP_LTM_MAX_CURVE_POINTS);
    }

    /* if successful, mask the Y array values. */
    if (!ret) {
        unsigned i;
        for (i = 0; i < RPP_LTM_MAX_CURVE_POINTS; ++i)
        {
            curve->y[i] = REG_GET_SLICE(curve->y[i], RPP_LTM_TONECURVE_YM);
        }
    }

    return ret;
}

/******************************************************************************
 * rpp_ltm_set_ltm_params
 *****************************************************************************/
/* not all the numerical limits can be obtained from
 * the register info header files. */
#define L0X_MIN     0x100

#define KMIND_MIN   0x2526
#define KMIND_MAX   0xDFFF

#define KMAXD_MIN   0x295C
#define KMAXD_MAX   0xE526

#define KDIFF_MIN   0x0435
#define KDIFF_MAX   0x052B

#define KDIFF_R_MIN 0x3189
#define KDIFF_R_MAX 0x3CD5

#define KW_MIN      0x1000
#define KW_MAX      0xC000

#define KW_R_MIN    0x1555

int rpp_ltm_set_ltm_params(rpp_ltm_drv_t * drv,
        rpp_ltm_param_t const * const params)
{
    uint32_t regs[13];

    /* simple sanity checks. */
    if ( !check_valid(drv) || !params )
    {
        return -EINVAL;
    }

#define RANGE_CHECK(_VAL,_MIN,_MAX) ((_VAL < _MIN) || (_VAL > _MAX))
    /* range-check the parameters. */
    if (
            RANGE_CHECK(params->L0w,        L0X_MIN,        RPP_LTM_L0W_MASK)   ||
            REG_RANGE_CHECK(params->L0w_r,  RPP_LTM_L0W_R)                      ||
            RANGE_CHECK(params->L0d,        L0X_MIN,        RPP_LTM_L0D_MASK)   ||
            REG_RANGE_CHECK(params->L0d_r,  RPP_LTM_L0D_R)                      ||
            RANGE_CHECK(params->kmd,        KMIND_MIN,      KMIND_MAX)          ||
            RANGE_CHECK(params->kMd,        KMAXD_MIN,      KMAXD_MAX)          ||
            RANGE_CHECK(params->kdDiff,     KDIFF_MIN,      KDIFF_MAX)          ||
            RANGE_CHECK(params->kdDiff_r,   KDIFF_R_MIN,    KDIFF_R_MAX)        ||
            RANGE_CHECK(params->kw,         KW_MIN,         KW_MAX)             ||
            RANGE_CHECK(params->kw_r,       KW_R_MIN,       RPP_LTM_KW_R_MASK)  ||
            REG_RANGE_CHECK(params->cgain,  RPP_LTM_CGAIN)                      ||
            (params->LprcH_r > ((1LLU << 48) - 1))
       )
    {
        return -ERANGE;
    }

    /* now, prepare the register set. The values are written into
     * adjacent registers, the 48bit LprcH_r value must be split-up
     * into two 32-bit parts. */
    regs[ 0] = params->L0w;
    regs[ 1] = params->L0w_r;
    regs[ 2] = params->L0d;
    regs[ 3] = params->L0d_r;
    regs[ 4] = params->kmd;
    regs[ 5] = params->kMd;
    regs[ 6] = params->kdDiff;
    regs[ 7] = params->kdDiff_r;
    regs[ 8] = params->kw;
    regs[ 9] = params->kw_r;
    regs[10] = params->cgain;
    /* _HIGH register comes first, _LOW is second in address map.
     * Note the swapped indices. */
    regs[12] = params->LprcH_r & 0xFFFFFFFF;
    regs[11] = (params->LprcH_r >> 32) & 0xFFFFFFFF;

    return WRITE_REGS(RPP_RPP_LTM_L0W_REG, regs, 13);
}


/******************************************************************************
 * rpp_ltm_ltm_params
 *****************************************************************************/
int rpp_ltm_ltm_params(rpp_ltm_drv_t * drv, rpp_ltm_param_t * const params)
{
    uint32_t regs[13];
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !params )
    {
        return -EINVAL;
    }

    /* read and parse the registers. */
    ret = READ_REGS(RPP_RPP_LTM_L0W_REG, regs, 13);
    if (!ret) {
        params->L0w         = REG_GET_SLICE(regs[ 0], RPP_LTM_L0W);
        params->L0w_r       = REG_GET_SLICE(regs[ 1], RPP_LTM_L0W_R);
        params->L0d         = REG_GET_SLICE(regs[ 2], RPP_LTM_L0D);
        params->L0d_r       = REG_GET_SLICE(regs[ 3], RPP_LTM_L0D_R);
        params->kmd         = REG_GET_SLICE(regs[ 4], RPP_LTM_K_MIN_D);
        params->kMd         = REG_GET_SLICE(regs[ 5], RPP_LTM_K_MAX_D);
        params->kdDiff      = REG_GET_SLICE(regs[ 6], RPP_LTM_K_DIFF_D);
        params->kdDiff_r    = REG_GET_SLICE(regs[ 7], RPP_LTM_K_DIFF_D_R);
        params->kw          = REG_GET_SLICE(regs[ 8], RPP_LTM_KW);
        params->kw_r        = REG_GET_SLICE(regs[ 9], RPP_LTM_KW_R);
        params->cgain       = REG_GET_SLICE(regs[10], RPP_LTM_CGAIN);
        params->LprcH_r     = REG_GET_SLICE(regs[11], RPP_LTM_LPRCH_R_HIGH);
        params->LprcH_r     = (params->LprcH_r << 32) | REG_GET_SLICE(regs[12], RPP_LTM_LPRCH_R_LOW);
    }

    return ret;
}
