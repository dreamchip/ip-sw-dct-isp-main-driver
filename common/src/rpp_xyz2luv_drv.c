/******************************************************************************
 *
 * Copyright 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_xyz2luv_drv.c
 *
 * @brief   Implementation of of CIE XYZ to LUV unit driver
 *
 *****************************************************************************/
#include <rpp_xyz2luv_drv.h>

// make sure that the base is 0 in the auto-generated
// header-file, before including it
#define RPP_XYZ2LUV_BASE 0
#include <rpp_xyz2luv_regs_addr_map.h>
#include <rpp_xyz2luv_regs_mask.h>

/******************************************************************************
 * local definitions
 *****************************************************************************/

static inline int check_valid(const rpp_xyz2luv_drv_t * drv)
{
    return (drv && drv->dev);
}

/******************************************************************************
 * rpp_xyz2luv_init
 *****************************************************************************/
int rpp_xyz2luv_init(rpp_device * dev, uint32_t base, rpp_xyz2luv_drv_t * drv)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !dev || !drv )
    {
        return ( -EINVAL );
    }
    drv->dev = dev;
    drv->base = base;
    drv->version = 0;

    /* read the version register. */
    ret = READL(RPP_RPP_XYZ2LUV_VERSION_REG, &v);
    if (!ret)
    {
        drv->version = REG_GET_SLICE(v, RPP_XYZ2LUV_XYZ2LUV_VERSION);
        /* check for a minimum version of the core. */
        if (drv->version < 0x04)
        {
            ret = -ENODEV;
        }
    }

    return ret;
}

/******************************************************************************
 * rpp_xyz2luv_set_ref
 *****************************************************************************/
int rpp_xyz2luv_set_ref(rpp_xyz2luv_drv_t * drv, const uint16_t u_ref,
        const uint16_t v_ref)
{
    uint32_t regs[2];

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    /* range-check the arguments */
    if (
            REG_RANGE_CHECK(u_ref, RPP_XYZ2LUV_XYZ2LUV_U_REF)   ||
            REG_RANGE_CHECK(v_ref, RPP_XYZ2LUV_XYZ2LUV_V_REF)
       )
    {
        return -ERANGE;
    }

    /* prepare the register cache. */
    regs[0] = u_ref;
    regs[1] = v_ref;

    return WRITE_REGS(RPP_RPP_XYZ2LUV_U_REF_REG, regs, 2);
}

/******************************************************************************
 * rpp_xyz2luv_ref
 *****************************************************************************/
int rpp_xyz2luv_ref(rpp_xyz2luv_drv_t * drv, uint16_t * const u_ref,
        uint16_t * const v_ref)
{
    uint32_t regs[2];
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !u_ref || !v_ref )
    {
        return ( -EINVAL );
    }

    /* read and parse the configuration registers. */
    ret = READ_REGS(RPP_RPP_XYZ2LUV_U_REF_REG, regs, 2);
    if (!ret) {
        *u_ref = REG_GET_SLICE(regs[0], RPP_XYZ2LUV_XYZ2LUV_U_REF);
        *v_ref = REG_GET_SLICE(regs[1], RPP_XYZ2LUV_XYZ2LUV_V_REF);
    }

    return ret;
}

/******************************************************************************
 * rpp_xyz2luv_set_output_factors
 *****************************************************************************/
int rpp_xyz2luv_set_output_factors(rpp_xyz2luv_drv_t * drv, const uint8_t luma,
        const uint8_t chroma)
{
    uint32_t regs[2];

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    /* range-check the parameters. */
    if (
            REG_RANGE_CHECK(luma,   RPP_XYZ2LUV_XYZ2LUV_LUMA_OUT_FAC)   ||
            REG_RANGE_CHECK(chroma, RPP_XYZ2LUV_XYZ2LUV_CHROMA_OUT_FAC)
       )
    {
        return -ERANGE;
    }

    /* prepare the register cache. */
    regs[0] = luma;
    regs[1] = chroma;

    return WRITE_REGS(RPP_RPP_XYZ2LUV_LUMA_OUT_FAC_REG, regs, 2);
}

/******************************************************************************
 * rpp_xyz2luv_output_factors
 *****************************************************************************/
int rpp_xyz2luv_output_factors(rpp_xyz2luv_drv_t * drv, uint8_t * const luma,
        uint8_t * const chroma)
{
    uint32_t regs[2];
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !luma || !chroma )
    {
        return ( -EINVAL );
    }

    /* read and parse the configuration registers. */
    ret = READ_REGS(RPP_RPP_XYZ2LUV_LUMA_OUT_FAC_REG, regs, 2);
    if (!ret) {
        *luma   = REG_GET_SLICE(regs[0], RPP_XYZ2LUV_XYZ2LUV_LUMA_OUT_FAC);
        *chroma = REG_GET_SLICE(regs[1], RPP_XYZ2LUV_XYZ2LUV_CHROMA_OUT_FAC);
    }

    return ret;
}


