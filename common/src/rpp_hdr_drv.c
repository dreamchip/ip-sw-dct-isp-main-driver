/******************************************************************************
 *
 * Copyright 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_hdr_drv.c
 *
 * @brief   Implementation of RPP HDR pipeline unit driver.
 *
 *****************************************************************************/
#include <rpp_hdr_drv.h>

#define RPP_HDRREGS_BASE       0
#include <rpp_hdrregs_regs_addr_map.h>
#include <rpp_hdrregs_regs_mask.h>

#define RPP_IRQ_FUN_BASE       0
#include <rpp_irq_fun_regs_addr_map.h>
#include <rpp_irq_fun_regs_mask.h>

#define RPP_HDR_BASE           0
#include <rpp_hdr_module_base_addr.h>

#define RPP_HDR_FMU_BASE       0
#include <rpp_hdr_fmu_regs_addr_map.h>
#include <rpp_hdr_fmu_regs_mask.h>

#define DO_INIT_INDEX(name, base, index, drv_name)                     \
    if (!ret) {                                                        \
        ret = rpp_ ## name ## _init(dev, base, index, &drv->drv_name); \
    }

#define DO_INIT(name, base, drv_name)                                  \
    if (!ret) {                                                        \
        ret = rpp_ ## name ## _init(dev, base, &drv->drv_name);        \
    }


/******************************************************************************
 * local definitions
 *****************************************************************************/

static inline int check_valid(rpp_hdr_drv_t * drv)
{
    return ( drv && drv->dev && drv->version && drv->inputs );
}

/******************************************************************************
 * rpp_hdr_init
 *****************************************************************************/
int rpp_hdr_init(rpp_device * dev, uint32_t base, unsigned inputs, rpp_hdr_drv_t * drv)
{
    uint32_t v;
    int ret = 0;

    /* simple sanity check */
    if ( !dev || !drv )
    {
        return ( -EINVAL );
    }

    /* the base address is always zero, no need to store it. */
    drv->dev = dev;
    drv->base = base;
    drv->version = 0;
    drv->inputs = 0;

    /* range-check the parameter. */
    if ( (inputs < 2) || (inputs > 3) )
    {
        return -ERANGE;
    }

    /* read and parse the version register. */
    ret = READL(RPP_RPP_HDRREGS_VERSION_REG, &v);
    if (!ret) {
        drv->version = REG_GET_SLICE(v, RPP_HDRREGS_HDRREGS_VERSION);
        if (!drv->version) {
            ret = -ENODEV;
        }
    }

    /* call the initialization routines of all sub-units and pipeline stages. */
    DO_INIT_INDEX(main_pre, base + RPP_ADDR_BASE_HDR_MAIN_PRE_1, 0, main_pre1);
    DO_INIT_INDEX(main_pre, base + RPP_ADDR_BASE_HDR_MAIN_PRE_2, 1, main_pre2);
    if (inputs > 2) {
        DO_INIT_INDEX(main_pre, base + RPP_ADDR_BASE_HDR_MAIN_PRE_3, 2, main_pre3);
    }
    DO_INIT(rmap, base + RPP_ADDR_BASE_HDR_RMAP, rmap);
    DO_INIT(rmap_meas, base + RPP_ADDR_BASE_HDR_RMAP_MEAS, rmap_meas);
    DO_INIT(main_post, base + RPP_ADDR_BASE_HDR_MAIN_POST, main_post);
    DO_INIT(out, base + RPP_ADDR_BASE_HDR_OUT, out);
    DO_INIT(mv_out, base + RPP_ADDR_BASE_HDR_MV_OUT, mv_out);

    /* finally, when all initialization has completed successfully,
     * make the structure valid by setting the 'inputs' property. */
    if (!ret)
        drv->inputs = inputs;

    return ret;
}

/******************************************************************************
 * rpp_hdr_input_enable
 *****************************************************************************/
int rpp_hdr_input_enable(rpp_hdr_drv_t * drv, unsigned on)
{
    uint32_t v = 0;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return -EINVAL;
    }

    REG_SET_SLICE(v, RPP_HDRREGS_REGS_INFORM_EN, !!on);
    return WRITEL(RPP_RPP_HDR_INFORM_ENABLE_REG, v);
}


/******************************************************************************
 * rpp_hdr_input_enabled
 *****************************************************************************/
int rpp_hdr_input_enabled(rpp_hdr_drv_t * drv, unsigned * const on)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) || !on )
    {
        return -EINVAL;
    }

    /* read and parse the configuration register. */
    ret = READL(RPP_RPP_HDR_INFORM_ENABLE_REG, &v);
    if (!ret) {
        *on = REG_GET_SLICE(v, RPP_HDRREGS_REGS_INFORM_EN);
    }

    return ret;
}

/******************************************************************************
 * rpp_hdr_output_enable
 *****************************************************************************/
int rpp_hdr_output_enable(rpp_hdr_drv_t * drv, unsigned hv_on, unsigned mv_on)
{
    uint32_t v = 0;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return -EINVAL;
    }

    /* an effect is only gained if a "1" is written to the register
     * for the corresponding output port. Writing a "0" does nothing. */
    REG_SET_SLICE(v, RPP_HDRREGS_RPP_HV_OUT_ON, !!hv_on);
    REG_SET_SLICE(v, RPP_HDRREGS_RPP_MV_OUT_ON, !!mv_on);
    return WRITEL(RPP_RPP_HDR_OUT_IF_ON_REG, v);
}

/******************************************************************************
 * rpp_hdr_output_enable
 *****************************************************************************/
int rpp_hdr_output_disable(rpp_hdr_drv_t * drv, unsigned hv_off, unsigned mv_off)
{
    uint32_t v = 0;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return -EINVAL;
    }

    /* an effect is only gained if a "1" is written to the register
     * for the corresponding output port. Writing a "0" does nothing. */
    REG_SET_SLICE(v, RPP_HDRREGS_RPP_HV_OUT_OFF, !!hv_off);
    REG_SET_SLICE(v, RPP_HDRREGS_RPP_MV_OUT_OFF, !!mv_off);
    return WRITEL(RPP_RPP_HDR_OUT_IF_OFF_REG, v);
}

/******************************************************************************
 * rpp_hdr_shadow_update
 *****************************************************************************/
int rpp_hdr_shadow_update(rpp_hdr_drv_t * drv, unsigned force)
{
    uint32_t v = 0;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return -EINVAL;
    }

    /* depending on the force flag, set the corresponding bit
     * in the register. */
    if (force)
    {
        REG_SET_SLICE(v, RPP_HDRREGS_REGS_CFG_UPD,      1);
    } else {
        REG_SET_SLICE(v, RPP_HDRREGS_REGS_GEN_CFG_UPD,  1);
    }

    return WRITEL(RPP_RPP_HDR_UPD_REG, v);
}

/******************************************************************************
 * rpp_hdr_shadow_update
 *****************************************************************************/
int rpp_hdr_soft_reset(rpp_hdr_drv_t * drv, unsigned assert)
{
    uint32_t v = 0;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return -EINVAL;
    }

    /* check whether to set or clear the soft-reset flag. */
    if (assert)
    {
        REG_SET_SLICE(v, RPP_HDRREGS_SOFT_RESET, 1);
    }

    return WRITEL(RPP_RPP_HDR_SOFT_RESET_REG, v);
}

/******************************************************************************
 * rpp_hdr_set_fmu_safe_access_enable
 *****************************************************************************/
int rpp_hdr_set_fmu_safe_access_enable(rpp_hdr_drv_t * drv, unsigned on)
{
    uint32_t v = 0;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return -EINVAL;
    }

    REG_SET_SLICE(v, RPP_HDRREGS_SAFETY_ACCESS_PROTECTION, !!on);
    return WRITEL(RPP_ADDR_BASE_HDR_HDRREGS + RPP_RPP_HDR_SAFETY_ACCESS_PROTECTION_REG, v);
}

/******************************************************************************
 * rpp_hdr_get_fmu_safe_access_enable
 *****************************************************************************/
int rpp_hdr_get_fmu_safe_access_enable(rpp_hdr_drv_t * drv, unsigned * const on)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) || !on )
    {
        return -EINVAL;
    }

    /* read and parse the configuration register. */
    ret = READL(RPP_ADDR_BASE_HDR_HDRREGS + RPP_RPP_HDR_SAFETY_ACCESS_PROTECTION_REG, &v);
    if (!ret) {
        *on = REG_GET_SLICE(v, RPP_HDRREGS_SAFETY_ACCESS_PROTECTION);
    }

    return ret;
}

/******************************************************************************
 * rpp_hdr_irqflags_enable
 *****************************************************************************/
int rpp_hdr_irqflags_enable(rpp_hdr_drv_t * drv, rpp_hdr_irqflag_t const * const flags)
{
    rpp_hdr_irqflag_t v;

    /* simple sanity checks */
    if ( !check_valid(drv) || !flags )
    {
        return -EINVAL;
    }

    /* copy the original flags into a temporary variable */
    v.value     = flags->value;
    /* set the unused flags to zero. */
    v._unused   = 0;

    /* now, write the configuration register. */
    return WRITEL(RPP_RPP_ISM__0_REG, v.value);
}

/******************************************************************************
 * rpp_hdr_irqflags_enabled
 *****************************************************************************/
int rpp_hdr_irqflags_enabled(rpp_hdr_drv_t * drv, rpp_hdr_irqflag_t * const flags)
{
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !flags )
    {
        return -EINVAL;
    }

    /* read and parse the configuration register. */
    ret = READL(RPP_RPP_ISM__0_REG, &flags->value);
    if (!ret) {
        /* on success, clear the excessive flags which are not used. */
        flags->_unused = 0;
    }

    return ret;
}

/******************************************************************************
 * rpp_hdr_irqflags_enabled
 *****************************************************************************/
int rpp_hdr_irqflags_get(rpp_hdr_drv_t * drv, rpp_hdr_irqflag_t * const flags)
{
    int ret;

    /* simple sanity checks. */
    if ( !check_valid(drv) || !flags )
    {
        return -EINVAL;
    }

    /* now read and parse the masked interrupt status register */
    ret = READL(RPP_RPP_MIS__0_REG, &flags->value);

    /* if successful, clear the excessive bits and acknowledge the interrupts. */
    if (!ret) {
        flags->_unused = 0;
        ret = WRITEL(RPP_RPP_ISC__0_REG, flags->value);
    }

    return ret;
}

/******************************************************************************
 * rpp_hdr_fmuflags_enable
 *****************************************************************************/
int rpp_hdr_fmuflags_enable(rpp_hdr_drv_t * drv, rpp_hdr_fmuflag_t const * const flags)
{
    rpp_hdr_fmuflag_t v;

    /* simple sanity checks */
    if ( !check_valid(drv) || !flags )
    {
        return -EINVAL;
    }

    /* copy the original flags into a temporary variable */
    v.value     = flags->value;
    /* set the unused flags to zero. */
    v._unused   = 0;

    /* now, write the configuration register. */
    return WRITEL(RPP_ADDR_BASE_HDR_HDR_FMU + RPP_RPP_HDR_FMU_FSM__0_REG, v.value);
}

/******************************************************************************
 * rpp_hdr_fmuflags_enabled
 *****************************************************************************/
int rpp_hdr_fmuflags_enabled(rpp_hdr_drv_t * drv, rpp_hdr_fmuflag_t * const flags)
{
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !flags )
    {
        return -EINVAL;
    }

    /* read and parse the configuration register. */
    ret = READL(RPP_ADDR_BASE_HDR_HDR_FMU + RPP_RPP_HDR_FMU_FSM__0_REG, &flags->value);
    if (!ret) {
        /* on success, clear the excessive flags which are not used. */
        flags->_unused = 0;
    }

    return ret;
}

/******************************************************************************
 * rpp_hdr_fmuflags_get
 *****************************************************************************/
int rpp_hdr_fmuflags_get(rpp_hdr_drv_t * drv, rpp_hdr_fmuflag_t * const flags)
{
    int ret;

    /* simple sanity checks. */
    if ( !check_valid(drv) || !flags )
    {
        return -EINVAL;
    }

    /* now read and parse the masked interrupt status register */
    ret = READL(RPP_ADDR_BASE_HDR_HDR_FMU + RPP_RPP_HDR_FMU_MFS__0_REG, &flags->value);

    /* if successful, clear the excessive bits and acknowledge the interrupts. */
    if (!ret) {
        flags->_unused = 0;
        ret = WRITEL(RPP_ADDR_BASE_HDR_HDR_FMU + RPP_RPP_HDR_FMU_FSC__0_REG, flags->value);
    }

    return ret;
}
