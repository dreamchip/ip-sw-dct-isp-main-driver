/******************************************************************************
 *
 * Copyright 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_hist256_drv.c
 *
 * @brief   Implementation of 256 bin histogram measurement unit driver
 *
 *****************************************************************************/
#include <rpp_hist256_drv.h>

// make sure that the base is 0 in the auto-generated
// header-file, before including it
#define RPP_HIST256_BASE 0
#include <rpp_hist256_regs_addr_map.h>
#include <rpp_hist256_regs_mask.h>

/******************************************************************************
 * local definitions
 *****************************************************************************/

static inline int check_valid(const rpp_hist256_drv_t * drv)
{
    return (drv && drv->dev && drv->version);
}

/******************************************************************************
 * rpp_hist256_init
 *****************************************************************************/
int rpp_hist256_init(rpp_device * dev, uint32_t base, rpp_hist256_drv_t * drv)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !dev || !drv )
    {
        return ( -EINVAL );
    }
    drv->dev = dev;
    drv->base = base;
    drv->version = 0;

    /* read and parse the version register. */
    ret = READL(RPP_RPP_HIST256_VERSION_REG, &v);
    if (!ret) {
        drv->version = REG_GET_SLICE(v, RPP_HIST256_HIST256_VERSION);
        /* the only currently available version is 0x02 */
        if (!drv->version)
        {
            /* unsupported version */
            ret = -ENODEV;
        }
    }

    return ret;
}


/******************************************************************************
 * rpp_hist256_enable
 *****************************************************************************/
int rpp_hist256_enable(rpp_hist256_drv_t * drv,
        const unsigned enabled, const rpp_hist256_measure_channel_t channel)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) ) {
        return -EINVAL;
    }

    /* range-check... */
    if ( enabled && (channel > RPP_HIST256_MEASURE_CHANNEL_MAX) ) {
        return -ERANGE;
    }

    ret = READL(RPP_RPP_HIST256_MODE_REG, &v);
    if (!ret) {
        REG_SET_SLICE(v, RPP_HIST256_HIST256_MODE, !!enabled);
        ret = WRITEL(RPP_RPP_HIST256_MODE_REG, v);
    }
    if (!ret) {
        ret = READL(RPP_RPP_HIST256_CHANNEL_SEL_REG, &v);
    }
    if (!ret) {
        REG_SET_SLICE(v, RPP_HIST256_CHANNEL_SELECT, (enabled) ? channel : 0);
        ret = WRITEL(RPP_RPP_HIST256_CHANNEL_SEL_REG, v);
    }
    return ret;
}


/******************************************************************************
 * rpp_hist256_enabled
 *****************************************************************************/
int rpp_hist256_enabled(rpp_hist256_drv_t * drv,
        unsigned * const enabled, rpp_hist256_measure_channel_t * const channel)
{
    uint32_t v;
    int ret;

    /* simple sanity checks. */
    if ( !check_valid(drv) || !enabled || !channel )
    {
        return -EINVAL;
    }

    /* read the MODE register and check whether the unit is enabled. */
    ret = READL(RPP_RPP_HIST256_MODE_REG, &v);
    if (!ret)
    {
        *enabled = REG_GET_SLICE(v, RPP_HIST256_HIST256_MODE);
        /* also read the channel register now. */
        ret = READL(RPP_RPP_HIST256_CHANNEL_SEL_REG, &v);
    }
    if (!ret) {
        *channel = REG_GET_SLICE(v, RPP_HIST256_CHANNEL_SELECT);
    }
    return ret;
}

/******************************************************************************
 * rpp_hist256_set_meas_window
 *****************************************************************************/
int rpp_hist256_set_meas_window( rpp_hist256_drv_t * drv, const rpp_window_t * win)
{
    uint32_t regs[4];

    /* simple sanity checks */
    if ( !check_valid(drv) || !win )
    {
        return -EINVAL;
    }

    /* range checks... */
    if (
            REG_RANGE_CHECK(win->hoff,   RPP_HIST256_HIST256_H_OFFSET)  ||
            REG_RANGE_CHECK(win->voff,   RPP_HIST256_HIST256_V_OFFSET)  ||
            REG_RANGE_CHECK(win->width,  RPP_HIST256_HIST256_H_SIZE)    ||
            REG_RANGE_CHECK(win->height, RPP_HIST256_HIST256_V_SIZE)
       )
    {
        return -ERANGE;
    }

    regs[0] = win->hoff;
    regs[1] = win->voff;
    regs[2] = win->width;
    regs[3] = win->height;
    return WRITE_REGS(RPP_RPP_HIST256_H_OFFS_REG, regs, 4);
}


/******************************************************************************
 * rpp_hist256_meas_window
 *****************************************************************************/
int rpp_hist256_meas_window(rpp_hist256_drv_t * drv, rpp_window_t * const win)
{
    uint32_t regs[4];
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !win)
    {
        return -EINVAL;
    }

    /* read the window registers and extract the geometry */
    ret = READ_REGS(RPP_RPP_HIST256_H_OFFS_REG, regs, 4);
    if (!ret)
    {
        win->hoff   = REG_GET_SLICE(regs[0], RPP_HIST256_HIST256_H_OFFSET);
        win->voff   = REG_GET_SLICE(regs[1], RPP_HIST256_HIST256_V_OFFSET);
        win->width  = REG_GET_SLICE(regs[2], RPP_HIST256_HIST256_H_SIZE);
        win->height = REG_GET_SLICE(regs[3], RPP_HIST256_HIST256_V_SIZE);
    }

    return ret;
}


/******************************************************************************
 * rpp_hist256_set_sample_range
 *****************************************************************************/
int rpp_hist256_set_sample_range(rpp_hist256_drv_t * drv,
        const uint16_t scale, const uint32_t offset)
{
    uint32_t regs[2] = {0, };

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return -EINVAL;
    }

    /* range-check parameters... */
    if (
            REG_RANGE_CHECK(offset, RPP_HIST256_SAMPLE_OFFSET) ||
            REG_RANGE_CHECK(scale,  RPP_HIST256_SAMPLE_SCALE)
       )
    {
        return -ERANGE;
    }

    /* prepare registers for writing. */
    REG_SET_SLICE(regs[0], RPP_HIST256_SAMPLE_OFFSET,   offset);
    REG_SET_SLICE(regs[1], RPP_HIST256_SAMPLE_SCALE,    scale);
    return WRITE_REGS(RPP_RPP_HIST256_SAMPLE_OFFSET_REG, regs, 2);
}


/******************************************************************************
 * rpp_hist256_sample_range
 *****************************************************************************/
int rpp_hist256_sample_range(rpp_hist256_drv_t * drv,
        uint16_t * const scale, uint32_t * const offset)
{
    uint32_t regs[2];
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !scale || !offset )
    {
        return -EINVAL;
    }

    /* read the registers and extract values. */
    ret = READ_REGS(RPP_RPP_HIST256_SAMPLE_OFFSET_REG, regs, 2);
    if (!ret)
    {
        *offset = REG_GET_SLICE(regs[0], RPP_HIST256_SAMPLE_OFFSET);
        *scale  = REG_GET_SLICE(regs[1], RPP_HIST256_SAMPLE_SCALE);
    }
    return ret;
}

#define NR_DX_REGS NUM_REGS(RPP_HIST256_CURVE_POINTS - 1, RPP_HIST256_LOG_DX_2_SHIFT)

/******************************************************************************
 * rpp_hist256_set_curve
 *****************************************************************************/
int rpp_hist256_set_curve(rpp_hist256_drv_t * drv,
        const rpp_hist256_curve_t * const curve)
{
    unsigned i;
    uint32_t regs[NR_DX_REGS];
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) | !curve )
    {
        return -EINVAL;
    }

    /* range-check the curve... */
    if (curve->dx[0])
    {
        return -ERANGE;
    }
    for (i = 0; i < RPP_HIST256_CURVE_POINTS; ++i)
    {
        if (
                REG_RANGE_CHECK(curve->dx[i],   RPP_HIST256_LOG_DX_1) ||
                REG_RANGE_CHECK(curve->y[i],    RPP_HIST256_Y)
           )
        {
            return -ERANGE;
        }
    }

    /* Prepare register values. Pack DX values first.
     * Note that the first value is not programmed. */
    PACK_REGS(
            RPP_HIST256_CURVE_POINTS - 1,
            RPP_HIST256_LOG_DX_2_SHIFT,
            RPP_HIST256_LOG_DX_1_MASK,
            curve->dx + 1,
            regs
            );
    ret = WRITE_REGS(RPP_RPP_HIST256_LOG_DX_LO_REG, regs, NR_DX_REGS);

    if (!ret) {
        /* Write the y-axis values out as well. They have been
         * range-checked and are of appropriate datatype. */
        ret = WRITE_REGS(RPP_RPP_HIST256_Y__0_REG, curve->y, RPP_HIST256_CURVE_POINTS);
    }

    return ret;
}

/******************************************************************************
 * rpp_hist256_curve
 *****************************************************************************/
int rpp_hist256_curve(rpp_hist256_drv_t * drv, rpp_hist256_curve_t * const curve)
{
    uint32_t regs[NR_DX_REGS];
    int ret;

    /* simple sanity checks. */
    if ( !check_valid(drv) || !curve )
    {
        return -EINVAL;
    }

    /* read and unpack the DX array values. */
    ret = READ_REGS(RPP_RPP_HIST256_LOG_DX_LO_REG, regs, NR_DX_REGS);
    if (!ret) {
        UNPACK_REGS(
                RPP_HIST256_CURVE_POINTS - 1,
                RPP_HIST256_LOG_DX_2_SHIFT,
                RPP_HIST256_LOG_DX_1_MASK,
                regs,
                curve->dx + 1
                );
        /* make sure that the first DX value is zero,
         * as required by the driver. */
        curve->dx[0] = 0;
        /* also read the Y array values now. */
        ret = READ_REGS(RPP_RPP_HIST256_Y__0_REG, curve->y, RPP_HIST256_CURVE_POINTS);
    }
    if (!ret) {
        unsigned i;
        /* if the Y values were read successfully, mask the
         * returned values properly. */
        for (i = 0; i < RPP_HIST256_CURVE_POINTS; ++i) {
            curve->y[i] &= RPP_HIST256_Y_MASK;
        }
    }
    return ret;
}


/******************************************************************************
 * rpp_hist256_gamma_enable
 *****************************************************************************/
int rpp_hist256_gamma_enable(rpp_hist256_drv_t * drv, unsigned on)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return -EINVAL;
    }

    /* now read and modify the corresponding register. */
    ret = READL(RPP_RPP_HIST256_LOG_ENABLE_REG, &v);
    if (!ret) {
        REG_SET_SLICE(v, RPP_HIST256_HIST256_LOG_EN, !!on);
        ret = WRITEL(RPP_RPP_HIST256_LOG_ENABLE_REG, v);
    }

    return ret;
}

/******************************************************************************
 * rpp_hist256_gamma_enabled
 *****************************************************************************/
int rpp_hist256_gamma_enabled(rpp_hist256_drv_t * drv, unsigned * const on)
{
    uint32_t v;
    int ret;

    /* simple sanity checks. */
    if ( !check_valid(drv) || !on )
    {
        return -EINVAL;
    }

    /* read and parse the corresponding register. */
    ret = READL(RPP_RPP_HIST256_LOG_ENABLE_REG, &v);
    if (!ret) {
        *on = REG_GET_SLICE(v, RPP_HIST256_HIST256_LOG_EN);
    }

    return ret;
}

