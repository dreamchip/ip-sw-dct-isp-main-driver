/******************************************************************************
 *
 * Copyright 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_main_post_drv.c
 *
 * @brief   Implementation of RPP Post-Fusion Processing pipeline unit driver
 *
 *****************************************************************************/
#include <rpp_main_post_drv.h>

#define RPP_MAIN_POST_BASE  (0)
#include <rpp_main_post_module_base_addr.h>


/******************************************************************************
 * local definitions
 *****************************************************************************/

#define DO_INIT(mod, base)                                  \
    if (!ret) {                                             \
        ret = rpp_ ## mod ## _init(dev, base, &(drv->mod)); \
    }

/******************************************************************************
 * rpp_main_post_init
 *****************************************************************************/
int rpp_main_post_init(rpp_device * dev, uint32_t base, rpp_main_post_drv_t * drv)
{
    int ret = 0;

    /* simple sanity check */
    if ( !dev || !drv )
    {
        return ( -EINVAL );
    }

    /* call the initialization routines of all sub-units. */
    DO_INIT(awb_gain,   base + RPP_ADDR_BASE_MAIN_POST_AWB_GAIN);
    DO_INIT(filt,       base + RPP_ADDR_BASE_MAIN_POST_FILT);
    DO_INIT(cac,        base + RPP_ADDR_BASE_MAIN_POST_FILT);
    DO_INIT(ccor,       base + RPP_ADDR_BASE_MAIN_POST_CCOR);
    DO_INIT(hist,       base + RPP_ADDR_BASE_MAIN_POST_HIST);
    DO_INIT(ltm,        base + RPP_ADDR_BASE_MAIN_POST_LTM);
    DO_INIT(ltm_meas,   base + RPP_ADDR_BASE_MAIN_POST_LTM_MEAS);
    DO_INIT(awb_meas,   base + RPP_ADDR_BASE_MAIN_POST_AWB_MEAS);
    DO_INIT(rgbdenoise, base + RPP_ADDR_BASE_MAIN_POST_RGBDENOISE);
    DO_INIT(shrpcnr,    base + RPP_ADDR_BASE_MAIN_POST_SHRPCNR);
    return ret;
}

/******************************************************************************
 * rpp_main_post_base_setup
 *****************************************************************************/
int rpp_main_post_base_setup(
        rpp_main_post_drv_t *               drv,
        rpp_ccor_profile_t const * const    ccor,
        rpp_ccor_profile_t const * const    rgb2yuv,
        rpp_ccor_profile_t const * const    yuv2rgb,
        uint32_t                            width
        )
{
    int ret = 0;

    /* simple sanity checks. */
    if ( !drv || !ccor || !rgb2yuv || !yuv2rgb || !width )
    {
        return -EINVAL;
    }

    if (!ret)
        ret = rpp_ccor_set_profile(&drv->ccor, ccor);
    if (!ret)
        ret = rpp_rgbdenoise_set_profile(&drv->rgbdenoise, rgb2yuv);
    if (!ret)
        ret = rpp_shrpcnr_set_profile(&drv->shrpcnr, yuv2rgb);
    if (!ret)
        ret = rpp_ltm_set_linewidth(&drv->ltm, width);
    if (!ret)
        ret = rpp_shrpcnr_set_linewidth(&drv->shrpcnr, width);
    return ret;
}

