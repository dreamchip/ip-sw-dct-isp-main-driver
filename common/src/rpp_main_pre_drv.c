/******************************************************************************
 *
 * Copyright 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_main_pre_drv.c
 *
 * @brief   Implementation of RPP pre-fusion pipeline unit driver.
 *
 *****************************************************************************/
#include <rpp_main_pre_drv.h>

#define RPP_MAIN_PRE_1_BASE  (0)
#include <rpp_main_pre_1_module_base_addr.h>


/******************************************************************************
 * local definitions
 *****************************************************************************/

#define DO_INIT(mod, base)                                  \
    if (!ret) {                                             \
        ret = rpp_ ## mod ## _init(dev, base, &(drv->mod)); \
    }


/******************************************************************************
 * rpp_main_prex_init
 *
 * generic function for main_pre_2/main_pre_3 pipeline stages which receives
 * the base address as argument.
 *****************************************************************************/
int rpp_main_pre_init(rpp_device * dev, uint32_t base, uint32_t index, rpp_main_pre_drv_t * drv)
{
    int ret = 0;

    /* simple sanity check */
    if ( !dev || !drv )
    {
        return ( -EINVAL );
    }

    /* call the initialization routines of all sub-units. */

    DO_INIT(acq,        base + RPP_ADDR_BASE_MAIN_PRE_1_ACQ);
    DO_INIT(bls,        base + RPP_ADDR_BASE_MAIN_PRE_1_BLS);
    DO_INIT(gamma_in,   base + RPP_ADDR_BASE_MAIN_PRE_1_GAMMA_IN);
    DO_INIT(lsc4,       base + RPP_ADDR_BASE_MAIN_PRE_1_LSC4);
    DO_INIT(awb_gain,   base + RPP_ADDR_BASE_MAIN_PRE_1_AWB_GAIN);
    DO_INIT(dpcc,       base + RPP_ADDR_BASE_MAIN_PRE_1_DPCC);
    DO_INIT(dpf,        base + RPP_ADDR_BASE_MAIN_PRE_1_DPF);
    DO_INIT(hist,       base + RPP_ADDR_BASE_MAIN_PRE_1_HIST);
    DO_INIT(exm,        base + RPP_ADDR_BASE_MAIN_PRE_1_EXM);
    if (index == 0) {
        DO_INIT(hist256,base + RPP_ADDR_BASE_MAIN_PRE_1_HIST256);
    }

    return ret;
}


