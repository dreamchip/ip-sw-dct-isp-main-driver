/******************************************************************************
 *
 * Copyright 2019 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_gamma_out_drv.c
 *
 * @brief   Implementation of gamma-out driver
 *
 *****************************************************************************/
#include <rpp_gamma_out_drv.h>

// make sure that the base is 0 in the auto-generated
// header-file, before including it
//
// Note: the GAMMA_OUT register offsets in gamma_out, rpp_out and rpp_mv_out
// are all the same, so only one of these header groups needs to be included.
// Additionally, register slices are the same except for the Y-axis values,
// which are color component values as wide as 'colorbits' indicates. This
// can be handled programmatically.
#define RPP_GAMMA_OUT_BASE 0
#include <rpp_gamma_out_regs_addr_map.h>
#include <rpp_gamma_out_regs_mask.h>

/******************************************************************************
 * local definitions
 *****************************************************************************/

static inline int check_valid(const rpp_gamma_out_drv_t * drv)
{
    return (drv && drv->dev && drv->colorbits);
}


/******************************************************************************
 * rpp_gamma_out_init
 *****************************************************************************/
int rpp_gamma_out_init(rpp_device * dev, uint32_t base, rpp_gamma_out_drv_t * drv)
{
    uint32_t v;
    int ret;

    /* simple base address check */
    if ( !dev || !drv )
    {
        return ( -EINVAL );
    }

    drv->dev = dev;
    drv->base = base;
    drv->version = 0;
    drv->colorbits = 0;

    /* check that rpp-bls module exists */
    ret = READL(RPP_RPP_GAMMA_OUT_VERSION_REG, &v );
    if (!ret)
    {
        drv->version = REG_GET_SLICE(v, RPP_GAMMA_OUT_GAMMA_OUT_VERSION);
        /* check whether the register indicates a supported version. */
        switch(drv->version)
        {
            case 0x01:
                drv->colorbits = 12;
                break;
            case 0x02:
                drv->colorbits = 24;
                break;
            default:
                ret = -ENODEV;
        }
    }

    return ret;
}

/******************************************************************************
 * rpp_gamma_out_enable
 *****************************************************************************/
int rpp_gamma_out_enable(rpp_gamma_out_drv_t * drv, const unsigned on)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    ret = READL(RPP_RPP_GAMMA_OUT_ENABLE_REG, &v);
    if (!ret)
    {
        REG_SET_SLICE(v, RPP_GAMMA_OUT_GAMMA_OUT_EN, on ? 1u : 0u );
        ret = WRITEL(RPP_RPP_GAMMA_OUT_ENABLE_REG, v);
    }

    return ret;
}

/******************************************************************************
 * rpp_gamma_out_enabled
 *****************************************************************************/
int rpp_gamma_out_enabled(rpp_gamma_out_drv_t * drv, unsigned * const on)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) || !on )
    {
        return ( -EINVAL );
    }

    ret = READL(RPP_RPP_GAMMA_OUT_ENABLE_REG, &v);
    if (!ret)
    {
        *on = REG_GET_SLICE(v, RPP_GAMMA_OUT_GAMMA_OUT_EN);
    }

    return ret;
}

/******************************************************************************
 * rpp_gamma_out_set_curve
 *****************************************************************************/
int rpp_gamma_out_set_curve
(
    rpp_gamma_out_drv_t *               drv,
    const rpp_gamma_out_curve_t * const curve
)
{
    uint32_t v;
    unsigned i;
    uint32_t cmax;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !curve )
    {
        return ( -EINVAL );
    }

    /* range check */
    if ( !(curve->mode < RPP_GAMMA_OUT_X_MODE_MAX) )
    {
        return ( -ERANGE );
    }

    /* calculate maximum Y-axis value */
    cmax = UNSIGNED_MAX(drv->colorbits);

    for ( i = 0; i < GAMMA_OUT_NUM_SAMPLES; ++i )
    {
        /* range check */
        if (curve->y[i] > cmax)
        {
            return ( -ERANGE );
        }
    }

    /* set mode */
    ret = READL(RPP_RPP_GAMMA_OUT_MODE_REG, &v);
    if (!ret)
    {
        /* the curve->mode argument has been range-checked and can be
         * copied to the register slice without any twiddling. */
        REG_SET_SLICE( v, RPP_GAMMA_OUT_GAMMA_OUT_EQU_SEGM, curve->mode);
        ret = WRITEL( RPP_RPP_GAMMA_OUT_MODE_REG, v);
    }

    /* if successful, program Y-axis values. */
    if (!ret)
    {
        /* The y-values have been range-checked, are unsigned and there are
         * no other slices in these registers. The data can be written to
         * the unit without modifications using efficient multiple-register-writes. */
        ret = WRITE_REGS(RPP_RPP_GAMMA_OUT_Y__0_REG, curve->y, GAMMA_OUT_NUM_SAMPLES);
    }

    return ret;
}

/******************************************************************************
 * rpp_gamma_out_curve
 *****************************************************************************/
int rpp_gamma_out_curve
(
    rpp_gamma_out_drv_t *           drv,
    rpp_gamma_out_curve_t * const   curve
)
{
    uint32_t v;
    unsigned i;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !curve )
    {
        return ( -EINVAL );
    }

    /* get mode */
    ret = READL( RPP_RPP_GAMMA_OUT_MODE_REG, &v );
    if (!ret)
    {
        curve->mode = REG_GET_SLICE(v, RPP_GAMMA_OUT_GAMMA_OUT_EQU_SEGM);

        /* read Y-axis values directly into the supplied structure. */
        ret = READ_REGS(RPP_RPP_GAMMA_OUT_Y__0_REG, curve->y, GAMMA_OUT_NUM_SAMPLES);
    }

    /* if successful, mask the Y-axis values. */
    if (!ret)
    {
        uint32_t cmask = UNSIGNED_MAX(drv->colorbits);
        for (i = 0; i < GAMMA_OUT_NUM_SAMPLES; ++i)
        {
            curve->y[i] = curve->y[i] & cmask;
        }
    }

    return ret;
}

