/******************************************************************************
 *
 * Copyright 2019 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_bls_drv.c
 *
 * @brief   Implementation of blacklevel measurment and substraction driver
 *
 *****************************************************************************/
#include <rpp_bls_drv.h>

// make sure that the base is 0 in the auto-generated
// header-file, before including it
#define RPP_BLS24_BASE 0
#include <rpp_bls24_regs_addr_map.h>
#include <rpp_bls24_regs_mask.h>

/******************************************************************************
 * local definitions
 *****************************************************************************/

static inline int check_valid(const rpp_bls_drv_t * drv)
{
    return (drv && drv->dev && drv->colorbits);
}



/******************************************************************************
 * rpp_bls_init
 *****************************************************************************/
int rpp_bls_init(rpp_device * dev, uint32_t base, rpp_bls_drv_t * drv)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !dev || !drv )
    {
        return ( -EINVAL );
    }

    /* initialize the driver structure */
    drv->dev = dev;
    drv->base = base;
    drv->version = 0;
    drv->colorbits = 0;

    /* check that rpp bls module exists by reading the version register. */
    ret = READL(RPP_RPP_BLS_VERSION_REG, &v);

    if (!ret)
    {
        drv->version = REG_GET_SLICE(v, RPP_BLS_BLS_VERSION);

        /* check the version information to get the bitwidth. */
        switch(drv->version) {
            case 3:
            case 5:
                /* an instance supporting colorbits==12 */
                drv->colorbits = 12;
                break;
            case 2:
            case 4:
                /* an instance supporting colorbits==20 */
                drv->colorbits = 20;
                break;
            case 6:
                /* an instance supporting colorbits==24 */
                drv->colorbits = 24;
                break;
            default:
                /* unsupported version. */
                ret = -ENODEV;
        }
    }
    
    return ret;
}

/******************************************************************************
 * rpp_bls_enable
 *****************************************************************************/
int rpp_bls_enable(rpp_bls_drv_t * drv, const unsigned on)
{
    uint32_t v;
    int ret;

    /* simple base address check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    ret = READL(RPP_RPP_BLS_CTRL_REG, &v);
    if (!ret)
    {
        REG_SET_SLICE(v, RPP_BLS_BLS_EN, !!on);
        ret = WRITEL(RPP_RPP_BLS_CTRL_REG, v);
    }
    return ret;
}

/******************************************************************************
 * rpp_bls_enabled
 *****************************************************************************/
int rpp_bls_enabled(rpp_bls_drv_t * drv, unsigned * const on)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !on )
    {
        return ( -EINVAL );
    }

    ret = READL(RPP_RPP_BLS_CTRL_REG, &v);
    if (!ret)
    {
        *on      = REG_GET_SLICE(v, RPP_BLS_BLS_EN);
    }

    return ret;
}

/******************************************************************************
 * rpp_bls_set_black_level
 *****************************************************************************/
int rpp_bls_set_black_level(rpp_bls_drv_t * drv,
        const int32_t a, const int32_t b, const int32_t c, const int32_t d)
{
    int32_t cmin, cmax;
    uint32_t mask;
    uint32_t regs[4];

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    cmin = SIGNED_MIN(drv->colorbits + 1);
    cmax = SIGNED_MAX(drv->colorbits + 1);
    mask = UNSIGNED_MAX(drv->colorbits + 1);

    /* check value range */
    if ( (a > cmax) || (a < cmin) ||
         (b > cmax) || (b < cmin) ||
         (c > cmax) || (c < cmin) ||
         (d > cmax) || (d < cmin) )
    {
        return ( -ERANGE );
    }

    /* The fixed blacklevel registers are adjacent in the register
     * space without any additional slices included, so that efficient
     * multiple register writes can be used here. */
    regs[0] = a & mask;
    regs[1] = b & mask;
    regs[2] = c & mask;
    regs[3] = d & mask;
    return WRITE_REGS(RPP_RPP_BLS_A_FIXED_REG, regs, 4);
}

/******************************************************************************
 * rpp_bls_get_black_level
 *****************************************************************************/
int rpp_bls_get_black_level(rpp_bls_drv_t * drv,
        int32_t * const a, int32_t * const b, int32_t * const c, int32_t * const d)
{
    uint32_t regs[4];
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !a || !b || !c || !d )
    {
        return ( -EINVAL );
    }

    /* Get blacklevel values from module registers. These registers
     * are adjacent in register space without any additional slices
     * included, so that efficient multiple register reads can be
     * used here. */

    ret = READ_REGS(RPP_RPP_BLS_A_FIXED_REG, regs, 4);

    if (!ret)
    {
        /* The blacklevel values are signed, therefore there's one more bit
         * in the bitfield than required for color values. */
        const int bits = drv->colorbits + 1;
        const uint32_t mask = UNSIGNED_MAX(bits);

        *a = sign_extend(regs[0] & mask, bits);
        *b = sign_extend(regs[1] & mask, bits);
        *c = sign_extend(regs[2] & mask, bits);
        *d = sign_extend(regs[3] & mask, bits);
    }

    return ret;
}

/******************************************************************************
 * rpp_bls_get_measured_black_level
 *****************************************************************************/
int rpp_bls_get_measured_black_level(rpp_bls_drv_t * drv,
        int32_t * const a, int32_t * const b, int32_t * const c, int32_t * const d)
{
    uint32_t regs[4];
    int ret;

    /* simple base address check */
    if ( !check_valid(drv) || !a || !b || !c || !d )
    {
        return ( -EINVAL );
    }

    /* The measured blacklevels are always positive because they are computed
     * as the average value of strictly non-negative color samples. This
     * makes things a little easier.
     *
     * Again, use efficient multiple register reads for adjacent registers. */
    ret = READ_REGS(RPP_RPP_BLS_A_MEASURED_REG, regs, 4);

    if (!ret)
    {
        const uint32_t mask = SIGNED_MAX(drv->colorbits + 1);
        *a = regs[0] & mask;
        *b = regs[1] & mask;
        *c = regs[2] & mask;
        *d = regs[3] & mask;
    }

    return ret;
}

/******************************************************************************
 * rpp_bls_verify_window_size
 * note: no check with input resolution, has to be done in middle ware
 *****************************************************************************/
static int rpp_bls_verify_window_size(const rpp_window_t wnd)
{
    if (
            // sanity check: there must be one or more full bayer cluster(s)
            // which means that width and height must be even and >0
            (wnd.width  < 2) || (wnd.width  & 1u) ||
            (wnd.height < 2) || (wnd.height & 1u) ||
            // range check
            (wnd.hoff                     > RPP_BLS_BLS_H1_START_MASK) ||
            (wnd.voff                     > RPP_BLS_BLS_V1_START_MASK) ||
            ((wnd.hoff + wnd.width  - 1u) > RPP_BLS_BLS_H1_STOP_MASK)  ||
            ((wnd.voff + wnd.height - 1u) > RPP_BLS_BLS_V1_STOP_MASK)  )
    {
        return ( -ERANGE );
    }

    return ( 0 );
}

/******************************************************************************
 * rpp_bls_verify_windows
 *****************************************************************************/
int rpp_bls_verify_windows
(
    const unsigned          num,
    const rpp_window_t *    wnd,
    uint32_t * const        exponent
)
{
    uint32_t n;

    int res;

    /* parameter check */
    if ( (num > RPP_BLS_MAX_WINDOWS) || !wnd || !exponent )
    {
        return ( -EINVAL );
    }

    /* check size of first window */
    res = rpp_bls_verify_window_size( wnd[0u] );
    if ( res )
    {
        return ( res );
    }

    /* second window ? */
    if ( num > 1u )
    {
        /* check size of second window */
        res = rpp_bls_verify_window_size( wnd[1u] );
        if ( res )
        {
            return ( res );
        }
    }

    /* Check number of samples which is equal to the
     * divider. Since each Bayer component is counted
     * separately, the divider is equal to one fourth
     * of the total number of pixels. */
    n = (wnd[0].width * wnd[0].height) >> 2u;

    /* second window ? */
    if ( num > 1u )
    {
        n += (wnd[1].width * wnd[1].height) >> 2u;
    }

    /* check maximum sample count. */
    if ( n >  (1u << RPP_BLS_MAX_EXPONENT) )
    {
        return ( -ERANGE );
    }

    /* Check whether the number of samples is a power-of-two. */
    if ( n & (n-1) )
    {
        return ( -ERANGE );
    }

    /* compute the expoent now. */
    *exponent = log2i(n);

    return ( 0 );
}

/******************************************************************************
 * rpp_bls_set_measurement_window
 *****************************************************************************/
int rpp_bls_set_measurement_window
(
    rpp_bls_drv_t *         drv,
    const unsigned          num,
    const rpp_window_t *    wnd,
    const uint32_t          exponent
)
{
    /* initial value for window coordinate registers. */
    uint32_t regs[8] = {0, 0, 0, 0, 0, 0, 0, 0};
    /* variable holding the control register value. */
    uint32_t v;
    int ret;

    /* parameter check */
    if ( !check_valid(drv) || (num > RPP_BLS_MAX_WINDOWS) || (exponent > RPP_BLS_MAX_EXPONENT) )
    {
        return ( -EINVAL );
    }

    /* read control register to enable or disable measurement windows */
    ret = READL(RPP_RPP_BLS_CTRL_REG, &v);
    if (ret)
    {
        return ret;
    }

    /* if no windows are given, clear measurement windows. */
    if ( !num )
    {
        /* use efficient multiple register writes here. */
        ret = WRITE_REGS(RPP_RPP_BLS_H1_START_REG, regs, 8);

        if (!ret)
        {
            ret = WRITEL(RPP_RPP_BLS_SAMPLES_REG, 0);
        }

        if (!ret)
        {
            /* disable measurement windows */
            REG_SET_SLICE( v, RPP_BLS_BLS_WIN_EN, 0u );
            ret = WRITEL(RPP_RPP_BLS_CTRL_REG, v);
        }
        return ret;
    }

    /* parameter check */
    if ( !wnd )
    {
        return ( -EINVAL );
    }

    /* set first window, must always be present. */
    /* window size check */
    ret = rpp_bls_verify_window_size( wnd[0] );
    REG_SET_SLICE(regs[0], RPP_BLS_BLS_H1_START, wnd[0].hoff);
    REG_SET_SLICE(regs[1], RPP_BLS_BLS_H1_STOP,  wnd[0].hoff + wnd[0].width - 1u);
    REG_SET_SLICE(regs[2], RPP_BLS_BLS_V1_START, wnd[0].voff);
    REG_SET_SLICE(regs[3], RPP_BLS_BLS_V1_STOP,  wnd[0].voff + wnd[0].height - 1u);
    if ( (num > 1u) && !ret )
    {
            /* window size check */
            ret = rpp_bls_verify_window_size( wnd[1] );
            REG_SET_SLICE(regs[0], RPP_BLS_BLS_H2_START, wnd[1].hoff);
            REG_SET_SLICE(regs[1], RPP_BLS_BLS_H2_STOP,  wnd[1].hoff + wnd[1].width - 1u);
            REG_SET_SLICE(regs[2], RPP_BLS_BLS_V2_START, wnd[1].voff);
            REG_SET_SLICE(regs[3], RPP_BLS_BLS_V2_STOP,  wnd[1].voff + wnd[1].height - 1u);
    }
    /* write the window coordinate registers now. */
    if (!ret)
    {
        ret = WRITE_REGS(RPP_RPP_BLS_H1_START_REG, regs, 8);
    }
    /* program measured samples exponent. */
    if (!ret)
    {
        ret = WRITEL(RPP_RPP_BLS_SAMPLES_REG, exponent);
    }

    /* enable measurment windows */
    if (!ret)
    {
        REG_SET_SLICE( v, RPP_BLS_BLS_WIN_EN, (num == 1) ? 1u : 3u );
        ret = WRITEL(RPP_RPP_BLS_CTRL_REG, v);
    }

    return ret;
}

