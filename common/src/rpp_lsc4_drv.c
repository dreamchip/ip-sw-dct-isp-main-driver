/******************************************************************************
 *
 * Copyright 2018 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_lsc4_drv.c
 *
 * @brief   Implementation of input lense shade correction driver
 *
 *****************************************************************************/
#include <rpp_lsc4_drv.h>

// make sure that the base is 0 in the auto-generated
// header-file, before including it
#define RPP_LSC4_BASE 0
#include <rpp_lsc4_regs_addr_map.h>
#include <rpp_lsc4_regs_mask.h>

/******************************************************************************
 * local definitions
 *****************************************************************************/

/* these cannot be inferred from the SIG-generated files. */

#define RPP_LSC_TABLE_SAMPLE_MASK     ( 0x00000fffu )
#define RPP_LSC_TABLE_SAMPLE_SHIFT    ( 12u )

static inline int check_valid(const rpp_lsc4_drv_t * drv)
{
    return (drv && drv->dev && drv->version);
}

/******************************************************************************
 * rpp_lsc4_init
 *****************************************************************************/
int rpp_lsc4_init(rpp_device * dev, uint32_t base, rpp_lsc4_drv_t * drv)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !dev || !drv )
    {
        return ( -EINVAL );
    }

    drv->dev = dev;
    drv->base = base;
    drv->version = 0;

    /* read and parse the version register. */
    ret = READL(RPP_RPP_LSC_VERSION_REG, &v);
    if (!ret) {
        drv->version = REG_GET_SLICE(v, RPP_LSC_LSC_VERSION);
        /* currently, there's only one version supported that
         * has the full mesh definition, e.g. supports
         * 16 [xy]_size/grad points in the register set. */
        if (drv->version != 0x04)
            ret = -ENODEV;
    }

    return ret;
}

/******************************************************************************
 * rpp_lsc4_enable
 *****************************************************************************/
int rpp_lsc4_enable(rpp_lsc4_drv_t * drv, const unsigned on)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    /* program the control register to activate/deactivate the unit
     * for the next frame. */
    ret = READL(RPP_RPP_LSC_CTRL_REG, &v);
    if (!ret) {
        REG_SET_SLICE( v, RPP_LSC_LSC_EN, on ? 1u : 0u );
        ret = WRITEL(RPP_RPP_LSC_CTRL_REG, v);
    }

    return ret;
}

/******************************************************************************
 * rpp_lsc4_enabled
 *****************************************************************************/
int rpp_lsc4_enabled(rpp_lsc4_drv_t * drv, unsigned * const on)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !on )
    {
        return ( -EINVAL );
    }

    ret = READL(RPP_RPP_LSC_CTRL_REG, &v);
    if (!ret) {
        *on = REG_GET_SLICE( v, RPP_LSC_LSC_EN );
    }

    return ret;
}

/******************************************************************************
 * rpp_lsc4_active
 *****************************************************************************/
int rpp_lsc4_active(rpp_lsc4_drv_t * drv, unsigned * const on)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !on )
    {
        return ( -EINVAL );
    }

    ret = READL(RPP_RPP_LSC_STATUS_REG, &v);
    if (!ret) {
        *on = REG_GET_SLICE( v, RPP_LSC_LSC_EN_STATUS );
    }

    return ret;
}


/******************************************************************************
 * rpp_lsc4_set_table_id
 *****************************************************************************/
int rpp_lsc4_set_table_id(rpp_lsc4_drv_t * drv, const unsigned table_id)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    /* check the supplied table argument */
    if (! (table_id < RPP_LSC4_NUM_TABLES))
    {
        return -ERANGE;
    }

    /* update the table selection register. */
    ret = READL(RPP_RPP_LSC_TABLE_SEL_REG, &v);
    if (!ret)
    {
        REG_SET_SLICE(v, RPP_LSC_TABLE_SEL, table_id);
        ret = WRITEL(RPP_RPP_LSC_TABLE_SEL_REG, v);
    }

    return ret;
}


/******************************************************************************
 * rpp_lsc4_next_table_id
 *****************************************************************************/
int rpp_lsc4_next_table_id(rpp_lsc4_drv_t * drv, unsigned * const table_id)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !table_id )
    {
        return ( -EINVAL );
    }

    /* read the table selection register. */
    ret = READL(RPP_RPP_LSC_TABLE_SEL_REG, &v);
    if (!ret)
    {
        *table_id = REG_GET_SLICE( v, RPP_LSC_TABLE_SEL );
    }

    return ret;
}

/******************************************************************************
 * rpp_lsc4_active_table_id
 *****************************************************************************/
int rpp_lsc4_active_table_id(rpp_lsc4_drv_t * drv, unsigned * const table_id)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !table_id )
    {
        return ( -EINVAL );
    }

    /* read the table selection register. */
    ret = READL(RPP_RPP_LSC_STATUS_REG, &v);
    if (!ret)
    {
        *table_id = REG_GET_SLICE( v, RPP_LSC_ACTIVE_TABLE);
    }

    return ret;
}

#define NR_TABLE_REGS NUM_REGS(RPP_LSC4_TABLE_WIDTH, RPP_LSC_TABLE_SAMPLE_SHIFT)

static int program_table(
        rpp_lsc4_drv_t * drv,
        uint32_t    addr_reg,
        uint32_t    data_reg,
        uint32_t    addr_val,
        const uint16_t *  values
)
{
    unsigned i;
    int ret;

    /* Write indirect RAM address register. */
    ret = WRITEL(addr_reg, addr_val);
    if (ret)
    {
        return ret;
    }

    /* loop over all table lines. */
    for (i = 0; !ret && (i < RPP_LSC4_TABLE_HEIGHT); ++i) {
        /* program each line individually to conserve
         * stack memory. */
        uint32_t regs[NR_TABLE_REGS];

        /* pack the values into the registers as needed. */
        PACK_REGS(RPP_LSC4_TABLE_WIDTH,RPP_LSC_TABLE_SAMPLE_SHIFT,RPP_LSC_TABLE_SAMPLE_MASK,values,regs);
        /* increment the data pointer for the next round. */
        values += RPP_LSC4_TABLE_WIDTH;

        /* write the RAM cell values to the indirect RAM data register. */
        ret = WRITE_RAM(data_reg, regs, NR_TABLE_REGS);
    }
    return ret;
}

/******************************************************************************
 * rpp_lsc4_set_table
 *****************************************************************************/
int rpp_lsc4_set_table
(
    rpp_lsc4_drv_t *            drv,
    const unsigned              table_id,
    const rpp_lsc4_values_t * const   values
)
{
    int ret = 0;
    unsigned i;
    uint32_t v;

    /* simple sanity checks */
    if ( !check_valid(drv) || !values )
    {
        return ( -EINVAL );
    }

    if ( !(table_id < RPP_LSC4_NUM_TABLES) )
    {
        return -ERANGE;
    }

    /* table data range check */
    for ( i = 0u; i < RPP_LSC4_TABLE_SIZE; ++i)
    {
        if ( ( values->r[i]  & ~RPP_LSC_TABLE_SAMPLE_MASK) ||
             ( values->gr[i] & ~RPP_LSC_TABLE_SAMPLE_MASK) ||
             ( values->b[i]  & ~RPP_LSC_TABLE_SAMPLE_MASK) ||
             ( values->gb[i] & ~RPP_LSC_TABLE_SAMPLE_MASK)
             )
        {
            return ( -ERANGE );
        }
    }

    /* compute the indirect RAM address from the table_id */
    v = 0;
    REG_SET_SLICE( v, RPP_LSC_R_RAM_ADDR, (table_id == 0u) ? 0u : 153u );

    /* now, program the correction table values. */
    if (!ret)
    {
        ret = program_table(drv, RPP_RPP_LSC_R_TABLE_ADDR_REG , RPP_RPP_LSC_R_TABLE_DATA_REG , v, values->r );
    }
    if (!ret)
    {
        ret = program_table(drv, RPP_RPP_LSC_GR_TABLE_ADDR_REG, RPP_RPP_LSC_GR_TABLE_DATA_REG, v, values->gr);
    }
    if (!ret)
    {
        ret = program_table(drv, RPP_RPP_LSC_B_TABLE_ADDR_REG , RPP_RPP_LSC_B_TABLE_DATA_REG , v, values->b );
    }
    if (!ret)
    {
        ret = program_table(drv, RPP_RPP_LSC_GB_TABLE_ADDR_REG, RPP_RPP_LSC_GB_TABLE_DATA_REG, v, values->gb);
    }

    return ret;
}


static int read_table(
        rpp_lsc4_drv_t * drv,
        uint32_t    addr_reg,
        uint32_t    data_reg,
        uint32_t    addr_val,
        uint16_t *  values
)
{
    unsigned i;
    int ret;

    /* Write indirect RAM address register. */
    ret = WRITEL(addr_reg, addr_val);
    if (ret)
    {
        return ret;
    }

    /* loop over all table lines. */
    for (i = 0; i < RPP_LSC4_TABLE_HEIGHT; ++i) {
        /* read each line individually to conserve
         * stack memory. */
        uint32_t regs[NR_TABLE_REGS];

        /* read the RAM cell values from the indirect RAM data register. */
        ret = READ_RAM(data_reg, regs, NR_TABLE_REGS);
        if (ret)
        {
            break;
        }

        /* unpack the cells into the output array */
        UNPACK_REGS(
                RPP_LSC4_TABLE_WIDTH,
                RPP_LSC_TABLE_SAMPLE_SHIFT,
                RPP_LSC_TABLE_SAMPLE_MASK,
                regs,
                values
                );
        /* increment the data pointer for the next round. */
        values += RPP_LSC4_TABLE_WIDTH;
    }
    return ret;
}

/******************************************************************************
 * rpp_lsc4_table
 *****************************************************************************/
int rpp_lsc4_table
(
    rpp_lsc4_drv_t *            drv,
    const unsigned              table_id,
    rpp_lsc4_values_t * const   values
)
{
    uint32_t v;
    int ret = 0;

    /* simple sanity checks */
    if ( !check_valid(drv) || !values )
    {
        return ( -EINVAL );
    }

    if ( !(table_id < RPP_LSC4_NUM_TABLES) )
    {
        return -ERANGE;
    }

    /* compute the indirect RAM address from the table_id */
    v = 0;
    REG_SET_SLICE( v, RPP_LSC_R_RAM_ADDR, (table_id == 0u) ? 0u : 153u );

    /* now, read the correction table values. */
    if (!ret)
    {
        ret = read_table(drv, RPP_RPP_LSC_R_TABLE_ADDR_REG , RPP_RPP_LSC_R_TABLE_DATA_REG , v, values->r );
    }
    if (!ret)
    {
        ret = read_table(drv, RPP_RPP_LSC_GR_TABLE_ADDR_REG, RPP_RPP_LSC_GR_TABLE_DATA_REG, v, values->gr);
    }
    if (!ret)
    {
        ret = read_table(drv, RPP_RPP_LSC_B_TABLE_ADDR_REG , RPP_RPP_LSC_B_TABLE_DATA_REG , v, values->b );
    }
    if (!ret)
    {
        ret = read_table(drv, RPP_RPP_LSC_GB_TABLE_ADDR_REG, RPP_RPP_LSC_GB_TABLE_DATA_REG, v, values->gb);
    }

    return ret;
}

/******************************************************************************
 * rpp_lsc4_set_grid
 *****************************************************************************/
int rpp_lsc4_set_grid(rpp_lsc4_drv_t * drv,const rpp_lsc4_grid_t * const values)
{
    int ret = 0;
    unsigned i;
    uint32_t regs[NR_TABLE_REGS];

    /* simple sanity checks */
    if ( !check_valid(drv) || !values )
    {
        return ( -EINVAL );
    }

    /* range check */
    for (i = 0;  i < RPP_LSC4_GRID_SIZE; ++i)
    {
        if ( (REG_RANGE_CHECK( values->x_grad[i], RPP_LSC_XGRAD_0))       ||
             (REG_RANGE_CHECK( values->y_grad[i], RPP_LSC_YGRAD_0))       ||
             (REG_RANGE_CHECK( values->x_size[i], RPP_LSC_X_SECT_SIZE_0)) ||
             (REG_RANGE_CHECK( values->y_size[i], RPP_LSC_Y_SECT_SIZE_0)) )
        {
            return ( -ERANGE );
        }
    }

    /* x-grad */
    if (!ret) {
        PACK_REGS(
                RPP_LSC4_GRID_SIZE,
                RPP_LSC_XGRAD_1_SHIFT,
                RPP_LSC_XGRAD_0_MASK,
                values->x_grad,
                regs
                );
        ret = WRITE_REGS(RPP_RPP_LSC_XGRAD_01_REG, regs, NR_TABLE_REGS);
    }
    /* y-grad */
    if (!ret) {
        PACK_REGS(
                RPP_LSC4_GRID_SIZE,
                RPP_LSC_YGRAD_1_SHIFT,
                RPP_LSC_YGRAD_0_MASK,
                values->y_grad,
                regs
                );
        ret = WRITE_REGS(RPP_RPP_LSC_YGRAD_01_REG, regs, NR_TABLE_REGS);
    }
    /* x-size */
    if (!ret) {
        PACK_REGS(
                RPP_LSC4_GRID_SIZE,
                RPP_LSC_X_SECT_SIZE_1_SHIFT,
                RPP_LSC_X_SECT_SIZE_0_MASK,
                values->x_size,
                regs
                );
        ret = WRITE_REGS(RPP_RPP_LSC_XSIZE_01_REG, regs, NR_TABLE_REGS);
    }
    /* y-size */
    if (!ret) {
        PACK_REGS(
                RPP_LSC4_GRID_SIZE,
                RPP_LSC_Y_SECT_SIZE_1_SHIFT,
                RPP_LSC_Y_SECT_SIZE_0_MASK,
                values->y_size,
                regs
                );
        ret = WRITE_REGS(RPP_RPP_LSC_YSIZE_01_REG, regs, NR_TABLE_REGS);
    }

    return ret;
}

/******************************************************************************
 * rpp_lsc4_grid
 *****************************************************************************/
int rpp_lsc4_grid(rpp_lsc4_drv_t * drv, rpp_lsc4_grid_t * const values)
{
    int ret;
    uint32_t regs[NR_TABLE_REGS];

    /* simple sanity checks */
    if ( !check_valid(drv) || !values )
    {
        return ( -EINVAL );
    }

    /* read X_GRAD values. */
    ret = READ_REGS(RPP_RPP_LSC_XGRAD_01_REG, regs, NR_TABLE_REGS);
    if (!ret)
    {
        /* unpack X_GRAD values if successful */
        UNPACK_REGS(
                RPP_LSC4_GRID_SIZE,
                RPP_LSC_XGRAD_1_SHIFT,
                RPP_LSC_XGRAD_0_MASK,
                regs,
                values->x_grad
                );

        /* read Y_GRAD values if successful */
        ret = READ_REGS(RPP_RPP_LSC_YGRAD_01_REG, regs, NR_TABLE_REGS);
    }

    if (!ret)
    {
        /* unpack Y_GRAD values if successful */
        UNPACK_REGS(
                RPP_LSC4_GRID_SIZE,
                RPP_LSC_YGRAD_1_SHIFT,
                RPP_LSC_YGRAD_0_MASK,
                regs,
                values->y_grad
                );
        
        /* read X_SIZE values if successful */
        ret = READ_REGS(RPP_RPP_LSC_XSIZE_01_REG, regs, NR_TABLE_REGS);
    }

    if (!ret)
    {
        /* unpack X_SIZE values if successful */
        UNPACK_REGS(
                RPP_LSC4_GRID_SIZE,
                RPP_LSC_X_SECT_SIZE_1_SHIFT,
                RPP_LSC_X_SECT_SIZE_0_MASK,
                regs,
                values->x_size
                );

        /* read Y_SIZE values if successful */
        ret = READ_REGS(RPP_RPP_LSC_YSIZE_01_REG, regs, NR_TABLE_REGS);
    }

    if (!ret)
    {
        /* unpack Y_SIZE values if successful */
        UNPACK_REGS(
                RPP_LSC4_GRID_SIZE,
                RPP_LSC_Y_SECT_SIZE_1_SHIFT,
                RPP_LSC_Y_SECT_SIZE_0_MASK,
                regs,
                values->y_size
                );
    }

    return ret;
}

