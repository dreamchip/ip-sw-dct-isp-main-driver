/******************************************************************************
 *
 * Copyright 2018 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_gamma_in_drv.c
 *
 * @brief   Implementation of degamma driver
 *
 *****************************************************************************/
#include <rpp_gamma_in_drv.h>

// make sure that the base is 0 in the auto-generated
// header-file, before including it
#define RPP_GAMMA_IN24_BASE 0
#include <rpp_gamma_in24_regs_addr_map.h>
#include <rpp_gamma_in24_regs_mask.h>

/******************************************************************************
 * local definitions
 *****************************************************************************/

static inline int check_valid(const rpp_gamma_in_drv_t * drv)
{
    return (drv && drv->dev && drv->colorbits);
}


/******************************************************************************
 * rpp_gamma_in_init
 *****************************************************************************/
int rpp_gamma_in_init(rpp_device * dev, uint32_t base, rpp_gamma_in_drv_t * drv)
{
    uint32_t v;
    int ret;

    /* simple base address check */
    if ( !dev || !drv )
    {
        return ( -EINVAL );
    }

    drv->dev = dev;
    drv->base = base;
    drv->version = 0;
    drv->colorbits = 0;

    /* check that rpp-gamma-in module exists */
    ret = READL(RPP_RPP_GAMMA_IN_VERSION_REG, &v);

    if (!ret)
    {
        drv->version = REG_GET_SLICE( v, RPP_GAMMA_IN_GAMMA_IN_VERSION );

        /* support only those versions which have a 4bit dx slice. */
        switch( drv->version)
        {
            case 0x07:
                drv->colorbits = 12;
                break;
            case 0x08:
                drv->colorbits = 20;
                break;
            case 0x09:
                drv->colorbits = 24;
                break;
            default:
                return ( -ENODEV );
        }
    }

    return ret;
}

/******************************************************************************
 * rpp_gamma_in_enable
 *****************************************************************************/
int rpp_gamma_in_enable(rpp_gamma_in_drv_t * drv, const unsigned on)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    ret = READL(RPP_RPP_GAMMA_IN_ENABLE_REG, &v);
    if (!ret)
    {
        REG_SET_SLICE( v, RPP_GAMMA_IN_GAMMA_IN_EN, (on ? 1u : 0u) );
        ret = WRITEL(RPP_RPP_GAMMA_IN_ENABLE_REG, v );
    }

    return ret;
}

/******************************************************************************
 * rpp_gamma_in_enabled
 *****************************************************************************/
int rpp_gamma_in_enabled(rpp_gamma_in_drv_t * drv, unsigned * on)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !on )
    {
        return ( -EINVAL );
    }

    ret = READL(RPP_RPP_GAMMA_IN_ENABLE_REG, &v);
    if (!ret)
    {
        *on = REG_GET_SLICE( v, RPP_GAMMA_IN_GAMMA_IN_EN );
    }

    return ret;
}

/******************************************************************************
 * rpp_gamma_in_set_curves
 *****************************************************************************/
int rpp_gamma_in_set_curves(rpp_gamma_in_drv_t * drv, const rpp_gamma_in_curve_sample_t * samples)
{
    uint32_t dx[2] = {0, 0};
    uint32_t cmax, dxmax;
    uint32_t regs[RPP_GAMMA_IN_MAX_SAMPLES];
    unsigned i;
    int ret;

    /* simple base address check */
    if ( !check_valid(drv) || !samples )
    {
        return ( -EINVAL );
    }

    /* calculate validation constraints. */
    cmax = UNSIGNED_MAX(drv->colorbits);
    dxmax = (drv->colorbits > 12) ? 15 : 7;

    /* range check: start is at x==0 (cannot be programmed to HW, anyway). */
    if (  samples[0u].x != 0u )
    {
        return ( -ERANGE );
    }

    /* Range-check all curve points. On the way, also collect and merge
     * the DX values into their corresponding register slices. */
    for (i = 0; i < RPP_GAMMA_IN_MAX_SAMPLES; ++i)
    {
        if (
                (samples[i].x       > dxmax)    ||
                (samples[i].red     > cmax)     ||
                (samples[i].green   > cmax)     ||
                (samples[i].blue    > cmax)
           )
        {
            return -(ERANGE);
        }
        if (i)
        {
            unsigned j = (i > 8) ? 1 : 0;
            dx[j] = (dx[j] >> RPP_GAMMA_IN_GAMMA_DX_2_SHIFT) | (samples[i].x << (32 - RPP_GAMMA_IN_GAMMA_DX_2_SHIFT));
        }
    }

    /* Program the DX values. */
    ret = WRITE_REGS(RPP_RPP_GAMMA_IN_DX_LO_REG, dx, 2);

    if (!ret)
    {
        /* Collect the first color channel Y-axis values. */
        for (i = 0; i < RPP_GAMMA_IN_MAX_SAMPLES; ++i)
        {
            regs[i] = samples[i].red;
        }

        /* program the first color channel values efficiently using multiple register
         * writes. */
        ret = WRITE_REGS(RPP_RPP_GAMMA_IN_R_Y__0_REG, regs, RPP_GAMMA_IN_MAX_SAMPLES);
    }

    if (!ret)
    {
        /* extract the next color channel... */
        for (i = 0; i < RPP_GAMMA_IN_MAX_SAMPLES; ++i)
        {
            regs[i] = samples[i].green;
        }
        ret = WRITE_REGS(RPP_RPP_GAMMA_IN_G_Y__0_REG, regs, RPP_GAMMA_IN_MAX_SAMPLES);
    }
    if (!ret)
    {
        /* extract the last color channel */
        for (i = 0; i < RPP_GAMMA_IN_MAX_SAMPLES; ++i)
        {
            regs[i] = samples[i].blue;
        }
        ret = WRITE_REGS(RPP_RPP_GAMMA_IN_B_Y__0_REG, regs, RPP_GAMMA_IN_MAX_SAMPLES);
    }
    return ret;
}

/******************************************************************************
 * rpp_gamma_in_curves
 *****************************************************************************/
int rpp_gamma_in_curves(rpp_gamma_in_drv_t * drv, rpp_gamma_in_curve_sample_t * const samples)
{
    uint32_t regs[RPP_GAMMA_IN_MAX_SAMPLES];
    unsigned i;
    uint32_t cmask;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !samples )
    {
        return ( -EINVAL );
    }

    /* note that masking of Y-axis values depends on the colorbits. */
    cmask = UNSIGNED_MAX(drv->colorbits);

    /* read the DX registers. */
    ret = READ_REGS(RPP_RPP_GAMMA_IN_DX_LO_REG, regs, 2);
    if (!ret)
    {
        /* extract the register slices from the packed register set and
         * store them in the output array.*/
        samples[0].x = 0;
        for (i = 1; i < RPP_GAMMA_IN_MAX_SAMPLES; ++i)
        {
            unsigned j = (i > 8) ? 1 : 0;
            samples[i].x = regs[j] & RPP_GAMMA_IN_GAMMA_DX_1_MASK;
            regs[j] = regs[j] >> RPP_GAMMA_IN_GAMMA_DX_2_SHIFT;
        }

        /* read the first color channel registers now. */
        ret = READ_REGS(RPP_RPP_GAMMA_IN_R_Y__0_REG, regs, RPP_GAMMA_IN_MAX_SAMPLES);
    }

    if (!ret)
    {
        /* if successful, copy the color values into the output array. */
        for (i = 0; i < RPP_GAMMA_IN_MAX_SAMPLES; ++i)
        {
            samples[i].red = regs[i] & cmask;
        }

        /* and read the next color channel. */
        ret = READ_REGS(RPP_RPP_GAMMA_IN_G_Y__0_REG, regs, RPP_GAMMA_IN_MAX_SAMPLES);
    }

    if (!ret)
    {
        /* if successful, copy the color values into the output array. */
        for (i = 0; i < RPP_GAMMA_IN_MAX_SAMPLES; ++i)
        {
            samples[i].green = regs[i] & cmask;
        }

        /* and read the last color channel. */
        ret = READ_REGS(RPP_RPP_GAMMA_IN_B_Y__0_REG, regs, RPP_GAMMA_IN_MAX_SAMPLES);
    }

    if (!ret)
    {
        /* if successful, copy the color values into the output array. */
        for (i = 0; i < RPP_GAMMA_IN_MAX_SAMPLES; ++i)
        {
            samples[i].blue = regs[i] & cmask;
        }
    }

    return ret;
}


