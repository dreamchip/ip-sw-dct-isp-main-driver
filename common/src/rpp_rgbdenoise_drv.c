/******************************************************************************
 *
 * Copyright 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_rgbdenoise_drv.c
 *
 * @brief   Implementation of RGBDENOISE unit driver
 *
 *****************************************************************************/
#include <rpp_rgbdenoise_drv.h>

// make sure that the base is 0 in the auto-generated
// header-file, before including it
#define RPP_RGBDENOISE_BASE 0
#include <rpp_rgbdenoise_regs_addr_map.h>
#include <rpp_rgbdenoise_regs_mask.h>

/******************************************************************************
 * local definitions
 *****************************************************************************/

static inline int check_valid(const rpp_rgbdenoise_drv_t * drv)
{
    return (drv && drv->dev && drv->colorbits);
}

#define N_IF NUM_REGS(RPP_RGBDENOISE_IF_COEFF_NUM, \
        RPP_RGBDENOISE_C2NR_LU_IF_COEF_01_SHIFT)

/******************************************************************************
 * rpp_rgbdenoise_init
 *****************************************************************************/
int rpp_rgbdenoise_init(rpp_device * dev, uint32_t base, rpp_rgbdenoise_drv_t * drv)
{
    uint32_t v;
    int ret;

    /* simple base address check */
    if ( !dev || !drv )
    {
        return ( -EINVAL );
    }

    drv->dev = dev;
    drv->base = base;
    drv->version = 0;
    drv->colorbits = 0;

    /* check that rpp-bls module exists */
    ret = READL(RPP_RPP_RGBDENOISE_VERSION_REG, &v );
    if (!ret)
    {
        drv->version = REG_GET_SLICE(v, RPP_RGBDENOISE_RGBDENOISE_VERSION);

        /* currently, there are only 12bit versions of the unit. */
        if (drv->version)
        {
            drv->colorbits = 12;
        } else {
            ret = -ENODEV;
        }
    }

    return ret;
}

/******************************************************************************
 * rpp_rgbdenoise_enable
 *****************************************************************************/
int rpp_rgbdenoise_enable(rpp_rgbdenoise_drv_t * drv, const unsigned on)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    ret = READL(RPP_RPP_RGBDENOISE_HW_BYPASS_REG, &v);
    if (!ret)
    {
        REG_SET_SLICE(v, RPP_RGBDENOISE_BYPASS_EN, !on);
        ret = WRITEL(RPP_RPP_RGBDENOISE_HW_BYPASS_REG, v);
    }

    return ret;
}

/******************************************************************************
 * rpp_rgbdenoise_enabled
 *****************************************************************************/
int rpp_rgbdenoise_enabled(rpp_rgbdenoise_drv_t * drv, unsigned * const on)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) || !on )
    {
        return ( -EINVAL );
    }

    ret = READL(RPP_RPP_RGBDENOISE_HW_BYPASS_REG, &v);
    if (!ret)
    {
        *on = !REG_GET_SLICE(v, RPP_RGBDENOISE_BYPASS_EN);
    }

    return ret;
}

/******************************************************************************
 * rpp_rgbdenoise_set_denoise_config
 *****************************************************************************/
int rpp_rgbdenoise_set_denoise_config
(
    rpp_rgbdenoise_drv_t *              drv,
    const rpp_rgbdenoise_cfg_t * const  config
)
{
    uint32_t regs_if[N_IF];
    uint32_t regs_sf[5];
    uint32_t v;
    unsigned i;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !config )
    {
        return -EINVAL;
    }

    /* range-check the arguments. */
    if (
            (config->shift_y > RPP_RGBDENOISE_MAX_SHIFT) ||
            (config->shift_c > RPP_RGBDENOISE_MAX_SHIFT)
       )
    {
        return -ERANGE;
    }

    for (i = 0; i < RPP_RGBDENOISE_IF_COEFF_NUM; ++i)
    {
        if (
                REG_RANGE_CHECK(config->if_cy[i], RPP_RGBDENOISE_C2NR_LU_IF_COEF_00)    ||
                REG_RANGE_CHECK(config->if_cc[i], RPP_RGBDENOISE_C2NR_LU_IF_COEF_00)
           )
        {
            return -ERANGE;
        }
    }

    for (i = 0; i < RPP_RGBDENOISE_SF_COEFF_NUM; ++i)
    {
        //NOTE: because the sf values are transformed before writing the registers
        //      the value range is different
        if ((config->sf_cy[i] & ~0xf) || (config->sf_cc[i] & ~0xf))
        {
            return -ERANGE;
        }
    }

    /* now program the values. */
    PACK_REGS(
            RPP_RGBDENOISE_IF_COEFF_NUM,
            RPP_RGBDENOISE_C2NR_LU_IF_COEF_01_SHIFT,
            RPP_RGBDENOISE_C2NR_LU_IF_COEF_00_MASK,
            config->if_cy, regs_if
            );
    ret = WRITE_REGS(RPP_RPP_RGBDENOISE_SPNR_LUMA_IF_COEF_00_07_REG, regs_if, N_IF);

    if (!ret) {
        PACK_REGS(
                RPP_RGBDENOISE_IF_COEFF_NUM,
                RPP_RGBDENOISE_C2NR_CR_IF_COEF_01_SHIFT,
                RPP_RGBDENOISE_C2NR_CR_IF_COEF_00_MASK,
                config->if_cc, regs_if
                );
        ret = WRITE_REGS(RPP_RPP_RGBDENOISE_SPNR_CHROMA_IF_COEF_00_07_REG, regs_if, N_IF);
    }

    if (!ret) {
        uint32_t sf_lu[10];
        uint32_t sf_cr[10];

        /* convert coefficients into the register values */
        sf_lu[0] = config->sf_cy[0] * config->sf_cy[0];
        sf_lu[1] = config->sf_cy[0] * config->sf_cy[1];
        sf_lu[2] = config->sf_cy[0] * config->sf_cy[2];
        sf_lu[3] = config->sf_cy[0] * config->sf_cy[3];
        sf_lu[4] = config->sf_cy[1] * config->sf_cy[1];
        sf_lu[5] = config->sf_cy[2] * config->sf_cy[2];
        sf_lu[6] = config->sf_cy[3] * config->sf_cy[3];
        sf_lu[7] = config->sf_cy[1] * config->sf_cy[2];
        sf_lu[8] = config->sf_cy[1] * config->sf_cy[3];
        sf_lu[9] = config->sf_cy[2] * config->sf_cy[3];

        sf_cr[0] = config->sf_cc[0] * config->sf_cc[0];
        sf_cr[1] = config->sf_cc[0] * config->sf_cc[1];
        sf_cr[2] = config->sf_cc[0] * config->sf_cc[2];
        sf_cr[3] = config->sf_cc[0] * config->sf_cc[3];
        sf_cr[4] = config->sf_cc[1] * config->sf_cc[1];
        sf_cr[5] = config->sf_cc[2] * config->sf_cc[2];
        sf_cr[6] = config->sf_cc[3] * config->sf_cc[3];
        sf_cr[7] = config->sf_cc[1] * config->sf_cc[2];
        sf_cr[8] = config->sf_cc[1] * config->sf_cc[3];
        sf_cr[9] = config->sf_cc[2] * config->sf_cc[3];

        for(unsigned i = 0; i < 5; i++) {
            REG_SET_SLICE(regs_sf[i], RPP_RGBDENOISE_C2NR_LU_SPATIAL_COEFF_0, sf_lu[2*i + 0]);
            REG_SET_SLICE(regs_sf[i], RPP_RGBDENOISE_C2NR_LU_SPATIAL_COEFF_1, sf_lu[2*i + 1]);
            REG_SET_SLICE(regs_sf[i], RPP_RGBDENOISE_C2NR_CR_SPATIAL_COEFF_0, sf_cr[2*i + 0]);
            REG_SET_SLICE(regs_sf[i], RPP_RGBDENOISE_C2NR_CR_SPATIAL_COEFF_1, sf_cr[2*i + 1]);

        }

        ret = WRITE_REGS(RPP_RPP_RGBDENOISE_SPNR_SPATIAL_COEF_0_1_REG, regs_sf, 5);
    }

    if (!ret) {
        /* the control register contains the shifts as well as the 'enable' bit,
         * it must be read-modify-write, therefore. */
        ret = READL(RPP_RPP_RGBDENOISE_SPNR_CTRL_REG, &v);
    }
    if (!ret) {
        REG_SET_SLICE(v, RPP_RGBDENOISE_C2NR_INTENSITY_SHIFT_Y, config->shift_y);
        REG_SET_SLICE(v, RPP_RGBDENOISE_C2NR_INTENSITY_SHIFT_C, config->shift_c);
        ret = WRITEL(RPP_RPP_RGBDENOISE_SPNR_CTRL_REG, v);
    }
    
    return ret;
}

static uint64_t isqrt( uint64_t v )
{
    uint64_t t, q, b, r;
    r = v;           // r = v - x²
    b = 0x4000000000000000ULL;  // a²
    q = 0;           // 2ax
    while( b > 0 )
    {
        t = q + b;   // t = 2ax + a²
        q >>= 1;     // if a' = a/2, then q' = q/2
        if( r >= t ) // if (v - x²) >= 2ax + a²
        {
            r -= t;  // r' = (v - x²) - (2ax + a²)
            q += b;  // if x' = (x + a) then ax' = ax + a², thus q' = q' + b
        }
        b >>= 2;     // if a' = a/2, then b' = b / 4
    }
    return q;
}

/******************************************************************************
 * rpp_rgbdenoise_next_denoise_config
 *****************************************************************************/
int rpp_rgbdenoise_denoise_config
(
    rpp_rgbdenoise_drv_t *          drv,
    rpp_rgbdenoise_cfg_t * const    config
)
{
    uint32_t regs_if[N_IF];
    uint32_t regs_sf[5];
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !config )
    {
        return -EINVAL;
    }

    /* read and parse the programmed registers. */
    ret = READ_REGS(RPP_RPP_RGBDENOISE_SPNR_LUMA_IF_COEF_00_07_REG, regs_if, N_IF);
    if (!ret) {
        /* now extract the values. */
        UNPACK_REGS(
                RPP_RGBDENOISE_IF_COEFF_NUM,
                RPP_RGBDENOISE_C2NR_LU_IF_COEF_01_SHIFT,
                RPP_RGBDENOISE_C2NR_LU_IF_COEF_00_MASK,
                regs_if, config->if_cy
                );
        /* and read next chunk of data. */
        ret = READ_REGS(RPP_RPP_RGBDENOISE_SPNR_CHROMA_IF_COEF_00_07_REG, regs_if, N_IF);
    }
    if (!ret) {
        UNPACK_REGS(
                RPP_RGBDENOISE_IF_COEFF_NUM,
                RPP_RGBDENOISE_C2NR_CR_IF_COEF_01_SHIFT,
                RPP_RGBDENOISE_C2NR_CR_IF_COEF_00_MASK,
                regs_if, config->if_cc
                );
        /* and read next chunk of data. */
        ret = READ_REGS(RPP_RPP_RGBDENOISE_SPNR_SPATIAL_COEF_0_1_REG, regs_sf, 5);
    }

    if (!ret) {
        uint32_t sf_lu[4];
        uint32_t sf_cr[4];

        sf_lu[0] = REG_GET_SLICE(regs_sf[0], RPP_RGBDENOISE_C2NR_LU_SPATIAL_COEFF_0);
        sf_lu[1] = REG_GET_SLICE(regs_sf[2], RPP_RGBDENOISE_C2NR_LU_SPATIAL_COEFF_4);
        sf_lu[2] = REG_GET_SLICE(regs_sf[2], RPP_RGBDENOISE_C2NR_LU_SPATIAL_COEFF_5);
        sf_lu[3] = REG_GET_SLICE(regs_sf[3], RPP_RGBDENOISE_C2NR_LU_SPATIAL_COEFF_6);

        sf_cr[0] = REG_GET_SLICE(regs_sf[0], RPP_RGBDENOISE_C2NR_CR_SPATIAL_COEFF_0);
        sf_cr[1] = REG_GET_SLICE(regs_sf[2], RPP_RGBDENOISE_C2NR_CR_SPATIAL_COEFF_4);
        sf_cr[2] = REG_GET_SLICE(regs_sf[2], RPP_RGBDENOISE_C2NR_CR_SPATIAL_COEFF_5);
        sf_cr[3] = REG_GET_SLICE(regs_sf[3], RPP_RGBDENOISE_C2NR_CR_SPATIAL_COEFF_6);

        /* all the spatial filter values are in a single register. */
        config->sf_cy[0] = (uint8_t)isqrt(sf_lu[0] << 8);
        config->sf_cy[1] = (uint8_t)isqrt(sf_lu[1] << 8);
        config->sf_cy[2] = (uint8_t)isqrt(sf_lu[2] << 8);
        config->sf_cy[3] = (uint8_t)isqrt(sf_lu[3] << 8);
        config->sf_cc[0] = (uint8_t)isqrt(sf_cr[0] << 8);
        config->sf_cc[1] = (uint8_t)isqrt(sf_cr[1] << 8);
        config->sf_cc[2] = (uint8_t)isqrt(sf_cr[2] << 8);
        config->sf_cc[3] = (uint8_t)isqrt(sf_cr[3] << 8);
        /* and read the control register. */
        ret = READL(RPP_RPP_RGBDENOISE_SPNR_CTRL_REG, &v);
    }

    if (!ret) {
        config->shift_y = REG_GET_SLICE(v, RPP_RGBDENOISE_C2NR_INTENSITY_SHIFT_Y);
        config->shift_c = REG_GET_SLICE(v, RPP_RGBDENOISE_C2NR_INTENSITY_SHIFT_C);
    }
    
    return ret;
}

/******************************************************************************
 * rpp_rgbdenoise_set_profile
 *****************************************************************************/
int rpp_rgbdenoise_set_profile
(
    rpp_rgbdenoise_drv_t *              drv,
    const rpp_ccor_profile_t * const    profile
)
{
    int ret;
    unsigned i;

    /* simple sanity checks */
    if ( !check_valid(drv) || !profile )
    {
        return -EINVAL;
    }

    /* It is known that the all offsets in the profile must all be
     * non-negative. The sign bit is missing from the CCOR offset
     * registers in this block and cannot be programmed. */
    for (i = 0; i < RPP_CCOR_OFF_NO; ++i)
    {
        if (profile->off[i] < 0)
        {
            return -ERANGE;
        }
    }

    /* check the profile */
    ret = rpp_ccor_check_profile(drv->colorbits, profile);

    /* and program the profile if successful */
    if (!ret) {
        ret = rpp_ccor_write_profile(
                drv->dev, drv->base,
                RPP_RPP_RGBDENOISE_RGB2YUV_CCOR_COEFF_0_REG,
                drv->colorbits, profile
                );
    }

    return ret;
}

/******************************************************************************
 * rpp_rgbdenoise_next_profile
 *****************************************************************************/
int rpp_rgbdenoise_profile
(
    rpp_rgbdenoise_drv_t *      drv,
    rpp_ccor_profile_t * const  profile
)
{
    /* simple sanity checks */
    if ( !check_valid(drv) || !profile )
    {
        return -EINVAL;
    }

    return rpp_ccor_read_profile(
            drv->dev, drv->base,
            RPP_RPP_RGBDENOISE_RGB2YUV_CCOR_COEFF_0_REG,
            drv->colorbits, profile
            );
}

