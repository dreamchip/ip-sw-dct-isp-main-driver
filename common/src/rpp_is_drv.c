/******************************************************************************
 *
 * Copyright 2019 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_is_drv.c
 *
 * @brief   Implementation of image stabilization driver
 *
 *****************************************************************************/
#include <rpp_is_drv.h>

// make sure that the base is 0 in the auto-generated
// header-file, before including it
#define RPP_IS_BASE 0
#include <rpp_is_regs_addr_map.h>
#include <rpp_is_regs_mask.h>

/******************************************************************************
 * local definitions
 *****************************************************************************/

static inline int check_valid(const rpp_is_drv_t * drv)
{
    return (drv && drv->dev && drv->version);
}

/******************************************************************************
 * rpp_is_init
 *****************************************************************************/
int rpp_is_init(rpp_device * dev, uint32_t base, rpp_is_drv_t * drv)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !dev || !drv )
    {
        return ( -EINVAL );
    }
    drv->dev = dev;
    drv->base = base;
    drv->version = 0;

    /* read and parse the version register. */
    ret = READL(RPP_RPP_IS_VERSION_REG, &v);
    if (!ret)
    {
        drv->version = REG_GET_SLICE(v, RPP_IS_IS_VERSION);
        if (!drv->version)
        {
            ret = -ENODEV;
        }
    }

    return ret;
}

/******************************************************************************
 * rpp_is_set_window
 *****************************************************************************/
int rpp_is_set_window(rpp_is_drv_t * drv, const rpp_window_t * const w)
{
    uint32_t regs[4];

    /* simple sanity checks */
    if ( !check_valid(drv) || !w )
    {
        return ( -EINVAL );
    }

    if ( REG_RANGE_CHECK(w->hoff   , RPP_IS_IS_H_OFFS) ||
         REG_RANGE_CHECK(w->voff   , RPP_IS_IS_V_OFFS) ||
         REG_RANGE_CHECK(w->width  , RPP_IS_IS_H_SIZE) ||
         REG_RANGE_CHECK(w->height , RPP_IS_IS_V_SIZE) )
    {
        return ( -ERANGE );
    }

    /* copy the range-checked, unsigned values into a temporary
     * register array. */
    regs[0] = w->hoff;
    regs[1] = w->voff;
    regs[2] = w->width;
    regs[3] = w->height;

    return WRITE_REGS(RPP_RPP_IS_H_OFFS_REG, regs, 4);
}

/******************************************************************************
 * rpp_is_next_window
 *****************************************************************************/
int rpp_is_next_window(rpp_is_drv_t * drv, rpp_window_t * const w)
{
    uint32_t regs[4];
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !w )
    {
        return ( -EINVAL );
    }

    /* read the window coordinates into a temporary register array. */
    ret = READ_REGS(RPP_RPP_IS_H_OFFS_REG, regs, 4);

    /* if successful, extract relevant data. */
    if (!ret)
    {
        w->hoff     = REG_GET_SLICE(regs[0], RPP_IS_IS_H_OFFS);
        w->voff     = REG_GET_SLICE(regs[1], RPP_IS_IS_V_OFFS);
        w->width    = REG_GET_SLICE(regs[2], RPP_IS_IS_H_SIZE);
        w->height   = REG_GET_SLICE(regs[3], RPP_IS_IS_V_SIZE);
    }

    return ret;
}

/******************************************************************************
 * rpp_is_active_window
 *****************************************************************************/
int rpp_is_active_window(rpp_is_drv_t * drv, rpp_window_t * const w)
{
    uint32_t regs[4];
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !w )
    {
        return ( -EINVAL );
    }

    /* read the shadow window coordinates into a temporary register array. */
    ret = READ_REGS(RPP_RPP_IS_H_OFFS_SHD_REG, regs, 4);

    /* if successful, extract relevant data. */
    if (!ret)
    {
        w->hoff     = REG_GET_SLICE(regs[0], RPP_IS_IS_H_OFFS_SHD);
        w->voff     = REG_GET_SLICE(regs[1], RPP_IS_IS_V_OFFS_SHD);
        w->width    = REG_GET_SLICE(regs[2], RPP_IS_IS_H_SIZE_SHD);
        w->height   = REG_GET_SLICE(regs[3], RPP_IS_IS_V_SIZE_SHD);
    }

    return ret;
}


