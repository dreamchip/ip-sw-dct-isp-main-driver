/******************************************************************************
 *
 * Copyright 2018 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_acq_drv.c
 *
 * @brief   Implementation of input acquisition driver
 *
 *****************************************************************************/
#include <rpp_acq_drv.h>

// make sure that the base is 0 in the auto-generated
// header-file, before including it
#define RPP_ACQ_BASE 0
#include <rpp_acq_regs_addr_map.h>
#include <rpp_acq_regs_mask.h>

/******************************************************************************
 * local definitions
 *****************************************************************************/

static inline int check_valid(const rpp_acq_drv_t * drv)
{
    return (drv && drv->dev);
}

/******************************************************************************
 * rpp_acq_init
 *****************************************************************************/
int rpp_acq_init(rpp_device * dev, uint32_t base, rpp_acq_drv_t * drv)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !dev || !drv )
    {
        return ( -EINVAL );
    }

    /* fill in driver-specific structure */
    drv->base = base;
    drv->dev  = dev;
    drv->version = 0;

    /* check that rpp-acq module exists */
    ret = READL(RPP_RPP_ACQ_VERSION_REG, &v);
    if (!ret)
    {
        drv->version = REG_GET_SLICE(v, RPP_ACQ_ACQ_VERSION);
        if ( !drv->version)
        {
            return ( -ENODEV );
        }
    }

    return ret;
}

/******************************************************************************
 * rpp_acq_enable
 *****************************************************************************/
int rpp_acq_enable(rpp_acq_drv_t * drv, const unsigned on)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    /* enable or disable rpp-acq input formatter */
    ret = READL( RPP_RPP_ACQ_CTRL_REG, &v );
    if (!ret)
    {
        REG_SET_SLICE( v, RPP_ACQ_INFORM_EN, on ? 1u : 0u );
        ret = WRITEL( RPP_RPP_ACQ_CTRL_REG, v );
    }
    return ret;
}

/******************************************************************************
 * rpp_acq_enabled
 *****************************************************************************/
int rpp_acq_enabled(rpp_acq_drv_t * drv, unsigned * const on)
{
    int ret;
    uint32_t v;

    /* simple sanity checks */
    if ( !check_valid(drv) || !on )
    {
        return ( -EINVAL );
    }

    ret = READL(RPP_RPP_ACQ_CTRL_REG, &v);
    if (!ret)
    {
        *on = REG_GET_SLICE( v, RPP_ACQ_INFORM_EN );
    }

    return ret;
}

/******************************************************************************
 * rpp_acq_set_mode
 *****************************************************************************/
int rpp_acq_set_mode(rpp_acq_drv_t * drv, const unsigned mode)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv))
    {
        return ( -EINVAL );
    }

    /* range-check the parameter. */
    if (mode >= RPP_ACQ_MODE_MAX)
    {
        return -ERANGE;
    }

    /* set operation mode in rpp-acq module */
    ret = READL(RPP_RPP_ACQ_CTRL_REG, &v);
    if (!ret) {
        REG_SET_SLICE( v, RPP_ACQ_RPP_MODE, mode );
        ret = WRITEL( RPP_RPP_ACQ_CTRL_REG, v );
    }
    return ret;
}

/******************************************************************************
 * rpp_acq_mode
 *****************************************************************************/
int rpp_acq_mode(rpp_acq_drv_t * drv, unsigned * const mode)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !mode )
    {
        return ( -EINVAL );
    }

    /* read current operation mode from rpp-acq module */
    ret = READL(RPP_RPP_ACQ_CTRL_REG, &v);
    if (!ret) {
        *mode = REG_GET_SLICE( v, RPP_ACQ_RPP_MODE );
    }
    return ret;
}

/******************************************************************************
 * rpp_acq_set_alternative_mode
 *****************************************************************************/
int rpp_acq_set_alternative_mode(rpp_acq_drv_t * drv, const unsigned on)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv))
    {
        return ( -EINVAL );
    }

    /* set operation mode in rpp-acq module */
    ret = READL(RPP_RPP_ACQ_CTRL_REG, &v);
    if (!ret) {
        REG_SET_SLICE( v, RPP_ACQ_ALTERNATIVE_CFG_MODE, !!on );
        ret = WRITEL( RPP_RPP_ACQ_CTRL_REG, v );
    }
    return ret;
}

/******************************************************************************
 * rpp_acq_alternative_mode
 *****************************************************************************/
int rpp_acq_alternative_mode(rpp_acq_drv_t * drv, unsigned * const on )
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !on )
    {
        return ( -EINVAL );
    }

    /* read current operation mode from rpp-acq module */
    ret = READL(RPP_RPP_ACQ_CTRL_REG, &v);
    if (!ret) {
        *on = REG_GET_SLICE( v, RPP_ACQ_ALTERNATIVE_CFG_MODE);
    }
    return ret;
}


/******************************************************************************
 * rpp_acq_set_properties
 *****************************************************************************/
int rpp_acq_set_properties(rpp_acq_drv_t * drv, const rpp_acq_properties_t prop)
{
    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    /* the 'prop' structure has been aligned to match the register slice
     * definition, therefore the 'flags' value stored there can directly
     * be written to the register. */

    return WRITEL(RPP_RPP_ACQ_PROP_REG, prop.flags);
}

/******************************************************************************
 * rpp_acq_properties
 *****************************************************************************/
int rpp_acq_properties(rpp_acq_drv_t * drv, rpp_acq_properties_t * const prop)
{
    /* simple sanity checks */
    if ( !check_valid(drv) || !prop )
    {
        return ( -EINVAL );
    }

    /* the 'prop' structure has been aligned to match the register slice
     * definition, therefore the register value can directly be stored
     * in the 'flags' member of 'prop' */
    return READL(RPP_RPP_ACQ_PROP_REG, &(prop->flags));
}

/* helper function for code size reduction */
static int program_window(rpp_acq_drv_t * drv, uint32_t reg, const rpp_window_t * win)
{
    uint32_t coords[4];

    /* registers for window coordinates are placed adjacently in
     * register space without other slices than the coordinate
     * values, which means optimized writes for multiple registers
     * can be used. */

    coords[0] = win->hoff;
    coords[1] = win->voff;
    coords[2] = win->width;
    coords[3] = win->height;

    return WRITE_REGS(reg, coords, 4);
}

/******************************************************************************
 * rpp_acq_set_acq_window
 *****************************************************************************/    
int rpp_acq_set_acq_window
(
    rpp_acq_drv_t *         drv,
    const rpp_window_t *    in,
    const rpp_window_t *    out
)
{
    int         ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !in || !out )
    {
        return ( -EINVAL );
    }

    if (
            REG_RANGE_CHECK(in->hoff   , RPP_ACQ_ACQ_H_OFFS)         ||
            REG_RANGE_CHECK(in->voff   , RPP_ACQ_ACQ_V_OFFS)         ||
            REG_RANGE_CHECK(in->width  , RPP_ACQ_ACQ_H_SIZE)         ||
            REG_RANGE_CHECK(in->height , RPP_ACQ_ACQ_V_SIZE)         ||
            REG_RANGE_CHECK(out->hoff  , RPP_ACQ_RPP_OUT_H_OFFS)     ||
            REG_RANGE_CHECK(out->voff  , RPP_ACQ_RPP_OUT_V_OFFS)     ||
            REG_RANGE_CHECK(out->width , RPP_ACQ_RPP_OUT_H_SIZE)     ||
            REG_RANGE_CHECK(out->height, RPP_ACQ_RPP_OUT_V_SIZE)     ||
            // window size checks.
            (in->width < (out->hoff + out->width))                   ||
            (in->height < (out->voff + out->height) )
       )
    {
        return ( -ERANGE );
    }

    ret = program_window(drv, RPP_RPP_ACQ_H_OFFS_REG, in);
    if (!ret)
    {
        ret = program_window(drv, RPP_RPP_ACQ_OUT_H_OFFS_REG, out);
    }

    return ret;
}

/******************************************************************************
 * rpp_acq_get_acq_window
 *****************************************************************************/
int rpp_acq_get_acq_window
(
    rpp_acq_drv_t *         drv,
    rpp_window_t * const    in,
    rpp_window_t * const    out
)
{
    uint32_t regs[4];
    int ret = 0;

    /* simple sanity checks */
    if ( !check_valid(drv) || !in || !out )
    {
        return ( -EINVAL );
    }

    /* input acquisition window */
    ret = READ_REGS(RPP_RPP_ACQ_H_OFFS_REG, regs, 4);
    if (!ret) {
        in->hoff    = REG_GET_SLICE(regs[0], RPP_ACQ_ACQ_H_OFFS);
        in->voff    = REG_GET_SLICE(regs[1], RPP_ACQ_ACQ_V_OFFS);
        in->width   = REG_GET_SLICE(regs[2], RPP_ACQ_ACQ_H_SIZE);
        in->height  = REG_GET_SLICE(regs[3], RPP_ACQ_ACQ_V_SIZE);

        ret = READ_REGS(RPP_RPP_ACQ_OUT_H_OFFS_REG, regs, 4);
    }

    /* ISP output window */
    if (!ret) {
        out->hoff   = REG_GET_SLICE(regs[0], RPP_ACQ_RPP_OUT_H_OFFS);
        out->voff   = REG_GET_SLICE(regs[1], RPP_ACQ_RPP_OUT_V_OFFS);
        out->width  = REG_GET_SLICE(regs[2], RPP_ACQ_RPP_OUT_H_SIZE);
        out->height = REG_GET_SLICE(regs[3], RPP_ACQ_RPP_OUT_V_SIZE);
    }

    return ret;
}
