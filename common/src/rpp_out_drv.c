/******************************************************************************
 *
 * Copyright 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_out_drv.c
 *
 * @brief   Implementation of RPP Human Vision Output pipeline unit driver.
 *
 *****************************************************************************/
#include <rpp_out_drv.h>

#define RPP_OUT_BASE  (0)
#include <rpp_out_module_base_addr.h>

/******************************************************************************
 * local definitions
 *****************************************************************************/

#define DO_INIT(mod, base)                                  \
    if (!ret) {                                             \
        ret = rpp_ ## mod ## _init(dev, base, &(drv->mod)); \
    }

/******************************************************************************
 * rpp_out_init
 *****************************************************************************/
int rpp_out_init(rpp_device * dev, uint32_t base, rpp_out_drv_t * drv)
{
    int ret = 0;

    /* simple sanity check */
    if ( !dev || !drv )
    {
        return ( -EINVAL );
    }

    /* call the initialization routines of all sub-units. */
    DO_INIT(gamma_out, base + RPP_ADDR_BASE_OUT_GAMMA_OUT);
    DO_INIT(ccor,      base + RPP_ADDR_BASE_OUT_CCOR);
    DO_INIT(outregs,   base + RPP_ADDR_BASE_OUT_OUTREGS);
    DO_INIT(is,        base + RPP_ADDR_BASE_OUT_IS);
    DO_INIT(out_if,    base + RPP_ADDR_BASE_OUT_OUT_IF);

    return ret;
}

/******************************************************************************
 * rpp_out_set_isp_rgb
 *****************************************************************************/
int rpp_out_set_isp_rgb(rpp_out_drv_t * drv, rpp_gamma_out_curve_t * curve)
{
    int ret;

    /* simple sanity checks. */
    if ( !drv || !curve ) {
        return -EINVAL;
    }

    /* configure the gamma curve. */
    ret = rpp_gamma_out_set_curve(&drv->gamma_out, curve);
    if (!ret)
        ret = rpp_gamma_out_enable(&drv->gamma_out, 1);

    /* CCOR is bypassed in hardware... */

    /* configure the OUTREGS input multiplexer */
    if (!ret)
        ret = rpp_outregs_set_mode(&drv->outregs,
                RPP_OUT_IN_SEL_MAX, RPP_OUT_IN_SEL_MAIN);
    /* configure the OUTREGS output format */
    if (!ret)
        ret = rpp_outregs_set_format(&drv->outregs,
                RPP_OUT_FORMAT_RGB8);

    /* that's it, already. */
    return ret;
}

/******************************************************************************
 * rpp_out_set_isp_yuv
 *****************************************************************************/
int rpp_out_set_isp_yuv(rpp_out_drv_t * drv,
        rpp_gamma_out_curve_t *     curve,
        rpp_ccor_profile_t *        rgb2yuv,
        unsigned                    legal,
        rpp_out_422_method_t        method_422,
        unsigned                    output_420
        )
{
    int ret;
    rpp_out_format_t   fmt;
    rpp_ccor_range_t   range;

    /* simple sanity checks. */
    if ( !drv || !curve || !rgb2yuv)
    {
        return -EINVAL;
    }
    
    fmt = output_420 ? RPP_OUT_FORMAT_YUV420 : RPP_OUT_FORMAT_YUV422;
    range = legal ? RPP_CCOR_RANGE_LEGAL : RPP_CCOR_RANGE_FULL;

    /* configure the gamma curve. */
    ret = rpp_gamma_out_set_curve(&drv->gamma_out, curve);
    if (!ret)
        ret = rpp_gamma_out_enable(&drv->gamma_out, 1);

    /* configure the color-space conversion. */
    if (!ret)
        ret = rpp_ccor_set_range_and_profile(&drv->ccor,
                range, range, rgb2yuv);

    /* configure the OUTREGS input multiplexer */
    if (!ret)
        ret = rpp_outregs_set_mode(&drv->outregs,
                RPP_OUT_IN_SEL_MAX, RPP_OUT_IN_SEL_MAIN);
    /* configure the OUTREGS output format */
    if (!ret)
        ret = rpp_outregs_set_format(&drv->outregs, fmt);
    /* configure the OUTREGS 4:2:2 subsampling method. */
    if (!ret)
        ret = rpp_outregs_set_422_method(&drv->outregs, method_422);

    /* that's it already. */
    return ret;
}

/******************************************************************************
 * rpp_out_set_pre1_yuv
 *****************************************************************************/
static int rpp_out_set_prex_yuv(rpp_out_drv_t * drv, rpp_out_input_t input,
        unsigned output_420)
{
    int ret;
    rpp_out_format_t   fmt;

    /* simple sanity check */
    if ( !drv )
        return -EINVAL;

    /* get the required output format. */
    fmt = output_420 ? RPP_OUT_FORMAT_YUV420 : RPP_OUT_FORMAT_YUV422;

    /* GAMMA_OUT and CCOR are bypassed in this mode. */

    /* configure the OUTREGS input multiplexer as required */
    ret = rpp_outregs_set_mode(&drv->outregs,
            RPP_OUT_IN_SEL_MAX, input);
    /* configure the OUTREGS output format */
    if (!ret)
        ret = rpp_outregs_set_format(&drv->outregs, fmt);

    /* 4:2:2 subsampling is bypassed in this mode, as the input
     * is already in 4:2:2 format. */

    /* that's it already. */
    return ret;
}

int rpp_out_set_pre1_yuv(rpp_out_drv_t * drv, unsigned output_420)
{
    return rpp_out_set_prex_yuv(drv, RPP_OUT_IN_SEL_PRE1, output_420);
}

/******************************************************************************
 * rpp_out_set_pre2_yuv
 *****************************************************************************/
int rpp_out_set_pre2_yuv(rpp_out_drv_t * drv, unsigned output_420)
{
    return rpp_out_set_prex_yuv(drv, RPP_OUT_IN_SEL_PRE2, output_420);
}


/******************************************************************************
 * rpp_out_set_pre3_yuv
 *****************************************************************************/
int rpp_out_set_pre3_yuv(rpp_out_drv_t * drv, unsigned output_420)
{
    return rpp_out_set_prex_yuv(drv, RPP_OUT_IN_SEL_PRE3, output_420);
}

/******************************************************************************
 * rpp_out_enable
 *****************************************************************************/
int rpp_out_enable(rpp_out_drv_t * drv, unsigned num_frames, unsigned data_mode)
{
    int ret;

    /* simple sanity check. */
    if ( !drv )
    {
        return -EINVAL;
    }

    /* configure the number of output frames */
    ret = rpp_out_if_set_frames(&drv->out_if, num_frames);
    
    /* configure DATA vs. VIDEO mode. */
    if (!ret)
        ret = rpp_out_if_set_data_mode(&drv->out_if, data_mode);

    /* and set the interface active at next FRAME_START */
    if (!ret)
        rpp_out_if_set_active(&drv->out_if, 1);

    return ret;
}

/******************************************************************************
 * rpp_out_disable
 *****************************************************************************/
int rpp_out_disable(rpp_out_drv_t * drv)
{
    /* simple sanity check. */
    if ( !drv )
    {
        return -EINVAL;
    }

    return rpp_out_if_set_active(&drv->out_if, 0);
}

