/******************************************************************************
 *
 * Copyright 2019 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_rmap_drv.c
 *
 * @brief   Implementation of radiance mapping unit driver
 *
 *****************************************************************************/
#include <rpp_rmap_meas_drv.h>

// make sure that the base is 0 in the auto-generated
// header-file, before including it
#define RPP_RMAP_MEAS_BASE 0
#include <rpp_rmap_meas_regs_addr_map.h>
#include <rpp_rmap_meas_regs_mask.h>

/******************************************************************************
 * local definitions
 *****************************************************************************/

static inline int check_valid(const rpp_rmap_meas_drv_t * drv)
{
    return (drv && drv->dev && drv->colorbits_high && drv->colorbits_low);
}


/******************************************************************************
 * rpp_rmap_meas_init
 *****************************************************************************/
int rpp_rmap_meas_init(rpp_device * dev, uint32_t base, rpp_rmap_meas_drv_t * drv)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !dev || !drv )
    {
        return ( -EINVAL );
    }

    drv->dev = dev;
    drv->base = base;
    drv->version = 0;
    drv->colorbits_low = 0;
    drv->colorbits_high = 0;

    /* read and parse the version register. */
    ret = READL(RPP_RPP_RMAP_MEAS_VERSION_REG, &v);
    if (!ret) {
        drv->version = REG_GET_SLICE(v, RPP_RMAP_MEAS_RMAP_MEAS_VERSION);
        /* there is only one usable version of the module which defines
         * 24/12/12-bit color bit depths. */
        if (drv->version == 0x03) {
            drv->colorbits_high = 24;
            drv->colorbits_low  = 12;
        } else {
            ret = -ENODEV;
        }
    }

    return ret;
}

/******************************************************************************
 * rpp_rmap_meas_enable
 *****************************************************************************/
int rpp_rmap_meas_enable(rpp_rmap_meas_drv_t * drv, const unsigned mode)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    /* range check */
    if ( !(mode<RPP_RMAP_MEASURE_MODE_MAX) )
    {
        return ( -ERANGE );
    }

    ret = READL(RPP_RPP_RMAP_MEAS_MODE_REG, &v);
    if (!ret) {
        REG_SET_SLICE( v, RPP_RMAP_MEAS_RMAP_MEAS_MODE, mode);
        ret = WRITEL(RPP_RPP_RMAP_MEAS_MODE_REG, v);
    }

    return ret;
}

/******************************************************************************
 * rpp_rmap_meas_enabled
 *****************************************************************************/
int rpp_rmap_meas_enabled(rpp_rmap_meas_drv_t * drv, unsigned * const mode)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) || !mode )
    {
        return ( -EINVAL );
    }

    ret = READL(RPP_RPP_RMAP_MEAS_MODE_REG, &v);
    if (!ret) {
        *mode = REG_GET_SLICE( v, RPP_RMAP_MEAS_RMAP_MEAS_MODE );
    }

    return ret;
}

/******************************************************************************
 * rpp_rmap_meas_set_thresholds
 *****************************************************************************/
int rpp_rmap_meas_set_thresholds(rpp_rmap_meas_drv_t * drv, const uint16_t vs_min,
        const uint16_t s_min, const uint16_t s_max, const uint32_t l_max)
{
    uint32_t regs[4];

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    /* range check */
    if ( REG_RANGE_CHECK(vs_min, RPP_RMAP_MEAS_RMAP_MEAS_MIN_THRES_VERY_SHORT)  ||
         REG_RANGE_CHECK( s_min, RPP_RMAP_MEAS_RMAP_MEAS_MIN_THRES_SHORT)       ||
         REG_RANGE_CHECK( s_max, RPP_RMAP_MEAS_RMAP_MEAS_MAX_THRES_SHORT)       ||
         REG_RANGE_CHECK( l_max, RPP_RMAP_MEAS_RMAP_MEAS_MAX_THRES_LONG)        )
    {
        return ( -ERANGE );
    }

    regs[0] = vs_min;
    regs[1] =  s_min;
    regs[2] =  s_max;
    regs[3] =  l_max;

    return WRITE_REGS(RPP_RPP_RMAP_MEAS_MIN_THRES_VERY_SHORT_REG, regs, 4);
}

/******************************************************************************
 * rpp_rmap_meas_thresholds
 *****************************************************************************/
int rpp_rmap_meas_thresholds(rpp_rmap_meas_drv_t * drv, uint16_t * const vs_min,
        uint16_t * const s_min, uint16_t * const s_max, uint32_t * const l_max)
{
    uint32_t regs[4];
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) || !vs_min || !s_min || !s_max || !l_max )
    {
        return ( -EINVAL );
    }

    ret = READ_REGS(RPP_RPP_RMAP_MEAS_MIN_THRES_VERY_SHORT_REG, regs, 4);
    if (!ret) {
        *vs_min = REG_GET_SLICE( regs[0], RPP_RMAP_MEAS_RMAP_MEAS_MIN_THRES_VERY_SHORT);
        *s_min  = REG_GET_SLICE( regs[1], RPP_RMAP_MEAS_RMAP_MEAS_MIN_THRES_SHORT);
        *s_max  = REG_GET_SLICE( regs[2], RPP_RMAP_MEAS_RMAP_MEAS_MAX_THRES_SHORT);
        *l_max  = REG_GET_SLICE( regs[3], RPP_RMAP_MEAS_RMAP_MEAS_MAX_THRES_LONG);
    }

    return ret;
}

/******************************************************************************
 * rpp_rmap_meas_set_meas_window
 *****************************************************************************/
int rpp_rmap_meas_set_meas_window(rpp_rmap_meas_drv_t * drv,
        const rpp_window_t * win, const rpp_rmap_step_t * steps)
{
    uint32_t regs[4];
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) || !win || !steps )
    {
        return ( -EINVAL );
    }

    /* range check */
    if (
            REG_RANGE_CHECK(win->hoff  , RPP_RMAP_MEAS_RMAP_MEAS_H_OFFS)  ||
            REG_RANGE_CHECK(win->voff  , RPP_RMAP_MEAS_RMAP_MEAS_V_OFFS)  ||
            REG_RANGE_CHECK(win->width , RPP_RMAP_MEAS_RMAP_MEAS_H_SIZE)  ||
            REG_RANGE_CHECK(win->height, RPP_RMAP_MEAS_RMAP_MEAS_V_SIZE)  ||
            REG_RANGE_CHECK(steps->v_stepsize, RPP_RMAP_MEAS_V_STEPSIZE)  ||
            REG_RANGE_CHECK(steps->h_step_inc, RPP_RMAP_MEAS_H_STEP_INC)
       )
    {
        return ( -ERANGE );
    }

    /* sub-sampling configuration */
    regs[0] = 0;
    REG_SET_SLICE(regs[0], RPP_RMAP_MEAS_V_STEPSIZE, steps->v_stepsize);
    REG_SET_SLICE(regs[0], RPP_RMAP_MEAS_H_STEP_INC, steps->h_step_inc);
    ret = WRITEL(RPP_RPP_RMAP_MEAS_SUBSAMPLING_REG, regs[0]);

    /* measurement window */
    if (!ret) {
        regs[0] = win->hoff;
        regs[1] = win->voff;
        regs[2] = win->width;
        regs[3] = win->height;
        ret = WRITE_REGS(RPP_RPP_RMAP_MEAS_H_OFFS_REG, regs, 4);
    }
    if (!ret) {
        /* for interrupt raising */
        /* height-1u => raise interrupt after last line in measurement window */
        ret = WRITEL(RPP_RPP_RMAP_MEAS_LAST_MEAS_LINE_REG, win->voff + win->height - 1);
    }

    return ret;
}

/******************************************************************************
 * rpp_rmap_meas_meas_window
 *****************************************************************************/
int rpp_rmap_meas_window(rpp_rmap_meas_drv_t * drv,
        rpp_window_t * const win, rpp_rmap_step_t * const steps)
{
    uint32_t regs[4];
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) || !win || !steps )
    {
        return ( -EINVAL );
    }

    ret = READ_REGS(RPP_RPP_RMAP_MEAS_H_OFFS_REG, regs, 4);
    if (!ret) {
        win->hoff   = regs[0];
        win->voff   = regs[1];
        win->width  = regs[2];
        win->height = regs[3];
        ret = READL(RPP_RPP_RMAP_MEAS_SUBSAMPLING_REG, regs);
    }

    if (!ret) {
        steps->v_stepsize = REG_GET_SLICE(regs[0], RPP_RMAP_MEAS_V_STEPSIZE);
        steps->h_step_inc = REG_GET_SLICE(regs[0], RPP_RMAP_MEAS_H_STEP_INC);
    }

    return ret;
}

/******************************************************************************
 * rpp_rmap_meas_sums
 *****************************************************************************/
int rpp_rmap_meas_sums(rpp_rmap_meas_drv_t * drv, rpp_rmap_sums_t * sums)
{
    uint32_t regs[8];
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) || !sums )
    {
        return ( -EINVAL );
    }

    ret = READ_REGS(RPP_RPP_RMAP_MEAS_LS_RESULTSHORT0_REG, regs, 8);
    if (!ret) {
#define GET64(IDX)  ((uint64_t)(regs[IDX])) + (((uint64_t)(regs[IDX+4])) << 32)
        sums->ls_short  = GET64(0);
        sums->ls_long   = GET64(1);
        sums->vss_short = GET64(2);
        sums->vss_vshort= GET64(3);
#undef GET64
    }

    return ret;
}


