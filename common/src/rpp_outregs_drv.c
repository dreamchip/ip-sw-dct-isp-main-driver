/******************************************************************************
 *
 * Copyright 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_outregs_drv.c
 *
 * @brief   Implementation of Human Vision Output Selection unit driver
 *
 *****************************************************************************/
#include <rpp_outregs_drv.h>

// make sure that the base is 0 in the auto-generated
// header-file, before including it
#define RPP_OUTREGS_BASE 0
#include <rpp_outregs_regs_addr_map.h>
#include <rpp_outregs_regs_mask.h>

/******************************************************************************
 * local definitions
 *****************************************************************************/

static inline int check_valid(const rpp_outregs_drv_t * drv)
{
    return (drv && drv->dev && drv->version);
}

/******************************************************************************
 * rpp_outregs_init
 *****************************************************************************/
int rpp_outregs_init(rpp_device * dev, uint32_t base, rpp_outregs_drv_t * drv)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !dev || !drv )
    {
        return ( -EINVAL );
    }
    drv->dev = dev;
    drv->base = base;
    drv->version = 0;

    /* read and parse the version register. */
    ret = READL(RPP_RPP_OUTREGS_VERSION_REG, &v);
    if (!ret)
    {
        drv->version = REG_GET_SLICE(v, RPP_OUTREGS_OUTREGS_VERSION);
        if (!drv->version)
        {
            ret = -ENODEV;
        }
    }

    return ret;
}

/******************************************************************************
 * rpp_outregs_set_format
 *****************************************************************************/
int rpp_outregs_set_format(rpp_outregs_drv_t * drv, rpp_out_format_t format)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return -EINVAL;
    }

    /* range-check the parameter. */
    if ( format > RPP_OUT_FORMAT_MAX )
    {
        return -ERANGE;
    }

    /* read-modify-write the configuration register. */
    ret = READL(RPP_RPP_OUT_FORMAT_REG, &v);
    if (!ret) {
        REG_SET_SLICE(v, RPP_OUTREGS_OUTPUT_FORMAT, format);
        ret = WRITEL(RPP_RPP_OUT_FORMAT_REG, v);
    }

    return ret;
}

/******************************************************************************
 * rpp_outregs_format
 *****************************************************************************/
int rpp_outregs_format(rpp_outregs_drv_t * drv, rpp_out_format_t * const format)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !format )
    {
        return -EINVAL;
    }

    /* read and parse the configuration register */
    ret = READL(RPP_RPP_OUT_FORMAT_REG, &v);
    if (!ret) {
        *format = REG_GET_SLICE(v, RPP_OUTREGS_OUTPUT_FORMAT);
    }

    return ret;
}

/******************************************************************************
 * rpp_outregs_set_422_method
 *****************************************************************************/
int rpp_outregs_set_422_method(rpp_outregs_drv_t * drv, rpp_out_422_method_t method)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return -EINVAL;
    }

    /* range-check the argument */
    if ( method > RPP_OUT_422_MAX )
    {
        return -ERANGE;
    }

    /* read-modify-write the configuration register. */
    ret = READL(RPP_RPP_OUT_CONV_422_METHOD_REG, &v);
    if (!ret) {
        REG_SET_SLICE(v, RPP_OUTREGS_CONV_422_METHOD, method);
        ret = WRITEL(RPP_RPP_OUT_CONV_422_METHOD_REG, v);
    }
    return ret;
}

/******************************************************************************
 * rpp_outregs_422_method
 *****************************************************************************/
int rpp_outregs_422_method(rpp_outregs_drv_t * drv, rpp_out_422_method_t * const method)
{
    uint32_t v;
    int ret;

    /* simple sanity checks. */
    if ( !check_valid(drv) || !method)
    {
        return -EINVAL;
    }

    /* read and parse the configuration register. */
    ret = READL(RPP_RPP_OUT_CONV_422_METHOD_REG, &v);
    if (!ret) {
        *method = REG_GET_SLICE(v, RPP_OUTREGS_CONV_422_METHOD);
    }

    return ret;
}

/******************************************************************************
 * rpp_outregs_set_mode
 *****************************************************************************/
int rpp_outregs_set_mode(rpp_outregs_drv_t * drv, uint8_t unselected_mode,
        rpp_out_input_t input_select)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return -EINVAL;
    }

    /* range-check parameters. */
    if (
            (unselected_mode > RPP_OUT_IN_SEL_MAX)  ||  /* limit on unselected_mode             */
            (input_select == 0)                     ||  /* input_select must not be zero        */
            (input_select > RPP_OUT_IN_SEL_MAX)     ||  /* limit on input_select                */
            (input_select & (input_select - 1) )        /* input_select must be power-of-two    */
       )
    {
        return -ERANGE;
    }

    /* read-modify-write the configuration register */
    ret = READL(RPP_RPP_OUT_MODE_REG, &v);
    if (!ret) {
        REG_SET_SLICE(v, RPP_OUTREGS_UNSELECTED_MODE, unselected_mode);
        REG_SET_SLICE(v, RPP_OUTREGS_IN_SEL         , input_select);
        ret = WRITEL(RPP_RPP_OUT_MODE_REG, v);
    }

    return ret;
}

/******************************************************************************
 * rpp_outregs_mode
 *****************************************************************************/
int rpp_outregs_mode(rpp_outregs_drv_t * drv, uint8_t * const unselected_mode,
        rpp_out_input_t * const input_select)
{
    uint32_t v;
    int ret;

    /* simple sanity checks. */
    if ( !check_valid(drv) || !unselected_mode || !input_select)
    {
        return -EINVAL;
    }

    /* read and parse the configuration register */
    ret = READL(RPP_RPP_OUT_MODE_REG, &v);
    if (!ret) {
        *unselected_mode    = REG_GET_SLICE(v, RPP_OUTREGS_UNSELECTED_MODE);
        *input_select       = REG_GET_SLICE(v, RPP_OUTREGS_IN_SEL);
    }

    return ret;
}

