/******************************************************************************
 *
 * Copyright 2019 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_awb_gain_drv.c
 *
 * @brief   Implementation of of white-balance gain driver
 *
 *****************************************************************************/
#include <rpp_awb_gain_drv.h>

// make sure that the base is 0 in the auto-generated
// header-file, before including it
#define RPP_AWB_GAIN_BASE 0
#include <rpp_awb_gain_regs_addr_map.h>
#include <rpp_awb_gain_regs_mask.h>

/******************************************************************************
 * local definitions
 *****************************************************************************/

static inline int check_valid(const rpp_awb_gain_drv_t * drv)
{
    return (drv && drv->dev);
}

/******************************************************************************
 * rpp_awb_gain_init
 *****************************************************************************/
int rpp_awb_gain_init(rpp_device * dev, uint32_t base, rpp_awb_gain_drv_t * drv)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !dev || !drv )
    {
        return ( -EINVAL );
    }
    drv->dev = dev;
    drv->base = base;
    drv->version = 0;

    /* read the version register. */
    ret = READL(RPP_RPP_AWB_GAIN_VERSION_REG, &v);
    if (!ret)
    {
        drv->version = REG_GET_SLICE(v, RPP_AWBGAIN_AWB_GAIN_VERSION);
        /* Earler versions that 0x03 had a different register layout
         * with two factors per register and are incompatible with
         * this driver. */
        if (drv->version < 0x03)
        {
            ret = -ENODEV;
        }
    }

    return ret;
}

/******************************************************************************
 * rpp_awb_gain_enable
 *****************************************************************************/
int rpp_awb_gain_enable(rpp_awb_gain_drv_t * drv, const unsigned on)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    ret = READL(RPP_RPP_AWB_ENABLE_REG, &v);
    if (!ret)
    {
        REG_SET_SLICE(v, RPP_AWBGAIN_AWB_GAIN_EN, on ? 1u : 0u );
        ret = WRITEL(RPP_RPP_AWB_ENABLE_REG, v);
    }

    return ret;
}

/******************************************************************************
 * rpp_awb_gain_enabled
 *****************************************************************************/
int rpp_awb_gain_enabled(rpp_awb_gain_drv_t * drv, unsigned * const on)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !on )
    {
        return ( -EINVAL );
    }

    ret = READL(RPP_RPP_AWB_ENABLE_REG, &v);
    if (!ret)
    {
        *on = REG_GET_SLICE(v, RPP_AWBGAIN_AWB_GAIN_EN);
    }

    return ret;
}

/******************************************************************************
 * rpp_awb_gain_set_gains
 *****************************************************************************/
int rpp_awb_gain_set_gains
(
    rpp_awb_gain_drv_t *    drv,
    const uint32_t          red,
    const uint32_t          green_red,
    const uint32_t          blue,
    const uint32_t          green_blue
)
{
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    /* Range check. The factors all have the same format - no need to use
     * different range masks. */
    if ( (REG_RANGE_CHECK( red       , RPP_AWBGAIN_AWB_GAIN_GR )) ||
         (REG_RANGE_CHECK( green_red , RPP_AWBGAIN_AWB_GAIN_GR )) ||
         (REG_RANGE_CHECK( blue      , RPP_AWBGAIN_AWB_GAIN_GR )) ||
         (REG_RANGE_CHECK( green_blue, RPP_AWBGAIN_AWB_GAIN_GR )) )
    {
        return ( -ERANGE );
    }

    /* write the values one-by-one. */
    ret = WRITEL(RPP_RPP_AWB_GAIN_GR_REG, green_red);
    if (!ret)
        ret = WRITEL(RPP_RPP_AWB_GAIN_GB_REG, green_blue);
    if (!ret)
        ret = WRITEL(RPP_RPP_AWB_GAIN_R_REG, red);
    if (!ret)
        ret = WRITEL(RPP_RPP_AWB_GAIN_B_REG, blue);

    return ret;
}

/******************************************************************************
 * rpp_awb_gain_gains
 *****************************************************************************/
int rpp_awb_gain_gains
(
    rpp_awb_gain_drv_t *    drv,
    uint32_t * const        red,
    uint32_t * const        green_red,
    uint32_t * const        blue,
    uint32_t * const        green_blue
)
{
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !red || !green_red || !blue || !green_blue )
    {
        return ( -EINVAL );
    }

    ret = READL(RPP_RPP_AWB_GAIN_GR_REG, green_red);
    if (!ret)
        ret = READL(RPP_RPP_AWB_GAIN_GB_REG, green_blue);
    if (!ret)
        ret = READL(RPP_RPP_AWB_GAIN_R_REG, red);
    if (!ret)
        ret = READL(RPP_RPP_AWB_GAIN_B_REG, blue);

    return ret;
}


