/******************************************************************************
 *
 * Copyright 2019 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_ccor_drv.c
 *
 * @brief   Implementation of color correction driver
 *
 *****************************************************************************/
#include <rpp_ccor_drv.h>

// make sure that the base is 0 in the auto-generated
// header-file, before including it
#define RPP_CCOR_BASE 0
#include <rpp_ccor_regs_addr_map.h>
#include <rpp_ccor_regs_mask.h>

/******************************************************************************
 * local definitions
 *****************************************************************************/

static inline int check_valid(const rpp_ccor_drv_t * drv)
{
    return (drv && drv->dev && drv->colorbits);
}


/******************************************************************************
 * rpp_ccor_init
 *****************************************************************************/
int rpp_ccor_init(rpp_device * dev, uint32_t base, rpp_ccor_drv_t * drv)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !dev || !drv )
    {
        return ( -EINVAL );
    }

    drv->dev = dev;
    drv->base = base;
    drv->version = 0;
    drv->props = 0;
    drv->colorbits = 0;

    /* read and parse the version register. */
    ret = READL(RPP_RPP_CCOR_VERSION_REG, &v);
    if (!ret)
    {
        drv->version = REG_GET_SLICE(v, RPP_CCOR_CCOR_VERSION);
        switch(drv->version)
        {
            case 0x03:
                drv->colorbits = 12;
                break;
            case 0x04:
                drv->colorbits = 20;
                break;
            case 0x05:
                drv->colorbits = 24;
                break;
            default:
                ret = -ENODEV;
        }
    }

    /* The version and colorbits is known, read the "config_type"
     * register next. It is used to cache capability information
     * used by the driver. */
    if (!ret)
        ret = READL(RPP_RPP_CCOR_CONFIG_TYPE_REG, &v);

    if (!ret)
        /* use only the last byte here, the register contains
         * only two valid bits. */
        drv->props = v & 0xFF;

    return ret;
}



/******************************************************************************
 * rpp_ccor_set_profile
 *****************************************************************************/
int rpp_ccor_set_profile(rpp_ccor_drv_t * drv, const rpp_ccor_profile_t * const p)
{
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !p )
    {
        return ( -EINVAL );
    }

    /* check the profile validity */
    ret = rpp_ccor_check_profile(drv->colorbits, p);

    /* Program the profile, if OK. */
    if (!ret)
        ret = rpp_ccor_write_profile(
                drv->dev,
                drv->base,
                RPP_RPP_CCOR_COEFF__0_REG,
                drv->colorbits,
                p
                );

    return ret;
}


/******************************************************************************
 * rpp_ccor_profile
 *****************************************************************************/
int rpp_ccor_profile(rpp_ccor_drv_t * drv, rpp_ccor_profile_t * const p)
{
    /* simple sanity checks */
    if ( !check_valid(drv) || !p )
    {
        return ( -EINVAL );
    }

    /* read and return the profile */
    return rpp_ccor_read_profile(
            drv->dev,
            drv->base,
            RPP_RPP_CCOR_COEFF__0_REG,
            drv->colorbits,
            p
            );
}

/******************************************************************************
 * rpp_ccor_has_pre_offset
 *****************************************************************************/
int rpp_ccor_has_pre_offset(const rpp_ccor_drv_t * drv)
{
    if ( !check_valid(drv) )
    {
        return -EINVAL;
    }
    return !!(drv->props & RPP_CCOR_USE_OFFSETS_AS_PRE_OFFSETS_MASK);
}


/******************************************************************************
 * rpp_ccor_has_legal_range
 *****************************************************************************/
int rpp_ccor_has_legal_range(const rpp_ccor_drv_t * drv)
{
    if ( !check_valid(drv) )
    {
        return -EINVAL;
    }
    return !!(drv->props & RPP_CCOR_CCOR_RANGE_AVAILABLE_MASK);
}


/******************************************************************************
 * rpp_ccor_set_range
 *****************************************************************************/
int rpp_ccor_set_range(
        rpp_ccor_drv_t *    drv,
        rpp_ccor_range_t    luma,
        rpp_ccor_range_t    chroma
        )
{
    uint32_t v;
    unsigned requested;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return -EINVAL;
    }

    /* check luma and chroma ranges. */
    if ( (luma > RPP_CCOR_RANGE_MAX) || (chroma > RPP_CCOR_RANGE_MAX) )
    {
        return -ERANGE;
    }

    /* check whether legal range is supported and requested. if it is
     * requested but not supported, return an error. */
    requested = (luma == RPP_CCOR_RANGE_LEGAL) || (chroma == RPP_CCOR_RANGE_LEGAL);

    if (requested && !rpp_ccor_has_legal_range(drv))
    {
        return -ERANGE;
    }

    /* Program the values now. Note that the luma/chroma values
     * are usable as slice values because they have been
     * range-checked above. */
    v = 0;
    REG_SET_SLICE(v, RPP_CCOR_CCOR_Y_RANGE, luma);
    REG_SET_SLICE(v, RPP_CCOR_CCOR_C_RANGE, chroma);

    return WRITEL(RPP_RPP_CCOR_RANGE_REG, v);
}

/******************************************************************************
 * rpp_ccor_get_range
 *****************************************************************************/
int rpp_ccor_get_range(
        rpp_ccor_drv_t *    drv,
        rpp_ccor_range_t *  luma,
        rpp_ccor_range_t *  chroma
        )
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !luma || !chroma )
    {
        return -EINVAL;
    }

    /* read the register now into a temporary variable. */
    ret = READL(RPP_RPP_CCOR_RANGE_REG, &v);

    /* if successful, extract the information */
    if (!ret)
    {
        *luma   = REG_GET_SLICE(v, RPP_CCOR_CCOR_Y_RANGE);
        *chroma = REG_GET_SLICE(v, RPP_CCOR_CCOR_C_RANGE);
    }

    return ret;
}


/******************************************************************************
 * rpp_ccor_set_range_and_profile
 *****************************************************************************/
int rpp_ccor_set_range_and_profile(
        rpp_ccor_drv_t *        drv,
        rpp_ccor_range_t        luma,
        rpp_ccor_range_t        chroma,
        const rpp_ccor_profile_t * const p
        )
{
    int ret;
    unsigned i, do_scale;
    int32_t nrange;
    rpp_ccor_profile_t tmp_p;

    /* simple sanity checks */
    if ( !check_valid(drv) || !p)
    {
        return -EINVAL;
    }

    /* check profile. */
    ret = rpp_ccor_check_profile(drv->colorbits, p);
    if (ret)
    {
        return ret;
    }

    /* Now that this is done, check whether the range can be configured.
     * If not, abort the mission here. */
    ret = rpp_ccor_set_range(drv, luma, chroma);
    if (ret)
    {
        return ret;
    }

    /* Check whether legal range is wanted at all. if not, simply program
     * the profile right away. */
    if ( (luma == RPP_CCOR_RANGE_FULL) && (chroma == RPP_CCOR_RANGE_FULL) )
    {
        return rpp_ccor_set_profile(drv, p);
    }

    /* The supplied profile shall not be modified, so copy information into
     * a temporary variable. The offsets go without modification. */
    for (i = 0; i < RPP_CCOR_OFF_NO; ++i)
    {
        tmp_p.off[i] = p->off[i];
    }

    /* Copy or rescale matrix coefficients for the luma component:
     * from 0..255 to 16..235.
     * Old range contains 256 values and division by 256 is the same
     * as a right-shift by 8 bits. */
    nrange = 235 - 16 + 1;
    do_scale = (luma == RPP_CCOR_RANGE_LEGAL);
    for (i = 0; i < 3; ++i) {
        int32_t tmp = p->coeff[i];
        tmp_p.coeff[i] = (do_scale) ? ((tmp * nrange) >> 8) : tmp;
    }

    /* Copy or rescale matrix coefficients for the chroma components:
     * from 0..255 to 16..235.
     * Old range contains 256 values and division by 256 is the same
     * as a right-shift by 8 bits. */
    nrange = 240 - 16 + 1;
    do_scale = (chroma == RPP_CCOR_RANGE_LEGAL);
    for (; i < RPP_CCOR_COEFF_NO; ++i) {
        int32_t tmp = p->coeff[i];
        tmp_p.coeff[i] = do_scale ? ((tmp * nrange) >> 8) : tmp;
    }

    /* matrix coefficients have been adjusted, program the profile and
     * return the result. */
    return rpp_ccor_set_profile(drv, &tmp_p);
}

/******************************************************************************
 * rpp_ccor_check_profile
 *****************************************************************************/
int rpp_ccor_check_profile(uint32_t colorbits, const rpp_ccor_profile_t * const p)
{
    int32_t vmin, vmax;
    unsigned i, coeff_bits;

    /* check how many bits matrix coefficients are allowed to carry. */
    coeff_bits = log2i(RPP_CCOR_CCOR_COEFF_MASK + 1);

    /* Calculate upper/lower bounds for the coefficients. The coefficients
     * are signed, so use proper min/max here. */
    vmin = SIGNED_MIN(coeff_bits);
    vmax = SIGNED_MAX(coeff_bits);

    /* check whether the coefficients are in range. */
    for (i = 0; i < RPP_CCOR_COEFF_NO; ++i)
    {
        if ( (p->coeff[i] < vmin) || (p->coeff[i] > vmax) )
        {
            return -ERANGE;
        }
    }

    /* Check whether the offsets are in range. Note that the offsets
     * are signed as the coefficients, and there's one bit more
     * than the 'colorbits' indicate to store the sign. */
    vmin = SIGNED_MIN(colorbits + 1);
    vmax = SIGNED_MAX(colorbits + 1);

    for (i = 0; i < RPP_CCOR_OFF_NO; ++i)
    {
        if ( (p->off[i] < vmin) || (p->off[i] > vmax) )
        {
            return -ERANGE;
        }
    }

    return 0;
}


/******************************************************************************
 * rpp_ccor_write_profile
 *****************************************************************************/
int rpp_ccor_write_profile(
        rpp_device *    dev,
        uint32_t        base,
        uint32_t        offset,
        uint32_t        colorbits,
        const rpp_ccor_profile_t * const p
        )
{
    int ret;
    unsigned i;
    uint32_t regs[RPP_CCOR_COEFF_NO];
    uint32_t off_mask;
    
    const uint32_t increment = sizeof(uint32_t);
    /* Well, the profile is likely to have been checked before,
     * as well as all other function parameters. This function
     * is an internal function and these facts can be assumed.
     *
     * Note that all values here are signed, which means there
     * is masking to be done before writing registers.
     */
    for (i = 0; i < RPP_CCOR_COEFF_NO; ++i)
    {
        regs[i] = p->coeff[i] & RPP_CCOR_CCOR_COEFF_MASK;
    }

    /* program the registers using efficient multiple register writes */
    ret = dev->write_regs(dev, base, offset, RPP_CCOR_COEFF_NO, increment, regs);

    if (ret)
    {
        return ret;
    }

    /* Now, go for the offset registers. again, these need masking
     * because they are signed values. */
    off_mask = UNSIGNED_MAX(colorbits + 1);
    for (i = 0; i < RPP_CCOR_OFF_NO; ++i)
    {
        regs[i] = p->off[i] & off_mask;
    }

    /* adjust the register number from coefficients to offsets */
    offset += RPP_CCOR_COEFF_NO * increment;

    /* And program the registers using efficient multiple register writes. */
    return dev->write_regs(dev, base, offset, RPP_CCOR_OFF_NO, increment, regs);
}


/******************************************************************************
 * rpp_ccor_read_profile
 *****************************************************************************/
int rpp_ccor_read_profile(
        rpp_device *    dev,
        uint32_t        base,
        uint32_t        offset,
        uint32_t        colorbits,
        rpp_ccor_profile_t * const p
        )
{
    int ret;
    uint32_t coeff_bits;
    unsigned i;

    const uint32_t increment = sizeof(uint32_t);

    /* Well, the profile is likely to have been checked before,
     * as well as all other function parameters. This function
     * is an internal function and these facts can be assumed.
     *
     * Note that all values here are signed, which means there
     * is masking and sign extension to be done after reading
     * registers.
     *
     * First, read the coefficient registers.
     */
    ret = dev->read_regs(dev, base, offset, RPP_CCOR_COEFF_NO, increment, (uint32_t *)p->coeff);
    if (ret)
    {
        return ret;
    }

    coeff_bits = log2i(RPP_CCOR_CCOR_COEFF_MASK + 1);

    for (i = 0; i < RPP_CCOR_COEFF_NO; ++i)
    {
        p->coeff[i] = sign_extend(p->coeff[i] & RPP_CCOR_CCOR_COEFF_MASK, coeff_bits);
    }

    /* Read the offset register after adjusting the register address from coefficients
     * to the offsets. */
    offset += RPP_CCOR_COEFF_NO * increment;

    ret = dev->read_regs(dev, base, offset, RPP_CCOR_OFF_NO, increment, (uint32_t *)p->off);

    /* if successful, mask and sign-extend the coefficients. */
    if (!ret)
    {
        const uint32_t cmask = UNSIGNED_MAX(colorbits + 1);

        for (i = 0; i < RPP_CCOR_OFF_NO; ++i)
        {
            p->off[i] = sign_extend(p->off[i] & cmask, colorbits + 1);
        }
    }

    return ret;
}

