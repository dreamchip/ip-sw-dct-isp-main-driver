/******************************************************************************
 *
 * Copyright 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_cac_drv.c
 *
 * @brief   Implementation of Chromatic Aberration Correction unit driver
 *
 *****************************************************************************/
#include <rpp_cac_drv.h>

// make sure that the base is 0 in the auto-generated
// header-file, before including it
#define RPP_FILT_BASE 0
#include <rpp_filt_regs_addr_map.h>
#include <rpp_filt_regs_mask.h>

/******************************************************************************
 * local definitions
 *****************************************************************************/

static inline int check_valid(const rpp_cac_drv_t * drv)
{
    return (drv && drv->dev);
}

/******************************************************************************
 * rpp_cac_init
 *****************************************************************************/
int rpp_cac_init(rpp_device * dev, uint32_t base, rpp_cac_drv_t * drv)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !dev || !drv )
    {
        return ( -EINVAL );
    }
    drv->dev = dev;
    drv->base = base;
    drv->version = 0;

    /* read and parse the version register. */
    ret = READL(RPP_RPP_CAC_VERSION_REG, &v);
    if (!ret)
    {
        drv->version = REG_GET_SLICE(v, RPP_FILT_CAC_VERSION);
        if (!drv->version)
        {
            ret = -ENODEV;
        }
    }

    return ret;
}

/******************************************************************************
 * rpp_cac_enable
 *****************************************************************************/
int rpp_cac_enable(rpp_cac_drv_t * drv, unsigned const on,
        uint16_t const h_start, uint16_t const v_start)
{
    uint32_t v;
    int ret;

    /* sanity checks */
    if ( !check_valid(drv) ) {
        return -EINVAL;
    }

    /* parameter checks. only required if enabled. */
    if (on) {
        if (
                (h_start < 1) || (h_start > RPP_FILT_H_COUNT_START_MASK) ||
                (v_start < 1) || (v_start > RPP_FILT_H_COUNT_START_MASK)
           )
        {
            return -ERANGE;
        }
    }

    /* program the registers now, starting with the CAC_COUNT_START reg. */
    v = 0;
    REG_SET_SLICE(v, RPP_FILT_H_COUNT_START, h_start);
    REG_SET_SLICE(v, RPP_FILT_V_COUNT_START, v_start);
    ret = WRITEL(RPP_RPP_CAC_COUNT_START_REG, v);

    /* if successful, read the control register for an
     * read-modify-write operation. */
    if (!ret) {
        ret = READL(RPP_RPP_CAC_CTRL_REG, &v);
    }

    /* if successful, update the enable bit. */
    if (!ret) {
        REG_SET_SLICE(v, RPP_FILT_CAC_EN, !!on);
        ret = WRITEL(RPP_RPP_CAC_CTRL_REG, v);
    }

    return ret;
}

/******************************************************************************
 * rpp_cac_enabled
 *****************************************************************************/
int rpp_cac_enabled(rpp_cac_drv_t * drv, unsigned * const on)
{
    uint32_t v;
    int ret;

    /* basic sanity checks */
    if ( !check_valid(drv) || !on)
    {
        return -EINVAL;
    }

    /* read the control register */
    ret = READL(RPP_RPP_CAC_CTRL_REG, &v);

    /* if successful, extract the enable bit. */
    if (!ret) {
        *on = REG_GET_SLICE(v, RPP_FILT_CAC_EN);
    }

    return ret;
}


/******************************************************************************
 * rpp_cac_get_state
 *****************************************************************************/
int rpp_cac_get_state(rpp_cac_drv_t * drv, unsigned * const on,
        uint16_t * const h_start, uint16_t * const v_start)
{
    uint32_t v;
    int ret;

    /* basic sanity checks */
    if ( !check_valid(drv) || !on || !h_start || !v_start )
    {
        return -EINVAL;
    }

    /* read the control register. */
    ret = READL(RPP_RPP_CAC_CTRL_REG, &v);

    /* if successful, extract enable state and read the COUNT_START_REG as well */
    if (!ret) {
        *on = REG_GET_SLICE(v, RPP_FILT_CAC_EN);
        ret = READL(RPP_RPP_CAC_COUNT_START_REG, &v);
    }

    /* if successul, extract the counter values. */
    if (!ret) {
        *h_start = REG_GET_SLICE(v, RPP_FILT_H_COUNT_START);
        *v_start = REG_GET_SLICE(v, RPP_FILT_V_COUNT_START);
    }

    return ret;

}

/******************************************************************************
 * rpp_cac_set_clip_mode
 *****************************************************************************/
int rpp_cac_set_clip_mode(rpp_cac_drv_t * drv,
        rpp_cac_h_clip_mode_t const h_clip, rpp_cac_v_clip_mode_t const v_clip)
{
    uint32_t v;
    int ret;

    /* basic sanity check */
    if ( !check_valid(drv) )
    {
        return -EINVAL;
    }

    /* parameter checks */
    if (
            (h_clip >= RPP_CAC_H_CLIP_MODE_INVALID) ||
            (v_clip >= RPP_CAC_V_CLIP_MODE_INVALID)
       )
    {
        return -ERANGE;
    }

    /* read-modify-write the control register. */
    ret = READL(RPP_RPP_CAC_CTRL_REG, &v);

    if (!ret) {
        REG_SET_SLICE(v, RPP_FILT_H_CLIP_MODE, h_clip);
        REG_SET_SLICE(v, RPP_FILT_V_CLIP_MODE, v_clip);
        ret = WRITEL(RPP_RPP_CAC_CTRL_REG, v);
    }

    return ret;

}

/******************************************************************************
 * rpp_cac_clip_mode
 *****************************************************************************/
int rpp_cac_clip_mode(rpp_cac_drv_t * drv, rpp_cac_h_clip_mode_t * const h_clip,
        rpp_cac_v_clip_mode_t * const v_clip)
{
    uint32_t v;
    int ret;

    /* basic sanity checks */
    if ( !check_valid(drv) || !h_clip || !v_clip )
    {
        return -EINVAL;
    }

    /* read the control register. */
    ret = READL(RPP_RPP_CAC_CTRL_REG, &v);

    /* if successful, extract values from that register. */
    if (!ret) {
        *h_clip = REG_GET_SLICE(v, RPP_FILT_H_CLIP_MODE);
        *v_clip = REG_GET_SLICE(v, RPP_FILT_V_CLIP_MODE);
    }

    return ret;
}

/******************************************************************************
 * rpp_cac_set_coeffs
 *****************************************************************************/
int rpp_cac_set_coeffs(rpp_cac_drv_t * drv, rpp_cac_coeffs_t const * const coeffs)
{
    uint32_t regs[3] = {0, 0, 0};
    unsigned coeff_bits;
    int16_t vmin, vmax;

    /* basic sanity checks */
    if ( !check_valid(drv) || !coeffs )
    {
        return -EINVAL;
    }

    coeff_bits = log2i(RPP_FILT_A_RED_MASK + 1);
    vmin = SIGNED_MIN(coeff_bits);
    vmax = SIGNED_MAX(coeff_bits);

#define RANGE_CHECK(coeff)  ((coeff < vmin) || (coeff > vmax))
    /* range-check the coefficients */
    if (
            RANGE_CHECK(coeffs->a_red)  ||
            RANGE_CHECK(coeffs->a_blue) ||
            RANGE_CHECK(coeffs->b_red)  ||
            RANGE_CHECK(coeffs->b_blue) ||
            RANGE_CHECK(coeffs->c_red)  ||
            RANGE_CHECK(coeffs->c_blue)
       )
    {
        return -ERANGE;
    }

    /* fill in the A coefficients. */
    REG_SET_SLICE(regs[0], RPP_FILT_A_RED,   coeffs->a_red);
    REG_SET_SLICE(regs[0], RPP_FILT_A_BLUE,  coeffs->a_blue);
    /* fill in the B coefficients. */
    REG_SET_SLICE(regs[1], RPP_FILT_B_RED,   coeffs->b_red);
    REG_SET_SLICE(regs[1], RPP_FILT_B_BLUE,  coeffs->b_blue);
    /* fill in the C coefficients. */
    REG_SET_SLICE(regs[2], RPP_FILT_C_RED,   coeffs->c_red);
    REG_SET_SLICE(regs[2], RPP_FILT_C_BLUE,  coeffs->c_blue);

    /* and write the registers using efficient multiple-register writes */
    return WRITE_REGS(RPP_RPP_CAC_A_REG, regs, 3);
}

/******************************************************************************
 * rpp_cac_coeffs
 *****************************************************************************/
int rpp_cac_coeffs(rpp_cac_drv_t * drv, rpp_cac_coeffs_t * const coeffs)
{
    uint32_t regs[3];
    int ret;
    unsigned coeff_bits;

    /* basic sanity checks */
    if ( !check_valid(drv) || !coeffs )
    {
        return -EINVAL;
    }

    coeff_bits = log2i(RPP_FILT_A_RED_MASK + 1);

    /* read the registers using efficient multiple-register reads */
    ret = READ_REGS(RPP_RPP_CAC_A_REG, regs, 3);

    /* if successful, decode the data. */
    if (!ret) {
        coeffs->a_red   = sign_extend(REG_GET_SLICE(regs[0], RPP_FILT_A_RED),    coeff_bits);
        coeffs->a_blue  = sign_extend(REG_GET_SLICE(regs[0], RPP_FILT_A_BLUE),   coeff_bits);
        coeffs->b_red   = sign_extend(REG_GET_SLICE(regs[1], RPP_FILT_B_RED),    coeff_bits);
        coeffs->b_blue  = sign_extend(REG_GET_SLICE(regs[1], RPP_FILT_B_BLUE),   coeff_bits);
        coeffs->c_red   = sign_extend(REG_GET_SLICE(regs[2], RPP_FILT_C_RED),    coeff_bits);
        coeffs->c_blue  = sign_extend(REG_GET_SLICE(regs[2], RPP_FILT_C_BLUE),   coeff_bits);
    }

    return ret;
}

/******************************************************************************
 * rpp_cac_set_norm
 *****************************************************************************/
int rpp_cac_set_norm(rpp_cac_drv_t * drv, rpp_cac_norm_t const * const params)
{
    uint32_t regs[2] = {0, 0};

    /* basic sanity checks */
    if ( !check_valid(drv) || !params )
    {
        return -EINVAL;
    }

    /* range-check the parameters */
    if (
            REG_RANGE_CHECK(params->x_ns, RPP_FILT_X_NS) ||
            REG_RANGE_CHECK(params->x_nf, RPP_FILT_X_NF) ||
            REG_RANGE_CHECK(params->y_ns, RPP_FILT_Y_NS) ||
            REG_RANGE_CHECK(params->y_nf, RPP_FILT_Y_NF)
       )
    {
        return -ERANGE;
    }

    /* fill the data into the registers now. */
    REG_SET_SLICE(regs[0], RPP_FILT_X_NS, params->x_ns);
    REG_SET_SLICE(regs[0], RPP_FILT_X_NF, params->x_nf);
    REG_SET_SLICE(regs[1], RPP_FILT_Y_NS, params->y_ns);
    REG_SET_SLICE(regs[1], RPP_FILT_Y_NF, params->y_nf);

    return WRITE_REGS(RPP_RPP_CAC_X_NORM_REG, regs, 2);
}

/******************************************************************************
 * rpp_cac_init
 *****************************************************************************/
int rpp_cac_norm(rpp_cac_drv_t * drv, rpp_cac_norm_t * const params)
{
    uint32_t regs[2];
    int ret;

    /* basic sanity checks */
    if ( !check_valid(drv) || !params )
    {
        return -EINVAL;
    }

    /* read the registers now. */
    ret = READ_REGS(RPP_RPP_CAC_X_NORM_REG, regs, 2);

    /* if successful, extract values. */
    if (!ret) {
        params->x_ns = REG_GET_SLICE(regs[0], RPP_FILT_X_NS);
        params->x_nf = REG_GET_SLICE(regs[0], RPP_FILT_X_NF);
        params->y_ns = REG_GET_SLICE(regs[1], RPP_FILT_Y_NS);
        params->y_nf = REG_GET_SLICE(regs[1], RPP_FILT_Y_NF);
    }

    return ret;
}



