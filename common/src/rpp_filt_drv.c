/******************************************************************************
 *
 * Copyright 2019 - 2021, Dream Chip Technologies GmbH. All rights reserved.
 * No part of this work may be reproduced, modified, distributed, transmitted,
 * transcribed, or translated into any language or computer format, in any form
 * or by any means without written permission of:
 * Dream Chip Technologies GmbH, Steinriede 10, 30827 Garbsen / Berenbostel,
 * Germany
 *
 *****************************************************************************/
/**
 * @file    rpp_filt_drv.c
 *
 * @brief   Implementation of debayer filter driver
 *
 *****************************************************************************/
#include <rpp_filt_drv.h>

// make sure that the base is 0 in the auto-generated
// header-file, before including it
#define RPP_FILT_BASE 0
#include <rpp_filt_regs_addr_map.h>
#include <rpp_filt_regs_mask.h>

/**************************************************************************//**
 * @brief sharpen factors, ISP uses these values to strength the highpass
 *        signal behind filtering
 *****************************************************************************/
typedef struct rpp_filt_sharpen_factor_s
{
    uint8_t     filt_fac_sh1;           /**< factor of sharpen range 1 */
    uint8_t     filt_fac_sh0;           /**< factor of sharpen range 0 */
    uint8_t     filt_fac_mid;           /**< factor of transition range bluring<->sharpen */
    uint8_t     filt_fac_bl0;           /**< factor of blurring range 1 */
    uint8_t     filt_fac_bl1;           /**< factor of blurring range 0 */
} rpp_filt_sharpen_factor_t;

/**************************************************************************//**
 * @brief threshold definitions, ISP uses these values to evaluate the five
 *        detail level.  
 *****************************************************************************/
typedef struct rpp_filt_texture_threshold_s
{
    uint32_t    filt_thresh_sh1;        /**< value > threshold_sh1 => detail-level = 4 */
    uint32_t    filt_thresh_sh0;        /**< value > threshold_sh0 => detail-level = 3 */
    uint32_t    filt_thresh_bl0;        /**< value < threshold_bl0 => detail-level = 1 */
    uint32_t    filt_thresh_bl1;        /**< value < threshold_bl1 => detail-level = 0 */
} rpp_filt_texture_threshold_t;

/**************************************************************************//**
 * @brief filter modes
 *****************************************************************************/
typedef struct rpp_filt_modes_s
{
    uint8_t    stage1;      /**< stage1 filter kernel */
    uint8_t    chroma_v;    /**< chroma vertical filter mode */
    uint8_t    chroma_h;    /**< chroma horizontal filter mode */
} rpp_filt_modes_t;

/**************************************************************************//**
 * sharpen factor table
 *****************************************************************************/
static const rpp_filt_sharpen_factor_t sharpen_factor_00_07[RPP_FILT_MAX_DETAIL_LVL + 1] =
{
    /* lvl      sh1 ,  sh0 ,  mid ,  bl0    bl1  */
    /*    0 */ { 0x04u, 0x04u, 0x04u, 0x02u, 0x00u },
    /*    1 */ { 0x08u, 0x07u, 0x06u, 0x02u, 0x00u },
    /*    2 */ { 0x0cu, 0x0au, 0x08u, 0x04u, 0x00u },
    /*    3 */ { 0x10u, 0x0cu, 0x0au, 0x06u, 0x02u },
    /*    4 */ { 0x16u, 0x10u, 0x0cu, 0x08u, 0x04u },
    /*    5 */ { 0x1bu, 0x14u, 0x10u, 0x0au, 0x04u },
    /*    6 */ { 0x20u, 0x1au, 0x13u, 0x0cu, 0x06u },
    /*    7 */ { 0x26u, 0x1eu, 0x17u, 0x10u, 0x08u },
    /*    8 */ { 0x2cu, 0x24u, 0x1du, 0x15u, 0x0du },
    /*    9 */ { 0x30u, 0x2au, 0x22u, 0x1au, 0x14u },
    /*   10 */ { 0x3fu, 0x30u, 0x28u, 0x24u, 0x20u },
};

static const rpp_filt_sharpen_factor_t sharpen_factor_08_10[RPP_FILT_MAX_DETAIL_LVL + 1] =
{
    /* lvl      sh1 ,  sh0 ,  mid ,  bl0    bl1  */
    /*    0 */ { 0x04u, 0x04u, 0x04u, 0x02u, 0x00u },
    /*    1 */ { 0x08u, 0x07u, 0x06u, 0x02u, 0x00u },
    /*    2 */ { 0x0cu, 0x0au, 0x08u, 0x04u, 0x00u },
    /*    3 */ { 0x10u, 0x0cu, 0x0au, 0x06u, 0x02u },
    /*    4 */ { 0x16u, 0x10u, 0x0cu, 0x08u, 0x04u },
    /*    5 */ { 0x1bu, 0x14u, 0x10u, 0x07u, 0x02u },
    /*    6 */ { 0x20u, 0x1au, 0x13u, 0x09u, 0x03u },
    /*    7 */ { 0x26u, 0x1eu, 0x17u, 0x0cu, 0x04u },
    /*    8 */ { 0x2cu, 0x24u, 0x1du, 0x0au, 0x03u },
    /*    9 */ { 0x30u, 0x2au, 0x22u, 0x0du, 0x05u },
    /*   10 */ { 0x3fu, 0x30u, 0x28u, 0x12u, 0x08u },
};

/**************************************************************************//**
 * filter threshold table
 *
 * The filter threshold register widths were increased by 8 bits
 * after this data table was prepared. To have the table valid, shift
 * all thresholds stored here by the 8 bits.
 *****************************************************************************/
static const rpp_filt_texture_threshold_t thresholds[RPP_FILT_MAX_DENOISE_LVL + 1] =
{
    /* lvl      sh1 ,  sh0 ,  bl0    bl1  */
    /*    0 */ {    0u,    0u,    0u,    0u },
    /*    1 */ {   33u,   18u,    8u,    2u },
    /*    2 */ {   44u,   26u,   13u,    5u },
    /*    3 */ {   51u,   36u,   23u,   10u },
    /*    4 */ {   67u,   41u,   26u,   15u },
    /*    5 */ {  100u,   75u,   50u,   20u },
    /*    6 */ {  120u,   90u,   60u,   26u },
    /*    7 */ {  150u,  120u,   80u,   51u },
    /*    8 */ {  200u,  170u,  140u,  100u },
    /*    9 */ {  300u,  250u,  180u,  150u },
    /*   10 */ { 1023u, 1023u, 1023u, 1023u },
};

/**************************************************************************//**
 * filter mode table
 *****************************************************************************/
static const rpp_filt_modes_t filter_modes[RPP_FILT_MAX_DENOISE_LVL + 1] =
{
    /* lvl     stage1,  chr_v , chr_h  */
    /*    0 */ {     6u,      1u,     0u },
    /*    1 */ {     6u,      3u,     3u },
    /*    2 */ {     4u,      3u,     3u },
    /*    3 */ {     4u,      3u,     3u },
    /*    4 */ {     3u,      3u,     3u },
    /*    5 */ {     3u,      3u,     3u },
    /*    6 */ {     2u,      3u,     3u },
    /*    7 */ {     2u,      3u,     3u },
    /*    8 */ {     2u,      3u,     3u },
    /*    9 */ {     2u,      3u,     3u },
    /*   10 */ {     2u,      3u,     3u },
};

static inline int check_valid(const rpp_filt_drv_t * drv)
{
    return (drv && drv->dev);
}


/******************************************************************************
 * rpp_filt_init
 *****************************************************************************/
int rpp_filt_init(rpp_device * dev, uint32_t base, rpp_filt_drv_t * drv)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !dev || !drv )
    {
        return ( -EINVAL );
    }
    drv->dev = dev;
    drv->base = base;
    drv->version = 0;

    /* read and parse the version register. */
    ret = READL(RPP_RPP_FILT_VERSION_REG, &v);
    if (!ret) {
        drv->version = REG_GET_SLICE(v, RPP_FILT_FILT_VERSION);
        /* this driver only work with version >= 0x04 because of the
         * wider threshold registers. */
        if (drv->version < 0x04)
            ret = -ENODEV;
    }

    return ret;
}

/******************************************************************************
 * rpp_filt_enable
 *****************************************************************************/
int rpp_filt_enable(rpp_filt_drv_t * drv, const unsigned on)
{
    uint32_t v;
    int ret;

    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    ret = READL(RPP_RPP_FILT_MODE_REG, &v);
    if (!ret) {
        REG_SET_SLICE(v, RPP_FILT_FILT_ENABLE, on ? 1u : 0u );
        ret = WRITEL(RPP_RPP_FILT_MODE_REG, v);
    }

    return ret;
}

/******************************************************************************
 * rpp_filt_enabled
 *****************************************************************************/
int rpp_filt_enabled(rpp_filt_drv_t * drv, unsigned * const on)
{
    uint32_t v;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) || !on )
    {
        return ( -EINVAL );
    }

    ret = READL(RPP_RPP_FILT_MODE_REG, &v);
    if (!ret) {
        *on = REG_GET_SLICE(v, RPP_FILT_FILT_ENABLE);
    }

    return ret;
}

/******************************************************************************
 * rpp_filt_set_filter_level
 *****************************************************************************/
int rpp_filt_set_filter_level(rpp_filt_drv_t * drv,
        uint8_t const detail_level, uint8_t const denoise_level)
{
    /* filter threshold register cache */
    uint32_t filt_thresh[4];
    uint32_t filt_fac[5];
    uint32_t filt_mode;

    /* temporary variables */
    const rpp_filt_sharpen_factor_t * factors;
    uint8_t stage1;
    int ret;

    /* simple sanity checks */
    if ( !check_valid(drv) )
    {
        return ( -EINVAL );
    }

    /* range check of parameter */
    if ( (detail_level > RPP_FILT_MAX_DETAIL_LVL)   ||
         (denoise_level > RPP_FILT_MAX_DENOISE_LVL) )
    {
        return ( -ERANGE );
    }

    // read current filter-mode stettings
    ret = READL(RPP_RPP_FILT_MODE_REG, &filt_mode);
    if (ret)
    {
        return ret;
    }

    /* copy data from filter thresholds table to register cache. the
     * table has data in wrong order and needs to be shifted. */
    filt_thresh[0] = thresholds[denoise_level].filt_thresh_bl0 << 8;
    filt_thresh[1] = thresholds[denoise_level].filt_thresh_bl1 << 8;
    filt_thresh[2] = thresholds[denoise_level].filt_thresh_sh0 << 8;
    filt_thresh[3] = thresholds[denoise_level].filt_thresh_sh1 << 8;

    // get stage_1 value from lookup table. for higher denoising levels,
    // values need to be adopted depending on the detail_level.
    stage1 = filter_modes[denoise_level].stage1;
    if ((denoise_level == 9) && (detail_level < 4) )
        stage1 = 1;
    if (denoise_level == 10) {
        if (detail_level < 6)
            stage1 = 1;
        if (detail_level < 4)
            stage1 = 0;
    }
    
    /* update the cached filter mode register value as needed. */
    REG_SET_SLICE( filt_mode, RPP_FILT_FILT_LP_SELECT , stage1   );
    REG_SET_SLICE( filt_mode, RPP_FILT_FILT_CHR_V_MODE, filter_modes[denoise_level].chroma_v );
    REG_SET_SLICE( filt_mode, RPP_FILT_FILT_CHR_H_MODE, filter_modes[denoise_level].chroma_h );
    REG_SET_SLICE( filt_mode, RPP_FILT_FILT_MODE, 1u );

    // The detail level determines the sharpening/blurring factors. There
    // are two tables for different denoising level ranges.
    factors = (denoise_level < 8) ?
        sharpen_factor_00_07 + detail_level :
        sharpen_factor_08_10 + detail_level ;

    filt_fac[0] = factors->filt_fac_sh1 << 2;
    filt_fac[1] = factors->filt_fac_sh0 << 2;
    filt_fac[2] = factors->filt_fac_mid << 2;
    filt_fac[3] = factors->filt_fac_bl0 << 2;
    filt_fac[4] = factors->filt_fac_bl1 << 2;

    /* now, write the register data. */
    ret = WRITE_REGS(RPP_RPP_FILT_THRESH_BL0_REG, filt_thresh, 4);
    if (!ret)
    {
        ret = WRITE_REGS(RPP_RPP_FILT_FAC_SH1_REG, filt_fac, 5);
    }
    if (!ret)
    {
        ret = WRITEL(RPP_RPP_FILT_MODE_REG, filt_mode);
    }

    return ret;
}

/******************************************************************************
 * rpp_filt_set_demosaic_params
 *****************************************************************************/
int rpp_filt_set_demosaic_params(rpp_filt_drv_t * drv,
        const unsigned bypass, const uint16_t threshold)
{
    uint32_t v = 0;
    
    /* simple sanity check */
    if ( !check_valid(drv) )
    {
        return -EINVAL;
    }

    /* range check the numerical parameters */
    if ( REG_RANGE_CHECK(threshold, RPP_FILT_DEMOSAIC_TH) )
    {
        return -ERANGE;
    }

    /* there are no other slices in the register than the one that
     * are going to be configured. the register does not need to
     * be read-modified-written. */
    REG_SET_SLICE(v, RPP_FILT_DEMOSAIC_BYPASS, !!bypass);
    REG_SET_SLICE(v, RPP_FILT_DEMOSAIC_TH, threshold);
    return WRITEL(RPP_RPP_DEMOSAIC_REG, v);
}


/******************************************************************************
 * rpp_filt_demosaic_params
 *****************************************************************************/
int rpp_filt_demosaic_params(rpp_filt_drv_t * drv,
        unsigned * const bypass, uint16_t * const threshold)
{
    uint32_t v = 0;
    int ret;
    
    /* simple sanity checks */
    if ( !check_valid(drv) || !bypass || !threshold)
    {
        return -EINVAL;
    }

    /* read the register */
    ret = READL(RPP_RPP_DEMOSAIC_REG, &v);

    /* if successful, extract parameters. */
    if (!ret)
    {
        *bypass = REG_GET_SLICE(v, RPP_FILT_DEMOSAIC_BYPASS);
        *threshold = REG_GET_SLICE(v, RPP_FILT_DEMOSAIC_TH);
    }

    return ret;
}


/******************************************************************************
 * rpp_filt_set_filter_params
 *****************************************************************************/
int rpp_filt_set_filter_params(rpp_filt_drv_t * drv, const rpp_filt_params_t * const params)
{
    uint32_t v;
    int ret;
    int errs = 0;
    uint32_t regs[5];

    /* basic sanity checks */
    if ( !check_valid(drv) || !params )
    {
        return -EINVAL;
    }

    /* range-check the values in params */
    errs += (params->filt_lp_select > 8);
    errs += (REG_RANGE_CHECK(params->chroma_h_mode, RPP_FILT_FILT_CHR_H_MODE) != 0);
    errs += (REG_RANGE_CHECK(params->chroma_v_mode, RPP_FILT_FILT_CHR_V_MODE) != 0);
    errs += (REG_RANGE_CHECK(params->luma_gain,     RPP_FILT_LUM_WEIGHT_GAIN) != 0);
    errs += (REG_RANGE_CHECK(params->luma_kink,     RPP_FILT_LUM_WEIGHT_KINK) != 0);
    errs += (REG_RANGE_CHECK(params->luma_min,      RPP_FILT_LUM_WEIGHT_MIN ) != 0);
    errs += (REG_RANGE_CHECK(params->fac_sh1,       RPP_FILT_FILT_FAC_SH1   ) != 0);
    errs += (REG_RANGE_CHECK(params->fac_sh0,       RPP_FILT_FILT_FAC_SH0   ) != 0);
    errs += (REG_RANGE_CHECK(params->fac_mid,       RPP_FILT_FILT_FAC_MID   ) != 0);
    errs += (REG_RANGE_CHECK(params->fac_bl0,       RPP_FILT_FILT_FAC_BL0   ) != 0);
    errs += (REG_RANGE_CHECK(params->fac_bl1,       RPP_FILT_FILT_FAC_BL1   ) != 0);
    errs += (REG_RANGE_CHECK(params->thresh_sh1,    RPP_FILT_FILT_THRESH_SH1) != 0);
    errs += (REG_RANGE_CHECK(params->thresh_sh0,    RPP_FILT_FILT_THRESH_SH0) != 0);
    errs += (REG_RANGE_CHECK(params->thresh_bl0,    RPP_FILT_FILT_THRESH_BL0) != 0);
    errs += (REG_RANGE_CHECK(params->thresh_bl1,    RPP_FILT_FILT_THRESH_BL1) != 0);

    if (errs)
    {
        return -ERANGE;
    }

    /* read the filt_mode register. */
    ret = READL(RPP_RPP_FILT_MODE_REG, &v);

    /* program the filt_mode register. */
    if (!ret)
    {
        REG_SET_SLICE(v, RPP_FILT_FILT_MODE,        !!params->filt_mode);
        REG_SET_SLICE(v, RPP_FILT_FILT_CHR_V_MODE,  params->chroma_v_mode);
        REG_SET_SLICE(v, RPP_FILT_FILT_CHR_H_MODE,  params->chroma_h_mode);
        REG_SET_SLICE(v, RPP_FILT_FILT_LP_SELECT,   params->filt_lp_select);
        ret = WRITEL(RPP_RPP_FILT_MODE_REG, v);
    }

    /* program the lum_weight register. */
    if (!ret)
    {
        ret = READL(RPP_RPP_FILT_LUM_WEIGHT_REG, &v);
        if (!ret)
        {
            REG_SET_SLICE(v, RPP_FILT_LUM_WEIGHT_GAIN, params->luma_gain);
            REG_SET_SLICE(v, RPP_FILT_LUM_WEIGHT_KINK, params->luma_kink);
            REG_SET_SLICE(v, RPP_FILT_LUM_WEIGHT_MIN , params->luma_min );
            ret = WRITEL(RPP_RPP_FILT_LUM_WEIGHT_REG, v);
        }
    }

    /* program the threshold registers. the structure does not store them
     * in register order, so copy the values. */
    if (!ret)
    {
        regs[0] = params->thresh_bl0;
        regs[1] = params->thresh_bl1;
        regs[2] = params->thresh_sh0;
        regs[3] = params->thresh_sh1;
        ret = WRITE_REGS(RPP_RPP_FILT_THRESH_BL0_REG, regs, 4);
    }
    /* program the sharpening factors now. they need to be converted from
     * uint16_t to register width. */
    if (!ret)
    {
        regs[0] = params->fac_sh1;
        regs[1] = params->fac_sh0;
        regs[2] = params->fac_mid;
        regs[3] = params->fac_bl0;
        regs[4] = params->fac_bl1;
        ret = WRITE_REGS(RPP_RPP_FILT_FAC_SH1_REG, regs, 5);
    }

    return ret;
}

/******************************************************************************
 * rpp_filt_filter_params
 *****************************************************************************/
int rpp_filt_filter_params(rpp_filt_drv_t * drv, rpp_filt_params_t * const params)
{
    uint32_t regs[11];
    int ret;

    /* basic sanity checks */
    if ( !check_valid(drv) || !params )
    {
        return -EINVAL;
    }

    /* read all the registers regardless of whether filtering is enabled or not. */
    ret = READ_REGS(RPP_RPP_FILT_MODE_REG, regs, 11);

    if (!ret)
    {
        /* reading was successful, so parse the registers now. start with the filt_mode register. */
        params->filt_mode       = REG_GET_SLICE(regs[ 0], RPP_FILT_FILT_MODE);
        params->filt_lp_select  = REG_GET_SLICE(regs[ 0], RPP_FILT_FILT_LP_SELECT);
        params->chroma_h_mode   = REG_GET_SLICE(regs[ 0], RPP_FILT_FILT_CHR_H_MODE);
        params->chroma_v_mode   = REG_GET_SLICE(regs[ 0], RPP_FILT_FILT_CHR_V_MODE);

        /* next come the threshold registers. */
        params->thresh_bl0      = REG_GET_SLICE(regs[ 1], RPP_FILT_FILT_THRESH_BL0);
        params->thresh_bl1      = REG_GET_SLICE(regs[ 2], RPP_FILT_FILT_THRESH_BL1);
        params->thresh_sh0      = REG_GET_SLICE(regs[ 3], RPP_FILT_FILT_THRESH_SH0);
        params->thresh_sh1      = REG_GET_SLICE(regs[ 4], RPP_FILT_FILT_THRESH_SH1);

        /* next comes the lum_weight register */
        params->luma_gain       = REG_GET_SLICE(regs[ 5], RPP_FILT_LUM_WEIGHT_GAIN);
        params->luma_kink       = REG_GET_SLICE(regs[ 5], RPP_FILT_LUM_WEIGHT_KINK);
        params->luma_min        = REG_GET_SLICE(regs[ 5], RPP_FILT_LUM_WEIGHT_MIN);

        /* finally, the sharpening factor registers. */
        params->fac_sh1         = REG_GET_SLICE(regs[ 6], RPP_FILT_FILT_FAC_SH1);
        params->fac_sh0         = REG_GET_SLICE(regs[ 7], RPP_FILT_FILT_FAC_SH0);
        params->fac_mid         = REG_GET_SLICE(regs[ 8], RPP_FILT_FILT_FAC_MID);
        params->fac_bl0         = REG_GET_SLICE(regs[ 9], RPP_FILT_FILT_FAC_BL0);
        params->fac_bl1         = REG_GET_SLICE(regs[10], RPP_FILT_FILT_FAC_BL1);
    }

    return ret;
}


