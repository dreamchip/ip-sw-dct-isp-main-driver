/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 *
 */

#include "isp_main_rpp.h"

/* White balance table */
struct {
	int cct;
	rpp_ccor_profile_t ccm;
	rpp_ccor_profile_t inv_ccm;
} white_balance_table[] = {
	[0] = { 10000,
		{ { 5909, -1137, -713, -1565, 7103, -1412, -248, -1870, 6221 },
		  { 0, 0, 0 } },
		{ { 611, 485, 3022, 2661, 688, 733, 824, 2923, 341 },
		  { 0, 0, 0 } } },
	[1] = { 8000,
		{ { 5972, -1152, -753, -1670, 7231, -1428, -206, -1983, 6277 },
		  { 0, 0, 0 } },
		{ { 615, 500, 2998, 2630, 689, 759, 851, 2907, 338 },
		  { 0, 0, 0 } } },
	[2] = { 7500,
		{ { 5987, -1148, -769, -1698, 7268, -1436, -193, -2025, 6301 },
		  { 0, 0, 0 } },
		{ { 613, 505, 2992, 2622, 691, 766, 861, 2900, 338 },
		  { 0, 0, 0 } } },
	[3] = { 7000,
		{ { 6006, -1148, -786, -1727, 7309, -1444, -182, -2065, 6327 },
		  { 0, 0, 0 } },
		{ { 613, 511, 2986, 2612, 692, 772, 870, 2892, 338 },
		  { 0, 0, 0 } } },
	[4] = { 6500,
		{ { 6013, -1123, -814, -1747, 7333, -1448, -171, -2114, 6357 },
		  { 0, 0, 0 } },
		{ { 606, 520, 2981, 2607, 693, 777, 883, 2883, 338 },
		  { 0, 0, 0 } } },
	[5] = { 6000,
		{ { 6031, -1104, -844, -1781, 7376, -1456, -154, -2176, 6395 },
		  { 0, 0, 0 } },
		{ { 601, 529, 2973, 2597, 695, 785, 898, 2872, 338 },
		  { 0, 0, 0 } } },
	[6] = { 5600,
		{ { 6051, -1108, -858, -1817, 7429, -1467, -138, -2224, 6421 },
		  { 0, 0, 0 } },
		{ { 602, 534, 2966, 2585, 697, 792, 908, 2866, 338 },
		  { 0, 0, 0 } } },
	[7] = { 5000,
		{ { 6042, -959, -994, -1885, 7457, -1418, -61, -2402, 6508 },
		  { 0, 0, 0 } },
		{ { 566, 575, 2959, 2575, 685, 810, 956, 2836, 327 },
		  { 0, 0, 0 } } },
	[8] = { 4000,
		{ { 6094, -630, -1350, -2053, 7558, -1337, 55, -2832, 6783 },
		  { 0, 0, 0 } },
		{ { 497, 678, 2914, 2542, 670, 850, 1057, 2748, 331 },
		  { 0, 0, 0 } } },
	[9] = { 3200,
		{ { 6175, 15, -2032, -2264, 7793, -1351, 186, -3712, 7479 },
		  { 0, 0, 0 } },
		{ { 395, 842, 2837, 2480, 689, 889, 1221, 2565, 371 },
		  { 0, 0, 0 } } },
	[10] = { 2800,
		 { { 6105, 540, -2465, -2452, 8329, -1713, 444, -4773, 8273 },
		   { 0, 0, 0 } },
		 { { 339, 910, 2818, 2394, 766, 906, 1363, 2421, 372 },
		   { 0, 0, 0 } } },

	[11] = { 0,
		 { { 4096, 0, 0, 0, 4096, 0, 0, 0, 4096 }, { 0, 0, 0 } },
		 { { 4096, 0, 0, 0, 4096, 0, 0, 0, 4096 }, { 0, 0, 0 } } },
};
#define DEFAULT_CCM_INDEX 5

/* Forward declarations */
int isp_main_rpp_set_rmap(struct device *dev, rpp_hdr_drv_t *rpp_hdr,
			  IspMainRppConfig_t *cfg, int num_inputs,
			  IspMainRmapSettings_t *rmap_cfg);

int isp_main_rpp_set_rgbdenoise(struct device *dev, rpp_hdr_drv_t *rpp_hdr,
				IspMainRppConfig_t *cfg,
				IspMainRgbDenConfig_t *rgbden_cfg);

int isp_main_rpp_set_shrpcnr(struct device *dev, rpp_hdr_drv_t *rpp_hdr,
			     IspMainRppConfig_t *cfg,
			     IspMainShrpConfig_t *shrpcnr_cfg);

#define CHECK(ret, expr, msg, ...)                        \
	if (!ret) {                                       \
		ret = expr;                               \
		if (ret)                                  \
			dev_err(dev, msg, ##__VA_ARGS__); \
	}

int isp_main_rpp_init(struct device *dev, rpp_hdr_drv_t *rpp_hdr,
		      IspMainRppConfig_t *cfg)
{
	int pipe = 0;

	/* Perform basic pipeline setup: this includes image acquisition
     * for given acquisition mode and image geometry configuration
     * throughout the pipeline.
     */
	int ret = 0;
	int num_inputs = 1;
	uint64_t pixels;
	uint32_t prc[3];

	rpp_window_t cap, win;
	rpp_window_t meas_win;
	enum rpp_acq_input_type_e acq_in_type;
	rpp_acq_properties_t acq_props;
	rpp_rmap_step_t rm_steps = {
		.v_stepsize = 1,
		.h_step_inc = 1 << 16,
	};
	rpp_hist_csm_t hist_csm = { 0x80, 0x80, 0x80 };
	rpp_hist_weights_t hist_w;
	rpp_hist_step_t hist_steps = {
		.v_stepsize = 1,
		.h_step_inc = 1 << 16,
	};
	rpp_ccor_profile_t prof;
	rpp_awb_meas_cond_t awb_meas_cond = {
		.rgb_valid = 1,
		.frames = 7,
		.rgb = {
			.max_b =  0xBFFFFF,
			.max_r =  0xBFFFFF,
			.max_g =  0xBFFFFF,
		},
	};

	rpp_main_pre_drv_t *const main_pre[3] = { &rpp_hdr->main_pre1,
						  &rpp_hdr->main_pre2,
						  &rpp_hdr->main_pre3 };
	rpp_awb_gain_drv_t *const post_awb_gain =
		&(rpp_hdr->main_post.awb_gain);
	rpp_filt_drv_t *const filt = &(rpp_hdr->main_post.filt);
	rpp_ccor_drv_t *const post_ccor = &(rpp_hdr->main_post.ccor);
	rpp_hist_drv_t *const hist = &(rpp_hdr->main_post.hist);
	rpp_rmap_drv_t *const rmap = &(rpp_hdr->rmap);
	rpp_rmap_meas_drv_t *const rmeas = &(rpp_hdr->rmap_meas);
	rpp_ltm_drv_t *const ltm = &(rpp_hdr->main_post.ltm);
	rpp_ltm_meas_drv_t *const ltm_meas = &(rpp_hdr->main_post.ltm_meas);
	rpp_awb_meas_drv_t *const awbm = &(rpp_hdr->main_post.awb_meas);
	rpp_rgbdenoise_drv_t *const rgbden = &(rpp_hdr->main_post.rgbdenoise);
	rpp_shrpcnr_drv_t *const shrp = &(rpp_hdr->main_post.shrpcnr);

	/* FIXME tuned gamma_in curve performing decompanding for IMX390 showcase */
	rpp_gamma_in_curve_sample_t gamma_in[RPP_GAMMA_IN_MAX_SAMPLES] = {
		{ 0, 0x000000, 0x000000, 0x000000 },
		{ 14, 0x004000, 0x004000, 0x004000 },
		{ 13, 0x008000, 0x008000, 0x008000 },
		{ 13, 0x010000, 0x010000, 0x010000 },
		{ 12, 0x020000, 0x020000, 0x020000 },
		{ 12, 0x040000, 0x040000, 0x040000 },
		{ 12, 0x080000, 0x080000, 0x080000 },
		{ 12, 0x100000, 0x100000, 0x100000 },
		{ 12, 0x200000, 0x200000, 0x200000 },
		{ 12, 0x400000, 0x400000, 0x400000 },
		{ 12, 0x800000, 0x800000, 0x800000 },
		{ 12, 0xFFFFFF, 0xFFFFFF, 0xFFFFFF },
		{ 2, 4045177, 4045177, 4045177 },
		{ 2, 4045177, 4045177, 4045177 },
		{ 2, 4045177, 4045177, 4045177 },
		{ 2, 4045177, 4045177, 4045177 },
		{ 2, 4045177, 4045177, 4045177 },
	};
	/* Default RMAP config */
	IspMainRmapSettings_t rmap_cfg = {
		.long_bits = 24,
		.automatic = false,
		.thr_long_min = 3000,
		.thr_long_max = 4050,
		.thr_shrt_min = 3000,
		.thr_shrt_max = 4080,
		.map_fac_s = 16,
		.map_fac_vs = 16,
		.vs_min = 0,
		.s_min = 0,
		.s_max = 0,
		.l_max = 0,
	};
	/* Default tonecurve */
	rpp_ltm_curve_t curve = {
		.dx = {
			0, 8, 8, 8, 8, 6, 6, 6, 6, 8, 9, 9, 9, 9, 9, 9, 9, 9, 9, 10, 10,
			10, 10, 10, 10, 10, 10, 11, 11, 11, 11, 11, 11, 11, 11, 11, 12,
			11, 11, 12, 12, 12, 12, 11, 10, 9,  8,  8,  12,
		},
		.y = {
			0, 77, 150,  220,  286, 302, 319, 334, 350, 411, 527, 634, 734,
			828,  917,  1001, 1081, 1157, 1229, 1365, 1490, 1605, 1714, 1815,
			1910, 2001, 2086, 2245, 2390, 2524, 2647, 2763, 2871, 2973, 3069,
			3160, 3329, 3408, 3483, 3625, 3756, 3879, 3994, 4048, 4075, 4088,
			4095, 4095, 4095,
		}
	};
	/* RGB Denoise config */
	IspMainRgbDenConfig_t rgbden_cfg = {
		.enabled = false,
		.y_shift = 0,
		.c_shift = 0,
	};
	/* Sharpen config */
	IspMainShrpConfig_t shrpcnr_cfg = {
		.sharpen_enabled = false,
		.sharpen_level = 64,
		.sharpen_thresh = 100,
		.cnr_enabled = false,
		.cnr_thresh = 150,
		.cad_enabled = true,
		.cad_parm = {
			.restore_lvl = 2,
			/* U plane CAD excluded area */
			.umin           = 0,
			.umax           = 82,  // 0.04
			///* V plane CAD excluded area for negative U values */
			.uneg_vmin      = 0,
			.uneg_vmax      = 0,
			///* V plane CAD excluded area for positive U values */
			.upos_vmin      = -51, // -0.025
			.upos_vmax      = 682, //  0.333
		},
		.desat_enabled = false,
		.desat_thresh = 1024,
		.desat_slope = 25,
	};

	SET_HIST_WEIGHTS(hist_w, 16);

	RPP_SET_WINDOW(cap, cfg->acq_h_off, cfg->acq_v_off, cfg->width,
		       cfg->height);
	RPP_SET_WINDOW(win, 0, 0, cfg->width, cfg->height);
	// align to multiple of 5
	RPP_SET_WINDOW(meas_win, 0, 0, (cfg->width / 5) * 5,
		       (cfg->height / 5) * 5);

	switch (cfg->sync_mode) {
	case SYNC_MODE_HDR_SENSOR_FUSION: /* FALLTHROUGH */
	case SYNC_MODE_SDR:
		num_inputs = 1;
		break;
	case SYNC_MODE_HDR_DUAL_EXPOSURE:
		num_inputs = 2;
		break;
	case SYNC_MODE_HDR_TRIPLE_EXPOSURE:
		num_inputs = 3;
		break;
	default:
		return -EINVAL;
	}

	/* remember the number of pipelines in use (limited by
     * hardware capabilities). */
	if (num_inputs > rpp_hdr->inputs)
		num_inputs = rpp_hdr->inputs;

	switch (cfg->px_size) {
	case PX_SIZE_24BIT:
		acq_in_type = RPP_ACQ_INPUT_TYPE_24b;
		rmap_cfg.long_bits = 24;
		break;
	case PX_SIZE_20BIT:
		acq_in_type = RPP_ACQ_INPUT_TYPE_20b;
		rmap_cfg.long_bits = 20;
		break;
	case PX_SIZE_16BIT:
		acq_in_type = RPP_ACQ_INPUT_TYPE_16b;
		rmap_cfg.long_bits = 16;
		break;
	case PX_SIZE_14BIT:
		acq_in_type = RPP_ACQ_INPUT_TYPE_14b;
		rmap_cfg.long_bits = 14;
		break;
	case PX_SIZE_12BIT:
		acq_in_type = RPP_ACQ_INPUT_TYPE_12b;
		rmap_cfg.long_bits = 12;
		break;
	case PX_SIZE_10BIT:
		acq_in_type = RPP_ACQ_INPUT_TYPE_10b;
		rmap_cfg.long_bits = 10;
		break;
	case PX_SIZE_8BIT:
		acq_in_type = RPP_ACQ_INPUT_TYPE_8b;
		rmap_cfg.long_bits = 8;
		break;
	default:
		return -EINVAL;
	}

	acq_props.bayer_pattern = RPP_ACQ_BAYPAT_RGGB;
	acq_props.input_type = acq_in_type;
	acq_props.sensor_align_in = RPP_ACQ_ALIGN_MSB;
	acq_props.sample_edge = 0;
	acq_props.hsync_polarity = 1;
	acq_props.vsync_polarity = 1;

	for (pipe = 0; pipe < 3; pipe++) {
		rpp_acq_drv_t *acq = &main_pre[pipe]->acq;
		rpp_gamma_in_drv_t *gin = &main_pre[pipe]->gamma_in;
		rpp_bls_drv_t *bls = &main_pre[pipe]->bls;
		rpp_lsc4_drv_t *lsc4 = &main_pre[pipe]->lsc4;
		rpp_awb_gain_drv_t *awb_gain = &main_pre[pipe]->awb_gain;
		rpp_dpcc_drv_t *dpcc = &main_pre[pipe]->dpcc;
		rpp_dpf_drv_t *dpf = &main_pre[pipe]->dpf;
		rpp_hist256_drv_t *hist256 =
			(pipe == 0) ? &main_pre[0]->hist256 : NULL;

		// skip MAIN_PRE3 if inputs < 2
		if (rpp_hdr->inputs <= pipe) {
			break;
		}

		/* set up ACQ */
		CHECK(ret, rpp_acq_set_mode(acq, RPP_ACQ_MODE_BAYER_BT601),
		      "Failed to set MAIN_PRE%d ACQ mode", pipe + 1);
		CHECK(ret, rpp_acq_set_acq_window(acq, &cap, &win),
		      "Failed to set MAIN_PRE%d ACQ windows", pipe + 1);
		CHECK(ret, rpp_acq_set_properties(acq, acq_props),
		      "Failed to set MAIN_PRE%d ACQ props", pipe + 1);
		CHECK(ret, rpp_acq_enable(acq, (num_inputs > pipe)),
		      "Failed to enable MAIN_PRE%d ACQ.", pipe + 1);

		/* set up gamma-in for MAIN_PRE1. In case of pre-fused, compressed data,
		* uncompression might need to be used. */
		if (pipe == 0) {
			CHECK(ret, rpp_gamma_in_set_curves(gin, gamma_in),
			      "Failed to set MAIN_PRE1 gamma_in curve.");
			CHECK(ret, rpp_gamma_in_enable(gin, 1),
			      "Failed to enable MAIN_PRE1 gamma_in");
		} else
			CHECK(ret, rpp_gamma_in_enable(gin, 0),
			      "Failed to disable MAIN_PRE%d gamma_in",
			      pipe + 1);

		/* set up BLS */
		CHECK(ret, rpp_bls_set_black_level(bls, 0, 0, 0, 0),
		      "Failed to configure MAIN_PRE%d BLS levels", pipe + 1);
		CHECK(ret, rpp_bls_enable(bls, 1),
		      "Failed to enable MAIN_PRE%d BLS", pipe + 1);

		/* set up LSC4 */
		CHECK(ret, rpp_lsc4_enable(lsc4, 0),
		      "Failed to disable LSC4 of MAIN_PRE%d", pipe + 1);

		/* set up AWB */
		CHECK(ret, rpp_awb_gain_enable(awb_gain, 0),
		      "Failed to set up White Balance settings.");

		/* set up DPCC  */
		CHECK(ret, rpp_dpcc_enable(dpcc, 0),
		      "Failed to set up Defect Pixel Detection");

		/* set up DPF */
		{
			rpp_dpf_mode_t mode = {
				.enable = 0,
				.b_filter_off = 0,
				.gb_filter_off = 0,
				.gr_filter_off = 0,
				.r_filter_off = 0,
				.rb_filter_size9x9 = 1,
				.nll_equ_segm = 0,
				.lsc_gain_en = 1,
				.use_nf_gain = 0,
			};
			rpp_dpf_spatial_weight_t dpf_spatial_weights = {
				.g = { 13, 10, 7, 3, 2, 1 },
				.rb = { 11, 7, 3, 2, 1, 0 },
			};
			rpp_dpf_spatial_strength_t dpf_spatial_strength = {
				.red = 128, .green = 128, .blue = 128
			};
			rpp_dpf_nll_coeff_t dpf_coeff = {
				.coeff_rb = { 1023, 27, 28, 22, 21, 18, 16, 16,
					      16, 16, 16, 16, 16, 16, 16, 16,
					      16 },
				.coeff_g = { 1023, 28, 31, 22, 21, 18, 16, 15,
					     15, 15, 15, 15, 15, 15, 15, 15,
					     15 }
			};
			CHECK(ret,
			      rpp_dpf_set_spatial_strength(
				      dpf, &dpf_spatial_strength),
			      "Failed to set up DPF spatial stength");
			CHECK(ret,
			      rpp_dpf_set_spatial_weight(dpf,
							 &dpf_spatial_weights),
			      "Failed to set up DPF spatial weights");
			CHECK(ret, rpp_dpf_set_nll_coeff(dpf, &dpf_coeff),
			      "Failed to set up DPF nll coeff");

			CHECK(ret, rpp_dpf_set_mode(dpf, &mode),
			      "Failed to set up Denoising Pre-Filter");
		}

		/* set up hist256 */
		if (hist256) {
			CHECK(ret, rpp_hist256_set_meas_window(hist256, &win),
			      "Failed to set MAIN_PRE1 HIST256 meas_window");
			CHECK(ret,
			      rpp_hist256_set_sample_range(hist256, 256, 0),
			      "Failed to set MAIN_PRE1 HIST256 sample range");
			CHECK(ret, rpp_hist256_gamma_enable(hist256, 0),
			      "Failed to enable MAIN_PRE1 HIST256 curve");
			CHECK(ret,
			      rpp_hist256_enable(hist256, 1,
						 RPP_HIST256_MEASURE_CHANNEL_6),
			      "Failed to enable MAIN_PRE1 HIST256");
		}
	}

	////////////////////////////////////////////////////////////////////////////

	/*
     *
     * set up RMAP_MEAS and RMAP.
     *
     */
	CHECK(ret, rpp_rmap_set_hsize(rmap, win.width),
	      "Failed to set RMAP line length.");
	switch (num_inputs) {
	default:
	case 1:
		CHECK(ret,
		      rpp_rmap_meas_enable(rmeas,
					   RPP_RMAP_MEASURE_MODE_DISABLE),
		      "Failed to disable RMAP_MEAS");
		break;
	case 2:
		/* enable RMAP measurements with some default set up */
		CHECK(ret,
		      rpp_rmap_meas_set_thresholds(rmeas, 250, 250, 4095, 4000),
		      "Failed to set up RMAP_MEAS thresholds.");
		CHECK(ret,
		      rpp_rmap_meas_set_meas_window(rmeas, &win, &rm_steps),
		      "Failed to set up RMAP_MEAS window.");
		CHECK(ret,
		      rpp_rmap_meas_enable(rmeas, RPP_RMAP_MEASURE_MODE_ENABLE),
		      "Failed to enable RMAP_MEAS.");
		break;
	case 3:
		/* enable RMAP measurements with some default set up */
		CHECK(ret,
		      rpp_rmap_meas_set_thresholds(rmeas, 250, 250, 4095, 4000),
		      "Failed to set up RMAP_MEAS thresholds.");
		CHECK(ret,
		      rpp_rmap_meas_set_meas_window(rmeas, &win, &rm_steps),
		      "Failed to set up RMAP_MEAS window.");
		CHECK(ret,
		      rpp_rmap_meas_enable(rmeas, RPP_RMAP_MEASURE_MODE_ENABLE),
		      "Failed to enable RMAP_MEAS.");
	}
	CHECK(ret,
	      isp_main_rpp_set_rmap(dev, rpp_hdr, cfg, num_inputs, &rmap_cfg),
	      "Failed to setup RMAP");

	/*
     *
     * set up MAIN_POST pipeline stage.
     *
     */

	/* set up MAIN_POST AWBG */
	CHECK(ret, rpp_awb_gain_enable(post_awb_gain, 0),
	      "Failed to set MAIN_POST AWB gains");

	/* set up MAIN_POST FILT */
	CHECK(ret, rpp_filt_set_demosaic_params(filt, 0, 100),
	      "Failed to set MAIN_POST FILT demosaic_params");
	CHECK(ret, rpp_filt_set_filter_level(filt, 0, 3),
	      "Failed to set MAIN_POST FILT filter_level");
	CHECK(ret, rpp_filt_enable(filt, 1), "Failed to enable MAIN_POST FILT");

	SET_CCOR_BYPASS(&prof);
	CHECK(ret,
	      rpp_ccor_set_profile(post_ccor,
				   &white_balance_table[DEFAULT_CCM_INDEX].ccm),
	      "Failed to set MAIN_POST CCOR profile");

	/* set up MAIN_POST HIST32 */
	CHECK(ret, rpp_hist_set_csm(hist, hist_csm),
	      "Failed to set MAIN_POST HIST32 CSM");
	CHECK(ret, rpp_hist_set_sample_range(hist, 0, 0),
	      "Failed to set MAIN_POST HIST32 sample range");
	CHECK(ret, rpp_hist_set_meas_window(hist, &meas_win, &hist_steps),
	      "Failed to set MAIN_POST HIST32 meas_window");
	CHECK(ret, rpp_hist_set_weights(hist, hist_w),
	      "Failed to set MAIN_POST HIST32 weights");
	CHECK(ret,
	      rpp_hist_set_mode(hist, RPP_HIST_MEASURE_MODE_DISABLE,
				RPP_HIST_MEASURE_CHANNEL_0),
	      "Failed to enable MAIN_POST HIST32");

	/* set up LTM and LTM_MEAS */
	/* set weights to r=0.25 / g=0.5 / b=0.25 */
	CHECK(ret, rpp_ltm_meas_set_weights(ltm_meas, 256, 511, 256),
	      "Failed to set LTM_MEAS Luminance weights.");
	CHECK(ret, rpp_ltm_meas_set_meas_window(ltm_meas, &win),
	      "Failed to set LTM_MEAS window.");
	/* set up PRC thresholds for 0.001, 0.25 and 0.999 percentiles */
	pixels = win.width * win.height;
	prc[0] = pixels / 1000;
	prc[1] = pixels / 4;
	prc[2] = pixels * 999 / 1000;
	CHECK(ret, rpp_ltm_meas_set_prc_thresholds(ltm_meas, 3, prc),
	      "Failed to set up LTM_MEAS percentile thresholds");
	CHECK(ret, rpp_ltm_meas_enable(ltm_meas, 1),
	      "Failed to enable LTM_MEAS.");

	CHECK(ret, rpp_ltm_set_linewidth(ltm, win.width),
	      "Failed to set LTM line width.");
	CHECK(ret, rpp_ltm_set_tonecurve(ltm, &curve),
	      "Failed to set tonecurve.");
	CHECK(ret, rpp_ltm_enable(ltm, 1), "Failed to enable LTM.");
	//CHECK(ret, rpp_ltm_enable(ltm, 0), "Failed to disable LTM.");

	/* set up AWB measurement. use RGB measurement which requires a
     * bypass CCOR profile. */
	SET_CCOR_BYPASS(&prof);
	CHECK(ret,
	      rpp_awb_meas_set_profile(
		      awbm, &white_balance_table[DEFAULT_CCM_INDEX].inv_ccm),
	      "Failed to set up AWBM CCOR profile.");
	CHECK(ret, rpp_awb_meas_set_condition(awbm, &awb_meas_cond),
	      "Failed tp set up AWBM condition.");

	CHECK(ret, rpp_awb_meas_set_window(awbm, &win),
	      "Failed to set up AWBM meas_window.");
	CHECK(ret, rpp_awb_meas_set_mode(awbm, RPP_AWB_MEAS_MODE_RGB),
	      "Failed to enable AWBM.");

	/* set up RGBDENOISE */

	/* set up BT.709 RGB2YUV profile. denoising works in YUV color space. */
	SET_CCOR_BT709_RGB_2_YUV(&prof, rgbden->colorbits);
	CHECK(ret, rpp_rgbdenoise_set_profile(rgbden, &prof),
	      "Failed to set RGBDENOISE CCOR profile.");
	CHECK(ret, isp_main_rpp_set_rgbdenoise(dev, rpp_hdr, cfg, &rgbden_cfg),
	      "Failed to set up RGB denoise.");

	/* set up YUV to BT.709 RGB profile for SHRPCNR. SHRPCNR also works
	 * in YUV but reverses the RGB2YUV transform from before. */
	SET_CCOR_YUV_2_BT709_RGB(&prof, shrp->colorbits);
	CHECK(ret, rpp_shrpcnr_set_profile(shrp, &prof),
	      "Failed to set SHRPCNR CCOR profile.");
	CHECK(ret, rpp_shrpcnr_set_linewidth(shrp, win.width),
	      "Failed to set SHRPCNR linewidth");
	CHECK(ret, isp_main_rpp_set_shrpcnr(dev, rpp_hdr, cfg, &shrpcnr_cfg),
	      "Failed to set up SHRPCNR.");

	/* Force shadow update */
	CHECK(ret, rpp_hdr_shadow_update(rpp_hdr, 1),
	      "Failed to set pipeline UPDATE flag.");

	return ret;
}

int isp_main_rpp_start_input(struct device *dev, rpp_hdr_drv_t *rpp_hdr)
{
	int ret = 0;
	rpp_hdr_irqflag_t irqs;
	rpp_hdr_fmuflag_t fmu_flags = { .value = 0 };

	/* enable and clear interrupts */
	irqs.value = 0;
	irqs.out_frame_out_irq = 1;
	irqs.mv_out_frame_out_irq = 1;
	irqs.main_post_awb_meas_irq = 1;

	irqs.main_pre1_acq_vsync_irq = 1;
	irqs.main_pre1_acq_frame_in_irq = 1;
	irqs.main_pre1_hist_irq = 1;
	irqs.main_pre1_exm_irq = 1;

	fmu_flags.main_pre1_acq_fifo_ovflw = 1;
	fmu_flags.main_pre2_acq_fifo_ovflw = 1;
	fmu_flags.main_pre3_acq_fifo_ovflw = 1;

	rpp_hdr_set_fmu_safe_access_enable(rpp_hdr, 1);
	CHECK(ret, rpp_hdr_fmuflags_enable(rpp_hdr, &fmu_flags),
	      "Failed to enable RPP_HDR error IRQs");
	CHECK(ret, rpp_hdr_fmuflags_get(rpp_hdr, &fmu_flags),
	      "Failed to clear RPP_HDR error IRQs");
	rpp_hdr_set_fmu_safe_access_enable(rpp_hdr, 0);

	CHECK(ret, rpp_hdr_irqflags_enable(rpp_hdr, &irqs),
	      "Failed to enable RPP_HDR IRQs.");
	CHECK(ret, rpp_hdr_irqflags_get(rpp_hdr, &irqs),
	      "Failed to clear RPP_HDR IRQs.");

	CHECK(ret, rpp_hdr_input_enable(rpp_hdr, 1),
	      "Failed to enable ACQ units.");

	CHECK(ret, rpp_hdr_shadow_update(rpp_hdr, 1),
	      "Failed to set pipeline UPDATE flag.");

	return 0;
}

int isp_main_rpp_stop_input(struct device *dev, rpp_hdr_drv_t *rpp_hdr)
{
	int ret = 0;

	CHECK(ret, rpp_hdr_input_enable(rpp_hdr, 0),
	      "Failed to disable ACQ units.");

	CHECK(ret, rpp_hdr_shadow_update(rpp_hdr, 0),
	      "Failed to set pipeline UPDATE flag.");

	return 0;
}

int isp_main_rpp_start_hv_output(struct device *dev, rpp_hdr_drv_t *rpp_hdr)
{
	int ret = 0;

	CHECK(ret, rpp_hdr_output_enable(rpp_hdr, 1, 0),
	      "Failed to enable hv output.");
	CHECK(ret, rpp_hdr_shadow_update(rpp_hdr, 0),
	      "Failed to set pipeline UPDATE flag.");

	return 0;
}

int isp_main_rpp_start_mv_output(struct device *dev, rpp_hdr_drv_t *rpp_hdr)
{
	int ret = 0;

	CHECK(ret, rpp_hdr_output_enable(rpp_hdr, 0, 1),
	      "Failed to enable mv output.");
	CHECK(ret, rpp_hdr_shadow_update(rpp_hdr, 0),
	      "Failed to set pipeline UPDATE flag.");

	return 0;
}

int isp_main_rpp_stop_hv_output(struct device *dev, rpp_hdr_drv_t *rpp_hdr)
{
	rpp_out_drv_t *hv_out = &(rpp_hdr->out);
	int ret = 0;

	CHECK(ret, rpp_hdr_output_disable(rpp_hdr, 1, 0),
	      "Failed to disable hv output.");
	CHECK(ret, rpp_out_disable(hv_out),
	      "Failed to disable Human Vision Output.");
	CHECK(ret, rpp_hdr_shadow_update(rpp_hdr, 0),
	      "Failed to set pipeline UPDATE flag.");

	return 0;
}

int isp_main_rpp_stop_mv_output(struct device *dev, rpp_hdr_drv_t *rpp_hdr)
{
	rpp_mv_out_drv_t *mv_out = &(rpp_hdr->mv_out);
	int ret = 0;

	CHECK(ret, rpp_hdr_output_disable(rpp_hdr, 0, 1),
	      "Failed to disable mv output.");
	CHECK(ret, rpp_mv_out_disable(mv_out),
	      "Failed to disable Machine Vision Output.");
	CHECK(ret, rpp_hdr_shadow_update(rpp_hdr, 0),
	      "Failed to set pipeline UPDATE flag.");

	return 0;
}

int isp_main_rpp_set_white_balance(struct device *dev, rpp_hdr_drv_t *rpp_hdr,
				   uint32_t r, uint32_t gr, uint32_t b,
				   uint32_t gb)
{
	rpp_awb_gain_drv_t *const awb_gain = &(rpp_hdr->main_post.awb_gain);
	rpp_awb_meas_drv_t *const awb_meas = &(rpp_hdr->main_post.awb_meas);
	rpp_ccor_profile_t awb_meas_profile =
		white_balance_table[DEFAULT_CCM_INDEX].inv_ccm;
	int ret = 0;

	CHECK(ret, rpp_awb_gain_set_gains(awb_gain, r, gr, b, gb),
	      "Failed to set AWB Gains");
	CHECK(ret, rpp_awb_gain_enable(awb_gain, 1), "Failed to set AWB Gains");

	for (int i = 0; i < 9; i += 3)
		SET_CCOR_COEFF(&awb_meas_profile, i,
			       (white_balance_table[DEFAULT_CCM_INDEX]
					.inv_ccm.coeff[i] *
				4096) / gr);
	for (int i = 1; i < 9; i += 3)
		SET_CCOR_COEFF(&awb_meas_profile, i,
			       (white_balance_table[DEFAULT_CCM_INDEX]
					.inv_ccm.coeff[i] *
				4096) / b);
	for (int i = 2; i < 9; i += 3)
		SET_CCOR_COEFF(&awb_meas_profile, i,
			       (white_balance_table[DEFAULT_CCM_INDEX]
					.inv_ccm.coeff[i] *
				4096) / r);

	CHECK(ret, rpp_awb_meas_set_profile(awb_meas, &awb_meas_profile),
	      "Failed to set AWB MEAS profile.");

	CHECK(ret, rpp_hdr_shadow_update(rpp_hdr, 0),
	      "Failed to set pipeline UPDATE flag.");

	return ret;
}

int isp_main_rpp_set_ccor(struct device *dev, rpp_hdr_drv_t *rpp_hdr,
			  rpp_ccor_profile_t *profile)
{
	rpp_ccor_drv_t *const ccor = &(rpp_hdr->main_post.ccor);
	int ret = 0;

	CHECK(ret, rpp_ccor_set_profile(ccor, profile),
	      "Failed to set MAIN_POST CCOR profile");

	CHECK(ret, rpp_hdr_shadow_update(rpp_hdr, 0),
	      "Failed to set pipeline UPDATE flag.");

	return ret;
}

int isp_main_rpp_set_rmap(struct device *dev, rpp_hdr_drv_t *rpp_hdr,
			  IspMainRppConfig_t *cfg, int num_inputs,
			  IspMainRmapSettings_t *rmap_cfg)
{
	rpp_rmap_drv_t *rmap = &rpp_hdr->rmap;
	int ret = 0;
	rpp_rmap_long_exp_bits_t long_bits = RPP_RMAP_LONG_EXP_24BIT;
	static const rpp_rmap_long_exp_bits_t bits_map[13] = {
		RPP_RMAP_LONG_EXP_12BIT, // 0    (shouldn't happen)
		RPP_RMAP_LONG_EXP_12BIT, // 2    (shouldn't happen)
		RPP_RMAP_LONG_EXP_12BIT, // 4    (shouldn't happen)
		RPP_RMAP_LONG_EXP_12BIT, // 6    (shouldn't happen)
		RPP_RMAP_LONG_EXP_12BIT, // 8    (shouldn't happen)
		RPP_RMAP_LONG_EXP_12BIT, // 10   (shouldn't happen)
		RPP_RMAP_LONG_EXP_12BIT, // 12
		RPP_RMAP_LONG_EXP_14BIT, // 14
		RPP_RMAP_LONG_EXP_16BIT, // 16
		RPP_RMAP_LONG_EXP_18BIT, // 18
		RPP_RMAP_LONG_EXP_20BIT, // 20
		RPP_RMAP_LONG_EXP_20BIT, // 22   (shouldn't happen)
		RPP_RMAP_LONG_EXP_24BIT, // 24   (last possible value)
	};
	if (!rmap_cfg->long_bits)
		return 0;
	long_bits = bits_map[rmap_cfg->long_bits >> 1];

	/* check how many pipes are wanted */
	switch (num_inputs) {
	default:
	case 1:
		/* disable RMAP and show PRE_1 bypass */
		CHECK(ret,
		      rpp_rmap_set_mode(rmap, RPP_RMAP_MODE_BYPASS_PRE1,
					long_bits),
		      "Failed to disable RMAP.");
		break;
	case 2:
		/* enable RMAP fusion between L and S (MAIN_PRE1 and MAIN_PRE2) */
		CHECK(ret,
		      rpp_rmap_set_mode(rmap, RPP_RMAP_MODE_FUSE_2EXP,
					long_bits),
		      "Failed to enable RMAP FUSE_2EXP mode.");
		CHECK(ret, rpp_rmap_set_map_fac(rmap, 0, rmap_cfg->map_fac_s),
		      "Failed to set up RMAP mapping factors.");
		CHECK(ret,
		      rpp_rmap_set_blending_long(
			      rmap, rmap_cfg->thr_long_min,
			      rmap_cfg->thr_long_max,
			      (1 << (rmap_cfg->long_bits + 4)) /
				      (rmap_cfg->thr_long_max -
				       rmap_cfg->thr_long_min)),
		      "Failed to set up RMAP L+S blending region.");
		break;
	case 3:
		/* enable RMAP fusion between L, S and VS (all inputs) */
		CHECK(ret,
		      rpp_rmap_set_mode(rmap, RPP_RMAP_MODE_FUSE_3EXP,
					long_bits),
		      "Failed to enable RMAP FUSE_3EXP mode.");
		CHECK(ret,
		      rpp_rmap_set_map_fac(rmap, rmap_cfg->map_fac_vs,
					   rmap_cfg->map_fac_s),
		      "Failed to set up RMAP mapping factors.");
		CHECK(ret,
		      rpp_rmap_set_blending_short(
			      rmap, rmap_cfg->thr_shrt_min,
			      rmap_cfg->thr_shrt_max,
			      (1 << (rmap->colorbits_low + 4)) /
				      (rmap_cfg->thr_shrt_max -
				       rmap_cfg->thr_shrt_min)),
		      "Failed to set up RMAP S+VS blending region.");
		CHECK(ret,
		      rpp_rmap_set_blending_long(
			      rmap, rmap_cfg->thr_long_min,
			      rmap_cfg->thr_long_max,
			      (1 << (rmap_cfg->long_bits + 4)) /
				      (rmap_cfg->thr_long_max -
				       rmap_cfg->thr_long_min)),
		      "Failed to set up RMAP L+S blending region.");
	}

	return ret;
}

int isp_main_rpp_set_rgbdenoise(struct device *dev, rpp_hdr_drv_t *rpp_hdr,
				IspMainRppConfig_t *cfg,
				IspMainRgbDenConfig_t *rgbden_cfg)
{
	rpp_rgbdenoise_drv_t *rgbden = &(rpp_hdr->main_post.rgbdenoise);
	int ret = 0;

	rpp_rgbdenoise_cfg_t den_cfg = {
        .if_cy = {
            /* Luma -> Sigma = 0.0168 */
            0xF, 0xF, 0xF, 0xF, 0xF, 0xE, 0xE, 0xE,
            0xD, 0xD, 0xD, 0xC, 0xC, 0xB, 0xB, 0xA,
            0xA, 0x9, 0x9, 0x8, 0x8, 0x7, 0x7, 0x6,
            0x6, 0x5, 0x5, 0x5, 0x4, 0x4, 0x3, 0x0
        },
        .if_cc = {
            /* Chroma -> Sigma = 0.0112 */
            0xF, 0xF, 0xF, 0xE, 0xE, 0xE, 0xD, 0xC,
            0xC, 0xB, 0xA, 0x9, 0x9, 0x8, 0x7, 0x6,
            0x6, 0x5, 0x4, 0x4, 0x3, 0x3, 0x2, 0x2,
            0x2, 0x1, 0x1, 0x1, 0x1, 0x1, 0x0, 0x0
        },
        .sf_cy = {0xF, 0xF, 0xE, 0xC},
        .sf_cc = {0xF, 0xF, 0xE, 0xC},
        /* Small intensity shift for luma to keep signs readable */
        .shift_y = rgbden_cfg->y_shift,
        /* Big intensity shift for chroma to get rid of color artifacts */
        .shift_c = rgbden_cfg->c_shift,
    };
	CHECK(ret, rpp_rgbdenoise_set_denoise_config(rgbden, &den_cfg),
	      "Failed to set RGBDENOISE config.");
	CHECK(ret, rpp_rgbdenoise_enable(rgbden, rgbden_cfg->enabled),
	      "Failed to enable/disable RGBDENOISE.");

	CHECK(ret, rpp_hdr_shadow_update(rpp_hdr, 0),
	      "Failed to set pipeline UPDATE flag.");
	return ret;
}

int isp_main_rpp_set_shrpcnr(struct device *dev, rpp_hdr_drv_t *rpp_hdr,
			     IspMainRppConfig_t *cfg,
			     IspMainShrpConfig_t *shrpcnr_cfg)
{
	rpp_shrpcnr_drv_t *drv = &(rpp_hdr->main_post.shrpcnr);
	int ret = 0;

	/* set up sharpening (SHRP) */

	/* set high-pass filter coefficients for omnidirectional filtering */
	rpp_shrpcnr_coeffs_t hpf;
	RPP_SHRPCNR_HPF0(hpf);

	CHECK(ret,
	      rpp_shrpcnr_set_shrp_params(drv, shrpcnr_cfg->sharpen_level,
					  shrpcnr_cfg->sharpen_thresh),
	      "Failed to set SHRPCNR sharpen params.");
	CHECK(ret, rpp_shrpcnr_set_shrp_filter_coeff(drv, hpf),
	      "Failed to set SHRPCNR sharpen filter coefficients.");
	CHECK(ret, rpp_shrpcnr_shrp_enable(drv, shrpcnr_cfg->sharpen_enabled),
	      "Failed to enable SHRPCNR sharpen filter.");

	/* set up color noise reduction (CNR) */
	CHECK(ret,
	      rpp_shrpcnr_set_cnr_thresholds(drv, shrpcnr_cfg->cnr_thresh,
					     shrpcnr_cfg->cnr_thresh),
	      "Failed to set SHRPCNR CNR thresholds.");
	CHECK(ret, rpp_shrpcnr_cnr_enable(drv, shrpcnr_cfg->cnr_enabled),
	      "Failed to enable/disable SHRPCNR CNR filter.");

	/* set up contrast-adaptive desaturation (CAD) */

	CHECK(ret, rpp_shrpcnr_set_cad_params(drv, &shrpcnr_cfg->cad_parm),
	      "Failed to set SHRPCNR CAD parameters.");
	CHECK(ret, rpp_shrpcnr_cad_enable(drv, shrpcnr_cfg->cad_enabled),
	      "Failed to enable/disable SHRPCNR CAD.");

	/* set up chroma desaturation (color kill) */
	CHECK(ret,
	      rpp_shrpcnr_set_desat_params(drv, shrpcnr_cfg->desat_thresh,
					   shrpcnr_cfg->desat_slope, 7),
	      "Failed to set SHRPCNR DESAT parameters.");
	CHECK(ret, rpp_shrpcnr_desat_enable(drv, shrpcnr_cfg->desat_enabled),
	      "Failed to enable/disable SHRPCNR DESAT filter.");

	CHECK(ret, rpp_hdr_shadow_update(rpp_hdr, 0),
	      "Failed to set pipeline UPDATE flag.");
	return ret;
}

int isp_main_rpp_set_mv_out(struct device *dev, rpp_hdr_drv_t *rpp_hdr,
			    IspMainRppConfig_t *cfg, bool yuv)
{
	rpp_mv_out_drv_t *mv_out = &(rpp_hdr->mv_out);
	rpp_window_t win;
	rpp_gamma_out_curve_t sRGBcurve;
	rpp_ccor_profile_t profile;
	int ret = 0;

	RPP_SET_WINDOW(win, 0, 0, cfg->width, cfg->height);
	SET_GAMMA_OUT_SRGB(sRGBcurve, mv_out->gamma_out.colorbits);
	SET_CCOR_BT709_RGB_2_YUV(&profile, mv_out->ccor.colorbits);

	/* Swap for NV61M */
	if (yuv) {
		rpp_ccor_profile_t tmp = profile;
		profile.coeff[3] = tmp.coeff[6];
		profile.coeff[4] = tmp.coeff[7];
		profile.coeff[5] = tmp.coeff[8];
		profile.coeff[6] = tmp.coeff[3];
		profile.coeff[7] = tmp.coeff[4];
		profile.coeff[8] = tmp.coeff[5];
	}

	if (yuv) {
		CHECK(ret,
		      rpp_mv_out_set_yuv(mv_out, RPP_MV_OUT_IN_SEL_DENOISED,
					 &sRGBcurve, &profile, true,
					 RPP_MV_OUT_422_CO_SITED, false),
		      "Failed to set up Machine Vision RGB output.");
	} else {
		CHECK(ret,
		      rpp_mv_out_set_rgb(mv_out, RPP_MV_OUT_IN_SEL_DENOISED,
					 &sRGBcurve),
		      "Failed to set up Machine Vision RGB output.");
	}

	CHECK(ret, rpp_is_set_window(&(mv_out->is), &win),
	      "Failed to set up MV Out Image Stabilization Cropping window.");
	CHECK(ret, rpp_mv_out_enable(mv_out, 0, 0),
	      "Failed to enable Machine Vision Output.");

	return ret;
}

int isp_main_rpp_set_output(struct device *dev, rpp_hdr_drv_t *rpp_hdr,
			    IspMainRppConfig_t *cfg, bool yuv)
{
	rpp_out_drv_t *hv_out = &(rpp_hdr->out);
	rpp_window_t win;
	rpp_gamma_out_curve_t sRGBcurve;
	rpp_ccor_profile_t profile;
	int ret = 0;

	RPP_SET_WINDOW(win, 0, 0, cfg->width, cfg->height);
	SET_GAMMA_OUT_SRGB(sRGBcurve, hv_out->gamma_out.colorbits);
	SET_CCOR_BT709_RGB_2_YUV(&profile, hv_out->ccor.colorbits);

	/* Swap for NV61M */
	if (yuv) {
		rpp_ccor_profile_t tmp = profile;
		profile.coeff[3] = tmp.coeff[6];
		profile.coeff[4] = tmp.coeff[7];
		profile.coeff[5] = tmp.coeff[8];
		profile.coeff[6] = tmp.coeff[3];
		profile.coeff[7] = tmp.coeff[4];
		profile.coeff[8] = tmp.coeff[5];
	}

	if (yuv) {
		CHECK(ret,
		      rpp_out_set_isp_yuv(hv_out, &sRGBcurve, &profile, 1,
					  RPP_OUT_422_CO_SITED, 0),
		      "Failed to set up Human Vision YUV output.");
	} else {
		CHECK(ret, rpp_out_set_isp_rgb(hv_out, &sRGBcurve),
		      "Failed to set up Human Vision sRGB output.");
	}

	CHECK(ret, rpp_is_set_window(&(hv_out->is), &win),
	      "Failed to set up HV Out Image Stabilization Cropping window.");
	CHECK(ret, rpp_out_enable(hv_out, 0, 0),
	      "Failed to enable Human Vision Output.");

	return ret;
}
