/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 *
 */
#ifndef _ISP_MAIN_SYSCTRL_H
#define _ISP_MAIN_SYSCTRL_H

#include <linux/types.h>
#include <linux/device.h>

#include <dct_reg_helper.h>
#include <isp_main_regs.h>
typedef uint32_t IspMainRegister_t;

#define isp_main_addr(base, offset) \
	((void*)((uintptr_t)base + offset))
#define isp_main_writel(base, offset, value) \
	writel(value, isp_main_addr(base, offset))
#define isp_main_readl(base, offset) \
	readl(isp_main_addr(base, offset))

typedef enum {
	// 0x0: Sensor fusion HDR mode. 24 bit data from channel ID 0 is buffered
	// in FIFOs 0 and 1, each half of the 24 bit intput pixel and are merged
	// and output on the rpp_main_pre1_in_s interface
	SYNC_MODE_HDR_SENSOR_FUSION = 0,
	// 0x1: SDR. FIFO 0 buffers a single 12 bit exposure from channel ID 0 and
	// forwards it to rpp_main_pre1_in_s.
	SYNC_MODE_SDR = 1,
	// 0x2: Dual exposure HDR mode. FIFO 0 buffers channel ID 0, FIFO 1 buffers
	// channel ID 1 and data is forwarded to rpp_hdr only when available on all
	// interfaces.
	SYNC_MODE_HDR_DUAL_EXPOSURE = 2,
	// 0x3: Triple exposure HDR mode. FIFO 0 buffers channel ID 0, FIFO 1
	// channel ID 1 and FIFO 2 channel ID 2 and data is only forwarded to
	// rpp_hdr if available from all interfaces.
	SYNC_MODE_HDR_TRIPLE_EXPOSURE = 3,
} IspMainSyncMode_t;

typedef enum {
	/* Given address is an address in memory (DRAM).            */
	ADDR_MODE_MEM = 0,
	/* Given address is an I/O address on the AXI interface.    */
	ADDR_MODE_IO = 1,
} IspMainAddrMode_t;

typedef enum {
	PX_SIZE_8BIT = 0,
	PX_SIZE_10BIT = 1,
	PX_SIZE_12BIT = 2,
	PX_SIZE_14BIT = 3,
	PX_SIZE_16BIT = 4,
	PX_SIZE_20BIT = 5,
	PX_SIZE_24BIT = 6,
	PX_SIZE_NUM
} IspMainPxSize_t;

int isp_main_sysctrl_init(struct device *dev,
			  IspMainRegister_t __iomem *sysctrl);

int isp_main_sysctrl_input_config(struct device *dev,
				  IspMainRegister_t __iomem *regs,
				  IspMainSyncMode_t sync_mode,
				  IspMainAddrMode_t addr_mode,
				  IspMainPxSize_t px_size, int width,
				  int height);

int isp_main_sysctrl_tpg(struct device *dev, IspMainRegister_t __iomem *sysctrl,
			 int enabled);
#endif
