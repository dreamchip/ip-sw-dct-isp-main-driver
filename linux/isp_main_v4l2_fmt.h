/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 *
 */
#ifndef __ISP_MAIN_V4L2_FMT_H__
#define __ISP_MAIN_V4L2_FMT_H__

#include <rpp_awb_meas_drv.h>
#include <rpp_ltm_drv.h>

/**
 * struct dct_isp_x1_hist_stat - DCT ISP histogram data
 *
 * @hist_bins: histogram for 4 components, usually R, GR, GB, B
 */
struct dct_isp_x1_hist_stat {
	uint32_t hist_bins[4][256];
};

struct dct_isp_x1_ltm_meas {
	uint32_t prc[3];
	uint32_t lmin;
	uint32_t lmax;
	uint32_t lgmean;
};

struct dct_isp_x1_awb_stat {
	rpp_awb_meas_t awb_meas;
};

struct dct_isp_x1_bls_meas {
	int32_t meas_r;
	int32_t meas_gr;
	int32_t meas_gb;
	int32_t meas_b;
};

#define DCT_ISP_X1_STAT_HIST 0x0001
#define DCT_ISP_X1_STAT_LTM 0x0002
#define DCT_ISP_X1_STAT_AWB 0x0004
#define DCT_ISP_X1_STAT_BLS 0x0008

/**
 * struct dct_isp_x1_stat_buffer - DCT ISP X1 Statistics Meta Data
 *
 * FIXME
 * @meas_type: measurement types (DCT_ISP_X1_STAT_* flags)
 * @frame_id: frame ID for sync
 */
struct dct_isp_x1_stat_buffer {
	struct dct_isp_x1_hist_stat hist;
	struct dct_isp_x1_ltm_meas ltm;
	struct dct_isp_x1_awb_stat awb;
	struct dct_isp_x1_bls_meas bls;
	uint32_t meas_type;
	uint32_t frame_id;
};

/* Index and count definition for LCE control parameters */
#define DCT_ISP_X1_CTRL_LCE_L0W_IDX 0
#define DCT_ISP_X1_CTRL_LCE_L0W_R_IDX 1
#define DCT_ISP_X1_CTRL_LCE_L0D_IDX 2
#define DCT_ISP_X1_CTRL_LCE_L0D_R_IDX 3
#define DCT_ISP_X1_CTRL_LCE_KMD0_IDX 4
#define DCT_ISP_X1_CTRL_LCE_KMD1_IDX 5
#define DCT_ISP_X1_CTRL_LCE_KDDIFF_IDX 6
#define DCT_ISP_X1_CTRL_LCE_KDDIFF_R_IDX 7
#define DCT_ISP_X1_CTRL_LCE_KW_IDX 8
#define DCT_ISP_X1_CTRL_LCE_KW_R_IDX 9
#define DCT_ISP_X1_CTRL_LCE_CGAIN_IDX 10
#define DCT_ISP_X1_CTRL_LCE_LPRCH_R_LO_IDX 11
#define DCT_ISP_X1_CTRL_LCE_LPRCH_R_HI_IDX 12
#define DCT_ISP_X1_CTRL_LCE_PARAM_COUNT 13

/* Index and count definition for white balance control */
#define DCT_ISP_X1_CTRL_WB_RED 0
#define DCT_ISP_X1_CTRL_WB_GREENRED 1
#define DCT_ISP_X1_CTRL_WB_BLUE 2
#define DCT_ISP_X1_CTRL_WB_GREENBLUE 3
#define DCT_ISP_X1_CTRL_WB_PARAM_COUNT 4

/* Index and count definition for color matrix control */
#define DCT_ISP_X1_CTRL_CM_COEFF0 0
#define DCT_ISP_X1_CTRL_CM_COEFF1 1
#define DCT_ISP_X1_CTRL_CM_COEFF2 2
#define DCT_ISP_X1_CTRL_CM_COEFF3 3
#define DCT_ISP_X1_CTRL_CM_COEFF4 4
#define DCT_ISP_X1_CTRL_CM_COEFF5 5
#define DCT_ISP_X1_CTRL_CM_COEFF6 6
#define DCT_ISP_X1_CTRL_CM_COEFF7 7
#define DCT_ISP_X1_CTRL_CM_COEFF8 8
#define DCT_ISP_X1_CTRL_CM_OFF0 9
#define DCT_ISP_X1_CTRL_CM_OFF1 10
#define DCT_ISP_X1_CTRL_CM_OFF2 11
#define DCT_ISP_X1_CTRL_CM_PARAM_COUNT 12

#endif /* __ISP_MAIN_V4L2_FMT_H__ */
