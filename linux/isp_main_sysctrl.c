/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 *
 */

#include <linux/types.h>
#include <linux/device.h>

#include "isp_main_v4l2.h"
#include "isp_main_sysctrl.h"

#include <rpp_hdr_drv.h>

int isp_main_sysctrl_init(struct device *dev, IspMainRegister_t __iomem *regs)
{
	uint32_t id = isp_main_readl(regs, ISP_MAIN_ID_REG);

	/* Print module ID */
	dev_info(
		dev,
		"isp-main-sysctrl: "
		"DCT ID: 0x%x, Project ID: 0x%x, Revision: %d, Module ID: 0x%x",
		DCT_REGS_FIELD_GET(id, DCT_ID),
		DCT_REGS_FIELD_GET(id, PROJECT_ID),
		DCT_REGS_FIELD_GET(id, REVISION),
		DCT_REGS_FIELD_GET(id, MODULE_ID));

	switch (DCT_REGS_FIELD_GET(id, REVISION)) {
	case 1:
		break;
	default:
		dev_err(dev,
			"isp-main-sysctrl: unsupported isp_main revision\n");
		return -ENODEV;
	}

	isp_main_writel(regs, ISP_MAIN_SYNC_FIFO_FLUSH_REG,
		ISP_SYNC_FIFO_FLUSH_MASK);

	return 0;
}

int isp_main_sysctrl_input_config(struct device *dev,
				  IspMainRegister_t __iomem *regs,
				  IspMainSyncMode_t sync_mode,
				  IspMainAddrMode_t addr_mode,
				  IspMainPxSize_t px_size, int width,
				  int height)
{
	uint32_t sync_ctrl = isp_main_readl(regs, ISP_MAIN_SYNC_CTRL_REG);
	uint32_t clk_en = isp_main_readl(regs, ISP_MAIN_CLK_EN_REG);

	/* Check pixel size */
	if (sync_mode > SYNC_MODE_SDR && px_size > PX_SIZE_12BIT) {
		dev_err(dev,
			"isp-main-sysctrl: "
			"In multi exposure mode the pixel size must not exceed 12 Bit.");
		return -EINVAL;
	}

	/* Setup ISP Sync module */
	DCT_REGS_FIELD_SET(sync_ctrl, ISP_SYNC_MODE, sync_mode);
	DCT_REGS_FLAG_ASSIGN(sync_ctrl, CFG_DMA_USE_CHID,
		      addr_mode == ADDR_MODE_IO ? 1 : 0);
	DCT_REGS_FIELD_SET(sync_ctrl, CFG_DMA_PX_WIDTH, px_size);
	isp_main_writel(regs, ISP_MAIN_SYNC_CTRL_REG, sync_ctrl);

	/* Enable clocks of pre pipelines as needed */
	DCT_REGS_FLAG_SET(clk_en, EN_CLK_MVOUT);
	DCT_REGS_FLAG_ASSIGN(clk_en, EN_CLK_PRE2,
		      sync_mode >= SYNC_MODE_HDR_DUAL_EXPOSURE ? 1 : 0);
	DCT_REGS_FLAG_ASSIGN(clk_en, EN_CLK_PRE3,
		      sync_mode == SYNC_MODE_HDR_TRIPLE_EXPOSURE ? 1 : 0);

	// todo: fixme, force triple exposure clocks as the driver otherwise
	// returns the probeing with -19.
	isp_main_writel(regs, ISP_MAIN_CLK_EN_REG, 7 /*clk_en*/);

	/* Make sure TPG is disabled. It is only used to generate the video timing. */
	isp_main_writel(regs, CFG_TEST_PATTERN_GENERAL_REG, 0);

	isp_main_writel(regs, CFG_H_SIZE_REG, width);
	isp_main_writel(regs, CFG_V_SIZE_REG, height);
	/* Use minimum blanking which is required by the RPP (25 lines and pixels). */
	isp_main_writel(regs, CFG_H_BLANK_REG, 25);
	isp_main_writel(regs, CFG_V_BLANK_REG, 25);

	isp_main_writel(regs, EXT_IRQ_START_REG, 1);
	isp_main_writel(regs, EXT_IRQ_END_REG, 24);

	return 0;
}

int isp_main_sysctrl_tpg(struct device *dev, IspMainRegister_t __iomem *regs,
			 int enabled)
{
	uint32_t test_pattern_general = 0;
	DCT_REGS_FIELD_SET(test_pattern_general, CFG_BAYER_PAT, 0);
	DCT_REGS_FLAG_SET(test_pattern_general, CFG_IMAGE);
	DCT_REGS_FLAG_SET(test_pattern_general, CFG_SEL_422);
	DCT_REGS_FLAG_SET(test_pattern_general, CFG_SEL_YUV);
	DCT_REGS_FLAG_ASSIGN(test_pattern_general, CFG_TPG_ENABLE,
		      !!enabled);

	isp_main_writel(regs, CFG_TEST_PATTERN_GENERAL_REG, test_pattern_general);

	return 0;
}
