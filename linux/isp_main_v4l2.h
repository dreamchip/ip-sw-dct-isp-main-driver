/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 *
 */

#ifndef _ISP_MAIN_V4L2_H_
#define _ISP_MAIN_V4L2_H_

#include <linux/clk.h>

#include <media/v4l2-common.h>
#include <media/v4l2-device.h>
#include <media/media-entity.h>
#include <isp-dma-v4l2.h>

#include <rpp_config.h>
#include <rpp_hdr_drv.h>

#include "isp_main_sysctrl.h"
#include "isp_main_v4l2_fmt.h"

#define ISP_MAIN_DEV_NAME ("dct_isp_main")

typedef enum {
	ISP_MAIN_DMA_READ = 0,
	ISP_MAIN_DMA_WRITE_HV,
	ISP_MAIN_DMA_WRITE_MV,
	ISP_MAIN_DMA_WRITE_DPC,
	ISP_MAIN_DMA_WRITE_HIST,

	ISP_MAIN_DMA_COUNT
} IspMainDmaIndex_t;

struct isp_main_v4l2_data {
	struct platform_device *pdev;
	struct device *dev;
	struct clk *clk;
	struct reset_control *rst_noc;
	void __iomem *rpp_base;
	IspMainRegister_t __iomem *sysctrl;

	// rpp
	rpp_hdr_drv_t rpp_hdr;
	struct rpp_device rpp_dev;
	struct dct_isp_x1_stat_buffer stats_cache;

	// irq
	int irq_rpp_hdr_err;
	int irq_rpp_hdr_fun;
	int irq_isp_sync_tpg;

	// v4l2
	struct mutex ctrl_lock;
	struct v4l2_ctrl_handler v4l2_ctrl_handler;
	struct {
		struct v4l2_ctrl *tonecurve;
		struct v4l2_ctrl *lce;
		struct v4l2_ctrl *white_balance;
		struct v4l2_ctrl *color_matrix;
		struct v4l2_ctrl *raw_denoise;
	} controls;

	struct v4l2_device v4l2_dev;
	struct isp_dma dma[ISP_MAIN_DMA_COUNT];
	struct v4l2_subdev isp_sd;
	struct media_pipeline pipe;

	atomic_t num_inst;
	struct mutex dev_mutex;
	spinlock_t irqlock;

	u32 fourcc; /* input format code */
	u32 sequence;
	u32 mode; /* 0: m2m, 1:cap */

	int direct_source_ref_count;

	// stats
	struct {
		int irq_tpg_sync_count;
		int irq_rpp_hdr_err_count;
		int irq_rpp_hdr_count[27];
	} stats;

	// mdev
#ifdef CONFIG_MEDIA_CONTROLLER
	struct media_pad isp_pads[ISP_MAIN_DMA_COUNT];
	struct media_device mdev;
#endif
};

#endif //_ISP_MAIN_V4L2_H_
