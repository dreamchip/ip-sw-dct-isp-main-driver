/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 *
 */
#ifndef _ISP_MAIN_RPP_H
#define _ISP_MAIN_RPP_H

#include <linux/types.h>
#include <linux/device.h>

#include <rpp_helper.h>
#include <rpp_hdr_drv.h>

#include "isp_main_sysctrl.h"

typedef struct {
	int width;
	int height;
	int acq_h_off;
	int acq_v_off;
	IspMainSyncMode_t sync_mode;
	IspMainAddrMode_t addr_mode;
	IspMainPxSize_t px_size;
} IspMainRppConfig_t;

typedef struct {
	uint32_t long_bits;
	uint32_t automatic;
	uint32_t thr_long_min;
	uint32_t thr_long_max;
	uint32_t thr_shrt_min;
	uint32_t thr_shrt_max;
	uint32_t map_fac_s;
	uint32_t map_fac_vs;
	/* measurement thresholds, ignored by setRmap() and
     * used by IRQ handler only */
	uint32_t vs_min;
	uint32_t s_min;
	uint32_t s_max;
	uint32_t l_max;
} IspMainRmapSettings_t;

typedef struct {
	bool enabled;
	uint8_t y_shift;
	uint8_t c_shift;
} IspMainRgbDenConfig_t;

typedef struct {
	bool sharpen_enabled;
	uint8_t sharpen_level;
	uint16_t sharpen_thresh;

	bool cnr_enabled;
	uint16_t cnr_thresh;

	bool cad_enabled;
	rpp_shrpcnr_cad_params_t cad_parm;

	bool desat_enabled;
	uint16_t desat_thresh;
	uint8_t desat_slope;
} IspMainShrpConfig_t;

int isp_main_rpp_init(struct device *dev, rpp_hdr_drv_t *rpp_hdr,
		      IspMainRppConfig_t *cfg);

int isp_main_rpp_start_input(struct device *dev, rpp_hdr_drv_t *rpp_hdr);
int isp_main_rpp_stop_input(struct device *dev, rpp_hdr_drv_t *rpp_hdr);
int isp_main_rpp_start_hv_output(struct device *dev, rpp_hdr_drv_t *rpp_hdr);
int isp_main_rpp_start_mv_output(struct device *dev, rpp_hdr_drv_t *rpp_hdr);
int isp_main_rpp_stop_hv_output(struct device *dev, rpp_hdr_drv_t *rpp_hdr);
int isp_main_rpp_stop_mv_output(struct device *dev, rpp_hdr_drv_t *rpp_hdr);

int isp_main_rpp_set_output(struct device *dev, rpp_hdr_drv_t *rpp_hdr,
			    IspMainRppConfig_t *cfg, bool yuv);
int isp_main_rpp_set_mv_out(struct device *dev, rpp_hdr_drv_t *rpp_hdr,
			    IspMainRppConfig_t *cfg, bool yuv);

int isp_main_rpp_set_white_balance(struct device *dev, rpp_hdr_drv_t *rpp_hdr,
				   uint32_t r, uint32_t gr, uint32_t b,
				   uint32_t gb);

int isp_main_rpp_set_ccor(struct device *dev, rpp_hdr_drv_t *rpp_hdr,
			  rpp_ccor_profile_t *profile);

#endif
