/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 *
 */

//#define DEBUG

#include <linux/completion.h>
#include <linux/delay.h>
#include <linux/errno.h>
#include <linux/fb.h>
#include <linux/firmware.h>
#include <linux/fs.h>
#include <linux/i2c.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/io.h>
#include <linux/kernel.h>
#include <linux/mm.h>
#include <linux/module.h>
#include <linux/of_gpio.h>
#include <linux/of_platform.h>
#include <linux/platform_device.h>
#include <linux/reset.h>
#include <linux/slab.h>
#include <linux/time.h>
#include <linux/types.h>
#include <linux/version.h>

#include <linux/dma-mapping.h>
#include <linux/videodev2.h>

#include <media/v4l2-async.h>
#include <media/v4l2-common.h>
#include <media/v4l2-ctrls.h>
#include <media/v4l2-device.h>
#include <media/v4l2-event.h>
#include <media/v4l2-ioctl.h>
#include <media/v4l2-mediabus.h>
#include <media/videobuf2-dma-contig.h>
#include <media/videobuf2-v4l2.h>

#ifdef CONFIG_MEDIA_CONTROLLER
#include <media/media-device.h>
#include <media/v4l2-mc.h>
#endif

#include <isp-dma-v4l2.h>
#include <isp-dma.h>

#include "isp_main_v4l2.h"
#include "isp_main_sysctrl.h"
#include "isp_main_rpp.h"
#include "isp_main_v4l2_fmt.h"

#include "isp_pre_v4l2.h"

#include <rpp_config.h>
#include <rpp_hdr_drv.h>

static char const *const debug_irq2str[] = {
	[27U] = "PRE1_HIST256",	     [26U] = "PRE3_DPCC",
	[25U] = "PRE2_DPCC",	     [24U] = "PRE1_DPCC",
	[23U] = "MV_OUT_FRAME_OUT",  [22U] = "MV_OUT_OFF",
	[21U] = "POST_AWB_DONE",     [20U] = "POST_HIST",
	[19U] = "POST_TM",	     [18U] = "PRE1_EXM",
	[17U] = "PRE1_HIST",	     [16U] = "PRE1_ACQ_FRAME_IN",
	[15U] = "PRE1_ACQ_H_START",  [14U] = "PRE1_ACQ_V_START",
	[13U] = "PRE2_EXM",	     [12U] = "PRE2_HIST",
	[11U] = "PRE2_ACQ_FRAME_IN", [10U] = "PRE2_ACQ_H_START",
	[9U] = "PRE2_ACQ_V_START",   [8U] = "PRE3_EXM",
	[7U] = "PRE3_HIST",	     [6U] = "PRE3_ACQ_FRAME_IN",
	[5U] = "PRE3_ACQ_H_START",   [4U] = "PRE3_ACQ_V_START",
	[3U] = "OUT_FRAME_OUT",	     [2U] = "OUT_OFF",
	[1U] = "RMAP_MEAS",	     [0U] = "RMAP",
};

static ssize_t debug_irqinfo_show(struct device *dev,
				  struct device_attribute *attr, char *buf)
{
	struct isp_main_v4l2_data *priv = dev_get_drvdata(dev);
	char buffer[1024];
	int pos = 0;
	rpp_hdr_fmuflag_t fmu_flags;

	rpp_hdr_set_fmu_safe_access_enable(&priv->rpp_hdr, 1);
	rpp_hdr_fmuflags_get(&priv->rpp_hdr, &fmu_flags);
	rpp_hdr_set_fmu_safe_access_enable(&priv->rpp_hdr, 0);

	pos += snprintf(&buffer[pos], sizeof(buffer),
			"RPP_HDR_ERR:        %d\n"
			"  main_pre1_acq_fifo_ovflw: %d\n"
			"  main_pre2_acq_fifo_ovflw: %d\n"
			"  main_pre3_acq_fifo_ovflw: %d\n"
			"TPG_SYNC:           %d\n",
			priv->stats.irq_rpp_hdr_err_count,
			fmu_flags.main_pre1_acq_fifo_ovflw,
			fmu_flags.main_pre2_acq_fifo_ovflw,
			fmu_flags.main_pre3_acq_fifo_ovflw,
			priv->stats.irq_tpg_sync_count);

	for (int i = 0; i < 27; i++) {
		if (!priv->stats.irq_rpp_hdr_count[i])
			continue;
		pos += snprintf(&buffer[pos], sizeof(buffer) - pos,
				"%17s:\t%d\n", debug_irq2str[i],
				priv->stats.irq_rpp_hdr_count[i]);
	}

	return strlcpy(buf, buffer, PAGE_SIZE);
}

static DEVICE_ATTR_RO(debug_irqinfo);

int isp_main_create_sysfs(struct platform_device *pdev)
{
	device_create_file(&pdev->dev, &dev_attr_debug_irqinfo);

	return 0;
}

int isp_main_remove_sysfs(struct platform_device *pdev)
{
	device_remove_file(&pdev->dev, &dev_attr_debug_irqinfo);

	return 0;
}

static IspMainRppConfig_t default_cfg = { .acq_h_off = 0,
					  .acq_v_off = 0,
					  .addr_mode = ADDR_MODE_MEM,
					  .width = 1936,
					  .height = 1100,
					  .px_size = PX_SIZE_12BIT,
					  .sync_mode =
						  SYNC_MODE_HDR_SENSOR_FUSION };

static int rpp_readl(rpp_device *dev, uint32_t unit_base, uint32_t reg_off,
		     uint32_t *regval)
{
	uint32_t *ptr = (uint32_t *)dev->priv;
	uint32_t addr = (unit_base + reg_off) >> 2;
	*regval = ptr[addr];
	return 0;
}

static int rpp_writel(rpp_device *dev, uint32_t unit_base, uint32_t reg_off,
		      uint32_t regval)
{
	uint32_t *ptr = (uint32_t *)dev->priv;
	uint32_t addr = (unit_base + reg_off) >> 2;
	ptr[addr] = regval;
	return 0;
}

static int rpp_read_regs(rpp_device *dev, uint32_t unit_base, uint32_t reg_off,
			 uint32_t nregs, uint32_t increment, uint32_t *regvals)
{
	int ret = 0;
	for (uint32_t i = 0; !ret && (i < nregs); ++i) {
		ret = dev->readl(dev, unit_base, reg_off, regvals + i);
		reg_off += increment;
	}
	return ret;
}

static int rpp_write_regs(rpp_device *dev, uint32_t unit_base, uint32_t reg_off,
			  uint32_t nregs, uint32_t increment,
			  const uint32_t *regvals)
{
	int ret = 0;
	for (uint32_t i = 0; !ret && (i < nregs); ++i) {
		ret = dev->writel(dev, unit_base, reg_off, regvals[i]);
		reg_off += increment;
	}
	return ret;
}

static struct rpp_device default_device = { .priv = NULL,
					    .read_regs = rpp_read_regs,
					    .readl = rpp_readl,
					    .write_regs = rpp_write_regs,
					    .writel = rpp_writel };

static irqreturn_t isp_main_irq_rpp_hdr_err(int irq, void *data)
{
	struct isp_main_v4l2_data *priv = data;

	//dev_info(priv->dev, "IRQ: isp_main_irq_rpp_hdr_err(%d)\n", irq);
	priv->stats.irq_rpp_hdr_err_count++;

	//isp_dma_cancel_active_buffer()
	//rpp_hdr_soft_reset(&priv->rpp_hdr, 1);
	//rpp_hdr_soft_reset(&priv->rpp_hdr, 0);
	// then restart stream?!

	return IRQ_HANDLED;
}

static irqreturn_t isp_main_irq_rpp_hdr_fun(int irq, void *data)
{
	struct isp_main_v4l2_data *priv = data;
	rpp_hdr_irqflag_t flags;
	int ret;

	ret = rpp_hdr_irqflags_get(&priv->rpp_hdr, &flags);
	if (ret) {
		//dev_err(priv->dev, "IRQ: rpp_hdr_irqflags_get(): %d\n", ret);
		return IRQ_NONE;
	}

	// ignore already handled interrupts
	if (flags.value) {
		if (flags.main_post_awb_meas_irq) {
			ret = rpp_awb_meas(&priv->rpp_hdr.main_post.awb_meas,
					   &priv->stats_cache.awb.awb_meas);
			if (ret == 0) {
				priv->stats_cache.meas_type |=
					DCT_ISP_X1_STAT_AWB;
			}
		}

		//FIXME call interrupt handler

		//dev_info(priv->dev, "IRQ: isp_main_irq_rpp_hdr_fun(%d): %08x\n",
		//	 irq, flags.value);

		for (int i = 0; i < 27; i++) {
			if (flags.value & (1 << i)) {
				priv->stats.irq_rpp_hdr_count[i]++;
			}
		}
	}

	return IRQ_HANDLED;
}

static irqreturn_t isp_main_irq_isp_sync_tpg(int irq, void *data)
{
	struct isp_main_v4l2_data *priv = data;

	//dev_info(priv->dev, "IRQ: isp_main_irq_isp_sync_tpg(%d)\n", irq);
	priv->stats.irq_tpg_sync_count++;

	return IRQ_HANDLED;
}

int isp_main_dma_start_stream_output(struct device *dev, bool start)
{
	struct isp_main_v4l2_data *priv = dev_get_drvdata(dev);
	int ret;

	dev_info(dev, "isp_main_dma_start_stream(output, %d)\n", start);

	isp_main_writel(priv->sysctrl, ISP_MAIN_SYNC_FIFO_FLUSH_REG,
		ISP_SYNC_FIFO_FLUSH_MASK);

	if (start) {
		ret = isp_main_rpp_start_input(dev, &priv->rpp_hdr);
		if (ret < 0) {
			dev_err(priv->dev,
				"isp_main_rpp_start_input() failed: %d\n", ret);
			return ret;
		}
	} else {
		ret = isp_main_rpp_stop_input(dev, &priv->rpp_hdr);
		if (ret < 0) {
			dev_err(priv->dev,
				"isp_main_rpp_stop_input() failed: %d\n", ret);
			return ret;
		}
	}

	return ret;
}

void isp_main_dma_frame_ready(struct device *dev,
			      struct vb2_v4l2_buffer *v4l2_buf)
{
	(void)dev;
	(void)v4l2_buf;
}

static int isp_main_dma_start_stream_capture(struct device *dev, bool start,
					     bool hv)
{
	struct isp_main_v4l2_data *priv = dev_get_drvdata(dev);
	struct platform_device *direct_source =
		isp_dma_get_direct_source(priv->dma[ISP_MAIN_DMA_READ].channel);
	int ret = 0;

	if (start) {
		ret = hv ? isp_main_rpp_start_hv_output(dev, &priv->rpp_hdr) :
			   isp_main_rpp_start_mv_output(dev, &priv->rpp_hdr);
		if (ret < 0) {
			dev_err(priv->dev,
				"isp_main_rpp_start_%s_output() failed: %d\n",
				hv ? "hv" : "mv", ret);
			return ret;
		}
	} else {
		ret = hv ? isp_main_rpp_stop_hv_output(dev, &priv->rpp_hdr) :
			   isp_main_rpp_stop_mv_output(dev, &priv->rpp_hdr);
		if (ret < 0) {
			dev_err(priv->dev,
				"isp_main_rpp_stop_%s_output() failed: %d\n",
				hv ? "hv" : "mv", ret);
			return ret;
		}
	}

	/* Start the direct_source only once, and only stop it once the last
	 * capture device is stopped. */
	if (direct_source && (priv->direct_source_ref_count < 1 ||
			      (priv->direct_source_ref_count == 1 && !start))) {
		struct isp_pre_data *isp_pre =
			platform_get_drvdata(direct_source);

		ret = isp_main_dma_start_stream_output(dev, start);
		if (ret)
			goto bail;

		ret = isp_pre->dma.start_stream(isp_pre->dev, start);
		if (ret)
			goto bail;
	}

	if (start)
		priv->direct_source_ref_count++;
	else
		priv->direct_source_ref_count--;

	dev_info(dev, "isp_main_dma_start_stream(capture, %d)\n", start);

bail:
	return ret;
}

static int isp_main_dma_start_stream_capture_hv(struct device *dev, bool start)
{
	return isp_main_dma_start_stream_capture(dev, start, true);
}

static int isp_main_dma_start_stream_capture_mv(struct device *dev, bool start)
{
	return isp_main_dma_start_stream_capture(dev, start, false);
}

void isp_main_dma_stats_ready(struct device *dev,
			      struct vb2_v4l2_buffer *v4l2_buf)
{
	struct isp_main_v4l2_data *priv = dev_get_drvdata(dev);
	struct dct_isp_x1_stat_buffer *stat_buf =
		(struct dct_isp_x1_stat_buffer *)vb2_plane_vaddr(
			&v4l2_buf->vb2_buf, 0);
	int ret;

	//FIXME proper sequence id management
	static int frame_id = 0;

	dev_dbg(dev, "isp_main_dma_stats_ready()\n");

	stat_buf->frame_id = frame_id++;
	stat_buf->meas_type = DCT_ISP_X1_STAT_HIST;

	ret = rpp_ltm_meas_get_prc(&priv->rpp_hdr.main_post.ltm_meas, 3,
				   stat_buf->ltm.prc);
	if (ret == 0) {
		ret = rpp_ltm_meas_get_stats(&priv->rpp_hdr.main_post.ltm_meas,
					     &stat_buf->ltm.lmin,
					     &stat_buf->ltm.lmax,
					     &stat_buf->ltm.lgmean);
		if (ret == 0) {
			stat_buf->meas_type |= DCT_ISP_X1_STAT_LTM;
		}
	}

	if (priv->stats_cache.meas_type & DCT_ISP_X1_STAT_AWB) {
		stat_buf->meas_type |= DCT_ISP_X1_STAT_AWB;
		memcpy(&stat_buf->awb, &priv->stats_cache.awb,
		       sizeof(struct dct_isp_x1_awb_stat));
		priv->stats_cache.meas_type &= ~DCT_ISP_X1_STAT_AWB;
	}

	ret = rpp_bls_get_measured_black_level(&priv->rpp_hdr.main_pre1.bls,
					       &stat_buf->bls.meas_r,
					       &stat_buf->bls.meas_gr,
					       &stat_buf->bls.meas_gb,
					       &stat_buf->bls.meas_b);
	if (ret == 0) {
		stat_buf->meas_type |= DCT_ISP_X1_STAT_BLS;
	}
}

static int isp_main_dma_init(struct platform_device *pdev,
			     struct isp_main_v4l2_data *priv)
{
	struct v4l2_format input_format = {
		.type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE,
		.fmt.pix_mp.pixelformat = V4L2_PIX_FMT_SRGGB12,
		.fmt.pix_mp.num_planes = 1,
		.fmt.pix_mp.width = 1936,
		.fmt.pix_mp.height = 1100,
	};
	struct v4l2_format output_format = {
		.type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE,
		.fmt.pix_mp.pixelformat = V4L2_PIX_FMT_NV61M,
		.fmt.pix_mp.width = 1920,
		.fmt.pix_mp.height = 1080,
	};
	u32 rd_formats[] = {
		V4L2_PIX_FMT_SRGGB12,
	};
	u32 wr_formats[] = {
		V4L2_PIX_FMT_RGB24,
		V4L2_PIX_FMT_NV61M,
	};
	struct {
		struct isp_dma *dma;
		const char *channel_name;
		int (*start_stream)(struct device *, bool);
		void (*transfer_done)(struct device *,
				      struct vb2_v4l2_buffer *);
		u32 *supported_formats;
		size_t supported_format_count;
	} const dma_table[] = {
		// clang-format off
	{ &priv->dma[ISP_MAIN_DMA_READ], "read", isp_main_dma_start_stream_output, isp_main_dma_frame_ready, rd_formats, ARRAY_SIZE(rd_formats) },
	{ &priv->dma[ISP_MAIN_DMA_WRITE_HV], "write-hv", isp_main_dma_start_stream_capture_hv, isp_main_dma_frame_ready, wr_formats, ARRAY_SIZE(wr_formats) },
	{ &priv->dma[ISP_MAIN_DMA_WRITE_MV], "write-mv", isp_main_dma_start_stream_capture_mv, isp_main_dma_frame_ready, wr_formats, ARRAY_SIZE(wr_formats) },
	{ &priv->dma[ISP_MAIN_DMA_WRITE_DPC], "write-dpc", NULL, NULL, NULL, 0 },
	{ &priv->dma[ISP_MAIN_DMA_WRITE_HIST], "write-hist", NULL, isp_main_dma_stats_ready, NULL, 0 }
		// clang-format on
	};
	int const isp_main_dma_count = ARRAY_SIZE(dma_table);
	int ret;

	for (int i = 0; i < isp_main_dma_count; i++) {
		ret = isp_dma_init(&pdev->dev, dma_table[i].dma,
				   dma_table[i].channel_name, &priv->v4l2_dev,
				   &priv->pipe, dma_table[i].start_stream,
				   dma_table[i].transfer_done);
		if (ret < 0) {
			dev_err(priv->dev, "isp_dma_init(%s) failed: %d\n",
				dma_table[i].channel_name, ret);
			goto bail;
		}

		if (dma_table[i].supported_formats) {
			ret = isp_dma_set_supported_formats(
				dma_table[i].dma,
				dma_table[i].supported_formats,
				dma_table[i].supported_format_count);
			if (ret) {
				dev_err(priv->dev,
					"isp_dma_set_supported_formats failed: %d\n",
					ret);
				goto cleanup;
			}
		}
	}

	ret = isp_dma_set_metadata(&priv->dma[ISP_MAIN_DMA_WRITE_HIST],
				   V4L2_META_FMT_RK_ISP1_STAT_3A,
				   sizeof(struct dct_isp_x1_stat_buffer),
				   sizeof(struct dct_isp_x1_hist_stat));
	if (ret < 0) {
		dev_err(priv->dev, "isp_dma_set_metadata failed: %d\n", ret);
		goto cleanup;
	}

	/* Set default format */
	ret = isp_dma_override_format(&priv->dma[ISP_MAIN_DMA_READ],
				      &input_format);
	if (ret < 0) {
		dev_err(priv->dev, "isp_dma_override_format(read) failed: %d\n",
			ret);
		goto cleanup;
	}

	ret = isp_dma_override_format(&priv->dma[ISP_MAIN_DMA_WRITE_HV],
				      &output_format);
	if (ret < 0) {
		dev_err(priv->dev,
			"isp_dma_override_format(write-hv) failed: %d\n", ret);
		goto cleanup;
	}

	ret = isp_dma_override_format(&priv->dma[ISP_MAIN_DMA_WRITE_MV],
				      &output_format);
	if (ret < 0) {
		dev_err(priv->dev,
			"isp_dma_override_format(write-hv) failed: %d\n", ret);
		goto cleanup;
	}

	/* Set DMA mask of this device to the same value as the DMAs DMA mask,
	 * this ensures that we can use the same address range as the DMA when
	 * allocating memory. */
	ret = dma_coerce_mask_and_coherent(
		priv->dev, isp_dma_get_dma_mask(&priv->dma[ISP_MAIN_DMA_READ]));
	if (ret < 0) {
		dev_err(priv->dev, "dma_coerce_mask_and_coherent failed: %d\n",
			ret);
		goto cleanup;
	}

	return 0;

cleanup:
	for (int i = 0; i < ISP_MAIN_DMA_COUNT; i++) {
		isp_dma_cleanup(&priv->dma[i]);
	}

bail:
	return ret;
}

#ifdef CONFIG_MEDIA_CONTROLLER
static const struct media_device_ops m2m_media_ops = {
	.link_notify = v4l2_pipeline_link_notify,
};

static int isp_main_init_media_device(struct device *dev,
				      struct isp_main_v4l2_data *isp_main)
{
	isp_main->mdev.dev = dev;
	strscpy(isp_main->mdev.model, "isp-main", sizeof(isp_main->mdev.model));
	strscpy(isp_main->mdev.bus_info, "platform:isp-main",
		sizeof(isp_main->mdev.bus_info));
	media_device_init(&isp_main->mdev);
	isp_main->mdev.ops = &m2m_media_ops;
	isp_main->v4l2_dev.mdev = &isp_main->mdev;

	return 0;
}
#endif

int isp_main_sd_get_fmt(struct v4l2_subdev *sd, struct v4l2_subdev_state *state,
			struct v4l2_subdev_format *format)
{
	dev_info(sd->dev, "isp_main_sd_get_fmt()\n");

	return 0;
}

int isp_main_sd_set_fmt(struct v4l2_subdev *sd, struct v4l2_subdev_state *state,
			struct v4l2_subdev_format *format)
{
	struct isp_main_v4l2_data *priv = dev_get_drvdata(sd->dev);
	IspMainRppConfig_t cfg = default_cfg;
	int ret = 0;

	dev_info(sd->dev, "isp_main_sd_set_fmt(pad:%d, width:%d, height: %d)\n",
		 format->pad, format->format.width, format->format.height);

	cfg.width = format->format.width;
	cfg.height = format->format.height;

	/* TODO: if one of the write channels is already running,
	         check for compatibility of the configurations (image size) */
	switch (format->pad) {
	case ISP_MAIN_DMA_WRITE_HV:
	case ISP_MAIN_DMA_WRITE_MV:
		if (priv->direct_source_ref_count == 0) {
			ret = isp_main_rpp_init(sd->dev, &priv->rpp_hdr, &cfg);
			if (ret) {
				dev_err(sd->dev, "isp_main_rpp_init failed\n");
				goto bail;
			}
		}
		break;
	}

	switch (format->pad) {
	case ISP_MAIN_DMA_READ:
		ret = isp_main_sysctrl_input_config(sd->dev, priv->sysctrl,
						    cfg.sync_mode,
						    cfg.addr_mode, cfg.px_size,
						    cfg.width, cfg.height);
		if (ret) {
			dev_err(sd->dev,
				"isp_main_sysctrl_input_config failed\n");
			goto bail;
		}
		break;
	case ISP_MAIN_DMA_WRITE_HV:
		ret = isp_main_rpp_set_output(sd->dev, &priv->rpp_hdr, &cfg,
					      format->format.code ==
						      MEDIA_BUS_FMT_YUYV8_2X8);
		if (ret) {
			dev_err(sd->dev, "isp_main_rpp_set_output failed\n");
			goto bail;
		}
		break;
	case ISP_MAIN_DMA_WRITE_MV:
		ret = isp_main_rpp_set_mv_out(sd->dev, &priv->rpp_hdr, &cfg,
					      format->format.code ==
						      MEDIA_BUS_FMT_YUYV8_2X8);
		if (ret) {
			dev_err(sd->dev, "isp_main_rpp_set_mv_out failed\n");
			goto bail;
		}
		break;

	default:
		return -EINVAL;
	}

	ret = rpp_hdr_shadow_update(&priv->rpp_hdr, 1);
	if (ret) {
		dev_err(sd->dev, "rpp_hdr_shadow_update failed\n");
		goto bail;
	}

bail:
	return ret;
}

static struct v4l2_subdev_pad_ops isp_main_isp_sd_pad_ops = {
	.get_fmt = isp_main_sd_get_fmt,
	.set_fmt = isp_main_sd_set_fmt,

	//#ifdef CONFIG_MEDIA_CONTROLLER
	//	int (*link_validate)(struct v4l2_subdev *sd, struct media_link *link,
	//				struct v4l2_subdev_format *source_fmt,
	//				struct v4l2_subdev_format *sink_fmt);
	//#endif /* CONFIG_MEDIA_CONTROLLER */
};

/*
 * The base for DCT driver controls.
 * We reserve 16 controls for this driver.
 */
#define V4L2_CID_USER_DCT_BASE (V4L2_CID_USER_BASE + 0x11b0)
#define V4L2_CID_DCT_TONECURVE (V4L2_CID_USER_DCT_BASE + 1)
#define V4L2_CID_DCT_LCE (V4L2_CID_USER_DCT_BASE + 2)
#define V4L2_CID_DCT_WHITE_BALANCE (V4L2_CID_USER_DCT_BASE + 3)
#define V4L2_CID_DCT_COLOR_MATRIX (V4L2_CID_USER_DCT_BASE + 4)
#define V4L2_CID_DCT_RAW_DENOISE (V4L2_CID_USER_DCT_BASE + 5)

static struct v4l2_subdev_ops isp_main_isp_sd_ops = {
	.pad = &isp_main_isp_sd_pad_ops,
};

static int isp_main_isp_set_ctrl(struct v4l2_ctrl *ctrl)
{
	struct isp_main_v4l2_data *priv = container_of(
		ctrl->handler, struct isp_main_v4l2_data, v4l2_ctrl_handler);
	uint32_t *p = ctrl->p_new.p_u32;
	int ret = 0;

	dev_dbg(priv->dev, "isp_main_isp_set_ctrl: set control: %x\n",
		ctrl->id);

	switch (ctrl->id) {
	case V4L2_CID_DCT_TONECURVE: {
		rpp_ltm_curve_t ltm_curve;
		for (int i = 0; i < RPP_LTM_MAX_CURVE_POINTS; i++) {
			ltm_curve.dx[i] = p[i];
			ltm_curve.y[i] = p[RPP_LTM_MAX_CURVE_POINTS + i];
		}
		ret = rpp_ltm_set_tonecurve(&priv->rpp_hdr.main_post.ltm,
					    &ltm_curve);
		if (ret) {
			dev_err(priv->dev, "rpp_ltm_set_tonecurve failed: %d\n",
				ret);
			rpp_ltm_enable(&priv->rpp_hdr.main_post.ltm, 0);
			ret = 0;
		} else
			rpp_ltm_enable(&priv->rpp_hdr.main_post.ltm, 1);
		rpp_hdr_shadow_update(&priv->rpp_hdr, 0);
	} break;
	case V4L2_CID_DCT_LCE: {
		rpp_ltm_param_t ltm_params = {
			.L0w = p[DCT_ISP_X1_CTRL_LCE_L0W_IDX],
			.L0w_r = p[DCT_ISP_X1_CTRL_LCE_L0W_R_IDX],
			.L0d = p[DCT_ISP_X1_CTRL_LCE_L0D_IDX],
			.L0d_r = p[DCT_ISP_X1_CTRL_LCE_L0D_R_IDX],
			.kmd = p[DCT_ISP_X1_CTRL_LCE_KMD0_IDX],
			.kMd = p[DCT_ISP_X1_CTRL_LCE_KMD1_IDX],
			.kdDiff = p[DCT_ISP_X1_CTRL_LCE_KDDIFF_IDX],
			.kdDiff_r = p[DCT_ISP_X1_CTRL_LCE_KDDIFF_R_IDX],
			.kw = p[DCT_ISP_X1_CTRL_LCE_KW_IDX],
			.kw_r = p[DCT_ISP_X1_CTRL_LCE_KW_R_IDX],
			.cgain = p[DCT_ISP_X1_CTRL_LCE_CGAIN_IDX],
			.LprcH_r =
				p[DCT_ISP_X1_CTRL_LCE_LPRCH_R_LO_IDX] |
				((uint64_t)p[DCT_ISP_X1_CTRL_LCE_LPRCH_R_HI_IDX]
				 << 32),
		};
		ret = rpp_ltm_set_ltm_params(&priv->rpp_hdr.main_post.ltm,
					     &ltm_params);
		if (ret == 0)
			rpp_hdr_shadow_update(&priv->rpp_hdr, 0);
	} break;
	case V4L2_CID_DCT_WHITE_BALANCE:
		ret = isp_main_rpp_set_white_balance(
			priv->dev, &priv->rpp_hdr, p[DCT_ISP_X1_CTRL_WB_RED],
			p[DCT_ISP_X1_CTRL_WB_GREENRED],
			p[DCT_ISP_X1_CTRL_WB_BLUE],
			p[DCT_ISP_X1_CTRL_WB_GREENBLUE]);
		break;
	case V4L2_CID_DCT_COLOR_MATRIX: {
		rpp_ccor_profile_t profile = {
			.coeff = {
				sign_extend(p[0], 16),
				sign_extend(p[1], 16),
				sign_extend(p[2], 16),
				sign_extend(p[3], 16),
				sign_extend(p[4], 16),
				sign_extend(p[5], 16),
				sign_extend(p[6], 16),
				sign_extend(p[7], 16),
				sign_extend(p[8], 16)
			},
			.off = {
				sign_extend(p[9], 16),
				sign_extend(p[10], 16),
				sign_extend(p[11], 16),
			},
		};
		ret = isp_main_rpp_set_ccor(priv->dev, &priv->rpp_hdr,
					    &profile);
	} break;
	case V4L2_CID_DCT_RAW_DENOISE: {
		rpp_dpf_spatial_strength_t dpf_spatial_strength = {
			.red = p[0], .green = p[0], .blue = p[0]
		};
		rpp_dpf_mode_t dpf_mode;
		ret = rpp_dpf_mode(&priv->rpp_hdr.main_pre1.dpf, &dpf_mode);
		if (ret) {
			dev_err(priv->dev, "rpp_dpf_get_mode failed: %d\n",
				ret);
			break;
		}
		dpf_mode.enable = (p[0] > 0);

		ret = rpp_dpf_set_spatial_strength(&priv->rpp_hdr.main_pre1.dpf,
						   &dpf_spatial_strength);
		if (ret) {
			dev_err(priv->dev,
				"rpp_dpf_set_spatial_strength failed: %d\n",
				ret);
		} else {
			ret = rpp_dpf_set_mode(&priv->rpp_hdr.main_pre1.dpf,
					       &dpf_mode);
			if (ret) {
				dev_err(priv->dev,
					"rpp_dpf_set_mode failed: %d\n", ret);
			}
		}
		if (ret == 0)
			rpp_hdr_shadow_update(&priv->rpp_hdr, 0);
	} break;

	default:
		dev_warn(priv->dev,
			 "isp_main_isp_set_ctrl: unknown control %x\n",
			 ctrl->id);
	}

	return ret;
}

static const struct v4l2_ctrl_ops isp_main_ctrl_ops = {
	.s_ctrl = isp_main_isp_set_ctrl,
};

static const struct v4l2_ctrl_type_ops isp_main_ctrl_type_ops = {
	.init = v4l2_ctrl_type_op_init,
	.validate = v4l2_ctrl_type_op_validate,
	.log = v4l2_ctrl_type_op_log,
	.equal = v4l2_ctrl_type_op_equal,
};

struct v4l2_ctrl_config isp_main_tonecurve_ctrl_config = {
	.ops = &isp_main_ctrl_ops,
	.id = V4L2_CID_DCT_TONECURVE,
	.name = "tonecurve",
	.type = V4L2_CTRL_TYPE_U32,
	.min = 0x00000000,
	.max = 0xffffffff,
	.step = 1,
	.def = 0,
	.dims = { 2, RPP_LTM_MAX_CURVE_POINTS },
	.flags = V4L2_CTRL_FLAG_EXECUTE_ON_WRITE,
};

struct v4l2_ctrl_config isp_main_lce_ctrl_config = {
	.ops = &isp_main_ctrl_ops,
	.id = V4L2_CID_DCT_LCE,
	.name = "local_contrast_enh",
	.type = V4L2_CTRL_TYPE_U32,
	.min = 0x00000000,
	.max = 0xffffffff,
	.step = 1,
	.def = 0,
	.dims = { 1, DCT_ISP_X1_CTRL_LCE_PARAM_COUNT },
	.flags = V4L2_CTRL_FLAG_EXECUTE_ON_WRITE,
};

struct v4l2_ctrl_config isp_main_white_balance_ctrl_config = {
	.ops = &isp_main_ctrl_ops,
	.id = V4L2_CID_DCT_WHITE_BALANCE,
	.name = "white_balance",
	.type = V4L2_CTRL_TYPE_U32,
	.min = 0x00000000,
	.max = 0x0003ffff,
	.step = 1,
	.def = 0x1000,
	.dims = { 1, DCT_ISP_X1_CTRL_WB_PARAM_COUNT },
	.flags = V4L2_CTRL_FLAG_EXECUTE_ON_WRITE,
};

struct v4l2_ctrl_config isp_main_color_matrix_ctrl_config = {
	.ops = &isp_main_ctrl_ops,
	.id = V4L2_CID_DCT_COLOR_MATRIX,
	.name = "color_matrix",
	.type = V4L2_CTRL_TYPE_U32,
	.min = 0x00000000,
	.max = 0x0000ffff,
	.step = 1,
	.def = 0x1000,
	.dims = { 1, DCT_ISP_X1_CTRL_CM_PARAM_COUNT },
	.flags = V4L2_CTRL_FLAG_EXECUTE_ON_WRITE,
};

struct v4l2_ctrl_config isp_main_rawdenoise_ctrl_config = {
	.ops = &isp_main_ctrl_ops,
	.id = V4L2_CID_DCT_RAW_DENOISE,
	.name = "raw_denoise",
	.type = V4L2_CTRL_TYPE_U32,
	.min = 0x00000000,
	.max = 0x000000ff,
	.step = 1,
	.def = 0,
	.flags = V4L2_CTRL_FLAG_EXECUTE_ON_WRITE,
};

static int isp_main_v4l2_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct isp_main_v4l2_data *priv;
	int ret = 0;

	dev_info(dev, "probing isp-main-v4l2...\n");

	priv = kzalloc(sizeof(struct isp_main_v4l2_data), GFP_ATOMIC);
	if (priv == NULL)
		return -ENOMEM;

	platform_set_drvdata(pdev, priv);

	priv->clk = devm_clk_get_enabled(dev, "isp_main_clk");
	if (IS_ERR(priv->clk)) {
		dev_err(dev, "failed to retrieve clk\n");
		return PTR_ERR(priv->clk);
	}

	priv->sysctrl = devm_platform_ioremap_resource(pdev, 1);
	if (IS_ERR(priv->sysctrl))
		return PTR_ERR(priv->sysctrl);

	ret = isp_main_sysctrl_init(dev, priv->sysctrl);
	if (ret) {
		dev_err(dev, "isp_main_sysctrl_init failed\n");
		goto err_subdevs;
	}

	// default sysctrl for initialization, will be updated later
	ret = isp_main_sysctrl_input_config(
		dev, priv->sysctrl, default_cfg.sync_mode,
		default_cfg.addr_mode, default_cfg.px_size, default_cfg.width,
		default_cfg.height);
	if (ret) {
		dev_err(dev, "isp_main_sysctrl_input_config failed\n");
		goto err_subdevs;
	}

	priv->rpp_base = devm_platform_ioremap_resource(pdev, 0);
	if (IS_ERR(priv->rpp_base))
		return PTR_ERR(priv->rpp_base);

	priv->rpp_dev = default_device;
	priv->rpp_dev.priv = priv->rpp_base;
	ret = rpp_hdr_init(&priv->rpp_dev, 0, 3, &priv->rpp_hdr);
	if (ret) {
		dev_err(dev, "rpp_hdr_init failed: %d\n", ret);
		goto err_subdevs;
	}

	ret = isp_main_rpp_init(dev, &priv->rpp_hdr, &default_cfg);
	if (ret) {
		dev_err(dev, "isp_main_rpp_init failed: %d\n", ret);
		goto err_subdevs;
	}
	dev_info(dev, "isp_main_rpp_init OK\n");

	priv->pdev = pdev;
	priv->dev = &pdev->dev;

	priv->irq_rpp_hdr_err =
		fwnode_irq_get_byname(dev_fwnode(dev), "rpp_hdr_err_n");
	if (priv->irq_rpp_hdr_err < 0) {
		ret = dev_err_probe(dev, priv->irq_rpp_hdr_err,
				    "Failed to get rpp_hdr_err IRQ\n");
		goto err_irqs;
	}
	ret = request_irq(priv->irq_rpp_hdr_err, isp_main_irq_rpp_hdr_err, 0,
			  "dct-isp-main-v4l2", priv);
	if (ret) {
		dev_err(dev, "Unable to request rpp_hdr_err IRQ %d\n",
			priv->irq_rpp_hdr_err);
		priv->irq_rpp_hdr_err = -1;
		goto err_irqs;
	}
	priv->irq_rpp_hdr_fun =
		fwnode_irq_get_byname(dev_fwnode(dev), "rpp_hdr_fun");
	if (priv->irq_rpp_hdr_fun < 0) {
		ret = dev_err_probe(dev, priv->irq_rpp_hdr_fun,
				    "Failed to get rpp_hdr_fun IRQ\n");
		goto err_irqs;
	}
	ret = request_irq(priv->irq_rpp_hdr_fun, isp_main_irq_rpp_hdr_fun, 0,
			  "dct-isp-main-v4l2", priv);
	if (ret) {
		dev_err(dev, "Unable to request rpp_hdr_fun IRQ %d\n",
			priv->irq_rpp_hdr_fun);
		priv->irq_rpp_hdr_fun = -1;
		goto err_irqs;
	}
	priv->irq_isp_sync_tpg =
		fwnode_irq_get_byname(dev_fwnode(dev), "isp_sync_tpg");
	if (priv->irq_isp_sync_tpg < 0) {
		ret = dev_err_probe(dev, priv->irq_isp_sync_tpg,
				    "Failed to get isp_sync_tpg IRQ\n");
		goto err_irqs;
	}
	ret = request_irq(priv->irq_isp_sync_tpg, isp_main_irq_isp_sync_tpg, 0,
			  "dct-isp-main-v4l2", priv);
	if (ret) {
		dev_err(dev, "Unable to request isp_sync_tpg IRQ %d\n",
			priv->irq_isp_sync_tpg);
		priv->irq_isp_sync_tpg = -1;
		goto err_irqs;
	}

	priv->rst_noc = devm_reset_control_get_optional_shared(dev, NULL);
	if (IS_ERR(priv->rst_noc)) {
		dev_warn(dev, "warning getting reset control failed\n");
		// for zukimo soc reset is (currently) not needed so it is
		// ok to continue without having the reset
	}
	if (priv->rst_noc)
		reset_control_deassert(priv->rst_noc);

#ifdef CONFIG_MEDIA_CONTROLLER
	ret = isp_main_init_media_device(dev, priv);
	if (ret < 0) {
		dev_err(dev, "isp_main_init_media_device failed: %d\n", ret);
		goto err_irqs;
	}
#endif

	ret = v4l2_ctrl_handler_init(&priv->v4l2_ctrl_handler, 0);
	if (ret < 0) {
		dev_err(dev, "v4l2_ctrl_handler_init failed: %d\n", ret);
		goto err_irqs;
	}

	mutex_init(&priv->ctrl_lock);
	priv->v4l2_ctrl_handler.lock = &priv->ctrl_lock;

	priv->controls.tonecurve =
		v4l2_ctrl_new_custom(&priv->v4l2_ctrl_handler,
				     &isp_main_tonecurve_ctrl_config, priv);
	priv->controls.lce = v4l2_ctrl_new_custom(
		&priv->v4l2_ctrl_handler, &isp_main_lce_ctrl_config, priv);
	priv->controls.white_balance =
		v4l2_ctrl_new_custom(&priv->v4l2_ctrl_handler,
				     &isp_main_white_balance_ctrl_config, priv);
	priv->controls.color_matrix =
		v4l2_ctrl_new_custom(&priv->v4l2_ctrl_handler,
				     &isp_main_color_matrix_ctrl_config, priv);
	priv->controls.raw_denoise =
		v4l2_ctrl_new_custom(&priv->v4l2_ctrl_handler,
				     &isp_main_rawdenoise_ctrl_config, priv);

	if (priv->v4l2_ctrl_handler.error) {
		ret = priv->v4l2_ctrl_handler.error;
		dev_err(dev, "v4l2_ctrl_new_custom failed: %d\n", ret);
		goto err_irqs;
	}

	ret = v4l2_device_register(dev, &priv->v4l2_dev);
	if (ret < 0) {
		dev_err(dev, "v4l2_device_register failed: %d\n", ret);
		goto err_register_dev;
	}

	ret = isp_main_dma_init(pdev, priv);
	if (ret < 0) {
		dev_err(dev, "isp_main_dma_init failed: %d\n", ret);
		ret = -EPROBE_DEFER;
		goto err_init_dma;
	}

	v4l2_subdev_init(&priv->isp_sd, &isp_main_isp_sd_ops);
	snprintf(priv->isp_sd.name, sizeof(priv->isp_sd.name),
		 "isp-main-subdev");
	priv->isp_sd.entity.function = MEDIA_ENT_F_PROC_VIDEO_ISP;
	priv->isp_sd.dev = dev;
	priv->isp_sd.ctrl_handler = &priv->v4l2_ctrl_handler;
	priv->isp_sd.flags = V4L2_SUBDEV_FL_HAS_DEVNODE;

	ret = v4l2_device_register_subdev(&priv->v4l2_dev, &priv->isp_sd);
	if (ret < 0) {
		dev_err(dev, "v4l2_device_register_subdev failed: %d\n", ret);
		goto err_register_subdev;
	}

	ret = v4l2_device_register_subdev_nodes(&priv->v4l2_dev);
	if (ret < 0) {
		dev_err(dev, "v4l2_device_register_subdev_nodes failed: %d\n",
			ret);
		goto err_register_subdev;
	}

#ifdef CONFIG_MEDIA_CONTROLLER
	ret = media_device_register(&priv->mdev);
	if (ret) {
		dev_err(dev, "media_device_register failed: %d\n", ret);
		goto err_register_subdev;
	}

	priv->isp_pads[ISP_MAIN_DMA_READ].flags = MEDIA_PAD_FL_SINK;
	priv->isp_pads[ISP_MAIN_DMA_WRITE_HV].flags = MEDIA_PAD_FL_SOURCE;
	priv->isp_pads[ISP_MAIN_DMA_WRITE_MV].flags = MEDIA_PAD_FL_SOURCE;
	priv->isp_pads[ISP_MAIN_DMA_WRITE_DPC].flags = MEDIA_PAD_FL_SOURCE;
	priv->isp_pads[ISP_MAIN_DMA_WRITE_HIST].flags = MEDIA_PAD_FL_SOURCE;

	ret = media_entity_pads_init(&priv->isp_sd.entity, ISP_MAIN_DMA_COUNT,
				     priv->isp_pads);
	if (ret < 0) {
		dev_err(dev, "media_entity_pads_init failed\n");
		goto err_register_subdev;
	}

	ret = media_create_pad_link(
		&priv->dma[ISP_MAIN_DMA_READ].video_dev.entity,
		ISP_MAIN_DMA_READ, &priv->isp_sd.entity, 0,
		MEDIA_LNK_FL_ENABLED);
	if (ret) {
		dev_err(dev, "media_create_pad_link(dma->isp) failed: %d\n",
			ret);
		goto err_register_subdev;
	}

	for (int i = ISP_MAIN_DMA_WRITE_HV; i < ISP_MAIN_DMA_COUNT; i++) {
		ret = media_create_pad_link(&priv->isp_sd.entity, i,
					    &priv->dma[i].video_dev.entity, 0,
					    MEDIA_LNK_FL_ENABLED);
		if (ret) {
			dev_err(dev, "media_create_pad_link(%d) failed: %d\n",
				i, ret);
			goto err_register_subdev;
		}
	}
#endif

	dev_info(dev, "create sysfs entries\n");
	ret = isp_main_create_sysfs(pdev);
	if (ret) {
		dev_err(dev, "isp_main_create_sysfs failed: %d\n", ret);
		goto err_register_subdev;
	}

	dev_info(dev, "isp-main-v4l2 probed\n");

	return 0;

err_register_subdev:
	v4l2_device_unregister_subdev(&priv->isp_sd);
	for (int i = 0; i < ISP_MAIN_DMA_COUNT; i++) {
		isp_dma_cleanup(&priv->dma[i]);
	}
#ifdef CONFIG_MEDIA_CONTROLLER
	media_device_cleanup(&priv->mdev);
#endif

err_register_dev:
	v4l2_ctrl_handler_free(&priv->v4l2_ctrl_handler);
	mutex_destroy(&priv->ctrl_lock);

err_init_dma:
	v4l2_device_unregister(&priv->v4l2_dev);

err_irqs:
	if (priv->irq_rpp_hdr_err >= 0)
		free_irq(priv->irq_rpp_hdr_err, priv);
	if (priv->irq_rpp_hdr_fun >= 0)
		free_irq(priv->irq_rpp_hdr_fun, priv);
	if (priv->irq_isp_sync_tpg >= 0)
		free_irq(priv->irq_isp_sync_tpg, priv);

err_subdevs:
	kfree(priv);

	return ret;
}

static int isp_main_v4l2_remove(struct platform_device *pdev)
{
	struct isp_main_v4l2_data *priv = platform_get_drvdata(pdev);

	if (priv == NULL)
		return 0;

	isp_main_remove_sysfs(pdev);
	v4l2_ctrl_handler_free(&priv->v4l2_ctrl_handler);
	v4l2_device_unregister(&priv->v4l2_dev);
	v4l2_device_unregister_subdev(&priv->isp_sd);
	for (int i = 0; i < ISP_MAIN_DMA_COUNT; i++) {
		isp_dma_cleanup(&priv->dma[i]);
	}
	mutex_destroy(&priv->ctrl_lock);

#ifdef CONFIG_MEDIA_CONTROLLER
	media_device_unregister(&priv->mdev);
	media_device_cleanup(&priv->mdev);
#endif

	if (priv->irq_rpp_hdr_err >= 0)
		free_irq(priv->irq_rpp_hdr_err, priv);
	if (priv->irq_rpp_hdr_fun >= 0)
		free_irq(priv->irq_rpp_hdr_fun, priv);
	if (priv->irq_isp_sync_tpg >= 0)
		free_irq(priv->irq_isp_sync_tpg, priv);

	return 0;
}

static struct of_device_id isp_main_v4l2_of_match[] = {
	{
		.compatible = "dct,dct-isp-main",
	},
	{},
};
MODULE_DEVICE_TABLE(of, isp_main_v4l2_of_match);

static struct platform_driver isp_main_v4l2_driver = {
    .probe = isp_main_v4l2_probe,
    .remove = isp_main_v4l2_remove,
    .driver =
        {
            .name = "dct-isp-main-v4l2",
            .owner = THIS_MODULE,
            .of_match_table = of_match_ptr(isp_main_v4l2_of_match),
        },
};

static int __init isp_main_v4l2_init_module(void)
{
	int ret;

	ret = platform_driver_register(&isp_main_v4l2_driver);
	if (ret)
		return ret;

	return ret;
}

static void __exit isp_main_v4l2_exit_module(void)
{
	platform_driver_unregister(&isp_main_v4l2_driver);
}

module_init(isp_main_v4l2_init_module);
module_exit(isp_main_v4l2_exit_module);

MODULE_DESCRIPTION("DCT ISP-MAIN V4L driver");
MODULE_LICENSE("GPL v2");
